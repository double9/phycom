<?php

namespace phycom\console\models;


use phycom\common\helpers\ChildProcess;

use yii\base\BaseObject;
use yii\helpers\Console;
use yii\helpers\FileHelper;
use yii;

/**
 * Class DataImporter
 * @package phycom\console\models
 */
class DataImporter extends BaseObject
{
    public $logHandler;


    private $prevLog;


    public function import($filename, $dumpOwner = null)
    {
        if (!$filename || !file_exists($filename)) {
            $this->log('File "' . $filename . '" not found', Console::FG_RED);
            return false;
        }

        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if ($ext !== 'zip') {
            $this->log('Only .zip files supported, ' . $ext . ' given', Console::FG_RED);
            return false;
        }

        $sourcePath = pathinfo($filename, PATHINFO_DIRNAME) . '/' . pathinfo($filename, PATHINFO_FILENAME);

        $this->log(' - extracting the source file...', Console::FG_PURPLE);
        $this->exec('unzip -o ' . $filename . ' -d ' . pathinfo($filename, PATHINFO_DIRNAME));


        $dumpfile = null;
        foreach (glob($sourcePath . '/dump_*.gz') as $file) {
            $dumpfile = $file;
            break;
        }
        if (!$dumpfile) {
            $this->log(' - db dump was not found', Console::FG_RED);
            return false;
        }
        $this->log(' - importing dump: ' . $dumpfile . '...' . PHP_EOL, Console::FG_PURPLE);
        if (!$this->importDbDump($dumpfile, $dumpOwner)) {
            return false;
        }
        $this->log(' - copying files in place...', Console::FG_PURPLE);

        foreach (glob($sourcePath . '/*', GLOB_ONLYDIR) as $path) {
            $dest = Yii::getAlias('@files/' .  basename($path));
            $this->log(' - copying ' . $path . ' to ' . $dest . '...');
            FileHelper::copyDirectory($path, $dest);
        }

        $this->log(' - cleaning up...', Console::FG_PURPLE);
        $this->exec('rm -r ' . $sourcePath);

        return true;
    }

    /**
     * @param string $filename
     * @param string|null $dumpOwner
     * @return bool
     */
    public function importDbDump($filename, $dumpOwner = null)
    {
        if (!$filename || !file_exists($filename)) {
            $this->log('File "' . $filename . '" not found', Console::FG_RED);
            return false;
        }

        $dbUser = getenv(ENV . '_DB_USER');
        $dbName = getenv(ENV . '_DB_NAME');
        $dbPass = getenv(ENV . '_DB_PASS');
        $dbHost = getenv(ENV . '_DB_HOST');
        $dbPort = getenv(ENV . '_DB_PORT');

        $this->log('Drop current database '.$dbName, Console::FG_PURPLE);
        $this->exec('psql postgres -U '.$dbUser.' -w -h '.$dbHost.' -p '.$dbPort.' -c "drop database if exists ' . $dbName . ';"');

        $this->log('Create database and user', Console::FG_PURPLE);
        $this->exec('psql postgres -U '.$dbUser.' -w -h '.$dbHost.' -p '.$dbPort.' -c "create database ' . $dbName . ';"');
        $this->exec('psql '.$dbName.' -U '.$dbUser.' -w -h '.$dbHost.' -p '.$dbPort.' -c "CREATE EXTENSION IF NOT EXISTS unaccent;"');
        $this->exec('psql '.$dbName.' -U '.$dbUser.' -w -h '.$dbHost.' -p '.$dbPort.' -c "CREATE EXTENSION IF NOT EXISTS fuzzystrmatch;"');

//        $this->exec('sudo -u postgres psql postgres -c "create user ' . $dbUser . ' with password \'' . $dbPass . '\';"');
        $this->exec('psql postgres -U '.$dbUser.' -w -h '.$dbHost.' -p '.$dbPort.' -c "grant all privileges on database ' . $dbName . ' to ' . $dbUser . ';"');

        if ($dumpOwner && $dumpOwner !== $dbUser) {
            $this->log('Dump owner is not same as current db user ('.$dbUser.'). Creating a new temporary db user ' . $dumpOwner, Console::FG_PURPLE);
            $this->exec('psql postgres -U '.$dbUser.' -w -h '.$dbHost.' -p '.$dbPort.' -c "create user ' . $dumpOwner . ' with password \'' . $dbPass . '\';"');
            $this->exec('psql postgres -U '.$dbUser.' -w -h '.$dbHost.' -p '.$dbPort.' -c "grant all privileges on database ' . $dbName . ' to ' . $dumpOwner . ';"');
            $this->log('Change database user to ' . $dumpOwner, Console::FG_PURPLE);
            Yii::$app->db->username = $dumpOwner;
        }

        $this->log('Importing the dump...', Console::FG_PURPLE);
        $this->exec('gunzip -c ' . $filename . ' | psql '. $dbName. ' -U '.$dbUser.' -w -h '.$dbHost.' -p '.$dbPort);
        $this->log('Import done', Console::FG_GREEN);

        if ($dumpOwner && $dumpOwner !== $dbUser) {
            $this->reassignOwner($dumpOwner, $dbUser, $dbName);
            Yii::$app->db->username = $dbUser;
        }
        return true;
    }

    /**
     * @param string $currentOwner
     * @param string $newOwner
     * @param null $dbName
     */
    public function reassignOwner($currentOwner, $newOwner, $dbName = null)
    {
        $this->log('Change database owner to ' . $newOwner, Console::FG_PURPLE);

        $dbUser = getenv(ENV . '_DB_USER');
        $dbHost = getenv(ENV . '_DB_HOST');
        $dbPort = getenv(ENV . '_DB_PORT');

        if (!$dbName) {
            $dbName = getenv(ENV . '_DB_NAME');
        }

        $this->exec('psql '.$dbName.' -U '.$dbUser.' -w -h '.$dbHost.' -p '.$dbPort.' -c "REASSIGN OWNED BY '.$currentOwner.' TO '.$newOwner.';"');
        $this->exec('psql postgres -U '.$dbUser.' -w -h '.$dbHost.' -p '.$dbPort.' -c "ALTER DATABASE '.$dbName.' OWNER TO '.$newOwner.';"');
        $this->exec('psql '.$dbName.' -U '.$dbUser.' -w -h '.$dbHost.' -p '.$dbPort.' -c "DROP OWNED BY '.$currentOwner.';"');
        $this->exec('psql '.$dbName.' -U '.$dbUser.' -w -h '.$dbHost.' -p '.$dbPort.' -c "DROP USER IF EXISTS '.$currentOwner.';"');
    }

    /**
     * @param string $cmd
     * @return int
     */
    protected function exec(string $cmd)
    {
        return ChildProcess::execute($cmd, function ($line) {
            $this->log(' - ' . $line);
        }, function ($line) {
            $this->log(' - ' . $line);
        });
    }

    /**
     * Calls the log handler and checks for duplicate messages
     * @param $msg
     */
    protected function log($msg)
    {
        Yii::info($msg, 'data-importer');
        if (null === $this->prevLog || $this->prevLog !== $msg) {
            if (is_callable($this->logHandler)) {
                call_user_func_array($this->logHandler, func_get_args());
            }
            $this->prevLog = $msg;
        }
    }
}
