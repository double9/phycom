<?php

namespace phycom\console\models;

use phycom\common\models\Address;
use phycom\common\models\attributes\AddressType;
use phycom\common\models\attributes\ContactAttributeStatus;
use phycom\common\models\attributes\OrderStatus;
use phycom\common\models\attributes\ShopStatus;
use phycom\common\models\attributes\UserStatus;
use phycom\common\models\attributes\UserType;
use phycom\common\models\Email;
use phycom\common\models\OrderItem;
use phycom\common\models\Phone;
use phycom\common\models\product\Product;
use phycom\common\models\User;

use yii\base\BaseObject;
use yii\base\Exception;
use yii;


/**
 * Class DataGenerator
 * @package phycom\console\models
 */
class DataGenerator extends BaseObject
{
    const DEFAULT_PASSWORD = 'parool';

    protected $firstNames = [
        "Leigar",
        "Kairi",
        "Egert",
        "Dini",
        "Mariaana",
        "Luiza",
        "Evar",
        "Juliana",
        "Sandra",
        "Anne",
        "Deevy",
        "Anke-Lele",
        "Margot",
        "Tarmo",
        "Rauno",
        "Sven",
        "Hille",
        "Janek",
        "Marko",
        "Maris",
        "helgi",
        "Juhan",
        "Elis",
        "Hardi",
        "Annekatrin",
        "Christel Tiina",
        "Eleri-Riin",
        "Peep",
        "Lisanne",
        "Marten",
        "Marjana",
        "Darja",
        "Reili",
        "Sirje",
        "Pille-Riina",
        "Aile",
        "Kerly",
        "Juhani",
        "Karol",
        "Järvik",
        "Iiris",
        "Loit",
        "Zulfira",
        "Kärt-Katrin",
        "Harri-Koit",
        "Edvika",
        "Greete",
        "Meigo",
        "Kaarin",
        "Tatjana",
        "Evi",
        "Arnaud",
        "Liise-Marie",
        "Eneli",
        "Jeroen",
        "Reijo",
        "Sirly",
        "Elsa",
        "Eda Ilves",
        "Tuulika",
        "Olav",
        "Jorma",
        "Mark",
        "Garita",
        "Andrus",
        "Karl-Sander",
        "Kersten",
        "Kalver",
        "Ave-Mari",
        "Janari",
        "Maria",
        "Mari-Leen",
        "Jolanda",
        "Laila",
        "Saari",
        "Brett",
        "Marliis",
        "Heidi Christel",
        "Marika",
        "Mariina",
        "Arlet",
        "Alejsa",
        "Jako",
        "vallot",
        "ljudmila",
        "Heivika",
        "Egeth",
        "virve",
        "Villem",
        "Clairi",
        "Egon",
        "Birget",
        "Markki",
        "margarita",
        "Pilve",
        "Zinaida",
        "Joonas",
        "Getri",
        "Anželika Sarapuu",
        "Art",
        "Rane",
        "kristiin",
        "Kadri Kaija",
        "Antti",
        "Katrine",
        "Meelis",
        "Liisa",
        "Pirli",
        "Diana-Enely",
        "Ramila",
        "Grethel",
        "Marlene",
        "Pjotr",
        "Birgit",
        "Jury",
        "Rivo",
        "Lilia",
        "Küllike",
        "Haldi",
        "Mariam",
        "Ann",
        "Kirli",
        "Merylin",
        "andrus",
        "Kevin",
        "Tairi",
        "Taago",
        "Aamo-Harand-Meego",
        "Mirja",
        "Ulvi",
        "Erika",
        "Kai Laanet",
        "Freddy",
        "Rünga",
        "Piotr",
        "Harles",
        "Valmo",
        "Erle",
        "Kätlin-Kairi",
        "Zhanna",
        "Janete",
        "Aare",
        "Randam",
        "Andres",
        "Mary-Liis",
        "Helen Marie",
        "Marin",
        "Vaive",
        "Fjodor",
        "Kristin",
        "Edward",
        "Eleriin",
        "Caroliina",
        "Hanna-Liis",
        "Kadi",
        "Ülvi",
        "Vjatsheslav",
        "Damir",
        "Janika",
        "Remy",
        "Julianna",
        "Bianka",
        "Ariine",
        "Herstin",
        "Natalija",
        "Olive",
        "Marja-liisa",
        "Valve",
        "Veera",
        "Arli",
        "Rudolf-Gustav",
        "Hannes",
        "Larissa",
        "Marjaliisa",
        "Christiane",
        "Eve-Liis",
        "Iren",
        "Shally",
        "Raagini",
        "Vlada",
        "Ruslana",
        "Hellevi",
        "Rasmus-Rai",
        "Eva",
        "Marjeta",
        "Veronika",
        "Jäälaid",
        "Kirsti",
        "Aleksandra",
        "Victoria",
        "Heldi-Mall",
        "Gert",
        "Reila",
        "Regita",
        "Tanel",
        "Timur",
    ];

    protected $lastNames = [
        "Sokolova",
        "Priidik",
        "Frolova",
        "Promann",
        "Hirvonen",
        "Põlts",
        "Pikkov",
        "Kraas",
        "Heinpalu",
        "Leinemann",
        "Rätsep",
        "Oblikas",
        "Gradickiene",
        "Undrits",
        "Kadakas",
        "kodu",
        "Parve",
        "Küünarpuu",
        "Seppik",
        "Virroja",
        "Batejev",
        "Kahar-Martijanova",
        "Orumaa",
        "Kärssin",
        "Puustusmaa",
        "vantsi",
        "Hallaste",
        "Grünvald",
        "Kippasto",
        "Rekkaro",
        "Sammal",
        "Talak",
        "Võigas",
        "Tee",
        "Ehandi",
        "Lomppi",
        "Trofimova",
        "Alfjorov",
        "Käsk",
        "Tserkassova",
        "Bõkova",
        "Haritonov",
        "Kalm",
        "Palmets",
        "Šavljuga",
        "Rüütelmaa",
        "Remmal",
        "Jakobs",
        "Priske",
        "Allikas",
        "Teeääre",
        "Semmel-Diligentov",
        "Kiili",
        "Buklin",
        "Kruzajeva",
        "Kaseväli",
        "Štaub",
        "Gradov",
        "Oll",
        "Smelovskaja",
        "Rõigas",
        "Andema",
        "Vilkis",
        "pruuli",
        "Bristol",
        "Volina",
        "Ojap",
        "Orglaan",
        "Luunja",
        "eenkivi",
        "Varov",
        "Loddes",
        "Kaitsa",
        "Hänilane",
        "Varend",
        "Villem",
        "Märtmaa-Olefirenko",
        "Mastepan",
        "Lihtmaa",
        "Nõmmik",
        "Voropajeva",
        "Koort",
        "Joonas",
        "Eks",
        "Malikov",
        "Fraiman",
        "peters",
        "Pommer",
        "gavriljuk",
        "Tapo",
        "Maalik",
        "Sinisaar",
        "Noormaa",
        "Muusikus",
        "Zova",
        "Prokofjeva",
        "Kulli",
        "Ets",
        "Tanavsuu",
        "Grenman",
        "Mannima",
        "Harmut",
        "Postnikova",
        "Petuhhov",
        "Saadi",
        "Karuks",
        "Koidumets",
        "Merimaa",
        "Heinpõld",
        "Sipria-Mironov",
        "Sooneste",
        "Roasto",
        "Dari",
        "Balaš",
        "Leoste",
        "Ojakivi",
        "Vabistsevits",
        "Fuchs",
        "Brandt",
        "Armulik",
        "Kivioja",
        "Sirkel",
        "Telepina",
        "Rundgren",
        "Titova",
        "Järveoja",
        "Stepankiene",
        "Liivoja",
        "Ahosepp",
        "Vilde",
        "Kapitonova",
        "Rool",
        "Bedina",
        "Riiknurm",
        "Vigur",
        "Ruut",
        "Madar",
        "Rikas-Toompere",
        "Noźejeva",
        "Horn-Kaha",
        "Zirel",
        "Seedre",
        "Priimäe",
        "Böttcher",
        "Liiva",
        "shkeneva",
        "Elvelt",
        "Gergents",
        "Rahumägi",
        "Pajundi",
        "Aigro-Kallo",
        "Polon",
        "Schellbach",
        "Gavšina",
        "Adelman",
        "Maliarova",
        "Tõkmann",
        "Kotrikadze",
        "Takmazjan",
        "Jõeäär",
        "Jõerand",
        "Narusberg",
        "Shved",
        "Rattur",
        "Pilv",
        "Barkina",
        "solovjova",
        "Aluve",
        "Dobrõšman",
        "Leetmaa",
        "Veider",
        "Tšepurin",
        "Padar-Leppiman",
        "Allika",
        "Lugovoja",
        "Kattel",
        "Ingver",
        "Zirnask",
        "Pallas",
        "Ljäkin",
        "Noormägi",
        "Jarova",
        "Kiltšitskaja",
        "Erlach",
        "Sinivee",
        "Raap",
        "Zhigareva",
        "Timoštšuk",
        "Punnik",
        "Heidemann",
        "Sosi",
        "Samoljuk",
        "Kitt",
        "Kiipus",
        "Klooren",
        "Rock",
        "Koemets",
        "Mirka",
    ];

    public function generateOrders(int $numOrders)
    {
        $shop = Yii::$app->modelFactory->getShop()::findOne(['status' => ShopStatus::ACTIVE]);
        $numProducts = Yii::$app->modelFactory->getProduct()::find()->count();
        for ($n = 0; $n < $numOrders; $n++) {
            /**
             * @var User $user
             */
            $user = Yii::$app->modelFactory->getUser()::find()
                ->where(['type' => UserType::CLIENT])
                ->orderBy('random()')
                ->one();

            $order = Yii::$app->modelFactory->getOrder();
            $order->number = $this->genRandomNr(10);
            $order->user_id = $user->id;
            if ($shop) {
                $order->shop_id = $shop->id;
            }
            $order->comment = 'Lorem ipsum';
            $order->status = OrderStatus::all()[array_rand(OrderStatus::all())];

            // TODO update
//            if (!rand(0,4)) {
//                $order->delivery_time = new \DateTime();
//                $order->delivery_time->add(new \DateInterval('PT' . rand(1, 100000) . 'S'));
//            }

            if (in_array($order->status, [
                OrderStatus::COMPLETE,
                OrderStatus::SHIPPED,
                OrderStatus::PROCESSING,
                OrderStatus::PARTIALLY_SHIPPED
            ])) {
                $order->paid_at = new \DateTime();
            }

            if (!$order->save()) {
                throw new Exception('Error saving order: ' . json_encode($order->errors, JSON_PRETTY_PRINT));
            }

            $orderLines = rand(1,$numProducts);
            $products = Yii::$app->modelFactory->getProduct()::find()->orderBy('random()')->limit($orderLines)->all();
            /**
             * @var Product[] $products
             */
            foreach ($products as $product) {

                $line = new OrderItem();
                $line->order_id = $order->id;
                $line->product_id = $product->id;
                $line->quantity = rand(1,3);
                $line->price = $product->price ? $product->price->getPrice() : 999;
                $line->code = $product->sku;
                $line->calculateTotal();

                if (!$line->save()) {
                    throw new Exception('Error saving order line: ' . json_encode($line->errors, JSON_PRETTY_PRINT));
                }
            }

            $line = new OrderItem();
            $line->order_id = $order->id;
            $line->quantity = 1;
            $line->price = 500;
            $line->code = 'DELIVERY';
            $line->meta = json_encode(['title' => 'Delivery']);
            $line->calculateTotal();

            if (!$line->save()) {
                throw new Exception('Error saving order line: ' . json_encode($line->errors, JSON_PRETTY_PRINT));
            }
        }
    }

    /**
     * Generates users for testing
     * @param int $numUsers - number of users to generate
     * @throws Exception
     */
    public function generateUsers(int $numUsers)
    {
        for ($n = 0; $n < $numUsers; $n++) {

            $fName = $this->firstNames[array_rand($this->firstNames)];
            $lName = $this->lastNames[array_rand($this->lastNames)];
            $user = $this->createUser(['first_name' => $fName, 'last_name' => $lName]);

            $email = new Email;
            $email->user_id = $user->id;
            $email->status = ContactAttributeStatus::all()[array_rand(ContactAttributeStatus::all())];
            $email->email = strtolower($fName) . '.' . strtolower($lName) . '_' . $this->genRandomNr(2);

            if (!$email->save()) {
                throw new Exception('Error saving user email: ' . json_encode($email->errors, JSON_PRETTY_PRINT));
            }

            $phone = new Phone();
            $phone->user_id = $user->id;
            $phone->status = ContactAttributeStatus::all()[array_rand(ContactAttributeStatus::all())];
            $phone->country_code = '372';
            $phone->phone_nr = '5' . $this->genRandomNr(7);

            if (!$phone->save()) {
                throw new Exception('Error saving user phone: ' . json_encode($phone->errors, JSON_PRETTY_PRINT));
            }

            $address = new Address();
            $address->user_id = $user->id;
            $address->type = AddressType::MAIN;
            $address->status = ContactAttributeStatus::all()[array_rand(ContactAttributeStatus::all())];
            $address->country = 'EE';
            $address->city = 'Viljandi';
            $address->street = 'Tallinna 6';
            $address->postcode = '71012';

            if (!$address->save()) {
                throw new Exception('Error saving user address: ' . json_encode($address->errors, JSON_PRETTY_PRINT));
            }
        }
    }


    /**
     * @param int $digits
     * @return string
     */
    protected function genRandomNr(int $digits)
    {
        return str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
    }

    /**
     * @param array $params
     * @return User
     * @throws Exception
     */
    protected function createUser(array $params)
    {
        $user = Yii::$app->modelFactory->getUser(ArrayHelper::merge([
            'password' => self::DEFAULT_PASSWORD,
            'type'     => UserType::CLIENT,
            'status'   => UserStatus::ACTIVE
        ], $params));
        $user->generateAuthKey();
        if (!$user->save()) {
            throw new Exception('Error saving user: ' . json_encode($user->errors, JSON_PRETTY_PRINT));
        }
        return $user;
    }
}
