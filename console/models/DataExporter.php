<?php

namespace phycom\console\models;


use phycom\common\helpers\ChildProcess;
use yii\base\BaseObject;
use yii\helpers\Console;
use yii\helpers\FileHelper;
use yii;

/**
 * Class DataExporter
 * @package phycom\console\models
 */
class DataExporter extends BaseObject
{
    public $includeFiles = true;

    public $logHandler;

    /**
     * @param $msg
     */
    protected function log($msg)
    {
        if (is_callable($this->logHandler)) {
            call_user_func_array($this->logHandler, func_get_args());
        }
        Yii::info($msg, 'data-exporter');
    }

    /**
     * @param string|null $filename
     * @return bool|string|null
     * @throws \Exception
     */
    public function export(string $filename = null)
    {
        $now = new \DateTime();
        if (!$filename) {
            $filename = Yii::getAlias('@files/data_export_' . $now->format('Ymd_His') . '.zip');
        }

        $exportPath = pathinfo($filename, PATHINFO_DIRNAME) . '/' . pathinfo($filename, PATHINFO_FILENAME);
        $exportFolder = pathinfo($filename, PATHINFO_FILENAME);

        if (!file_exists($exportPath)) {
            $this->log(' - creating temporary export path ' . $exportPath, Console::FG_PURPLE);
            mkdir($exportPath, 0777, true);
        }

        $dbHost = getenv(ENV . '_DB_HOST');
        $dbName = getenv(ENV . '_DB_NAME');
        $dbUser = Yii::$app->db->username;
        $dbPass = Yii::$app->db->password;
        $dumpFile = $exportPath . '/dump_' . $now->format('Ymd_His') . '.gz';

        $this->log(' - exporting all data from db to ' . $dumpFile . '...', Console::FG_PURPLE);

        ChildProcess::execute('PGPASSWORD=' . $dbPass . ' pg_dump -h ' . $dbHost . ' -U ' . $dbUser . ' --exclude-table-data=audit_trail --exclude-table-data=user_activity ' . $dbName . ' | gzip > ' . $dumpFile, function ($line) {
            $this->log(' - ' . $line);
        }, function ($line) {
            $this->log(' - ' . $line);
        });

        $this->log(' - db dump created. Continuing with file copy...', Console::FG_GREEN);
        $filePaths = [
            Yii::getAlias('@files/content'),
            Yii::getAlias('@files/invoice'),
            Yii::getAlias('@files/payment-keys'),
            Yii::getAlias('@files/product')
        ];

        foreach ($filePaths as $path) {
            if (is_dir($path)) {
                $dest = $exportPath . '/' . basename($path);
                $this->log(' - copying ' . $path . ' to ' . $dest . '...');
                FileHelper::copyDirectory($path, $dest);
            }
        }

        $this->log(' - file copy complete, compressing export path contents...', Console::FG_GREEN);
        exec('cd ' . Yii::getAlias('@files') . ' && zip -r ' . $filename . ' ./' . $exportFolder . '/*');

        $this->log(' - removing ' . $exportPath . ' folder and its contents...' , Console::FG_YELLOW);
        exec('rm -r ' . $exportPath);
        return $filename;
    }
}
