<?php

namespace phycom\console\controllers;

use phycom\common\models\attributes\MessageStatus;
use phycom\common\models\Message;

use yii\helpers\Console;
use yii\console\ExitCode;

/**
 * Class MessageDispatcherController
 * @package phycom\console\controllers
 */
class MessageDispatcherController extends BaseConsoleController
{
    /**
     * @return int
     * @throws \yii\base\Exception
     */
    public function actionReQueue()
    {
        $query = Message::find()->where(['status' => MessageStatus::QUEUED]);
        $n = 0;
        if ($this->confirm($query->count() . ' queued messages found. Are you sure you want to re-queue these messages?')) {
            foreach ($query->batch() as $messages) {
                /**
                 * @var Message[] $messages
                 */
                foreach ($messages as $message) {
                    $this->printLn($message->queue());
                    $n++;
                }
            }
        }
        $this->printLn($n . ' messages queued', Console::FG_GREEN);
        return ExitCode::OK;
    }
}