<?php
namespace phycom\console\controllers;

use phycom\common\models\traits\ComponentTrait;

use yii\web\NotFoundHttpException;
use yii\console\Controller;
use yii;

/**
 * Class BaseConsoleController
 * @package phycom\common\console\controllers
 */
class BaseConsoleController extends Controller
{
    use ComponentTrait;
    /**
     * @var bool
     */
    public bool $verbose = true;

    /**
     * @param yii\base\Action $action
     * @return bool
     * @throws NotFoundHttpException
     */
    public function beforeAction($action)
    {
        if (!Yii::$app->request->isConsoleRequest) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function options($actionId)
    {
        return [
            'verbose'
        ];
    }

    /**
     * @param mixed $msg
     */
    public function printLn($msg)
    {
        if ($this->verbose) {
            echo call_user_func_array([$this, 'ansiFormat'], func_get_args()) . PHP_EOL;
        }
    }

    /**
     * @param mixed $msg
     */
    public function printMsg($msg)
    {
        if ($this->verbose) {
            echo call_user_func_array([$this, 'ansiFormat'], func_get_args());
        }
    }
}
