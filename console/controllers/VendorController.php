<?php

namespace phycom\console\controllers;


use phycom\common\models\attributes\ContactAttributeStatus;
use phycom\common\models\attributes\VendorStatus;
use phycom\common\models\Email;
use phycom\common\models\Phone;
use phycom\common\models\Vendor;

use yii\console\ExitCode;
use yii\helpers\Console;
use yii;

/**
 * Class VendorController
 * @package phycom\console\controllers
 */
class VendorController extends BaseConsoleController
{
	public $defaultAction = 'list';

	public function actionList()
	{
		$query = Yii::$app->modelFactory->getVendor()::find()->where([
			'not', ['status' => VendorStatus::DELETED]
		])->orderBy(['created_at' => SORT_ASC]);

		$count = $query->count();
		$this->printLn($count . ' record(s) found', Console::FG_YELLOW);
		foreach ($query->all() as $vendor) {
			$this->printLn($this->printVendor($vendor), Console::FG_GREY);
		}
		return ExitCode::OK;
	}

	public function addPhone($vendorId, $phoneNr, $status = ContactAttributeStatus::VERIFIED)
	{
		$vendor = Yii::$app->modelFactory->getVendor()::findOne($vendorId);
		if (!$vendor) {
			$this->printLn('Vendor ' . $vendorId . ' not found', Console::FG_RED);
			return ExitCode::USAGE;
		}

		$phone = new Phone();
		$phone->country_code = '372';
		$phone->phone_nr = $phoneNr;
		$phone->vendor_id = $vendor->id;
		$phone->status = $status;
		if (!$phone->save()) {
			throw new yii\base\Exception('Error saving email ' . json_encode($phone->errors));
		}
		return ExitCode::OK;
	}

	/**
	 * @param $vendorId
	 * @param $emailAddress
	 * @param string $status
	 * @return int
	 * @throws yii\base\Exception
	 */
	public function actionAddEmail($vendorId, $emailAddress, $status = ContactAttributeStatus::VERIFIED)
	{
		$vendor = Yii::$app->modelFactory->getVendor()::findOne($vendorId);
		if (!$vendor) {
			$this->printLn('Vendor ' . $vendorId . ' not found', Console::FG_RED);
			return ExitCode::USAGE;
		}
		$email = new Email();
		$email->email = $emailAddress;
		$email->vendor_id = $vendor->id;
		$email->status = $status;
		if (!$email->save()) {
			throw new yii\base\Exception('Error saving email ' . json_encode($email->errors));
		}
		return ExitCode::OK;
	}

	/**
	 * @param Vendor $vendor
	 * @param bool $includeMeta
	 * @return string
	 */
	protected function printVendor(Vendor $vendor, $includeMeta = true)
	{
		$vendorData = $this->exportAttributes($vendor->attributes);
		$vendorData['emails'] = [];
		$vendorData['addresses'] = [];
		$vendorData['phones'] = [];

		foreach ($vendor->emails as $email) {
			$vendorData['emails'][] = $this->exportAttributes($email->attributes);
		}
		foreach ($vendor->addresses as $address) {
			$vendorData['addresses'][] = $this->exportAttributes($address->attributes);
		}
		foreach ($vendor->phones as $phone) {
			$vendorData['phones'][] = $this->exportAttributes($phone->attributes);
		}
		return json_encode($vendorData, JSON_PRETTY_PRINT);
	}

	protected function exportAttributes(array $modelAttributes)
	{
		$attributes = [];
		foreach ($modelAttributes as $key => $value) {
			if (is_object($value)) {
				if (method_exists($value, '__toString')) {
					$attributes[$key] = (string) $value;
				} else if ($value instanceof \DateTime) {
					$attributes[$key] = $value->format(\DateTime::RFC3339_EXTENDED);
				} else {
					$attributes[$key] = $value;
				}
			} else {
				$attributes[$key] = $value;
			}
		}
		return $attributes;
	}
}