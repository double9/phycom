<?php

namespace phycom\console\controllers;

use phycom\common\jobs\ReferenceNumberJob;
use phycom\common\models\attributes\UserStatus;
use phycom\common\models\attributes\UserTokenType;
use phycom\common\models\attributes\UserType;
use phycom\common\models\User;
use phycom\common\models\UserToken;

use yii\console\ExitCode;
use yii\helpers\Console;
use yii;

class UserController extends BaseConsoleController
{
	const DEFAULT_PASSWORD = 'parool';

	/**
	 * Creates a new admin user
	 *
	 * @param $username
	 * @param string $password
	 * @param integer $vendorId
	 * @return int
	 */
	public function actionCreateAdminUser($username, $password = self::DEFAULT_PASSWORD, $vendorId = 1)
	{
		$user = Yii::$app->modelFactory->getUser();

		$user->username = $username;
		$user->first_name = ucfirst($username);
		$user->last_name = ucfirst(strrev($username));
		$user->password = $password;
		$user->type = UserType::ADMIN;
		$user->status = UserStatus::ACTIVE;
		$user->generateAuthKey();

		if (!$user->save()) {
			$this->printLn(json_encode($user->errors, JSON_PRETTY_PRINT), Console::FG_YELLOW);
			return ExitCode::USAGE;
		}

		Yii::$app->authManager->assign(Yii::$app->authManager->getRole('admin'), $user->id);

		$vendor = Yii::$app->modelFactory->getVendor()::findOne($vendorId);
		if ($vendor) {
			$vendor->addUser($user);
		}

		$this->printLn('User ' . $user->id . ' successfully created', Console::FG_GREEN);
		$this->printLn(json_encode($user->attributes, JSON_PRETTY_PRINT), Console::FG_PURPLE);
		return ExitCode::OK;
	}

	/**
	 * Creates a new test user
	 *
	 * @param $username
	 * @param string $password
	 * @return int
	 */
	public function actionCreateTestUser($username, $password = self::DEFAULT_PASSWORD)
	{
		$user = Yii::$app->modelFactory->getUser();

		$user->username = $username;
		$user->first_name = ucfirst($username);
		$user->last_name = ucfirst(strrev($username));
		$user->password = $password;
		$user->type = UserType::CLIENT;
		$user->status = UserStatus::ACTIVE;
		$user->generateAuthKey();

		if (!$user->save()) {
			$this->printLn(json_encode($user->errors, JSON_PRETTY_PRINT), Console::FG_YELLOW);
			return ExitCode::USAGE;
		}
		$this->printLn('User ' . $user->id . ' successfully created', Console::FG_GREEN);
		$this->printLn(json_encode($user->attributes, JSON_PRETTY_PRINT), Console::FG_PURPLE);
		return ExitCode::OK;
	}

	/**
	 * Generates access token
	 * @param int $userId
	 * @return int
	 */
	public function actionGenerateAccessToken($userId)
	{
		$token = new UserToken();
		$token->user_id = $userId;
		$token->type = UserTokenType::ACCESS_TOKEN;
		$token->generate()->save();

		$this->printLn($token->token, Console::FG_GREEN);
		return ExitCode::OK;
	}

	/**
	 * @return int
	 */
	public function actionAssignAdminUserPermissions()
	{
		$query = Yii::$app->modelFactory->getUser()::find()->where(['type' => UserType::ADMIN]);

		foreach ($query->batch() as $users) {
			/**
			 * @var User[] $users
			 */
			foreach ($users as $user) {
				Yii::$app->authManager->assign(Yii::$app->authManager->getRole('admin'), $user->id);
				$this->printLn('User ' . $user->id . ' permissions regenerated', Console::FG_PURPLE);
			}
		}
		$this->printLn('Done', Console::FG_GREEN);
		return ExitCode::OK;
	}

	/**
	 * @return int
	 */
	public function actionAssignAdminUserVendor()
	{
		$query = Yii::$app->modelFactory->getUser()::find()->where(['type' => UserType::ADMIN]);
		$vendor = Yii::$app->modelFactory->getVendor()::findOne(1);
		foreach ($query->batch() as $users) {
			/**
			 * @var User[] $users
			 */
			foreach ($users as $user) {
				if ($vendor) {
					$vendor->addUser($user);
				}
				$this->printLn('User ' . $user->id . ' vendor ' . $vendor->id, Console::FG_PURPLE);
			}
		}
		$this->printLn('Done', Console::FG_GREEN);
		return ExitCode::OK;
	}

	public function actionGenRefNumbers()
	{
		$query = Yii::$app->modelFactory->getUser()::find();
		$query->where('reference_number IS NULL');

		foreach ($query->batch() as $users) {
			foreach ($users as $user) {
				/**
				 * @var User $user
				 */
				Yii::$app->queue1->push(new ReferenceNumberJob(['id' => $user->id]));
				$this->printLn($user->id . ' queued', Console::FG_GREY);
			}
		}
		$this->printLn('Done', Console::FG_GREEN);
		return ExitCode::OK;
	}
}