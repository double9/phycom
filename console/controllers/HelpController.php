<?php

namespace phycom\console\controllers;

use Yii;
use yii\console\Application as ConsoleApplication;
use yii\console\controllers\HelpController as BaseHelpController;
use yii\helpers\Inflector;

/**
 * Provides help information about console commands.
 *
 * Class HelpController
 * @package phycom\console\controllers
 */
class HelpController extends BaseHelpController
{
    /**
     * Returns available commands of a specified module.
     * @param \yii\base\Module $module the module instance
     * @return array the available command names
     */
    protected function getModuleCommands($module)
    {
        $prefix = $module instanceof ConsoleApplication ? '' : $module->getUniqueID() . '/';

        $commands = [];
        foreach (array_keys($module->controllerMap) as $id) {
            $commands[] = $prefix . $id;
        }

//        foreach ($module->getModules() as $id => $child) {
//            if (($child = $module->getModule($id)) === null) {
//                continue;
//            }
//            foreach ($this->getModuleCommands($child) as $command) {
//                $commands[] = $command;
//            }
//        }

        $controllerPath = $module->getControllerPath();
        if (is_dir($controllerPath)) {
            $files = scandir($controllerPath);
            foreach ($files as $file) {
                if (!empty($file) && substr_compare($file, 'Controller.php', -14, 14) === 0) {
                    $controllerClass = $module->controllerNamespace . '\\' . substr(basename($file), 0, -4);
                    if ($this->validateControllerClass($controllerClass)) {
                        $commands[] = $prefix . Inflector::camel2id(substr(basename($file), 0, -14));
                    }
                }
            }
        }

        if (substr($module->controllerNamespace, 0, 4) !== 'base') {
	        /**
	         * Scan the app namespace too to include base app controllers
	         */
	        $appControllerNamespace = 'phycom\\' . $module->controllerNamespace;
	        $appControllerPath = Yii::getAlias('@' . str_replace('\\', '/', $appControllerNamespace));
	        if (is_dir($appControllerPath)) {
		        $files = scandir($appControllerPath);
		        foreach ($files as $file) {
			        if (!empty($file) && substr_compare($file, 'Controller.php', -14, 14) === 0) {
				        $controllerClass = $appControllerNamespace . '\\' . substr(basename($file), 0, -4);
				        if ($this->validateControllerClass($controllerClass)) {
					        $commands[] = $prefix . Inflector::camel2id(substr(basename($file), 0, -14));
				        }
			        }
		        }
	        }
        }

        return $commands;
    }
}