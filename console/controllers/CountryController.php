<?php

namespace phycom\console\controllers;

use phycom\common\models\Country;

use yii\console\ExitCode;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class CountryController
 * @package phycom\console\controllers
 */
class CountryController extends BaseConsoleController
{
    public function actionListByRegion($region = 'Europe')
    {
        $countries = Country::loadByCondition('geo.region', $region);
        $codes = ArrayHelper::map($countries, 'code', 'name');

        $this->printLn(Json::encode($codes, JSON_PRETTY_PRINT));
        return ExitCode::OK;
    }
}