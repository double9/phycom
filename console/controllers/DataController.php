<?php

namespace phycom\console\controllers;


use phycom\console\models\DataExporter;
use phycom\console\models\DataGenerator;
use phycom\console\models\DataImporter;

use yii\console\ExitCode;
use yii\helpers\Console;
use yii;

/**
 * Class DataController
 * @package phycom\console\controllers
 */
class DataController extends BaseConsoleController
{
    /**
     * Generates users for testing
     *
     * @param int $total - num. users to be generated
     * @return int
     * @throws yii\base\Exception
     */
	public function actionGenerateUsers($total = 100)
	{
        $generator = new DataGenerator();
        $generator->generateUsers($total);
        return ExitCode::OK;
	}

    /**
     * Generates dummy orders for testing.
     *
     * @param int $total
     * @return int
     * @throws yii\base\Exception
     */
	public function actionGenerateOrders($total = 50)
	{
	    $generator = new DataGenerator();
	    $generator->generateOrders($total);
		return ExitCode::OK;
	}

    /**
     * Export data from db and all files to a single zip file
     *
     * @param null $filename
     * @param bool $includeFiles
     * @return int
     * @throws \Exception
     */
	public function actionExport($filename = null, $includeFiles = true)
    {
        $exporter = new DataExporter();
        $exporter->logHandler = [$this, 'printLn'];
        $exporter->includeFiles = $includeFiles;

        $this->printLn('Start data export...', Console::FG_PURPLE);
        $file = $exporter->export($filename);
        $this->printLn('Data export complete', Console::FG_GREEN);
        $this->printLn('Export file ' . $file . ' was successfully generated', Console::FG_GREEN);
        return ExitCode::OK;
    }

    /**
     * Imports the zipped data file that was previously exported with DataExporter
     *
     * @param string $filename
     * @param bool $obfuscate
     * @return int
     * @throws yii\db\Exception
     */
    public function actionImport($filename, $obfuscate = false)
    {
        if ($this->confirm($this->ansiFormat('Are you sure you want to import data? All existing data will be overwritten!', Console::BG_RED))) {
            $importer = new DataImporter();
            $importer->logHandler = [$this, 'printLn'];
            if (!$importer->import($filename)) {
                return ExitCode::USAGE;
            }
            if (YII_ENV_PROD && $obfuscate) {
                $this->printLn('Obfuscation is not available in production', Console::FG_YELLOW);
            }
            if (!YII_ENV_PROD && $obfuscate && $this->actionObfuscate() === ExitCode::USAGE) {
                return ExitCode::USAGE;
            }
        }
        return ExitCode::OK;
    }


	/**
	 * Imports the gzipped dump file and obfuscates the data.
	 *
	 * @param string $filename
	 * @param string $dumpOwner - (optional) - imports using custom owner if the dump owner is not same as local db user
	 * @param bool $obfuscate - (optional) - obfuscates the imported data
	 *
	 * @return int - exit code
	 * @throws yii\db\Exception
	 */
	public function actionImportDbDump($filename, $dumpOwner = null, $obfuscate = true)
	{
        if ($this->confirm('Are you sure you want to import data from dump? All existing data will be lost')) {

            $importer = new DataImporter();
            $importer->logHandler = [$this, 'printLn'];

            if (!$importer->importDbDump($filename, $dumpOwner)) {
                return ExitCode::USAGE;
            }
            if (YII_ENV_PROD && $obfuscate) {
                $this->printLn('Obfuscation is not available in production', Console::FG_YELLOW);
            }
            if (!YII_ENV_PROD && $obfuscate && $this->actionObfuscate() === ExitCode::USAGE) {
                return ExitCode::USAGE;
            }
        }
		$this->printLn('All done', Console::FG_GREEN);
		return ExitCode::OK;
	}

	/**
	 * @param string $currentOwner
	 * @param string $newOwner
	 * @param string $dbName - optional db name
	 * @return int
	 */
	public function actionReassignDbOwner($currentOwner, $newOwner, $dbName = null)
	{
		if (YII_ENV_PROD) {
			$this->printLn('Can only be run locally', Console::FG_RED);
			return ExitCode::USAGE;
		}
		$importer = new DataImporter();
		$importer->logHandler = [$this, 'printLn'];
		$importer->reassignOwner($currentOwner, $newOwner, $dbName);
		return ExitCode::OK;
	}

	/**
	 * Obfuscates the data on database
	 *
	 * @return int
	 * @throws yii\db\Exception
	 */
	public function actionObfuscate()
	{
		if (YII_ENV_PROD) {
			$this->printLn('Can only be run locally', Console::FG_RED);
			return ExitCode::USAGE;
		}

		$this->printLn('Start data obfuscation...', Console::FG_PURPLE);
		$this->printMsg(' - clear audit trail data... ');
		Yii::$app->db->createCommand()->update('audit_trail', ['old_value' => null, 'new_value' => null])->execute();
		$this->printLn('ok', Console::FG_GREEN);

		$this->printMsg(' - update all passwords to be the same... ');
		Yii::$app->db->createCommand()->update('user', ['password_hash' => '$2y$13$Ywepm3ddw8R4ZAKImjyU9OxeHSWnBJnSOaOjE6754onEG8flLCfYu'])->execute();
		$this->printLn('ok', Console::FG_GREEN);

		$this->printMsg(' - update all usernames from personal-codes to database ID values... ');
		Yii::$app->db->createCommand("UPDATE \"user\" SET username = id WHERE username SIMILAR TO '\d{11}'")->execute();
//		Yii::$app->db->createCommand("ALTER TABLE \"user\" ADD CONSTRAINT user_personal_code UNIQUE USING INDEX user_personal_code DEFERRABLE;")->execute();
		$this->printLn('ok', Console::FG_GREEN);

		$this->printMsg(' - shuffle user personal codes in the database... ');
		$transaction = Yii::$app->db->beginTransaction();
		Yii::$app->db->createCommand("SET CONSTRAINTS ALL DEFERRED")->execute();
		Yii::$app->db->createCommand(<<<'EOD'
UPDATE "user" u1
SET personal_code = rn.personal_code FROM UNNEST(ARRAY(SELECT u2.personal_code FROM "user" u2 ORDER BY random()), ARRAY(SELECT u1.id FROM "user" u1)) as rn(personal_code, id)
WHERE u1.id = rn.id;
EOD
		)->execute();
		$transaction->commit();
		$this->printLn('ok', Console::FG_GREEN);

		$this->printMsg(' - replace all e-mail addresses... ');
		Yii::$app->db->createCommand("UPDATE \"email\" SET email = concat('user_', user_id, '+', id, '@test.local') ")->execute();
		$this->printLn('ok', Console::FG_GREEN);

		$this->printMsg(' - replace all user phones... ');
		Yii::$app->db->createCommand("UPDATE \"phone\" SET phone_nr = overlay(phone_nr placing '000' from 7 for 3)")->execute();
		$this->printLn('ok', Console::FG_GREEN);

		$this->printMsg(' - remove all user tokens... ');
		Yii::$app->db->createCommand("ALTER TABLE \"user_token\" DROP CONSTRAINT IF EXISTS user_token_token_key")->execute();
		Yii::$app->db->createCommand("DROP INDEX IF EXISTS user_token_token_key")->execute();
		Yii::$app->db->createCommand("UPDATE \"user_token\" SET token = 'dummytoken'")->execute();
		$this->printLn('ok', Console::FG_GREEN);

		$this->printMsg(' - remove text from all messages... ');
		Yii::$app->db->createCommand("UPDATE \"message\" SET content = 'dummycontent' WHERE type = 'message'")->execute();
		$this->printLn('ok', Console::FG_GREEN);

		$this->printLn('Obfuscation complete.', Console::FG_PURPLE);
		return ExitCode::OK;
	}
}
