<?php

namespace phycom\console\controllers;

use yii\console\ExitCode;
use yii;

/**
 * Reloads the user permissions
 */
class PermissionController extends BaseConsoleController
{
    /**
     * @var string controller default action ID.
     */
    public $defaultAction = 'reload';

    /**
     * Updates user permission from the data array
     */
    public function actionReload()
    {
        Yii::$app->modelFactory->getRbacData()->reload();
        return ExitCode::OK;
    }
}