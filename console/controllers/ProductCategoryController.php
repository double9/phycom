<?php

namespace phycom\console\controllers;

use phycom\common\models\attributes\CategoryStatus;
use phycom\common\models\product\ProductCategory;

use yii\console\ExitCode;


/**
 * Class ProductCategoryController
 * @package phycom\console\controllers
 */
class ProductCategoryController extends BaseConsoleController
{
    /**
     * Checks that all categories with parent deleted will be deleted also
     */
	public function actionPurge()
	{
        /**
         * @var ProductCategory[] $categories
         */
        $categories = ProductCategory::find()->where(['status' => CategoryStatus::DELETED])->all();
        foreach ($categories as $category) {
            $category->deleteRecursive();
        }
        return ExitCode::OK;
	}
}
