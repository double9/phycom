<?php

namespace phycom\console\controllers;


use phycom\common\helpers\Date;
use phycom\common\jobs\InvoicePdfJob;

use phycom\common\models\attributes\InvoiceStatus;
use phycom\common\models\Invoice;
use yii\helpers\Console;
use yii\console\ExitCode;
use yii;

/**
 * Class InvoiceController
 * @package phycom\console\controllers
 */
class InvoiceController extends BaseConsoleController
{
    /**
     * @param $invoiceNumber
     * @param bool $queue
     * @return int
     * @throws \Mpdf\MpdfException
     * @throws yii\base\Exception
     */
    public function actionRegeneratePdf($invoiceNumber, $queue = true)
    {
        $job = new InvoicePdfJob();
        $job->invoiceNumber = $invoiceNumber;

        if ($queue) {
            $this->printLn('Job ' . Yii::$app->queue1->push($job) . ' queued', Console::FG_YELLOW);
        } else {
            $job->execute(Yii::$app->queue1);
        }
        return ExitCode::OK;
    }

    /**
     * @param string|null $since - a date since the invoices should be regenerated
     * @return int
     */
    public function actionRegeneratePdfAll($since = null)
    {
        $since = $since ? new \DateTime($since) : null;
        if ($since) {
            $this->printLn('Regenerating all invoices since ' . $since->format('Y-m-d') . '...', Console::FG_YELLOW);
        } else {
            $this->printLn('Regenerating all invoices...', Console::FG_YELLOW);
        }

        $query = Yii::$app->modelFactory->getInvoice()::find()
            ->where(['not', ['status' => [InvoiceStatus::DELETED, InvoiceStatus::CANCELED]]]);
        if ($since) {
            $query->andWhere(['>', 'created_at', Date::create($since)->dbTimestamp]);
        }
        $count = $query->count();
        $queued = [];
        if ($this->confirm($count . ' invoices found, continue?')) {
            foreach ($query->batch() as $invoices) {
                /**
                 * @var Invoice $invoice
                 */
                foreach ($invoices as $invoice) {
                    $job = new InvoicePdfJob();
                    $job->invoiceNumber = $invoice->number;
                    $queued[] = Yii::$app->queue1->push($job);
                }
            }
        }
        $this->printLn(count($queued) . ' jobs queued', Console::FG_GREEN);
        return ExitCode::OK;
    }
}