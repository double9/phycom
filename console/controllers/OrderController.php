<?php

namespace phycom\console\controllers;

use phycom\common\models\attributes\OrderStatus;
use phycom\common\models\attributes\InvoiceStatus;
use phycom\common\models\Order;

use yii\console\ExitCode;
use yii\helpers\Console;
use yii;

/**
 * Class OrderController
 * @package phycom\console\controllers
 */
class OrderController extends BaseConsoleController
{
	const EXPIRE_AFTER_DAYS = 5;

	public function actionStatusCheck()
	{
		$query = Yii::$app->modelFactory->getOrder()::find()
			->where(['not', ['status' => [OrderStatus::COMPLETE, OrderStatus::DELETED, OrderStatus::CLOSED]]]);

		$currentDate = new \DateTime();

		foreach ($query->batch() as $orders) {
			/**
			 * @var Order $order
			 */
			foreach ($orders as $order) {
				$diff = $order->updated_at->diff($currentDate);
				if ($order->status->in([OrderStatus::NEW, OrderStatus::PENDING_PAYMENT]) && $diff->days > self::EXPIRE_AFTER_DAYS) {
					$order->updateStatus(OrderStatus::EXPIRED);
					foreach ($order->invoices as $invoice) {
						$invoice->updateStatus(InvoiceStatus::DELETED);
					}
					continue;
				}
			}
		}
		return ExitCode::OK;
	}

	public function actionInfo($id)
	{
		$order = Yii::$app->modelFactory->getOrder()::findOne($id);
		if (!$order) {
			$this->printLn('Order ' . $id . ' was not found', Console::FG_RED);
			return ExitCode::USAGE;
		}

		$info = [
			'processing time' => $order->totalProcessingTime
		];
		$this->printLn(json_encode($info, JSON_PRETTY_PRINT), Console::FG_YELLOW);
		return ExitCode::OK;
	}


	public function regenerateInvoice($order)
    {

    }
}