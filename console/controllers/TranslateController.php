<?php

namespace phycom\console\controllers;

use yii\base\UserException as Exception;
use yii\console\controllers\MessageController;
use yii\console\ExitCode;
use yii\db\Connection;
use yii\di\Instance;
use yii\helpers\FileHelper;
use yii\helpers\ArrayHelper;
use yii;

/**
 * Extracts messages to be translated from source files.
 * Modification of the Yii's default Message controller
 *
 * Class TranslateController
 * @package phycom\console\controllers
 */
class TranslateController extends MessageController
{
	/**
	 * @var string controller default action ID.
	 */
	public $defaultAction = 'extract';
	/**
	 * @var string required, root directory of all source files.
	 */
	public $sourcePath = '@app' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR;
	/**
	 * @var string required, root directory containing message translations.
	 */
	public $messagePath = '@app' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'translations';
	/**
	 * @var array required, list of language codes that the extracted messages
	 * should be translated to. For example, ['zh-CN', 'de'].
	 */
	public $languages = [];
	/**
	 * @var string the name of the function for translating messages.
	 * Defaults to 'Yii::t'. This is used as a mark to find the messages to be
	 * translated. You may use a string for single function name or an array for
	 * multiple function names.
	 */
	public $translator = ['Yii::t', 'yiit'];
	/**
	 * @var string generated file format. Can be "php", "db", "po" or "pot".
	 */
	public $format = 'po';
	/**
	 * @var bool whether the message file should be overwritten with the merged messages
	 */
	public $overwrite = true;
	/**
	 * @var bool whether to sort messages by keys when merging new messages
	 * with the existing ones. Defaults to false, which means the new (untranslated)
	 * messages will be separated from the old (translated) ones.
	 */
	public $sort = true;

    /**
     * @var bool whether to remove messages that no longer appear in the source code.
     * Defaults to false, which means these messages will NOT be removed.
     */
    public $removeUnused = true;
	/**
	 * @var array list of patterns that specify which files/directories should NOT be processed.
	 * If empty or not set, all files/directories will be processed.
	 * See helpers/FileHelper::findFiles() description for pattern matching rules.
	 * If a file/directory matches both a pattern in "only" and "except", it will NOT be processed.
	 */
	public $except = [
		'.svn',
		'.git',
		'.gitignore',
		'.gitkeep',
		'.hgignore',
		'.hgkeep',
		'.bowerrc',
		'.vagrant',
		'.idea',
		'.DS_Store',
		'*.yml',
		'/vendor',
		'/logs',
		'/files',
		'/environments',
		'/ansible',
		'/translations'
	];
	/**
	 * @var string name of the file that will be used for translations for "po" format.
	 */
	public $catalog = 'messages';
	/**
	 * @var array list of patterns that specify which files (not directories) should be processed.
	 * If empty or not set, all files will be processed.
	 * See helpers/FileHelper::findFiles() description for pattern matching rules.
	 * If a file/directory matches both a pattern in "only" and "except", it will NOT be processed.
	 */
	public $only = ['*.php','*.twig'];


	public function init()
	{
		parent::init();
		$this->languages = Yii::$app->params['languages'];
	}

	public function actionExtract($configFile = null)
	{
		$this->actionExtractBase();
		$this->actionExtractApp();
		return ExitCode::OK;
	}


    /**
     * Extracts base translation source strings
     *
     * @return int
     * @throws Exception
     * @throws yii\base\InvalidConfigException
     */
	public function actionExtractBase()
	{
	    $this->messagePath = '@phycom' . DIRECTORY_SEPARATOR . 'translations';

		$this->sourcePath = '@phycom' . DIRECTORY_SEPARATOR . 'backend';
		$this->catalog = 'backend';
		$this->extractInternal();

        $except = $this->except;
        $this->except[] = 'modules/';
		$this->sourcePath = '@phycom' . DIRECTORY_SEPARATOR . 'common';
		$this->catalog = 'common';
		$this->extractInternal();
		$this->except = $except;

        $this->sourcePath = '@phycom' . DIRECTORY_SEPARATOR . 'common' . DIRECTORY_SEPARATOR . 'modules';
        $this->catalog = 'modules';
        $this->extractInternal();

		$this->sourcePath = '@phycom' . DIRECTORY_SEPARATOR . 'frontend';
		$this->catalog = 'public';
		$this->extractInternal();

		$this->sourcePath = '@phycom' . DIRECTORY_SEPARATOR . 'templates';
		$this->catalog = 'template';
		$this->extractInternal();

		return ExitCode::OK;
	}


    /**
     * Extracts current application source strings
     *
     * @return int
     * @throws Exception
     * @throws yii\base\InvalidConfigException
     */
	public function actionExtractApp()
	{
		$this->messagePath = '@translations';

		$this->sourcePath = '@backend';
		$this->catalog = 'backend';
		$this->extractInternal();

		$this->sourcePath = '@common';
		$this->catalog = 'common';
		$this->extractInternal();

		$this->sourcePath = '@frontend';
		$this->catalog = 'public';
		$this->extractInternal();

		$this->sourcePath = '@templates';
		$this->catalog = 'template';
		$this->extractInternal();

		return ExitCode::OK;
	}


    /**
     * Copied from vendor/yiisoft/yii2/console/controllers/MessageController.php actionExtract
     * and modified to support array sourcePath configuration
     *
     * @param array $customConfig
     * @throws Exception
     * @throws yii\base\InvalidConfigException
     */
	protected function extractInternal(array $customConfig = [])
	{
		$config = array_merge(
			$this->getOptionValues($this->action->id),
			$customConfig,
			$this->getPassedOptionValues()
		);

		if (is_array($config['sourcePath'])) {
			foreach ($config['sourcePath'] as $key => $path) {
				$config['sourcePath'][$key] = Yii::getAlias($path);
				if (!is_dir($config['sourcePath'][$key])) {
					throw new Exception("The source path {$config['sourcePath'][$key]} is not a valid directory.");
				}
			}
		} else {
			$config['sourcePath'] = Yii::getAlias($config['sourcePath']);
		}
		$config['messagePath'] = Yii::getAlias($config['messagePath']);

		if (!isset($config['sourcePath'], $config['languages']) || (is_array($config['sourcePath']) && empty($config['sourcePath']))) {
			throw new Exception('The configuration file must specify "sourcePath" and "languages".');
		}
		if (!is_array($config['sourcePath']) && !is_dir($config['sourcePath'])) {
			throw new Exception("The source path {$config['sourcePath']} is not a valid directory.");
		}
		if (empty($config['format']) || !in_array($config['format'], ['php', 'po', 'pot', 'db'])) {
			throw new Exception('Format should be either "php", "po", "pot" or "db".');
		}
		if (in_array($config['format'], ['php', 'po', 'pot'])) {
			if (!isset($config['messagePath'])) {
				throw new Exception('The configuration file must specify "messagePath".');
			}
			if (!is_dir($config['messagePath'])) {
				throw new Exception("The message path {$config['messagePath']} is not a valid directory.");
			}
		}
		if (empty($config['languages'])) {
			throw new Exception('Languages cannot be empty.');
		}

		$sourcePath = $config['sourcePath'];

		if (is_array($sourcePath)) {
			$files = [];
			foreach ($sourcePath as $path) {
				$sourceFiles = FileHelper::findFiles(realpath($path), $config);
				$files = ArrayHelper::merge($files, $sourceFiles);
			}
		} else {
			$files = FileHelper::findFiles(realpath($sourcePath), $config);
		}

		$messages = [];
		foreach ($files as $file) {
			$messages = array_merge_recursive($messages, $this->extractMessages($file, $config['translator'], $config['ignoreCategories']));
		}

		$catalog = isset($config['catalog']) ? $config['catalog'] : 'messages';

		if (in_array($config['format'], ['php', 'po'])) {
			foreach ($config['languages'] as $language) {
				$dir = $config['messagePath'] . DIRECTORY_SEPARATOR . $language;
				if (!is_dir($dir) && !@mkdir($dir)) {
					throw new Exception("Directory '{$dir}' can not be created.");
				}
				if ($config['format'] === 'po') {
					$this->saveMessagesToPO($messages, $dir, $config['overwrite'], $config['removeUnused'], $config['sort'], $catalog, $config['markUnused']);
				} else {
					$this->saveMessagesToPHP($messages, $dir, $config['overwrite'], $config['removeUnused'], $config['sort'], $config['markUnused']);
				}
			}
		} elseif ($config['format'] === 'db') {
			/** @var Connection $db */
			$db = Instance::ensure($config['db'], Connection::class);
			$sourceMessageTable = isset($config['sourceMessageTable']) ? $config['sourceMessageTable'] : '{{%source_message}}';
			$messageTable = isset($config['messageTable']) ? $config['messageTable'] : '{{%message}}';
			$this->saveMessagesToDb(
				$messages,
				$db,
				$sourceMessageTable,
				$messageTable,
				$config['removeUnused'],
				$config['languages'],
				$config['markUnused']
			);
		} elseif ($config['format'] === 'pot') {
			$this->saveMessagesToPOT($messages, $config['messagePath'], $catalog);
		}
	}
}
