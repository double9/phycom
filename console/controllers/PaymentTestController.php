<?php

namespace phycom\console\controllers;

use phycom\common\modules\payment\Module;

use yii\console\ExitCode;
use yii;

/**
 * Class TestController
 * @package phycom\console\controllers
 */
class PaymentTestController extends BaseConsoleController
{
		public function actionMk()
		{
			/**
			 * @var Module $paymentModule
			 */
			$paymentModule = Yii::$app->getModule('payment');

			/**
			 * @var \phycom\common\modules\payment\methods\mk\Module $mk
			 */
			$mk = $paymentModule->getModule($paymentModule::METHOD_MK);


			var_dump($mk);


			echo PHP_EOL . PHP_EOL . PHP_EOL;


			echo json_encode($mk->getPublicKey()) . PHP_EOL;
			echo json_encode($mk->getPrivateKey()) . PHP_EOL;

			echo PHP_EOL . PHP_EOL . PHP_EOL;

//			echo json_encode($mk, JSON_PRETTY_PRINT) . PHP_EOL;

			echo json_encode($mk->getPaymentMethods(), JSON_PRETTY_PRINT) . PHP_EOL;
			return ExitCode::OK;
		}
}