<?php

use phycom\console\models\Migration;

class m170720140715AddressPostcode extends Migration
{

    public function safeUp()
    {
	    $this->alterColumn('{{%address}}', 'postcode', 'DROP NOT NULL'); //for drop not null
	    $this->alterColumn('{{%address}}', 'postcode', 'SET DEFAULT NULL'); //for set default null value
    }

    public function safeDown()
    {

    }

}
