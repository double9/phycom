<?php

use phycom\common\models\attributes\PriceUnitMode;
use phycom\common\models\attributes\UnitType;
use phycom\console\models\Migration;

class m180429153735PriceUnit extends Migration
{
    const TABLE_NAME = '{{%product}}';

    public function safeUp()
    {
        $this->renameColumn(self::TABLE_NAME, 'unit_type', 'price_unit');
        $this->cmd('ALTER TABLE ' . self::TABLE_NAME . ' ALTER COLUMN "price_unit" SET DEFAULT \'' . UnitType::PIECE . '\';');

        $this->addColumn(self::TABLE_NAME, 'price_unit_mode', $this->string()->defaultValue(PriceUnitMode::STRICT));

        $this->update(self::TABLE_NAME, ['price_unit_mode' => PriceUnitMode::STRICT]);
        $this->cmd('ALTER TABLE ' . self::TABLE_NAME . ' ALTER COLUMN "price_unit_mode" SET NOT NULL;');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'price_unit_mode');
        $this->renameColumn(self::TABLE_NAME, 'price_unit', 'unit_type');
    }
}
