<?php

use phycom\console\models\Migration;

class m180409054505Subscription extends Migration
{

    public function safeUp()
    {
        $this->alterColumn('{{%newsletter_subscription}}', 'first_name', 'DROP NOT NULL');
        $this->alterColumn('{{%newsletter_subscription}}', 'last_name', 'DROP NOT NULL');
    }

    public function safeDown()
    {
        echo "m180409_054505_subscription cannot be reverted.\n";
        return false;
    }

}
