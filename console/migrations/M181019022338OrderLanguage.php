<?php

use phycom\console\models\Migration;

class m181019022338OrderLanguage extends Migration
{
    const TBL = '{{%order}}';

    public function safeUp()
    {
        $this->addColumn(self::TBL, 'language_code', $this->char(2));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TBL, 'language_code');
    }
}
