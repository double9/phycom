<?php

use phycom\console\models\Migration;

class m170414151118File extends Migration
{

    public function safeUp()
    {
	    $table = '{{%file}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'mime_type' => $this->string()->notNull(),
		    'name' => $this->text(),
		    'filename' => $this->text()->notNull(),
		    'bucket' => $this->string()->notNull(),
		    'status' => $this->string()->notNull(),
		    'created_by' => $this->integer(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->addForeignKey('fk_file_user', $table, 'created_by', 'user', 'id', 'SET NULL', 'CASCADE');

	    $table = '{{%product_attachment}}';
	    $this->createTable($table, [
	    	'product_id' => $this->integer()->notNull(),
		    'file_id' => $this->integer()->notNull(),
		    'order' => $this->smallInteger()->notNull(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->addPrimaryKey('pk_product_attachment', $table, ['product_id', 'file_id']);
	    $this->addForeignKey('fk_product_attachment_product', $table, 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('fk_product_attachment_file', $table, 'file_id', 'file', 'id', 'CASCADE', 'CASCADE');

	    $table = '{{%post_attachment}}';
	    $this->createTable($table, [
		    'post_id' => $this->integer()->notNull(),
		    'file_id' => $this->integer()->notNull(),
		    'order' => $this->smallInteger()->notNull(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->addPrimaryKey('pk_post_attachment', $table, ['post_id', 'file_id']);
	    $this->addForeignKey('fk_post_attachment_post', $table, 'post_id', 'post', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('fk_post_attachment_file', $table, 'file_id', 'file', 'id', 'CASCADE', 'CASCADE');

	    $this->addColumn('product', 'image_id', $this->integer());
	    $this->addForeignKey('fk_product_image', '{{%product}}', 'image_id', 'file', 'id', 'SET NULL', 'CASCADE');

	    $this->addColumn('post', 'image_id', $this->integer());
	    $this->addForeignKey('fk_post_image', '{{%post}}', 'image_id', 'file', 'id', 'SET NULL', 'CASCADE');

	    $table = '{{%user_file}}';
	    $this->createTable($table, [
		    'user_id' => $this->integer()->notNull(),
		    'file_id' => $this->integer()->notNull(),
		    'type' => $this->string()->notNull(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->addPrimaryKey('pk_user_file', $table, ['user_id', 'file_id']);
	    $this->addForeignKey('fk_user_file_user', $table, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('fk_user_file_file', $table, 'file_id', 'file', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
	    $this->dropForeignKey('fk_user_file_user', '{{%user_file}}');
	    $this->dropForeignKey('fk_user_file_file', '{{%user_file}}');
	    $this->dropTable('{{%user_file}}');

    	$this->dropForeignKey('fk_product_image', '{{%product}}');
	    $this->dropColumn('{{%product}}', 'image_id');

    	$this->dropForeignKey('fk_post_image', '{{%post}}');
		$this->dropColumn('{{%post}}', 'image_id');

	    $this->dropForeignKey('fk_post_attachment_post', '{{%post_attachment}}');
	    $this->dropForeignKey('fk_post_attachment_file', '{{%post_attachment}}');
	    $this->dropTable('{{%post_attachment}}');

    	$this->dropForeignKey('fk_product_attachment_product', '{{%product_attachment}}');
	    $this->dropForeignKey('fk_product_attachment_file', '{{%product_attachment}}');
	    $this->dropTable('{{%product_attachment}}');

    	$this->dropForeignKey('fk_file_user', '{{%file}}');
        $this->dropTable('{{%file}}');
    }

}
