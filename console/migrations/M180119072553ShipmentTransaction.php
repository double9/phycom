<?php

use phycom\console\models\Migration;

class m180119072553ShipmentTransaction extends Migration
{
    const TABLE_NAME = '{{%shipment}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'transaction_id', $this->string());
        $this->addColumn(self::TABLE_NAME, 'original_eta', 'timestamptz');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'transaction_id');
        $this->dropColumn(self::TABLE_NAME, 'original_eta');
    }

}
