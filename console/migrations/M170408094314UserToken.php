<?php

use phycom\console\models\Migration;

class m170408094314UserToken extends Migration
{

    public function safeUp()
    {
	    $table = '{{%user_token}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'user_id' => $this->integer(),
		    'token' => $this->string()->notNull()->unique(),
		    'data' => 'jsonb',
		    'type' => $this->string()->notNull(),
		    'expires_at' => 'TIMESTAMPTZ',
		    'used' => $this->integer()->defaultValue(0),
		    'created_by' => $this->integer(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->addForeignKey('fk_user_token_user', $table, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('fk_user_token_created', $table, 'created_by', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
    	$this->dropForeignKey('fk_user_token_user', '{{%user_token}}');
	    $this->dropForeignKey('fk_user_token_created', '{{%user_token}}');
	    $this->dropTable('{{%user_token}}');
    }

}
