<?php

use phycom\console\models\Migration;

class m170408163950Shop extends Migration
{

    public function safeUp()
    {
	    $table = '{{%shop}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'name' => $this->string()->notNull()->unique(),
		    'vendor_id' => $this->integer()->notNull(),
		    'status' => $this->string()->notNull(),
		    'options' => 'jsonb',
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->addForeignKey('fk_shop_vendor', $table, 'vendor_id', 'vendor', 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
	    $this->dropForeignKey('fk_shop_vendor', '{{%shop}}');
	    $this->dropTable('{{%shop}}');
    }

}
