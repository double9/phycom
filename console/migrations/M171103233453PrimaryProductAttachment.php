<?php

use phycom\console\models\Migration;
use yii\db\Schema;

class m171103233453PrimaryProductAttachment extends Migration
{
    const TABLE_NAME = '{{%product_attachment}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'is_primary', Schema::TYPE_BOOLEAN . ' DEFAULT false');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'is_primary');
    }
}
