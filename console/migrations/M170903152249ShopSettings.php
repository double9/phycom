<?php

use phycom\console\models\Migration;

class m170903152249ShopSettings extends Migration
{
	const TABLE_NAME = '{{%shop}}';

    public function safeUp()
    {
	    $this->renameColumn(self::TABLE_NAME, 'options', 'settings');
    }

    public function safeDown()
    {
	    $this->renameColumn(self::TABLE_NAME, 'settings', 'options');
    }

}
