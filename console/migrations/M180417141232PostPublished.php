<?php

use phycom\console\models\Migration;

class m180417141232PostPublished extends Migration
{
    const TABLE_NAME = '{{%post}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'published_at', 'timestamptz');
        $this->createIndex('idx_post_published_at', self::TABLE_NAME, 'published_at');

        $this->dropForeignKey('fk_post_image', self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, 'image_id');
    }

    public function safeDown()
    {
        $this->dropIndex('idx_post_published_at', self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, 'published_at');

        $this->addColumn(self::TABLE_NAME, 'image_id', $this->integer());
        $this->addForeignKey('fk_post_image', self::TABLE_NAME, 'image_id', 'file', 'id', 'SET NULL', 'CASCADE');
    }

}
