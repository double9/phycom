<?php

use phycom\console\models\Migration;

class m170413092608Order extends Migration
{
    public function safeUp()
    {
	    $table = '{{%order}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'number' => $this->string()->unique()->notNull(),
		    'user_id' => $this->integer()->notNull(),
		    'shop_id' => $this->integer(),
		    'promotion_code' => $this->string(),
		    'status' => $this->string()->notNull(),
		    'comment' => $this->text(),
		    'delivery_time' => 'TIMESTAMPTZ',
		    'paid_at' => 'TIMESTAMPTZ',
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->addForeignKey('fk_order_shop', $table, 'shop_id', 'shop', 'id', 'SET NULL', 'CASCADE');
	    $this->addForeignKey('fk_order_user', $table, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');

	    $table = '{{%order_item}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'parent_id' => $this->integer(),
		    'order_id' => $this->integer()->notNull(),
		    'product_id' => $this->integer(),
		    'code' => $this->string()->notNull(),
		    'product_attributes' => 'jsonb',
		    'meta' => 'jsonb',
		    'quantity' => $this->integer()->defaultValue(1),
		    'price' => $this->decimal(13, 4)->notNull(),
		    'discount' => $this->decimal(5, 4)->notNull()->defaultValue(0),
		    'total' => $this->decimal(13, 4)->notNull(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->addForeignKey('fk_order_item_order', $table, 'order_id', 'order', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('fk_order_item_product', $table, 'product_id', 'product', 'id', 'SET NULL', 'CASCADE');
	    $this->addForeignKey('fk_order_item_parent', $table, 'parent_id', $table, 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
    	$this->dropForeignKey('fk_order_item_order', '{{%order_item}}');
	    $this->dropForeignKey('fk_order_item_product', '{{%order_item}}');
	    $this->dropForeignKey('fk_order_item_parent', '{{%order_item}}');
	    $this->dropTable('{{%order_item}}');

    	$this->dropForeignKey('fk_order_shop', '{{%order}}');
	    $this->dropForeignKey('fk_order_user', '{{%order}}');
	    $this->dropTable('{{%order}}');
    }

}
