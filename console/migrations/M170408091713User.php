<?php

use phycom\console\models\Migration;

class m170408091713User extends Migration
{
    public function safeUp()
    {
    	$table = '{{%user}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'username' => $this->string()->unique(),
		    'personal_code' => $this->string()->unique(),
			'birthday' => $this->date(),
			'first_name' => $this->string()->notNull(),
		    'last_name' => $this->string()->notNull(),
			'company_name' => $this->string(),
		    'display_name' => $this->string(),
		    'status' => $this->string()->notNull(),
		    'auth_key' => $this->string(32)->notNull()->unique(),
			'password_hash' => $this->string(),
			'failed_login_attempts' => $this->integer()->defaultValue(0),
			'type' => $this->string()->notNull(),
		    'settings' => 'jsonb',
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->createIndex('idx_user_status', $table, 'status');
    }

    public function safeDown()
    {
	    $this->dropTable('{{%user}}');
	    $this->dropEnum('user_type');
    }
}
