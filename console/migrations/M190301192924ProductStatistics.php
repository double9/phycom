<?php

use phycom\console\models\Migration;

class m190301192924ProductStatistics extends Migration
{
    const TABLE = '{{%product_statistics}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'total_quantity_ordered', $this->integer()->notNull()->defaultValue(0));
        $this->createIndex('idx_product_statistics_num_transactions', self::TABLE, 'num_transactions');
        $this->createIndex('idx_product_statistics_total_quantity_ordered', self::TABLE, 'total_quantity_ordered');
    }

    public function safeDown()
    {
        $this->dropIndex('idx_product_statistics_num_transactions', self::TABLE);
        $this->dropIndex('idx_product_statistics_total_quantity_ordered', self::TABLE);
        $this->dropColumn(self::TABLE, 'total_quantity_ordered');
    }

}
