<?php

use phycom\console\models\Migration;

class m180204022936DiscountRule extends Migration
{
    const FORMER_TABLE_NAME = '{{%discount_card}}';
    const TABLE_NAME = '{{%discount_rule}}';

    const FORMER_C_TABLE_NAME = '{{%discount_card_category}}';
    const C_TABLE_NAME = '{{%discount_rule_category}}';

    public function safeUp()
    {
        $this->dropIndex('idx_discount_card_code', self::FORMER_TABLE_NAME);
        $this->dropForeignKey('fk_discount_card_vendor', self::FORMER_TABLE_NAME);
        $this->dropForeignKey('fk_discount_card_shop', self::FORMER_TABLE_NAME);
        $this->dropForeignKey('fk_discount_card_user', self::FORMER_TABLE_NAME);

        $this->renameTable(self::FORMER_TABLE_NAME, self::TABLE_NAME);
        $this->cmd('ALTER INDEX discount_card_pkey RENAME TO discount_rule_pkey');

        $this->cmd('CREATE UNIQUE INDEX idx_discount_rule_code ON '.self::TABLE_NAME.' (code) WHERE code IS NOT NULL');
        $this->addForeignKey('fk_discount_rule_vendor', self::TABLE_NAME, 'vendor_id', 'vendor', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_discount_rule_shop', self::TABLE_NAME, 'shop_id', 'shop', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_discount_rule_user', self::TABLE_NAME, 'created_by', 'user', 'id', 'SET NULL', 'CASCADE');

        $this->addColumn(self::TABLE_NAME, 'discount_amount', $this->decimal(13, 4));
        $this->addColumn(self::TABLE_NAME, 'rule', $this->string());
        $this->addColumn(self::TABLE_NAME, 'starts_at', 'TIMESTAMPTZ');
        $this->addColumn(self::TABLE_NAME, 'min_purchase', $this->decimal(13, 4));
        $this->addColumn(self::TABLE_NAME, 'max_usage', $this->integer());


        $this->dropForeignKey('fk_discount_card_category_discount_card', self::FORMER_C_TABLE_NAME);
        $this->dropForeignKey('fk_discount_card_category_product_category', self::FORMER_C_TABLE_NAME);

        $this->renameTable(self::FORMER_C_TABLE_NAME, self::C_TABLE_NAME);
        $this->cmd('ALTER INDEX discount_card_category_pkey RENAME TO discount_rule_category_pkey');
        $this->renameColumn(self::C_TABLE_NAME, 'discount_card_id', 'discount_rule_id');

        $this->addForeignKey('fk_discount_rule_category_discount_rule', self::C_TABLE_NAME, 'discount_rule_id', self::TABLE_NAME, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_discount_rule_category_product_category', self::C_TABLE_NAME, 'product_category_id', 'product_category', 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'discount_amount');
        $this->dropColumn(self::TABLE_NAME, 'rule');
        $this->dropColumn(self::TABLE_NAME, 'starts_at');
        $this->dropColumn(self::TABLE_NAME, 'min_purchase');
        $this->dropColumn(self::TABLE_NAME, 'max_usage');

        $this->dropIndex('idx_discount_rule_code', self::TABLE_NAME);
        $this->dropForeignKey('fk_discount_rule_vendor', self::TABLE_NAME);
        $this->dropForeignKey('fk_discount_rule_shop', self::TABLE_NAME);
        $this->dropForeignKey('fk_discount_rule_user', self::TABLE_NAME);

        $this->renameTable(self::TABLE_NAME, self::FORMER_TABLE_NAME);
        $this->cmd('ALTER INDEX discount_rule_pkey RENAME TO discount_card_pkey');

        $this->createIndex('idx_discount_card_code', self::FORMER_TABLE_NAME, 'code');
        $this->addForeignKey('fk_discount_card_vendor', self::FORMER_TABLE_NAME, 'vendor_id', 'vendor', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_discount_card_shop', self::FORMER_TABLE_NAME, 'shop_id', 'shop', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_discount_card_user', self::FORMER_TABLE_NAME, 'created_by', 'user', 'id', 'SET NULL', 'CASCADE');


        $this->dropForeignKey('fk_discount_rule_category_discount_rule', self::C_TABLE_NAME);
        $this->dropForeignKey('fk_discount_rule_category_product_category', self::C_TABLE_NAME);

        $this->renameTable(self::C_TABLE_NAME, self::FORMER_C_TABLE_NAME);
        $this->cmd('ALTER INDEX discount_rule_category_pkey RENAME TO discount_card_category_pkey');
        $this->renameColumn(self::FORMER_C_TABLE_NAME, 'discount_rule_id', 'discount_card_id');

        $this->addForeignKey('fk_discount_card_category_discount_card', self::FORMER_C_TABLE_NAME, 'discount_card_id', self::FORMER_TABLE_NAME, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_discount_card_category_product_category', self::FORMER_C_TABLE_NAME, 'product_category_id', 'product_category', 'id', 'SET NULL', 'CASCADE');


    }

}
