<?php

use phycom\console\models\Migration;

class m180920030156OrderUnregistered extends Migration
{

    const TABLE_NAME = '{{%order}}';
    const TABLE_MESSAGE = '{{%message}}';

    public function safeUp()
    {
        $this->alterColumn(self::TABLE_NAME, 'user_id', 'DROP NOT NULL');

        $this->addColumn(self::TABLE_NAME, 'first_name', $this->string());
        $this->addColumn(self::TABLE_NAME, 'last_name', $this->string());
        $this->addColumn(self::TABLE_NAME, 'company_name', $this->string());
        $this->addColumn(self::TABLE_NAME, 'email', $this->string());

        $this->addColumn(self::TABLE_MESSAGE, 'to', $this->string());
        $this->addColumn(self::TABLE_MESSAGE, 'to_name', $this->string());
        $this->alterColumn(self::TABLE_MESSAGE, 'user_to', 'DROP NOT NULL');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'first_name');
        $this->dropColumn(self::TABLE_NAME, 'last_name');
        $this->dropColumn(self::TABLE_NAME, 'company_name');
        $this->dropColumn(self::TABLE_NAME, 'email');

        $this->dropColumn(self::TABLE_MESSAGE, 'to');
        $this->dropColumn(self::TABLE_MESSAGE, 'to_name');
    }

}
