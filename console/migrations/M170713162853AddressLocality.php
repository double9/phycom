<?php

use phycom\console\models\Migration;

class m170713162853AddressLocality extends Migration
{

    public function safeUp()
    {
		$this->addColumn('{{%address}}', 'locality', \yii\db\Schema::TYPE_STRING);
    }

    public function safeDown()
    {
       $this->dropColumn('{{%address}}', 'locality');
    }

}
