<?php

use phycom\console\models\Migration;
use yii\db\Schema;

class m171028204727CarrierDurationTerms extends Migration
{
    const TABLE_NAME = '{{%shipment}}';

    public function safeUp()
    {
        $this->renameColumn(self::TABLE_NAME, 'comment', 'duration_terms');
    }

    public function safeDown()
    {
        $this->renameColumn(self::TABLE_NAME, 'duration_terms', 'comment');
    }

}
