<?php

use phycom\console\models\Migration;

class m170610125728NewsletterSubscription extends Migration
{

    public function safeUp()
    {
	    $table = '{{%newsletter_subscription}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'first_name' => $this->string()->notNull(),
		    'last_name' => $this->string()->notNull(),
		    'email' => $this->string()->unique()->notNull(),
		    'emails_sent' => $this->integer()->notNull()->defaultValue(0),
		    'status' => $this->string()->notNull(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);
	    $this->createIndex('idx_newsletter_subscription_email', $table, 'email');
    }

    public function safeDown()
    {
    	$this->dropIndex('idx_newsletter_subscription_email', '{{%newsletter_subscription}}');
	    $this->dropTable('{{%newsletter_subscription}}');
    }

}
