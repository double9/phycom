<?php

use phycom\console\models\Migration;

/**
 * Rename post.key to post.identifier and make it not unique
 * Class m200301_093808_post_identifier
 */
class m200301093808PostIdentifier extends Migration
{
    const TABLE_NAME = '{{%post}}';

    public function safeUp()
    {
        $this->dropIndex('idx_post_key', self::TABLE_NAME);
        $this->cmd('ALTER TABLE ' . self::TABLE_NAME . ' DROP CONSTRAINT IF EXISTS post_url_key_key');
        $this->renameColumn(self::TABLE_NAME, 'key', 'identifier');
        $this->createIndex('idx_post_identifier', self::TABLE_NAME, 'identifier');
    }

    public function safeDown()
    {
        $this->dropIndex('idx_post_identifier', self::TABLE_NAME);
        $this->renameColumn(self::TABLE_NAME, 'identifier', 'key');
        $this->createIndex('idx_post_key', self::TABLE_NAME, 'key', true);
        $this->cmd('ALTER TABLE ' . self::TABLE_NAME . ' ADD CONSTRAINT post_url_key_key UNIQUE (key)');
    }

}
