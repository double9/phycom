<?php

use phycom\console\models\Migration;

class m180528211140Postcode extends Migration
{
    const TABLE_NAME = '{{%postcode}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'country' => $this->char(2)->notNull(),
            'postcode' => $this->string()->notNull(),
            'province' => $this->string()->notNull(),
            'locality' => $this->string(),
            'city' => $this->string(),
            'district' => $this->string(),
            'street' => $this->string(),
            'created_at' => 'TIMESTAMPTZ NOT NULL',
            'updated_at' => 'TIMESTAMPTZ NOT NULL'
        ]);

        $this->createIndex('idx_postcode_postcode', self::TABLE_NAME, 'postcode');
        $this->createIndex('idx_postcode_province', self::TABLE_NAME, 'province');
        $this->createIndex('idx_postcode_city', self::TABLE_NAME, 'city');
        $this->createIndex('idx_postcode_street', self::TABLE_NAME, 'street');
    }

    public function safeDown()
    {
        $this->dropIndex('idx_postcode_postcode', self::TABLE_NAME);
        $this->dropIndex('idx_postcode_province', self::TABLE_NAME);
        $this->dropIndex('idx_postcode_city', self::TABLE_NAME);
        $this->dropIndex('idx_postcode_street', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }

}
