<?php

use phycom\console\models\Migration;

class m180523180528PostTranslationMeta extends Migration
{
    const TABLE_NAME = '{{%post_translation}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'meta', 'JSONB');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'meta');
    }

}
