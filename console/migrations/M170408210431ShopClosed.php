<?php

use phycom\console\models\Migration;

class m170408210431ShopClosed extends Migration
{

    public function safeUp()
    {
	    $table = '{{%shop_closed}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'shop_id' => $this->integer()->notNull(),
		    'date' => $this->date()->notNull(),
		    'comment' => $this->text(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->addForeignKey('fk_shop_closed_shop', $table, 'shop_id', 'shop', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
	    $this->dropForeignKey('fk_shop_closed_shop', '{{%shop_closed}}');
	    $this->dropTable('{{%shop_closed}}');
    }

}
