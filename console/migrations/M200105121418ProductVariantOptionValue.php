<?php

use phycom\console\models\Migration;

class m200105121418ProductVariantOptionValue extends Migration
{
    const TABLE = '{{%product_variant_option}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'default_value', 'JSONB');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'default_value');
    }

}
