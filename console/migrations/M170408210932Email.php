<?php

use phycom\console\models\Migration;

class m170408210932Email extends Migration
{

    public function safeUp()
    {
	    $table = '{{%email}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'user_id' => $this->integer(),
		    'vendor_id' => $this->integer(),
		    'shop_id' => $this->integer(),
		    'email' => $this->string()->notNull(),
		    'status' => $this->string()->notNull(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->addForeignKey('fk_email_user', $table, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('fk_email_vendor', $table, 'vendor_id', 'vendor', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('fk_email_shop', $table, 'shop_id', 'shop', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
    	$this->dropForeignKey('fk_email_user', '{{%email}}');
	    $this->dropForeignKey('fk_email_vendor', '{{%email}}');
	    $this->dropForeignKey('fk_email_shop', '{{%email}}');
	    $this->dropTable('{{%email}}');
    }

}
