<?php

use phycom\console\models\Migration;

class m191107202031TranslationOutline extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%product_translation}}', 'outline', $this->text());
        $this->addColumn('{{%post_translation}}', 'outline', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%product_translation}}', 'outline');
        $this->dropColumn('{{%post_translation}}', 'outline');
    }

}
