<?php

use phycom\console\models\Migration;

class m171105095044ProductAttributeOption extends Migration
{
    const TABLE_NAME = '{{%product_attribute_option}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'product_attribute_id' => $this->integer()->notNull(),
            'key' => $this->string()->notNull(),
            'sku' => $this->string()->unique(),
            'price' => $this->decimal(13, 4),
            'stock' => $this->decimal(13, 4),
            'status' => $this->string(),
            'created_at' => 'TIMESTAMPTZ NOT NULL',
            'updated_at' => 'TIMESTAMPTZ NOT NULL',
        ]);
        $this->addForeignKey('fk_product_attribute_option_attribute', self::TABLE_NAME, 'product_attribute_id', 'product_attribute', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('idx_product_attribute_option', self::TABLE_NAME, ['product_attribute_id', 'key'], true);
    }

    public function safeDown()
    {
        $this->dropIndex('idx_product_attribute_option', self::TABLE_NAME);
        $this->dropForeignKey('fk_product_attribute_option_attribute', self::TABLE_NAME);
        $this->dropTable(self::TABLE_NAME);
    }
}
