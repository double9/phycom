<?php

use phycom\console\models\Migration;

class m170608224527ProductTag extends Migration
{
	const TABLE_NAME = '{{%product_tag}}';
	public function safeUp()
	{
		$this->createTable(self::TABLE_NAME, [
			'id' => $this->primaryKey(),
			'product_id' => $this->integer()->notNull(),
			'value' => $this->string()->notNull(),
			'created_by' => $this->integer()->notNull(),
			'created_at' => 'TIMESTAMPTZ NOT NULL',
		]);

		$this->addForeignKey('fk_product_tag_user', self::TABLE_NAME, 'created_by', 'user', 'id', 'SET NULL', 'CASCADE');
		$this->addForeignKey('fk_product_tag_product', self::TABLE_NAME, 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
		$this->dropForeignKey('fk_product_tag_product', self::TABLE_NAME);
		$this->dropForeignKey('fk_product_tag_user', self::TABLE_NAME);
		$this->dropTable(self::TABLE_NAME);
	}

}
