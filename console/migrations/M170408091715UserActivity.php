<?php

use phycom\console\models\Migration;

class m170408091715UserActivity extends Migration
{
    public function safeUp()
    {
	    $table = '{{%user_activity}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'user_id' => $this->integer()->notNull(),
		    'action' => $this->text()->notNull(),
		    'ip' => $this->string()->notNull(),
		    'user_agent' => $this->text(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->addForeignKey('fk_user_activity_user', $table, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
	    $this->dropForeignKey('fk_user_activity_user', '{{%user_activity}}');
	    $this->dropTable('{{%user_activity}}');
    }
}
