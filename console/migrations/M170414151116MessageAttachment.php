<?php

use phycom\console\models\Migration;

class m170414151116MessageAttachment extends Migration
{

    public function safeUp()
    {
	    $table = '{{%message_attachment}}';
	    $this->createTable($table, [
	    	'id' => $this->primaryKey(),
		    'message_id' => $this->integer()->notNull(),
		    'mime_type' => $this->string()->notNull(),
		    'file_name' => $this->text(),
		    'file_path' => $this->text()->notNull(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->addForeignKey('fk_message_attachment_message', $table, 'message_id', 'message', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
	    $this->dropForeignKey('fk_message_attachment_message', '{{%message_attachment}}');
	    $this->dropTable('{{%message_attachment}}');
    }

}
