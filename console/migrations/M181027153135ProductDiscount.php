<?php

use phycom\console\models\Migration;

class m181027153135ProductDiscount extends Migration
{
    const TBL = '{{%product}}';

    public function safeUp()
    {
        $this->addColumn(self::TBL, 'discount', $this->boolean());
    }

    public function safeDown()
    {
        $this->dropColumn(self::TBL, 'discount');
    }

}
