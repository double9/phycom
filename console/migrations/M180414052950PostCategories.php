<?php

use phycom\console\models\Migration;

class m180414052950PostCategories extends Migration
{

    public function safeUp()
    {
        $table = '{{%post_category}}';
        $this->createTable($table, [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer(),
            'status' => $this->string()->notNull(),
            'order' => $this->smallInteger()->notNull(),
            'featured' => $this->boolean()->notNull()->defaultValue(false),
            'created_by' => $this->integer(),
            'created_at' => 'TIMESTAMPTZ NOT NULL',
            'updated_at' => 'TIMESTAMPTZ NOT NULL',
        ]);

        $this->addForeignKey('fk_post_category_user', $table, 'created_by', 'user', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_post_category_parent', $table, 'parent_id', $table, 'id', 'SET NULL', 'CASCADE');


        $table = '{{%post_category_translation}}';
        $this->createTable($table, [
            'id' => $this->primaryKey(),
            'post_category_id' => $this->integer()->notNull(),
            'language' => $this->char(2)->notNull(),
            'url_key' => $this->string()->unique()->notNull(),
            'title' => $this->text(),
            'description' => $this->text(),
            'meta_title' => $this->text(),
            'meta_keywords' => $this->text(),
            'meta_description' => $this->text(),
            'status' => $this->string()->notNull(),
            'created_at' => 'TIMESTAMPTZ NOT NULL',
            'updated_at' => 'TIMESTAMPTZ NOT NULL',
        ]);

        $this->addForeignKey(
            'fk_post_category_translation_language',
            $table,
            'language',
            'language',
            'code',
            'NO ACTION',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_post_category_translation_post_category',
            $table,
            'post_category_id',
            'post_category',
            'id',
            'SET NULL',
            'CASCADE'
        );


        $table = '{{%post_in_post_category}}';
        $this->createTable($table, [
            'post_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
            'created_by' => $this->integer(),
            'created_at' => 'TIMESTAMPTZ NOT NULL',
        ]);
        $this->addPrimaryKey('pk_post_in_post_category', $table, ['post_id', 'category_id']);
        $this->addForeignKey('fk_post_in_post_category_user', $table, 'created_by', 'user', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_post_in_post_category_post', $table, 'post_id', 'post', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_post_in_post_category_category', $table, 'category_id', 'post_category', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_post_in_post_category_user', '{{%post_in_post_category}}');
        $this->dropForeignKey('fk_post_in_post_category_post', '{{%post_in_post_category}}');
        $this->dropForeignKey('fk_post_in_post_category_category', '{{%post_in_post_category}}');
        $this->dropTable('{{%post_in_post_category}}');

        $this->dropForeignKey('fk_post_category_translation_language', '{{%post_category_translation}}');
        $this->dropForeignKey('fk_post_category_translation_post_category', '{{%post_category_translation}}');
        $this->dropTable('{{%post_category_translation}}');

        $this->dropForeignKey('fk_post_category_parent', '{{%post_category}}');
        $this->dropForeignKey('fk_post_category_user', '{{%post_category}}');
        $this->dropTable('{{%post_category}}');
    }

}
