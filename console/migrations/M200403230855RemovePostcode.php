<?php

namespace phycom\console\migrations;

use phycom\console\models\Migration;

class M200403230855RemovePostcode extends Migration
{
    public function safeUp()
    {
        $this->dropIndex('idx_postcode_keyword', 'postcode');
        $this->dropIndex('idx_postcode_house_number', 'postcode');
        $this->dropIndex('idx_postcode_postcode', 'postcode');
        $this->dropIndex('idx_postcode_province', 'postcode');
        $this->dropIndex('idx_postcode_city', 'postcode');
        $this->dropIndex('idx_postcode_street', 'postcode');

        $this->dropTable('postcode');
    }

    public function safeDown()
    {
        $this->createTable('postcode', [
            'id' => $this->primaryKey(),
            'country' => $this->char(2)->notNull(),
            'postcode' => $this->string()->notNull(),
            'province' => $this->string()->notNull(),
            'locality' => $this->string(),
            'city' => $this->string(),
            'district' => $this->string(),
            'street' => $this->string(),
            'keyword' => $this->string(),
            'house_number' => $this->integer(),
            'created_at' => 'TIMESTAMPTZ NOT NULL',
            'updated_at' => 'TIMESTAMPTZ NOT NULL'
        ]);

        $this->createIndex('idx_postcode_postcode', 'postcode', 'postcode');
        $this->createIndex('idx_postcode_province', 'postcode', 'province');
        $this->createIndex('idx_postcode_city', 'postcode', 'city');
        $this->createIndex('idx_postcode_street', 'postcode', 'street');
        $this->createIndex('idx_postcode_keyword', 'postcode', 'keyword');
        $this->createIndex('idx_postcode_house_number', 'postcode', 'house_number');
    }
}
