<?php

use phycom\console\models\Migration;

class m170408223546AuditTrail extends Migration
{

    public function safeUp()
    {
	    $table = '{{%audit_trail}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'old_value' => 'json',
		    'new_value' => 'json',
		    'action' => $this->string()->notNull(),
		    'model' => $this->string()->notNull(),
		    'model_id' => $this->string()->notNull(),
		    'user_id' => $this->string(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->createIndex( 'idx_audit_trail_user_id', $table, 'user_id');
	    $this->createIndex( 'idx_audit_trail_model_id', $table, 'model_id');
	    $this->createIndex( 'idx_audit_trail_model', $table, 'model');
	    $this->createIndex( 'idx_audit_trail_action', $table, 'action');
    }

    public function safeDown()
    {
	    $this->dropTable('{{%audit_trail}}');
    }

}
