<?php

use phycom\console\models\Migration;

class m170414083512Setting extends Migration
{

    public function safeUp()
    {
	    $table = '{{%setting}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'key' => $this->string()->notNull()->unique(),
		    'value' => $this->string()->notNull(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);
    }

    public function safeDown()
    {
	    $this->dropTable('{{%setting}}');
    }
}
