<?php

use phycom\console\models\Migration;

class m170408091714UserTag extends Migration
{

    public function safeUp()
    {
	    $table = '{{%user_tag}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'user_id' => $this->integer()->notNull(),
		    'value' => $this->string()->notNull(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
	    ]);
	    $this->addForeignKey('fk_user_tag_user', $table, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
	    $this->dropForeignKey('fk_user_tag_user', '{{%user_tag}}');
	    $this->dropTable('{{%user_tag}}');
    }
}
