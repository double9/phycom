<?php

use phycom\console\models\Migration;
use yii\db\Schema;

class m170822212616PaymentTransactionId extends Migration
{
	const TABLE_NAME = '{{%payment}}';

    public function safeUp()
    {
	    $this->addColumn(self::TABLE_NAME, 'transaction_id', Schema::TYPE_STRING);
    }

    public function safeDown()
    {
	    $this->dropColumn(self::TABLE_NAME, 'transaction_id');
    }

}
