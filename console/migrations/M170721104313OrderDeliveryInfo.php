<?php

use phycom\console\models\Migration;
use yii\db\Schema;

class m170721104313OrderDeliveryInfo extends Migration
{
	const TABLE_NAME = '{{%order}}';

    public function safeUp()
    {
	    $this->addColumn(self::TABLE_NAME, 'delivery_service', Schema::TYPE_STRING);
		$this->addColumn(self::TABLE_NAME, 'delivery_area', Schema::TYPE_STRING);
	    $this->addColumn(self::TABLE_NAME, 'delivery_address', 'jsonb');
    }

    public function safeDown()
    {
	    $this->dropColumn(self::TABLE_NAME, 'delivery_service');
        $this->dropColumn(self::TABLE_NAME, 'delivery_area');
	    $this->dropColumn(self::TABLE_NAME, 'delivery_address');
    }

}
