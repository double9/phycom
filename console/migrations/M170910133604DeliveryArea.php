<?php

use phycom\console\models\Migration;

class m170910133604DeliveryArea extends Migration
{
	const TABLE_NAME = '{{%delivery_area}}';
    public function safeUp()
    {
	    $this->createTable(self::TABLE_NAME, [
		    'id' => $this->primaryKey(),
		    'code' => $this->string()->notNull(),
		    'courier' => $this->string()->notNull(),
		    'service' => $this->string(),
		    'area_code' => $this->string()->notNull(),
		    'area' => 'jsonb NOT NULL',
		    'status' => $this->string()->notNull(),
			'price' => $this->decimal(13, 4),
		    'delivery_time' => $this->string(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }

}
