<?php

use phycom\console\models\Migration;
use yii\db\Schema;

class m170831142004UserReferenceNumber extends Migration
{
	const TABLE_NAME = '{{%user}}';

    public function safeUp()
    {
	    $this->addColumn(self::TABLE_NAME, 'reference_number', Schema::TYPE_STRING);
    }

    public function safeDown()
    {
	    $this->dropColumn(self::TABLE_NAME, 'reference_number');
    }

}
