<?php

use phycom\console\models\Migration;

class m180315164432FeaturedCategory extends Migration
{
    const TABLE_NAME = '{{%product_category}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'featured', \yii\db\Schema::TYPE_BOOLEAN);
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'featured');
    }
}
