<?php

use phycom\console\models\Migration;
use phycom\common\modules\delivery\Module as DeliveryModule;

class m170924074017CourierName extends Migration
{
    public function safeUp()
    {
		$this->update('delivery_area', ['courier' => DeliveryModule::METHOD_SELF_PICKUP], 'courier = \'P\'');
	    $this->update('order', ['delivery_service' => DeliveryModule::METHOD_SELF_PICKUP], 'delivery_service = \'P\'');
	    $this->update('shipment', ['courier_name' => DeliveryModule::METHOD_SELF_PICKUP], 'courier_name = \'P\'');

	    $this->update('delivery_area', ['courier' => DeliveryModule::METHOD_COURIER], 'courier = \'C\'');
	    $this->update('order', ['delivery_service' => DeliveryModule::METHOD_COURIER], 'delivery_service = \'C\'');
	    $this->update('shipment', ['courier_name' => DeliveryModule::METHOD_COURIER], 'courier_name = \'C\'');
    }

    public function safeDown()
    {
	    $this->update('delivery_area', ['courier' => 'P'], 'courier = :courier', ['courier' => DeliveryModule::METHOD_SELF_PICKUP]);
	    $this->update('order', ['delivery_service' => 'P'], 'delivery_service = :courier', ['courier' => DeliveryModule::METHOD_SELF_PICKUP]);
	    $this->update('shipment', ['courier_name' => 'P'], 'courier_name = :courier', ['courier' => DeliveryModule::METHOD_SELF_PICKUP]);

	    $this->update('delivery_area', ['courier' => 'C'], 'courier = :courier', ['courier' => DeliveryModule::METHOD_COURIER]);
	    $this->update('order', ['delivery_service' => 'C'], 'delivery_service = :courier', ['courier' => DeliveryModule::METHOD_COURIER]);
	    $this->update('shipment', ['courier_name' => 'C'], 'courier_name = :courier', ['courier' => DeliveryModule::METHOD_COURIER]);
    }
}
