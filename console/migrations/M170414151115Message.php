<?php

use phycom\console\models\Migration;

class m170414151115Message extends Migration
{

    public function safeUp()
    {
	    $table = '{{%message}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'user_from' => $this->integer()->notNull(),
		    'user_to' => $this->integer()->notNull(),
		    'subject' => $this->string(),
		    'content' => $this->text()->notNull(),
		    'status' => $this->string()->notNull(),
		    'type' => $this->string()->notNull(),
		    'priority' => $this->integer()->notNull(),
		    'parent_id' => $this->integer(),
		    'dispatched_at' => 'TIMESTAMPTZ',
		    'delivery_time' => 'TIMESTAMPTZ',
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->addForeignKey('fk_message_from', $table, 'user_from', 'user', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('fk_message_to', $table, 'user_to', 'user', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('fk_message_parent', $table, 'parent_id', $table, 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
		$this->dropForeignKey('fk_message_from', '{{%message}}');
	    $this->dropForeignKey('fk_message_to', '{{%message}}');
	    $this->dropForeignKey('fk_message_parent', '{{%message}}');
		$this->dropTable('{{%message}}');
    }

}
