<?php

use phycom\console\models\Migration;

class m170409070929ProductPrice extends Migration
{
	public function safeUp()
    {
	    $table = '{{%product_price}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'product_id' => $this->integer()->notNull(),
		    'sku' => $this->string(),
		    'num_units' => $this->decimal(6,2)->notNull()->defaultValue(1),
		    'unit_type' => $this->string(),
		    'price' => $this->decimal(13,4)->notNull(),
		    'discount_price' => $this->decimal(13,4),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ]);

	    $this->addForeignKey('fk_product_price', 'product', 'price_id', $table, 'id', 'SET NULL', 'CASCADE');
	    $this->addForeignKey('fk_product_price_product', $table, 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');

	    $table = '{{%product_price_attribute}}';
	    $this->createTable($table, [
	    	'product_price_id' => $this->integer()->notNull(),
		    'attribute' => $this->string()->notNull(),
		    'option' => $this->string(),
		    'price' => $this->decimal(13,4),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
        ]);
	    $this->addForeignKey('fk_product_price_attribute_product_price', $table, 'product_price_id', 'product_price', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('idx_product_price_attribute', $table, ['product_price_id', 'attribute', 'option'], true);
    }

    public function safeDown()
    {
    	$this->dropForeignKey('fk_product_price_attribute_product_price', '{{%product_price_attribute}}');
		$this->dropIndex('idx_product_price_attribute', '{{%product_price_attribute}}');
		$this->dropTable('{{%product_price_attribute}}');

    	$this->dropForeignKey('fk_product_price_product', '{{%product_price}}');
	    $this->dropForeignKey('fk_product_price', '{{%product}}');
    	$this->dropTable('{{%product_price}}');
    }

}
