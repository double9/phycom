<?php

use phycom\console\models\Migration;

class m170408091716Rbac extends Migration
{
    public function safeUp()
    {
	    $this->createTables();
	    Yii::$app->modelFactory->getRbacData(['db' => $this->db])->reload();
    }

    public function safeDown()
    {
	    $this->dropTable(Yii::$app->authManager->assignmentTable);
	    $this->dropTable(Yii::$app->authManager->itemChildTable);
	    $this->dropTable(Yii::$app->authManager->itemTable);
	    $this->dropTable(Yii::$app->authManager->ruleTable);
    }

	protected function createTables()
	{
		$this->createTable(Yii::$app->authManager->ruleTable, [
			'id' => $this->primaryKey(),
			'name' => $this->string(128)->notNull(),
			'data' => $this->text(),
			'created_at' => 'TIMESTAMPTZ NOT NULL',
			'updated_at' => 'TIMESTAMPTZ NOT NULL'
		]);
		$this->createIndex('idx_auth_rule_name', Yii::$app->authManager->ruleTable, 'name', true);

		$this->createTable(Yii::$app->authManager->itemTable, [
			'id' => $this->primaryKey(),
			'name' => $this->string(128)->notNull(),
			'type' => $this->integer()->notNull(),
			'description' => $this->text(),
			'rule_name' => $this->string(128),
			'data' => $this->text(),
			'created_at' => 'TIMESTAMPTZ NOT NULL',
			'updated_at' => 'TIMESTAMPTZ NOT NULL',
			'FOREIGN KEY (rule_name) REFERENCES ' . Yii::$app->authManager->ruleTable . ' (name) ON DELETE SET NULL ON UPDATE CASCADE',
		]);
		$this->createIndex('idx_auth_item_type', Yii::$app->authManager->itemTable, 'type');
		$this->createIndex('idx_auth_item_name', Yii::$app->authManager->itemTable, 'name', true);


		$this->createTable(Yii::$app->authManager->itemChildTable, [
			'id' => $this->primaryKey(),
			'parent' => $this->string(128)->notNull(),
			'child' => $this->string(128)->notNull(),
			'FOREIGN KEY (parent) REFERENCES ' . Yii::$app->authManager->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE',
			'FOREIGN KEY (child) REFERENCES ' . Yii::$app->authManager->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE',
		]);
		$this->createIndex('idx_auth_item_hierarchy_relation', Yii::$app->authManager->itemChildTable, ['parent','child'], true);

		$this->createTable(Yii::$app->authManager->assignmentTable, [
			'id' => $this->primaryKey(),
			'item_name' => $this->string(128)->notNull(),
			'user_id' => $this->integer()->notNull(),
			'created_at' => 'TIMESTAMPTZ NOT NULL',
			'FOREIGN KEY (item_name) REFERENCES ' . Yii::$app->authManager->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE',
			'FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE ON UPDATE CASCADE',
		]);
		$this->createIndex('idx_user_auth_item_assignment', Yii::$app->authManager->assignmentTable, ['item_name','user_id'], true);

	}
}
