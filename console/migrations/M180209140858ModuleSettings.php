<?php

use phycom\console\models\Migration;

/**
 * Class m180209_140858_module_settings
 */
class m180209140858ModuleSettings extends Migration
{
    const TABLE_NAME = '{{%module_setting}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'module' => $this->string()->notNull(),
            'submodule' => $this->string(),
            'key' => $this->string()->notNull(),
            'value' => $this->text()->notNull(),
            'created_at' => 'TIMESTAMPTZ NOT NULL',
            'updated_at' => 'TIMESTAMPTZ NOT NULL',
        ], null);
        $this->createIndex('idx_module_setting_key', self::TABLE_NAME, ['module', 'submodule', 'key'], true);
    }

    public function safeDown()
    {
        $this->dropIndex('idx_module_setting_key', self::TABLE_NAME);
        $this->dropTable(self::TABLE_NAME);
    }

}
