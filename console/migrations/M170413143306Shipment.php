<?php

use phycom\console\models\Migration;

class m170413143306Shipment extends Migration
{

    public function safeUp()
    {
	    $table = '{{%shipment}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'order_id' => $this->integer()->notNull(),
		    'tracking_number' => $this->string(),
		    'courier_name' => $this->string(),
		    'courier_service' => $this->string(),
		    'courier_code' => $this->string(),
		    'courier_area' => $this->string(),
		    'courier_delivery_time' => $this->string(),
		    'recipient_name' => $this->string()->notNull(),
		    'recipient_phone' => $this->string(),
		    'destination_address' => 'jsonb NOT NULL',
		    'status' => $this->string()->notNull(),
		    'type' => $this->string()->notNull(),
		    'comment' => $this->text(),
		    'shipped_at' => 'TIMESTAMPTZ',
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->addForeignKey('fk_shipment_order', $table, 'order_id', 'order', 'id', 'CASCADE', 'CASCADE');

	    $table = '{{%shipment_item}}';
	    $this->createTable($table, [
		    'shipment_id' => $this->integer()->notNull(),
		    'order_item_id' => $this->integer()->notNull()
	    ], null);

	    $this->addPrimaryKey('pk_shipment_item', $table, ['shipment_id', 'order_item_id']);
	    $this->addForeignKey('fk_shipment_item_shipment', $table, 'shipment_id', 'shipment', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('fk_shipment_item_order_item', $table, 'order_item_id', 'order_item', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
    	$this->dropForeignKey('fk_shipment_item_shipment', '{{%shipment_item}}');
	    $this->dropForeignKey('fk_shipment_item_order_item', '{{%shipment_item}}');
	    $this->dropTable('{{%shipment_item}}');

	    $this->dropForeignKey('fk_shipment_order', '{{%shipment}}');
	    $this->dropTable('{{%shipment}}');
    }

}
