<?php

use phycom\console\models\Migration;

class m170610095444DiscountCardCategory extends Migration
{

    public function safeUp()
    {
	    $table = '{{%discount_card_category}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'discount_card_id' => $this->integer()->notNull(),
		    'product_category_id' => $this->integer()->notNull(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
	    ]);
	    $this->addForeignKey('fk_discount_card_category_discount_card', $table, 'discount_card_id', 'discount_card', 'id', 'SET NULL', 'CASCADE');
	    $this->addForeignKey('fk_discount_card_category_product_category', $table, 'product_category_id', 'product_category', 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
	    $this->dropForeignKey('fk_discount_card_category_product_category', '{{%discount_card_category}}');
	    $this->dropForeignKey('fk_discount_card_category_discount_card', '{{%discount_card_category}}');
	    $this->dropTable('{{%discount_card_category}}');
    }

}
