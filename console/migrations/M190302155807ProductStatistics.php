<?php

use phycom\console\models\Migration;

class m190302155807ProductStatistics extends Migration
{
    const TABLE = '{{%product_statistics}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'revenue', $this->decimal(13, 4)->notNull()->defaultValue(0.0000));
        $this->createIndex('idx_product_statistics_revenue', self::TABLE, 'revenue');

        $this->moveColumns(self::TABLE, [
            'total_quantity_ordered' => $this->integer()->notNull()->defaultValue(0),
            'revenue' => $this->decimal(13, 4)->notNull()->defaultValue(0.0000),
            'created_at' => 'TIMESTAMPTZ',
            'updated_at' => 'TIMESTAMPTZ'
        ]);
    }

    public function safeDown()
    {
        $this->dropIndex('idx_product_statistics_revenue', self::TABLE);
        $this->dropColumn(self::TABLE, 'revenue');
    }
}
