<?php

use phycom\console\models\Migration;

class m170413092609Invoice extends Migration
{

    public function safeUp()
    {
	    $table = '{{%invoice}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'number' => $this->string()->unique(),
		    'order_id' => $this->integer()->notNull(),
		    'status' => $this->string()->notNull(),
		    'file' => $this->text()->notNull(),
		    'recipient' => $this->string()->notNull(),
		    'recipient_type' => $this->string()->notNull(),
		    'reg_no' => $this->string(),
		    'address' => 'jsonb',
		    'created_at' => 'TIMESTAMPTZ NOT NULL'
	    ], null);

	    $this->addForeignKey('fk_invoice_order', $table, 'order_id', 'order', 'id', 'CASCADE', 'CASCADE');

	    $table = '{{%invoice_item}}';
	    $this->createTable($table, [
		    'invoice_id' => $this->integer()->notNull(),
		    'order_item_id' => $this->integer()->notNull(),
		    'quantity' => $this->integer()
	    ], null);

	    $this->addPrimaryKey('pk_invoice_item', $table, ['invoice_id', 'order_item_id']);
	    $this->addForeignKey('fk_invoice_item_invoice', $table, 'invoice_id', 'invoice', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('fk_invoice_item_order_item', $table, 'order_item_id', 'order_item', 'id', 'CASCADE', 'CASCADE');

    }

    public function safeDown()
    {
	    $this->dropForeignKey('fk_invoice_item_invoice', '{{%invoice_item}}');
	    $this->dropForeignKey('fk_invoice_item_order_item', '{{%invoice_item}}');
	    $this->dropTable('{{%invoice_item}}');

    	$this->dropForeignKey('fk_invoice_order', '{{%invoice}}');
        $this->dropTable('{{%invoice}}');
    }

}
