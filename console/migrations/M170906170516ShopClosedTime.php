<?php

use phycom\console\models\Migration;
use yii\db\Schema;

class m170906170516ShopClosedTime extends Migration
{
	const TABLE_NAME = '{{%shop_closed}}';

    public function safeUp()
    {
	    $this->addColumn(self::TABLE_NAME, 'time_from', Schema::TYPE_TIME);
	    $this->addColumn(self::TABLE_NAME, 'time_to', Schema::TYPE_TIME);
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'time_from');
	    $this->dropColumn(self::TABLE_NAME, 'time_to');
    }
}
