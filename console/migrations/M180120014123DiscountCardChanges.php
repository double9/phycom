<?php

use phycom\console\models\Migration;

class m180120014123DiscountCardChanges extends Migration
{
    const TABLE_NAME = '{{%discount_card}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'personal_code', $this->string());
        $this->addColumn(self::TABLE_NAME, 'vendor_id', $this->integer());
        $this->addForeignKey('fk_discount_card_vendor', self::TABLE_NAME, 'vendor_id', 'vendor', 'id', 'SET NULL', 'CASCADE');
        $this->createIndex('idx_discount_card_code', self::TABLE_NAME, 'code', true);
    }

    public function safeDown()
    {
        $this->dropIndex('idx_discount_card_code', self::TABLE_NAME);
        $this->dropForeignKey('fk_discount_card_vendor', self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, 'personal_code');
        $this->dropColumn(self::TABLE_NAME, 'vendor_id');
    }

}
