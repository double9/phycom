<?php

use phycom\console\models\Migration;

class m180918012604ShipmentDeliveryDate extends Migration
{
    const TABLE_NAME = '{{%shipment}}';

    public function safeUp()
    {
        $this->renameColumn(self::TABLE_NAME, 'carrier_delivery_time', 'delivery_time');
        $this->addColumn(self::TABLE_NAME, 'delivery_date', $this->date());
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'delivery_date');
        $this->renameColumn(self::TABLE_NAME, 'delivery_time', 'carrier_delivery_time');
    }

}
