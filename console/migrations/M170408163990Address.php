<?php

use phycom\console\models\Migration;

class m170408163990Address extends Migration
{

    public function safeUp()
    {
	    $table = '{{%address}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'user_id' => $this->integer(),
		    'vendor_id' => $this->integer(),
		    'shop_id' => $this->integer(),
		    'country' => $this->string()->notNull(),
		    'province' => $this->string(),
		    'city' => $this->string(),
		    'district' => $this->string(),
		    'street' => $this->string()->notNull(),
		    'postcode' => $this->string()->notNull(),
		    'type' => $this->string()->notNull(),
		    'status' => $this->string()->notNull(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->addForeignKey('fk_address_user', $table, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('fk_address_vendor', $table, 'vendor_id', 'vendor', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('fk_address_shop', $table, 'shop_id', 'shop', 'id', 'CASCADE', 'CASCADE');
    }


    public function safeDown()
    {
	    $this->dropForeignKey('fk_address_user', '{{%address}}');
	    $this->dropForeignKey('fk_address_vendor', '{{%address}}');
	    $this->dropForeignKey('fk_address_shop', '{{%address}}');
	    $this->dropTable('{{%address}}');
    }

}
