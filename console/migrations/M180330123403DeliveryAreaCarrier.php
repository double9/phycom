<?php

use phycom\console\models\Migration;

class m180330123403DeliveryAreaCarrier extends Migration
{

    const TABLE_NAME = '{{%delivery_area}}';

    public function safeUp()
    {
        $this->alterColumn(self::TABLE_NAME, 'carrier', 'DROP NOT NULL'); //for drop not null

    }

    public function safeDown()
    {
        echo "m180330_123403_delivery_area_carrier cannot be reverted.\n";
        return false;
    }

}
