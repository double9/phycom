<?php

use phycom\console\models\Migration;
use yii\db\Schema;

class m171028092710ShipmentCarrierRateId extends Migration
{
    const TABLE_NAME = '{{%shipment}}';

    public function safeUp()
    {
        $this->renameColumn(self::TABLE_NAME, 'reference_id', 'shipment_id');
        $this->addColumn(self::TABLE_NAME, 'rate_id', Schema::TYPE_STRING);
        $this->addColumn(self::TABLE_NAME, 'eta', 'timestamptz');
        $this->addColumn(self::TABLE_NAME, 'tracking_status', Schema::TYPE_STRING);
        $this->addColumn(self::TABLE_NAME, 'tracking_url', Schema::TYPE_STRING);
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'rate_id');
        $this->dropColumn(self::TABLE_NAME, 'eta');
        $this->dropColumn(self::TABLE_NAME, 'tracking_status');
        $this->dropColumn(self::TABLE_NAME, 'tracking_url');
        $this->renameColumn(self::TABLE_NAME, 'shipment_id', 'reference_id');
    }
}
