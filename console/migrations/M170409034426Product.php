<?php

use phycom\console\models\Migration;

class m170409034426Product extends Migration
{

    public function safeUp()
    {
	    $table = '{{%product}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'sku' => $this->string()->notNull(),
		    'vendor_id' => $this->integer()->notNull(),
		    'price_id' => $this->integer(),
		    'include_vat' => $this->boolean(),
		    'unit_type' => $this->string()->notNull(),
		    'stock' => $this->integer(),
		    'status' => $this->string()->notNull(),
		    'data' => 'jsonb',
		    'created_by' => $this->integer(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ]);
	    $this->addForeignKey('fk_product_user', $table, 'created_by', 'user', 'id', 'SET NULL', 'CASCADE');
	    $this->addForeignKey('fk_product_vendor', $table, 'vendor_id', 'vendor', 'id', 'SET NULL', 'CASCADE');

	    $table = '{{%product_translation}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'product_id' => $this->integer()->notNull(),
		    'language' => $this->char(2)->notNull(),
		    'url_key' => $this->string()->unique()->notNull(),
		    'title' => $this->text(),
		    'description' => $this->text(),
		    'meta_title' => $this->text(),
		    'meta_keywords' => $this->text(),
		    'meta_description' => $this->text(),
		    'status' => $this->string()->notNull(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ]);

	    $this->addForeignKey('fk_product_translation_language', $table, 'language', 'language', 'code', 'NO ACTION', 'CASCADE');
	    $this->addForeignKey('fk_product_translation_product', $table, 'product_id', 'product', 'id', 'SET NULL', 'CASCADE');

	    $table = '{{%product_in_product_category}}';
	    $this->createTable($table, [
		    'product_id' => $this->integer()->notNull(),
		    'category_id' => $this->integer()->notNull(),
		    'created_by' => $this->integer(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
	    ]);
	    $this->addPrimaryKey('pk_product_in_product_category', $table, ['product_id', 'category_id']);
	    $this->addForeignKey('fk_product_in_product_category_user', $table, 'created_by', 'user', 'id', 'SET NULL', 'CASCADE');
	    $this->addForeignKey('fk_product_in_product_category_product', $table, 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('fk_product_in_product_category_category', $table, 'category_id', 'product_category', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
	    $this->dropForeignKey('fk_product_translation_language', '{{%product_translation}}');
    	$this->dropForeignKey('fk_product_translation_product', '{{%product_translation}}');
	    $this->dropTable('{{%product_translation}}');

	    $this->dropForeignKey('fk_product_in_product_category_user', '{{%product_in_product_category}}');
	    $this->dropForeignKey('fk_product_in_product_category_product', '{{%product_in_product_category}}');
	    $this->dropForeignKey('fk_product_in_product_category_category', '{{%product_in_product_category}}');
	    $this->dropTable('{{%product_in_product_category}}');

	    $this->dropForeignKey('fk_product_user', '{{%product}}');
	    $this->dropForeignKey('fk_product_vendor', '{{%product}}');
	    $this->dropTable('{{%product}}');
    }

}
