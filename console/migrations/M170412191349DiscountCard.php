<?php

use phycom\console\models\Migration;

class m170412191349DiscountCard extends Migration
{
    public function safeUp()
    {
	    $table = '{{%discount_card}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'shop_id' => $this->integer(),
		    'code' => $this->string()->notNull(),
		    'discount_rate' => $this->decimal(5,4),
		    'birthday_discount_rate' => $this->decimal(5,4),
		    'name' => $this->string(),
		    'birthday' => $this->date(),
		    'used' => $this->integer()->defaultValue(0),
		    'type' => $this->string()->notNull(),
		    'status' => $this->string()->notNull(),
		    'expires_at' => 'TIMESTAMPTZ',
		    'created_by' => $this->integer(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->addForeignKey('fk_discount_card_shop', $table, 'shop_id', 'shop', 'id', 'SET NULL', 'CASCADE');
	    $this->addForeignKey('fk_discount_card_user', $table, 'created_by', 'user', 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
	    $this->dropForeignKey('fk_discount_card_shop', '{{%discount_card}}');
	    $this->dropForeignKey('fk_discount_card_user', '{{%discount_card}}');
	    $this->dropTable('{{%discount_card}}');
    }
}
