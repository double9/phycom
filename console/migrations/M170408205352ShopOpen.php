<?php

use phycom\console\models\Migration;

class m170408205352ShopOpen extends Migration
{

    public function safeUp()
    {
	    $table = '{{%shop_open}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'shop_id' => $this->integer()->notNull(),
		    'day_of_week' => $this->smallInteger(1)->notNull(),
		    'opened_at' => $this->time(),
		    'closed_at' => $this->time(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->addForeignKey('fk_shop_open_shop', $table, 'shop_id', 'shop', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
	    $this->dropForeignKey('fk_shop_open_shop', '{{%shop_open}}');
	    $this->dropTable('{{%shop_open}}');
    }

}
