<?php

use phycom\console\models\Migration;

class m180526010714SupplyDays extends Migration
{
    const TABLE_NAME = '{{%shop_supply}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'shop_id' => $this->integer()->notNull(),
            'day_of_week' => $this->smallInteger(1)->notNull(),
            'dispatched_at' => $this->time(),
            'delivery_at' => $this->time(),
            'created_at' => 'TIMESTAMPTZ NOT NULL',
            'updated_at' => 'TIMESTAMPTZ NOT NULL',
        ], null);

        $this->addForeignKey('fk_shop_supply_shop', self::TABLE_NAME, 'shop_id', 'shop', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_shop_supply_shop', self::TABLE_NAME);
        $this->dropTable(self::TABLE_NAME);
    }

}
