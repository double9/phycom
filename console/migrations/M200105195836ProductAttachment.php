<?php

use phycom\console\models\Migration;

class m200105195836ProductAttachment extends Migration
{
    const TABLE = '{{%product_attachment}}';

    public function safeUp()
    {
        $this->renameColumn(self::TABLE, 'is_primary', 'is_visible');
    }

    public function safeDown()
    {
        $this->renameColumn(self::TABLE, 'is_visible', 'is_primary');
    }

}
