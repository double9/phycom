<?php

use phycom\console\models\Migration;

class m180414054713PostAttachmentMeta extends Migration
{
    const TABLE_NAME = '{{%post_attachment}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'meta', 'jsonb');
        $this->addColumn(self::TABLE_NAME, 'is_primary', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'is_primary');
        $this->dropColumn(self::TABLE_NAME, 'meta');
    }
}
