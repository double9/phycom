<?php

use phycom\console\models\Migration;

class m170409034300ProductCategory extends Migration
{

    public function safeUp()
    {
	    $table = '{{%product_category}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'shop_id' => $this->integer(),
		    'parent_id' => $this->integer(),
		    'status' => $this->string()->notNull(),
		    'order' => $this->smallInteger()->notNull(),
		    'created_by' => $this->integer(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ]);

	    $this->addForeignKey('fk_product_category_shop', $table, 'shop_id', 'shop', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('fk_product_category_user', $table, 'created_by', 'user', 'id', 'SET NULL', 'CASCADE');
	    $this->addForeignKey('fk_product_category_parent', $table, 'parent_id', $table, 'id', 'SET NULL', 'CASCADE');

	    $table = '{{%product_category_relation}}';
	    $this->createTable($table, [
		    'category_id' => $this->integer()->notNull(),
		    'related_category_id' => $this->integer()->notNull(),
		    'relation_type' => $this->string()->notNull(),
		    'status' => $this->string()->notNull(),
		    'created_by' => $this->integer(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ]);

	    $this->addPrimaryKey('pk_product_category_relation', $table, ['category_id','related_category_id']);
	    $this->addForeignKey('fk_product_category_relation_category', $table, 'category_id', 'product_category', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('fk_product_category_relation_related', $table, 'related_category_id', 'product_category', 'id', 'CASCADE', 'CASCADE');


	    $table = '{{%product_category_translation}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'product_category_id' => $this->integer()->notNull(),
		    'language' => $this->char(2)->notNull(),
		    'url_key' => $this->string()->unique()->notNull(),
		    'title' => $this->text(),
		    'description' => $this->text(),
		    'meta_title' => $this->text(),
		    'meta_keywords' => $this->text(),
		    'meta_description' => $this->text(),
		    'status' => $this->string()->notNull(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ]);

	    $this->addForeignKey(
	    	'fk_product_category_translation_language',
	        $table,
		    'language',
		    'language',
		    'code',
		    'NO ACTION',
		    'CASCADE'
	    );
	    $this->addForeignKey(
	    	'fk_product_category_translation_product_category',
		    $table,
		    'product_category_id',
		    'product_category',
		    'id',
		    'CASCADE',
		    'CASCADE'
	    );
    }

    public function safeDown()
    {
    	$this->dropForeignKey('fk_product_category_relation_category', '{{%product_category_relation}}');
    	$this->dropForeignKey('fk_product_category_relation_related', '{{%product_category_relation}}');
    	$this->dropTable('{{%product_category_relation}}');

	    $this->dropForeignKey('fk_product_category_translation_language', '{{%product_category_translation}}');
	    $this->dropForeignKey('fk_product_category_translation_product_category', '{{%product_category_translation}}');
	    $this->dropTable('{{%product_category_translation}}');

	    $this->dropForeignKey('fk_product_category_shop', '{{%product_category}}');
	    $this->dropForeignKey('fk_product_category_parent', '{{%product_category}}');
	    $this->dropForeignKey('fk_product_category_user', '{{%product_category}}');
	    $this->dropTable('{{%product_category}}');
    }

}
