<?php

use phycom\console\models\Migration;

class m191117214737ProductVariant extends Migration
{
    const TABLE_MAIN = '{{%product_variant}}';
    const TABLE_OPTS = '{{%product_variant_option}}';

    public function safeUp()
    {
        $this->renameTable('{{%product_attribute}}', self::TABLE_MAIN);
        $this->renameTable('{{%product_attribute_option}}', self::TABLE_OPTS);

        $this->renameColumn(self::TABLE_MAIN, 'attribute', 'name');
        $this->dropColumn(self::TABLE_MAIN, 'value');

        $this->dropColumn(self::TABLE_OPTS, 'price');

        $this->renameColumn(self::TABLE_OPTS, 'product_attribute_id', 'product_variant_id');

        $this->cmd('ALTER INDEX product_attribute_pkey RENAME TO product_variant_pkey');
        $this->cmd('ALTER TABLE product_variant RENAME CONSTRAINT fk_product_attribute_product TO fk_product_variant_product');
        $this->cmd('ALTER INDEX idx_product_attribute RENAME TO idx_product_variant');

        $this->cmd('ALTER INDEX product_attribute_option_pkey RENAME TO product_variant_option_pkey');
        $this->cmd('ALTER INDEX product_attribute_option_sku_key RENAME TO product_variant_option_sku_key');
        $this->cmd('ALTER TABLE product_variant_option RENAME CONSTRAINT fk_product_attribute_option_attribute TO fk_product_variant_option_product_variant');
        $this->cmd('ALTER INDEX idx_product_attribute_option RENAME TO idx_product_variant_option');

        $this->addColumn(self::TABLE_MAIN, 'created_by', $this->integer());
        $this->addColumn(self::TABLE_MAIN, 'updated_by', $this->integer());
        $this->addForeignKey('fk_product_variant_created_by', self::TABLE_MAIN, 'created_by', 'user', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_product_variant_updated_by', self::TABLE_MAIN, 'updated_by', 'user', 'id', 'SET NULL', 'CASCADE');

        $this->addColumn(self::TABLE_OPTS, 'is_visible', $this->boolean());
        $this->update(self::TABLE_OPTS, ['is_visible' => true]);
        $this->alterColumn(self::TABLE_OPTS, 'is_visible', $this->boolean()->notNull()->defaultValue(true));

        $this->dropColumn(self::TABLE_OPTS, 'status');

        $this->addColumn(self::TABLE_MAIN, 'default_option_id', $this->integer());
        $this->addForeignKey('fk_product_variant_default_option', self::TABLE_MAIN, 'default_option_id', self::TABLE_OPTS, 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_product_variant_default_option', self::TABLE_MAIN);
        $this->dropColumn(self::TABLE_MAIN, 'default_option_id');

        $this->addColumn(self::TABLE_OPTS, 'status', $this->string());
        $this->dropColumn(self::TABLE_OPTS, 'is_visible');

        $this->dropForeignKey('fk_product_variant_created_by', self::TABLE_MAIN);
        $this->dropForeignKey('fk_product_variant_updated_by', self::TABLE_MAIN);
        $this->dropColumn(self::TABLE_MAIN, 'created_by');
        $this->dropColumn(self::TABLE_MAIN, 'updated_by');

        $this->cmd('ALTER INDEX product_variant_pkey RENAME TO product_attribute_pkey');
        $this->cmd('ALTER TABLE product_variant RENAME CONSTRAINT fk_product_variant_product TO fk_product_attribute_product');
        $this->cmd('ALTER INDEX idx_product_variant RENAME TO idx_product_attribute');

        $this->cmd('ALTER INDEX product_variant_option_pkey RENAME TO product_attribute_option_pkey');
        $this->cmd('ALTER INDEX product_variant_option_sku_key RENAME TO product_attribute_option_sku_key');
        $this->cmd('ALTER TABLE product_variant_option RENAME CONSTRAINT fk_product_variant_option_product_variant TO fk_product_attribute_option_attribute');
        $this->cmd('ALTER INDEX idx_product_variant_option RENAME TO idx_product_attribute_option');

        $this->renameColumn(self::TABLE_OPTS, 'product_variant_id', 'product_attribute_id');

        $this->addColumn(self::TABLE_OPTS, 'price', $this->decimal(13,4));

        $this->addColumn(self::TABLE_MAIN, 'value', $this->string());
        $this->renameColumn(self::TABLE_MAIN, 'name', 'attribute');

        $this->renameTable(self::TABLE_MAIN, '{{%product_attribute}}');
        $this->renameTable(self::TABLE_OPTS, '{{%product_attribute_option}}');
    }

}
