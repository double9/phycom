<?php

use phycom\console\models\Migration;

class m171014235918AddressMeta extends Migration
{
    const TABLE_NAME = '{{%address_meta}}';
    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'address_id' => $this->integer()->notNull(),
            'name' => $this->string(),
            'company' => $this->string(),
            'email' => $this->string(),
            'phone' => $this->string(),
            'lat' => $this->decimal(10,6),
            'lng' => $this->decimal(10,6),
            'description' => $this->text(),
            'created_at' => 'TIMESTAMPTZ NOT NULL',
            'updated_at' => 'TIMESTAMPTZ NOT NULL'
        ]);
        $this->addForeignKey('fk_address_meta_address', self::TABLE_NAME, 'address_id', 'address', 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_address_meta_address', self::TABLE_NAME);
        $this->dropTable(self::TABLE_NAME);
    }

}
