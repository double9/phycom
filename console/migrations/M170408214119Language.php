<?php

use phycom\console\models\Migration;
use phycom\common\models\Language;
use phycom\common\models\attributes\LanguageStatus;

class m170408214119Language extends Migration
{

    public function safeUp()
    {
	    $table = '{{%language}}';
	    $this->createTable($table, [
		    'code' => $this->char(2)->notNull(),
		    'name' => $this->string()->notNull(),
		    'native' => $this->string(),
		    'status' => $this->string()->notNull()->defaultValue(LanguageStatus::UNUSED)
	    ], null);
	    $this->addPrimaryKey('pk_language_code', $table, ['code']);
	    $this->populateData();
	    $this->update($table, ['status' => LanguageStatus::VISIBLE], 'code = :lang', ['lang' => substr(Yii::$app->language, 0, 2)]);
    }

    public function safeDown()
    {
	    $this->dropTable('{{%language}}');
    }


    public function populateData()
    {
	    $this->batchCmd('
            INSERT INTO "language" ("code", "name", "native") VALUES(\'aa\', \'Afar\', \'Afaraf\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ab\', \'Abkhazian\', \'Аҧсуа\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ae\', \'Avestan\', \'avesta\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'af\', \'Afrikaans\', \'Afrikaans\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ak\', \'Akan\', \'Akan\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'am\', \'Amharic\', \'አማርኛ\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'an\', \'Aragonese\', \'Aragonés\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ar\', \'Arabic\', \'العربية\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'as\', \'Assamese\', \'অসমীয়া\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'av\', \'Avaric\', \'магӀарул мацӀ\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ay\', \'Aymara\', \'aymar aru\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'az\', \'Azerbaijani\', \'azərbaycan dili\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ba\', \'Bashkir\', \'башҡорт теле\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'be\', \'Belarusian\', \'Беларуская\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'bg\', \'Bulgarian\', \'български език\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'bh\', \'Bihari\', \'भोजपुरी\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'bi\', \'Bislama\', \'Bislama\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'bm\', \'Bambara\', \'bamanankan\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'bn\', \'Bengali\', \'বাংলা\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'bo\', \'Tibetan\', \'བོད་ཡིག\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'br\', \'Breton\', \'brezhoneg\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'bs\', \'Bosnian\', \'bosanski jezik\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ca\', \'Catalan\', \'Català\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ce\', \'Chechen\', \'нохчийн мотт\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ch\', \'Chamorro\', \'Chamoru\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'co\', \'Corsican\', \'corsu\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'cr\', \'Cree\', \'ᓀᐦᐃᔭᐍᐏᐣ\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'cs\', \'Czech\', \'česky\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'cu\', \'Church Slavic\', \'ѩзыкъ словѣньскъ\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'cv\', \'Chuvash\', \'чӑваш чӗлхи\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'cy\', \'Welsh\', \'Cymraeg\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'da\', \'Danish\', \'dansk\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'de\', \'German\', \'Deutsch\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'dv\', \'Divehi\', \'ދިވެހި\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'dz\', \'Dzongkha\', \'རྫོང་ཁ\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ee\', \'Ewe\', \'Ɛʋɛgbɛ\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'el\', \'Greek\', \'Ελληνικά\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'en\', \'English\', \'English\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'eo\', \'Esperanto\', \'Esperanto\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'es\', \'Spanish\', \'Español\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'et\', \'Estonian\', \'Eesti\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'eu\', \'Basque\', \'euskara\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'fa\', \'Persian\', \'فارسی\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ff\', \'Fulah\', \'Fulfulde\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'fi\', \'Finnish\', \'suomi\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'fj\', \'Fijian\', \'vosa Vakaviti\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'fo\', \'Faroese\', \'Føroyskt\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'fr\', \'French\', \'Français\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'fy\', \'Western Frisian\', \'Frysk\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ga\', \'Irish\', \'Gaeilge\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'gd\', \'Scottish Gaelic\', \'Gàidhlig\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'gl\', \'Galician\', \'Galego\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'gn\', \'Guaraní\', \'Avañe\'\'ẽ\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'gu\', \'Gujarati\', \'ગુજરાતી\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'gv\', \'Manx\', \'Gaelg\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ha\', \'Hausa\', \'هَوُسَ\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'he\', \'Hebrew\', \'עברית\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'hi\', \'Hindi\', \'हिन्दी\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ho\', \'Hiri Motu\', \'Hiri Motu\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'hr\', \'Croatian\', \'Hrvatski\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ht\', \'Haitian\', \'Kreyòl ayisyen\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'hu\', \'Hungarian\', \'Magyar\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'hy\', \'Armenian\', \'Հայերեն\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'hz\', \'Herero\', \'Otjiherero\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ia\', \'Interlingua (International Auxiliary Language Association\', \'Interlingua\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'id\', \'Indonesian\', \'Bahasa Indonesia\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ie\', \'Interlingue\', \'Interlingue\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ig\', \'Igbo\', \'Igbo\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ii\', \'Sichuan Yi\', \'ꆇꉙ\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ik\', \'Inupiaq\', \'Iñupiaq\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'io\', \'Ido\', \'Ido\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'is\', \'Icelandic\', \'Íslenska\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'it\', \'Italian\', \'Italiano\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'iu\', \'Inuktitut\', \'ᐃᓄᒃᑎᑐᑦ\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ja\', \'Japanese\', \'日本語 (にほんご／にっぽんご\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'jv\', \'Javanese\', \'basa Jawa\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ka\', \'Georgian\', \'ქართული\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'kg\', \'Kongo\', \'KiKongo\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ki\', \'Kikuyu\', \'Gĩkũyũ\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'kj\', \'Kwanyama\', \'Kuanyama\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'kk\', \'Kazakh\', \'Қазақ тілі\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'kl\', \'Kalaallisut\', \'kalaallisut\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'km\', \'Khmer\', \'ភាសាខ្មែរ\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'kn\', \'Kannada\', \'ಕನ್ನಡ\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ko\', \'Korean\', \'한국어 (韓國語\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'kr\', \'Kanuri\', \'Kanuri\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ks\', \'Kashmiri\', \'कश्मीरी\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ku\', \'Kurdish\', \'كوردی‎\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'kv\', \'Komi\', \'коми кыв\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'kw\', \'Cornish\', \'Kernewek\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ky\', \'Kirghiz\', \'кыргыз тили\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'la\', \'Latin\', \'latine\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'lb\', \'Luxembourgish\', \'Lëtzebuergesch\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'lg\', \'Ganda\', \'Luganda\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'li\', \'Limburgish\', \'Limburgs\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ln\', \'Lingala\', \'Lingála\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'lo\', \'Lao\', \'ພາສາລາວ\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'lt\', \'Lithuanian\', \'lietuvių kalba\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'lu\', \'Luba-Katanga\', \'\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'lv\', \'Latvian\', \'latviešu valoda\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'mg\', \'Malagasy\', \'Malagasy fiteny\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'mh\', \'Marshallese\', \'Kajin M̧ajeļ\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'mi\', \'Māori\', \'te reo Māori\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'mk\', \'Macedonian\', \'македонски јазик\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ml\', \'Malayalam\', \'മലയാളം\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'mn\', \'Mongolian\', \'Монгол\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'mr\', \'Marathi\', \'मराठी\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ms\', \'Malay\', \'بهاس ملايو‎\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'mt\', \'Maltese\', \'Malti\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'my\', \'Burmese\', \'ဗမာစာ\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'na\', \'Nauru\', \'Ekakairũ Naoero\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'nb\', \'Norwegian Bokmål\', \'Norsk bokmål\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'nd\', \'North Ndebele\', \'isiNdebele\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ne\', \'Nepali\', \'नेपाली\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ng\', \'Ndonga\', \'Owambo\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'nl\', \'Dutch\', \'Nederlands\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'nn\', \'Norwegian Nynorsk\', \'Norsk nynorsk\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'no\', \'Norwegian\', \'Norsk\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'nr\', \'South Ndebele\', \'isiNdebele\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'nv\', \'Navajo\', \'Diné bizaad\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ny\', \'Chichewa\', \'chiCheŵa\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'oc\', \'Occitan\', \'Occitan\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'oj\', \'Ojibwa\', \'ᐊᓂᔑᓈᐯᒧᐎᓐ\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'om\', \'Oromo\', \'Afaan Oromoo\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'or\', \'Oriya\', \'ଓଡ଼ିଆ\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'os\', \'Ossetian\', \'Ирон æвзаг\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'pa\', \'Panjabi\', \'ਪੰਜਾਬੀ\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'pi\', \'Pāli\', \'पाऴि\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'pl\', \'Polish\', \'polski\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ps\', \'Pashto\', \'پښتو\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'pt\', \'Portuguese\', \'Português\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'qu\', \'Quechua\', \'Runa Simi\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'rm\', \'Raeto-Romance\', \'rumantsch grischun\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'rn\', \'Kirundi\', \'kiRundi\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ro\', \'Romanian\', \'română\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ru\', \'Russian\', \'русский язык\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'rw\', \'Kinyarwanda\', \'Ikinyarwanda\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'sa\', \'Sanskrit\', \'संस्कृतम्\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'sc\', \'Sardinian\', \'sardu\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'sd\', \'Sindhi\', \'सिन्धी; سنڌي، سندھی‎\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'se\', \'Northern Sami\', \'Davvisámegiella\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'sg\', \'Sango\', \'yângâ tî sängö\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'sh\', \'Serbo-Croatian\', \'Српскохрватски\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'si\', \'Sinhala\', \'සිංහල\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'sk\', \'Slovak\', \'slovenčina\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'sl\', \'Slovenian\', \'slovenščina\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'sm\', \'Samoan\', \'gagana fa\'\'a Samoa\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'sn\', \'Shona\', \'chiShona\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'so\', \'Somali\', \'Soomaaliga\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'sq\', \'Albanian\', \'Shqip\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'sr\', \'Serbian\', \'српски језик\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ss\', \'Swati\', \'SiSwati\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'st\', \'Southern Sotho\', \'Sesotho\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'su\', \'Sundanese\', \'Basa Sunda\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'sv\', \'Swedish\', \'svenska\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'sw\', \'Swahili\', \'Kiswahili\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ta\', \'Tamil\', \'தமிழ்\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'te\', \'Telugu\', \'తెలుగు\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'tg\', \'Tajik\', \'тоҷикӣ\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'th\', \'Thai\', \'ไทย\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ti\', \'Tigrinya\', \'ትግርኛ\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'tk\', \'Turkmen\', \'Түркмен\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'tl\', \'Tagalog\', \'Tagalog\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'tn\', \'Tswana\', \'Setswana\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'to\', \'Tonga\', \'faka Tonga\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'tr\', \'Turkish\', \'Türkçe\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ts\', \'Tsonga\', \'Xitsonga\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'tt\', \'Tatar\', \'татарча\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'tw\', \'Twi\', \'Twi\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ty\', \'Tahitian\', \'Reo Mā"ohi\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ug\', \'Uighur\', \'Uyƣurqə\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'uk\', \'Ukrainian\', \'Українська\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ur\', \'Urdu\', \'اردو\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'uz\', \'Uzbek\', \'Ўзбек‎\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'ve\', \'Venda\', \'Tshivenḓa\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'vi\', \'Vietnamese\', \'Tiếng Việt\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'vo\', \'Volapük\', \'Volapük\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'wa\', \'Walloon\', \'Walon\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'wo\', \'Wolof\', \'Wollof\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'xh\', \'Xhosa\', \'isiXhosa\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'yi\', \'Yiddish\', \'ייִדיש\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'yo\', \'Yoruba\', \'Yorùbá\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'za\', \'Zhuang\', \'Saɯ cueŋƅ\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'zh\', \'Chinese\', \'中文 (Zhōngwén\');
INSERT INTO "language" ("code", "name", "native") VALUES(\'zu\', \'Zulu\', \'isiZulu\');
        ');
    }
}
