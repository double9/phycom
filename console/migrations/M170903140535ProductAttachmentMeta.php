<?php

use phycom\console\models\Migration;

class m170903140535ProductAttachmentMeta extends Migration
{
	const TABLE_NAME = '{{%product_attachment}}';

    public function safeUp()
    {
	    $this->addColumn(self::TABLE_NAME, 'meta', 'jsonb');
    }

    public function safeDown()
    {
	    $this->dropColumn(self::TABLE_NAME, 'meta');
    }
}
