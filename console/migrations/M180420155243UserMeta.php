<?php

use phycom\console\models\Migration;
use yii\db\pgsql\Schema;

class m180420155243UserMeta extends Migration
{
    const TABLE_NAME = '{{%user}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'meta', Schema::TYPE_JSONB);
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'meta');
    }

}
