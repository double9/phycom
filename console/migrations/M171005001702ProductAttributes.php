<?php

use yii\db\Schema;
use phycom\console\models\Migration;
use phycom\common\models\product\ProductVariant;
use phycom\common\models\attributes\ProductVariantStatus;

/**
 * Class m171005_001702_product_attributes
 *
 * Migrates the product.properties to product_attribute table
 */
class m171005001702ProductAttributes extends Migration
{
	const TABLE_NAME = '{{%product_attribute}}';

	public function safeUp()
    {
	    $this->addColumn(self::TABLE_NAME, 'value', Schema::TYPE_STRING);
	    $this->addColumn(self::TABLE_NAME, 'user_select', Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT FALSE');


	    $reader = $this->db->createCommand('SELECT * FROM product')->query();
	    while ($row = $reader->read()) {

	    	$properties = json_decode($row['properties'], true);
	    	if ($properties && is_array($properties)) {
			    foreach ($properties as $key => $value) {
				    $attribute = Yii::$app->modelFactory->getVariant()::findByName($key);

				    if (!$attribute) {
				    	throw new \yii\base\InvalidCallException('You must migrate all product properties to attributes first');
				    }

				    $productAttribute = ProductVariant::find()
					    ->where(['product_id' => $row['id']])
					    ->andWhere(['attribute' => $attribute->name])
					    ->one();

				    if (!$productAttribute) {
					    $productAttribute = new ProductVariant();
					    $productAttribute->name = $attribute->name;
					    $productAttribute->product_id = $row['id'];
				    }

					$productAttribute->name = $key;
				    $productAttribute->value = (string) $value;
				    $productAttribute->status = ProductVariantStatus::VISIBLE;
				    if (!$productAttribute->save()) {
				        echo json_encode($productAttribute->errors, JSON_PRETTY_PRINT) . PHP_EOL;
				    }
			    }
		    }
	    }
	    $this->dropColumn('{{%product}}', 'properties');
    }


    public function safeDown()
    {
    	$this->addColumn('{{%product}}', 'properties', 'jsonb');
    	$properties = [];
	    $reader = $this->db->createCommand('SELECT * FROM product_attribute ORDER BY product_id')->query();
	    while ($row = $reader->read()) {
	        if (!isset($properties[$row['product_id']])) {
	            $properties[$row['product_id']] = [];
	        }
	        $properties[$row['product_id']][$row['attribute']] = $row['value'];
	    }
	    foreach ($properties as $productId => $value) {
		    if (!empty($value)) {
			    $this->update('{{%product}}', ['properties' => json_encode($properties)], 'id = :id', ['id' => $productId]);
		    }
	    }
	    $this->db->createCommand('DELETE * FROM product_attribute')->execute();
        $this->dropColumn(self::TABLE_NAME, 'value');
        $this->dropColumn(self::TABLE_NAME, 'user_select');
    }
}
