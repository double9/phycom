<?php

use phycom\console\models\Migration;

class m170408163940VendorUser extends Migration
{

    public function safeUp()
    {
	    $table = '{{%vendor_user}}';
	    $this->createTable($table, [
		    'vendor_id' => $this->integer()->notNull(),
		    'user_id' => $this->integer()->notNull(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->addPrimaryKey('pk_vendor_user', $table, ['vendor_id', 'user_id']);
	    $this->addForeignKey('fk_vendor_user_vendor', $table, 'vendor_id', 'vendor', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('fk_vendor_user_user', $table, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
    	$this->dropForeignKey('fk_vendor_user_vendor', '{{%vendor_user}}');
	    $this->dropForeignKey('fk_vendor_user_user', '{{%vendor_user}}');
	    $this->dropTable('{{%vendor_user}}');
    }

}
