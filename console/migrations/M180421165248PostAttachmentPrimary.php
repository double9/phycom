<?php

use phycom\console\models\Migration;

class m180421165248PostAttachmentPrimary extends Migration
{
    const TABLE_NAME = '{{%post_attachment}}';

    public function safeUp()
    {
        $this->dropColumn(self::TABLE_NAME, 'is_primary');
    }

    public function safeDown()
    {
        $this->addColumn(self::TABLE_NAME, 'is_primary', $this->boolean()->defaultValue(false));
    }

}
