<?php

use phycom\console\models\Migration;

class m180317123405Reviews extends Migration
{
    const TABLE_REVIEW = '{{%review}}';
    const TABLE_PRODUCT_REVIEW = '{{%product_review}}';
    const TABLE_SHOP_REVIEW = '{{%shop_review}}';


    public function safeUp()
    {
        $this->createTable(self::TABLE_REVIEW, [
            'id' => $this->primaryKey(),
            'created_by' => $this->integer()->notNull(),
            'approved_by' => $this->integer(),
            'score' => $this->integer()->notNull(),
            'title' => $this->text(),
            'description' => $this->text(),
            'status' => $this->string(),
            'verified_purchase' => $this->boolean(),
            'created_at' => 'TIMESTAMPTZ NOT NULL',
            'updated_at' => 'TIMESTAMPTZ NOT NULL',
        ], null);

        $this->addForeignKey('fk_review_created_by', self::TABLE_REVIEW, 'created_by', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_review_approved_by', self::TABLE_REVIEW, 'approved_by', 'user', 'id', 'SET NULL', 'CASCADE');
        $this->createIndex('idx_review_score', self::TABLE_REVIEW, 'score');
        $this->createIndex('idx_review_created_at', self::TABLE_REVIEW, 'created_at');

        $this->createTable(self::TABLE_PRODUCT_REVIEW, [
            'product_id' => $this->integer()->notNull(),
            'review_id' => $this->integer()->notNull()
        ], null);
        $this->addPrimaryKey('pk_product_review', self::TABLE_PRODUCT_REVIEW, ['product_id','review_id']);
        $this->addForeignKey('fk_product_review_product', self::TABLE_PRODUCT_REVIEW, 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_product_review_review', self::TABLE_PRODUCT_REVIEW, 'review_id', self::TABLE_REVIEW, 'id', 'CASCADE', 'CASCADE');


        $this->createTable(self::TABLE_SHOP_REVIEW, [
            'shop_id' => $this->integer()->notNull(),
            'review_id' => $this->integer()->notNull()
        ], null);
        $this->addPrimaryKey('pk_shop_review', self::TABLE_SHOP_REVIEW, ['shop_id','review_id']);
        $this->addForeignKey('fk_shop_review_shop', self::TABLE_SHOP_REVIEW, 'shop_id', 'shop', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_shop_review_review', self::TABLE_SHOP_REVIEW, 'review_id', self::TABLE_REVIEW, 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_PRODUCT_REVIEW . ' CASCADE');
        $this->dropTable(self::TABLE_SHOP_REVIEW . ' CASCADE');
        $this->dropTable(self::TABLE_REVIEW . ' CASCADE');
    }

}
