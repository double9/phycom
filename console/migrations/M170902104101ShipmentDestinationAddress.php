<?php

use phycom\console\models\Migration;

class m170902104101ShipmentDestinationAddress extends Migration
{
	const TABLE_NAME = '{{%shipment}}';

    public function safeUp()
    {
	    $this->alterColumn(self::TABLE_NAME, 'destination_address', 'DROP NOT NULL'); //for drop not null
	    $this->alterColumn(self::TABLE_NAME, 'destination_address', 'SET DEFAULT NULL'); //for set default null value
    }

    public function safeDown()
    {

    }

}
