<?php

use phycom\console\models\Migration;

class m170412173855ProductAttribute extends Migration
{

    public function safeUp()
    {
	    $table = '{{%product_attribute}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'product_id' => $this->integer()->notNull(),
		    'attribute' => $this->string()->notNull(),
		    'status' => $this->string(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ]);
	    $this->addForeignKey('fk_product_attribute_product', $table, 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
	    $this->createIndex('idx_product_attribute', $table, ['product_id', 'attribute'], true);
    }

    public function safeDown()
    {
    	$this->dropIndex('idx_product_attribute', '{{%product_attribute}}');
	    $this->dropForeignKey('fk_product_attribute_product', '{{%product_attribute}}');
	    $this->dropTable('{{%product_attribute}}');
    }

}
