<?php

use phycom\console\models\Migration;

class m180911094727PostcodeFields extends Migration
{
    const TABLE_NAME = '{{%postcode}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'keyword', $this->string());
        $this->addColumn(self::TABLE_NAME, 'house_number', $this->integer());

        $this->createIndex('idx_postcode_keyword', self::TABLE_NAME, 'keyword');
        $this->createIndex('idx_postcode_house_number', self::TABLE_NAME, 'house_number');
    }

    public function safeDown()
    {
        $this->dropIndex('idx_postcode_keyword', self::TABLE_NAME);
        $this->dropIndex('idx_postcode_house_number', self::TABLE_NAME);

        $this->dropColumn(self::TABLE_NAME, 'keyword');
        $this->dropColumn(self::TABLE_NAME, 'house_number');
    }

}
