<?php

use phycom\console\models\Migration;

class m170610083658Indexes extends Migration
{

    public function safeUp()
    {
	    $this->createIndex('idx_user_created_at', '{{%user}}', 'created_at');
	    $this->createIndex('idx_email_updated_at', '{{%email}}', 'updated_at');
    }

    public function safeDown()
    {
	    $this->dropIndex('idx_user_created_at', '{{%user}}');
	    $this->dropIndex('idx_email_updated_at', '{{%email}}');
    }

}
