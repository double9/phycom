<?php
$params = array_merge(
    require(PHYCOM_PATH . '/common/config/params.php'),
    require(__DIR__ . '/params.php'),
    require(ROOT_PATH . '/common/config/params.php'),
    require(ROOT_PATH . '/console/config/params.php')
);

return [
    'id' => 'app-console',
    'basePath' => ROOT_PATH . '/console',
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'controllerMap' => [
        'fixture'  => [
            'class'     => \yii\console\controllers\FixtureController::class,
            'namespace' => 'phycom\common\fixtures',
        ],
        'migrate'  => [
            'class'               => \yii\console\controllers\MigrateController::class,
            'migrationPath'       => null,
            'templateFile'        => '@base-app/migrations/template.php',
            'migrationNamespaces' => [
                'phycom\console\migrations'
            ],
        ],
        'help'     => \phycom\console\controllers\HelpController::class,
        'email'    => \phycom\common\modules\email\controllers\ConsoleController::class,
        'sms'      => \phycom\common\modules\sms\controllers\ConsoleController::class,
        'delivery' => \phycom\common\modules\delivery\controllers\ConsoleController::class
    ],
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
	    'fileStorage' => [
		    'storages' => [
			    'local' => [
				    'baseUrl' => '@backend/file/download',
			    ]
		    ]
	    ],
    ],
    'params' => $params,
];
