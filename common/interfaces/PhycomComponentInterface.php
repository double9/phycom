<?php

namespace phycom\common\interfaces;

/**
 * Generic interface for any Phycom application component
 *
 * Interface PhycomComponentInterface
 *
 * @package phycom\common\interfaces
 */
interface PhycomComponentInterface
{
    /**
     * @return bool
     */
    public function isEnabled();
}
