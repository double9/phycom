<?php

namespace phycom\common\interfaces;

use yii\data\ActiveDataProvider;

interface SearchModelInterface
{
	/**
	 * @param array $params
	 * @return ActiveDataProvider
	 */
	public function search(array $params = []);
}