<?php

namespace phycom\common\interfaces;


use phycom\common\models\ModuleSettingsForm;

/**
 * Interface ModuleSettingsInterface
 * @package phycom\common\interfaces
 */
interface ModuleSettingsInterface
{
    /**
     * @return string
     */
    public function getSettingsFormClassName();
    /**
     * @return ModuleSettingsForm
     */
    public function getSettings();
}