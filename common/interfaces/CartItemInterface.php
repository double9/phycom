<?php

namespace phycom\common\interfaces;


/**
 * Adds the quantity feature to base cart interface
 *
 * Interface CartItemInterface
 * @package phycom\common\interfaces
 */
interface CartItemInterface extends CartItemBaseInterface
{
	public function getCode();

	public function getQuantity();

	public function setQuantity(int $value);

	public function getTotalPrice();

	public function getOptionValue($attribute);

	public function getOptions();

    /**
     * @return float
     */
	public function getWeight();

    /**
     * @return array [length, width, height]
     */
	public function getDimensions();
}