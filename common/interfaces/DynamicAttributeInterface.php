<?php

namespace phycom\common\interfaces;


interface DynamicAttributeInterface
{
	/**
	 * @param mixed|null $value
	 * @param array $config
	 */
	public function __construct($value = null, array $config = []);

	/**
	 * @return mixed
	 */
	public function getValue();


	/**
	 * @param mixed $value
	 * @return bool
	 */
	public function is($value);


	/**
	 * @param array $values
	 * @return bool
	 */
	public function in(array $values);

	/**
	 * @return string
	 */
	public function __toString();
}