<?php

namespace phycom\common\interfaces;

/**
 * Interface ModelOwnerInterface
 * @package phycom\common\interfaces
 */
interface ModelOwnerInterface
{
	/**
	 * @param int $userId
	 * @returns bool
	 */
	public function isOwner($userId);
}