<?php

namespace phycom\common\interfaces;

/**
 * Interface CommerceComponentInterface
 *
 * @package phycom\common\interfaces
 *
 * Interface for any Phycom application component that is a child of Commerce component
 */
interface CommerceComponentInterface extends PhycomComponentInterface
{

}
