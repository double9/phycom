<?php

namespace phycom\common\interfaces;

/**
 * Interface CartItemDiscountInterface
 * @package phycom\common\interfaces
 */
interface CartItemDiscountInterface extends CartItemBaseInterface
{
    public function getCode();

    public function calculateDiscount();
}