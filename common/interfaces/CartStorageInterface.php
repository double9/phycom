<?php

namespace phycom\common\interfaces;

use phycom\common\components\Cart;


/**
 * Interface StorageInterface
 * @package phycom\common\interfaces
 */
interface CartStorageInterface
{
    /**
     * @param Cart $cart
     *
     * @return array|mixed
     */
    public function load(Cart $cart);

    /**
     * @param Cart $cart
     */
    public function save(Cart $cart);
}
