
var ActiveFormHelper = (function ($) {

    return {

        validateAndSubmit: function ($form, onError) {
            onError = onError || function () {};
            return this.validate($form, onError).then(this.submit).fail(function(errors) {
                onError(errors);
            });
        },

        submit: function ($form) {
            // submit all forms by ajax
            return $.ajax({
                url: $form.attr('action'),
                type: 'POST',
                data: $form.serialize()
            });
        },

        validate: function ($form, onReject) {
            var deferred = $.Deferred();

            onReject = onReject || function () {};

            // validate form but not submit
            $form.data('yiiActiveForm').submitting = true;
            $form.yiiActiveForm('validate');
            $form.one('submit', function (e) {
                e.preventDefault();
                return false;
            });

            $form.one('afterValidate', function (e, messages, errorAttributes) {
                for (var attribute in messages) {
                    if (messages.hasOwnProperty(attribute) && messages[attribute].length) {
                        onReject(messages[attribute]);
                        return deferred.reject(messages[attribute][0]);
                    }
                }
                return deferred.resolve($form);
            });
            return deferred.promise();
        },

        validateP: function ($form, onReject) {

            onReject = onReject || function () {};

            return new Promise((resolve, reject) => {

                // validate form but not submit
                $form.data('yiiActiveForm').submitting = true;
                $form.yiiActiveForm('validate');
                $form.one('submit', function (e) {
                    e.preventDefault();
                    return false;
                });

                $form.one('afterValidate', function (e, messages, errorAttributes) {
                    for (var attribute in messages) {
                        if (messages.hasOwnProperty(attribute) && messages[attribute].length) {
                            onReject(messages[attribute]);
                            return reject(messages[attribute][0]);
                        }
                    }
                    return resolve($form);
                });
            });
        },

        addError: function ($form, id, errorMessage) {
            var messages = $.isArray(errorMessage) ? errorMessage : [errorMessage];
            $form.yiiActiveForm('updateAttribute', id, messages);
        }
    };

})(jQuery);
