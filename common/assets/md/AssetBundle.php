<?php

namespace phycom\common\assets\md;


/**
 * Class AssetBundle
 * @package phycom\common\assets\md
 */
class AssetBundle extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@vendor/mervick/material-design-icons';
    /**
     * @inheritdoc
     */
    public $css = [
        'css/material-icons.min.css',
    ];
    /**
     * Initializes the bundle.
     * Set publish options to copy only necessary files (in this case css and font folders)
     */
    public function init()
    {
        parent::init();
        $this->publishOptions['beforeCopy'] = function ($from, $to) {
            return preg_match('%(/|\\\\)(fonts|css)%', $from);
        };
    }
}