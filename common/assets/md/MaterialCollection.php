<?php

namespace phycom\common\assets\md;

/**
 * Class BaseIconCollection
 * @package phycom\common\assets\md
 */
abstract class MaterialCollection extends \yii\base\BaseObject
{
    /**
     * @param string $name
     * @param array $options
     * @return MaterialIcon
     */
    public static function icon($name, $options = [])
    {
        return new MaterialIcon($name, $options);
    }

    /**
     * @param array $options
     * @return MaterialStack
     */
    public static function stack($options = [])
    {
        return new MaterialStack($options);
    }

    /**
     * Size values
     * @see Icon::size
     */
    const SIZE_LARGE = 'lg';
    const SIZE_2X = '2x';
    const SIZE_3X = '3x';
    const SIZE_4X = '4x';
    const SIZE_5X = '5x';

    /**
     * Rotate values
     * @see Icon::rotate
     */
    const ROTATE_90 = '90';
    const ROTATE_180 = '180';
    const ROTATE_270 = '270';

    /**
     * Flip values
     * @see Icon::flip
     */
    const FLIP_HORIZONTAL = 'horizontal';
    const FLIP_VERTICAL = 'vertical';
}