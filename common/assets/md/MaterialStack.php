<?php

namespace phycom\common\assets\md;

use yii\base\InvalidArgumentException;
use yii\helpers\Html;

/**
 * Class Stack
 * @package phycom\common\assets\md
 */
class MaterialStack
{
    /** @var array  */
    private $options = [];

    /** @var MaterialIcon */
    private $icon_front;

    /** @var MaterialIcon */
    private $icon_back;

    /** @var string */
    private $tag = 'span';


    /**
     * @param array $options
     */
    public function __construct($options = [])
    {
        if (isset($options['tag'])) {
            if (is_string($options['tag'])) {
                $this->tag = $options['tag'];
            }
            unset($options['tag']);
        }

        Html::addCssClass($options, MD::$cssPrefix .'-stack');

        $this->options = $options;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * @param string|MaterialIcon $icon
     * @param array $options
     * @return MaterialStack
     */
    public function icon($icon, $options = [])
    {
        if (is_string($icon)) {
            $icon = new MaterialIcon($icon, $options);
        }

        $this->icon_front = $icon;

        return $this;
    }

    /**
     * @param string|MaterialIcon $icon
     * @param array $options
     * @return MaterialStack
     */
    public function on($icon, $options = [])
    {
        if (is_string($icon)) {
            $icon = new MaterialIcon($icon, $options);
        }

        $this->icon_back = $icon;

        return $this;
    }

    /**
     * @param string $tag
     * @return MaterialStack
     * @throws InvalidArgumentException
     */
    public function tag($tag)
    {
        if (is_string($tag)) {
            $this->tag = $tag;
        } else {
            throw new InvalidArgumentException(
                sprintf('Param tag expect type string, %s given ', gettype($tag))
            );
        }

        return $this;
    }

    /**
     * @return string
     */
    public function render()
    {
        $icon_back = $this->icon_back instanceof MaterialIcon
            ? $this->icon_back->addCssClass(MD::$cssPrefix .'-stack-2x')
            : null;

        $icon_front = $this->icon_front instanceof MaterialIcon
            ? $this->icon_front->addCssClass(MD::$cssPrefix .'-stack-1x')
            : null;

        return Html::tag('span',
            $icon_back . $icon_front,
            $this->options
        );
    }
}