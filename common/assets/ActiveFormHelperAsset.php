<?php

namespace phycom\common\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\widgets\ActiveFormAsset;

/**
 * Class ActiveFormHelperAsset
 * @package phycom\common\assets
 */
class ActiveFormHelperAsset extends AssetBundle
{
	public $sourcePath = '@phycom/common/assets/active-form-helper';
	public $js = [
	    'form.js'
	];
	public $depends = [
	    ActiveFormAsset::class,
        JqueryAsset::class
	];
}