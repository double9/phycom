<?php

namespace phycom\common\jobs;

use phycom\common\components\queue\Beanstalk;
use phycom\common\models\Message;
use yii\base\Exception;
use yii\base\BaseObject;
use yii;

/**
 * Class MessageJob
 * @package phycom\common\jobs
 */
class MessageJob extends BaseObject implements yii\queue\JobInterface
{
	public $id;

	/**
	 * @param Beanstalk $queue which pushed and is handling the job
	 * @throws Exception
	 */
	public function execute($queue)
	{
		if (!$message = Message::findOne($this->id)) {
			throw new Exception('Error sending message: message ' . $this->id . ' not found');
		}
		$message->dispatch();
	}
}