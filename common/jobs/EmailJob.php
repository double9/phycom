<?php

namespace phycom\common\jobs;

use phycom\common\components\queue\Beanstalk;
use phycom\common\modules\email\Module as EmailModule;
use yii\base\Exception;
use yii\base\BaseObject;
use yii;

/**
 * Class EmailJob
 * @package phycom\common\jobs
 */
class EmailJob extends BaseObject implements yii\queue\JobInterface
{
    public $from;
    public $to;
    public $subject;
    public $content;
    public $attachments = [];
    public $provider;

    /**
     * @param Beanstalk $queue which pushed and is handling the job
     * @throws Exception
     */
    public function execute($queue)
    {
        /**
         * @var EmailModule $email
         */
        $email = Yii::$app->getModule('email');
        $email->sendEmail($this->to, $this->subject, $this->content, $this->attachments, $this->from, $this->provider);
    }
}