<?php

namespace phycom\common\jobs;

use phycom\common\components\FileStorage;
use phycom\common\components\queue\Beanstalk;
use phycom\common\models\Invoice;

use Mpdf\Mpdf;

use yii\base\Exception;
use yii\base\BaseObject;
use yii;

/**
 * Class InvoicePdfJob
 * @package phycom\common\jobs
 */
class InvoicePdfJob extends BaseObject implements yii\queue\RetryableJobInterface
{
    public $maxAttempts = 3;
    public $template = 'invoice';
    /**
     * @var int - invoice number
     */
    public $invoiceNumber;

    public function getTtr()
    {
        return 10;
    }

    public function canRetry($attempt, $error)
    {
        return $attempt < $this->maxAttempts;
    }

    /**
     * @param Beanstalk $queue
     * @throws Exception
     * @throws \Mpdf\MpdfException
     */
    public function execute($queue)
    {
        if (!$invoice = Invoice::findOne(['number' => $this->invoiceNumber])) {
            throw new Exception('Error creating pdf. Invoice ' . $this->invoiceNumber . ' not found');
        }
        $filename = md5($invoice->number) . '.pdf';

        $template = Yii::$app->modelFactory->getMessageTemplate()::getTemplate($this->template);

        $data = file_get_contents(Yii::getAlias('@frontend/web/images/logo.png'));
        $type = 'image/png';
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        $html = $template->render(['invoice' => $invoice, 'logo' => $base64]);

        $tempDir = Yii::getAlias('@files/mpdf');
        if (!is_dir($tempDir)) {
            mkdir($tempDir);
        }

        $mPdf = new Mpdf(['tempDir' => $tempDir]);
        $mPdf->WriteHTML($html);
        $pdf = $mPdf->Output('', 'S');

        $bucket = Yii::$app->fileStorage->getBucket(FileStorage::BUCKET_INVOICES);
        $bucket->saveFileContent($filename, $pdf);

        if (!$bucket->fileExists($filename)) {
            throw new Exception('Error saving invoice ' . $filename);
        }
        $invoice->file = $filename;
        $invoice->save(false, ['file']);
    }
}