<?php

namespace phycom\common\validators;

/**
 * Class EstonianPersonalCodeValidator
 * @package phycom\common\validators
 */
class EstonianPersonalCodeValidator extends PersonalCodeValidator
{
    /**
     * @param mixed $personalCode
     * @return array|null
     * @throws \Exception
     */
    public function validateValue($personalCode)
    {
        if (!preg_match('/^[0-9]{11}$/', $personalCode)) {
            return [\Yii::t('common/error', 'Personal code is invalid'), []];
        }

        $data = @unpack("a1gender/a2year/a2month/a2day/a3index/a1check", $personalCode);
        $check = (int)$data['check'];


        $age = $this->getAge($personalCode);

        if ($age < 18 || $age > 90) {
            return [\Yii::t('common/error', 'Personal code is invalid'), []];
        }

        $sum = 0;
        $weight = [1, 2, 3, 4, 5, 6, 7, 8, 9, 1];

        for ($i = 0; $i < 10; $i++) {
            $sum += intval($personalCode[$i], 10) * $weight[$i];
        }
        $sum = $sum % 11;
        if ($sum !== 10) {
            if ($sum !== $check) {
                return [\Yii::t('common/error', 'Personal code is invalid'), []];
            } else {
                return null;
            }
        }

        $sum = 0;
        $weight = [3, 4, 5, 6, 7, 8, 9, 1, 2, 3];
        for ($i = 0; $i < 10; $i++) {
            $sum += intval($personalCode[$i], 10) * $weight[$i];
        }
        $sum = $sum % 11;
        if ($sum === 10) {
            $sum = 0;
        }
        if ($sum !== $check) {
            return [\Yii::t('common/error', 'Personal code is invalid'), []];
        } else {
            return null;
        }
    }

    /**
     * @param $personalCode
     * @return int|null
     * @throws \Exception
     */
    public function getAge($personalCode)
    {
        if ($birthday = $this->getBirthday($personalCode)) {
            return (new \DateTime())->diff($birthday)->y;
        }
        return null;
    }

    /**
     * @param $personalCode
     * @return string
     */
    public function getGender($personalCode)
    {
        $data = @unpack("a1gender/a2year/a2month/a2day/a3index/a1check", $personalCode);
        $gender = (int)$data['gender'];

        if ($gender === 4 || $gender === 6) {
            return self::GENDER_FEMALE;
        }
        if ($gender === 3 || $gender === 5) {
            return self::GENDER_MALE;
        }
        return self::GENDER_MALE;
    }

    /**
     * @param $personalCode
     * @return \DateTime
     * @throws \Exception
     */
    public function getBirthday($personalCode)
    {
        $data = @unpack("a1gender/a2year/a2month/a2day/a3index/a1check", $personalCode);
        $year = (17 + (int)(($data['gender'] + 1) / 2)) . $data['year'];
        $month = $data['month'];
        $day = $data['day'];

        return new \DateTime($year . "-" . $month . "-" . $day, new \DateTimeZone('Europe/Tallinn'));
    }

}
