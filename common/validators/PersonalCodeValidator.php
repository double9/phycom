<?php

namespace phycom\common\validators;

use yii\validators\Validator;
use Yii;

/**
 * Class PersonalCodeValidator
 * @package phycom\common\validators
 */
abstract class PersonalCodeValidator extends Validator
{
    const GENDER_MALE = 'male';
    const GENDER_FEMALE = 'female';

    public $message;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if ($this->message === null) {
            $this->message = Yii::t('common/yii', '{attribute} is invalid.');
        }
    }

    /**
     * @inheritdoc
     */
    public function validateAttribute($object, $attribute)
    {
        $value = $object->$attribute;
        $result = $this->validateValue($value);

        if (!empty($result)) {
            $this->addError($object, $attribute, $result[0], $result[1]);
        }
        return;
    }

    /**
     * @inheritdoc
     */
    public function validateValue($personalCode)
    {
        return null;
    }

    /**
     * @param string $personalCode
     * @return int
     */
    public function getAge($personalCode)
    {
        return null;
    }

    /**
     * @param string $personalCode
     * @return string
     */
    public function getGender($personalCode)
    {
        return self::GENDER_MALE;
    }

    /**
     * @param string $personalCode
     * @return null
     */
    public function getBirthday($personalCode)
    {
        return null;
    }

}
