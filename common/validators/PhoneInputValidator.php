<?php

namespace phycom\common\validators;

use phycom\common\helpers\PhoneHelper as PhoneHelper;
use phycom\common\models\Country;

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberType;
use libphonenumber\PhoneNumberUtil;

use yii\helpers\ArrayHelper;
use yii;

/**
 * Class PhoneInputValidator
 * @package phycom\common\validators
 */
class PhoneInputValidator extends \borales\extensions\phoneInput\PhoneInputValidator
{
	public function validateAttribute($model, $attribute)
	{
	    if (!$this->region) {
            if (isset($model->phoneCode)) {
                $this->setRegionByPhoneCode($model->phoneCode);
            }
            if (isset($model->phoneNumber)) {
                $this->setRegionByPhoneCode(PhoneHelper::getPhoneCode($model->phoneNumber));
            }
        }
		parent::validateAttribute($model, $attribute);
	}

	protected function validateValue($value)
	{
		$valid = false;
		$value = $string = preg_replace('/\s+/', '', $value);
		$phoneUtil = PhoneNumberUtil::getInstance();
		try {
            $region = is_array($this->region) ? $this->region[0] : $this->region;
            $phoneProto = $phoneUtil->parse($value, $region);
			if ($this->region !== null) {
				if (is_array($this->region)) {
					foreach ($this->region as $region) {
                        $phoneProto = $phoneUtil->parse($value, $region);
						if ($phoneUtil->isValidNumberForRegion($phoneProto, $region)) {
							$valid = true;
							break;
						}
					}
				} else {
					if ($phoneUtil->isValidNumberForRegion($phoneProto, $this->region)) {
						$valid = true;
					}
				}
			} else {
				if ($phoneUtil->isValidNumber($phoneProto)) {
					$valid = true;
				}
			}
            if ($this->type !== null) {
                if (PhoneNumberType::UNKNOWN != $type = $phoneUtil->getNumberType($phoneProto)) {
                    $valid = $valid && $type == $this->type;
                }
            }

		} catch (NumberParseException $e) {
			Yii::error($e->getMessage(), __METHOD__);
		}
		return $valid ? null : [$this->message, []];
	}

	protected function setRegionByPhoneCode($phoneCode = null)
	{
	    if (!$phoneCode) {
	        $this->region = Yii::$app->formatter->countryCode;
	        return;
        }
        $countries = Country::findByPhoneCode($phoneCode);
		if (!empty($countries)) {
			$this->region = ArrayHelper::getColumn($countries, 'code');
		}
	}
}