<?php

namespace phycom\common\modules\sms;

use yii\base\Exception;

/**
 * SmsException is the base class for exceptions that are related to sms module
 */
class SmsException extends Exception
{

}
