<?php

namespace phycom\common\modules\sms\controllers;

use phycom\common\models\Phone;
use phycom\common\modules\sms\Module;

use yii\helpers\Console;
use yii\helpers\Json;
use yii\console\ExitCode;
use yii\console\Controller;
use yii;

/**
 * Class ConsoleController
 * @package phycom\common\modules\sms\controllers
 *
 * @property Module $module
 */
class ConsoleController extends Controller
{
    public $verbose = true;
    public $defaultAction = 'status';

    /**
     * @inheritdoc
     */
    public function options($actionId)
    {
        return ['verbose'];
    }

    public function beforeAction($action)
    {
        // allow only console access
        if (!Yii::$app->request->isConsoleRequest) {
            throw new yii\web\NotAcceptableHttpException('Invalid action ' . $action->uniqueId);
        }
        return parent::beforeAction($action);
    }


    public function actionStatus()
    {
        /**
         * @var Module $module
         */
        $module = Yii::$app->getModule('sms');
        $this->printLn('### SMS module config ###' . PHP_EOL, Console::FG_YELLOW);
        $pad = 0;
        $properties = [];
        $reflection = new \ReflectionObject($module);
        foreach ($reflection->getProperties() as $property) {
            $name = $property->getName();
            if (strlen($name) > $pad) {
                $pad = strlen($name);
            }
            $properties[$name] = $property->getValue($module);
        }
        foreach ($properties as $key => $value) {
            $name = str_pad($key, $pad) . $this->ansiFormat(' ->  ', Console::FG_YELLOW);
            $this->printLn($name . $this->ansiFormat(json_encode($value, JSON_PRETTY_PRINT), Console::FG_PURPLE));
        }
        $this->printLn('');
        return ExitCode::OK;
    }

    /**
     * @param string $message
     * @param string $to
     * @param string|null $provider
     * @return int
     * @throws \phycom\common\modules\sms\SmsException
     */
    public function actionSend($message, $to, $provider = null)
    {
        /**
         * @var Module $module
         */
        $module = Yii::$app->getModule('sms');
        $response = $module->send($message, Phone::create($to), $provider);
        $this->printLn(Json::encode($response, JSON_PRETTY_PRINT));
        return ExitCode::OK;
    }


    /**
     * @param string $msg
     */
    protected function printLn($msg)
    {
        if ($this->verbose) {
            echo call_user_func_array([$this, 'ansiFormat'], func_get_args()) . PHP_EOL;
        }
    }
}
