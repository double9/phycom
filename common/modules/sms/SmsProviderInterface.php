<?php

namespace phycom\common\modules\sms;

use phycom\common\models\Phone;

/**
 * Interface SmsProviderInterface
 *
 * @package phycom\common\modules\sms
 */
interface SmsProviderInterface
{
    /**
     * @param string $message
     * @param Phone $to
     * @param mixed|null $from
     *
     * @return bool|mixed
     * @throws SmsException
     */
    public function sendSms(string $message, Phone $to, $from = null);
}
