<?php

namespace phycom\common\modules\delivery\methods;

use phycom\common\modules\delivery\helpers\Log;
use phycom\common\modules\delivery\models\Address;
use phycom\common\modules\delivery\models\DeliveryTimeOption;
use phycom\common\modules\delivery\models\DispatchTimeCalculator;
use phycom\common\modules\delivery\models\Shipment;
use phycom\common\modules\delivery\models\DeliveryMethodSettings;

use phycom\common\models\traits\ModuleSettingsTrait;
use phycom\common\interfaces\ModuleSettingsInterface;

use yii\base\Module as BaseModule;
use yii\helpers\Inflector;
use yii;

/**
 * Class DeliveryMethod
 * @package phycom\common\modules\delivery\methods
 *
 * @property bool $isEnabled
 * @property-read string $shipmentClassName
 * @property-read bool $isMultiCarrier
 */
abstract class DeliveryMethod extends BaseModule implements ModuleSettingsInterface, yii\base\ViewContextInterface
{
    use ModuleSettingsTrait;

	public $enabled;
	public $countries;
	public $excludedCountries;
	public $shipmentAction = false;
	public $multiService = false;
	public $code;
	public $serviceLevels;
	public $excludedServiceLevels;
	public $postageLabel = true;
    /**
     * @var bool - set this true to use separate first name last name fields
     */
    public $addressFormExtendedName = false;
    public $addressFormSuggest = false;
    public $addressFormViewPath;

    public $customDeliveryDate = false;
    public $customDeliveryTime = false;
    public $lastDeliveryBeforeClosingTime = 10;        // number of minutes when can pickup or pass delivery of an order before shop is closed

	public $defaultCarrier;
	public $defaultService;
    public $defaultPricePerKg;
    public $defaultDeliveryDays;
    public $defaultImage;
    public $defaultThumb;

    /**
     * @var array - Available time periods that client can choose to receive goods.
     */
    public $deliveryTimeOptions = [];

    /**
     * @var string|false - ISO interval how often items are dispatched
     */
    public $dispatchInterval = false;
    /**
     * @var string HH:mm the time of day when packages are forwarded to carrier
     */
    public $dispatchTime;
    /**
     * @var array
     */
    public $dispatchDays = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];
    /**
     * @var bool
     */
    public $dispatchOnHolidays = false;
    /**
     * @var string Class name of the dispatch time calculator used
     */
    public $dispatchTimeCalculator = DispatchTimeCalculator::class;


    public function init()
    {
        parent::init();
        // initialize delivery time options
        if (!empty($this->deliveryTimeOptions)) {
            foreach ($this->deliveryTimeOptions as $key => $timePeriod) {
                $this->deliveryTimeOptions[$key] = Yii::createObject($timePeriod);
            }
        }
    }

    public function getIsMultiCarrier()
    {
        return false;
    }

	public function getIsEnabled()
	{
		return (bool) $this->enabled;
	}

	public function getCode($defaultCode)
    {
        return $this->code ?: $defaultCode;
    }

	public function getId()
	{
		return $this->id;
	}

    public function getName()
    {
        return Inflector::titleize($this->getId());
    }

    public function getLabel()
    {
        return $this->getName();
    }

    public function getSettingsFormClassName()
    {
        return DeliveryMethodSettings::class;
    }

    public function getShipmentClassName()
    {
        return Shipment::class;
    }

    /**
     * @param array $items
     * @param Address $address
     * @param string $areaCode
     * @param string|\DateTime $deliveryDate
     * @param DeliveryTimeOption|string $deliveryTime
     *
     * @return Shipment
     */
    public function createShipment(array $items, Address $address = null, string $areaCode = null, $deliveryDate = null, $deliveryTime = null)
    {
        /**
         * @var Shipment $ShipmentClass
         */
        $ShipmentClass = $this->getShipmentClassName();
        $shipment = $ShipmentClass::create($items, $this->id);
        $shipment->fromVendor = Yii::$app->vendor;

        if ($deliveryDate) {
            $shipment->deliveryDate = $deliveryDate;
        }
        if ($deliveryTime) {
            $shipment->deliveryTime = $deliveryTime;
        }
        if ($address) {
            $shipment->address = $address;
        }
        if ($areaCode) {
            $shipment->setDispatchAreaCode($areaCode);
        }

        return $shipment;
    }

    public function getServiceLabel($service)
    {
        return Inflector::humanize($service, true);
    }

    public function logTrace($msg, $category = null)
    {
        $this->log($msg, 'trace', $category);
    }

    public function logInfo($msg, $category = null)
    {
        $this->log($msg, 'info', $category);
    }

    public function logWarning($msg, $category = null)
    {
        $this->log($msg, 'warning', $category);
    }

    public function logError($msg, $category = null)
    {
        $this->log($msg, 'error', $category);
    }

    protected function log($msg, $level, $category = null)
    {
        if (!$category) {
            $category = Log::DEFAULT_CATEGORY;
        }
        Log::$level('[' . strtoupper($this->id) . '] - ' . $msg, $category);
    }
}
