<?php

namespace phycom\common\modules\delivery\methods\selfPickup;

use phycom\common\assets\md\MD;
use phycom\common\helpers\Currency;
use phycom\common\modules\delivery\methods\DeliveryMethod;
use phycom\common\modules\delivery\interfaces\DeliveryMethodInterface;
use phycom\common\modules\delivery\methods\selfPickup\models\ServiceLevel;
use phycom\common\modules\delivery\methods\selfPickup\models\Shipment;
use phycom\common\modules\delivery\models\AddressForm;
use phycom\common\modules\delivery\models\DeliveryArea;
use phycom\common\modules\delivery\models\DeliveryAreaStatus;
use phycom\common\modules\delivery\models\DeliveryType;

use yii;

/**
 * Class Module
 * @package phycom\common\modules\delivery\methods\selfPickup
 */
class Module extends DeliveryMethod implements DeliveryMethodInterface
{
    public $defaultService = 'default';
    public $postageLabel = false;

	private $type;

	public function init()
	{
		parent::init();
		$this->type = DeliveryType::create(DeliveryType::DROP_OFF);
	}

	public function getName()
	{
        return Yii::t('common/modules/delivery', 'Self pickup');
	}

	public function getLabel()
	{
        return MD::icon(MD::_DIRECTIONS_WALK, ['style' =>'top: 4px;'])->size(MD::SIZE_2X) . '&nbsp;&nbsp;' . '<span style="position: relative; top: -4px;">' . $this->getName() . '</span>';
	}

	public function getType()
	{
		return $this->type;
	}

	public function getAreas($countries = null)
	{
		return DeliveryArea::find()->where(['method' => $this->id])->andWhere(['not', ['status' => [DeliveryAreaStatus::DELETED]]])->all();
	}

    public function getShipmentClassName()
    {
        return Shipment::class;
    }

	public function getPrice(DeliveryArea $area)
	{
		return Currency::toInteger($area->price);
	}

    public function hasDeliveryAddress()
    {
        return false;
    }

    public function getAddressFormModel()
    {
        return new AddressForm($this);
    }

    public function renderAddressForm(array $params = [])
    {
        return '';
    }

    public function getServiceLabel($service)
    {
        if ($service = ServiceLevel::exists($service)) {
            return $service->label;
        }
        return parent::getServiceLabel($service);
    }

    public function createPostageLabel($shippingRateId)
    {
        return null;
    }
}