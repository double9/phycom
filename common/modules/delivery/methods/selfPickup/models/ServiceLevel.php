<?php

namespace phycom\common\modules\delivery\methods\selfPickup\models;


use phycom\common\modules\delivery\models\Token;
use yii;

/**
 * Class ServiceLevel
 * @package phycom\common\modules\delivery\methods\selfPickup\models
 */
class ServiceLevel extends Token
{
    protected function getTokens()
    {
        return [
            'default' => Yii::t('common/modules/delivery', 'Self pickup'),
        ];
    }
}