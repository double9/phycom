<?php

namespace phycom\common\modules\delivery\methods\selfPickup\models;

use phycom\common\modules\delivery\methods\defaultCourier\Module;
use phycom\common\modules\delivery\models\DeliveryArea;
use phycom\common\modules\delivery\models\DeliveryRate;

/**
 * Class Shipment
 * @package phycom\common\modules\delivery\methods\selfPickup\models
 *
 * @property Module $method
 */
class Shipment extends \phycom\common\modules\delivery\models\Shipment
{
    private $_rates = [];

    public function build($deliveryService = null)
    {
        $this->selectRateByService($deliveryService);
        return $this;
    }

    public function getRates()
    {
        if (empty($this->_rates)) {
            $deliveryAreas = $this->toAddress->findAreas($this->method);
            if (empty($deliveryAreas)) {
                $this->_rates = [$this->getDefaultRate()];
            } else {
                $this->populateRates($deliveryAreas);
            }
        }
        return $this->_rates;
    }

    protected function populateRates(array $ratesData)
    {
        $rates = [];
        /**
         * @var DeliveryArea $area
         */
        foreach ($ratesData as $area) {

            // use only predefined service levels when serviceLevels param. is set
            if (is_array($this->method->serviceLevels) && !in_array($area->service, $this->method->serviceLevels)) {
                continue;
            }

            $service = ServiceLevel::exists($area->service);

            $r = new DeliveryRate($this);
            $r->referenceId = (string)$area->id;
            $r->carrier = $area->carrier;
            $r->service = $service ? $service->token : null;
            $r->serviceLabel = $service ? $service->label : null;
            $r->attributes = [DeliveryRate::A_BEST_VALUE];
            $r->amount = $area->price;
            $r->zone = $area->area_code;
            $r->messages = [];
            $r->createdAt = new \DateTime();

            $rates[] = $r;
        }
        $this->_rates = $rates;
    }
}