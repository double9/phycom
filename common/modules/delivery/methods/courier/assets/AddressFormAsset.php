<?php

namespace phycom\common\modules\delivery\methods\courier\assets;

use phycom\frontend\assets\ButtonHelperAsset;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class AddressFormAsset
 * @package phycom\common\modules\delivery\methods\courier\assets
 */
class AddressFormAsset extends AssetBundle
{
    public $sourcePath = '@phycom/common/modules/delivery/methods/courier/assets/address-form';
//    public $css = [
//        'main.css'
//    ];
    public $js = [
        'main.js'
    ];
    public $publishOptions = ['except' => ['*.less']];
    public $depends = [
        JqueryAsset::class,
        ButtonHelperAsset::class
    ];
}