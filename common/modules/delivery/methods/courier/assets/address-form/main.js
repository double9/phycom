jQuery(function ($) {

    var formId = courierAddressFormParams.formId;
    var modalId = courierAddressFormParams.modalId;
    var targetId = courierAddressFormParams.targetId;

    var $addressField = $(targetId);

    $(formId).on('click', 'button.submit:not(.disabled)', function (e) {
        var $btn = $(this),
            $form = $btn.closest('form'),
            $msgContainer = $('.address-form-messages', $form),
            action = $form.attr('action'),
            data = $form.serialize();

        ButtonHelper.loading($btn);

        ActiveFormHelper.validate($form)
            .then(function () {
                return $.post(action, data);
            })
            .done(function (data) {

                console.log(data);
                if (data.error) {
                    $msgContainer.append(data.error);
                } else {
                    $msgContainer.append(data.msg);
                    // $addressField.removeClass('hide');
                    $addressField.append($("<option></option>")
                        .attr('value', data.value)
                        .attr('data-address', data.address)
                        .attr('data-code', data.code)
                        .attr('data-price', data.price)
                        .attr('data-label', data.dataLabel)
                        .attr('data-price-display', data.priceDisplay)
                        .attr('data-multi-service', data.multiService)
                        .text(data.label));

                    $addressField.val(data.value);
                    $addressField.trigger('change');

                    window.setTimeout(function () {
                        $(modalId).modal('hide');
                        $form.find('input[type="text"]').val('');
                        $msgContainer.html('');
                        $form.yiiActiveForm('resetForm');
                    }, 1000);
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {

                // handle error
                console.log(jqXHR);

                // var title = (errMsg.charAt(0).toUpperCase() + errMsg.slice(1));
                // var alert = '<div class="alert alert-error alert-dismissible" role="alert">' +
                //     '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                //     '<strong>'+ title +':</strong> ' + jqXHR.status + ' ' + jqXHR.statusText +
                //     '</div>';
                //
                // $msgContainer.append(alert);
            })
            .always(function () {
                ButtonHelper.reset($btn);
            });

    });

});