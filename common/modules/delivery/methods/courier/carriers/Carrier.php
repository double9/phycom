<?php

namespace phycom\common\modules\delivery\methods\courier\carriers;

use phycom\common\models\traits\ModuleSettingsTrait;
use phycom\common\modules\delivery\methods\courier\Module as CourierModule;

use yii\base\Module;
use yii\helpers\Inflector;

/**
 * Class Carrier
 * @package phycom\common\modules\delivery\methods\courier\carriers
 *
 * @property-read string $label
 * @property-read CourierModule $module
 */
abstract class Carrier extends Module
{
    use ModuleSettingsTrait;

    public $enabled;
    public $enabledFromTime = '00:00';
    public $enabledToTime = '00:00';
    public $enabledWeekdays = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];
    public $deliveryTimeOptions = [];
    public $preference = 10;
    public $notify = false;
    public $phone;
    public $email;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return Inflector::titleize($this->getId());
    }

    public function getLabel()
    {
        return $this->getName();
    }


    public function getIsEnabled()
    {
        if (!$this->enabled) {
            return false;
        }
        $curTime = new \DateTime;
        if (!in_array($curTime->format('D'), $this->enabledWeekdays)) {
            return false;
        }
        if ($this->enabledFromTime === $this->enabledToTime) {
            return true;
        }
        $time = (int) $curTime->format('Gi');
        $enabledFrom = (int) str_replace(':', '', $this->enabledFromTime);
        $enabledTo = (int) str_replace(':', '', $this->enabledToTime);

        if ($time < $enabledFrom || $time >= $enabledTo) {
            return false;
        }
        return true;
    }
}