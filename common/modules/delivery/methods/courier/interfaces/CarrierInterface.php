<?php

namespace phycom\common\modules\delivery\methods\courier\interfaces;


use phycom\common\modules\delivery\models\DeliveryArea;

/**
 * Interface CarrierInterface
 * @package phycom\common\modules\delivery\methods\courier\interfaces
 */
interface CarrierInterface
{
    /**
     * @return string
     */
    public function getLabel();
}