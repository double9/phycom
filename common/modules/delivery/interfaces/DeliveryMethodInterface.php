<?php

namespace phycom\common\modules\delivery\interfaces;

use phycom\common\modules\delivery\models\Address;
use phycom\common\modules\delivery\models\DeliveryArea;
use phycom\common\modules\delivery\models\DeliveryType;
use phycom\common\modules\delivery\models\AddressForm;
use phycom\common\modules\delivery\models\PostageLabel;
use phycom\common\modules\delivery\models\Shipment;

/**
 * Interface CourierInterface
 * @package phycom\common\modules\delivery\interfaces
 */
interface DeliveryMethodInterface
{
	/**
	 * @return bool
	 */
	public function getIsEnabled();
	/**
	 * Unique delivery provider id
	 * @return string
	 */
	public function getId();

    /**
     * code shown at cart line
     *
     * @param $defaultCode
     * @return mixed
     */
    public function getCode($defaultCode);

	/**
	 * Delivery provider display name
	 * @return string
	 */
	public function getName();

	/**
	 * Delivery provider short name
	 * @return string
	 */
	public function getLabel();

	/**
	 * @param DeliveryArea $area
	 * @return mixed
	 */
	public function getPrice(DeliveryArea $area);

	/**
	 * Returns list of delivery areas supported by the courier
     * @param string|array|null $countries
	 * @return DeliveryArea[]
	 */
	public function getAreas($countries = null);

	/**
	 * @return DeliveryType
	 */
	public function getType();

    /**
     * @return bool
     */
	public function hasDeliveryAddress();

    /**
     * @return AddressForm
     */
    public function getAddressFormModel();

    /**
     * @param array $params
     * @return string
     */
	public function renderAddressForm(array $params = []);

    /**
     * @return Shipment - string Shipment model class name
     */
    public function getShipmentClassName();

    /**
     * Creates a postage label which merchant can print out and attach to the parcel
     * @param mixed $shippingRateId
     * @return PostageLabel
     */
    public function createPostageLabel($shippingRateId);


    /**
     * @param array $items
     * @param Address $address
     * @param string $areaCode
     * @param mixed $deliveryDate
     * @param mixed $deliveryTime
     *
     * @return Shipment
     */
    public function createShipment(array $items, Address $address = null, string $areaCode = null, $deliveryDate = null, $deliveryTime = null);

    /**
     * Returns human readable carrier service name
     * @param $service - carrier service token
     * @return string
     */
    public function getServiceLabel($service);
}