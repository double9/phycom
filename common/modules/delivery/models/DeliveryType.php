<?php

namespace phycom\common\modules\delivery\models;


use phycom\common\models\attributes\EnumAttribute;

/**
 * Class DeliveryType
 * @package phycom\common\modules\delivery\models
 */
class DeliveryType extends EnumAttribute
{
    /**
     * Goods can be picket up from a designated point of collection: post office or other drop off point.
     */
	const DROP_OFF = 'drop-off';
    /**
     * Goods are delivered to the customer by courier
     */
	const DELIVERY = 'delivery';
    /**
     * Both drop off and delivery service is available
     */
	const COMBINED = 'combined';
}