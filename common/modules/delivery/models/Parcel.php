<?php

namespace phycom\common\modules\delivery\models;


use yii\base\BaseObject;
use yii\base\InvalidArgumentException;

/**
 * Class Parcel
 * @package phycom\common\modules\delivery\models
 *
 * @property int $length - parcel length in millimeters
 * @property int $width - parcel width in millimeters
 * @property int $height - parcel height in millimeters
 * @property int $weight - parcel weight in grams
 */
class Parcel extends BaseObject
{
    // weight units
    const UNIT_G = 'g';
    const UNIT_KG = 'kg';


    // distance units
    const UNIT_MM = 'mm';
    const UNIT_CM = 'cm';
    const UNIT_M = 'm';
    const UNIT_KM = 'km';

    /**
     * @var  int parcel length in millimeters
     */
    protected $length;
    /**
     * @var int parcel width in millimeters
     */
    protected $width;
    /**
     * @var int parcel height in millimeters
     */
    protected $height;
    /**
     * @var int parcel weight in grams
     */
    protected $weight;


    # GETTERS

    public function getLength()
    {
        return $this->length;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    # SETTERS

    public function setLength($value, $unit = self::UNIT_MM)
    {
        $this->length = $this->convertDistanceUnits($value, $unit);
    }

    public function setWidth($value, $unit = self::UNIT_MM)
    {
        $this->width = $this->convertDistanceUnits($value, $unit);
    }

    public function setHeight($value, $unit = self::UNIT_MM)
    {
        $this->height = $this->convertDistanceUnits($value, $unit);
    }

    public function setWeight($value, $unit = self::UNIT_G)
    {
        switch ($unit) {
            case self::UNIT_G:
                $this->weight = (int) $value;
                break;
            case self::UNIT_KG:
                $this->weight = (int) round($value / 1000);
                break;
            default:
                throw new InvalidArgumentException('Invalid weight unit ' . $unit);
        }
    }

    protected function convertDistanceUnits($value, $unit)
    {
        switch ($unit) {
            case self::UNIT_MM:
                return (int) $value;
            case self::UNIT_CM:
                return (int) round($value / 10);
            case self::UNIT_M:
                return (int) round($value / 1000);
            case self::UNIT_KM:
                return (int) round($value / 1000000);
            default:
                throw new InvalidArgumentException('Invalid distance unit ' . $unit);
        }
    }
}