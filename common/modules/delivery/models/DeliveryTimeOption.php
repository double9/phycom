<?php

namespace phycom\common\modules\delivery\models;


use yii\base\BaseObject;
use yii;

/**
 * Class DeliveryTimeOption
 * @package phycom\common\modules\delivery\models
 *
 * @property-read string $key
 * @property-read string $label
 */
class DeliveryTimeOption extends BaseObject
{
    public $from;
    public $to;

    /**
     * @param string $key
     * @return DeliveryTimeOption
     */
    public static function create(string $key)
    {
        $times = explode('-', $key);

        if (count($times) !== 2) {
            throw new yii\base\InvalidArgumentException('Invalid ' . __CLASS__ . ' key ' . $key);
        }

        return new static([
            'from' => $times[0],
            'to' => $times[1]
        ]);
    }

    public function getKey()
    {
        return $this->from . '-' . $this->to;
    }

    public function getLabel()
    {
        return Yii::t('public/delivery', '{from} to {to}', ['from' => $this->from, 'to' => $this->to]);
    }

    public function __toString()
    {
        return $this->getKey();
    }
}