<?php

namespace phycom\common\modules\delivery\models;


use phycom\common\helpers\Currency;
use phycom\common\interfaces\CartItemInterface;
use phycom\common\models\ActiveRecord;
use phycom\common\models\OrderItem;
use phycom\common\models\Shop;
use phycom\common\models\Vendor;
use phycom\common\modules\delivery\DeliveryException;
use phycom\common\modules\delivery\interfaces\DeliveryMethodInterface;
use phycom\common\modules\delivery\methods\DeliveryMethod;

use yii\base\Model;
use yii\helpers\Json;
use yii;

/**
 * Class Shipment
 * @package phycom\common\modules\delivery\models
 *
 * @property DeliveryMethod|DeliveryMethodInterface $method
 * @property bool $insurance
 *
 * @property-read DeliveryMethodInterface $courier
 * @property-read string $referenceId
 * @property-read OrderItem[] $contents
 * @property-read array $parcelSize
 * @property-read array $parcelWeight
 * @property-read Address $fromAddress
 * @property-read Address $toAddress
 * @property-read DeliveryRate[] $rates
 * @property-read ShipmentLine $shipmentLine
 * @property-read array $messages
 * @property-read string $hash
 *
 * @property-write Shop $dispatchLocation
 * @property-write Address $address
 * @property-write Shop $fromShop
 * @property-write Vendor $fromVendor
 * @property-write DeliveryTimeOption|string $deliveryTime
 * @property-write string|\DateTime $deliveryDate
 */
class Shipment extends Model
{
    /**
     * shipment reference id
     * @var string
     */
    protected $referenceId;
    /**
     * @var DeliveryMethodInterface|DeliveryMethod
     */
    protected $method;
    /**
     * @var Address
     */
    protected $fromAddress;
    /**
     * @var Address
     */
    protected $toAddress;
    /**
     * @var array
     */
    protected $items = [];

    /**
     * @var DeliveryRate
     */
    protected $selectedRate;

    /**
     * @var ShipmentMessage[]
     */
    protected $messages = [];

    /**
     * @var float|true|false
     */
    protected $insurance = false;

    /**
     * @var Shop
     */
    protected $dispatchLocation;

    /**
     * @var \DateTime
     */
    protected $deliveryDate;

    /**
     * @var string
     */
    protected $deliveryTime;

    /**
     * @var DeliveryTimeOption
     */
    protected $deliveryTimeOption;

    /**
     * @var \DateTime
     */
    private $calculatedDispatchTime;

    /**
     * @param CartItemInterface[] $items
     * @param string $methodId - delivery method id
     * @return static
     */
    public static function create(array $items, $methodId)
    {
        $model = new static;
        foreach ($items as $item) {
            $model->addItem($item);
        }
        $module = Yii::$app->getModule('delivery');
        if (!$method = $module->getModule($methodId)) {
            throw new yii\base\InvalidArgumentException('Invalid delivery method ' . $methodId);
        }
        $model->method = $method;
        return $model;
    }

    public static function createByReferenceId($referenceId)
    {

    }

    /**
     * @return string - delivery method service id
     */
    public function getReferenceId()
    {
        return $this->referenceId;
    }

    public function resetRelatedCartItems()
    {
        $this->method = $this->method->getId();
        foreach ($this->contents as $item) {
            if ($item instanceof ActiveRecord) {
                foreach ($item->modelEvents() as $event) {
                    $item->off($event);
                }
                foreach ($item->relatedRecords as $recordName => $model) {
                    unset($item->$recordName);
                }
            }
        }
    }

    public function setInsurance($value)
    {
        if (is_numeric($value)) {
            $this->insurance = (float) $value;
        } else {
            $this->insurance = (bool) $value;
        }
    }

    public function getInsurance()
    {
        return $this->insurance;
    }

    public function setFromShop(Shop $shop)
    {
        $address = new Address();
        $address->attributes = $shop->address->attributes;
        $address->name = $shop->name;
        $address->company = $shop->vendor->legal_name;
        $address->phone = (string) $shop->vendor->phone;
        $address->email = (string) $shop->vendor->email;

        $this->fromAddress = $address;
    }

    public function setFromVendor(Vendor $vendor)
    {
        $address = new Address();
        $address->attributes = $vendor->address->attributes;
        $address->name = $vendor->name;
        $address->company = $vendor->legal_name;
        $address->phone = (string) $vendor->phone;
        $address->email = (string) $vendor->email;

        $this->fromAddress = $address;
    }

    public function setAddress(Address $address)
    {
        $this->toAddress = $address;
    }

    public function getFromAddress()
    {
        return $this->fromAddress;
    }

    public function getToAddress()
    {
        return $this->toAddress;
    }

    /**
     * @param \DateTime|string $date
     * @throws DeliveryException
     */
    public function setDeliveryDate($date)
    {
        if (!$date instanceof \DateTime && !is_string($date)) {
            throw new DeliveryException('Invalid delivery date ' . json_encode($date));
        }
        $deliveryDate = is_string($date) ? new \DateTime($date) : $date;
        $deliveryDate->setTime(0, 0);
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * Sets the expected delivery time of the shipment - either a string (hh:mm) or DeliveryTimeOption
     *
     * @param string|DeliveryTimeOption $deliveryTime
     * @throws DeliveryException
     */
    public function setDeliveryTime($deliveryTime)
    {
        if ($deliveryTime instanceof DeliveryTimeOption) {
            $this->deliveryTimeOption = $deliveryTime;
        } elseif (
            is_string($deliveryTime) &&
            strlen($deliveryTime) &&
            preg_match('/^\d{2}\:\d{2}/', $deliveryTime) &&
            ((int) substr($deliveryTime, 0, 2)) <= 23 &&
            ((int) substr($deliveryTime, 3, 2)) <= 59
        ) {
            $this->deliveryTime = $deliveryTime;
        } else {
            throw new DeliveryException('Invalid delivery time ' . json_encode($deliveryTime));
        }
    }


    public function setMethod(DeliveryMethodInterface $deliveryMethod)
    {
        $this->method = $deliveryMethod;
    }

    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return string - identifies unique shipment content
     */
    public function getHash()
    {
        return md5(implode(array_keys($this->items)));
    }

    /**
     * @return OrderItem[]
     */
    public function getContents()
    {
        $items = [];
        foreach ($this->items as $id => $code) {

            $product = Yii::$app->modelFactory->getProduct()::findOne(['sku' => $code]);
            $orderItem = Yii::$app->modelFactory->getOrderItem();
            $orderItem->code = $product->sku;
            $orderItem->product_id = $product->id;
            $orderItem->populateRelation('product', $product);

            if ($cartItem = Yii::$app->cart->getItem($id)) {
                $orderItem->quantity = $cartItem->getQuantity();
                $orderItem->price = $cartItem->getPrice();
                $orderItem->product_attributes = Json::encode($cartItem->getOptions());
            } else {
                $orderItem->quantity = 1;
                $orderItem->price = $product->price->getPrice();
            }
            $items[] = $orderItem;
        }
        return $items;
    }

    public function addItem(CartItemInterface $item)
    {
        if (isset($this->items[$item->getUniqueId()])) {
            throw new yii\base\InvalidArgumentException('Cart item ' . $item->getUniqueId() . ' already exists');
        }
        $this->items[$item->getUniqueId()] = $item->getCode();
    }

    public function setRate(DeliveryRate $rate)
    {
        $this->selectedRate = $rate;
    }

    public function getSelectedRate()
    {
        return $this->selectedRate;
    }


    public function setDispatchAreaCode($areaCode)
    {
        foreach ($this->method->getAreas() as $area) {
            if ($area->area_code === $areaCode) {
                if ($shop = $area->getShop()) {
                    $this->setDispatchLocation($shop);
                }
                break;
            }
        }
    }

    public function setDispatchLocation(Shop $shop)
    {
        $this->dispatchLocation = $shop;
    }

    /**
     * @return DispatchTimeCalculator
     */
    public function getDispatchTimeCalculator()
    {
        $pTime = 0;
        foreach ($this->contents as $item) {
            if ($item->product && $item->product->processingTime > $pTime) {
                $pTime = $item->product->processingTime;
            }
        }

        $DispatchTimeCalculator = $this->method->dispatchTimeCalculator;
        /**
         * @var DispatchTimeCalculator $dTime
         */
        return new $DispatchTimeCalculator($this->method, $this->dispatchLocation, $pTime);
    }

    /**
     * First available time when the parcel will be collected by the carrier
     * @return \DateTime|null
     */
    public function getDispatchTime()
    {
        if (!$this->calculatedDispatchTime) {
            $this->calculatedDispatchTime = $this->getDispatchTimeCalculator()->calculateDispatchTime();
        }
        return $this->calculatedDispatchTime;
    }

    /**
     * @return Parcel[]
     */
    public function getParcels()
    {
        $parcels = [];

        $width = 0;
        $length = 0;
        $height = 0;
        $weight = 0;

        foreach ($this->contents as $item) {

            if ($item->hasSeparateParcel) {
                list($l, $w, $h) = $item->parcelDimensions;
                $parcels[] = $this->createParcel($l, $w, $h, $item->parcelWeight ?: 0);
            } else {
                list($l, $w, $h) = $item->dimensions;
                if ($l && $l > $length) {
                    $length = $l;
                }
                if ($w && $w > $width) {
                    $width = $w;
                }
                if ($h) {
                    $height += $h;
                }
                $weight += $item->weight;
            }
        }
        if ($width > 0 && $length > 0 && $height > 0) {

            $margin = 20;
            $packageWeight = 100;

            $width += $margin;
            $length += $margin;
            $height += $margin;
            $weight += $packageWeight;

            $parcels[] = $this->createParcel($length, $width, $height, $weight);
        }
        return $parcels;
    }


    public function getMessages()
    {
        $messages = [];
        foreach ($this->messages as $msg) {
            $messages[] = (string) $msg;
        }
        return $messages;
    }

    /**
     * @param $code
     * @return bool
     */
    public function hasMessage($code)
    {
        foreach ($this->messages as $msg) {
            if ($msg->code === $code) {
                return true;
            }
        }
        return false;
    }

    public function hasErrorMessages()
    {
        foreach ($this->messages as $msg) {
            /**
             * @var ShipmentMessage $msg
             */
            if ($msg->isError) {
                return true;
            }
        }
        return false;
    }

    public function removeMessage($code)
    {
        foreach ($this->messages as $key => $msg) {
            if ($msg->code === $code) {
                unset($this->messages[$key]);
            }
        }
    }

    /**
     * @return DeliveryRate[]
     */
    public function getRates()
    {
        return [];
    }

    /**
     * @param null $service
     * @return $this
     */
    public function build($service = null)
    {
        return $this;
    }

    public function populate($shipmentData)
    {

    }

    public function selectRateByService($service)
    {
        foreach ($this->rates as $rate) {
            if ($rate->service === $service) {
                $this->selectedRate = $rate;
                return $rate;
            }
        }
        return false;
    }

    public function export($includeRates = false)
    {
        $sl = $this->getShipmentLine();
        $res = [
            'id' => $sl->id,
            'code' => $sl->code,
            'label' => $sl->label,
            'method' => $sl->method,
            'carrier' => $sl->carrier,
            'service' => $sl->service,
            'price' => Currency::toDecimal($sl->price),
        ];
        if ($includeRates) {
            $res['rates'] = [];
            $rates = $this->getRates();
            foreach ($rates as $rate) {
                $res['rates'][] = $rate->export();
            }
        }
        return $res;
    }

    /**
     * @return ShipmentLine
     */
    public function getShipmentLine()
    {
        $sl = new ShipmentLine();
        $sl->id = $this->hash;
        $sl->shipmentId = $this->referenceId;

        if ($this->selectedRate) {
            $sl->rateId = $this->selectedRate->referenceId;
            $sl->label = $this->selectedRate->label;
            $sl->price = $this->selectedRate->amount;

            $sl->method = $this->method->getId();
            $sl->carrier = $this->selectedRate->carrier;
            $sl->service = $this->selectedRate->service;
            $sl->deliveryDays = $this->selectedRate->estimatedDays;
            $sl->durationTerms = $this->selectedRate->durationTerms;
            $sl->arrivalTime = $this->selectedRate->arrivalTime;
        }
        $sl->dispatchAt = $this->getDispatchTime();
        $sl->deliveryDate = $this->deliveryDate;
        $sl->deliveryTime = $this->deliveryTimeOption ? (string) $this->deliveryTimeOption : $this->deliveryTime;
        $sl->fromAddress = $this->exportAddress($this->fromAddress);
        $sl->toAddress = $this->exportAddress($this->toAddress, true);
        $sl->areaCode = $this->toAddress->findArea($this->method)->area_code;
        return $sl;
    }

    private function exportAddress(Address $address, bool $includeId = false)
    {
        $addressData = $address->export()->toArray();
        if ($includeId) {
            $addressData['id'] = $address->id;
        }
        return Json::encode($addressData);
    }

    /**
     * Creates a default rate in case there are service connection problems
     * @throws DeliveryException
     */
    protected function getDefaultRate()
    {
        if (!$this->method->defaultCarrier) {
            throw new DeliveryException('Default carrier not found');
        }
        if (!$this->method->defaultPricePerKg) {
            throw new DeliveryException('Default price not found');
        }

        $r = new DeliveryRate($this);
        $r->referenceId = 'default';
        $r->carrier = $this->method;

        $kgAmount = $this->method->defaultPricePerKg;
        $weight = max(1, $this->getParcelWeight());
        $r->amount = Currency::toInteger($weight * $kgAmount);

        $r->attributes = [DeliveryRate::A_BEST_VALUE];
        $r->estimatedDays = $this->method->defaultDeliveryDays;
        $r->image = $this->method->defaultImage;
        $r->thumb = $this->method->defaultThumb;
        $r->messages = [];
        $r->createdAt = new \DateTime();

        return $r;
    }


    protected function createParcel($length, $width, $height, $weight)
    {
        $parcel = new Parcel();
        $parcel->length = $length;
        $parcel->width = $width;
        $parcel->height = $height;
        $parcel->weight = $weight;
        return $parcel;
    }
}
