<?php

namespace phycom\common\modules\delivery\models;


use phycom\common\modules\delivery\methods\courier\carriers\Carrier;
use phycom\common\modules\delivery\methods\courier\Module as Courier;

use yii;

/**
 * Class CarrierToken
 * @package phycom\common\modules\delivery\models
 */
class CarrierToken extends Token
{
    protected function getTokens()
    {
        $tokens = [
            'australia_post'    => 'Australia Post (also used for Startrack)',
            'aramex'            => 'Aramex',
            'asendia_us'        => 'Asendia',
            'borderguru'        => 'BorderGuru',
            'boxberry'          => 'Boxberry',
            'bring'             => 'Bring (also used for Posten Norge)',
            'canada_post'       => 'Canada Post',
            'correios_br'       => 'Correios Brazil',
            'correos_espana'    => 'Correos Espana',
            'collect_plus'      => 'CollectPlus',
            'couriersplease'    => 'CouriersPlease',
            'deutsche_post'     => 'Deutsche Post',
            'dhl_benelux'       => 'DHL Benelux',
            'dhl_germany'       => 'DHL Germany',
            'dhl_ecommerce'     => 'DHL eCommerce',
            'dhl_express'       => 'DHL Express',
            'dpd_germany'       => 'DPD Germany',
            'dpd_uk'            => 'DPD UK',
            'estafeta'          => 'Estafeta',
            'fastway_australia' => 'Fastway Australia',
            'fedex'             => 'FedEx',
            'gls_de'            => 'GLS Germany',
            'gls_fr'            => 'GLS France',
            'globegistics'      => 'Globegistics',
            'gophr'             => 'Gophr',
            'gso'               => 'GSO',
            'hermes_uk'         => 'Hermes UK',
            'hongkong_post'     => 'HongKong Post',
            'lasership'         => 'Lasership',
            'mondial_relay'     => 'Mondial Relay',
            'new_zealand_post'  => 'New Zealand Post (also used for Pace and CourierPost)',
            'newgistics'        => 'Newgistics',
            'ontrac'            => 'OnTrac',
            'orangeds'          => 'OrangeDS',
            'parcel'            => 'Parcel',
            'posti'             => 'Posti',
            'purolator'         => 'Purolator',
            'rr_donnelley'      => 'RR Donnelley',
            'russian_post'      => 'Russian Post',
            'sendle'            => 'Sendle',
            'skypostal'         => 'SkyPostal',
            'stuart'            => 'Stuart',
            'ups'               => 'UPS',
            'usps'              => 'USPS',
            'yodel'             => 'Yodel'
        ];

        /**
         * Append custom carrier modules here
         * @var Courier $courier
         */
        $courier = Yii::$app->getModule('delivery')->getModule('courier');
        foreach ($courier->modules as $id => $moduleParams) {
            /**
             * @var Carrier $carrier
             */
            $carrier = $courier->getModule($id);
            $tokens[$id] = $carrier->getLabel();
        }
        return $tokens;
    }
}