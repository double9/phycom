<?php

namespace phycom\common\modules\delivery\models;

use yii\helpers\Json;
use yii\base\BaseObject;

/**
 * Class ShipmentMessage
 * @package phycom\common\modules\delivery\models
 *
 * @property-read bool $isError
 */
class ShipmentMessage extends BaseObject
{
    const SOURCE_DELIVERY = 'Delivery';

    public $source;
    public $code;
    public $text;

    public static function create($json)
    {
        return new static(Json::decode((string)$json));
    }

    public function __toString()
    {
        return ($this->source ? $this->source . ': ' : '') . ($this->code ? '[' . $this->code . '] - ' : '') . $this->text;
    }

    public function toJson()
    {
        $refObj = new \ReflectionObject($this);
        return Json::encode($refObj->getProperties(\ReflectionProperty::IS_PUBLIC));
    }

    public function getIsError()
    {
        return strpos($this->code, 'ERR') !== false || strpos($this->code, 'err') !== false;
    }
}