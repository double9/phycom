<?php

namespace phycom\common\modules\delivery\models;


use phycom\common\models\ModuleSettingsForm;
use phycom\common\modules\delivery\Module;

use yii;

/**
 * Class Settings
 * @package phycom\common\modules\delivery\models
 *
 * @property-read Module $module
 */
class Settings extends ModuleSettingsForm
{
    public $insuranceContent;
    public $nonDeliveryOption;
    public $customsSigner;
    public $customsDescription;
    public $contentsExplanation;
    public $originCountry;


    public function rules()
    {
        return [
            [['insuranceContent','customsDescription', 'customsSigner','contentsExplanation'], 'string'],
            ['originCountry', 'string', 'min' => 2, 'max' => 2],
            ['nonDeliveryOption', 'in', 'range' => [Module::NON_DELIVERY_ABANDON, Module::NON_DELIVERY_RETURN]]
        ];
    }

    public function attributeLabels()
    {
        return [
            'insuranceContent' => Yii::t('common/delivery', 'Insured content'),
            'nonDeliveryOption' => Yii::t('common/delivery', 'Non delivery option'),
            'customsSigner' => Yii::t('common/delivery', 'Customs signer'),
            'customsDescription' => Yii::t('common/delivery', 'Item customs description'),
            'contentsExplanation' => Yii::t('common/delivery', 'Contents explanation'),
            'originCountry' => Yii::t('common/delivery', 'Origin country'),
        ];
    }

    public function attributeHints()
    {
        return [
            'insuranceContent' => Yii::t('common/delivery', 'Content type insured'),
            'customsDescription' => Yii::t('common/delivery', 'Fallback description of parcel item for customs declaration'),
            'customsSigner' => Yii::t('common/delivery', 'Name of the person who created the customs declaration and is responsible for the validity of all information provided'),
            'nonDeliveryOption' => Yii::t('common/delivery', 'Indicates how the carrier should proceed in case the shipment can\'t be delivered'),
            'contentsExplanation' => Yii::t('common/delivery', 'Explanation of the type of goods of the shipment for customs'),
            'originCountry' => Yii::t('common/delivery', 'Country of origin of the items shipped (ISO 2 country code)'),
        ];
    }
}