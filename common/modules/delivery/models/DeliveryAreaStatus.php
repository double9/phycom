<?php

namespace phycom\common\modules\delivery\models;


use phycom\common\models\attributes\EnumAttribute;

/**
 * Class DeliveryAreaStatus
 * @package phycom\common\modules\delivery\models
 */
class DeliveryAreaStatus extends EnumAttribute
{
	const ACTIVE = 'active';
	const DISABLED = 'disabled';
	const DELETED = 'deleted';
}