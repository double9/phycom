<?php

namespace phycom\common\modules\delivery\models;

use yii\base\BaseObject;

/**
 * Class Token
 * @package phycom\common\modules\delivery\models
 *
 * @property-read string $token
 * @property-read string $label
 * @property-read bool $tokenExists
 */
abstract class Token extends BaseObject implements \IteratorAggregate
{
    protected $token;
    protected $label;
    protected $tokenExists = false;

    /**
     * @param string $token
     * @return false|static
     */
    public static function exists($token)
    {
        return (new static($token))->findToken();
    }

    /**
     * @param string $label
     * @return null|static
     */
    public static function findByLabel($label)
    {
        foreach (static::all() as $model) {
            if ($model->label === $label) {
                return $model;
            }
        }
        return null;
    }

    /**
     * @param string $token
     * @return null|string
     */
    public static function label($token)
    {
        $model = (new static($token))->findToken();
        return $model ? $model->label : null;
    }

    /**
     * @return static[]
     */
    public static function all()
    {
        return (new static())->getIterator();
    }

    public function __construct($token = null, array $config = [])
    {
        $this->token = $token;
        parent::__construct($config);
    }

    /**
     * @return bool|static
     */
    public function findToken()
    {
        $tokens = $this->getTokens();
        if (isset($tokens[$this->token])) {
            $this->label = $tokens[$this->token];
            $this->tokenExists = true;
            return $this;
        }
        return $this->tokenExists = false;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getTokenExists()
    {
        return $this->tokenExists;
    }

    public function getIterator()
    {
        $tokens = [];
        foreach ($this->getTokens() as $token => $label) {
            $tokens[] = (new static($token))->findToken();
        }
        return new \ArrayIterator($tokens);
    }

    abstract protected function getTokens();
}