<?php

namespace phycom\common\modules\delivery\models;


use phycom\common\interfaces\AreaInterface;
use phycom\common\models\ActiveRecord;
use phycom\common\models\attributes\ShopStatus;
use phycom\common\models\behaviors\CurrencyBehavior;
use phycom\common\models\Country;
use phycom\common\models\Shop;
use phycom\common\modules\delivery\interfaces\DeliveryMethodInterface;
use phycom\common\modules\delivery\Module as DeliveryModule;

use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii;

/**
 * Class DeliveryArea
 * @package phycom\common\modules\delivery\models
 *
 * @property int $id
 * @property string $code
 * @property string $method
 * @property string $carrier
 * @property string $service
 * @property string $area_code
 * @property array $area
 * @property DeliveryAreaStatus $status
 * @property int $price
 * @property string $delivery_time
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property-read string $country
 * @property-read string $state
 * @property-read string $province
 * @property-read string $city
 * @property-read string $locality
 * @property-read string $district
 * @property-read string $street
 *
 * @property-read Shop $shop
 * @property-read string $name
 * @property-read string $countryName
 * @property-read DeliveryMethodInterface $methodInstance
 * @property-read string $serviceLabel
 *
 * @property-write Address $address
 */
class DeliveryArea extends ActiveRecord implements AreaInterface
{
	private $addressId;

	private $shop;

	public static function tableName()
	{
		return 'delivery_area';
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'dynamic-attribute' => [
				'attributes' => [
					'status' => DeliveryAreaStatus::class
				]
			],
			'currency' => [
				'class' => CurrencyBehavior::class,
				'attributes' => ['price']
			],
		]);
	}

	public function rules()
	{
		return [
			[['code', 'area_code', 'method', 'status'], 'required'],
            ['carrier', 'required', 'when' => function ($model) {
		           /**
                    * @var static $model
                    */
		            return $model->method !== DeliveryModule::METHOD_SELF_PICKUP;
                }
            ],
            [['price'], 'number'],
            [['created_at', 'updated_at', 'status'], 'safe'],
            [['service', 'code','carrier','delivery_time'], 'string', 'max' => 255]
		];
	}

	public function attributeLabels()
    {
        return [
            'code'          => Yii::t('common/modules/delivery', 'Code'),
            'method'        => Yii::t('common/modules/delivery', 'Method'),
            'carrier'       => Yii::t('common/modules/delivery', 'Carrier'),
            'service'       => Yii::t('common/modules/delivery', 'Service'),
            'area_code'     => Yii::t('common/modules/delivery', 'Area Code'),
            'area'          => Yii::t('common/modules/delivery', 'Area'),
            'status'        => Yii::t('common/modules/delivery', 'Status'),
            'price'         => Yii::t('common/modules/delivery', 'Price'),
            'delivery_time' => Yii::t('common/modules/delivery', 'Delivery Time'),
            'created_at'    => Yii::t('common/modules/delivery', 'Created At'),
            'updated_at'    => Yii::t('common/modules/delivery', 'Updated At'),
        ];
    }

    public function generateCode()
    {
        if (!$this->area_code) {
            throw new yii\base\InvalidCallException('area code is needed to generate code');
        }
        $hash = md5($this->method . $this->carrier . $this->service . $this->area_code);
        return strtoupper(substr($hash, 0, 6));
    }

	public function generateAreaCode()
    {
        $baseName = null;
        $prefix = '';

        if (isset($this->area['street'])) {
            $baseName = $this->area['street'];
        }
        if (!$baseName && isset($this->area['district'])) {
            $baseName = $this->area['district'];
        }
        if ($baseName && isset($this->area['city'])) {
            $prefix = Address::generateCityCode($this->area['city']) . '-';
        }
        if (!$baseName && isset($this->area['city'])) {
            $baseName = Address::generateCityCode($this->area['city']);
        }
        if (!$baseName && isset($this->area['locality'])) {
            // for EE localities
            $locality = strtolower($this->area['locality']);
            $locality = str_replace(['küla','alevik',','], ['','','-'], $locality);
            $locality = preg_replace('/[\s+]|[\d+]/', '', $locality);
            $baseName = $locality;
        }

        if (!$baseName && isset($this->area['province'])) {
            $baseName = $this->area['province'];
        }

        if (!$baseName) {
            $baseName = $this->area['country'];
            $prefix = '';
        } else if ($this->area['country'] !== (string)Yii::$app->country) {
            $prefix = $this->area['country'] . '-' . $prefix;
        }
        $baseName = preg_replace('/[\s+]|[\d+]/', '', strtoupper(Inflector::transliterate($baseName)));
        return $prefix . $baseName;
    }

	public function getMethodInstance()
	{
		return Yii::$app->getModule('delivery')->getModule($this->method);
	}

	public function setAddress(Address $address)
	{
		$this->addressId = $address->id;
	}

	public function getName() : string
	{
		$areaData = $this->area;
		$name = [];

		if (isset($areaData['street'])) {
			$name[] = $areaData['street'];
		} else if (isset($areaData['district'])) {
			$name[] = $areaData['district'];
		}

		if (isset($areaData['city'])) {
			$name[] = $areaData['city'];
		} else if (isset($areaData['locality'])) {
			$name[] = $areaData['locality'];
		}

		if (!isset($areaData['city']) && isset($areaData['province'])) {
			$name[] = $areaData['province'];
		}

		if (!empty($name)) {
			return implode(', ', $name);
		}

		if (isset($areaData['city'])) {
			return $this->city;
		}
		if (isset($areaData['locality'])) {
			return $this->locality;
		}
		if (isset($areaData['province'])) {
			return $this->province;
		}
		return Country::findOne(['code' => $areaData['country']])->name;
	}

	public function getAreaCode()
	{
		return $this->area_code;
	}

	public function getCode()
	{
		return $this->code;
	}

	public function getUniqueId()
	{
		return $this->id;
	}

	public function getLabel()
	{
		return $this->methodInstance->getLabel();
	}

	public function getServiceLabel()
    {
        return $this->service ? Inflector::humanize($this->service) : null;
    }

	public function getPrice()
	{
		return $this->price;
	}

	public function getMethod()
	{
		return $this->carrier;
	}

	public function getAddressId()
	{
		return $this->addressId;
	}

	public function getDeliveryTime()
	{
		return new \DateInterval($this->delivery_time);
	}

	public function getCountry() : string
	{
		return $this->getAreaAttribute('country');
	}

	public function getCountryName() : ?string
    {
        $countryCode = $this->getAreaAttribute('country');
        return $countryCode ? Country::findOne(['code' => $countryCode])->name : null;
    }

    public function getState() : ?string
    {
        return $this->getAreaAttribute('state');
    }

	public function getProvince() : ?string
	{
		return $this->getAreaAttribute('province');
	}

	public function getCity() : ?string
	{
		return $this->getAreaAttribute('city');
	}

	public function getLocality() : ?string
	{
		return $this->getAreaAttribute('locality');
	}

	public function getDistrict() : ?string
	{
		return $this->getAreaAttribute('district');
	}

    public function getStreet() : ?string
    {
        return $this->getAreaAttribute('street');
    }

	protected function getAreaAttribute($key)
	{
		return $this->area[$key] ?? null;
	}

    /**
     * @return Shop|null
     */
    public function getShop() : ?Shop
    {
        if (null === $this->shop) {
            $Shop = Yii::$app->modelFactory->getShop();
            $shops = $Shop::find()->where(['not', ['status' => [ShopStatus::DELETED]]])->all();
            /**
             * @var Shop[] $shops
             */
            $this->shop = false;
            foreach ($shops as $shop) {
                if ($shop->settings->deliveryArea === $this->id) {
                    $this->shop = $shop;
                    break;
                }
            }
        }
        return $this->shop ?: null;
    }

}
