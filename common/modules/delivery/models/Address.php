<?php

namespace phycom\common\modules\delivery\models;

use phycom\common\models\AddressMeta;
use phycom\common\models\Country;
use phycom\common\modules\delivery\interfaces\DeliveryMethodInterface;
use phycom\common\validators\PhoneInputValidator;

use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii;

/**
 * Class Address
 * @package phycom\common\modules\delivery\models
 * @property-read DeliveryArea $area
 * @property-read bool $isResidential
 */
class Address extends \phycom\common\models\Address
{
    public $name;
    public $firstName;
    public $lastName;
    public $company;
    public $email;
    public $phone;
    public $lat;
    public $lng;
    public $description;

	/**
	 * @var DeliveryArea
	 */
	private $_area;

	public static function create($data)
    {
        $attributes = is_array($data) ? $data : Json::decode($data);
        $model = new static();
        if (is_array($attributes)) {
            foreach ($attributes as $key => $value) {
                if (($model->hasAttribute($key) || $model->hasProperty($key)) && !empty($value)) {
                    $model->$key = $value;
                }
            }
        }
        return $model;
    }

    public function rules()
    {
        return yii\helpers\ArrayHelper::merge(parent::rules(), [
            [['name', 'firstName', 'lastName', 'company', 'email', 'phone'], 'trim'],
            [['name', 'firstName', 'lastName', 'company', 'email', 'phone'], 'string', 'max' => 255],
            ['email', 'email'],
            ['phone', PhoneInputValidator::class],
            [['lat','lng'], 'number'],
            [['description'], 'string']
        ]);
    }

    public function attributeLabels()
    {
        return yii\helpers\ArrayHelper::merge(parent::attributeLabels(), [
            'name' => Yii::t('common/modules/delivery', 'Name'),
            'firstName' => Yii::t('common/modules/delivery', 'First name'),
            'lastName' => Yii::t('common/modules/delivery', 'Last name'),
            'company' => Yii::t('common/modules/delivery', 'Company'),
            'email' => Yii::t('common/modules/delivery', 'Email'),
            'phone' => Yii::t('common/modules/delivery', 'phone'),
            'lat' => Yii::t('common/modules/delivery', 'Latitude'),
            'lng' => Yii::t('common/modules/delivery', 'Longitude'),
            'description' => Yii::t('common/modules/delivery', 'Description'),
        ]);
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->populateMeta();
    }

    public function populateMeta()
    {
        if ($this->meta) {
            $this->name = $this->meta->name ?: null;
            $this->firstName = $this->meta->first_name ?: null;
            $this->lastName = $this->meta->last_name ?: null;
            $this->company = $this->meta->company ?: null;
            $this->email = $this->meta->email ?: null;
            $this->phone = $this->meta->phone ?: null;
            $this->lng = $this->meta->lng ?: null;
            $this->lat = $this->meta->lat ?: null;
            $this->description = $this->meta->description ?: null;
        }
    }

    public function updateMeta()
    {
        if ($this->meta) {
            $meta = $this->meta;
        } else {
            $meta = new AddressMeta();
            $meta->address_id = $this->id;
            $this->populateRelation('meta', $meta);
        }
        $meta->name = $this->name;
        $meta->firstName = $this->firstName;
        $meta->lastName = $this->lastName;
        $meta->company = $this->company;
        $meta->email = $this->email;
        $meta->phone = $this->phone;
        $meta->lat = $this->lat;
        $meta->lng = $this->lng;
        $meta->description = $this->description;

        return $meta;
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (parent::save($runValidation, $attributeNames)) {
                if ($this->updateMeta()->save()) {
                    $transaction->commit();
                    return true;
                }
            }
            $transaction->rollBack();
            return false;

        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        if (empty($this->name)) {
            $names = [];
            if ($this->firstName) {
                $names[] = $this->firstName;
            }
            if ($this->lastName) {
                $names[] = $this->lastName;
            }
            return !empty($names) ? implode(' ', $names) : null;
        } else {
            return $this->name;
        }
    }

    public function getArea()
	{
		return $this->_area;
	}

	public function getIsResidential() : bool
    {
        return !$this->company;
    }

	/**
	 * @param DeliveryMethodInterface $method
	 * @return DeliveryArea|null
	 */
	public function findArea(DeliveryMethodInterface $method)
	{
	    $areas = $this->findAreas($method, 1);
	    if (!empty($areas)) {
            return $this->_area = $areas[0];
        }
        return null;
	}

    /**
     * @param DeliveryMethodInterface $method
     * @param int $limit
     * @return DeliveryArea[]
     */
    public function findAreas(DeliveryMethodInterface $method, $limit = null)
    {
        $areas = $method->getAreas($this->country);
        if (isset($areas[$this->country]) && is_array($areas[$this->country])) {
            $areas = $areas[$this->country];
        }
        $count = 0;
        $matchedAreas = $this->searchArea($areas, $count, $limit);
        if (!empty($matchedAreas)) {
            foreach ($matchedAreas as $area) {
                $area->address = $this;
            }
        }
        return $matchedAreas;
    }

    /**
     * @param array $deliveryAreas
     * @param int $count
     * @param int $limit
     * @param bool $strict
     * @return array|mixed
     */
    private function searchArea(array $deliveryAreas, &$count, &$limit, bool $strict = true)
    {
        $compareAttributes = [
            'street',
            'district',
            'city',
            'locality',
            'province',
            'country'
        ];
        $matchedAreas = [];
        foreach ($deliveryAreas as $area) {
            $match = true;
            foreach ($compareAttributes as $attribute) {
                if ($strict) {
                    if ($area->$attribute !== $this->$attribute) {
                        $match = false;
                        break;
                    }
                } else {
                    if ($area->$attribute && $area->$attribute !== $this->$attribute) {
                        $match = false;
                        break;
                    }
                }
            }
            if ($match) {
                $matchedAreas[] = $area;
                $count++;
                if ($limit && $count >= $limit) {
                    break;
                }
            }
        }
        return empty($matchedAreas) && $strict === true ? $this->searchArea($deliveryAreas, $count, $limit, false) : $matchedAreas;
    }


    public function export()
    {
        $a = parent::export();

        $a->name = $this->name;
        $a->firstName = $this->firstName;
        $a->lastName = $this->lastName;
        $a->company = $this->company;
        $a->email = $this->email;
        $a->phone = $this->phone;
        $a->lat = $this->lat;
        $a->lng = $this->lng;
        $a->info = $this->description;

        return $a;
    }

}