<?php

namespace phycom\common\modules\delivery\models;


use phycom\common\models\ModuleSettingsForm;
use phycom\common\modules\delivery\interfaces\DeliveryMethodInterface;
use phycom\common\modules\delivery\methods\DeliveryMethod;

use yii;

/**
 * Class Settings
 * @package phycom\common\modules\delivery\models
 *
 * @property-read DeliveryMethod|DeliveryMethodInterface $module
 */
class DeliveryMethodSettings extends ModuleSettingsForm
{
    public $enabled;

    public function rules()
    {
        return [
            ['enabled', 'boolean']
        ];
    }

    public function attributeLabels()
    {
        return [
            'enabled' => Yii::t('common/delivery', 'Enabled')
        ];
    }

    public function attributeHints()
    {
        return [
            'enabled' => Yii::t('common/delivery', 'Is delivery method enabled')
        ];
    }
}