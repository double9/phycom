<?php

namespace phycom\common\modules\delivery\models;

use phycom\common\helpers\Currency;
use phycom\common\helpers\Date;
use phycom\common\helpers\f;
use yii\base\BaseObject;
use yii\helpers\Inflector;
use yii;

/**
 * Class DeliveryRate
 * @package phycom\common\modules\delivery\models
 *
 * @property-read string $id
 * @property-read bool $isFastest
 * @property-read bool $isCheapest
 * @property-read bool $isBestValue
 * @property-read string $label
 * @property-read string $carrierLabel
 */
class DeliveryRate extends BaseObject
{
    const A_CHEAPEST = 'CHEAPEST';
    const A_BEST_VALUE = 'BESTVALUE';
    const A_FASTEST = 'FASTEST';

    public $referenceId;
    public $carrier;
    public $service;
    public $serviceLabel;
    public $serviceTerms;
    public $attributes = [];
    public $thumb;
    public $image;
    public $amount;
    public $estimatedDays;
    public $arrivalTime;
    public $durationTerms;
    public $messages = [];
    public $zone;
    public $createdAt;

    /**
     * @var Shipment
     */
    protected $shipment;

    public function __construct(Shipment $shipment, array $config = [])
    {
        $this->shipment = $shipment;
        parent::__construct($config);
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        if ($this->serviceLabel) {
            return $this->carrierLabel ? $this->carrierLabel . ' - ' . Inflector::humanize($this->serviceLabel) : Inflector::humanize($this->serviceLabel);
        }
        return $this->carrierLabel;
    }

    public function getCarrierLabel()
    {
        return CarrierToken::label($this->carrier);
    }

    /**
     * @return string
     */
    public function getEstimationLabel()
    {
        if (is_numeric($this->estimatedDays) && !$this->arrivalTime) {
            return f::days($this->estimatedDays);
        }
        if (is_numeric($this->estimatedDays) && $this->arrivalTime) {
            return f::days($this->estimatedDays) . ' ' . Yii::t('common/modules/delivery', 'by') . ' ' . $this->arrivalTime;
        }
        if (!$this->estimatedDays && $this->arrivalTime) {
            return $this->arrivalTime;
        }
        return Yii::t('common/modules/delivery', 'No time estimation available');
    }

    /**
     * @return bool
     */
    public function getIsBestValue()
    {
        return $this->hasAttribute(self::A_BEST_VALUE);
    }

    /**
     * @return bool
     */
    public function getIsCheapest()
    {
        return $this->hasAttribute(self::A_CHEAPEST);
    }

    /**
     * @return bool
     */
    public function getIsFastest()
    {
        return $this->hasAttribute(self::A_FASTEST);
    }
    /**
     * @return array
     */
    public function export()
    {
        $export = [];
        $properties = (new \ReflectionObject($this))->getProperties(\ReflectionProperty::IS_PUBLIC);
        foreach ($properties as $property) {
            $name = $property->getName();
            switch ($name) {
                case 'amount':
                    $value = Currency::toDecimal($this->$name);
                    break;
                case 'createdAt':
                    $value = Date::create($this->$name)->dbTimestamp;
                    break;
                default:
                    $value = $this->$name;
            }
            $export[$name] = $value;
        }
        $export['id'] = $this->shipment->hash;
        $export['shipment'] = $this->shipment->referenceId;
        $export['label'] = $this->getLabel();
        $export['amountF'] = f::currency($this->amount);
        $export['estimationLabel'] = $this->getEstimationLabel();
        return $export;
    }

    /**
     * @param string $attributeName
     * @return bool
     */
    protected function hasAttribute($attributeName)
    {
        return in_array($attributeName, $this->attributes);
    }
}