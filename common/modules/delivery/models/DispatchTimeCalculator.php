<?php

namespace phycom\common\modules\delivery\models;


use phycom\common\models\Shop;
use phycom\common\modules\delivery\methods\DeliveryMethod;

use yii\base\BaseObject;
use yii;

/**
 * Class DispatchDateCalculator
 * @package phycom\common\modules\delivery\models
 */
class DispatchTimeCalculator extends BaseObject
{
    protected $shop;
    protected $deliveryMethod;
    protected $orderProcessingTime;

    public function __construct(DeliveryMethod $deliveryMethod, Shop $shop = null, int $orderProcessingTime = 0, array $config = [])
    {
        $this->deliveryMethod = $deliveryMethod;
        $this->shop = $shop ?: Yii::$app->vendor->shop;
        $this->orderProcessingTime = $orderProcessingTime;

        parent::__construct($config);
    }

    /**
     * @param \DateTime|null $orderConfirmedAt
     * @return \DateTime|null - First available time when order is ready for dispatch
     */
    public function calculateDispatchTime(\DateTime $orderConfirmedAt = null)
    {
        return null;
    }

    /**
     * @param \DateTime|null $orderConfirmedAt
     * @return \DateTime|null - An estimated (quickest) time of arrival considering the order is shipped as soon as it is ready to dispatch
     */
    public function calculateEta(\DateTime $orderConfirmedAt = null)
    {
        return null;
    }

    /**
     * @param \DateTime|null $orderConfirmedAt
     * @param \DateInterval|null $interval
     * @return \DateTime[] - Estimated available delivery/arrival dates within selected time-frame. If no dates are found then ETA is returned even if it is outside of the  time-frame
     */
    public function calculateDeliveryDates(\DateTime $orderConfirmedAt = null, \DateInterval $interval = null)
    {
        return [];
    }
}