<?php

namespace phycom\common\modules\delivery\models;


use phycom\common\helpers\Json;
use phycom\common\models\traits\ModelTrait;
use phycom\common\models\attributes\AddressType;
use phycom\common\models\attributes\ContactAttributeStatus;
use phycom\common\models\User;

use phycom\common\modules\delivery\interfaces\DeliveryMethodInterface;
use phycom\common\modules\delivery\methods\DeliveryMethod;
use phycom\common\validators\PhoneInputValidator;

use yii\helpers\ArrayHelper;
use yii\base\Model;
use Yii;

/**
 * Class AddressForm
 * @package phycom\common\modules\delivery\models
 *
 * @property int $area
 * @property-read DeliveryArea $deliveryArea
 * @property-read DeliveryMethodInterface|DeliveryMethod $deliveryMethod
 * @property-read array $areas
 * @property-write User $user
 */
class AddressForm extends Model
{
	use ModelTrait;

	const SCENARIO_SUGGEST = 'suggest';

    public $firstName;
    public $lastName;
	public $name;
	public $company;
	public $email;
	public $phoneNumber;
	public $phone;
	public $description;

	public $address; // for a combo field where user can choose correct address from the list

	public $province;
	public $locality;
	public $city;
	public $district;
	public $street;
	public $house;
	public $room;
    public $postcode;

	protected $user;
	protected $area;
	protected $deliveryMethod;

	public function __construct(DeliveryMethodInterface $deliveryMethod, array $config = [])
	{
		$this->deliveryMethod = $deliveryMethod;
		parent::__construct($config);
	}

    public function rules()
	{
		return [
            [['name', 'firstName', 'lastName', 'company', 'email', 'phone', 'street', 'city', 'postcode'], 'trim'],
            [['name', 'firstName', 'lastName', 'company', 'email', 'phone', 'phoneNumber', 'city', 'postcode', 'house', 'room'], 'string', 'max' => 255],
            ['email', 'email'],
            ['phone', PhoneInputValidator::class],
            [['description'], 'string'],
			[['street', 'area'], 'required', 'on' => self::SCENARIO_DEFAULT],
            [['address'], 'required', 'on' => self::SCENARIO_SUGGEST]
		];
	}

	public function attributeLabels()
	{
        return [
            'name'        => Yii::t('common/modules/delivery', 'Name'),
            'firstName'   => Yii::t('common/modules/delivery', 'First name'),
            'lastName'    => Yii::t('common/modules/delivery', 'Last name'),
            'company'     => Yii::t('common/modules/delivery', 'Company'),
            'email'       => Yii::t('common/modules/delivery', 'Email'),
            'phone'       => Yii::t('common/modules/delivery', 'Phone'),
            'description' => Yii::t('common/modules/delivery', 'Description'),
            'province'    => Yii::t('common/modules/delivery', 'Province or State'),
            'locality'    => Yii::t('common/modules/delivery', 'Locality'),
            'city'        => Yii::t('common/modules/delivery', 'City'),
            'district'    => Yii::t('common/modules/delivery', 'District'),
            'street'      => Yii::t('common/modules/delivery', 'Street'),
            'house'       => Yii::t('common/modules/delivery', 'House'),
            'room'        => Yii::t('common/modules/delivery', 'Room'),
            'postcode'    => Yii::t('common/modules/delivery', 'Postcode'),
            'area'        => Yii::t('common/modules/delivery', 'Area'),
            'address'     => Yii::t('common/modules/delivery', 'Address'),
        ];
	}

    public function getDeliveryMethod()
	{
		return $this->deliveryMethod;
	}

	public function setUser(User $user)
	{
		$this->user = $user;
	}

	public function getArea()
	{
		return $this->area;
	}

	public function getAreas($countries = null)
	{
		return ArrayHelper::map($this->deliveryMethod->getAreas($countries), 'id', 'name');
	}

	public function getDeliveryArea()
	{
	    if (!$this->area) {
	        return null;
        }
	    if (is_numeric($this->area)) {
	        return DeliveryArea::findOne($this->area);
        }
        return DeliveryArea::findOne(['code' => $this->area, 'method' => $this->deliveryMethod->id]);
	}

	public function setArea(DeliveryArea $area)
    {
        $this->area = $area->id ?: $area->code;
    }

	public function save()
	{
		if ($this->validate()) {

			if ($this->user) {
				foreach ($this->user->addresses as $address) {
					if ($address->street === $this->street && $address->postcode === $this->postcode) {
						$this->addError('street', Yii::t('common/modules/delivery', 'Address already exists'));
						return false;
					}
				}
			}

			if ($this->scenario === self::SCENARIO_SUGGEST) {
			    $this->populateDataFromAddressField();
            }

			$transaction  = Yii::$app->db->beginTransaction();
			try {
				$address = $this->createAddressModel();
				if ($this->user) {
					$address->user_id = $this->user->id;
					if (!$address->save()) {
						return $this->rollback($transaction, $address->errors);
					}
				} else if (!$address->validate()) {
					return $this->rollback($transaction, $address->errors);
				}
				$transaction->commit();
				return $address;

			} catch (\Exception $e) {
				$transaction->rollBack();
				throw $e;
			}
		}
		return false;
	}

	protected function createAddressModel()
    {
        $address = new Address();
        $address->name = $this->parseName();
        $address->firstName = $this->firstName;
        $address->lastName = $this->lastName;
        $address->company = $this->company;
        $address->email = $this->email;
        $address->phone = $this->phoneNumber;
        $address->description = $this->description;
        $address->country = $this->deliveryArea->country;

        if ($this->deliveryArea->country === 'US') {
            $address->state = $this->deliveryArea->state;
        }

        $address->province = $this->deliveryArea->province ?: $this->province;
        $address->city = $this->deliveryArea->city ?: $this->city;
        $address->locality = $this->deliveryArea->locality ?: $this->locality;
        $address->district = $this->deliveryArea->district ?: $this->district;
        $address->street = $this->street;
        $address->house = $this->house;
        $address->room = $this->room;
        $address->postcode = $this->postcode;
        $address->type = AddressType::SHIPPING;
        $address->status = ContactAttributeStatus::UNVERIFIED;

        return $address;
    }

    protected function parseName()
    {
        if ($this->deliveryMethod->addressFormExtendedName) {
            $names = [];
            if ($this->firstName) {
                $names[] = $this->firstName;
            }
            if ($this->lastName) {
                $names[] = $this->lastName;
            }
            return !empty($names) ? implode(' ', $names) : null;
        } else {
            return $this->name;
        }
    }

    protected function populateDataFromAddressField()
    {
        foreach (Json::decode($this->address) as $key => $value) {
            if (property_exists($this, $key) && $value !== null) {
                $this->$key = $value;
            }
        }
    }
}
