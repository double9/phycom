<?php

namespace phycom\common\modules\delivery\models;

use phycom\common\models\traits\ModelTrait;
use phycom\common\models\attributes\ContactAttributeStatus;
use phycom\common\modules\delivery\DeliveryException;
use phycom\common\modules\delivery\interfaces\DeliveryMethodInterface;
use phycom\common\modules\delivery\methods\DeliveryMethod;
use phycom\common\modules\delivery\Module as DeliveryModule;

use yii\base\Model;
use yii;

/**
 * Class ShippingForm
 * @package phycom\common\modules\delivery\models
 *
 * @property-read DeliveryModule $module
 *
 * @property-read DeliveryMethodInterface[] $methods
 * @property-read DeliveryMethodInterface|DeliveryMethod $method
 *
 * @property-read Address $address
 * @property-read DeliveryArea $area
 *
 * @property-read Address[] $userAddresses
 * @property-read array $addressList
 */
class ShippingForm extends Model
{
	use ModelTrait;

	public $methodId;
	public $service;
	public $areaCode;
	public $addressId;
	public $insurance;
    public $deliveryDate;
    public $deliveryTime;
    public $deliveryTimeOption;

	private $_userAddresses;

	public function rules()
	{
		return [
			[['addressId', 'insurance'], 'safe'],
			[['service', 'methodId', 'areaCode', 'deliveryDate', 'deliveryTime', 'deliveryTimeOption'], 'string'],
			['areaCode', 'required', 'when' => function ($model) {
				return $model->addressId === null;
			}],
			['addressId', 'required', 'when' => function ($model) {
				return $model->areaCode === null;
			}]
		];
	}

	public function attributeLabels()
	{
		$labels = [
			'areaCode' => Yii::t('common/modules/delivery', 'Shipping area'),
			'addressId' => Yii::t('common/modules/delivery', 'Delivery address'),
            'insurance' => Yii::t('common/modules/delivery', 'Insurance'),
            'deliveryDate' => Yii::t('common/modules/delivery', 'Delivery date'),
            'deliveryTime' => Yii::t('common/modules/delivery', 'Delivery time'),
            'deliveryTimeOption' => Yii::t('common/modules/delivery', 'Delivery time'),
		];
        if ($this->methodId === DeliveryModule::METHOD_SELF_PICKUP) {
            $labels['addressId'] = Yii::t('common/modules/delivery', 'Pickup address');
            $labels['deliveryDate'] = Yii::t('common/modules/delivery', 'Pickup date');
            $labels['deliveryTime'] = Yii::t('common/modules/delivery', 'Pickup time');
            $labels['deliveryTimeOption'] = Yii::t('common/modules/delivery', 'Pickup time');
        }
		return $labels;
	}

	public function init()
	{
		parent::init();
		if (Yii::$app->session->get('order')) {
			$order = Yii::$app->modelFactory->getOrder()::findOne(Yii::$app->session->get('order'));
			$this->methodId = $order->shipment->method;
			$this->service = $order->shipment->carrier_service;
		}
	}

	public function getUserAddresses()
	{
	    if (Yii::$app->user->isGuest) {
            $this->_userAddresses = [];
        } else if ($this->_userAddresses === null) {
            /**
             * @var Address[] $addresses
             */
			$addresses = Address::find()
				->where(['user_id' => Yii::$app->user->id])
				->andWhere(['not', ['status' => ContactAttributeStatus::DELETED]])
				->all();

                $filtered = [];
                foreach ($addresses as $address) {
                    if ($address->findArea($this->method)) {
                        $filtered[] = $address;
                    }
                };

			$this->_userAddresses = $filtered;
		}
		return $this->_userAddresses;
	}

	public function getAddressList()
	{
		$addresses = [];
		foreach ($this->userAddresses as $address) {
			$addresses[$address->id] = $address->label;
		}
		return $addresses;
	}

	public function hasAddress()
    {
        return $this->addressId || $this->areaCode;
    }

	/**
	 * @return null|Address
	 */
	public function getAddress()
	{
		if (!$this->addressId) {
			return Address::create($this->getArea()->area);
		}
		if (is_numeric($this->addressId)) {
			return Address::findOne($this->addressId);
		}
		return Address::create($this->addressId);
	}

	/**
	 * @return DeliveryArea|null
	 */
	public function getArea()
	{
		if ($this->areaCode) {
			foreach ($this->method->getAreas() as $area) {
				if ($area->area_code === $this->areaCode) {
					return $area;
				}
			}
		} else if ($this->addressId) {
			$address = $this->getAddress();
			return $address->findArea($this->method);
		}
		return null;
	}

    /**
     * @return null|DeliveryMethod|DeliveryMethodInterface
     */
	public function getMethod()
	{
	    return $this->methodId ? $this->module->getModule($this->methodId) : null;
	}

	/**
	 * @return DeliveryMethod[]|DeliveryMethodInterface[]
	 */
	public function getMethods()
	{
		return $this->module->methods;
	}

	/**
	 * @return DeliveryModule|yii\base\Module
	 */
	public function getModule()
	{
		return Yii::$app->getModule('delivery');
	}

    /**
     * @param array $items
     * @return Shipment
     */
    public function createShipment(array $items)
    {
        $deliveryDate = !empty($this->deliveryDate) ? $this->deliveryDate : null;

        if (!empty($this->deliveryTimeOption)) {
            $deliveryTime = DeliveryTimeOption::create($this->deliveryTimeOption);
        } elseif (!empty($this->deliveryTime)) {
            $deliveryTime = $this->deliveryTime;
        } else {
            $deliveryTime = null;
        }

        $shipment = $this->method->createShipment($items, $this->address, $this->areaCode, $deliveryDate, $deliveryTime);

        return $shipment->build($this->service);
    }


    /**
     * @param \phycom\common\models\Shipment $shipment
     */
    public function loadShipmentData(\phycom\common\models\Shipment $shipment)
    {
        if ($shipment->delivery_date) {
            $this->deliveryDate = $shipment->delivery_date->format('Y-m-d');
        }
        if ($shipment->delivery_time) {
            $attribute = preg_match('/^\d{2}\:\d{2}\-\d{2}\:\d{2}/', $shipment->delivery_time) ? 'deliveryTimeOption' : 'deliveryTime';
            $this->$attribute = $shipment->delivery_time;
        }
    }

}