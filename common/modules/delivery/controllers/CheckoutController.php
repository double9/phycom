<?php

namespace phycom\common\modules\delivery\controllers;

use phycom\common\models\attributes\AddressField;
use phycom\common\models\ShopOpen;
use phycom\common\models\traits\WebControllerTrait;
use phycom\common\helpers\Currency;
use phycom\common\helpers\f;
use phycom\common\helpers\FlashMsg;
use phycom\common\modules\delivery\DeliveryException;
use phycom\common\modules\delivery\interfaces\DeliveryMethodInterface;
use phycom\common\modules\delivery\methods\DeliveryMethod;
use phycom\common\modules\delivery\models\Address;
use phycom\common\modules\delivery\models\DeliveryArea;
use phycom\common\modules\delivery\models\ShippingForm;
use phycom\common\modules\delivery\Module;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;
use yii;

/**
 * Class CartActionsController
 * @package phycom\common\modules\delivery\controllers
 *
 * @property-read Module $module
 */
class CheckoutController extends Controller
{
	use WebControllerTrait;

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'actions' => ['add-address', 'get-delivery-dates', 'get-delivery-times'],
						'allow' => true,
						'roles' => ['?','@']
					],
					['allow' => true, 'roles' => ['@']], // Only logged in users should be able to access
					['allow' => false]
				],
			],
			'verbs' => [
				'class' => VerbFilter::class,
				'actions' => [
					'add' => ['post'],
					'set-delivery-method' => ['post'],
                    'get-delivery-dates' => ['get'],
                    'get-delivery-times' => ['get']
				]
			]
		];
	}


	public function actionSetDeliveryMethod()
	{
        throw new DeliveryException('Deprecated call ' . $this->route);
	}

	/**
	 * If user is logged in we add the address to database and return the id.
	 * In case user is not authenticated we just validate the form and return the serialized address value instead of the id.
	 *
	 * @param string $id - delivery method id
	 * @return array
	 * @throws DeliveryException
     * @throws yii\web\NotFoundHttpException
	 */
	public function actionAddAddress($id)
	{
		$this->ajaxOnly(Response::FORMAT_JSON);
		/**
		 * @var DeliveryMethodInterface $method
		 */
		$method = $this->module->getModule($id);
		$addressForm = $method->getAddressFormModel();

		if ($addressForm->load(Yii::$app->request->post())) {
			if ($address = $addressForm->save()) {

			    $value = $address->isNewRecord ? (string)$address->export() : $address->id;
			    if (!is_numeric($value)) {
                    Yii::$app->session->set('delivery_method', $id);
			        Yii::$app->session->set('delivery_address', $value);
                }

				FlashMsg::success(Yii::t('common/modules/delivery', 'Address added successfully'));
				return $this->ajaxSuccess([
					'value' => $value,
					'label' => $address->label,
					'dataLabel' => $addressForm->deliveryArea->getLabel(),
					'price' => Currency::toDecimal($addressForm->deliveryArea->price),
					'priceDisplay' => f::currency($addressForm->deliveryArea->price),
					'code' => $addressForm->deliveryArea->getCode()
				]);
			}
			FlashMsg::error($addressForm->lastError);
			return $this->ajaxError();

		} else {
			throw new DeliveryException('Invalid call ' . $this->route);
		}
	}

    /**
     * @return mixed
     * @throws yii\web\NotFoundHttpException
     */
	public function actionGetDeliveryDates()
    {
        $this->ajaxOnly(Response::FORMAT_JSON);
        $formattedDates = [];

        $form = new ShippingForm();

        if ($form->load(Yii::$app->request->get())) {

            $shipment = $form->method->createShipment(Yii::$app->cart->getProductItems(), $form->address, $form->areaCode);
            $deliveryDates = $shipment->getDispatchTimeCalculator()->calculateDeliveryDates();
            $formattedDates = array_map(function (\DateTime $date) {
                return $date->format('Y-m-d');
            }, $deliveryDates);
        }
        return $formattedDates;
    }

    /**
     * @param string $deliveryMethod
     * @param string $areaCode
     * @param string $date
     *
     * @return mixed
     * @throws DeliveryException
     * @throws yii\web\NotFoundHttpException
     */
    public function actionGetDeliveryTimes($deliveryMethod, $areaCode, $date)
    {
        $this->ajaxOnly(Response::FORMAT_JSON);

        if (!$deliveryDate = new \DateTime($date)) {
            throw new DeliveryException('Invalid delivery date ' . $date);
        }
        if (!$deliveryArea = DeliveryArea::findOne(['method' => $deliveryMethod, 'area_code' => $areaCode])) {
            throw new DeliveryException('Delivery area not found');
        }
        if (!$shop = $deliveryArea->getShop()) {
            return ['from' => '00:00', 'to' => '00:00'];
        }

        $dayOfWeek = (int) $deliveryDate->format('N');

        if (!$shopOpen = ShopOpen::findOne(['shop_id' => $shop->id, 'day_of_week' => $dayOfWeek])) {
            return ['from' => '00:00', 'to' => '00:00'];
        }

        $from = clone $shopOpen->opened_at;
        $to = clone $shopOpen->closed_at;

        /**
         * @var DeliveryMethodInterface|DeliveryMethod $method
         */
        $method = $this->module->getModule($deliveryMethod);
        if ($method->lastDeliveryBeforeClosingTime) {
            $to->sub(new \DateInterval('PT' . $method->lastDeliveryBeforeClosingTime . 'M'));
            // set 15 min. as minimum open duration which cannot be reduced
            if ($to < $from) {
                $to = clone $from;
                $to->add(new \DateInterval('PT15M'));
            }
        }

        return ['from' => $from->format('H:i'), 'to' => $to->format('H:i')];
    }
}
