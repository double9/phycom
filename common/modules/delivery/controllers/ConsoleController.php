<?php

namespace phycom\common\modules\delivery\controllers;

use phycom\common\models\Shop;
use phycom\common\modules\delivery\models\DispatchTimeCalculator;
use phycom\common\modules\delivery\Module;
use phycom\console\controllers\BaseConsoleController;

use yii\helpers\Console;
use yii\console\ExitCode;
use yii;

/**
 * Class ConsoleController
 * @package phycom\common\modules\delivery\controllers
 */
class ConsoleController extends BaseConsoleController
{
    /**
     * @return null|yii\base\Module|Module
     */
    public function getModule()
    {
        return Yii::$app->getModule('delivery');
    }

    /**
     * @param string $deliveryMethod
     * @param int $shopId
     * @param string $orderTime
     * @param int $processingTime
     * @return int
     */
    public function actionCalculateDeliveryDates($deliveryMethod, int $shopId, $orderTime = 'now', $processingTime = 0)
    {
        $deliveryMethod = $this->getModule()->getDeliveryMethod($deliveryMethod);
        $shop = Shop::findOne($shopId);


        $DispatchTimeCalculator = $deliveryMethod->dispatchTimeCalculator;
        /**
         * @var DispatchTimeCalculator $calculator
         */
        $calculator = new $DispatchTimeCalculator($deliveryMethod, $shop, $processingTime = 0);


        $times = $calculator->calculateDeliveryDates(new \DateTime($orderTime));
        $this->printLn(json_encode($times, JSON_PRETTY_PRINT), Console::FG_YELLOW);

        return ExitCode::OK;
    }
}