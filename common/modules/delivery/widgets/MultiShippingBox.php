<?php

namespace phycom\common\modules\delivery\widgets;


use phycom\frontend\widgets\ActiveForm;
use phycom\frontend\widgets\Modal;

use phycom\common\helpers\f;
use phycom\common\modules\delivery\interfaces\DeliveryMethodInterface;
use phycom\common\modules\delivery\methods\DeliveryMethod;
use phycom\common\modules\delivery\models\DeliveryType;
use phycom\common\modules\delivery\models\ShippingForm;


use yii\helpers\Url;
use yii\helpers\Html;
use yii;

/**
 * Class MultiShippingBox
 * @package phycom\common\modules\delivery\widgets
 */
class MultiShippingBox extends DeliveryWidget
{
    public $activeClassLink = false; // set true to assign active class on a tag

	public $options = ['class' => 'shipping-box'];
	public $tabContentOptions = ['class' => 'col-md-8 col-md-offset-1'];
	public $tabItemOptions = ['role' => 'tabpanel', 'class' => 'tab-pane'];
	public $navOptions = ['class' => 'nav nav-pills', 'role' => 'tablist', 'aria-orientation' => 'vertical'];
	public $navItemOptions = ['class' => 'nav-item'];
	public $navItemLinkOptions = ['class' => 'nav-link', 'data-toggle' => 'pill'];



	public function run()
	{
		ob_get_clean();

        if (!$this->selectedMethod) {
            $this->findActiveMethod();
        }

        // tabs container start
		echo Html::beginTag('div', $this->options);
		echo Html::beginTag('div', ['class' => 'shipping-box-container row clearfix', 'data-base-url' => Url::toRoute(['/delivery/checkout'])]);

		// tabs navigation
		echo $this->renderNav();

		// tab content container
        echo Html::beginTag('div', $this->tabContentOptions);

            // tabs content
            echo Html::beginTag('div', ['id' => 'delivery-method', 'class' => 'tab-content']);
                foreach ($this->methods as $deliveryMethod) {

                    $tabOptions = $this->tabItemOptions;
                    $tabOptions['id'] = $deliveryMethod->getId();
                    if ($deliveryMethod->getId() === $this->selectedMethod) {
                        Html::addCssClass($tabOptions, 'active');
                    }
                    echo Html::beginTag('div', $tabOptions);
                    if ($deliveryMethod->hasDeliveryAddress() && $this->inlineAddressForm) {
                        echo $this->renderInlineAddressForm($deliveryMethod);
                    }
                    echo $this->renderTabContent($deliveryMethod);
                    echo Html::endTag('div');
                }
            echo Html::endTag('div');

            // render address modal if needed
            foreach ($this->methods as $deliveryMethod) {
                if ($deliveryMethod->hasDeliveryAddress() && !$this->inlineAddressForm) {
                    /**
                     * @var Modal $Modal
                     */
                    $Modal = Yii::$app->modelFactory->getClassName('Modal');
                    echo $Modal::widget([
                        'id' => 'delivery-address-modal_' . $deliveryMethod->getId(),
                        'title' => Yii::t('common/modules/delivery', 'Add new delivery address'),
                        'content' => $deliveryMethod->renderAddressForm()
                    ]);
                }
            }

        echo Html::endTag('div');
        // tabs container end
        echo Html::endTag('div');
		echo Html::endTag('div');

        $this->registerAssets();
	}


    /**
     * @return string
     */
	protected function renderNav()
	{
	    if ($this->hideMethodsNav) {
	        return '';
        }
        $navOptions = $this->navOptions;
	    Html::addCssClass($navOptions, ['delivery-method-nav']);

        $html = Html::beginTag('ul', $navOptions);
        foreach ($this->methods as $deliveryMethod) {
            $html .= $this->renderNavLink($deliveryMethod);
        }
        $html .= Html::endTag('ul');
		return Html::tag('div', $html, ['class' => 'col-md-3']);
	}

    /**
     * @param DeliveryMethodInterface $deliveryMethod
     * @return string
     */
	protected function renderNavLink(DeliveryMethodInterface $deliveryMethod)
	{
		$navItemOptions = $this->navItemOptions;
		$navItemOptions['data-id'] = $deliveryMethod->getId();
		$url = '#' . $deliveryMethod->getId();

        $linkOptions = $this->navItemLinkOptions;

        if ($deliveryMethod->getId() === $this->selectedMethod) {
            if ($this->activeClassLink) {
                Html::addCssClass($linkOptions, 'active');
            } else {
                Html::addCssClass($navItemOptions, 'active');
            }
        }

		$html = Html::beginTag('li', $navItemOptions);
		$html .= Html::a($deliveryMethod->getLabel(), $url, $linkOptions);
		$html .= Html::endTag('li');
		return $html;
	}

    /**
     * @param DeliveryMethodInterface|DeliveryMethod $deliveryMethod
     * @return string
     * @throws yii\base\UnknownPropertyException
     */
	protected function renderTabContent(DeliveryMethodInterface $deliveryMethod)
	{
        ob_start();
        ob_implicit_flush(false);

        /**
         * @var ActiveForm $ActiveForm
         */
        $ActiveForm = Yii::$app->modelFactory->getClassName('ActiveForm');

        $formId = md5('shipping-form_' . $deliveryMethod->getId());
        $this->form = $ActiveForm::begin([
            'id' => $formId,
            'options' => ['class' => 'delivery-method-form'],
            'action' => Url::toRoute(['/delivery/checkout/set-delivery-method'])
        ]);

        $model = new ShippingForm();
        $model->methodId = $deliveryMethod->getId();

        if ($this->shipment && $this->shipment->method === $deliveryMethod->getId()) {
            $model->loadShipmentData($this->shipment);
        }

        if (!$deliveryMethod->multiService) {
            $model->service = $deliveryMethod->defaultService;
        }

        $methodField = $this->form->field($model, 'methodId', ['template' => '{input}', 'options' => ['class' => 'no-margin']])->hiddenInput()->label(false);
        $serviceField = $this->form->field($model, 'service', ['template' => '{input}', 'options' => ['class' => 'no-margin']])->hiddenInput()->label(false);

        $field = $methodField . $serviceField;

        if ($deliveryMethod->getType()->is(DeliveryType::DROP_OFF)) {

            foreach (Yii::$app->cart->getDeliveryItems() as $item) {
                $model->areaCode = $item->getAreaCode();
            }

            echo Html::beginTag('div', ['class' => 'row']);
            echo Html::tag('div',
                $field .
                $this->renderDeliveryAreaDropdown($model, $deliveryMethod),
                ['class' => 'col-md-8']
            );
            echo Html::endTag('div');


            if ($deliveryMethod->customDeliveryDate) {
                echo Html::beginTag('div', ['class' => 'row']);
                echo Html::tag('div', $this->renderDeliveryDatePicker($model, $deliveryMethod, ['id' => $deliveryMethod->getId() . '-date']), ['class' => 'col-md-8']);
                if ($deliveryMethod->customDeliveryTime) {
                    echo Html::tag('div', $this->renderDeliveryTimePicker($model, $deliveryMethod, ['id' => $deliveryMethod->getId() . '-time']), ['class' => 'col-md-3']);
                }
                echo Html::endTag('div');
            }
        } else if ($deliveryMethod->getType()->is(DeliveryType::DELIVERY)) {

            foreach (Yii::$app->cart->getDeliveryItems() as $item) {
                $model->addressId = $item->getToAddressId();
            }
            $addressList = $model->addressList;
            if ($this->selectedAddress && $deliveryMethod->hasDeliveryAddress() && $deliveryMethod->getId() === $this->selectedMethod) {
                $addressList[(string)$this->selectedAddress->export()] = f::address($this->selectedAddress);
            }

            echo Html::beginTag('div', ['class' => 'row']);
                if ($this->inlineAddressForm) {
                    echo Html::tag('div', $field . Html::tag('div', null, ['class' => 'content']), ['class' => 'col-md-12']);
                } else {
                    echo Html::tag('div',
                        $field .
                        $this->renderDeliveryAddressDropdown($model, $deliveryMethod, $addressList),
                        ['class' => 'col-md-8']
                    );
                    echo Html::tag('div', $this->renderAddressButton($deliveryMethod), ['class' => 'col-md-4']);
                }
            echo Html::endTag('div');


            if ($deliveryMethod->customDeliveryDate) {
                echo Html::beginTag('div', ['class' => 'row']);
                echo Html::tag('div',
                    $this->renderDeliveryDatePicker($model, $deliveryMethod, ['id' => $deliveryMethod->getId() . '-date']),
                    ['class' => 'col-md-8']
                );
                if ($deliveryMethod->customDeliveryTime) {
                    echo Html::tag('div', $this->renderDeliveryTimePicker($model, $deliveryMethod), ['id' => $deliveryMethod->getId() . '-time', 'class' => 'col-md-3']);
                }
                echo Html::endTag('div');
            }
        }

        echo Html::tag('div', Html::tag('div', Html::tag('div', null, ['class' => 'content']), ['class' => 'col-md-10 col-md-offset-2']), ['class' => 'row']);

        $ActiveForm::end();
        return ob_get_clean();
	}

}