<?php

namespace phycom\common\modules\delivery\widgets;


use phycom\common\helpers\Json;
use phycom\common\modules\delivery\interfaces\DeliveryMethodInterface;
use phycom\common\modules\delivery\methods\DeliveryMethod;
use phycom\common\modules\delivery\models\Address;
use phycom\common\modules\delivery\assets\ShippingBoxAsset;
use phycom\common\modules\delivery\assets\ShippingBoxStyles;
use phycom\common\modules\delivery\models\ShippingForm;
use phycom\common\modules\delivery\Module;

use phycom\frontend\widgets\ActiveForm;

use phycom\common\helpers\f;
use phycom\common\helpers\Currency;
use phycom\common\models\Shipment;

use rmrevin\yii\fontawesome\FAS;

use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii;

/**
 * Class DeliveryWidget
 * @package phycom\common\modules\delivery\widgets
 */
abstract class DeliveryWidget extends Widget
{
    /**
     * @var Address
     */
    public $selectedAddress;
    /**
     * @var string
     */
    public $selectedMethod;
    /**
     * @var Shipment
     */
    public $shipment;


    public $loadStyles = true;


    public $inlineAddressForm = false;


    public $options = ['class' => 'shipping-box'];
    public $tabContentOptions = ['class' => 'col-md-12'];
    public $tabItemOptions = ['role' => 'tabpanel', 'class' => 'tab-pane'];
    public $navOptions = ['class' => 'nav nav-pills nav-stacked', 'role' => 'tablist'];
    public $navItemOptions = [];
    public $navItemLinkOptions = ['data-toggle' => 'pill'];

    public $clientOptions = [];

    /**
     * @var DeliveryMethodInterface[]|DeliveryMethod[]
     */
    public $methods = [];

    /**
     * @var ActiveForm
     */
    protected $form;

    protected $model;

    protected $hideMethodsNav = false;


    /**
     * Initializes the widget.
     * This renders the form open tag.
     */
    public function init()
    {
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
        $this->loadItems();
        if (!Yii::$app->user->isGuest && !empty(Yii::$app->user->identity->addresses)) {
            $this->inlineAddressForm = false;
        }
        ob_start();
        ob_implicit_flush(false);
    }


    public function loadItems()
    {
        /**
         * @var Module $module
         */
        $module = Yii::$app->getModule('delivery');
        $this->methods = $module->methods;
    }


    public function __toString()
    {
        return '';
    }


    protected function registerAssets()
    {
        $id = $this->options['id'];
        $view = $this->getView();
        ShippingBoxAsset::register($view);

        if ($this->loadStyles) {
            ShippingBoxStyles::register($view);
        }

        $options = [];
        foreach ($this->methods as $deliveryMethod) {
            $options[$deliveryMethod->getId()] = [
                'customDeliveryDate' => $deliveryMethod->customDeliveryDate,
                'customDeliveryTime' => $deliveryMethod->customDeliveryTime && empty($deliveryMethod->deliveryTimeOptions),
                'customDeliveryTimeOption' => $deliveryMethod->customDeliveryTime && !empty($deliveryMethod->deliveryTimeOptions)
            ];
        }
        $options = Json::encode(ArrayHelper::merge($options, $this->clientOptions));
        $view->registerJs("shippingBox.init('#$id', $options);");
    }


    protected function findActiveMethod()
    {
        if ($this->shipment) {
            $this->selectedMethod = $this->shipment->method;
        } elseif ($this->selectedAddress && !$this->selectedMethod) {
            foreach ($this->methods as $deliveryMethod) {
                if ($deliveryMethod->hasDeliveryAddress()) {
                    $this->selectedMethod = $deliveryMethod->getId();
                    break;
                }
            }
        }
        if (!$this->selectedMethod && !empty($this->methods)) {
            $this->selectedMethod = $this->methods[0]->getId();
        }
    }

    /**
     * @param DeliveryMethodInterface|DeliveryMethod $deliveryMethod
     * @return string
     */
    protected function renderInlineAddressForm(DeliveryMethodInterface $deliveryMethod)
    {
        $model = $deliveryMethod->getAddressFormModel();
        if ($this->selectedAddress) {
            $model->name = $this->selectedAddress->name;
            $model->firstName = $this->selectedAddress->firstName;
            $model->lastName = $this->selectedAddress->lastName;
            $model->email = $this->selectedAddress->email;
            $model->phone = PhoneHelper::getNationalPhoneNumber($this->selectedAddress->phone);
            $model->phoneNumber = $this->selectedAddress->phone;
            if ($model->hasProperty('country')) {
                $model->country = $this->selectedAddress->country;
            }
            $model->province = $this->selectedAddress->province;
            $model->city = $this->selectedAddress->city;
            $model->street = $this->selectedAddress->street;
            $model->postcode = $this->selectedAddress->postcode;

            if (!$this->selectedAddress->area) {
                $this->selectedAddress->findArea($deliveryMethod);
            }
            $model->area = $this->selectedAddress->area;
        }

        $html = Html::beginTag('div', ['class' => 'row clearfix', 'style' => 'margin-bottom: 20px;']);
        $html .= Html::tag('div', $deliveryMethod->renderAddressForm(['model' => $model]), ['class' => 'col-md-12']);
        $html .= Html::endTag('div');
        return $html;
    }


    /**
     * @param ShippingForm $model
     * @param DeliveryMethodInterface|DeliveryMethod $deliveryMethod
     * @return \phycom\frontend\widgets\ActiveField
     */
    protected function renderDeliveryAreaDropdown(ShippingForm $model, DeliveryMethodInterface $deliveryMethod)
    {
        $areaList = [];
        $options = [];
        foreach ($deliveryMethod->getAreas() as $area) {
            $areaList[$area->area_code] = $area->name;
            $options[$area->area_code] = [
                'data-code' => $area->getCode(),
                'data-price' => Currency::toDecimal($area->price),
                'data-label' => $area->getLabel(),
                'data-price-display' => f::currency($area->getPrice()),
                'data-method' => $area->carrier
            ];
        }

        if ($this->selectedAddress && $deliveryMethod->getId() === $this->selectedMethod && $this->selectedAddress->findArea($deliveryMethod)) {
            $model->areaCode = $this->selectedAddress->getArea()->area_code;
        }

        return $this->form->field($model, 'areaCode')->dropDownList(
            $areaList,
            [
                'options' => $options,
                'class' => 'delivery-address form-control',
            ]
        )->label(false);
    }

    /**
     * @param ShippingForm $model
     * @param DeliveryMethodInterface|DeliveryMethod $deliveryMethod
     * @param array $addressList
     * @return \phycom\frontend\widgets\ActiveField
     */
    protected function renderDeliveryAddressDropdown(ShippingForm $model, DeliveryMethodInterface $deliveryMethod, array &$addressList)
    {
        $shipmentUrl = $deliveryMethod->shipmentAction ? Url::toRoute($deliveryMethod->shipmentAction) : '';
        $options = [];

        foreach ($model->userAddresses as $address) {
            $options[$address->id] = [
                'data-address' => $address->export()->toJson(),
                'data-code' => $address->findArea($deliveryMethod)->getCode(),
                'data-price' => Currency::toDecimal($address->area->price),
                'data-label' => $address->area->getLabel(),
                'data-price-display' => f::currency($address->area->getPrice()),
                'data-method' => $address->area->carrier,
                'data-multi-service' => $deliveryMethod->multiService ? 1 : 0
            ];
        }

        if ($this->selectedAddress && $deliveryMethod->hasDeliveryAddress() && $deliveryMethod->getId() === $this->selectedMethod) {
            $address = $this->selectedAddress;
            $value = (string)$address->export();
            $options[$value] = [
                'data-address' => $address->export()->toJson(),
                'data-code' => $address->findArea($deliveryMethod)->getCode(),
                'data-price' => Currency::toDecimal($address->area->price),
                'data-label' => $address->area->getLabel(),
                'data-price-display' => f::currency($address->area->getPrice()),
                'data-method' => $address->area->carrier,
                'data-multi-service' => $deliveryMethod->multiService ? 1 : 0
            ];
            $model->addressId = $value;
        }


        return $this->form->field($model, 'addressId')->dropDownList(
            $addressList,
            [
                'options' => $options,
                'class' => 'delivery-address form-control' . (empty($addressList) ? ' hide' : ''),
                'data-url' => $shipmentUrl,
                'prompt' => (!empty($addressList) ? Yii::t('common/modules/delivery', 'Select delivery address') : null)
            ]
        )->label(false);
    }


    /**
     * @param DeliveryMethodInterface|DeliveryMethod $deliveryMethod
     * @param array $options
     * @return string
     */
    protected function renderAddressButton(DeliveryMethodInterface $deliveryMethod, array $options = [])
    {
        if ($deliveryMethod->hasDeliveryAddress()) {

            $defaultOptions = [
                'class' => 'btn btn-flat btn-primary add-address',
                'data-toggle' => 'modal',
                'data-target' => '#delivery-address-modal_' . $deliveryMethod->getId(),
                'style' => 'margin-bottom: 10px;'
            ];

            if (isset($options['class'])) {
                Html::addCssClass($defaultOptions, $options['class']);
                unset($options['class']);
            }

            $btn = Html::button(
                FAS::i(FAS::_MAP_MARKER_ALT, ['tag' => 'span']) . '&nbsp;&nbsp;' . Yii::t('common/modules/delivery', 'Add new delivery address'),
                ArrayHelper::merge($defaultOptions, $options)
            );

            return $btn;
        }
        return '';
    }

    /**
     * @param ShippingForm $model
     * @param DeliveryMethodInterface|DeliveryMethod $deliveryMethod
     * @param array $options
     * @return \phycom\frontend\widgets\ActiveField
     */
    protected function renderDeliveryDatePicker(ShippingForm $model, DeliveryMethodInterface $deliveryMethod, array $options = [])
    {
        return $this->form->field($model, 'deliveryDate')->datePicker(ArrayHelper::merge([
            'inline' => true,
            'clientOptions' => [
                'minDate' => (new \DateTime('today'))->format('Y-m-d'),
                'defaultDate' => (new \DateTime('today'))->format('Y-m-d')
            ]
        ], $options));
    }

    /**
     * @param ShippingForm $model
     * @param DeliveryMethodInterface|DeliveryMethod $deliveryMethod
     * @param array $options
     * @return \phycom\frontend\widgets\ActiveField
     */
    protected function renderDeliveryTimePicker(ShippingForm $model, DeliveryMethodInterface $deliveryMethod, array $options = [])
    {
        /**
         * @var DeliveryMethod $deliveryMethod
         */
        if (!empty($deliveryMethod->deliveryTimeOptions)) {

            $options = ArrayHelper::map($deliveryMethod->deliveryTimeOptions, 'key', 'label');
            return $this->form->field($model, 'deliveryTimeOption')->dropDownList($options);

        } else {

            $dateTime = $model->deliveryDate ? new \DateTime($model->deliveryDate . ($model->deliveryTime ? ' ' . $model->deliveryTime . ':00' : ' 00:00:00')) : null;
            $date = $dateTime ? $dateTime->format('Y-m-d H:i:s') : null;

            $roundToQuarterHour = function ($timeString) {
                $minutes = date('i', strtotime($timeString));
                return $minutes + ($minutes % 15);
            };
            $defaultDate = new \DateTime();
            $minutes = $roundToQuarterHour($defaultDate->format('Y-m-d H:i:s'));
            $defaultDate->setTime((int)$defaultDate->format('H'), $minutes);
            $defaultDate->add(new \DateInterval('PT15M'));

            return $this->form->field($model, 'deliveryTime')->timePicker(ArrayHelper::merge([
                'inline' => true,
                'clientOptions' => [
                    'stepping' => 15,
                    'date' => $date,
                    'defaultDate' => $defaultDate->format('Y-m-d H:i:s'),
                    'useCurrent' => false
                ]
            ], $options));
        }
    }

}