<?php

namespace phycom\common\modules\delivery\widgets;


use phycom\frontend\widgets\ActiveForm;
use phycom\frontend\widgets\Modal;

use phycom\common\helpers\PhoneHelper as PhoneHelper;
use phycom\common\helpers\f;
use phycom\common\modules\delivery\interfaces\DeliveryMethodInterface;
use phycom\common\modules\delivery\methods\DeliveryMethod;
use phycom\common\modules\delivery\models\DeliveryType;
use phycom\common\modules\delivery\models\ShippingForm;

use rmrevin\yii\fontawesome\FAS;

use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii;

/**
 * Class ShippingBox
 * @package phycom\common\modules\delivery\widgets
 */
class ShippingBox extends DeliveryWidget
{
	public $options = ['class' => 'shipping-box'];
	public $tabContentOptions = ['class' => 'col-md-12'];
	public $tabItemOptions = ['role' => 'tabpanel', 'class' => 'tab-pane'];
	public $navOptions = ['class' => 'nav nav-pills nav-stacked', 'role' => 'tablist'];
	public $navItemOptions = [];
	public $navItemLinkOptions = ['data-toggle' => 'pill'];
	public $hideMethodsNavThreshold = 1;

	public $inlineAddressForm = true;


	public function run()
	{
	    if (count($this->methods) <= $this->hideMethodsNavThreshold) {
	        $this->hideMethodsNav = true;
        }
		ob_get_clean();

        if (!$this->selectedMethod) {
            $this->findActiveMethod();
        }

	    echo $this->renderTabsStart();
		echo Html::beginTag('div', ['class' => 'shipping-box-container row clearfix']);
		echo $this->renderNav();
		echo $this->renderContentStart();
		echo Html::beginTag('div', ['id' => 'delivery-method', 'class' => 'tab-content']);

		foreach ($this->methods as $deliveryMethod) {
            if ($deliveryMethod->hasDeliveryAddress() && $this->inlineAddressForm) {
                echo $this->renderInlineAddressForm($deliveryMethod);
            }
			echo $this->renderTabContent($deliveryMethod);
		}
		echo Html::endTag('div');

		foreach ($this->methods as $deliveryMethod) {
		    if ($deliveryMethod->hasDeliveryAddress() && !$this->inlineAddressForm) {
                /**
                 * @var Modal $Modal
                 */
		        $Modal = Yii::$app->modelFactory->getClassName('Modal');
                echo $Modal::widget([
                    'id'      => 'delivery-address-modal_' . $deliveryMethod->getId(),
                    'title'   => Yii::t('common/modules/delivery', 'Add new delivery address'),
                    'content' => $deliveryMethod->renderAddressForm()
                ]);
            }
		}

		echo $this->renderContentEnd();
		echo Html::endTag('div');
		echo $this->renderTabsEnd();

        $this->registerAssets();
	}


	protected function renderInlineAddressForm(DeliveryMethodInterface $deliveryMethod)
    {
        $model = $deliveryMethod->getAddressFormModel();
        if ($this->selectedAddress) {
            $model->name = $this->selectedAddress->name;
            $model->firstName = $this->selectedAddress->firstName;
            $model->lastName = $this->selectedAddress->lastName;
            $model->email = $this->selectedAddress->email;
            $model->phone = PhoneHelper::getNationalPhoneNumber($this->selectedAddress->phone);
            $model->phoneNumber = $this->selectedAddress->phone;
            if ($model->hasProperty('country')) {
                $model->country = $this->selectedAddress->country;
            }
            $model->province = $this->selectedAddress->province;
            $model->city = $this->selectedAddress->city;
            $model->street = $this->selectedAddress->street;
            $model->postcode = $this->selectedAddress->postcode;

            if (!$this->selectedAddress->area) {
                $this->selectedAddress->findArea($deliveryMethod);
            }
            $model->area = $this->selectedAddress->area;
        }

        $html = Html::beginTag('div', ['class' => 'row']);
        $html .= Html::tag('div', $deliveryMethod->renderAddressForm(['submitBtnPosition' => 'left', 'model' => $model]), ['class' => 'col-md-8']);
        $html .= Html::endTag('div');
        return $html;
    }


	protected function renderNav()
	{
	    if ($this->hideMethodsNav) {
	        return '';
        }

        $html = Html::beginTag('ul', $this->navOptions);
        foreach ($this->methods as $deliveryMethod) {
            $html .= $this->renderNavLink($deliveryMethod);
        }
        $html .= Html::endTag('ul');
		return Html::tag('div', $html, ['class' => 'col-md-3']);
	}

	protected function renderNavLink(DeliveryMethodInterface $deliveryMethod)
	{
		$navItemOptions = $this->navItemOptions;
		$navItemOptions['data-id'] = $deliveryMethod->getId();
		$url = '#' . $deliveryMethod->getId();

		if ($deliveryMethod->getId() === $this->selectedMethod) {
			Html::addCssClass($navItemOptions, 'active');
		}

		$html = Html::beginTag('li', $navItemOptions);
		$html .= Html::a($deliveryMethod->getLabel(), $url, $this->navItemLinkOptions);
		$html .= Html::endTag('li');
		return $html;
	}

	protected function renderTabContent(DeliveryMethodInterface $deliveryMethod)
	{
		$tabOptions = $this->tabItemOptions;
		$tabOptions['id'] = $deliveryMethod->getId();
		if ($deliveryMethod->getId() === $this->selectedMethod) {
			Html::addCssClass($tabOptions, 'active');
		}
		$content = $this->getContent($deliveryMethod);
		return Html::tag('div', $content, $tabOptions);
	}

	protected function renderContentStart()
	{
		return Html::beginTag('div', $this->tabContentOptions);
	}

	protected function renderContentEnd()
	{
		return Html::endTag('div');
	}

	protected function renderTabsStart()
	{
		return Html::beginTag('div', $this->options);
	}

	protected function renderTabsEnd()
	{
		return Html::endTag('div');
	}

    /**
     * @param DeliveryMethodInterface|DeliveryMethod $deliveryMethod
     * @return string
     * @throws yii\base\UnknownPropertyException
     */
	protected function getContent(DeliveryMethodInterface $deliveryMethod)
	{
		ob_start();
		ob_implicit_flush(false);

        /**
         * @var ActiveForm $ActiveForm
         */
		$ActiveForm = Yii::$app->modelFactory->getClassName('ActiveForm');

		$formId = md5('shipping-form_' . $deliveryMethod->getId());
		$this->form = $ActiveForm::begin([
			'id' => $formId,
			'options' => ['class' => 'delivery-method-form'],
			'action' => Url::toRoute(['/delivery/checkout/set-delivery-method'])
		]);

		$model = new ShippingForm();
		$model->methodId = $deliveryMethod->getId();

		if ($this->shipment && $this->shipment->method === $deliveryMethod->getId()) {
            $model->loadShipmentData($this->shipment);
        }

		if (!$deliveryMethod->multiService) {
            $model->service = $deliveryMethod->defaultService;
        }
		$methodField = $this->form->field($model, 'methodId', ['template' => '{input}', 'options' => ['class' => 'no-margin']])->hiddenInput()->label(false);
        $serviceField = $this->form->field($model, 'service', ['template' => '{input}', 'options' => ['class' => 'no-margin']])->hiddenInput()->label(false);

		$field = $methodField . $serviceField;

        echo Html::beginTag('div', ['class' => 'row']);


		if ($deliveryMethod->getType()->is(DeliveryType::DROP_OFF)) {

			foreach (Yii::$app->cart->getDeliveryItems() as $item) {
				$model->areaCode = $item->getAreaCode();
			}

			$field .= $this->renderDeliveryAreaDropdown($model, $deliveryMethod);
            echo Html::tag('div', $field . Html::tag('div', null, ['class' => 'content']), ['class' => 'col-md-12']);

		} else if ($deliveryMethod->getType()->is(DeliveryType::DELIVERY)) {

			foreach (Yii::$app->cart->getDeliveryItems() as $item) {
				$model->addressId = $item->getToAddressId();
			}
            $addressList = $model->addressList;
            if ($this->selectedAddress && $deliveryMethod->hasDeliveryAddress() && $deliveryMethod->getId() === $this->selectedMethod) {
                $addressList[(string)$this->selectedAddress->export()] = f::address($this->selectedAddress);
            }

			$field .= $this->renderDeliveryAddressDropdown($model, $deliveryMethod, $addressList);
			$info = '';

			if (!Yii::$app->user->isGuest && !empty($addressList)) {
			    $info .= '<br><p style="margin-bottom: 10px;">'.Yii::t('common/modules/delivery', 'Or select existing address from the drop-down').'</p>';
            }

            if ($this->inlineAddressForm) {
			    echo Html::tag('div', $field . Html::tag('div', null, ['class' => 'content']), ['class' => 'col-md-12']);
            } else {
                echo Html::tag('div', $this->renderAddressButton($deliveryMethod) . $info, ['class' => 'col-md-4']);
                echo Html::tag('div', $field . Html::tag('div', null, ['class' => 'content']), ['class' => 'col-md-8']);
            }
		}

        echo Html::endTag('div');
        $ActiveForm::end();

		return ob_get_clean(); //$this->view->render('');
	}



    /**
     * @param DeliveryMethodInterface $deliveryMethod
     * @param array $options
     * @return string
     */
	protected function renderAddressButton(DeliveryMethodInterface $deliveryMethod, array $options = [])
    {
        if ($deliveryMethod->hasDeliveryAddress()) {

            $defaultOptions = [
                'class' => 'btn btn-flat btn-primary add-address',
                'data-toggle' => 'modal',
                'data-target' => '#delivery-address-modal_' . $deliveryMethod->getId(),
                'style' => 'margin-bottom: 10px;'
            ];

            if (isset($options['class'])) {
                Html::addCssClass($defaultOptions, $options['class']);
                unset($options['class']);
            }

            $btn = Html::button(
                FAS::i(FAS::_MAP_MARKER_ALT, ['tag' => 'span']) . '&nbsp;&nbsp;' . Yii::t('common/modules/delivery', 'Add new delivery address'),
                ArrayHelper::merge($defaultOptions, $options)
            );

            return $btn;
        }
        return '';
    }

	protected function renderPriceDisplay()
	{
//		$html = Html::beginTag('div', ['class' => 'row']);
		$html = Html::beginTag('div', ['class' => 'col-md-12 clearfix']);
		$html .= Html::tag('div', '12.00 €', ['class' => 'shipping-price pull-right']);
		$html .= Html::endTag('div');
		$html .= '<br />';
//		$html .= Html::endTag('div');

		return $html;
	}
}