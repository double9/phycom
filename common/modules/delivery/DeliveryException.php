<?php

namespace phycom\common\modules\delivery;

use phycom\common\modules\delivery\helpers\Log;
use yii\base\Exception;

/**
 * Class DeliveryException
 * @package phycom\common\modules\delivery
 */
class DeliveryException extends Exception
{
	public function __construct($message = "", $code = 0, \Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
		Log::error($this->getMessage());
	}
}