<?php
namespace phycom\common\modules\delivery;

use phycom\common\interfaces\CommerceComponentInterface;
use phycom\common\modules\delivery\methods\DeliveryMethod;
use phycom\common\modules\delivery\models\Address;
use phycom\common\modules\delivery\interfaces\DeliveryMethodInterface;
use phycom\common\modules\delivery\models\Settings;

use phycom\common\interfaces\ModuleSettingsInterface;
use phycom\common\models\traits\ModuleSettingsTrait;

use yii\helpers\ArrayHelper;
use yii\base\Module as BaseModule;
use yii;

/**
 * Class Module
 *
 * @package phycom\common\modules\delivery
 * @property DeliveryMethodInterface[] $methods - enabled carrier modules
 * @property DeliveryMethodInterface[] $allMethods - all carrier modules
 * @property-read array $shippingBoxConfig
 */
class Module extends BaseModule implements ModuleSettingsInterface, CommerceComponentInterface
{
    use ModuleSettingsTrait;

	const METHOD_SELF_PICKUP = 'self-pickup';
	const METHOD_COURIER = 'courier';


	const NON_DELIVERY_ABANDON = 'ABANDON';
	const NON_DELIVERY_RETURN = 'RETURN';

    /**
     * @var bool
     */
	public bool $enabled = true;
    /**
     * @var string
     */
	public $insuranceContent = 'merchandise';
    /**
     * @var string
     */
	public $customsDescription;
    /**
     * Name of the person who created the customs declaration and is responsible for the validity of all information provided.
     * @var string
     */
	public $customsSigner;
    /**
     * Indicates how the carrier should proceed in case the shipment can't be delivered.
     * @var string
     */
	public $nonDeliveryOption = self::NON_DELIVERY_RETURN;
    /**
     * Explanation of the type of goods of the shipment.
     * @var string
     */
	public $contentsExplanation;
    /**
     * @var string - ISO 3166-1-alpha-2 code (ISO 2 country code) - Country of origin of the items shipped
     */
	public $originCountry;


	public function init()
	{
		parent::init();
		$subModules = [
			self::METHOD_SELF_PICKUP => [
				'class' => methods\selfPickup\Module::class,
                'enabled' => false,
			],
			self::METHOD_COURIER     => [
				'class' => methods\courier\Module::class,
                'enabled' => false,
			]
		];

		foreach ($this->modules as $id => $moduleParams) {
            $params = isset($subModules[$id]) ? ArrayHelper::merge($subModules[$id], $moduleParams) : $moduleParams;
            $this->setModule($id, $params);
        }
	}

	public function isEnabled()
    {
        return $this->enabled;
    }

    public function getSettingsFormClassName()
    {
        return Settings::class;
    }

	/**
	 * @return DeliveryMethod[]|DeliveryMethodInterface[]
	 */
	public function getMethods()
	{
		$enabled = [];

		foreach ($this->modules as $id => $moduleParams) {
			$module = $this->getModule($id);
			/**
			 * @var DeliveryMethodInterface $module
			 */
			if ($module->getIsEnabled()) {
				$enabled[] = $module;
			}
		}
		return $enabled;
	}

	/**
	 * @return DeliveryMethod[]|DeliveryMethodInterface[]
	 */
	public function getAllMethods()
	{
		$couriers = [];
		foreach ($this->modules as $id => $moduleParams) {
			$couriers[] = $this->getModule($id);
		}
		return $couriers;
	}

    /**
     * @param string $id
     * @return null|BaseModule|DeliveryMethod|DeliveryMethodInterface
     */
	public function getDeliveryMethod($id)
    {
        return $this->getModule($id);
    }

    /**
     * @param array $config
     * @return array
     */
    public function getShippingBoxConfig(array $config = [])
    {
        if (Yii::$app->session->get('order')) {
            $order = Yii::$app->modelFactory->getOrder()::findOne(Yii::$app->session->get('order'));
            if ($order->shipment) {
                $config['shipment'] = $order->shipment;
                if (!$order->shipment->isDeliveryAddressSaved) {
                    $config['selectedAddress'] = Address::create($order->shipment->to_address);
                }
                if ($order->shipment->method) {
                    $config['selectedMethod'] = $order->shipment->method;
                }
            }
        } elseif (Yii::$app->session->get('delivery_address')) {
            $config['selectedAddress'] = Address::create(Yii::$app->session->get('delivery_address'));
            if (Yii::$app->session->get('delivery_method')) {
                $config['selectedMethod'] = Yii::$app->session->get('delivery_method');
            }
        }
        return $config;
    }
}
