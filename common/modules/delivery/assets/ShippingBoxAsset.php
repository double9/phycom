<?php

namespace phycom\common\modules\delivery\assets;

use yii\web\AssetBundle;

/**
 * ShippingBox widget asset bundle.
 */
class ShippingBoxAsset extends AssetBundle
{
	public $sourcePath = '@phycom/common/modules/delivery/assets/shipping-box';
	public $js = [
		'main.js'
	];
    public $publishOptions = ['except' => ['*.less', '*.scss']];
	public $depends = [
		'yii\web\JqueryAsset'
	];
}