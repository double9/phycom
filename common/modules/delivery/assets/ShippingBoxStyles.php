<?php

namespace phycom\common\modules\delivery\assets;

use yii\web\AssetBundle;

/**
 * ShippingBox widget asset bundle.
 */
class ShippingBoxStyles extends AssetBundle
{
	public $sourcePath = '@phycom/common/modules/delivery/assets/shipping-box';
	public $css = [
	    'main.css'
    ];
	public $publishOptions = ['except' => ['*.less']];
}