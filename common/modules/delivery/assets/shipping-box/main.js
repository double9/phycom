var shippingBox = (function ($) {

    var instance;

    var ShippingBox = function(el, options) {

        this.el = el;
        this.$el = $(el);
        if (!this.$el.length) {
            throw 'Element ' + el + ' was not found';
        }
        this.cart = typeof window.cart !== 'undefined' ? window.cart : null;
        this.baseUrl = this.$el.find('.shipping-box-container').data('base-url');

        this.opts = options;
        this.init();
    };

    ShippingBox.prototype.init = function () {
        let self = this;

        this.$el.on('dp.change', 'input[name="ShippingForm[deliveryDate]"]', function (e) {

            let deliveryMethod = $(this).closest('form').find('input[name="ShippingForm[methodId]"]').val(),
                options = self.opts[deliveryMethod],
                areaCode, url;

            if (options.customDeliveryTime) {

                areaCode = $(this).closest('form').find('.delivery-address').val();
                url = self.baseUrl + '/get-delivery-times?deliveryMethod=' + deliveryMethod + '&areaCode=' + areaCode + '&date=' + e.date.format('YYYY-MM-DD');

                $.get(url).done(function (times) {

                    let timePicker = $('#' + deliveryMethod + '-time').data('DateTimePicker'),
                        hStart = parseInt(times.from.split(':')[0]),
                        hEnd = parseInt(times.to.split(':')[0]),
                        hours = [];

                    for (let i = hStart; i <= hEnd; i++) {
                        hours.push(i);
                    }
                    timePicker.enabledHours(hours);

                    if (hStart > 0) {
                        timePicker.disabledHours([0]);
                    }

                    let intervals = [];
                    let from = times.from;

                    if (moment(e.date.format('YYYY-MM-DD') + 'T' + from, moment.HTML5_FMT.DATETIME_LOCAL).isBefore(moment())) {
                        from = moment().add(15, 'm').format(moment.HTML5_FMT.TIME);
                    }

                    if (from !== '00:00') {
                        intervals.push([
                            moment('00:00', moment.HTML5_FMT.TIME),
                            moment(from, moment.HTML5_FMT.TIME)
                        ]);
                    }

                    if (parseInt(times.from.replace(':','')) < parseInt(times.to.replace(':','')) && times.to !== '00:00') {
                        intervals.push([
                            moment(times.to, moment.HTML5_FMT.TIME),
                            moment('23:59', moment.HTML5_FMT.TIME)
                        ]);
                    }

                    if (intervals.length) {
                        timePicker.disabledTimeIntervals(intervals);
                    }

                    if (timePicker.date().isBefore(moment(from, moment.HTML5_FMT.TIME))) {
                        timePicker.date(moment(from, moment.HTML5_FMT.TIME));
                    }
                });
            }

        });


        this.$el.on('change', '.delivery-address', function (e) {
            var value = $(this).val() || null,
                url = self.baseUrl + '/get-delivery-dates?' + $(this).closest('form').serialize(),
                $dateField = $(this).closest('form').find('input[name="ShippingForm[deliveryDate]"]'),
                datePicker,
                selectedDate;

            if (value) {
                self.updateCart($(this).find('option:selected'));


                if ($dateField.length) {
                    datePicker = $dateField.data('DateTimePicker');
                    selectedDate = $dateField.val();

                    $.get(url).done(function (dates) {

                        if (-1 === $.inArray(selectedDate, dates)) {
                            if (dates.length) {
                                datePicker.date(dates[0]);
                            }
                        } else {
                            $dateField.trigger({
                                type: 'dp.change',
                                date: moment(dates[0], moment.HTML5_FMT.DATE),
                                oldDate: moment(dates[0], moment.HTML5_FMT.DATE)
                            });
                        }
                        datePicker.enabledDates(dates);
                    });
                }
            }
        });


        $('.nav', this.el).on('shown.bs.tab', function (e) {
            var $deliveryAddress = self.getSelectedAddress();
            $deliveryAddress.trigger('change');
            self.checkOpenAddressForm($deliveryAddress);
        });

        // if ($('.cart-table tr[data-type="delivery"]').length > 0) {
        //
        //
        // } else {
        //
        //
        // }

        var $deliveryAddress = this.getSelectedAddress();
        if ($deliveryAddress.length && $deliveryAddress.val()) {
            $deliveryAddress.trigger('change');
        }
    };

    ShippingBox.prototype.checkOpenAddressForm = function ($deliveryAddress) {
        if ($deliveryAddress.find('option').length === 0 && !$deliveryAddress.val()) {
            var $addressBtn = $deliveryAddress.closest('.shipping-box').find('.tab-content .tab-pane.active .add-address');
            if ($addressBtn.length) {
                $addressBtn.trigger('click');
            }
        }
    };


    ShippingBox.prototype.getSelectedAddress = function () {
        return this.$el.find('.delivery-address:visible');
    };


    ShippingBox.prototype.destroy = function () {
        this.$el.off('change');
    };

    ShippingBox.prototype.updateCart = function ($deliveryAddress) {

        if (!this.cart || !this.cart.productCount()) {
            return;
        }

        var multiService = $deliveryAddress.data('multi-service') || false,
            data = {}, labels = {};

        if (multiService) {
            return;
        }

        data['id'] = $deliveryAddress.attr('data-code');
        data['code'] = $deliveryAddress.attr('data-code');
        data['price'] = $deliveryAddress.attr('data-price');
        data['type'] = 'delivery';

        labels['price'] = $deliveryAddress.attr('data-price-display');
        labels['label'] = $deliveryAddress.attr('data-label');

        this.cart.removeByType('delivery');
        this.cart.addOrUpdateJs(data, labels);
    };

    ShippingBox.prototype.getActiveForm = function () {
        return $('#delivery-method').find('.tab-pane.active > form');
    };

    return {
        getPrototype: function () {
            return ShippingBox.prototype;
        },
        init: function (el, config) {
            instance = new ShippingBox(el, config);
        },
        getInstance: function () {
            return instance;
        },
        getForm: function () {
            return instance.getActiveForm();
        }
    };
})(jQuery);