<?php

namespace phycom\common\modules\email;

use phycom\common\modules\email\helpers\Log;
use yii\base\UserException;

/**
 * EmailException is the base class for exceptions that are related to email module
 */
class EmailException extends UserException
{
	public function __construct($message = "", $code = 0, \Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
		Log::error($this->getMessage());
	}
}