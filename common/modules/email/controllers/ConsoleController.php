<?php

namespace phycom\common\modules\email\controllers;

use phycom\common\models\attributes\MessageStatus;
use phycom\common\models\attributes\MessageType;
use phycom\common\models\Message;
use phycom\common\models\MessageAttachment;
use phycom\common\modules\email\helpers\EmailParser;
use phycom\common\modules\email\Module;

use yii\helpers\FileHelper;
use yii\helpers\Console;
use yii\console\ExitCode;
use yii\console\Controller;
use yii;

/**
 * Class ConsoleController
 * @package phycom\common\modules\email\controllers
 *
 * @property Module $module
 */
class ConsoleController extends Controller
{
    public $verbose = true;
    /**
     * @inheritdoc
     */
    public function options($actionId)
    {
        return ['verbose'];
    }

    public function beforeAction($action)
    {
        // allow only console access
        if (!Yii::$app->request->isConsoleRequest) {
            throw new yii\web\NotAcceptableHttpException('Invalid action ' . $action->uniqueId);
        }
        return parent::beforeAction($action);
    }

    /**
     * Sends an email
     *
     * @param string $subject
     * @param string $content
     * @param string $recipients
     * @param string|null $provider
     * @param array $attachments - attachment id's
     * @return int
     * @throws \phycom\common\modules\email\EmailException
     */
    public function actionSend($subject, $content, $recipients, $provider = null, array $attachments = [])
    {
        $response = $this->getModule()->sendEmail($recipients, $subject, $content, $attachments, null, $provider);
        $this->printLn(json_encode($response, JSON_PRETTY_PRINT));
        return ExitCode::OK;
    }

    /**
     * Creates a new Message and sends it using email module
     *
     * @param string $subject
     * @param string $content
     * @param string $recipient
     * @param string|null $provider
     * @param array $attachments
     * @return int
     * @throws \phycom\common\modules\email\EmailException
     * @throws yii\base\InvalidConfigException
     */
    public function actionSendMessage($subject, $content, $recipient, $provider = null, array $attachments = [])
    {
        (new EmailParser($recipient))->export();

        $msg = new Message();
        $msg->type = MessageType::EMAIL;
        $msg->status = MessageStatus::NEW;
        $msg->from = (string)Yii::$app->getSystemUser()->email;
        $msg->from_name = (string)Yii::$app->getSystemUser()->fullName;
        $msg->to = (new EmailParser($recipient))->getEmail();
        $msg->to_name = (new EmailParser($recipient))->getName();
        $msg->subject = $subject;
        $msg->content = $content;

        if (!$msg->save()) {
            $this->printLn('Error saving message', Console::FG_RED);
            $this->printLn(json_encode($msg->errors, JSON_PRETTY_PRINT), Console::FG_YELLOW);
            return ExitCode::CONFIG;
        }

        if (!empty($attachments)) {
            foreach ($attachments as $attachment) {
                $msgAttachment = new MessageAttachment();
                $msgAttachment->message_id = $msg->id;
                $msgAttachment->file_path = $attachment;
                $msgAttachment->file_name = pathinfo($attachment, PATHINFO_FILENAME) . '.' . pathinfo($attachment, PATHINFO_EXTENSION);
                $msgAttachment->mime_type = FileHelper::getMimeType($attachment);

                if (!$msgAttachment->save()) {
                    $this->printLn('Error saving attachment ' . $attachment, Console::FG_RED);
                    $this->printLn(json_encode($msgAttachment->errors, JSON_PRETTY_PRINT), Console::FG_YELLOW);
                }
            }
        }

        $this->printLn('Message ' . $msg->id  . ' successfully created', Console::FG_GREEN);
        $this->printLn(json_encode($msg->attributes, JSON_PRETTY_PRINT));

        $response = $this->getModule()->send($msg, false, $provider);
        $this->printLn(json_encode($response, JSON_PRETTY_PRINT));
        return ExitCode::OK;
    }

    /**
     * @return Module|yii\base\Module|null
     */
    protected function getModule()
    {
        return Yii::$app->getModule('email');
    }

    /**
     * @param string $msg
     */
    protected function printLn($msg)
    {
        if ($this->verbose) {
            echo call_user_func_array([$this, 'ansiFormat'], func_get_args()) . PHP_EOL;
        }
    }
}
