<?php

namespace phycom\common\modules\email\helpers;

use phycom\common\models\Message;
use phycom\common\modules\email\EmailException;

/**
 * Trait EmailTrait
 *
 * @package phycom\common\modules\email\helpers
 */
trait EmailTrait
{
    /**
     * @param Message $message
     * @throws EmailException
     */
    public function  validateMessage(Message $message)
    {
        if (!$message->from && $message->userFrom && !$message->userFrom->email) {
            throw new EmailException('Sender user (' . $message->userFrom->id . ') email address missing.');
        }
        if (!$message->to && $message->userTo && !$message->userTo->email) {
            throw new EmailException('Recipient user (' . $message->userTo->id . ') email address missing.');
        }
        if (!$senderName = $message->getSenderName()) {
            throw new EmailException('Message ' . $message->id . ' sender name is missing.');
        }
        if (!$recipientName = $message->getRecipientName()) {
            throw new EmailException('Message ' . $message->id . ' recipient name is missing.');
        }
    }

    /**
     * @param Message $message
     * @return string|null
     */
    public function getSenderName(Message $message)
    {
        return $message->getSenderName();
    }

    /**
     * @param Message $message
     * @return string
     * @throws EmailException
     */
    public function getSenderEmail(Message $message)
    {
        try {
            return $message->from ?: (string)$message->userFrom->email;
        } catch (\Exception $e) {
            throw new EmailException('Error parsing sender email: ' . $e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @param Message $message
     * @return string|null
     */
    public function getRecipientName(Message $message)
    {
        return $message->getRecipientName();
    }

    /**
     * @param Message $message
     * @return string
     * @throws EmailException
     */
    public function getRecipientEmail(Message $message)
    {
        try {
            return $message->to ?: (string)$message->userTo->email;
        } catch (\Exception $e) {
            throw new EmailException('Error parsing recipient email: ' . $e->getMessage(), $e->getCode(), $e);
        }
    }
}
