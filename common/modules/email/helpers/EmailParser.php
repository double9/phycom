<?php

namespace phycom\common\modules\email\helpers;

use phycom\common\modules\email\EmailException;

use yii\base\BaseObject;
use yii\helpers\Json;

/**
 * Class EmailParser
 * @package phycom\common\modules\email\helpers
 *
 * @property-read string $email
 * @property-read string $name
 */
class EmailParser extends BaseObject
{
    protected $value;
    protected $email;
    protected $name;

    public function __construct($value, array $config = [])
    {
        if (!is_string($value)) {
            throw new EmailException('Invalid email value ' . Json::encode($value));
        }
        $this->value = $value;
        parent::__construct($config);
    }

    public function init()
    {
        parent::init();
        $matches = [];
        if (preg_match('/([^<]+)\s?<(.*)>/', $this->value, $matches)) {
            $this->name = trim($matches[1]);
            $this->email = trim(filter_var($matches[2], FILTER_VALIDATE_EMAIL));
        } else if (filter_var($this->value, FILTER_VALIDATE_EMAIL)) {
            $this->email = trim($this->value);
        } else {
            throw new EmailException('Invalid email ' . $this->value);
        }
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getName()
    {
        return $this->name;
    }

    public function export()
    {
        return [$this->email, $this->name];
    }
}
