<?php

namespace phycom\common\modules\payment\exceptions;

/**
 * Class NotFoundException
 * @package phycom\common\modules\payment\exceptions
 */
class NotFoundException extends PaymentException
{

}
