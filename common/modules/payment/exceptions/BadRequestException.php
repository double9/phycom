<?php

namespace phycom\common\modules\payment\exceptions;


use yii\web\BadRequestHttpException;


/**
 * Class BadRequestException
 * @package phycom\common\modules\payment\exceptions
 */
class BadRequestException extends BadRequestHttpException
{

}