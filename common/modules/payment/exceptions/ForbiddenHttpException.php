<?php

namespace phycom\common\modules\payment\exceptions;


/**
 * Class ForbiddenHttpException
 * @package phycom\common\modules\payment\exceptions
 */
class ForbiddenHttpException extends \yii\web\ForbiddenHttpException
{

}