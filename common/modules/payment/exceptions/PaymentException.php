<?php

namespace phycom\common\modules\payment\exceptions;

use yii\base\Exception;

/**
 * Class BaseException
 * @package phycom\common\modules\payment\exceptions
 */
class PaymentException extends Exception
{

}