<?php

namespace phycom\common\modules\payment\exceptions;


/**
 * Class NotFoundHttpException
 * @package phycom\common\modules\payment\exceptions
 */
class NotFoundHttpException extends \yii\web\NotFoundHttpException
{

}