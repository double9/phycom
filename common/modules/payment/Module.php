<?php
namespace phycom\common\modules\payment;

use phycom\common\interfaces\CommerceComponentInterface;
use phycom\common\modules\payment\interfaces\PaymentMethodInterface;
use phycom\common\modules\payment\methods\PaymentMethod;

use yii\helpers\ArrayHelper;
use yii\base\Module as BaseModule;

/**
 * Class Module
 * @package phycom\common\modules\payment
 * @property PaymentMethodInterface[] $methods - enabled payment methods
 */
class Module extends BaseModule implements CommerceComponentInterface
{
    public bool $enabled = true;

	public $defaultMethod;
	public $keyPath = '@files/payment-keys';
	public $currency = 'EUR';
	public $country = 'ee';
	public $companyName;

	public function init()
	{
		parent::init();
		$subModules = [
            methods\invoice\Module::ID => [
				'class' => methods\invoice\Module::class,
			],
            methods\cash\Module::ID => [
				'class' => methods\cash\Module::class,
			]
		];

        foreach ($this->getModules() as $id => $params) {
            $params['useSettings'] = true;
            $params['enabled'] = false;

            if (isset($subModules[$id])) {
                $params = ArrayHelper::merge($subModules[$id], $params);
            }
            $this->setModule($id, $params);
        }
	}

	public function isEnabled()
    {
        return $this->enabled;
    }

    /**
	 * @return PaymentMethod[]|PaymentMethodInterface[]
	 */
	public function getMethods()
	{
		$enabled = [];

		foreach ($this->getModules() as $id => $module) {
		    if (is_array($module)) {
		        $module = $this->getModule($id);
            }
			/**
			 * @var PaymentMethodInterface $module
			 */
			if ($module->isEnabled()) {
				$enabled[] = $module;
			}
		}
		return $enabled;
	}
}
