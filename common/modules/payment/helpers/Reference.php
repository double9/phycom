<?php

namespace phycom\common\modules\payment\helpers;

use phycom\common\modules\payment\exceptions\NotFoundException;

use phycom\common\models\Payment;

use yii\base\BaseObject;
use yii;

/**
 * Class Reference
 * @package phycom\common\modules\payment\helpers
 */
class Reference extends BaseObject
{
    /**
     * @param Payment $payment
     * @return string
     */
    public static function create(Payment $payment)
    {
        return $payment->id . '-' . $payment->invoice->number;
    }

    /**
     * @param string $key
     * @return Payment
     * @throws NotFoundException
     */
    public static function parse($key)
    {
        list($paymentId) = explode('-', $key);
        $payment = Yii::$app->modelFactory->getPayment()::findOne($paymentId);
        if (!$payment) {
            throw new NotFoundException('Payment ' . $paymentId . ' not found');
        }
        return $payment;
    }
}