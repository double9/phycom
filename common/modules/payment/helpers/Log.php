<?php

namespace phycom\common\modules\payment\helpers;

use yii;

/**
 * Class Log
 * @package phycom\common\modules\payment\helpers
 */
class Log
{
    const DEFAULT_CATEGORY = 'payment';

    public static function info($msg, $category = self::DEFAULT_CATEGORY)
    {
        Yii::info($msg, $category);
    }

    public static function trace($msg, $category = self::DEFAULT_CATEGORY)
    {
        static::debug($msg, $category);
    }

    public static function debug($msg, $category = self::DEFAULT_CATEGORY)
    {
        Yii::debug($msg, $category);
    }

    public static function warning($msg, $category = self::DEFAULT_CATEGORY)
    {
        Yii::warning($msg, $category);
    }

    public static function error($msg, $category = self::DEFAULT_CATEGORY)
    {
        Yii::error($msg, $category);
    }
}