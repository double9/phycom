<?php

namespace phycom\common\modules\payment\models;


use phycom\common\models\ModuleSettingsForm;
use phycom\common\modules\payment\interfaces\PaymentMethodInterface;
use phycom\common\modules\payment\methods\PaymentMethod;

use Yii;

/**
 * Class Settings
 * @package phycom\common\modules\payment\models
 *
 * @property-read PaymentMethod|PaymentMethodInterface $module
 */
class Settings extends ModuleSettingsForm
{
    public $enabled;
    public $testMode;

    public function rules()
    {
        return [
            ['enabled', 'boolean'],
            ['testMode', 'boolean']
        ];
    }

    public function attributeLabels()
    {
        return [
            'enabled' => Yii::t('common/payment', 'Enabled'),
            'testMode' => Yii::t('common/payment', 'Test Mode'),
        ];
    }

    public function attributeHints()
    {
        return [
            'enabled' => Yii::t('common/payment', 'Is payment method enabled'),
            'testMode' => Yii::t('common/payment', 'Execute payments on test environment'),
        ];
    }
}
