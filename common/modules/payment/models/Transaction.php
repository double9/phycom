<?php

namespace phycom\common\modules\payment\models;

use phycom\common\modules\payment\helpers\Log;
use phycom\common\modules\payment\interfaces\TransactionInterface;

use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class Transaction
 *
 * @package phycom\common\modules\payment\models
 */
class Transaction extends BaseObject implements TransactionInterface
{
	protected $data;
	protected $method;

	public function __construct($transactionData, array $config = [])
	{
		$this->data = $transactionData;
		parent::__construct($config);
	}

	public function init()
    {
        parent::init();
        Log::info('Transaction ' . $this->getId() . ' created');
        Log::info(Json::encode($this->data, JSON_PRETTY_PRINT));
    }

    public function setMethod($value)
	{
		$this->method = $value;
        Log::info('Transaction ' . $this->getId() . ' method changed to ' . $value);
	}

	public function getId()
	{
		return $this->data['id'] ?? null;
	}

	public function getAmount()
	{
		return $this->data['amount'];
	}

	public function getData()
	{
		return Json::encode($this->data);
	}

	public function getPaymentLink()
	{
		return $this->data['paymentUrl'] ?? '';
	}

	public function getReference()
    {
        return $this->data['reference'];
    }

	public function getResponseData(array $params = [])
    {
        return ArrayHelper::merge([
            'reference' => $this->getReference(),
            'amount'    => $this->getAmount(),
            'url'       => $this->getPaymentLink()
        ], $params);
    }
}
