<?php

namespace phycom\common\modules\payment\interfaces;

use phycom\common\models\Payment;

/**
 * Interface PaymentMethodInterface
 * @package phycom\common\modules\payment\interfaces
 */
interface PaymentMethodInterface
{
	/**
	 * @return bool
	 */
	public function isEnabled() : bool;
	/**
	 * Unique delivery provider id
	 * @return string
	 */
	public function getId() : string;

	/**
	 * Delivery provider display name
	 * @return string
	 */
	public function getName() : string;

	/**
	 * Delivery provider short name
	 * @return string
	 */
	public function getLabel() : string;

	/**
	 * @return string
	 */
	public function getPaymentInfo() : ?string;

	/**
	 * @return string|null
	 */
	public function getLogo() : string;

	/**
	 * @param array $params
	 * @return string
	 */
	public function getPaymentButtons(array $params = []) : string;

	/**
	 * @param Payment $payment
	 * @param string $methodName
	 * @return TransactionInterface
	 */
	public function createTransaction(Payment $payment, string $methodName = null) : TransactionInterface;
}
