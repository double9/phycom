<?php

namespace phycom\common\modules\payment\interfaces;

/**
 * Interface TransactionInterface
 * @package phycom\common\modules\payment\interfaces
 */
interface TransactionInterface
{
	/**
	 * @return string
	 */
	public function getId();
	/**
	 * @return number
	 */
	public function getAmount();
	/**
	 * @return string - json encoded raw transaction data
	 */
	public function getData();
	/**
	 * @return string
	 */
	public function getPaymentLink();
    /**
     * @param array $params - additional params to include
     * @return mixed - data returned by transaction/begin controller action
     */
	public function getResponseData(array $params = []);
}