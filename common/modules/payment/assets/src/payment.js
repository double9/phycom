"use strict";


jQuery(function($) {

    function isMobile() {
        return navigator.userAgent.match(/Android/i)
            || navigator.userAgent.match(/webOS/i)
            || navigator.userAgent.match(/iPhone/i)
            || navigator.userAgent.match(/iPad/i)
            || navigator.userAgent.match(/iPod/i)
            || navigator.userAgent.match(/BlackBerry/i)
            || navigator.userAgent.match(/Windows Phone/i);
    }

    window.submitPayment = function(paymentMethod, paymentButton) {
        var url = $('#payment-methods').data('transaction-route').length ? $('#payment-methods').data('transaction-route') + '/begin?method=' + paymentMethod : null;
        var $btn = $(paymentButton);
        $btn.button('loading');

        if (!url) {
            throw Error('Transaction route not found');
        }

        var beginTransaction = function() {
            return $.ajax({
                url: url,
                type: 'POST',
            }).done(function(data) {
                $btn.button('reset');
                return data;
            }).fail(function(jqXHR, errMsg) {
                console.log(jqXHR);
                console.log(errMsg);
                $btn.button('reset');
            });
        };
        if (typeof window.submitOrderWithPayment !== 'undefined' && window.submitOrderWithPayment === true) {
            return window.order.submit().then(beginTransaction).fail(function (err) {
                $btn.button('reset');
            });
        } else {
            return beginTransaction();
        }
    };

	$(document).ready(function(e) {

        //check if inside paytailor app whe show only paytailor method
        if (isMobile() && typeof window.Paytailor !== "undefined") {
            $('#payment-methods .alert').hide();
            $('.payment-button').each(function(i, el) {
                var $btn = $(el);
                var $container = $btn.closest('.payment-method');

                if (['paytailor-payment-button', 'paytailor-in-app-payment-button'].indexOf(el.id) === -1) {
                    $container.remove();
                }
                if (el.id === 'paytailor-payment-button') {
                    $btn.remove();
                }
                if (el.id === 'paytailor-in-app-payment-button') {
                    $container.find('.payment-info').hide();
                    $btn.show();
                }
            });
        }

        $('#payment-methods').show();
	});
});