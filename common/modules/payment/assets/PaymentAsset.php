<?php

namespace phycom\common\modules\payment\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Main backend application asset bundle.
 */
class PaymentAsset extends AssetBundle
{
    public $sourcePath = '@phycom/common/modules/payment/assets/src';
    public $css = [
    	'payment.css'
    ];
    public $js = [
	    'payment.js'
    ];
	public $publishOptions = ['except' => ['*.less']];
    public $depends = [
        JqueryAsset::class
    ];
}
