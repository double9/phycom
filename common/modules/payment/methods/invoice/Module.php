<?php

namespace phycom\common\modules\payment\methods\invoice;

use phycom\common\helpers\Currency;
use phycom\common\models\Payment;
use phycom\common\modules\payment\helpers\Reference;
use phycom\common\modules\payment\interfaces\PaymentMethodInterface;
use phycom\common\modules\payment\interfaces\TransactionInterface;
use phycom\common\modules\payment\models\Transaction;
use phycom\common\modules\payment\methods\PaymentMethod;
use phycom\common\modules\payment\methods\invoice\assets\InvoiceBrandAsset;

use Yii;

/**
 * Class Module
 * @package phycom\common\modules\payment\methods\invoice
 *
 * @property string $id
 * @property bool $isEnabled
 * @property string $name
 * @property string $label
 */
class Module extends PaymentMethod implements PaymentMethodInterface
{
    const ID = 'invoice';

	public ?string $paymentInfo = null;

    /**
     * @return string
     */
	public function getLabel() : string
	{
		return Yii::t('common/modules/payment', 'Invoice payment');
	}

    /**
     * @return string
     */
	public function getLogo() : string
	{
        $bundle = InvoiceBrandAsset::register(Yii::$app->view);
        return $bundle->baseUrl . '/invoice.svg';
	}

    /**
     * @param array $params
     * @return string
     */
	public function getPaymentButtons(array $params = []) : string
	{
		return '';
	}

    /**
     * @return string|null
     */
	public function getPaymentInfo() : ?string
	{
		if ($this->paymentInfo === false) {
			return null;
		}
		if (strlen($this->paymentInfo)) {
			return $this->paymentInfo;
		}
		return '';
	}

    /**
     * @param Payment $payment
     * @param string|null $methodName
     * @return TransactionInterface|Transaction|object
     * @throws \yii\base\InvalidConfigException
     */
	public function createTransaction(Payment $payment, string $methodName = null) : TransactionInterface
	{
        return Yii::createObject(Transaction::class, [[
            'amount'    => Currency::toDecimal($payment->amount),
            'reference' => Reference::create($payment)
        ]]);
	}
}
