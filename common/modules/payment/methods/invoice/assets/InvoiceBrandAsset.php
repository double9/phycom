<?php

namespace phycom\common\modules\payment\methods\invoice\assets;

use yii\web\AssetBundle;

/**
 * Invoice brand asset bundle.
 */
class InvoiceBrandAsset extends AssetBundle
{
	public $sourcePath = '@phycom/common/modules/payment/methods/invoice/assets/brand';
}