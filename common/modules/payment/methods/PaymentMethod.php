<?php

namespace phycom\common\modules\payment\methods;

use phycom\common\modules\payment\Module as PaymentModule;
use phycom\common\modules\payment\models\Settings;

use phycom\common\interfaces\ModuleSettingsInterface;
use phycom\common\models\traits\ModuleSettingsTrait;

use yii\base\Module as BaseModule;
use yii\helpers\Inflector;
use yii;

/**
 * Class PaymentMethod
 *
 * @package phycom\common\modules\payment\methods
 */
abstract class PaymentMethod extends BaseModule implements ModuleSettingsInterface
{
    const ID = 'default';

    use ModuleSettingsTrait;

    /**
     * @var bool
     */
    public bool $enabled = true;
    /**
     * @var bool
     */
    public bool $testMode = false;
    /**
     * @var string
     */
    public ?string $paymentInfo = null;

    /**
     * @return PaymentModule|yii\base\Module|null
     */
    public function getBaseModule() : ?PaymentModule
    {
        return Yii::$app->getModule('payment');
    }

    public function getName() : string
    {
        return $this->id;
    }

    public function getId() : string
    {
        return $this->id;
    }

    public function getLabel() : string
    {
        return Inflector::humanize($this->id);
    }

    public function getPaymentInfo() : ?string
    {
        return $this->paymentInfo ?: null;
    }

    public function isEnabled() : bool
    {
        return $this->enabled;
    }

    public function getSettingsFormClassName() : string
    {
        return Settings::class;
    }
}
