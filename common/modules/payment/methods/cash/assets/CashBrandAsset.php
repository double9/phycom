<?php

namespace phycom\common\modules\payment\methods\cash\assets;

use yii\web\AssetBundle;

/**
 * Cash payment brand asset bundle.
 */
class CashBrandAsset extends AssetBundle
{
	public $sourcePath = '@phycom/common/modules/payment/methods/cash/assets/brand';
}