<?php

namespace phycom\common\modules\payment\methods\cash\assets;

use yii\web\AssetBundle;

/**
 * Cash payment method asset bundle.
 */
class CashAsset extends AssetBundle
{
    public $sourcePath = '@phycom/common/modules/payment/methods/cash/assets/src';
    public $css = [];
    public $js = [
        'cash.js'
    ];
    public $publishOptions = ['except' => ['*.less']];
    public $depends = [
        'phycom\common\modules\payment\assets\PaymentAsset'
    ];
}