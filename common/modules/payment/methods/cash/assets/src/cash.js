jQuery(function($) {
    $("#cash-payment-button:not(.disabled)").click(function(e) {
        e.preventDefault();
        window.submitPayment('cash', this).done(function (data) {
            var $paymentForm = $("#cash-form");
            if ($paymentForm.length) {
                console.log("Writing data: ", data, " to ", $paymentForm);
                $paymentForm.submit();
            }
        });
    });
});