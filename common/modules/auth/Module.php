<?php
namespace phycom\common\modules\auth;

use phycom\common\modules\auth\methods\AuthenticationMethod;

use yii\helpers\ArrayHelper;
use yii\base\Module as BaseModule;

/**
 * Class Module
 * @package phycom\common\modules\auth
 */
class Module extends BaseModule
{
    const METHOD_GOOGLE = 'google';
    const METHOD_FACEBOOK = 'facebook';

    public function init()
    {
        parent::init();
        $subModules = [
            self::METHOD_GOOGLE => [
                'class' => methods\google\Module::class,
                'enabled' => false,
                'clientId' => getenv(ENV . '_GOOGLE_CLIENT_ID'),
            ],
            self::METHOD_FACEBOOK => [
                'class' => methods\facebook\Module::class,
                'enabled' => false,
                'appId' => getenv(ENV . '_FB_APP_ID'),
            ]
        ];
        foreach ($subModules as $id => $moduleParams) {
            $params = isset($this->modules[$id]) ? ArrayHelper::merge($moduleParams, $this->modules[$id]) : $moduleParams;
            $this->setModule($id, $params);
        }
    }

    /**
     * @return AuthenticationMethod[]
     */
    public function getMethods()
    {
        $enabled = [];

        foreach ($this->modules as $id => $moduleParams) {
            $module = $this->getModule($id);
            /**
             * @var AuthenticationMethod $module
             */
            if ($module->getIsEnabled()) {
                $enabled[] = $module;
            }
        }
        return $enabled;
    }

    /**
     * @param string $id - method id
     * @return AuthenticationMethod
     */
    public function getMethod($id)
    {
        /**
         * @var AuthenticationMethod $module
         */
        $module = $this->getModule($id);
        return $module && $module->getIsEnabled() ? $module : null;
    }

    /**
     * @param string $method - method id
     * @param null $label
     * @param array $options
     * @return string
     */
    public function render($method, $label = null, array $options = [])
    {
        $method = $this->getMethod($method);
        return $method ? $method->render($label, $options) : '';
    }
}
