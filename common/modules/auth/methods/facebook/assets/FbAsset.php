<?php

namespace phycom\common\modules\auth\methods\facebook\assets;

use phycom\common\modules\auth\methods\facebook\Module;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii;

/**
 * Fb login asset bundle.
 */
class FbAsset extends AssetBundle
{
	public $sourcePath = '@phycom/common/modules/auth/methods/facebook/assets/fb';
	public $js = [
		'facebook.js',
	];
	public $depends = [
		JqueryAsset::class,
	];

	public function init()
    {
        parent::init();
        $this->jsOptions = [
            'id' => 'fb-auth-script',
            'data-appid' => $this->getModule()->appId
        ];
    }

    /**
     * @return null|yii\base\Module|Module
     */
    public function getModule()
    {
        return Yii::$app->getModule('auth')->getModule('facebook');
    }
}
