<?php
namespace phycom\common\modules\auth\methods\facebook;


use phycom\common\modules\auth\methods\AuthenticationMethod;
use phycom\common\modules\auth\methods\facebook\assets\FbAsset;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii;

/**
 * Class Module
 * @package phycom\common\modules\auth\methods\facebook
 */
class Module extends AuthenticationMethod
{
    public $appId;

    public $defaultRoute = 'signup';

    public function render($label = null, array $options = [])
    {
        $label = $label ?: Yii::t('common/auth','Facebook');
        $options = ArrayHelper::merge([
            'id' => 'facebook-login-button',
            'class' => 'btn btn-blue tx-spacing-1 tx-size-sm tx-uppercase tx-medium',
            'data-url' => Yii::$app->urlManager->createAbsoluteUrl(['/auth/facebook/signup/authenticate'])
        ], $options);

        FbAsset::register(Yii::$app->view);
        return Html::button($label, $options);
    }
}