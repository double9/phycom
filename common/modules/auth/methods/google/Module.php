<?php
namespace phycom\common\modules\auth\methods\google;


use phycom\common\modules\auth\methods\AuthenticationMethod;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii;

/**
 * Class Module
 * @package phycom\common\modules\auth\methods\google
 */
class Module extends AuthenticationMethod
{
    public $defaultRoute = 'signup';

    public $clientId;

    public $authScope = [
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/userinfo.email',
        'https://www.googleapis.com/auth/user.phonenumbers.read',
        'https://www.googleapis.com/auth/user.addresses.read'
    ];

    protected $oAuthUrl = 'https://accounts.google.com/o/oauth2/auth';

    public function render($label = null, array $options = [])
    {
        $label = $label ?: Yii::t('common/auth','Google');
        $options = ArrayHelper::merge([
            'id' => 'google-login-button',
            'class' => 'btn btn-danger tx-spacing-1 tx-size-sm tx-uppercase tx-medium mg-b-10'
        ], $options);

        return Html::a($label, $this->createAuthUrl(), $options);
    }

    public function createAuthUrl($redirect = null)
    {
        $redirect = $redirect ?: Yii::$app->urlManager->createAbsoluteUrl(['/auth/google/']);
        $url = $this->oAuthUrl .
            '?scope=' . implode(' ', $this->authScope) .
            '&client_id=' . $this->clientId .
            '&redirect_uri=' . $redirect .
            '&response_type=token';

        return $url;
    }
}