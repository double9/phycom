<?php 

$url = Yii::$app->urlManager->createAbsoluteUrl(['auth/google/signup/authenticate']);

$this->registerJs(
<<<JS
    jQuery(function ($) {
		$.post('$url', window.location.hash);
    });
JS
, yii\web\View::POS_END);

?>
<div id="google-auth-loading" class="container-wrapper">
    <div class="alert-container">
        <h3><?= Yii::t('public/main', 'Authentication in progress...') ?></h3>
    </div>
</div>