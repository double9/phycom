<?php

namespace phycom\common\modules\auth\methods\google\controllers;


use phycom\common\helpers\FlashMsg;
use phycom\common\modules\auth\methods\google\Module;
use phycom\common\modules\auth\models\ExternalSignupForm;

use phycom\common\models\UserTag;
use phycom\common\models\traits\WebControllerTrait;

use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\MessageFactoryDiscovery;

use yii\web\Controller as BaseController;
use yii\web\Response;
use yii;

/**
 * Class GoogleController
 * @package frontend\controllers
 */
class SignupController extends BaseController
{
	use WebControllerTrait;

	public $enableCsrfValidation = false;

	public function actionIndex()
	{
		return $this->render('/login');
	}

	public function actionAuthenticate()
	{
		Yii::info('Google authenticate - ' . json_encode(Yii::$app->request->post()), __METHOD__);
		$this->ajaxOnly(Response::FORMAT_JSON);

		$request = MessageFactoryDiscovery::find()->createRequest('get', 'https://www.googleapis.com/oauth2/v1/userinfo?access_token=' . Yii::$app->request->post('#access_token'));
		$httpAsyncClient = HttpClientDiscovery::find();

		$response = $httpAsyncClient->sendRequest($request);
		$data = json_decode($response->getBody()->getContents());

		if (!isset($data->error)) {
			$model = new ExternalSignupForm();
			$model->email = $data->email;
			$model->firstName = $data->given_name;
			$model->lastName = $data->family_name;
			$model->externalId = $data->id;

			$user = $model->getUser();

			if ($user === null) {
			    if (!Module::getInstance()->autoSignup) {
			        FlashMsg::error(Yii::t('common/auth', 'User not found'));
                    return $this->goBack();
                }
				$user = $model->signup();
				$user->tag(UserTag::TAG_REGISTERED_WITH_GOOGLE);
			}
			Yii::$app->getUser()->login($user);
		}
		return $this->goBack();
	}
}
