<?php

namespace phycom\common\modules\auth\methods;

use phycom\common\modules\auth\models\Settings;

use phycom\common\models\traits\ModuleSettingsTrait;

use yii\base\Module as BaseModule;


/**
 * Class Module
 * @package phycom\common\modules\auth\methods
 */
abstract class AuthenticationMethod extends BaseModule
{
    use ModuleSettingsTrait;
    /**
     * @var bool
     */
    public $enabled;
    /**
     * @var bool
     */
    public $testMode;
    /**
     * @var bool
     */
    public $autoSignup = true;


    public function getId()
    {
        return $this->id;
    }

    public function getIsEnabled()
    {
        return $this->enabled;
    }

    public function getSettingsFormClassName()
    {
        return Settings::class;
    }

    /**
     * @var string $label
     * @var array $options
     * @return string
     */
    abstract public function render($label = null, array $options = []);
}