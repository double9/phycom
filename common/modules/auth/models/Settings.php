<?php

namespace phycom\common\modules\auth\models;


use phycom\common\models\ModuleSettingsForm;

use phycom\common\modules\auth\methods\AuthenticationMethod;
use yii;

/**
 * Class Settings
 * @package phycom\common\modules\auth\models
 *
 * @property-read AuthenticationMethod $module
 */
class Settings extends ModuleSettingsForm
{
    public $enabled;
    public $testMode;

    public function rules()
    {
        return [
            ['enabled', 'boolean'],
            ['testMode', 'boolean']
        ];
    }

    public function attributeLabels()
    {
        return [
            'enabled' => Yii::t('common/auth', 'Enabled'),
            'testMode' => Yii::t('common/auth', 'Test Mode'),
        ];
    }

    public function attributeHints()
    {
        return [
            'enabled' => Yii::t('common/auth', 'Is payment method enabled'),
            'testMode' => Yii::t('common/auth', 'Execute payments on test environment'),
        ];
    }
}