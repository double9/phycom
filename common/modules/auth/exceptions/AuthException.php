<?php

namespace phycom\common\modules\auth\exceptions;

use yii\base\Exception;

/**
 * Class AuthException
 * @package phycom\common\modules\auth\exceptions
 */
class AuthException extends Exception
{

}