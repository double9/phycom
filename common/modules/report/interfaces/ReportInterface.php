<?php

namespace phycom\common\modules\report\interfaces;


use phycom\common\models\Shop;

/**
 * Interface ReportInterface
 * @package phycom\common\modules\report\interfaces
 */
interface ReportInterface
{
    const FORMAT_CSV = 'csv';
    const FORMAT_XLS = 'xls';

    /**
     * @return string|null
     */
    public function getTitle();

    /**
     * @return string|null
     */
    public function getDescription();

    /**
     * @return array
     */
    public function getFormats();
	/**
	 * @param \DateTime $start
	 * @param \DateTime $end
	 * @return mixed
	 */
	public function setPeriod(\DateTime $start, \DateTime $end);

    /**
     * @param string $languageCode
     * @return mixed
     */
	public function setLanguage($languageCode);

	/**
	 * Calculates the result data and fills the model attributes
	 * @return static
	 */
	public function calculate();

    /**
     * @return ReportRowInterface[]
     */
	public function getModels();

	/**
	 * @param Shop $shop
	 * @return mixed
	 */
	public function setShop(Shop $shop);

	/**
	 * @return Shop|null
	 */
	public function getShop();


	public function getExporterColumns();
}