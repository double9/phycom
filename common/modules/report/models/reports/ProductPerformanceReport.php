<?php

namespace phycom\common\modules\report\models\reports;

use phycom\common\helpers\Date;
use phycom\common\models\attributes\OrderStatus;
use phycom\common\models\translation\ProductTranslation;
use phycom\common\modules\report\interfaces\ReportInterface;
use phycom\common\modules\report\models\ReportTrait;
use phycom\common\modules\report\ReportException;

use yii\base\Model;
use yii\db\Query;
use yii;

/**
 * Class ProductPerformanceReport
 * @package phycom\common\modules\report\models
 */
class ProductPerformanceReport extends Model implements ReportInterface
{
	use ReportTrait;

	protected $models;

	public function init()
    {
        parent::init();
        $this->title = Yii::t('common/modules/report', 'Product Performance Report');
        $this->description = Yii::t('common/modules/report', 'Shows how much revenue was generated from an individual product, how many unique purchases occurred, the quantity sold, and average price.');
    }

    public function getFormats()
    {
        return [static::FORMAT_XLS];
    }

    public function calculate()
	{
	    try {
            $query = (new Query())->select('r.*')->from([
                'r' => (new Query())
                    ->select([
                        'p.id AS "productId"',
                        'p.sku AS "productSku"',
                        't.title AS "productTitle"',
                        'SUM(oi.quantity) AS "purchaseQuantity"',
                        'COUNT(oi.id) AS "uniquePurchase"',
                        'SUM((1 - oi.discount) * (oi.price * oi.quantity)) AS revenue'
                    ])
                    ->from(['oi' => Yii::$app->modelFactory->getOrderItem()::tableName()])
                    ->innerJoin(['o' => Yii::$app->modelFactory->getOrder()::tableName()], [
                        'and',
                        'oi.order_id = o.id',
                        'o.paid_at IS NOT NULL',
                        ['not', ['o.status' => [OrderStatus::DELETED, OrderStatus::CANCELED, OrderStatus::ON_HOLD]]],
                        ['>', 'o.updated_at', Date::create($this->start)->dbTimestamp],
                        ['<=', 'o.updated_at', Date::create($this->end)->dbTimestamp]
                    ])
                    ->leftJoin(['p' => Yii::$app->modelFactory->getProduct()::tableName()], 'p.id = oi.product_id')
                    ->leftJoin(['t' => ProductTranslation::tableName()], [
                        'and',
                        't.product_id = p.id',
                        ['t.language' => $this->languageCode]
                    ])
                    ->where('oi.product_id IS NOT NULL')
                    ->groupBy(['oi.product_id', 'p.id', 'p.sku', 't.title'])
            ])->orderBy(['r.uniquePurchase' => SORT_DESC]);

            $reader = $query->createCommand()->query();
            $this->models = [];
            while ($row = $reader->read()) {
                $model = new ProductPerformanceReportRow($row);
                $this->models[] = $model;
            }
            return $this;

        } catch (\Exception $e) {
	        throw $e;
	        //throw new ReportException('Error calculating ' . __CLASS__, 0, $e);
        }
	}

	public function getModels()
    {
        return $this->models;
    }

    /**
     * @return array
     */
    public function getExporterColumns()
    {
        return (new ProductPerformanceReportRow())->getExporterColumns();
    }
}