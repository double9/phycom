<?php

namespace phycom\common\modules\report\models\reports;

use phycom\common\modules\report\models\ReportRow;

use yii;

/**
 * Class ProductPerformanceReportRow
 * @package phycom\common\modules\report\models\reports
 */
class ProductPerformanceReportRow extends ReportRow
{
    public $productId;
    public $productSku;
    public $productTitle;
    public $purchaseQuantity = 0;
    public $uniquePurchase = 0;
    public $revenue = 0.00;
    public $avgPrice = 0.00;
    public $avgQuantity = 0.00;

    public function init()
    {
        parent::init();

        if ($this->revenue > 0 && $this->purchaseQuantity > 0) {
            $this->avgPrice = $this->revenue / $this->purchaseQuantity;
        }
        if ($this->uniquePurchase > 0 && $this->purchaseQuantity > 0) {
            $this->avgQuantity = $this->uniquePurchase / $this->purchaseQuantity;
        }
    }

    public function attributeLabels()
    {
        return [
            'productId'        => Yii::t('common/modules/report', 'Product ID'),
            'productSku'       => Yii::t('common/modules/report', 'SKU'),
            'productTitle'     => Yii::t('common/modules/report', 'Title'),
            'purchaseQuantity' => Yii::t('common/modules/report', 'Purchase quantity'),
            'uniquePurchase'   => Yii::t('common/modules/report', 'Unique purchases'),
            'revenue'          => Yii::t('common/modules/report', 'Revenue'),
            'avgPrice'         => Yii::t('common/modules/report', 'Avg. Price'),
            'avgQuantity'      => Yii::t('common/modules/report', 'Avg. Quantity'),
        ];
    }

    public function getExporterColumns()
    {
        return [
            [
                'attribute' => 'productId',
                'format'    => 'integer',
                'contentOptions' => ['alignment' => ['horizontal' => 'left', 'vertical' => 'center']],
                'dimensionOptions' => ['autoSize' => true]
            ],
            [
                'attribute' => 'productSku',
                'format'    => 'text',
                'contentOptions' => ['alignment' => ['horizontal' => 'left', 'vertical' => 'center']],
                'dimensionOptions' => ['autoSize' => true]
            ],
            [
                'attribute' => 'productTitle',
                'format'    => 'text',
                'contentOptions' => ['alignment' => ['horizontal' => 'left', 'vertical' => 'center']],
                'dimensionOptions' => ['autoSize' => true]
            ],
            [
                'attribute' => 'purchaseQuantity',
                'format'    => 'integer',
                'contentOptions' => ['alignment' => ['horizontal' => 'left', 'vertical' => 'center']],
                'dimensionOptions' => ['autoSize' => true]
            ],
            [
                'attribute' => 'uniquePurchase',
                'format'    => 'integer',
                'contentOptions' => ['alignment' => ['horizontal' => 'left', 'vertical' => 'center']],
                'dimensionOptions' => ['autoSize' => true]
            ],
            [
                'attribute' => 'revenue',
                'format'    => 'decimal',
                'contentOptions' => ['alignment' => ['horizontal' => 'left', 'vertical' => 'center']],
                'dimensionOptions' => ['autoSize' => true]
            ],
            [
                'attribute' => 'avgPrice',
                'format'    => 'decimal',
                'contentOptions' => ['alignment' => ['horizontal' => 'left', 'vertical' => 'center']],
                'dimensionOptions' => ['autoSize' => true]
            ],
            [
                'attribute' => 'avgQuantity',
                'format'    => 'decimal',
                'contentOptions' => ['alignment' => ['horizontal' => 'left', 'vertical' => 'center']],
                'dimensionOptions' => ['autoSize' => true]
            ]
        ];
    }
}