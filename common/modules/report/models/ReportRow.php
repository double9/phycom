<?php

namespace phycom\common\modules\report\models;


use phycom\common\modules\report\interfaces\ReportRowInterface;

use yii\base\Model;

/**
 * Class ReportRow
 * @package phycom\common\modules\report\models
 */
abstract class ReportRow extends Model implements ReportRowInterface
{

}