<?php

namespace phycom\common\modules\report\models;


use phycom\common\modules\report\interfaces\ReportInterface;
use phycom\common\modules\report\ReportException;
use yii\data\ArrayDataProvider;
use yii2tech\spreadsheet\Spreadsheet;

use yii\helpers\Inflector;
use yii\base\Model;
use yii;

/**
 * Class ReportForm
 * @package phycom\common\modules\report\models
 */
class ReportForm extends Model
{
    public $startDate;
    public $endDate;
    public $format = ReportInterface::FORMAT_XLS;

    protected $reportModel;

    /**
     * @param string $reportModelClassName
     * @param array $config
     * @return static
     */
    public static function create($reportModelClassName, array $config = [])
    {
        $reportModel = new $reportModelClassName;
        return new static($reportModel, $config);
    }

    public function __construct(ReportInterface $reportModel, array $config = [])
    {
        $this->reportModel = $reportModel;
        parent::__construct($config);
    }

    public function init()
    {
        parent::init();
        $this->startDate = (new \DateTime())->sub(new \DateInterval('P1M'))->format('Y-m-d');
        $this->endDate = (new \DateTime())->format('Y-m-d');
    }

    public function rules()
    {
        return [
            [['startDate', 'endDate'], 'required'],
            [['startDate', 'endDate'], 'date', 'format' => 'php:Y-m-d'],
            [['format'], 'in', 'range' => $this->reportModel->getFormats()],
        ];
    }

    public function getReportTitle()
    {
        if ($this->reportModel->getTitle()) {
            return $this->reportModel->getTitle();
        }
        $modelName = (new \ReflectionClass($this->reportModel))->getShortName();
        return Inflector::camel2words($modelName);
    }

    public function getReportDescription()
    {
        return $this->reportModel->getDescription();
    }

    /**
     * @return bool|ReportInterface
     * @throws \Exception
     */
    public function calculateReport()
    {
        if ($this->validate()) {
            $startDate = new \DateTime($this->startDate);
            $endDate = new \DateTime($this->endDate);

            $this->reportModel->setPeriod($startDate, $endDate);
            $this->reportModel->setLanguage(Yii::$app->lang->current->code);
            return $this->reportModel->calculate();
        }
        return false;
    }

    /**
     * @return \yii\web\Response
     * @throws ReportException
     * @throws \ReflectionException
     */
    public function exportFile()
    {
        switch ($this->format) {
            case ReportInterface::FORMAT_XLS:
                $modelName = (new \ReflectionClass($this->reportModel))->getShortName();
                return (new Spreadsheet([
                    'dataProvider' =>  new ArrayDataProvider(['allModels' => $this->reportModel->getModels()]),
                    'columns' => $this->reportModel->getExporterColumns()
                ]))->send(Inflector::camel2id($modelName) . '_' . (new \DateTime())->format('Y-m-d\THis') . '.xls');
            default:
                throw new ReportException('Invalid report format ' . $this->format);
        }
    }
}
