<?php

namespace phycom\common\modules\report\models;

use phycom\common\models\Shop;
use phycom\common\modules\report\ReportException;

/**
 * Trait ReportTrait
 * @package phycom\common\modules\report\models
 */
trait ReportTrait
{
	protected $start;
	protected $end;
	protected $shop;
	protected $languageCode;
	protected $title;
	protected $description;

	public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

	public function setPeriod(\DateTime $start, \DateTime $end)
	{
		if ($start > $end) {
			throw new ReportException('Invalid report period');
		}
		$this->start = $start;
		$this->end = $end;
	}

    public function setLanguage($languageCode)
    {
        $this->languageCode = $languageCode;
    }

	public function setShop(Shop $shop)
	{
		$this->shop = $shop;
	}

	public function getShop()
	{
		return $this->shop;
	}
}