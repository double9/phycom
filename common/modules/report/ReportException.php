<?php

namespace phycom\common\modules\report;

use phycom\common\modules\report\helpers\Log;
use yii\base\Exception;

class ReportException extends Exception
{
	public function __construct($message = "", $code = 0, \Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
		Log::error($this->getMessage());
	}
}