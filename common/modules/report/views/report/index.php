<?php

/**
 * @var $this yii\web\View
 * @var \phycom\common\modules\report\models\ReportForm[] $reports
 */

use phycom\backend\widgets\Box;
use phycom\backend\widgets\ActiveForm;

use dosamigos\datepicker\DateRangePicker;

use yii\helpers\Html;

$this->title = Yii::t('common/modules/report', 'Reports');
$this->params['breadcrumbs'][] = $this->title
?>

<div class="row">
	<div class="col-md-12">

		<?= Box::begin([
			'options' => ['class' => 'box box-default'],
			'showHeader' => false,
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>
        <div class="row">
            <div class="col-md-8">
            <?php foreach ($reports as $report): ?>

            <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-md-12">
                        <h4><?= $report->getReportTitle() ?></h4>
                        <span class="text-muted text-sm" style="font-style: italic;"><?= $report->getReportDescription(); ?></span>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-8">
                        <?= $form->field($report, 'startDate')->widget(DateRangePicker::class, [
                            'attributeTo'   => 'endDate',
                            'form'          => $form, // best for correct client validation
                            'language'      => Yii::$app->lang->current->code,
                            'labelTo'       => Yii::t('common/modules/report', 'to'),
                            'clientOptions' => [
                                'todayHighlight' => true,
                                'autoclose'      => true,
                                'format'         => Yii::$app->formatter->jsDateFormat
                            ]
                            ])->label(false);
                        ?>
                    </div>
                    <div class="col-md-4">
                        <?= Html::submitButton('<i class="fas fa-file-download"></i>&nbsp;&nbsp;' . Yii::t('common/modules/report', 'Download report'),
                            [
                                'class' => 'btn btn-md btn-flat btn-primary',
                            ]
                        ) ?>
                    </div>
                </div>
            <?php $form::end(); ?>

            <?php endforeach; ?>
            </div>
        </div>

		<?= Box::end(); ?>

	</div>
</div>
