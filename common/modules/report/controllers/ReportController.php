<?php

namespace phycom\common\modules\report\controllers;


use phycom\backend\helpers\UserLanguage;
use phycom\common\helpers\FlashMsg;
use phycom\common\models\traits\WebControllerTrait;

use phycom\common\modules\report\models\ReportForm;
use phycom\common\modules\report\models\reports\ProductPerformanceReport;


use yii\base\Controller;
use yii;

/**
 * Class ReportController
 * @package phycom\common\modules\report\controllers
 */
class ReportController extends Controller
{
	use WebControllerTrait;

    public function beforeAction($action)
    {
        Yii::$app->user->identity && (new UserLanguage(Yii::$app->user->identity))->check();
        return parent::beforeAction($action);
    }

	public function actionIndex()
	{
		$this->checkPermission('search_reports');

		$productPerformanceReport = ReportForm::create(ProductPerformanceReport::class);
		if ($productPerformanceReport->load(Yii::$app->request->post())) {
		    if ($productPerformanceReport->validate() && $productPerformanceReport->calculateReport()) {
                return $productPerformanceReport->exportFile();
            } else {
		        FlashMsg::error($productPerformanceReport->errors);
            }
        }

		return $this->render('index', [
		    'reports' => [$productPerformanceReport]
        ]);
	}
}
