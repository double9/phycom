<?php

namespace phycom\common\components;

use phycom\common\interfaces\CartItemBaseInterface as CartItemInterface;
use phycom\common\interfaces\CartStorageInterface;
use phycom\common\models\ActiveRecord;
use phycom\common\models\cart\storage\DatabaseStorage;
use phycom\common\models\cart\storage\SessionStorage;

use yii;

/**
 * Class Cart provides basic cart functionality (adding, removing, clearing, listing items). You can extend this class and
 * override it in the application configuration to extend/customize the functionality
 *
 * modified version of yii2mod\cart package
 *
 * @package phycom\common\components
 */
class Cart extends yii\base\Component
{
    const EVENT_AFTER_CLEAR = 'eventAfterClear';
    /**
     * Override this to provide custom (e.g. database) storage for cart data
     *
     * @var string|CartStorageInterface
     */
    public $storageClass = SessionStorage::class;

    /**
     * @var array cart items
     */
    protected $items;

    /**
     * @var CartStorageInterface
     */
    private $storage;


	const ITEM_PRODUCT = \phycom\common\interfaces\CartItemInterface::class;
	/**
	 * @var string \phycom\common\interfaces\CartItemDeliveryInterface class name
	 */
	const ITEM_DELIVERY = \phycom\common\interfaces\CartItemDeliveryInterface::class;
    /**
     * @var string \phycom\common\interfaces\CartItemDiscountInterface class name
     */
	const ITEM_DISCOUNT = \phycom\common\interfaces\CartItemDiscountInterface::class;

    /**
     * @var array
     */
	public $itemTypes = [
	    self::ITEM_PRODUCT => 'product',
        self::ITEM_DELIVERY => 'delivery',
        self::ITEM_DISCOUNT => 'discount'
    ];


    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->clear(false);
        $this->setStorage(Yii::createObject($this->storageClass));
        $this->items = $this->getStorage()->load($this);
    }


    /**
     * @return mixed
     */
    public function getStorage()
    {
        return $this->storage;
    }

    /**
     * @param mixed $storage
     */
    public function setStorage($storage)
    {
        $this->storage = $storage;
    }



    /**
     * Add an item to the cart
     *
     * @param CartItemInterface $element
     * @param bool $save
     * @return $this
     */
	public function add(CartItemInterface $element, $save = true)
	{
		if ($element instanceof ActiveRecord) {
			foreach ($element->modelEvents() as $event) {
				$element->off($event);
			}
			foreach ($element->relatedRecords as $recordName => $model) {
			    unset($element->$recordName);
            }
		}

        $this->addItem($element);
        $save && $this->getStorage()->save($this);

        return $this;
	}

	/**
	 * @param CartItemInterface $item
	 */
	protected function addItem(CartItemInterface $item)
	{
		$uniqueId = $item->getUniqueId();
		if (isset($this->items[$uniqueId])) {
			/**
			 * @var $currentItem CartItemInterface
			 * @var $item \phycom\common\interfaces\CartItemProductInterface
			 */
			$currentItem = $this->items[$uniqueId];
			if ($currentItem instanceof \phycom\common\interfaces\CartItemInterface) {
				$currentItem->setQuantity($currentItem->getQuantity() + $item->getQuantity());
			}
		} else {
			$this->items[$uniqueId] = $item;
		}
	}


    /**
     * Removes an item from the cart
     *
     * @param string $uniqueId
     * @param bool $save
     *
     * @throws \yii\base\InvalidParamException
     *
     * @return $this
     */
    public function remove($uniqueId, $save = true)
    {
        if (!isset($this->items[$uniqueId])) {
            throw new yii\base\InvalidArgumentException('Item not found');
        }

        unset($this->items[$uniqueId]);

        $save && $this->getStorage()->save($this);

        return $this;
    }


    /**
     * Assigns cart to logged in user
     *
     * @param string
     * @param string
     */
    public function reassign($sessionId, $userId)
    {
        if (get_class($this->getStorage()) === DatabaseStorage::class) {
            if (!empty($this->items)) {
                $storage = $this->getStorage();
                $storage->reassign($sessionId, $userId);
                self::init();
            }
        }
    }


    /**
     * Delete all items from the cart
     *
     * @param bool $save
     *
     * @return $this
     */
    public function clear($save = true)
    {
        $this->items = [];
        $save && $this->getStorage()->save($this) && $this->trigger(self::EVENT_AFTER_CLEAR);
        return $this;
    }


    /**
     * Returns all items of a delivery type from the cart
     * @return \phycom\common\interfaces\CartItemDeliveryInterface[]
     */
    public function getDeliveryItems()
    {
        return $this->getItems(self::ITEM_DELIVERY);
    }

    /**
     * Returns all items of a discount type from the cart
     * @return \phycom\common\interfaces\CartItemDiscountInterface[]
     */
    public function getDiscountItems()
    {
        return $this->getItems(self::ITEM_DISCOUNT);
    }

	/**
	 * Returns all cart item type from the cart
	 * @return \phycom\common\interfaces\CartItemInterface[]
	 */
	public function getProductItems()
	{
		return $this->getItems(self::ITEM_PRODUCT);
	}

    /**
     * @param mixed $uniqueId
     * @return bool
     */
	public function hasItem($uniqueId)
    {
        return $this->getItem($uniqueId) ? true : false;
    }

    public function save()
    {
        $this->getStorage()->save($this);
    }

    /**
     * @param mixed $uniqueId
     * @return null|CartItemInterface|\phycom\common\interfaces\CartItemInterface
     */
    public function getItem($uniqueId)
    {
        foreach ($this->getItems() as $item) {
            if ($item->getUniqueId() === $uniqueId) {
                return $item;
            }
        }
        return null;
    }

    /**
     * Returns all items of a given type from the cart
     *
     * @param string $itemType One of self::ITEM_ constants
     *
     * @return CartItemInterface[]
     */
    public function getItems($itemType = null)
    {
        $items = $this->items;

        if (!is_null($itemType)) {
            $items = array_filter(
                $items,
                function ($item) use ($itemType) {
                    /* @var $item CartItemInterface */
                    return is_subclass_of($item, $itemType);
                }
            );
        }

        return $items;
    }


    /**
     * @param string $itemType If specified, only items of that type will be counted
     *
     * @return int
     */
    public function getCount($itemType = null)
    {
        return count($this->getItems($itemType));
    }


	
	public function getTotal()
	{
		$total = 0;
		foreach ($this->getItems() as $item) {
            if ($item instanceof \phycom\common\interfaces\CartItemDeliveryInterface) {
                $total += $item->getPrice();
            } elseif ($item instanceof \phycom\common\interfaces\CartItemDiscountInterface) {
                $total -= $item->calculateDiscount();
            } else {
                $total += $item->getTotalPrice();
			}
		}
		return $total;
	}

    /**
     * Finds all items of type $itemType, sums the values of $attribute of all models and returns the sum.
     *
     * @param string $attribute
     * @param string|null $itemType
     *
     * @return int
     */
    public function getAttributeTotal($attribute, $itemType = null)
    {
        $sum = 0;
        foreach ($this->getItems($itemType) as $model) {
            $sum += $model->{$attribute};
        }

        return $sum;
    }

}