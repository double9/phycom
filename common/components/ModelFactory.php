<?php

namespace phycom\common\components;

use phycom\common\models\product\Product;

use yii\helpers\ArrayHelper;
use yii\base\Component;
use yii\base\UnknownPropertyException;
use yii\helpers\Inflector;
use Yii;

/**
 * Class ModelFactory
 *
 * @package phycom\common\components
 *
 * @method \phycom\common\models\User getUser(array $params = [])
 * @method \phycom\common\models\UserTag getUserTag(array $params = [])
 * @method \phycom\common\models\Vendor getVendor(array $params = [])
 * @method \phycom\common\models\Shop getShop(array $params = [])
 * @method \phycom\common\models\VendorUser getVendorUser(array $params = [])
 * @method \phycom\common\models\product\Product getProduct(array $params = [])
 * @method \phycom\common\models\product\Variant getVariant(array $params = [])
 * @method \phycom\common\models\product\VariantOption getVariantOption(array $params = [])
 * @method \phycom\common\models\product\Param getParam(array $params = [])
 * @method \phycom\common\models\product\ParamOption getParamOption(array $params = [])
 * @method \phycom\common\models\product\AttachmentParam getAttachmentParam(array $params = [])
 * @method \phycom\common\models\product\AttachmentParamOption getAttachmentParamOption(array $params = [])
 * @method \phycom\common\models\product\ProductPrice getProductPrice(array $params = [])
 * @method \phycom\common\models\product\ProductPriceVariation getProductPriceVariation(array $params = [])
 * @method \phycom\common\models\product\ProductTag getProductTag(array $params = [])
 * @method \phycom\common\models\product\ProductStatistics getProductStatistics(array $params = [])
 * @method \phycom\common\helpers\ProductPrice getProductPriceHelper(Product $product, $units = null, array $params = [])
 * @method \phycom\common\models\Invoice getInvoice(array $params = [])
 * @method \phycom\common\models\Payment getPayment(array $params = [])
 * @method \phycom\common\models\Order getOrder(array $params = [])
 * @method \phycom\common\models\OrderItem getOrderItem(array $params = [])
 * @method \phycom\common\models\Shipment getShipment(array $params = [])
 * @method \phycom\common\models\ShipmentItem getShipmentItem(array $params = [])
 * @method \phycom\common\models\MessageTemplate getMessageTemplate(array $params = [])
 * @method \phycom\common\models\Setting getSetting(array $params = [])
 * @method \phycom\common\models\UserMessage getUserMessage(array $params = [])
 * @method \phycom\common\rbac\Data getRbacData(array $params = [])
 */
class ModelFactory extends Component
{
	const DEFAULT_NS = 'common\models\\';

    /**
     * @var array - array of namespaces to search for
     */
	public array $fallbackSequence = ['phycom\\'];

    /**
     * @var array - can be extended in child implementation
     */
	protected array $definitions = [];

    /**
     * @var array - contains all model definitions
     */
	private array $repository = [];
    /**
     * @var array - base namespace conf
     */
    private array $commonDefinitions = [
        'getProduct'               => 'common\models\product\Product',
        'getVariant'               => 'common\models\product\Variant',
        'getVariantOption'         => 'common\models\product\VariantOption',
        'getParam'                 => 'common\models\product\Param',
        'getParamOption'           => 'common\models\product\ParamOption',
        'getAttachmentParam'       => 'common\models\product\AttachmentParam',
        'getAttachmentParamOption' => 'common\models\product\AttachmentParamOption',
        'getProductPrice'          => 'common\models\product\ProductPrice',
        'getProductPriceHelper'    => 'common\helpers\ProductPrice',
        'getProductPriceVariation' => 'common\models\product\ProductPriceVariation',
        'getProductTag'            => 'common\models\product\ProductTag',
        'getProductStatistics'     => 'common\models\product\ProductStatistics',
        'getRbacData'              => 'common\rbac\Data'
    ];

    public function init()
    {
        parent::init();
        $this->repository = ArrayHelper::merge($this->commonDefinitions, $this->definitions);
    }

    /**
     * @return array
     */
    public function getModels()
    {
        return $this->repository;
    }

    /**
     * @param array $definitions
     */
    public function setDefinitions(array $definitions)
    {
        $this->definitions = ArrayHelper::merge($this->definitions, $definitions);
    }

	/**
	 * Custom getter for accessing factory model class names like properties $modelFactory->{ModelClassName}
	 *
	 * @param string $name - model name
	 * @return mixed|string
     *
     * @throws UnknownPropertyException
	 */
	public function __get($name)
	{
		try {
			return parent::__get($name);
		} catch (UnknownPropertyException $e) {
			return $this->getClassName($name);
		}
	}

    /**
     * For accessing model instances as methods of model factory: $modelFactory->get{ModelClassName}()
     *
     * @param string $name
     * @param array $arguments
     * @return mixed
     *
     * @throws UnknownPropertyException;
     * @throws \yii\base\InvalidConfigException
     */
	public function __call($name, $arguments)
	{
		try {
			return $this->getModelInstance(substr($name, 3), $arguments);
		} catch (UnknownPropertyException $e) {
			try {
				return parent::__call($name, $arguments);
			} catch (\Exception $e2) {
				throw $e;
			}
		}
	}

    /**
     * @param string $name - class namespace
     * @param array $arguments
     * @return mixed
     *
     * @throws UnknownPropertyException
     * @throws \yii\base\InvalidConfigException
     */
	public function getModelInstance($name, array $arguments = [])
	{
		$className = $this->getClassName($name);
		return Yii::createObject($className, $arguments);
	}

	/**
	 * @param $className
	 * @return string - correct class name path
     *
	 * @throws UnknownPropertyException
	 */
	public function getClassName($className)
	{
        $className = $this->repository['get' . $className] ?? static::DEFAULT_NS . $className;

        if (class_exists($className)) {
            return $className;
        }

        foreach ($this->fallbackSequence as $namespace) {
            if (substr($namespace, 0, 5) === 'psr4:') {
                $fullClassName = substr($namespace, 5) . $this->getPsr4ClassName($className);
                if (class_exists($fullClassName)) {
                    return $fullClassName;
                }
            } else if (class_exists($namespace .  $className)) {
                return $namespace .  $className;
            }
        }

        throw new UnknownPropertyException('Class ' . $className . ' not found.');
	}

    /**
     * @param string $className
     * @return string
     */
    protected function getPsr4ClassName(string $className)
    {
        $parts = explode('\\', $className);
        $name = array_pop($parts);
        $className = '';
        foreach ($parts as $part) {
            if (strlen($part)) {
                $className .= Inflector::camelize($part);
            }
            $className .= '\\';
        }
        return $className . $name;
    }
}
