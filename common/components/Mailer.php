<?php

namespace phycom\common\components;

use yii\mail\BaseMailer;
use yii\mail\MailerInterface;

/**
 * Class Mailer
 * @package phycom\common\components
 */
class Mailer extends BaseMailer implements MailerInterface
{
    public $useFileTransport = false;

    public function compose($view = null, array $params = [])
    {
        return parent::compose($view, $params);
    }

    public function sendMessage($message)
    {
        return $message->send($this);
    }
}