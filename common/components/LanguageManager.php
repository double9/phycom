<?php

namespace phycom\common\components;

use phycom\common\models\attributes\LanguageStatus;
use phycom\common\models\Language;

use yii\helpers\Json;
use yii\base\Component;
use yii;

/**
 * Class LanguageManager
 * @package phycom\common\components
 *
 * @property-read Language $default
 * @property-read Language $current
 * @property-read Language $source
 * @property-read Language[] $visible
 * @property-read Language[] $hidden
 * @property-read Language[] $enabled
 * @property-read Language[] $all
 */
class LanguageManager extends Component
{
	/**
	 * @var Language[]
	 */
	protected ?array $languages = null;
    /**
     * @var string
     */
	private string $defaultLanguageCode;
	/**
	 * @var Language
	 */
	private Language $source;


	public function init()
    {
        parent::init();
        if (!isset($this->defaultLanguageCode)) {
            $this->defaultLanguageCode = Yii::$app->language;
        }
    }

    public function setDefault($language)
    {
        if ($language instanceof Language) {
            $this->defaultLanguageCode = $language->code;
        } else if (is_string($language) && Language::findOne(['code' => substr($language, 0, 2)])) {
            $this->defaultLanguageCode = $language;
        } else {
            throw new yii\base\InvalidArgumentException('Invalid default language ' . Json::encode($language));
        }
    }

    /**
     * @return Language|null
     */
    public function getDefault() : ?Language
    {
        return $this->findLanguageByLocale($this->defaultLanguageCode);
    }

    /**
     * @return Language|null
     */
    public function getCurrent() : ?Language
	{
	    return $this->findLanguageByLocale(Yii::$app->language);
	}

    /**
     * @return Language|null
     */
	public function getSource() : ?Language
	{
		if (!isset($this->source)) {
			$languageCode = substr(Yii::$app->sourceLanguage, 0, 2);
			$this->source = Language::findOne(['code' => $languageCode]);
		}
		return $this->source;
	}

    /**
     * @return array|Language[]
     */
	public function getVisible() : array
	{
		return $this->findByStatus(LanguageStatus::VISIBLE);
	}

    /**
     * @return array|Language[]
     */
	public function getHidden() : array
	{
		return $this->findByStatus(LanguageStatus::HIDDEN);
	}

    /**
     * @return Language[]
     */
	public function getEnabled() : array
	{
		if (is_null($this->languages)) {
			$this->updateLanguageData();
		}
		return $this->languages;
	}

    /**
     * Finds a fallback language - an alternative language to use as replacement in case the content is missing in selected language.
     *
     * @param Language $language
     * @return Language|null
     */
	public function findFallbackLanguage(Language $language) : ?Language
    {
        $fallbackLanguage = $this->getSource();
        if ($language->code === $fallbackLanguage->code) {
            $defaultLanguage = $this->getDefault();
            if ($defaultLanguage->code !== $fallbackLanguage->code) {
                $fallbackLanguage = $defaultLanguage;
            } else {
                foreach ($this->getEnabled() as $language) {
                    if ($language->code !== $fallbackLanguage->code) {
                        $fallbackLanguage = $language;
                        break;
                    }
                }
            }
        }
        return $fallbackLanguage;
    }

    /**
     * @return Language[]
     */
	public function getAll() : array
	{
		return Language::findAll([]);
	}

	/**
	 * @param string $languageCode
	 * @return Language|null
	 */
	public function get($languageCode)
	{
		foreach ($this->enabled as $language) {
			if ($language->code === $languageCode) {
				return $language;
			}
		}
		return null;
	}

    /**
     * @param string $locale
     * @return Language|null
     */
    protected function findLanguageByLocale($locale) : ?Language
    {
        if (is_null($this->languages)) {
            $this->updateLanguageData();
        }
        $languageCode = substr($locale, 0, 2);
        foreach ($this->languages as $language) {
            if ($language->code === $languageCode) {
                return $language;
            }
        }
        return Language::findOne(['code' => $languageCode]);
    }

	/**
	 * @param mixed $status
	 * @return array|Language[]
	 */
	protected function findByStatus($status) : array
	{
		if (!is_array($status)) {
			$status = [$status];
		}
		if (is_null($this->languages)) {
			$this->updateLanguageData();
		}
		return array_filter($this->languages, function($language) use($status) {
			return $language->status->in($status);
		});
	}


	protected function updateLanguageData()
	{
		$this->languages = Language::find()->where(['status' => LanguageStatus::VISIBLE])->all();
	}
}
