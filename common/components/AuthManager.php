<?php

namespace phycom\common\components;

use phycom\common\helpers\Date;
use phycom\common\rbac\Role;

use yii\base\Exception;
use yii\db\Query;
use yii\rbac\Item;
use yii\rbac\Assignment;
use yii\rbac\DbManager;
use yii;

/**
 * Class AuthManager
 * @package phycom\common\components
 */
class AuthManager extends DbManager
{
    public $sessionCache = true;
    public $rootRoles = [];

    /**
     * Improved version with session caching
     *
     * @param int|string $userId
     * @param string $permissionName
     * @param array $params
     * @return bool
     */
    public function checkAccess($userId, $permissionName, $params=[])
    {
        $_access = Yii::$app->session->get('_access');

        if ($this->sessionCache && empty($params) && !Yii::$app->user->isGuest && isset($_access[$permissionName])) {
            return $_access[$permissionName];
        }
        $access = parent::checkAccess($userId, $permissionName, $params);

        if ($this->sessionCache && empty($params) && !Yii::$app->user->isGuest) {
            if (!is_array($_access)) {
                $_access = [];
            }
            $_access[$permissionName] = $access;
            Yii::$app->session->set('_access', $_access);
        }
        return $access;
    }

    /**
     * @param yii\rbac\Rule $rule
     * @return bool
     * @throws Exception
     * @throws yii\db\Exception
     */
    protected function addRule($rule)
    {
        if (!is_subclass_of($rule, '\\yii\\rbac\\Rule')) {
            throw new Exception('invalid auth rule');
        }

        if ($rule->createdAt === null) {
            $rule->createdAt = Date::dbNow();
        }
        if ($rule->updatedAt === null) {
            $rule->updatedAt = Date::dbNow();
        }

        $this->db->createCommand()
            ->insert($this->ruleTable, [
                'name' => $rule->name,
                'data' => get_class($rule),
                'created_at' => $rule->createdAt,
                'updated_at' => $rule->updatedAt,
            ])->execute();

        return true;
    }

    /**
     * @inheritdoc
     */
    public function getRule($name)
    {
        $row = (new Query)->select('*')
            ->from($this->ruleTable)
            ->where(['name' => $name])
            ->one($this->db);

        if ($row === false) {
            return null;
        } else {
            return $this->populateRule($row);
        }
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    private function populateRule($data)
    {
        $className = $data['data'];
        if (!strlen($className)) {
            throw new Exception('undefined auth rule');
        }
        $rule = new $className;
        if (!is_subclass_of($rule, '\\yii\\rbac\\Rule')) {
            throw new Exception('invalid auth rule');
        }
        $rule->name = $data['name'];
        $rule->createdAt = Date::create($data['created_at']);
        $rule->updatedAt = Date::create($data['updated_at']);
        return $rule;
    }

    /**
     * @inheritdoc
     */
    public function getRules()
    {
        $query = (new Query)->from($this->ruleTable);

        $rules = [];
        foreach ($query->all($this->db) as $row) {
            $rules[$row['name']] = $this->populateRule($row);
        }
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function assign($role, $userId)
    {
        $assignment = new Assignment([
            'userId' => $userId,
            'roleName' => $role->name,
            'createdAt' => Date::now(),
        ]);

	    $exists = (new Query())
		    ->select('*')
		    ->from($this->assignmentTable)
		    ->where(['user_id' => $assignment->userId])
		    ->andWhere(['item_name' => $assignment->roleName])
		    ->one();

        if ($exists) {
        	$assignment->createdAt = Date::create($exists['created_at'])->dateTime;
        	return $assignment;
        }

        $this->db->createCommand()
            ->insert($this->assignmentTable, [
                'user_id' => $assignment->userId,
                'item_name' => $assignment->roleName,
                'created_at' => $assignment->createdAt->format('Y-m-d H:i:s'),
            ])->execute();

        return $assignment;
    }

    /**
     * @param int $userId
     * @return array
     */
	public function getDefaultRolesByUser($userId)
	{
		$roles = [];
		foreach ($this->defaultRoles as $name) {
			if ($this->checkAccess($userId, $name)) {
				$role = $this->getRole($name);
				$role->createdAt = null;
				$role->updatedAt = null;
				$roles[] = $role;
			}
		}
		return $roles;
	}

    /**
     * @param bool $includeSpecial
     * @return Item[]|Role[]
     */
    public function getRoles($includeSpecial = true)
    {
        $roles = parent::getRoles();
        if (!$includeSpecial) {
            // exclude root roles and default roles defined in configuration
            foreach ($roles as $key => $role) {
                if (in_array($role->name, $this->defaultRoles)) {
                    unset($roles[$key]);
                    continue;
                }
                if (in_array($role->name, $this->rootRoles)) {
                    unset($roles[$key]);
                }
            }
        }
        return $roles;
    }

    /**
     * Populates an auth item with the data fetched from database.
     * @param array $row the data from the auth item table
     * @return Item the populated auth item instance (either Role or Permission)
     */
    protected function populateItem($row)
    {
        $class = $row['type'] == Item::TYPE_PERMISSION ? yii\rbac\Permission::class : Role::class;

        if (!isset($row['data']) || ($data = @unserialize(is_resource($row['data']) ? stream_get_contents($row['data']) : $row['data'])) === false) {
            $data = null;
        }

        return new $class([
            'name'        => $row['name'],
            'type'        => $row['type'],
            'description' => $row['description'],
            'ruleName'    => $row['rule_name'] ?: null,
            'data'        => $data,
            'createdAt'   => $row['created_at'],
            'updatedAt'   => $row['updated_at'],
        ]);
    }
}