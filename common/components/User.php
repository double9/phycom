<?php

namespace phycom\common\components;

use phycom\common\models\attributes\UserStatus;
use phycom\common\models\attributes\UserType;
use phycom\common\models\UserActivity;
use phycom\common\models\Vendor;

use yii;

/**
 * Class User
 *
 * @package phycom\common\components
 *
 * @property \phycom\common\models\User $identity
 * @property \phycom\common\models\Vendor $vendor
 */
class User extends \yii\web\User
{
    /**
     * @var Vendor
     */
	private Vendor $vendor;

    /**
     * @param $value
     * @return bool
     */
    public function activity($value) : bool
    {
        return UserActivity::create($this->id, $value);
    }

    /**
     * @return Vendor|null
     */
    public function getVendor() : ?Vendor
    {
    	if ($this->identity) {
    		if (!isset($this->vendor)) {
    			$this->vendor = Vendor::findOne(['id' => 1]);
		    }
    		return $this->vendor;
	    }
	    return null;
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function getAdminMessageSubscribers() : yii\db\ActiveQuery
    {
        return Yii::$app->modelFactory->getUser()::find()
            ->where(['type' => UserType::ADMIN])
            ->andWhere(['status' => UserStatus::ACTIVE])
            ->andWhere([
                'or',
                '(settings #>> \'{"receiveOrderNotifications"}\') :: text = \'true\'',
                '(settings #>> \'{"receiveErrorNotifications"}\') :: text = \'true\'',
                '(settings #>> \'{"receiveShipmentNotifications"}\') :: text = \'true\''
            ]);
    }

    /**
     * @param yii\web\IdentityInterface $identity
     */
    public function setLanguage(yii\web\IdentityInterface $identity)
    {
        if ($identity instanceof \phycom\common\models\User && $identity->settings->language) {
            Yii::$app->language = $identity->settings->language;
        }
    }

    /**
     * This method is called after the user is successfully logged in.
     * The default implementation will trigger the [[EVENT_AFTER_LOGIN]] event.
     * If you override this method, make sure you call the parent implementation
     * so that the event is triggered.
     * @param yii\web\IdentityInterface $identity the user identity information
     * @param boolean $cookieBased whether the login is cookie-based
     * @param integer $duration number of seconds that the user can remain in logged-in status.
     * If 0, it means login till the user closes the browser or the session is manually destroyed.
     */
    protected function afterLogin($identity, $cookieBased, $duration)
    {
	    /**
	     * @var \phycom\common\models\User $identity
	     */
        Yii::$app->session->set('roles', Yii::$app->authManager->getRolesByUser($identity->id));

	    $this->setLanguage($identity);
        $this->activity('login');

        parent::afterLogin($identity, $cookieBased, $duration);
    }

    /**
     * @param yii\web\IdentityInterface $identity
     */
    protected function afterLogout($identity)
    {
	    /**
	     * @var \phycom\common\models\User $identity
	     */
        UserActivity::create($identity->id, 'logout');
        parent::afterLogout($identity);
    }
}

