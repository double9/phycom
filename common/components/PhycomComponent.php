<?php

namespace phycom\common\components;


use yii\base\Component;

/**
 * Class PhycomComponent
 *
 * @package phycom\common\components
 */
abstract class PhycomComponent extends Component
{
    public bool $enabled = false;

    public function isEnabled() : bool
    {
        return $this->enabled;
    }
}
