<?php

namespace phycom\common\components;

use phycom\common\exceptions\TranslationMissingException;
use yii\i18n\MessageSource as BaseMessageSource;
use yii\i18n\MissingTranslationEvent;
use yii\i18n\GettextMoFile;
use yii\i18n\GettextPoFile;
use yii;

/**
 * Uses yii gettext messagesource. Language files must be placed in a
 * base dir named as <locale-name>.<extension>
 *
 * For example:
 *
 *  - base-dir/en-US.po for english translations
 */
class MessageSource extends BaseMessageSource
{
	const MO_FILE_EXT = '.mo';
	const PO_FILE_EXT = '.po';

	/**
	 * @var array
	 */
	public $basePath = ['@app/messages'];
	/**
	 * @var string
	 */
	public $catalog = 'messages';
	/**
	 * @var bool
	 */
	public $useMoFile = true;
	/**
	 * @var bool
	 */
	public $useBigEndian = false;


	public function init()
    {
        parent::init();
        // make sure we have an array base path
        if (!is_array($this->basePath)) {
        	$this->basePath = [$this->basePath];
        }
        $this->useMoFile = false;
        $this->sourceLanguage = 'en';

        $this->on(self::EVENT_MISSING_TRANSLATION, function(MissingTranslationEvent $event) {
            $message = 'Translation missing for string: "'. $event->message .'""';
            $e = new TranslationMissingException($message);
//            Yii::$app->newrelic->noticeError($message, $e);
        });
    }

	protected function loadMessages($category, $language)
	{
		$messageFiles = $this->getMessageFilePath($language);
		$messages = $this->loadMessagesFromFile($messageFiles, $category);

		$fallbackLanguage = substr($language, 0, 2);
		$fallbackSourceLanguage = substr($this->sourceLanguage, 0, 2);

		if ($fallbackLanguage !== $language) {
			$messages = $this->loadFallbackMessages($category, $fallbackLanguage, $messages, $messageFiles);
		} elseif ($language === $fallbackSourceLanguage) {
			$messages = $this->loadFallbackMessages($category, $this->sourceLanguage, $messages, $messageFiles);
		} else {
			if ($messages === null) {
				Yii::error("The message file(s) for category '$category' does not exist: " . implode(', ', $messageFiles), __METHOD__);
			}
		}
		return (array) $messages;
	}

	/**
	 * @param string $category
	 * @param string $fallbackLanguage
	 * @param array $messages
	 * @param array $originalMessageFiles
	 * @return array|null
	 */
	protected function loadFallbackMessages($category, $fallbackLanguage, $messages, array $originalMessageFiles)
	{
		$fallbackMessageFiles = $this->getMessageFilePath($fallbackLanguage);
		$fallbackMessages = $this->loadMessagesFromFile($fallbackMessageFiles, $category);

		if (
			$messages === null && $fallbackMessages === null
			&& $fallbackLanguage !== $this->sourceLanguage
			&& $fallbackLanguage !== substr($this->sourceLanguage, 0, 2)
		) {
			Yii::error("The message file(s) for category '$category' does not exist: "
				. implode(', ', $originalMessageFiles) . ". Fallback file(s) does not exist as well: "
				. implode(', ', $fallbackMessageFiles), __METHOD__);

		} elseif (empty($messages)) {
			return $fallbackMessages;
		} elseif (!empty($fallbackMessages)) {
			foreach ($fallbackMessages as $key => $value) {
				if (!empty($value) && empty($messages[$key])) {
					$messages[$key] = $fallbackMessages[$key];
				}
			}
		}
		return (array) $messages;
	}

	/**
	 * Loads the message translation for the specified language and category or returns null if file(s) does not exist.
	 *
	 * @param array $messageFiles paths to message files
	 * @param string $category the message category
	 *
	 * @return array|null array of messages or null if files are not found
	 */
	protected function loadMessagesFromFile($messageFiles, $category)
	{
		$messages = null;
		foreach ($messageFiles as $messageFile) {
			if (is_file($messageFile)) {
				if ($this->useMoFile) {
					$gettextFile = new GettextMoFile(['useBigEndian' => $this->useBigEndian]);
				} else {
					$gettextFile = new GettextPoFile();
				}
				$pathMessages = $gettextFile->load($messageFile, $category);
				if (!is_array($pathMessages)) {
					$pathMessages = [];
				}

				if ($messages === null) {
					$messages = $pathMessages;
				} else {
					$messages = yii\helpers\ArrayHelper::merge($messages, $pathMessages);
				}
			}
		}
		return $messages;
	}


	/**
	 * Returns message file paths for the specified language and category.
	 *
	 * @param string $language the target language
	 * @return array path to message file
	 */
	protected function getMessageFilePath($language)
	{
		$dPos = strrpos($language, '-');
		$languagePath = $dPos ? substr($language, 0, $dPos) : $language;
		$messageFiles = [];
		foreach ($this->basePath as $path) {
			$messageFile = Yii::getAlias($path) . '/' . $languagePath . '/' . $this->catalog;
			if ($this->useMoFile) {
				$messageFile .= self::MO_FILE_EXT;
			} else {
				$messageFile .= self::PO_FILE_EXT;
			}
			$messageFiles[] = $messageFile;
		}
		return $messageFiles;
	}
}
