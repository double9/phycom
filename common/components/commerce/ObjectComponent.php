<?php

namespace phycom\common\components\commerce;

use yii\base\BaseObject;

/**
 * Class ObjectComponent
 *
 * @package phycom\common\components\commerce
 */
abstract class ObjectComponent extends BaseObject
{
    public bool $enabled = true;

    public function isEnabled() : bool
    {
        return $this->enabled;
    }
}
