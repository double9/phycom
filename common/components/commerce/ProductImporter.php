<?php

namespace phycom\common\components\commerce;

use phycom\common\interfaces\CommerceComponentInterface;
use phycom\common\models\product\import\ProductImportStrategyInterface;

/**
 * Class SProductImporter
 *
 * @package phycom\common\components\commerce
 */
class ProductImporter extends ObjectComponent implements CommerceComponentInterface
{
    public string $logCategory = 'product-import';
    /**
     * @param ProductImportStrategyInterface $strategy
     * @param callable|null $onProgress
     * @return bool
     */
    public function import(ProductImportStrategyInterface $strategy, callable $onProgress = null) : bool
    {
        return $strategy->prepare()->import($onProgress);
    }
}

