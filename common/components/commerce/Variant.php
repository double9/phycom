<?php

namespace phycom\common\components\commerce;

use phycom\common\interfaces\CommerceComponentInterface;

/**
 * Class Variant
 *
 * @package phycom\common\components\commerce
 */
class Variant extends ObjectComponent implements CommerceComponentInterface
{
    public array $definitions = [];
}
