<?php

namespace phycom\common\components\commerce;


use phycom\common\interfaces\CommerceComponentInterface;
use phycom\common\interfaces\PhycomComponentInterface;
use phycom\common\modules\payment\Module as PaymentModule;
use phycom\common\modules\delivery\Module as DeliveryModule;

use Yii;
use yii\di\ServiceLocator;

/**
 * Class Commerce
 *
 * @package phycom\common\components\commerce
 *
 * @property-read PaymentModule|CommerceComponentInterface $payment
 * @property-read DeliveryModule|CommerceComponentInterface $delivery
 *
 * @property-read ClientCard $clientCards
 * @property-read PromoCode $promoCodes
 * @property-read Variant $variants
 * @property-read Param $params
 * @property-read Shop $shop
 * @property-read ShopScheduleOpen $shopOpen
 * @property-read ShopScheduleClosed $shopClosed
 * @property-read ShopSupply $shopSupply
 * @property-read ShopContent $shopContent
 * @property-read ProductImporter $productImporter
 */
class Commerce extends ServiceLocator implements PhycomComponentInterface
{
    /**
     * @var bool
     */
    public bool $enabled = true;
    /**
     * @var bool - when true every pricing row also has a SKU field so that SKU could be different for some unit(s)
     */
    public bool $usePricingSkuPerUnit = false;
    /**
     * @var array
     */
    public array $unitLabels = [];

    /**
     * @return bool
     */
    public function isEnabled() : bool
    {
        return $this->enabled;
    }

    /**
     * @return PaymentModule|\yii\base\Module|CommerceComponentInterface|null
     */
    public function getPayment() : ?PaymentModule
    {
        return Yii::$app->getModule('payment');
    }

    /**
     * @return DeliveryModule|\yii\base\Module|CommerceComponentInterface|null
     */
    public function getDelivery() : ?DeliveryModule
    {
        return Yii::$app->getModule('delivery');
    }
}
