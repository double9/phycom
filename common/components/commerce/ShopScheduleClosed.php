<?php

namespace phycom\common\components\commerce;

use phycom\common\interfaces\CommerceComponentInterface;

/**
 * Class ShopScheduleClosed
 *
 * @package phycom\common\components\commerce
 */
class ShopScheduleClosed extends ObjectComponent implements CommerceComponentInterface
{

}
