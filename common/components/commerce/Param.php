<?php

namespace phycom\common\components\commerce;

use phycom\common\interfaces\CommerceComponentInterface;

/**
 * Class Param
 *
 * @package phycom\common\components\commerce
 */
class Param extends ObjectComponent implements CommerceComponentInterface
{
    public array $definitions = [];
}
