<?php

namespace phycom\common\components;

use yii\base\BootstrapInterface;

/**
 * Class Bootstrap
 *
 * @package phycom\common\components
 */
class Bootstrap implements BootstrapInterface
{
	/**
	 * @param \FrontendApplication|\BackendApplication|\ConsoleApplication $app
	 */
	public function bootstrap($app)
	{

    }
}
