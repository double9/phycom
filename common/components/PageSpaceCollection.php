<?php

namespace phycom\common\components;


use phycom\common\models\attributes\PostStatus;
use phycom\common\models\PageSpace;

use yii\helpers\Inflector;
use yii\base\Component;
use yii;

/**
 * Class PageSpaceCollection
 * @package phycom\common\components
 */
class PageSpaceCollection extends Component
{
    /**
     * @var PageSpace[]
     */
    protected $items = [];

    /**
     * @return PageSpace[]
     */
    public function getItems()
    {
        return $this->items;
    }

    public function setItems(array $pageSpace)
    {
        $items = [];
        foreach ($pageSpace as $identifier => $item) {
            if (is_array($item)) {
                if (!isset($item['class'])) {
                    $item['class'] = PageSpace::class;
                }
                if (!isset($item['identifier'])) {
                    $item['identifier'] = $identifier;
                }
            } elseif (is_string($item)) {
                $item = [
                    'class' => PageSpace::class,
                    'identifier' => $item
                ];
            }

            if (!isset($item['name'])) {
                $item['name'] = Inflector::titleize($item['identifier']);
            }

            $item = Yii::createObject($item);
            if (!$item instanceof PageSpace) {
                throw new yii\base\InvalidArgumentException('Invalid PageSpace class');
            }
            $items[] = $item;
        }
        $this->items = $items;
    }

    /**
     * @param string $identifier
     * @param bool $onlyPublished
     * @return \phycom\common\models\Post|\phycom\frontend\models\post\SearchPost|null
     */
    public function getPage(string $identifier, $onlyPublished = false)
    {
        foreach ($this->items as $pageSpace) {
            if ($pageSpace->identifier === $identifier) {
                $page = $pageSpace->getPage();
                if ($onlyPublished && !$page->status->is(PostStatus::PUBLISHED)) {
                    return null;
                };
                return $page;
            }
        }
        return null;
    }
}
