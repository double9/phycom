<?php
namespace phycom\common\components\queue;

use Pheanstalk\Exception\ServerException as PheanstalkException;
use yii\helpers\Console;

class InfoAction extends \yii\queue\beanstalk\InfoAction
{
	/**
	 * Info about queue status.
	 */
	public function run()
	{
		try {
			parent::run();
		} catch (PheanstalkException $e) {
			if ($e->getMessage() === 'Server reported NOT_FOUND') {
				Console::output($this->format('Tube "' . $this->queue->tube . '" is inactive', Console::FG_YELLOW));
			} else {
				throw $e;
			}
		}
	}
}
