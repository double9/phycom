<?php
namespace phycom\common\components\queue;

use Pheanstalk\Exception\ServerException;

use yii\queue\beanstalk\Queue;
use yii\queue\serializers\JsonSerializer;
use yii\queue\LogBehavior;

/**
 * Class Beanstalk
 * @package phycom\common\components\queue
 */
class Beanstalk extends Queue
{
	public $tube = 'default';
	public $commandClass = Command::class;
	public $serializer = JsonSerializer::class;

	public function behaviors()
	{
		return ['log' => LogBehavior::class];
	}

    /**
     * bury's all jobs in tube
     */
	public function buryAll()
    {

        $pheanstalk = $this->getPheanstalk();
        while ($job = $pheanstalk->peekReady($this->tube)) {
            try {
                $pheanstalk->bury($job);
            } catch (ServerException $e) {

            }
        }
    }

    /**
     * Deletes all jobs from tube
     */
    public function clearTube()
    {
        $pheanstalk = $this->getPheanstalk();
        while ($job = $pheanstalk->peekReady($this->tube)) {
            try {
                $pheanstalk->delete($job);
            } catch (ServerException $e) {

            }
        }
    }
}
