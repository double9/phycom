<?php
namespace phycom\common\components\queue;

use yii\queue\db\Queue;
use yii\queue\LogBehavior;
use yii\queue\serializers\JsonSerializer;

/**
 * Class Db
 *
 * @package phycom\common\components\queue
 */
class Db extends Queue
{
    public $commandClass = Command::class;
    public $serializer = JsonSerializer::class;

    public function behaviors()
    {
        return ['log' => LogBehavior::class];
    }

    /**
     * bury's all jobs in tube
     */
    public function buryAll()
    {

    }
}
