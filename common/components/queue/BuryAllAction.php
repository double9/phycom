<?php
namespace phycom\common\components\queue;


use phycom\common\components\queue\Beanstalk as Queue;

use yii\console\ExitCode;
use yii\helpers\Console;


class BuryAllAction extends \yii\queue\cli\Action
{
    /**
     * @var Queue
     */
    public $queue;
    /**
     * Bury all jobs in tube
     */
    public function run()
    {
        Console::output($this->format('Bury all jobs from tube:', Console::FG_GREEN));
        $this->queue->buryAll();
        return ExitCode::OK;
    }
}