<?php

namespace phycom\common\components\queue;

class Command extends \yii\queue\beanstalk\Command
{
	public $isolate = false;
	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'info' => InfoAction::class,
            'bury-all' => BuryAllAction::class
		];
	}
}

