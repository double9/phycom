<?php
namespace phycom\common\fixtures;

use yii\test\ActiveFixture;

class User extends ActiveFixture
{
    public $modelClass = 'phycom\common\models\User';
}