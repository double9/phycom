<?php

namespace phycom\common\rbac;

use yii\helpers\Inflector;
/**
 * Class Role
 * @package phycom\common\rbac
 *
 * @property-read string $displayName
 * @property-read string $displayDescription
 */
class Role extends \yii\rbac\Role
{
    /**
     * @return mixed|string
     */
    public function getDisplayName()
    {
        if ($config = $this->getRoleConfig()) {
            return $config['displayName'] ?? Inflector::titleize($this->name);
        }
        return Inflector::titleize($this->name);
    }

    /**
     * @return string
     */
    public function getDisplayDescription()
    {
        if ($config = $this->getRoleConfig()) {
            return $config['description'] ?? $this->description;
        }
        return $this->description;
    }

    /**
     * @return array|null
     */
    protected function getRoleConfig()
    {
        $roles = (new Data())->getRoles();
        foreach ($roles as $role) {
            if ($role['name'] === $this->name) {
                return $role;
            }
        }
        return null;
    }
}

