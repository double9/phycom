<?php

namespace phycom\common\rbac\rules;

use yii\rbac\Rule;

/**
 * Class NotAuthenticatedRule
 * @package phycom\common\rbac\rules
 */
class NotAuthenticatedRule extends Rule
{
    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        return \Yii::$app->user->isGuest;
    }
}
