<?php

namespace phycom\common\rbac\rules;

use yii;
use yii\rbac\Rule;

/**
 * Class IsSelfRule
 * @package phycom\common\rbac\rules
 */
class IsSelfRule extends Rule
{
    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        if (!$user || Yii::$app->user->isGuest) {
            return false;
        }
        return $user === (int) $params;
    }
}
