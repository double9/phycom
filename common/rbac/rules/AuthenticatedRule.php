<?php

namespace phycom\common\rbac\rules;

use yii\rbac\Rule;

/**
 * Class AuthenticatedRule
 * @package phycom\common\rbac\rules
 */
class AuthenticatedRule extends Rule
{
    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        return !\Yii::$app->user->isGuest;
    }
}
