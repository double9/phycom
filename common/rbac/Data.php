<?php

namespace phycom\common\rbac;

use yii;

/**
 * This is the data according to which the user permissions and rules are created.
 *
 * Class AuthManagerData
 * @package phycom\common\rbac
 *
 */
class Data extends DataHandler
{

    public function getRules()
    {
        return [
            'authenticated'     => 'authenticated',
            'not_authenticated' => 'notAuthenticated',
            'self'              => 'isSelf',
            'owner'             => 'ownsItem'
        ];
    }

    public function getPermissions()
    {
        return [
            ['use_backend', 'Access to administration area.'],

            ['search_users', 'Search registered users'],
            ['search_unregistered_users', 'Search unregistered users'],
	        ['search_special_users', 'Search special users (system, message groups)'],
            ['view_user', 'View user data'],
            ['view_self', 'View self', 'self'],
            ['create_user', 'Create new user account'],
            ['create_backend_user', 'Create new backend user account'],
            ['update_user', 'Modify user profile and settings'],
            ['update_self', 'Modify my own profile and settings', 'self'],

	        ['search_user_activity'],
	        ['invite_backend_users'],
            ['remove_pending_users'],
	        ['view_permissions'],
	        ['change_avatar'],
	        ['change_own_avatar', 'Change own avatar', 'self'],

            ['search_products'],
            ['view_product'],
            ['create_product'],
            ['update_product'],
            ['delete_product'],

	        ['search_product_attributes'],
            ['search_product_params'],

	        ['search_product_categories'],
	        ['view_product_category'],
	        ['update_product_category'],
			['create_product_category'],
	        ['move_product_category'],
	        ['delete_product_category'],

            ['search_post_categories'],
            ['view_post_category'],
            ['update_post_category'],
            ['create_post_category'],
            ['move_post_category'],
            ['delete_post_category'],

            ['search_files'],
            ['view_file'],
            ['view_own_file', 'View file', 'owner'],
            ['hide_file'],
            ['show_file'],

	        ['search'],
	        ['search_newsletter_subscriptions'],
	        ['update_global_settings'],
            ['update_payment_settings'],
            ['update_delivery_settings'],

	        ['search_message_templates'],
	        ['view_message_template'],

	        ['search_promotion_codes'],
	        ['create_promotion_code'],
            ['update_promotion_code'],
            ['delete_promotion_code'],

	        ['search_client_cards'],
			['create_client_card'],
            ['update_client_card'],
            ['delete_client_card'],

	        ['search_reports'],
            ['search_statistics'],

	        ['search_comments'],
	        ['create_comment'],
	        ['update_comment'],
	        ['delete_comment'],

            ['search_reviews'],
            ['create_review'],
            ['update_review'],
            ['delete_review'],

	        ['search_shops'],
	        ['create_shop'],
	        ['update_shop'],
	        ['delete_shop'],

            ['search_vendors'],
            ['create_vendor'],
            ['update_vendor'],
            ['delete_vendor'],

            ['search_posts'],
            ['create_post'],
            ['update_post'],
            ['delete_post'],

            ['search_pages'],
            ['create_page'],
            ['update_page'],
            ['delete_page'],

            ['search_landing_pages'],
            ['create_landing_page'],
            ['update_landing_page'],
            ['delete_landing_page'],

	        ['search_orders'],
	        ['create_order'],
	        ['update_order'],

	        ['search_invoices'],
	        ['create_invoice'],
	        ['update_invoice'],

	        ['search_payments'],
	        ['create_payment'],
	        ['update_payment'],

	        ['search_shipments'],
	        ['create_shipment'],
	        ['update_shipment'],

            ['search_delivery_areas'],
            ['create_delivery_area'],
            ['update_delivery_area'],
            ['delete_delivery_area'],

            ['search_partner_contracts'],
            ['create_partner_contract'],
            ['update_partner_contract'],
            ['delete_partner_contract']
        ];
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return [
            [
                'name'        => 'system',
                'displayName' => Yii::t('common/user', 'System'),
                'description' => Yii::t('common/user', 'System user have all privileges and can be used only by application itself.')
            ],
            [
                'name'        => 'super_admin',
                'displayName' => Yii::t('common/user', 'Super Admin'),
                'description' => Yii::t('common/user', 'Super admin user have all admin role privileges and some extra permissions over other users and entities.')
            ],
            [
                'name'        => 'admin',
                'displayName' => Yii::t('common/user', 'Admin'),
                'description' => Yii::t('common/user', 'Admin user have all privileges.')
            ],
            [
                'name'        => 'shop_admin',
                'displayName' => Yii::t('common/user', 'Shop Admin'),
                'description' => Yii::t('common/user', 'Shop admin can manage shops campaigns and access the purchase data')
            ],
            [
                'name'        => 'product_admin',
                'displayName' => Yii::t('common/user', 'Product Admin'),
                'description' => Yii::t('common/user', 'Product admin can manage products and related content and have all contributor permissions')
            ],
            [
                'name'        => 'contributor',
                'displayName' => Yii::t('common/user', 'Contributor'),
                'description' => Yii::t('common/user', 'Has the privileges to add, view or modify basic content')
            ],
            [
                'name'        => 'user',
                'displayName' => Yii::t('common/user', 'User'),
                'description' => Yii::t('common/user', 'Registered user. Have access to public site and his own profile'),
                'rule'        => 'authenticated'
            ],
            [
                'name'        => 'guest',
                'displayName' => Yii::t('common/user', 'Guest'),
                'description' => Yii::t('common/user', 'Guest users. Have only public site view privileges.'),
                'rule'        => 'not_authenticated'
            ]
        ];
    }

    public function getHierarchy()
    {
        return [

            ###### PERMISSIONS #####

            'update_self' => [
                'update_user',
                'view_self'
            ],
            'view_self' => [
                'view_user'
            ],
            'view_own_file' => [
                'view_file'
            ],
	        'change_own_avatar' => [
	        	'change_avatar',
	        ],

            ###### ROLES #####

            'system' => [
            	'super_admin'
            ],
            'super_admin' => [
                'use_backend',
                'admin',
                'search_unregistered_users',
                'search_vendors',
                'create_vendor',
                'delete_vendor',
                'view_permissions',
            ],
            'admin' => [
                'use_backend',
                'shop_admin',
                'product_admin',
                'search_user_activity',
                'change_avatar',
                'invite_backend_users',
                'remove_pending_users',
                'update_user',
                'delete_product_category',
                'delete_post_category',
                'update_vendor',
                'search_shops',
                'create_shop',
                'delete_shop',
                'search_reports',
                'search_statistics',
                'update_global_settings',
                'search_message_templates',
                'view_message_template',
                'delete_post',
                'delete_page',
                'delete_landing_page',
                'update_payment_settings',
                'update_delivery_settings',
                'delete_delivery_area',
                'search_partner_contracts',
                'create_partner_contract',
                'update_partner_contract',
                'delete_partner_contract',
            ],
	        'shop_admin' => [
		        'use_backend',
                'product_admin',
                'view_user',
                'search_products',
                'search_product_categories',
                'view_product_category',
                'search_shops',
                'update_shop',
                'search_orders',
                'create_order',
                'update_order',
                'search_invoices',
                'create_invoice',
                'update_invoice',
                'search_payments',
                'create_payment',
                'update_payment',
                'search_shipments',
                'create_shipment',
                'update_shipment',
                'search_delivery_areas',
                'create_delivery_area',
                'update_delivery_area',
                'delete_comment',
                'delete_review',
                'search_newsletter_subscriptions',
                'search_promotion_codes',
                'create_promotion_code',
                'update_promotion_code',
                'delete_promotion_code',
                'search_client_cards',
                'create_client_card',
                'update_client_card',
                'delete_client_card',
	        ],
            'product_admin' => [
                'contributor',
                'search_products',
                'create_product',
                'update_product',
                'search_product_categories',
                'view_product_category',
                'update_product_category',
                'create_product_category',
                'move_product_category',
                'delete_product'
            ],
            'contributor' => [
                'use_backend',
                'change_own_avatar',
                'search',
                'search_users',
                'update_self',
                'search_product_attributes',
                'search_product_params',
                'search_posts',
                'create_post',
                'update_post',
                'search_post_categories',
                'view_post_category',
                'update_post_category',
                'create_post_category',
                'move_post_category',
                'search_pages',
                'create_page',
                'update_page',
                'search_landing_pages',
                'create_landing_page',
                'update_landing_page',
                'search_comments',
                'create_comment',
                'update_comment',
                'search_reviews',
                'create_review',
                'update_review',
            ]
        ];
    }
}
