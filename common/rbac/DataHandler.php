<?php

namespace phycom\common\rbac;

use yii\rbac\DbManager;
use yii\base\InvalidConfigException;
use yii\base\Exception;
use yii\db\Expression;
use yii\db\Migration;
use yii\rbac\Item;
use yii;

/**
 * Class DataHandler
 * @package phycom\common\rbac
 *
 * @property array rules
 * @property array permissions
 * @property array roles
 * @property array hierarchy
 */
abstract class DataHandler extends Migration
{
	/**
	 * Permission rules. Used in conjunction with roles and permission items
	 * @return array
	 *     - key: rule name
	 *     - value: Rule class reference
	 */
	abstract public function getRules();

	/**
	 * Permission items
	 * @return array
	 *     0 - permission name
	 *     1 - permission description
	 *     2 - associated rule name
	 */
	abstract public function getPermissions();

	/**
	 * Roles
	 * @return array
	 *     0 - role name
	 *     1 - role description
	 *     2 - rule name associated with role
	 */
	abstract public function getRoles();

	/**
	 * Permission hierarchy
	 * @return array
	 *     - key: parent auth item
	 *     - value: array of child items
	 */
	abstract public function getHierarchy();

	/**
	 * Updates the permission data from the array
	 * @throws \Exception
	 */
	public function reload()
	{
		$transaction = $this->db->beginTransaction();
		try {
			$this->revoke();
			$this->assign();
			$transaction->commit();
		} catch (\Exception $e) {
			$transaction->rollBack();
			throw $e;
		}
	}

    /**
     * @throws InvalidConfigException
     * @return DbManager
     */
    protected function getAuthManager()
    {
        $authManager = Yii::$app->getAuthManager();
        if (!$authManager instanceof DbManager) {
            throw new InvalidConfigException('You should configure "authManager" component to use database before executing this migration.');
        }
        return $authManager;
    }

    /**
     * Clear up the permission items, rules and hierarchy in db
     * @return bool
     * @throws InvalidConfigException
     */
    protected function revoke()
    {
        $authManager = $this->getAuthManager();
        $this->dropConstraints();
        $this->truncateTable($authManager->itemTable);
        $this->truncateTable($authManager->itemChildTable);
        $this->truncateTable($authManager->ruleTable);
        return true;
    }

    /**
     * @throws Exception
     * @throws InvalidConfigException
     * @throws yii\db\Exception
     */
    protected function assign()
    {
        $this->createRules();
        $this->createPermissions();
        $this->createRoles();
        $this->createHierarchy();
        $this->createConstraints();
    }

    /**
     * @throws InvalidConfigException
     */
	protected function dropConstraints()
	{
		$authManager = $this->getAuthManager();
		$this->dropForeignKey($authManager->assignmentTable . '_item_name_fkey', $authManager->assignmentTable);
		$this->dropForeignKey($authManager->itemTable . '_rule_name_fkey', $authManager->itemTable);
		$this->dropForeignKey($authManager->itemChildTable . '_child_fkey', $authManager->itemChildTable);
		$this->dropForeignKey($authManager->itemChildTable . '_parent_fkey', $authManager->itemChildTable);
	}

    /**
     * @throws InvalidConfigException
     */
    protected function createConstraints()
    {
        $authManager = $this->getAuthManager();

        $this->addForeignKey(
            $authManager->assignmentTable . '_item_name_fkey',
            $authManager->assignmentTable,
            'item_name',
            $authManager->itemTable,
            'name',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            $authManager->itemTable . '_rule_name_fkey',
            $authManager->itemTable,
            'rule_name',
            $authManager->ruleTable,
            'name',
            'SET NULL',
            'CASCADE'
        );

        $this->addForeignKey(
            $authManager->itemChildTable . '_child_fkey',
            $authManager->itemChildTable,
            'child',
            $authManager->itemTable,
            'name',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            $authManager->itemChildTable . '_parent_fkey',
            $authManager->itemChildTable,
            'parent',
            $authManager->itemTable,
            'name',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @throws Exception
     * @throws InvalidConfigException
     */
    private function createRules()
    {
        foreach ($this->rules as $rule => $className) {
            if (strlen($className)) {
                $this->createRule($rule, $className);
            } else {
                $this->createRule($rule);
            }
        }
    }

    /**
     * @throws InvalidConfigException
     */
    private function createPermissions()
    {
        foreach ($this->permissions as $permission) {
            $this->createPermission(...$permission);
        }
    }

    /**
     * @throws InvalidConfigException
     */
    private function createRoles()
    {
        foreach ($this->roles as $role) {
            $params = [
                $role['name'],
                $role['description'] ?? null,
                $role['rule'] ?? null,
                $role['data'] ?? null
            ];
            $this->createRole(...$params);
        }
    }

    /**
     * @throws InvalidConfigException
     * @throws yii\db\Exception
     */
    private function createHierarchy()
    {
        foreach ($this->hierarchy as $parent => $children) {
            $this->assignRelation($parent, $children);
        }
    }

    /**
     * @param $name
     * @param null $description
     * @param null $ruleName
     * @param null $data
     * @throws InvalidConfigException
     */
    private function createRole($name, $description = null, $ruleName = null, $data = null)
    {
        $authManager = $this->getAuthManager();
        $this->insert($authManager->itemTable, [
            'name'        => $name,
            'description' => $description,
            'type'        => Item::TYPE_ROLE,
            'rule_name'   => $ruleName,
            'data'        => $data,
            'created_at'  => new Expression('NOW()'),
            'updated_at'  => new Expression('NOW()')
        ]);
    }

    /**
     * @param string $name
     * @param string $description
     * @param string|null $ruleName
     * @param string|null $data
     * @throws InvalidConfigException
     */
    private function createPermission($name, $description = '', $ruleName = null, $data = null)
    {
        $authManager = $this->getAuthManager();
        $this->insert($authManager->itemTable, [
            'name'        => $name,
            'description' => $description,
            'type'        => Item::TYPE_PERMISSION,
            'rule_name'   => $ruleName,
            'data'        => $data,
            'created_at'  => new Expression('NOW()'),
            'updated_at'  => new Expression('NOW()')
        ]);
    }

    /**
     * @param $parent
     * @param $child
     * @throws InvalidConfigException
     * @throws yii\db\Exception
     */
    private function assignRelation($parent, $child)
    {
        $authManager = $this->getAuthManager();
        if (is_array($child)) {
            foreach ($child as $item) {
                $command = $this->db->createCommand(
                    'SELECT "id" FROM ' . $authManager->itemChildTable . ' AS t WHERE ' .
                    't.parent = :parent AND t.child = :child',
                    [
                        'parent' => $parent,
                        'child'  => $item,
                    ]
                );
                if ($command->queryScalar()) {
                    continue;
                }
                $this->insert($authManager->itemChildTable, [
                    'parent' => $parent,
                    'child'  => $item
                ]);
            }
        } else {

            $command = $this->db->createCommand(
                'SELECT "id" FROM ' . $authManager->itemChildTable . ' AS t WHERE ' .
                't.parent = :parent AND t.child = :child',
                [
                    'parent' => $parent,
                    'child'  => $child,
                ]
            );
            if (!$command->queryScalar()) {
                $this->insert($authManager->itemChildTable, [
                    'parent' => $parent,
                    'child'  => $child
                ]);
            }
        }
    }

    /**
     * @param $name
     * @param null $ruleClass
     * @throws Exception
     * @throws InvalidConfigException
     */
    private function createRule($name, $ruleClass=null)
    {
        $authManager = $this->getAuthManager();
        if (!$ruleClass) {
            $ruleClass = $name;
        }
        $rule = '\phycom\common\rbac\rules\\' . ucwords($ruleClass) . 'Rule';
        if (!class_exists($rule)) {
            throw new Exception('rule: "' . $rule . '" not found');
        }
        $this->insert($authManager->ruleTable, [
            'name'       => $name,
            'data'       => $rule,
            'created_at' => new Expression('NOW()'),
            'updated_at' => new Expression('NOW()')
        ]);
    }

}