<?php

namespace phycom\common\events;

use yii\base\Event;

/**
 * Class EventWithPayload
 *
 * @package phycom\common\events
 */
class EventWithPayload extends Event
{
    /**
     * @var mixed
     */
    public $payload;
}
