<?php

namespace phycom\common\events;

use yii\base\Event;
use yii\base\Model;

/**
 * Class ModelUpdateEvent
 * @package phycom\common\events
 */
class ModelUpdateEvent extends Event
{
    /**
     * @var mixed
     */
    public $key;
    /**
     * @var Model
     */
    public $model;
    /**
     * @var array
     */
    public $attributes;
}
