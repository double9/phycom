<?php

namespace phycom\common\events;

use yii\base\Event;

/**
 * Class StatusUpdateEvent
 * @package phycom\common\events
 */
class StatusUpdateEvent extends Event
{
	/**
	 * @var string The status value that had changed and were saved.
	 */
	public $prevStatus;
}