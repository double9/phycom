<?php

namespace phycom\common\models\statistics;

use phycom\common\models\traits\SearchQueryFilter;
use phycom\common\interfaces\SearchModelInterface;

use yii\data\ArrayDataProvider;
use yii\data\Sort;
use yii\base\Model;
use yii\db\Query;
use yii;


/**
 * Class SearchStatistics
 * @package phycom\common\models\statistics
 */
abstract class SearchStatistics extends Model implements SearchModelInterface
{
    use SearchQueryFilter;

    public $from;
    public $to;

    public function rules()
    {
        return [
            [['from','to'],'date', 'format' => 'php:Y-m-d']
        ];
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return yii\data\ArrayDataProvider
     */
    public function search(array $params = [])
    {
        $query = $this->createSearchQuery();
        $dataProvider = new ArrayDataProvider([
            'allModels'  => $query->all(),
            'sort'       => ['defaultOrder' => ['date' => SORT_ASC]],
            'pagination' => false,
        ]);
        $this->sort($dataProvider->sort);
        return $dataProvider;
    }


    protected function sort(Sort $sort)
    {

    }


    /**
     * @return Query
     */
    abstract protected function createSearchQuery();
}