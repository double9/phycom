<?php

namespace phycom\common\models\statistics;

use yii\base\BaseObject;

class SalesIndicators extends BaseObject
{
    public $dailyRevenue;
    public $avgDailyRevenue;
    public $dailyRevenueGrowth;

    public $weeklyRevenue;
    public $avgWeeklyRevenue;
    public $weeklyRevenueGrowth;

    public $monthlyRevenue;
    public $avgMonthlyRevenue;
    public $monthlyRevenueGrowth;

    public $totalRevenue;
    public $avgRevenuePerOrder;

    public $revenue;
    public $avgRevenue;
}