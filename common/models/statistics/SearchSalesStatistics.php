<?php

namespace phycom\common\models\statistics;

use phycom\common\models\attributes\PaymentStatus;
use phycom\common\models\Payment;

use yii\data\Sort;
use yii\db\Query;
use yii\db\Expression;

/**
 * Class SearchSalesStatistics
 * @package phycom\common\models\statistics
 */
class SearchSalesStatistics extends SearchStatistics
{
    /**
     * @return Query
     */
	protected function createSearchQuery()
	{
		$query = (new Query())
            ->select([
                'MAX((p.transaction_time) :: date) AS date',
                'SUM(CASE WHEN p.status = :completed THEN p.amount ELSE 0 END) AS revenue',
            ])
            ->from(['p' => Payment::tableName()])
            ->where(['not', ['p.status' => PaymentStatus::DELETED]])
            ->orderBy(['(p.transaction_time) :: date' => SORT_ASC])
            ->groupBy(['(p.transaction_time) :: date'])
            ->addParams([
                'completed' => PaymentStatus::COMPLETED,
            ]);

		if ($this->from) {
            $query->andWhere(['>=', '(p.transaction_time) :: date', $this->from]);
        }
		if ($this->to) {
            $query->andWhere(['<=', '(p.transaction_time) :: date', $this->to]);
        }
		return $query;
	}
}