<?php

namespace phycom\common\models\statistics;

use phycom\common\models\attributes\OrderStatus;
use phycom\common\models\Order;

use yii\db\Query;

/**
 * Class SearchOrderStatistics
 * @package phycom\common\models\statistics
 */
class SearchOrderStatistics extends SearchStatistics
{
    /**
     * @return Query
     */
	protected function createSearchQuery()
	{
		$query = (new Query())
            ->select([
                'MAX((o.created_at) :: date) AS date',
                'count(o.id) AS count',
                'count(CASE WHEN o.paid_at IS NULL AND o.updated_at < (\'now\' :: timestamp - \'1 hour\' :: interval) THEN 1 END) AS abandoned'
            ])
            ->from(['o' => Order::tableName()])
            ->where(['not', ['o.status' => [
                OrderStatus::DELETED,
                OrderStatus::CANCELED,
                OrderStatus::CLOSED
            ]]])
            ->orderBy(['(o.created_at) :: date' => SORT_ASC])
            ->groupBy(['(o.created_at) :: date']);

		if ($this->from) {
		    $query->andWhere(['>=', '(o.created_at) :: date', $this->from]);
        }
		if ($this->to) {
            $query->andWhere(['<=', '(o.created_at) :: date', $this->to]);
        }
		return $query;
	}
}