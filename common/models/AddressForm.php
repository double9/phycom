<?php

namespace phycom\common\models;

use phycom\common\models\traits\ModelTrait;
use phycom\common\models\attributes\ContactAttributeStatus;
use phycom\common\models\attributes\AddressType;

use yii\helpers\ArrayHelper;
use yii\base\Model;
use yii;

/**
 * Class AddressForm
 * @package phycom\common\models
 *
 * @property-read array $countries
 * @property-read array $divisions
 * @property-read User|Vendor|Shop $model
 */
abstract class AddressForm extends Model
{
	use ModelTrait;

	public $country;
	public $province;
	public $locality;
	public $city;
	public $district;
	public $street;
	public $postcode;

	abstract public function getModel();

	public function rules()
	{
		return [
		    [['country','province','street','postcode'], 'required'],
			['country', 'string', 'length' => 2],
			['country', 'exist', 'targetClass' => Country::class, 'targetAttribute' => 'code'],
			[['province','locality','city','district','street','postcode'], 'string', 'max' => 255]
		];
	}

	public function attributeLabels()
    {
        return [
            'country'  => Yii::t('common/address', 'Country or Territory'),
            'province' => Yii::t('common/address', 'Province or state'),
            'locality' => Yii::t('common/address', 'Locality'),
            'city'     => Yii::t('common/address', 'City'),
            'district' => Yii::t('common/address', 'District'),
            'street'   => Yii::t('common/address', 'Street'),
            'postcode' => Yii::t('common/address', 'Postcode'),
        ];
    }

    public function init()
	{
		if (!$this->country) {
			$this->country = Yii::$app->country;
		}
	}

	public function getCountries()
	{
		$countries = Country::find()->orderBy(['name' => SORT_ASC])->all();
		return ArrayHelper::map($countries, 'code', 'name');
	}

	public function getDivisions()
    {
        if ($this->country) {
            $country = Country::findOne(['code' => $this->country]);

            // In US there are states and state codes
            if ($country->code === 'US') {
                return array_map(function ($item) {return $item['name'];}, $country->divisions);
            } else {
                return ArrayHelper::map($country->divisions, 'name', 'name');
            }
        }
        return [];
    }

	public function populate(Address $address)
    {
        $this->country = $address->country;
        $this->province = $address->province;
        $this->locality = $address->locality;
        $this->city = $address->city;
        $this->district = $address->district;
        $this->street = $address->street;
        $this->postcode = $address->postcode;
    }

	public function update()
	{
		if ($this->validate()) {
			$transaction = Yii::$app->db->beginTransaction();
			try {

				$modelAttribute = $this->getModelAttribute();

				$address = new Address();
				$address->$modelAttribute = $this->model->id;
				$address->country = $this->country;
				$address->province = $this->province;
				$address->locality = $this->locality;
				$address->city = $this->city;
				$address->district = $this->district;
				$address->street = $this->street;
				$address->postcode = $this->postcode;
				$address->type = AddressType::MAIN;
				$address->status = ContactAttributeStatus::UNVERIFIED;

				$similar = $address->getSimilar($modelAttribute);
				if (!empty($similar)) {
					$this->addError('error', Yii::t('common/user', 'Address is similar to existing addresses {addresses}', [
					    'addresses' => json_encode(ArrayHelper::getColumn($similar, 'id'))
                    ]));
					$transaction->rollBack();
					return false;
				}
				if (!$address->save()) {
					$this->setErrors($address->errors);
					$transaction->rollBack();
					return false;
				}

				$transaction->commit();
				return true;
			} catch (\Exception $e) {
				$transaction->rollBack();
				throw $e;
			}
		}
		return false;
	}

	protected function getModelAttribute()
	{
		if ($this->model instanceof User) {
			return 'user_id';
		} else if ($this->model instanceof Vendor) {
			return 'vendor_id';
		} else if ($this->model instanceof Shop) {
			return 'shop_id';
		} else {
			throw new yii\base\InvalidValueException('Invalid value of ' . __CLASS__ . '::getModel()');
		}
	}

}