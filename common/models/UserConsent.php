<?php

namespace phycom\common\models;


use phycom\common\models\behaviors\TimestampBehavior;

use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "user_consent".
 * Every action (either consent is given or revoked) should create a new record. This way all user consent history can be checked at any point of time.
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $partner_contract_id
 * @property boolean $consent               - consent given or false when revoked
 * @property \integer $valid_for            - how many days the given consent is valid
 * @property \DateTime $time                - consent or non-consent time
 * @property \DateTime $created_at
 *
 * @property User $user
 * @property PartnerContract $contract
 */
class UserConsent extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_consent';
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ['created_at', ActiveRecord::EVENT_BEFORE_INSERT],
                    'time'
                ]
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'partner_contract_id', 'consent', 'valid_for', 'time'], 'required'],
            [['user_id', 'partner_contract_id', 'valid_for'], 'integer'],
            [['consent'], 'boolean', 'trueValue' => true, 'falseValue' => false, 'strict' => true],
            [['created_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/user','ID'),
            'user_id' => Yii::t('common/user','User ID'),
            'partner_contract_id' => Yii::t('common/user','Partner contract ID'),
            'consent' => Yii::t('common/user','Consent'),
            'valid_for' => Yii::t('common/user','Valid For'),
            'time' => Yii::t('common/user','Assignment time'),
            'created_at' => Yii::t('common/user','Created At'),
        ];
    }

    /**
     * @return \DateTime|null
     * @throws \Exception
     */
    public function getExpireTime()
    {
        if (!$this->consent) {
            return null;
        }
        $time = clone $this->time;
        $time->add(new \DateInterval('P' . $this->valid_for . 'D'));
        return $time;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(PartnerContract::class, ['id' => 'partner_contract_id']);
    }

    /**
     * @param User $user
     * @param PartnerContract $consentContract
     * @param bool $consentValue
     */
    public function setConsentValue(User $user, PartnerContract $consentContract, bool $consentValue)
    {
        $this->user_id = $user->id;
        $this->consent = (bool) $consentValue;
        $this->partner_contract_id = $consentContract->id;
        $this->valid_for = $consentContract->max_consent_days;
        $this->time = new \DateTime();

        $this->save();
    }
}
