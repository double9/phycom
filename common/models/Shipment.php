<?php

namespace phycom\common\models;

use phycom\common\events\StatusUpdateEvent;
use phycom\common\helpers\f;
use phycom\common\models\attributes\OrderStatus;
use phycom\common\models\attributes\ShipmentStatus;
use phycom\common\models\attributes\ShipmentType;
use phycom\common\models\behaviors\DateBehavior;
use phycom\common\models\behaviors\TimestampBehavior;
use phycom\common\modules\delivery\interfaces\DeliveryMethodInterface;
use phycom\common\modules\delivery\methods\DeliveryMethod;
use phycom\common\modules\delivery\models\CarrierToken;

use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii;

/**
 * This is the model class for table "shipment".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $method - Delivery module method id
 *
 * @property string $shipment_id - shipment id in delivery provider system
 * @property string $rate_id - shipping rate id in delivery provider system
 * @property string $carrier_name
 * @property string $carrier_service
 * @property string $carrier_area
 * @property string $carrier_delivery_days
 * @property string $duration_terms
 *
 * @property \DateTime $delivery_date
 * @property string $delivery_time
 *
 * @property array $from_address
 * @property array $to_address
 * @property array $return_address
 *
 * @property string $transaction_id - shipping label purchase id in delivery provider system
 * @property array $postage_label
 * @property string $tracking_number
 * @property string $tracking_status
 * @property string $tracking_url
 * @property \DateTime $dispatch_at - time (estimate) when the parcel should be collected by carrier
 * @property \DateTime $eta
 * @property \DateTime $original_eta
 *
 * @property ShipmentStatus $status
 * @property ShipmentType $type
 * @property \DateTime $shipped_at - time when the parcel was shipped by carrier
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property Order $order
 * @property ShipmentItem[] $shipmentItems
 * @property OrderItem[] $orderItems
 * @property-read string $label
 * @property-read string $carrierNameLabel
 * @property-read bool $isDelivery
 * @property-read bool $isDeliveryAddressSaved
 * @property-read DeliveryMethodInterface|DeliveryMethod|null $deliveryMethod
 * @property-read array|null $postageLabelData
 * @property-read string $recipientName
 * @property-read string $recipientCompany
 * @property-read string $recipientEmail
 * @property-read string $recipientPhone
 * @property-read string $estimatedArrival
 *
 * @property-read array $messages
 *
 */
class Shipment extends ActiveRecord
{
	const EVENT_AFTER_STATUS_UPDATE = 'afterStatusUpdate';

	protected $messages = [];
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'timestamp' => [
				'class' => TimestampBehavior::class,
				'attributes' => [
					['created_at', ActiveRecord::EVENT_BEFORE_INSERT],
					['updated_at', ActiveRecord::EVENT_BEFORE_INSERT, ActiveRecord::EVENT_BEFORE_UPDATE],
                    'shipped_at',
                    'dispatch_at',
                    'eta',
                    'original_eta'
				]
			],
			'date' => [
			    'class' => DateBehavior::class,
                'attributes' => ['delivery_date']
            ],
			'dynamic-attribute' => [
				'attributes' => [
					'status' => ShipmentStatus::class,
					'type' => ShipmentType::class
				]
			]
		]);
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shipment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'status', 'type'], 'required'],
            [['order_id', 'carrier_delivery_days'], 'integer'],
            [['duration_terms'], 'string'],
            [['from_address', 'to_address', 'return_address', 'postage_label', 'shipped_at', 'created_at', 'updated_at', 'status', 'eta', 'original_eta', 'dispatch_at', 'delivery_date', 'type'], 'safe'],
            [[
                'tracking_number',
                'tracking_status',
                'tracking_url',
                'method',
                'carrier_name',
                'carrier_service',
                'carrier_area',
                'shipment_id',
                'rate_id',
                'transaction_id',
                'delivery_time'
                ],
                'string', 'max' => 255
            ],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getOrder()), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/main', 'ID'),
            'order_id' => Yii::t('common/main', 'Order ID'),
            'method' => Yii::t('common/main', 'Delivery method'),
            'shipment_id' => Yii::t('common/main', 'Delivery method ID'),
            'rate_id' => Yii::t('common/main', 'Rate ID'),
            'transaction_id' => Yii::t('common/main', 'Transaction ID'),
            'carrier_name' => Yii::t('common/main', 'Carrier'),
            'carrier_service' => Yii::t('common/main', 'Carrier Service'),
	        'carrier_area' => Yii::t('common/main', 'Area'),
            'carrier_delivery_days' => Yii::t('common/main', 'Delivery Days'),
            'postage_label' => Yii::t('common/main', 'Postage label'),
            'tracking_number' => Yii::t('common/main', 'Tracking Number'),
            'tracking_status' => Yii::t('common/main', 'Tracking Status'),
            'tracking_url' => Yii::t('common/main', 'Tracking Url'),
            'dispatch_at' => Yii::t('common/main', 'Dispatch At'),
            'delivery_date' => Yii::t('common/main', 'Delivery Date'),
            'delivery_time' => Yii::t('common/main', 'Delivery Time'),
            'eta' => Yii::t('common/main', 'Current ETA'),
            'original_eta' => Yii::t('common/main', 'Original ETA'),
            'from_address' => Yii::t('common/main', 'Sender Address'),
            'to_address' => Yii::t('common/main', 'Destination Address'),
            'return_address' => Yii::t('common/main', 'Return Address'),
            'status' => Yii::t('common/main', 'Status'),
	        'type' => Yii::t('common/main', 'Type'),
            'duration_terms' => Yii::t('common/main', 'Duration terms'),
            'shipped_at' => Yii::t('common/main', 'Shipped At'),
            'created_at' => Yii::t('common/main', 'Created At'),
            'updated_at' => Yii::t('common/main', 'Updated At'),
        ];
    }

    public function init()
    {
	    parent::init();
	    $this->on(self::EVENT_AFTER_STATUS_UPDATE, function ($event) {
		    /**
		     * @var StatusUpdateEvent $event
		     */
		    switch ((string) $this->status) {
			    case ShipmentStatus::DISPATCHED:
			    	if (!$this->order->status->is(OrderStatus::SHIPPED)) {
			    		$this->order->updateStatus(OrderStatus::SHIPPED);
				    }
				    break;
			    case ShipmentStatus::DELIVERED:
			    	if ($this->order->isPaid() && $this->order->shipmentsDelivered()) {
			    		$this->order->updateStatus(OrderStatus::COMPLETE);
				    } else if (!$this->order->status->is(OrderStatus::SHIPPED)) {
					    $this->order->updateStatus(OrderStatus::SHIPPED);
				    }
			    	break;
		    }
	    });
    }

	public function afterSave($insert, $changedAttributes)
	{
		parent::afterSave($insert, $changedAttributes);
		if (in_array('status', array_keys($changedAttributes))) {
			$this->trigger(self::EVENT_AFTER_STATUS_UPDATE, new StatusUpdateEvent([
				'prevStatus' => $changedAttributes['status']
			]));
		}
	}

	public function getRecipientName()
    {
        $a = Json::decode($this->to_address);
        if (isset($a['name'])) {
            return $a['name'];
        }
        if (isset($a['first_name']) && isset($a['last_name'])) {
            return $a['first_name'] . ' ' . $a['last_name'];
        }
        return null;
    }

    public function getRecipientCompany()
    {
        return $this->to_address['company'] ?? null;
    }

    public function getRecipientEmail()
    {
        return $this->to_address['email'] ?? null;
    }

    public function getRecipientPhone()
    {
        return $this->to_address['phone'] ?? $this->order->phone_code . $this->order->phone_number ?? null;
    }

    public function getPostageLabelData()
    {
        return $this->postage_label;
    }

    public function getMessages()
    {
        return $this->messages;
    }

    public function addMessage($message)
    {
        $this->messages[] = $message;
    }

    public function createPostageLabel()
    {
        if (!$this->postage_label) {
            try {
                $label = $this->deliveryMethod->createPostageLabel($this->rate_id);

                if ($label->status !== $label::STATUS_SUCCESS) {
                    foreach ($label->messages as $message) {
                        $this->addError('postage_label', $message->text);
                    }
                    return false;
                } elseif (!empty($label->messages)) {
                    foreach ($label->messages as $message) {
                        $this->addMessage($message->text);
                    }
                }

                $this->transaction_id = $label->referenceId;
                $this->postage_label = $label->export();
                $this->tracking_number = $label->trackingNumber;
                $this->tracking_url = $label->trackingUrl;
                $this->tracking_status = $label->trackingStatus;
                $this->eta = $label->eta;
                $this->original_eta = $label->eta;

                if ($this->save()) {
                    return true;
                } else {
                    Yii::error('Error saving postage label on shipment ' . $this->id . ' errors: ' . json_encode($this->errors), __METHOD__);
                }

            } catch (\Exception $e) {
                Yii::error('Error saving postage label on shipment ' . $this->id . ' ' . $e->getMessage() . ' - ' . $e->getFile() . ':' . $e->getLine(), __METHOD__);
                Yii::error($e->getTraceAsString(), __METHOD__);
                $this->addError('postage_label', 'Error creating postage label: ' . $e->getMessage());
            }
        }
        return false;
    }

	/**
	 * @param $status
	 * @return bool
     * @throws \Exception|\Throwable in case update failed.
	 */
	public function updateStatus($status)
	{
		$this->status = (string) $status;
		if (!$this->update(true, ['status'])) {
			Yii::error('Error updating shipment ' . $this->id . ' status: ' . json_encode($this->errors), __METHOD__);
			return false;
		}
		return true;
	}

    public function generateTrackingNumber()
    {
    	$this->tracking_number = Yii::$app->security->generateRandomString();
    }

    /**
     * @return Address|bool
     */
    public function getIsDeliveryAddressSaved()
    {
        if ($this->to_address && !empty($this->order->user->addresses)) {
            $deliveryAddress = (string) Address::create($this->to_address)->export();
            foreach ($this->order->user->addresses as $address) {
                if ((string)$address->export() === $deliveryAddress) {
                    return $address;
                }
            }
        }
        return false;
    }

    public function getIsDelivery()
    {
        return (string)$this->type === ShipmentType::DELIVERY;
    }

    /**
     * @return \DateTime
     * @throws \Exception
     */
    public function getDeliveryTime()
    {
        $deliveryTime = $this->delivery_date ? clone $this->delivery_date : new \DateTime();
        if ($this->delivery_time) {
            $deliveryTime->setTime((int)substr($this->delivery_time, 0, 2), (int)substr($this->delivery_time, 2, 2));
        }
        return $deliveryTime;
    }

	/**
	 * @return string
	 */
    public function getLabel()
    {
    	return $this->carrier_name ?
            CarrierToken::label($this->carrier_name) :
            ($this->deliveryMethod ? $this->deliveryMethod->getLabel() : $this->method);
    }

    public function getCarrierNameLabel()
    {
        return $this->carrier_name ? CarrierToken::label($this->carrier_name) : null;
    }

    /**
     * @return string
     */
    public function getEstimatedArrival()
    {
        if ($this->eta) {
            return Yii::t('common/shipment', 'approximately {time}', ['time' => f::datetime($this->eta)]);
        }
        $est = '';
        if ($this->carrier_delivery_days) {
            $est .= f::days($this->carrier_delivery_days);
        }
        if ($this->delivery_time) {
            $est .= (strlen($est) ? ' ' : '')  . Yii::t('common/shipment', 'at {time}', ['time' => $this->delivery_time]);
        }
        return strlen($est) ? $est : null;
    }

    /**
     * @return DeliveryMethodInterface|DeliveryMethod|yii\base\Module|null
     */
    public function getDeliveryMethod()
    {
        return Yii::$app->getModule('delivery')->getModule($this->method);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getOrder()), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShipmentItems()
    {
        return $this->hasMany(get_class(Yii::$app->modelFactory->getShipmentItem()), ['shipment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(get_class(Yii::$app->modelFactory->getOrderItem()), ['id' => 'order_item_id'])->viaTable('shipment_item', ['shipment_id' => 'id']);
    }
}
