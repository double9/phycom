<?php

namespace phycom\common\models;

use yii;

/**
 * This is the model class for table "vendor_user".
 *
 * @property integer $vendor_id
 * @property integer $user_id
 * @property \DateTime $created_at
 *
 * @property Vendor $vendor
 * @property User $user
 */
class VendorUser extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vendor_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vendor_id', 'user_id'], 'required'],
            [['vendor_id', 'user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['vendor_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getVendor()), 'targetAttribute' => ['vendor_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vendor_id'  => Yii::t('common/main', 'Vendor ID'),
            'user_id'    => Yii::t('common/main', 'User ID'),
            'created_at' => Yii::t('common/main', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getVendor()), ['id' => 'vendor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'user_id']);
    }
}
