<?php

namespace phycom\common\models;

use Rinvex\Country\CountryLoader;
use Rinvex\Country\CountryLoaderException;

use yii;

/**
 * This is the model class for table "country".
 *
 * @property string $code
 * @property string $iso3
 * @property integer $number
 * @property integer $phonecode
 * @property string $name
 *
 * @property \Rinvex\Country\Country $info
 * @property-read array $divisions
 * @property-read string $measurementSystem
 * @property-read bool $isImperial
 * @property-read bool $isMetric
 * @property-read bool $isEuMember
 * @property-read array $states
 */
class Country extends ActiveRecord
{
    const UNIT_IMPERIAL = 'imperial';
    const UNIT_METRIC = 'metric';

	private $_data;

	protected $euMembers = [
        'AT','BE','BG','HR','CY','CZ','DK','EE','FI','FR','GF','DE','GR','HU','IE','IT','LV','LT','LU','MT','NL','PL','PT','RO','SK','SI','ES','SE','GB'
    ];

	public function behaviors()
	{
		return [];
	}

	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    public static function loadByCondition($key, $value)
    {
        $whereCountries = CountryLoader::where($key, $value);
        $countries = [];
        foreach ($whereCountries as $code => $value) {
            if ($country = static::findOne(['code' => strtoupper($code)])) {
                $countries[] = $country;
            }
        }
        return $countries;
    }

	/**
	 * @param $phoneCode
	 * @return static[]
	 */
    public static function findByPhoneCode($phoneCode)
    {
    	return static::find()->where(['phonecode' => $phoneCode])->all();
    }

    /**
     * @param $phoneCode
     * @return static|null
     */
    public static function findOneByPhoneCode($phoneCode)
    {
        return static::findOne(['phonecode' => $phoneCode]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'iso3', 'name'], 'required'],
            [['number', 'phonecode'], 'integer'],
            [['code'], 'string', 'max' => 2],
            [['iso3'], 'string', 'max' => 3],
            [['name'], 'string', 'max' => 255],
            [['iso3'], 'unique'],
            [['name'], 'unique'],
            [['number'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code' => Yii::t('common/main', 'Code'),
            'iso3' => Yii::t('common/main', 'Iso3'),
            'number' => Yii::t('common/main', 'Number'),
            'phonecode' => Yii::t('common/main', 'Phonecode'),
            'name' => Yii::t('common/main', 'Name'),
        ];
    }

    public function getDivisions()
    {
        if ($info = $this->getInfo()) {
            return $info->getDivisions() ?: [];
        }
        return [];
    }

    public function getIsEuMember()
    {
        return in_array($this->code, $this->euMembers);
    }

    public function getIsImperial()
    {
        return $this->getMeasurementSystem() === self::UNIT_IMPERIAL;
    }

    public function getIsMetric()
    {
        return $this->getMeasurementSystem() === self::UNIT_METRIC;
    }

    public function getMeasurementSystem()
    {
        switch ($this->code) {
            case 'US':
            case 'LR':
            case 'MM':
                return self::UNIT_IMPERIAL;
            default:
                return self::UNIT_METRIC;
        }
    }

	/**
	 * @return \Rinvex\Country\Country|null
	 */
	public function getInfo()
	{
        try {
            if (!$this->_data && $this->code) {
                $this->_data = country($this->code);
            }
            return $this->_data;
        } catch(CountryLoaderException $e) {
            Yii::warning('Country data not available for country ' . $this->code . ': ' . $e->getMessage(), __METHOD__);
            return null;
        }
	}
}
