<?php
namespace phycom\common\models;

use phycom\common\components\ModelFactory;
use phycom\common\helpers\Checksum731;
use phycom\common\helpers\Gravatar;
use phycom\common\helpers\Url;
use phycom\common\jobs\ReferenceNumberJob;
use phycom\common\models\attributes\ContactAttributeStatus;
use phycom\common\models\attributes\FileStatus;
use phycom\common\models\attributes\UserFileType;
use phycom\common\models\attributes\UserMeta;
use phycom\common\models\attributes\UserSetting;
use phycom\common\models\attributes\UserTokenType;
use phycom\common\models\attributes\UserType;
use phycom\common\models\attributes\UserStatus;
use phycom\common\models\attributes\VendorStatus;
use phycom\common\models\behaviors\JsonAttributeBehavior;
use phycom\common\helpers\PhoneHelper;

use yii\base\NotSupportedException;
use yii\helpers\ArrayHelper;
use yii;

/**
 * User AR model
 *
 * @property integer $id
 * @property string $username
 * @property string $personal_code
 * @property \DateTime $birthday
 * @property string $first_name
 * @property string $last_name
 * @property string $company_name
 * @property string $display_name
 * @property string $password_hash
 * @property string $auth_key
 * @property string $reference_number
 * @property integer $failed_login_attempts
 * @property UserType $type
 * @property UserStatus $status
 * @property UserSetting $settings
 * @property UserMeta $meta
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property-read string $fullName
 * @property-read string $displayName
 * @property-read string $avatar
 *
 * @property-write string $password write-only password
 * @property-read UserToken[] $userTokens
 *
 * @property-read yii\rbac\Role[] $roles
 * @property-read UserMessage $userMessage
 *
 * @property-read Order[] $orders
 * @property-read File[] $files
 *
 * @property-read File $avatarImage
 * @property-read File $profileImage
 *
 * @property-read Email[] $emails
 * @property-read Email $email
 *
 * @property-read Phone[] $phones
 * @property-read Phone $phone
 *
 * @property-read Address[] $addresses
 * @property-read Vendor[] $vendors
 */
class User extends ActiveRecord implements yii\web\IdentityInterface
{
    /**
     * @var UserMessage
     */
    private $_userMessage;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

	/**
	 * @param mixed $status
	 * @inheritdoc
	 */
	public static function findIdentity($id, $status = [UserStatus::ACTIVE, UserStatus::PENDING_ACTIVATION])
	{
		return static::findOne(['id' => $id, 'status' => $status]);
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token, $type = null)
	{
		throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
	}

	/**
	 * Search user by username
	 *
	 * @param string $username
	 * @param mixed $status
	 * @return static|null
	 */
	public static function findByUsername($username, $status = [UserStatus::ACTIVE, UserStatus::PENDING_ACTIVATION])
	{
		return static::findOne(['username' => $username, 'status' => $status]);
	}

	/**
	 * Search user by reference number
	 *
	 * @param string $referenceNumber
	 * @param mixed $status
	 * @return static|null
	 */
	public static function findByReferenceNumber($referenceNumber, $status = [UserStatus::ACTIVE, UserStatus::PENDING_ACTIVATION])
	{
		return static::findOne(['reference_number' => $referenceNumber, 'status' => $status]);
	}

	/**
	 * Search user by email address
	 *
	 * @param string $email
	 * @param mixed $status
	 * @return static|null
	 */
	public static function findByEmail($email, $status = [UserStatus::ACTIVE, UserStatus::PENDING_ACTIVATION])
	{
		/**
		 * @var Email[] $emails
		 */
		$emails = Email::find()
			->where(['email' => $email])
			->andWhere('user_id IS NOT NULL')
			->andWhere(['not', ['email.status' => ContactAttributeStatus::DELETED]])
			->all();

		$count = count($emails);
		if ($count === 0) {
			return null;
		} else if ($count === 1) {
			return static::findOne(['id' => $emails[0]->user_id, 'status' => $status]);
		} else {
			Yii::error('Error: '.$count.' records with same email address '.$email.' found');
			return null;
		}
	}

	/**
	 * Search user by phone
	 *
	 * @param string $phone
	 * @param string $phoneCode - optional
	 * @param mixed $status
	 * @return static|null
	 */
	public static function findByPhone($phone, $phoneCode = null, $status = [UserStatus::ACTIVE, UserStatus::PENDING_ACTIVATION])
	{
		if (!$phoneCode) {
			$phoneCode = Yii::$app->phone->defaultCode;
		}
		/**
		 * @var Email[] $emails
		 */
		$phones = Phone::find()
			->where(['phone_nr' => $phone])
			->andWhere(['country_code' => $phoneCode])
			->andWhere('user_id IS NOT NULL')
			->all();

		$count = count($phones);
		if ($count === 0) {
			return null;
		} else if ($count === 1) {
			return static::findOne(['id' => $phones[0]->user_id, 'status' => $status]);
		} else {
			Yii::error('Error: '.$count.' records with same phone '.$phone.' found');
			return null;
		}
	}

	/**
	 * Finds user by password reset token
	 *
	 * @param string $token password reset token
	 * @return static|null
	 */
	public static function findByPasswordResetToken($token)
	{

		$query = static::find()->innerJoinwith([
			'userTokens' => function ($query) use ($token) {
				/**
				 * @var Query $query
				 */
				$query->alias('t')
					->where('t.token = :key AND t.used = :used AND t.type = :type', [
						':key' => $token,
						':type' => UserTokenType::PASSWORD_RESET,
						':used' => 0
					])
					->andWhere('t.expires_at IS NULL OR t.expires_at > NOW()');
			}
		]);

		$res = $query->one();
		return $res;
	}

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
	        'timestamp' => [
		        'attributes' => [
			        ['created_at', ActiveRecord::EVENT_BEFORE_INSERT],
			        ['updated_at', ActiveRecord::EVENT_BEFORE_INSERT, ActiveRecord::EVENT_BEFORE_UPDATE],
			        'birthday'
		        ]
	        ],
	        'dynamic-attribute' => [
	        	'attributes' => ['status' => UserStatus::class, 'type' => UserType::class]
	        ],
	        'json-attribute' => [
	        	'class' => JsonAttributeBehavior::class,
	        	'passOwner' => true,
	        	'attributes' => ['settings' => UserSetting::class, 'meta' => UserMeta::class]
	        ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        	[['first_name', 'last_name', 'status', 'type', 'created_at', 'updated_at'], 'safe'],
	        [['company_name', 'display_name', 'reference_number'], 'string', 'max' => 255],
            ['status', 'in', 'range' => UserStatus::all()],
        ];
    }

    public function init()
    {
	    parent::init();
	    $this->on(self::EVENT_AFTER_INSERT, function($event) {
	        Yii::$app->queue1->delay(1)->push(new ReferenceNumberJob(['id' => $this->id]));
	    });
    }

    public function __toString()
    {
        return $this->fullName;
    }


    public function hasTag($tagName)
    {
		return Yii::$app->modelFactory->getUserTag()::findOne(['user_id' => $this->id, 'value' => $tagName]);
    }

	/**
	 * @param string $tagName
	 * @return UserTag|bool
	 */
    public function tag($tagName)
    {
		$tag = Yii::$app->modelFactory->getUserTag();
	    if (!in_array($tagName, $tag::all())) {
	        throw new yii\base\InvalidArgumentException('Invalid user ' . $this->id . ' tag ' . json_encode($tagName));
	    }
	    $existingTag = $this->hasTag($tagName);
	    if (!$existingTag) {
		    $tag->value = $tagName;
		    $tag->user_id = $this->id;
		    $tag->save();
		    return $tag;
	    }
	    return $existingTag;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function generateReferenceNumber()
    {
    	if ($this->isNewRecord) {
    		throw new yii\base\InvalidCallException('Error generating reference number. Invalid record state');
	    }
	    $nr = (string) $this->id;
    	$length = 8 - strlen($nr);
	    for($i = 0; $i < $length; $i++) {
		    $nr .= (string) mt_rand(0, 9);
	    }
	    $this->reference_number = $nr . (string) Checksum731::calculate($nr);
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
		$token = new UserToken();
		$token->user_id = $this->id;
		$token->populateRelation('user', $this);
		$token->type = UserTokenType::PASSWORD_RESET;
		$token->expires_at = (new \DateTime())->add(new \DateInterval('P3D'));
		$token->generate()->save();
	    if ($token->errors) {
	    	Yii::error($token->errors, __METHOD__);
		    return null;
	    }
	    return $token;
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
    	foreach ($this->userTokens as $token) {
    		if ((string)$token->type === UserTokenType::PASSWORD_RESET) {
    			$token->setExpired();
		    }
	    }
    }

	/**
	 * @param string $type
	 * @return UserToken|null
	 */
    public function getToken($type)
    {
        return UserToken::find()
	        ->where(['user_id' => $this->id])
	        ->andWhere(['type' => $type])
	        ->orderBy(['id' => SORT_DESC])
	        ->one();
    }

    /**
     * @return UserMessage
     */
    public function getUserMessage()
    {
        if (!$this->_userMessage) {
            $this->_userMessage = Yii::$app->modelFactory->getUserMessage([$this]);
        }
        return $this->_userMessage;
    }

	/**
	 * Fetch user setting.
	 * @param string $key - setting key
	 * @return array|mixed|null
	 */
	public function getSetting($key)
	{
		return $this->settings->$key;
	}

	/**
	 * Assign user setting.
	 * To save the data User model's settings attribute must be updates as the settings are stored in this column
	 *
	 * @param string $key
	 * @param mixed $value
	 * @param bool $save
	 * @return bool
	 */
	public function setSetting($key, $value, $save = true)
	{
		$this->settings->$key = $value;
		if ($save && !$this->isNewRecord && !$this->save(false, ['settings'])) {
			Yii::error($this->errors, 'user');
			return false;
		}
		return $this->settings->$key;
	}

	/**
     * @param int $size
	 * @return string
	 */
	public function getAvatar($size = 128)
	{
        if ($this->avatarImage) {
            return $this->avatarImage->url;
        }
        if ($this->type->isClient) {
            return Gravatar::createUrl($this->email, ['size' => $size]);
        } else {
            return Gravatar::createUrl($this->email, ['size' => $size, 'default' => 'mm']);
        }
	}

    /**
     * @return string
     */
	public function getImage()
    {
        if ($this->profileImage) {
            return $this->profileImage->url;
        }
        return Url::cachedTo('@web/images/default-profile.svg');
    }


    /**
     * Check if user has an email
     *
     * @param $email
     * @return Email|bool
     */
	public function hasEmail($email)
    {
        foreach ($this->emails as $model) {
            if ($model->email === $email) {
                return $model;
            }
        }
        return false;
    }

    /**
     * Check if user has phone
     *
     * @param string $phoneNumber - full international phone number including phone code
     * @return Phone|bool
     */
    public function hasPhone($phoneNumber)
    {
        $localNumber = PhoneHelper::getNationalPhoneNumber($phoneNumber);
        $phoneCode = PhoneHelper::getPhoneCode($phoneNumber);
        foreach ($this->phones as $model) {
            if ($model->phone_nr === $localNumber && $model->country_code === $phoneCode) {
                return $model;
            }
        }
        return false;
    }

    public function delete()
    {
        $this->status = UserStatus::DELETED;
        return $this->save();
    }

    public function getEmail()
	{
		return !empty($this->emails) ? $this->emails[0] : null;
	}

	public function getPhone()
	{
		return !empty($this->phones) ? $this->phones[0] : null;
	}

	/**
	 * @return array|yii\rbac\Role[]
	 */
	public function getRoles()
	{
		return Yii::$app->authManager->getRolesByUser($this->id);
	}

	public function getFullName()
	{
		return trim($this->first_name . ' ' . $this->last_name);
	}

	public function getDisplayName()
    {
        return $this->display_name ?: $this->first_name;
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEmails()
	{
		return $this->hasMany(Email::class, ['user_id' => 'id'])->orderBy(['created_at' => SORT_DESC])->andWhere(['not', ['email.status' => ContactAttributeStatus::DELETED]]);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPhones()
	{
		return $this->hasMany(Phone::class, ['user_id' => 'id'])->orderBy(['created_at' => SORT_DESC])->andWhere(['not', ['phone.status' => ContactAttributeStatus::DELETED]]);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAddresses()
	{
		return $this->hasMany(Address::class, ['user_id' => 'id'])->orderBy(['created_at' => SORT_DESC])->andWhere(['not', ['address.status' => ContactAttributeStatus::DELETED]]);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUserTokens()
	{
		return $this->hasMany(UserToken::class, ['user_id' => 'id']);
	}

    /**
     * @return yii\db\ActiveQuery
     */
	public function getOrders()
    {
        return $this->hasMany(get_class(Yii::$app->modelFactory->getOrder()), ['user_id' => 'id']);
    }

	/**
	 * @return \yii\db\ActiveQuery
	 * @param mixed $status
	 */
	public function getVendors($status = null)
	{
		$query = $this->hasMany(get_class(Yii::$app->modelFactory->getVendor()), ['id' => 'vendor_id'])
			->andWhere(['not', ['vendor.status' => VendorStatus::DELETED]])
			->viaTable(VendorUser::tableName(), ['user_id' => 'id']);

		if ($status) {
			$query->andWhere(['status' => $status]);
		}
		return $query;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getFiles()
	{
		return $this->hasMany(File::class, ['id' => 'file_id'])->viaTable(UserFile::tableName(), ['user_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProfileImage()
	{
		return $this->hasOne(File::class, ['id' => 'file_id'])
			->viaTable(UserFile::tableName(), ['user_id' => 'id'], function($query) {
				/**
				 * @var yii\db\QueryInterface $query
				 */
				$query->andWhere([UserFile::tableName() . '.type' => UserFileType::PROFILE_IMAGE]);
			})
			->andWhere(['not', ['file.status' => FileStatus::DELETED]]);
	}

    /**
     * @return \yii\db\ActiveQuery
     */
	public function getAvatarImage()
    {
        return $this->hasOne(File::class, ['id' => 'file_id'])
            ->viaTable(UserFile::tableName(), ['user_id' => 'id'], function($query) {
                /**
                 * @var yii\db\QueryInterface $query
                 */
                $query->andWhere([UserFile::tableName() . '.type' => UserFileType::AVATAR]);
            })
            ->andWhere(['not', ['file.status' => FileStatus::DELETED]]);
    }
}
