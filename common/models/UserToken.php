<?php

namespace phycom\common\models;

use phycom\common\models\attributes\UserTokenType;
use phycom\common\helpers\Date;
use phycom\common\models\behaviors\TimestampBehavior;

use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "user_token".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $token
 * @property array $data
 * @property UserTokenType $type
 * @property \DateTime $expires_at
 * @property integer $used
 * @property integer $created_by
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property-read bool isValid
 * @property-read User $user
 */
class UserToken extends ActiveRecord
{

	/**
	 * @param string $key - token
	 * @param UserTokenType|string $type
	 * @return static|null
	 */
	public static function findValid($key, $type = null)
	{
		$query = static::find()->where('token = :key AND (expires_at IS NULL OR expires_at > NOW())', [':key' => $key]);
		if ($type) {
			$query->andWhere(['type' => $type]);
		}
		return $query->one();
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'timestamp' => [
				'class' => TimestampBehavior::class,
				'attributes' => [
					['created_at', ActiveRecord::EVENT_BEFORE_INSERT],
					['updated_at', ActiveRecord::EVENT_BEFORE_INSERT, ActiveRecord::EVENT_BEFORE_UPDATE],
					'expires_at'
				]
			],
			'dynamic-attribute' => [
				'attributes' => ['type' => UserTokenType::class]
			]
		]);
	}

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_token';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'used', 'created_by'], 'integer'],
            [['token', 'type'], 'required'],
            [['expires_at', 'created_at', 'updated_at', 'data'], 'safe'],
            [['token'], 'string', 'max' => 255],
            [['token'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['user_id' => 'id']],
	        [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/main', 'ID'),
            'user_id' => Yii::t('common/main', 'User ID'),
            'token' => Yii::t('common/main', 'Token'),
	        'data' => Yii::t('common/main', 'Data'),
            'type' => Yii::t('common/main', 'Type'),
            'expires_at' => Yii::t('common/main', 'Expires At'),
            'used' => Yii::t('common/main', 'Used'),
	        'created_by' => Yii::t('common/main', 'Created By'),
            'created_at' => Yii::t('common/main', 'Created At'),
            'updated_at' => Yii::t('common/main', 'Updated At'),
        ];
    }

    public function init()
    {
	    parent::init();
	    $this->on( ActiveRecord::EVENT_BEFORE_INSERT, function () {
	    	if ($this->used === null) {
	    		$this->used = 0;
		    }
		    $this->created_by = Yii::$app->user->id;

		    // We'll expire previous user tokens with similar attributes
		    self::updateAll(
			    [
				    'expires_at' => Date::dbNow(),
				    'updated_at' => Date::dbNow()
			    ],
			    'user_id = :user AND type = :type AND expires_at > NOW()',
			    [
			    	'user' => $this->user_id,
				    'type' => $this->type
			    ]
		    );
	    });
    }

	public function generate()
    {
	    $this->token = Yii::$app->getSecurity()->generateRandomString(64);
	    return $this;
    }

    public function setExpired()
    {
    	$this->expires_at = Date::now();
    	$this->save();
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getParam($key)
    {
        return $this->data[$key] ?? null;
    }

    public function setParam($key, $value, $save = false)
    {
        $this->data[$key] = $value;
        if ($save) {
            return $this->save();
        }
        return true;
    }

	/**
	 * @return bool
	 */
    public function getIsValid()
    {
    	return !$this->expires_at || $this->expires_at > (new \DateTime('now'));
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'user_id']);
    }
}
