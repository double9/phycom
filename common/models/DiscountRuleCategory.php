<?php

namespace phycom\common\models;


use phycom\common\models\product\ProductCategory;

/**
 * Class DiscountCardCategory
 * @package phycom\common\models
 *
 * @property integer $id
 * @property integer $discount_rule_id
 * @property integer $product_category_id
 * @property \DateTime $created_at
 *
 * @property ProductCategory $category
 * @property DiscountRule $discountCard
 */
class DiscountRuleCategory extends \phycom\common\models\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'discount_rule_category';
	}

	public function rules()
	{
		return [
			[['discount_rule_id', 'product_category_id'], 'integer'],
			[['discount_rule_id', 'product_category_id'], 'required'],
			[['created_at'], 'safe'],
			[['product_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::class, 'targetAttribute' => ['product_category_id' => 'id']],
			[['discount_rule_id'], 'exist', 'skipOnError' => true, 'targetClass' => DiscountRule::class, 'targetAttribute' => ['discount_rule_id' => 'id']]
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCategory()
	{
		return $this->hasOne(ProductCategory::class, ['id' => 'product_category_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDiscountCard()
	{
		return $this->hasOne(DiscountRule::class, ['id' => 'discount_rule_id']);
	}

}