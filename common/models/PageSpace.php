<?php

namespace phycom\common\models;


use phycom\common\helpers\Url;

use yii\base\BaseObject;
use yii\helpers\Inflector;
use yii;

/**
 * Class PageSpace
 * @package phycom\common\models
 *
 * @property-read string $label
 */
class PageSpace extends BaseObject
{
    /**
     * @var string
     */
    public $identifier;

    /**
     * @var string
     */
    public $name;

    /**
     * @var bool - If true multiple pages can use same PageSpace otherwise only one page can use a certain PageSpace
     */
    public bool $multiple = false;

    /**
     * @return string
     */
    public function getLabel()
    {
        return Inflector::humanize($this->name ?: $this->identifier);
    }

    /**
     * @return array
     */
    public function getRoute()
    {
        return ['page/' . $this->identifier, 'url-language' => Yii::$app->language];
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return Url::toFeRoute(['/page/view', 'key' => $this->identifier]);
    }

    /**
     * @return \phycom\frontend\models\post\SearchPost|\phycom\common\models\Post|null
     */
    public function getPage()
    {
        return Yii::$app->modelFactory->getSearchPost()::findByKey($this->identifier);
    }
}
