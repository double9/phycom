<?php

namespace phycom\common\models;

use phycom\common\validators\PhoneInputValidator;

use yii\helpers\Json;
use yii;

/**
 * This is the model class for table "address_meta". Contains additional info related to an address record
 *
 * @property integer $id
 * @property integer $address_id
 * @property string $name
 * @property string $first_name
 * @property string $last_name
 * @property string $company
 * @property string $email
 * @property string $phone
 * @property string $lat
 * @property string $lng
 * @property string $description
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property-write string $firstName
 * @property-write string $lastName
 *
 * @property Address $address
 */
class AddressMeta extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address_meta';
    }

    /**
     * @param string $json
     * @return static
     */
    public static function create($json)
    {
        return new static(Json::decode($json));
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'first_name', 'last_name', 'company', 'email', 'phone'], 'trim'],
            [['name', 'first_name', 'last_name', 'company', 'email', 'phone'], 'string', 'max' => 255],
            ['email', 'email'],
            ['phone', PhoneInputValidator::class],
            [['lat','lng'], 'number'],
            [['description'], 'string'],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::class, 'targetAttribute' => ['address_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/main', 'ID'),
            'address_id' => Yii::t('common/main', 'Address ID'),
            'name' => Yii::t('common/main', 'Name'),
            'first_name' => Yii::t('common/main', 'First name'),
            'last_name' => Yii::t('common/main', 'Last name'),
            'company' => Yii::t('common/main', 'Company'),
	        'email' => Yii::t('common/main', 'Email'),
            'phone' => Yii::t('common/main', 'phone'),
            'lat' => Yii::t('common/main', 'Latitude'),
            'lng' => Yii::t('common/main', 'Longitude'),
            'description' => Yii::t('common/main', 'Description'),
            'created_at' => Yii::t('common/main', 'Created At'),
            'updated_at' => Yii::t('common/main', 'Updated At'),
        ];
    }


    public function setFirstName($value)
    {
        $this->first_name = $value;
    }

    public function setLastName($value)
    {
        $this->last_name = $value;
    }

    public function __toString()
    {
        $exportData = [];
        foreach ($this->getAttributes(null, ['id','created_at','updated_at','first_name','last_name']) as $key => $value) {
            if ($value) {
                $exportData[$key] = $value;
            }
        }
        if ($this->first_name)
            $exportData['firstName'] = $this->first_name;
        if ($this->last_name)
            $exportData['lastName'] = $this->last_name;

	    return Json::encode($exportData);
    }

	/**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(Address::class, ['id' => 'address_id']);
    }
}
