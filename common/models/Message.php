<?php

namespace phycom\common\models;

use phycom\common\components\FileStorage;
use phycom\common\helpers\Date;
use phycom\common\jobs\MessageJob;
use phycom\common\models\attributes\MessagePriority;
use phycom\common\models\attributes\MessageStatus;
use phycom\common\models\attributes\MessageType;
use phycom\common\models\behaviors\TimestampBehavior;
use Mailgun\Constants\ExceptionMessages;
use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "message".
 *
 * @property integer $id
 *
 * @property integer $user_from
 * @property string $from
 * @property string $from_name
 *
 * @property integer $user_to
 * @property string $to
 * @property string $to_name
 *
 * @property string $subject
 * @property string $content
 *
 * @property MessageStatus $status
 * @property MessageType $type
 * @property MessagePriority $priority
 * @property integer $parent_id
 * @property \DateTime $delivery_time
 * @property \DateTime $dispatched_at
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property Message $parent
 * @property User $userFrom
 * @property User $userTo
 * @property MessageAttachment[] $attachments
 */
class Message extends ActiveRecord
{
	const EVENT_AFTER_DISPATCHED = 'after_dispatched';
	const EVENT_AFTER_QUEUED = 'after_queued';

	const PRIORITY_HIGH = 'P1';
	const PRIORITY_LOW = 'P2';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

	/**
	 * @param string $templateId
	 * @param array $params
     * @param string $language
	 * @return Message
     * @throws yii\base\NotSupportedException
	 */
    public static function createByTemplateId($templateId, array $params = [], $language = null)
    {
    	if (!$template = Yii::$app->modelFactory->getMessageTemplate()::getTemplate($templateId)) {
    	    throw new yii\base\InvalidArgumentException('Template ' . $templateId . ' not found');
	    }
    	return static::createByTemplate($template, $params, $language);
    }

    /**
     * @param MessageTemplate $template
     * @param array $params
     * @param string $language
     * @return Message
     * @throws yii\base\NotSupportedException
     */
    public static function createByTemplate(MessageTemplate $template, array $params = [], $language = null)
    {
	    $message = new static();
	    $message->type = $template->type;
        $message->subject = $template->renderTitle($params, $language);
	    $message->content = $template->render($params, $language);
	    $message->status = MessageStatus::NEW;
	    $message->user_from = Yii::$app->systemUser->id;
	    return $message;
    }

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'timestamp' => [
				'class' => TimestampBehavior::class,
				'attributes' => [
					['created_at', ActiveRecord::EVENT_BEFORE_INSERT],
					['updated_at', ActiveRecord::EVENT_BEFORE_INSERT, ActiveRecord::EVENT_BEFORE_UPDATE],
					'delivery_time',
					'dispatched_at'
				]
			],
			'dynamic-attribute' => [
				'attributes' => [
					'status' => MessageStatus::class,
					'type' => MessageType::class,
					'priority' => MessagePriority::class
				]
			]
		]);
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content', 'status', 'type'], 'required'],
            ['user_from', 'required', 'when' => function () {
                return empty($this->from);
            }],
            ['from', 'required', 'when' => function () {
                return !$this->user_from;
            }],
            ['user_to', 'required', 'when' => function () {
                return empty($this->to);
            }],
            ['to', 'required', 'when' => function () {
                return !$this->user_to;
            }],
            [['user_from', 'user_to', 'parent_id'], 'integer'],
            [['content'], 'string'],
            [['dispatched_at', 'created_at', 'updated_at', 'priority'], 'safe'],
            [['subject', 'from', 'to', 'from_name', 'to_name'], 'string', 'max' => 255],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => static::class, 'targetAttribute' => ['parent_id' => 'id']],
            [['user_from'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['user_from' => 'id']],
            [['user_to'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['user_to' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/message', 'ID'),
            'user_from' => Yii::t('common/message', 'User From'),
            'from' => Yii::t('common/message', 'From'),
            'user_to' => Yii::t('common/message', 'User To'),
            'to' => Yii::t('common/message', 'To'),
            'subject' => Yii::t('common/message', 'Subject'),
            'content' => Yii::t('common/message', 'Content'),
            'status' => Yii::t('common/message', 'Status'),
            'type' => Yii::t('common/message', 'Type'),
	        'priority' => Yii::t('common/message', 'Priority'),
            'parent_id' => Yii::t('common/message', 'Parent ID'),
	        'delivery_time' => Yii::t('common/message', 'Delivery Time'),
            'dispatched_at' => Yii::t('common/message', 'Dispatched At'),
            'created_at' => Yii::t('common/message', 'Created At'),
            'updated_at' => Yii::t('common/message', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
    	if (!$this->priority) {
    		$this->priority = MessagePriority::PRIORITY_DEFAULT;
	    }
	    return parent::beforeSave($insert);
    }

    /**
     * @return bool
     * @throws yii\base\Exception
     */
	public function queue()
    {
    	if ($this->isNewRecord) {
    		throw new yii\base\Exception('Message must be saved before it can be queued');
	    }
	    $queue = $this->priority->isUrgent ? Yii::$app->queue1 : Yii::$app->queue2;
    	$job = new MessageJob(['id' => $this->id]);
    	if ($this->delivery_time) {
		    $timeout = max(0, ($this->delivery_time->getTimestamp() - (new \DateTime())->getTimestamp()));
    		if ($timeout > 0) {
    		    $jobId = $queue->delay($timeout);
    		    return $this->afterMessageQueued($jobId);
			}
	    }
	    $jobId = $queue->push($job);
	    return $this->afterMessageQueued($jobId);
    }

    /**
     * @return bool
     *
     * @throws \phycom\common\modules\email\EmailException
     * @throws \phycom\common\modules\sms\SmsException
     * @throws yii\base\NotSupportedException
     */
    public function dispatch()
    {
	    if ($this->type->isEmail) {
            /**
             * @var \phycom\common\modules\email\Module $emailModule
             */
            $emailModule = Yii::$app->getModule('email');
            $emailModule->send($this);
            return $this->afterMessageDispatched();

        } elseif ($this->type->isSms) {

            /**
             * @var \phycom\common\modules\sms\Module $smsModule
             */
            $smsModule = Yii::$app->getModule('sms');
            $smsModule->sendMessage($this);
            return $this->afterMessageDispatched();

	    } else {
		    throw new yii\base\NotSupportedException('Message type ' . $this->type . ' transport is not yet supported');
	    }
    }

	protected function afterMessageQueued($jobId)
	{
		$this->status = MessageStatus::QUEUED;
		Yii::debug('Message ' . $this->id . ' was queued at ' . Date::now()->format('Y-m-d') . ' with job id ' . $jobId);
		if (!$this->save()) {
			Yii::error($this->errors, __CLASS__);
			Yii::error($this->attributes, __CLASS__);
			return false;
		}
		$this->trigger(self::EVENT_AFTER_QUEUED);
		return $jobId;
	}

	protected function afterMessageDispatched()
	{
		$this->status = MessageStatus::DISPATCHED;
		$this->dispatched_at = Date::now();
		Yii::debug('Message ' . $this->id . ' was dispatched at ' . $this->dispatched_at->format('Y-m-d'));
		if (!$this->save()) {
			Yii::error($this->errors, __CLASS__);
			Yii::error($this->attributes, __CLASS__);
			return false;
		}
		$this->trigger(self::EVENT_AFTER_DISPATCHED);
		return true;
	}

    /**
     * @return string|null
     */
    public function getSenderName()
    {
        if ($this->from_name) {
            return $this->from_name;
        }
        if ($this->user_from) {
            return $this->userFrom->getFullName();
        }
        return null;
    }

    /**
     * @return string|null
     */
	public function getRecipientName()
    {
        if ($this->to_name) {
            return $this->to_name;
        }
        if ($this->user_to) {
            return $this->userTo->getFullName();
        }
        return null;
    }



	public function getAttachmentFiles()
	{
		$attachments = $this->getAttachments()->all();
		$res = [];
		if ($attachments) {
			foreach ($attachments as $attachment) {
				$res[] = $attachment->url;
			}
		}
		return $res;
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(static::class, ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserFrom()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'user_from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTo()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'user_to']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(MessageAttachment::class, ['message_id' => 'id']);
    }
}
