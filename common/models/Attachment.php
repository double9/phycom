<?php

namespace phycom\common\models;

use phycom\common\models\behaviors\Sortable;
use yii;

/**
 * Class Attachment
 * @package phycom\common\models
 *
 * @property integer $file_id
 * @property array $meta
 * @property integer $order
 * @property \DateTime $created_at
 *
 * @property File $file
 *
 * @method insertBefore(Attachment $target)
 * @method appendTo(Attachment $target)
 * @method insertAfter(Attachment $target)
 * @method setOrderByIndex(int $index)
 */
abstract class Attachment extends ActiveRecord
{
    protected $targetAttributeName;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['sortable'] = [
            'class' => Sortable::class,
            'condition' => function ($model) {
                return [$this->targetAttributeName => $model->{$this->targetAttributeName}];
            },
            'parentAttribute' => null
        ];
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[$this->targetAttributeName, 'file_id'], 'required'],
            [[$this->targetAttributeName, 'file_id', 'order'], 'integer'],
            [['created_at', 'meta'], 'safe'],
            [['file_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::class, 'targetAttribute' => ['file_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'file_id'    => Yii::t('common/main', 'File ID'),
            'order'      => Yii::t('common/main', 'Order'),
            'meta'       => Yii::t('common/main', 'Meta'),
            'created_at' => Yii::t('common/main', 'Created At'),
        ];
    }

    public function updateOrder(int $order)
    {

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(File::class, ['id' => 'file_id']);
    }
}