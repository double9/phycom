<?php

namespace phycom\common\models;

use yii;

/**
 * This is the model class for table "post_attachment".
 *
 * @property integer $post_id
 * @property Post $post
 */
class PostAttachment extends Attachment
{
    protected $targetAttributeName = 'post_id';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_attachment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [[$this->targetAttributeName], 'exist', 'skipOnError' => true, 'targetClass' => Post::class, 'targetAttribute' => [$this->targetAttributeName => 'id']];
        return $rules;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::class, ['id' => $this->targetAttributeName]);
    }
}
