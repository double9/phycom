<?php

namespace phycom\common\models;

use yii;

/**
 * The followings are the available columns in table 'audit_trail':
 * @var integer $id
 * @var string $new_value
 * @var string $old_value
 * @var string $action
 * @var string $model
 * @var string $created_at
 * @var integer $user_id
 * @var string $model_id
 */
class AuditTrail extends ActiveRecord
{
    const ACTION_CREATE = 'CREATE';
    const ACTION_CHANGE = 'CHANGE';
    const ACTION_DELETE = 'DELETE';

    /**
     * @return string the associated database table name
     */
    public static function tableName()
    {
	    return '{{%audit_trail}}';
    }

    public function behaviors()
    {
        return [];
    }

    public static function actions()
    {
        return [
            self::ACTION_CREATE => Yii::t('common/trail','Create'),
            self::ACTION_CHANGE => Yii::t('common/trail','Change'),
            self::ACTION_DELETE => Yii::t('common/trail','Delete'),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/trail','ID'),
            'old_value' => Yii::t('common/trail','Old Value'),
            'new_value' => Yii::t('common/trail','New Value'),
            'action' => Yii::t('common/trail','Action'),
            'model' => Yii::t('common/trail','Type'),
            'created_at' => Yii::t('common/trail','Created At'),
            'user_id' => Yii::t('common/trail','User'),
            'model_id' => Yii::t('common/trail','Model ID'),
        ];
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            [['action', 'model', 'created_at', 'model_id'], 'required'],
            ['action', 'string', 'max' => 255],
            ['model', 'string', 'max' => 255],
            ['model_id', 'string', 'max' => 255],
            ['user_id', 'string', 'max' => 255],
            [['old_value', 'new_value'], 'safe']
        ];
    }

	/**
	 * @param yii\db\Query $query
	 */
    public static function recently($query)
    {
        $query->orderBy(['[[created_at]]' => SORT_DESC]);
    }

	/**
	 * @return yii\db\ActiveQuery
	 */
    public function getUser()
    {
	    return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'user_id']);
    }

	/**
	 * @return \phycom\common\models\ActiveRecord
	 */
    public function getParent()
    {
        $modelName = $this->model;
        return new $modelName;
    }
}