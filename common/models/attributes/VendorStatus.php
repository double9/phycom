<?php

namespace phycom\common\models\attributes;

use yii;
/**
 * Represents the status attribute on Vendor model
 *
 * Class VendorStatus
 * @package phycom\common\models\attributes
 */
class VendorStatus extends EnumAttribute
{
	const ACTIVE = 'active';
	const INACTIVE = 'inactive';
	const DELETED = 'deleted';

    public function attributeLabels()
    {
        return [
            self::ACTIVE   => Yii::t('common/main', 'Active'),
            self::INACTIVE => Yii::t('common/main', 'Inactive'),
        ];
    }
}