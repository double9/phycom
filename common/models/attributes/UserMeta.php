<?php

namespace phycom\common\models\attributes;

use phycom\common\models\User;

use yii;

/**
 * Class UserMeta
 * @package phycom\common\models\attributes
 *
 * @property-read array $socialMediaLinks
 */
class UserMeta extends JsonAttribute
{
	public $profession;
	public $about;

	public $facebook;
	public $twitter;
	public $google;
	public $linkedIn;

	protected $socialMediaAttributes = [
	    'facebook',
        'twitter',
        'google',
        'linkedIn'
    ];
    /**
     * @var User
     */
	protected $owner;

    public function attributeLabels()
	{
        return [
            'profession' => Yii::t('common/main', 'Profession'),
            'about'      => Yii::t('common/main', 'About'),
        ];
	}

	public function rules()
    {
        return [
            [['about', 'profession', 'facebook', 'twitter', 'google', 'linkedIn'], 'trim'],
            [['about'], 'string'],
            [['profession', 'facebook', 'twitter', 'google', 'linkedIn'], 'string', 'max' => 255]
        ];
    }

    /**
     * @return array
     */
    public function getSocialMediaLinks()
    {
        $links = [];
        foreach ($this->socialMediaAttributes as $attribute) {
            if ($this->$attribute) {
                $links[$attribute] = $this->$attribute;
            }
        }
        return $links;
    }
}