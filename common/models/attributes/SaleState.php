<?php

namespace phycom\common\models\attributes;

use yii;
/**
 * Class SaleState
 * @package phycom\common\models\attributes
 */
class SaleState extends EnumAttribute
{
	const OPEN = 'open';
	const CLOSED = 'closed';

    public function attributeLabels()
    {
        return [
            self::OPEN   => Yii::t('common/main', 'Open'),
            self::CLOSED => Yii::t('common/main', 'Closed'),
        ];
    }
}