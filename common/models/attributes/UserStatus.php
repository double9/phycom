<?php

namespace phycom\common\models\attributes;

use yii;
/**
 * Represents the status attribute on User model
 *
 * Class UserStatus
 * @package phycom\common\models\attributes
 */
class UserStatus extends EnumAttribute
{
	const HIDDEN = 'hidden';                            // user is hidden cannot login and not shown on reports etc. (assigned to users who have ordered without registration)
	const PENDING = 'pending';                          // the account has been created by sending the invitation but is pending user confirmation (cannot log in)
	const PENDING_ACTIVATION = 'pending_activation';    // user has signed up but not activated his/her account (can log in)
	const ACTIVE = 'active';                            // account is active and ready to use
	const BLOCKED = 'blocked';                          // account is blocked, no login is possible
	const DELETED = 'deleted';                          // account is deleted

    public function attributeLabels()
    {
        return [
            self::HIDDEN             => Yii::t('common/user', 'Hidden'),
            self::PENDING            => Yii::t('common/user', 'Pending'),
            self::PENDING_ACTIVATION => Yii::t('common/user', 'Pending Activation'),
            self::ACTIVE             => Yii::t('common/user', 'Active'),
            self::BLOCKED            => Yii::t('common/user', 'Blocked')
        ];
    }

	public function getLabelClass()
	{
		switch ($this->value) {
			case self::ACTIVE:
				return $this->cssClassPrefix . 'success';
			case self::DELETED:
			case self::BLOCKED:
				return $this->cssClassPrefix . 'danger';
			case self::PENDING_ACTIVATION:
				return $this->cssClassPrefix . 'warning';
			default:
				return $this->cssClassPrefix . 'default';
		}
	}
}