<?php

namespace phycom\common\models\attributes;

use yii;

/**
 * Represents the unit mode of the product pricing. Unit mode defines rules for which unit selection is available for the product.
 *
 * Class PriceUnitMode
 * @package phycom\common\models\attributes
 *
 * @property-read string $unitLabel
 */
class PriceUnitMode extends EnumAttribute
{
	const ANY = 'any';          // all units can be selected regardless of the product_price record exists or not
	const STRICT = 'strict';    // only units defined in product_price table can be selected
    const LIMIT = 'limit';      // units limited by min max value


	public function attributeLabels()
    {
        return [
            self::ANY    => Yii::t('common/product', 'Any'),
            self::STRICT => Yii::t('common/product', 'Strict'),
            self::LIMIT  => Yii::t('common/product', 'Limit'),
        ];
    }
}