<?php

namespace phycom\common\models\attributes;

use yii;
/**
 * Represents the status attribute on Subscription model
 *
 * Class SubscriptionStatus
 * @package phycom\common\models\attributes
 */
class SubscriptionStatus extends EnumAttribute
{
	const ACTIVE = 'active';
	const INACTIVE = 'inactive';
	const OPT_OUT = 'opt_out';

    public function attributeLabels()
    {
        return [
            self::ACTIVE   => Yii::t('common/subscription', 'Active'),
            self::INACTIVE => Yii::t('common/subscription', 'Inactive'),
            self::OPT_OUT  => Yii::t('common/subscription', 'Opt out'),
        ];
    }
}