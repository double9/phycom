<?php

namespace phycom\common\models\attributes;

use yii;

/**
 * Represents the customer_type attribute on Invoice model
 *
 * Class CustomerType
 * @package phycom\common\models\attributes
 */
class CustomerType extends EnumAttribute
{
	const INDIVIDUAL = 'individual';
	const COMPANY = 'company';

    public function attributeLabels()
    {
        return [
            self::INDIVIDUAL => Yii::t('common/main', 'Individual'),
            self::COMPANY    => Yii::t('common/main', 'Company')
        ];
    }
}