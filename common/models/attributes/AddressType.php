<?php

namespace phycom\common\models\attributes;

use yii;
/**
 * Represents the type attribute on Address model
 *
 * Class AddressType
 * @package phycom\common\models\attributes
 */
class AddressType extends EnumAttribute
{
	const MAIN = 'main';
	const INVOICE = 'invoice';
	const SHIPPING = 'shipping';
	const OTHER = 'other';

	public function attributeLabels()
    {
        return [
            self::MAIN     => Yii::t('common/address', 'Main'),
            self::INVOICE  => Yii::t('common/address', 'Invoice'),
            self::SHIPPING => Yii::t('common/address', 'Shipping'),
            self::OTHER    => Yii::t('common/address', 'Other')
        ];
    }
}