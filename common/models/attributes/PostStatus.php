<?php

namespace phycom\common\models\attributes;

use yii;
/**
 * Represents the status attribute on Post model
 *
 * Class PostStatus
 * @package phycom\common\models\attributes
 */
class PostStatus extends EnumAttribute
{
	const DRAFT = 'draft';
	const PUBLISHED = 'published';
	const HIDDEN = 'hidden';
	const DELETED = 'deleted';

	public function attributeLabels()
    {
        return [
            self::DRAFT     => Yii::t('common/post', 'Draft'),
            self::PUBLISHED => Yii::t('common/post', 'Published'),
            self::HIDDEN    => Yii::t('common/post', 'Hidden'),
        ];
    }


    public function getLabelClass()
    {
        if (in_array($this->value, [self::DELETED])) {
            return $this->cssClassPrefix . 'danger';
        }
        if (in_array($this->value, [self::PUBLISHED])) {
            return $this->cssClassPrefix . 'success';
        }
        return parent::getLabelClass();
    }
}