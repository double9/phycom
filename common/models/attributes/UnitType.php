<?php

namespace phycom\common\models\attributes;

use yii;

/**
 * Represents the unit of the product measurement and pricing
 *
 * Class UnitType
 * @package phycom\common\models\attributes
 *
 * @property-read string $unitLabel
 */
class UnitType extends EnumAttribute
{
	const WEIGHT_G = 'weight_g';                // unit of weight in grams
	const WEIGHT_KG = 'weight_kg';              // unit of weight in kilograms
	const PACKAGE_SIZE = 'pack_size';           // unit type where one or more units can be bundled in one package
	const PIECE = 'piece';                      // default unit where one unit is one piece

	public function attributeLabels()
    {
        return [
            self::WEIGHT_G     => Yii::t('common/product', 'Weight (grams)'),
            self::WEIGHT_KG    => Yii::t('common/product', 'Weight (kilograms)'),
            self::PACKAGE_SIZE => Yii::t('common/product', 'Package Size'),
            self::PIECE        => Yii::t('common/product', 'Piece'),
        ];
    }

	public function getIsWeight()
    {
        return in_array($this->value, [self::WEIGHT_G, self::WEIGHT_KG]);
    }

	/**
	 * @return array
	 */
	public static function unitLabels()
	{
        return [
            self::WEIGHT_G     => 'g',
            self::WEIGHT_KG    => 'kg',
            self::PACKAGE_SIZE => 'pkg',
            self::PIECE        => 'pc',
        ];
	}

	public function getUnitLabel()
	{
		return static::unitLabels()[$this->value];
	}
}
