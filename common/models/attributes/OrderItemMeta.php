<?php

namespace phycom\common\models\attributes;

use yii;

/**
 * Class OrderItemMeta
 * @package phycom\common\models
 */
class OrderItemMeta extends JsonAttribute
{
	public $title;

	public function attributeLabels()
	{
		return [
			'title' => Yii::t('common/main', 'Title'),
		];
	}
}