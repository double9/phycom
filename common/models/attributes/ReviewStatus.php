<?php

namespace phycom\common\models\attributes;

use yii;
/**
 * Represents the status attribute on Review model
 *
 * Class ReviewStatus
 * @package phycom\common\models\attributes
 */
class ReviewStatus extends EnumAttribute
{
	const PENDING = 'pending';
	const APPROVED = 'approved';
	const REJECTED = 'rejected';
	const DELETED = 'deleted';

    public function attributeLabels()
    {
        return [
            self::PENDING  => Yii::t('common/review', 'Pending'),
            self::APPROVED => Yii::t('common/review', 'Approved'),
            self::REJECTED => Yii::t('common/review', 'Rejected'),
        ];
    }

    public function isPending()
    {
        return $this->value === self::PENDING;
    }
}