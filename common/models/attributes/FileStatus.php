<?php

namespace phycom\common\models\attributes;

use yii;
/**
 * Represents the status attribute on File model
 *
 * Class FileStatus
 * @package phycom\common\models\attributes
 */
class FileStatus extends EnumAttribute
{
	const PROCESSING = 'processing';
	const VISIBLE = 'visible';
	const HIDDEN = 'hidden';
	const DELETED = 'deleted';

    public function attributeLabels()
    {
        return [
            self::PROCESSING => Yii::t('common/file', 'Processing'),
            self::VISIBLE    => Yii::t('common/file', 'Visible'),
            self::HIDDEN     => Yii::t('common/file', 'Hidden')
        ];
    }
}