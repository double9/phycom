<?php

namespace phycom\common\models\attributes;


use phycom\common\interfaces\DynamicAttributeInterface;

use yii\base\InvalidArgumentException;
use yii\base\BaseObject;

use Yii;

/**
 * Represents the priority attribute on Message model
 *
 * Class MessagePriority
 * @package phycom\common\models\attributes
 *
 * @property integer $value
 * @property bool isUrgent
 * @property bool isCritical
 */
class MessagePriority extends BaseObject implements DynamicAttributeInterface
{
	const PRIORITY_1 = 'P_512'; // most urgent
    const PRIORITY_2 = 'P_1024';
    const PRIORITY_3 = 'P_2048';
    const PRIORITY_4 = 'P_4096';
    const PRIORITY_5 = 'P_8192'; // least urgent
    const PRIORITY_DEFAULT = 'P_2048';

	private $value;

    /**
     * @param null $value
     * @return static|object
     * @throws \yii\base\InvalidConfigException
     */
	public static function create($value = null)
	{
		return Yii::createObject(static::class, [$value]);
	}

	public function __construct($value = null, array $config = [])
	{
		$this->value = $this->normalize($value);
		parent::__construct($config);
	}

	public function getIsCritical()
	{
		return $this->value < 1024;
	}

	public function getIsUrgent()
	{
		return $this->value < 2048;
	}

	public function getValue()
	{
		return $this->value;
	}

	/**
	 * @param mixed $value
	 * @return bool
	 */
	public function is($value)
	{
		return $this->value === $this->normalize($value);
	}

	/**
	 * @param array $values
	 * @return bool
	 */
	public function in(array $values)
	{
		$values = array_map(function($value) {
			return $this->normalize($value);
		}, $values);
		return in_array($this->value, $values);
	}

	public function __toString()
	{
		return (string) $this->value;
	}

	public function export()
	{
		return 'P_' . $this->value;
	}

	/**
	 * @param $priority
	 * @return int
	 */
	protected function normalize($priority)
	{
		if ($priority instanceof static) {
			return $priority->value;
		} else if (is_string($priority)) {
			return (int) substr($priority, 2);
		} else if (is_numeric($priority)) {
			return (int) $priority;
		} else {
			throw new InvalidArgumentException('Invalid message priority: ' . json_encode($priority));
		}
	}

}
