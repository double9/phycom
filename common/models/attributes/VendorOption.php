<?php

namespace phycom\common\models\attributes;

use yii;

/**
 * Class VendorOption
 * @package phycom\common\models\attributes
 *
 */
class VendorOption extends JsonAttribute
{
    public $vatNumber;
    public $paymentAccounts = [];

    public function attributeLabels()
    {
        return [
            'vatNumber'       => Yii::t('common/main', 'VAT Number'),
            'paymentAccounts' => Yii::t('common/main', 'Payment accounts'),
        ];
    }
}
