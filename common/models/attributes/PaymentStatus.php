<?php

namespace phycom\common\models\attributes;

use yii;

/**
 * Represents the status attribute on Payment model
 *
 * Class PaymentStatus
 * @package phycom\common\models\attributes
 *
 * @property-read bool $isPending
 */
class PaymentStatus extends EnumAttribute
{
	const PENDING = 'pending';
	const COMPLETED = 'completed';
	const CANCELED = 'cancelled';
	const FAILED = 'failed';
	const EXPIRED = 'expired';
	const DELETED = 'deleted';

	public function attributeLabels()
    {
        return [
            self::PENDING   => Yii::t('common/payment', 'Pending'),
            self::COMPLETED => Yii::t('common/payment', 'Completed'),
            self::CANCELED  => Yii::t('common/payment', 'Canceled'),
            self::FAILED    => Yii::t('common/payment', 'Failed'),
            self::EXPIRED   => Yii::t('common/payment', 'Expired'),
        ];
    }

    public function getLabelClass()
	{
		switch ($this->value) {
			case self::PENDING:
				return $this->cssClassPrefix . 'warning';
			case self::COMPLETED:
				return $this->cssClassPrefix . 'success';
			case self::DELETED:
			case self::CANCELED:
            case self::EXPIRED:
            case self::FAILED:
				return $this->cssClassPrefix . 'danger';
			default:
				return $this->cssClassPrefix . 'default';
		}
	}

	public function getIsPending()
    {
        return $this->value === self::PENDING;
    }
}