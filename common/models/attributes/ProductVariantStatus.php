<?php

namespace phycom\common\models\attributes;

use yii;
/**
 * Represents the status attribute on ProductVariant model
 *
 * Class ProductVariantStatus
 * @package phycom\common\models\attributes
 */
class ProductVariantStatus extends EnumAttribute
{
	const VISIBLE = 'visible';
	const HIDDEN = 'hidden';

    public function attributeLabels()
    {
        return [
            self::VISIBLE => Yii::t('common/variant', 'Visible'),
            self::HIDDEN  => Yii::t('common/variant', 'Hidden')
        ];
    }
}
