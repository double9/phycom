<?php

namespace phycom\common\models\attributes;

use yii;
/**
 * Represents the type attribute on UserFile model
 *
 * Class UserFileType
 * @package phycom\common\models\attributes
 */
class UserFileType extends EnumAttribute
{
	const PROFILE_IMAGE = 'profile_image';
	const AVATAR = 'avatar';
	const DOCUMENT = 'document';
	const OTHER = 'other';

    public function attributeLabels()
    {
        return [
            self::PROFILE_IMAGE => Yii::t('common/file', 'Profile image'),
            self::AVATAR        => Yii::t('common/file', 'Avatar'),
            self::DOCUMENT      => Yii::t('common/file', 'Document'),
            self::OTHER         => Yii::t('common/file', 'Other'),
        ];
    }
}