<?php

namespace phycom\common\models\attributes;

use yii;
/**
 * Represents the type attribute on User model
 *
 * Class UserType
 * @package phycom\common\models\attributes
 *
 * @property-read bool $isAdmin
 * @property-read bool $isClient
 * @property-read bool $isUnregistered
 */
class UserType extends EnumAttribute
{
	/**
	 * registered customer
	 */
	const CLIENT = 'client';
	/**
	 * admin user - can access the backend area
	 */
	const ADMIN = 'admin';


    public function attributeLabels()
    {
        return [
            self::CLIENT => Yii::t('common/user', 'Client'),
            self::ADMIN  => Yii::t('common/user', 'Admin'),
        ];
    }


    public function getIsAdmin()
	{
		return $this->value === static::ADMIN;
	}

	public function getIsClient()
	{
		return $this->value === static::CLIENT;
	}

	public function getLabelClass()
	{
		switch ($this->value) {
			case self::CLIENT:
				return $this->cssClassPrefix . 'info';
			case self::ADMIN:
				return $this->cssClassPrefix . 'primary';
			default:
				return $this->cssClassPrefix . 'default';
		}
	}
}