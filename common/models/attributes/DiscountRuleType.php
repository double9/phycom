<?php

namespace phycom\common\models\attributes;

use yii;
/**
 * Represents the type attribute on DiscountRule model
 *
 * Class DiscountRuleType
 * @package phycom\common\models\attributes
 */
class DiscountRuleType extends EnumAttribute
{
    const RULE = 'rule';                // discount rule that applies without the code
	const COUPON = 'coupon';            // offers one-time discount by percentage
	const VOUCHER = 'voucher';          // one-time discount card by a certain amount
	const CLIENT_CARD = 'client_card';  // personalized coupons for using multiple times until expired/deleted

    public function attributeLabels()
    {
        return [
            self::RULE        => Yii::t('common/discount', 'Rule'),
            self::COUPON      => Yii::t('common/discount', 'Coupon'),
            self::VOUCHER     => Yii::t('common/discount', 'Voucher'),
            self::CLIENT_CARD => Yii::t('common/discount', 'Client Card'),
        ];
    }
}