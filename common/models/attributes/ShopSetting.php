<?php

namespace phycom\common\models\attributes;

use phycom\common\models\Shop;
use phycom\common\modules\delivery\Module as DeliveryModule;
use yii;

/**
 * Class ShopSetting
 * @package phycom\common\models
 *
 * @property-read Shop $shop
 */
class ShopSetting extends JsonAttribute
{
    public $deliveryArea;
	public $defaultClosedMessage;
	public $closedMessage;
	public $deliveryMethods = [];
	public $courierStart;
	public $courierEnd;

	public function attributeLabels()
	{
        return [
            'closedMessage'        => Yii::t('common/shop', 'Closed message'),
            'defaultClosedMessage' => Yii::t('common/shop', 'Default Closed message'),
            'deliveryMethods'      => Yii::t('common/shop', 'Delivery methods'),
            'courierStart'         => Yii::t('common/shop', 'Courier start time'),
            'courierEnd'           => Yii::t('common/shop', 'Courier end time'),
        ];
	}

	public function rules()
	{
		return [
		    ['deliveryArea', 'integer'],
			[['closedMessage', 'defaultClosedMessage'], 'trim'],
			[['closedMessage', 'defaultClosedMessage', 'courierStart', 'courierEnd'], 'string'],
			['defaultClosedMessage', 'required'],
			['deliveryMethods', 'validateDeliveryMethods']
		];
	}

	public function assignDefaults()
	{
		/**
		 * @var DeliveryModule $deliveryModule
		 */
		$deliveryModule = Yii::$app->getModule('delivery');
		$this->defaultClosedMessage = Yii::t('common/shop', 'We are currently closed');
		$this->deliveryMethods = array_keys($deliveryModule->modules);
	}

	public function validateDeliveryMethods($attribute, $params, $validator)
	{
		if (!is_array($this->$attribute)) {
			$this->addError($attribute, Yii::t('common/main', '{attribute} must be an array', ['attribute' => $attribute]));
		}
		/**
		 * @var DeliveryModule $deliveryModule
		 */
		$deliveryModule = Yii::$app->getModule('delivery');
		$couriers = $deliveryModule->methods;

		foreach ($this->$attribute as $method) {
			$courierExists = false;
			foreach ($couriers as $courier) {
				if ($courier->getId() === $method) {
					$courierExists = true;
				}
			}
			if (!$courierExists) {
				$this->addError($attribute, Yii::t('common/main', 'Delivery method {method} was not found', ['method' => $method]));
			}
		}
	}

	/**
	 * @return Shop
	 */
	public function getShop()
	{
		return $this->owner;
	}
}