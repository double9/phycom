<?php

namespace phycom\common\models\attributes;

use yii;
/**
 * Represents the status attribute on ProductCategory model
 *
 * Class ProductCategoryStatus
 * @package phycom\common\models\attributes
 *
 * @property bool $isHidden
 * @property bool $isVisible
 */
class CategoryStatus extends EnumAttribute
{
	const VISIBLE = 'visible';
	const HIDDEN = 'hidden';
	const DELETED = 'deleted';

	public function attributeLabels()
    {
        return [
            self::VISIBLE => Yii::t('common/category', 'Visible'),
            self::HIDDEN  => Yii::t('common/category', 'Hidden'),
            self::DELETED => Yii::t('common/category', 'Deleted')
        ];
    }

    public function getLabelClass()
	{
		switch ($this->value) {
			case self::VISIBLE:
				return $this->cssClassPrefix . 'success';
			default:
				return parent::getLabelClass();
		}
	}

	public function getIsHidden()
	{
		return $this->is(self::HIDDEN);
	}

	public function getIsVisible()
	{
		return $this->is(self::VISIBLE);
	}
}