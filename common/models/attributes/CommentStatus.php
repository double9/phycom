<?php

namespace phycom\common\models\attributes;

use yii;
/**
 * Represents the status attribute on Comment model
 *
 * Class CommentStatus
 * @package phycom\common\models\attributes
 */
class CommentStatus extends EnumAttribute
{
	const PENDING = 'pending';
	const APPROVED = 'approved';
    const REJECTED = 'rejected';
	const SPAM = 'spam';
	const DELETED = 'deleted';

    public function isPending()
    {
        return $this->value === self::PENDING;
    }

    public function attributeLabels()
    {
        return [
            self::PENDING  => Yii::t('common/comment', 'Pending'),
            self::APPROVED => Yii::t('common/comment', 'Approved'),
            self::REJECTED => Yii::t('common/comment', 'Rejected'),
            self::SPAM     => Yii::t('common/comment', 'Spam')
        ];
    }
}