<?php

namespace phycom\common\models\attributes;

use yii;
/**
 * Represents the status attribute on Language model
 *
 * Class UserStatus
 * @package phycom\common\models\attributes
 */
class LanguageStatus extends EnumAttribute
{
	const VISIBLE = 'visible';
	const HIDDEN = 'hidden';
	const UNUSED = 'unused';

    public function attributeLabels()
    {
        return [
            self::VISIBLE => Yii::t('common/main', 'Visible'),
            self::HIDDEN  => Yii::t('common/main', 'Hidden'),
            self::UNUSED  => Yii::t('common/main', 'Unused')
        ];
    }
}