<?php

namespace phycom\common\models\attributes;

/**
 * Represents the type attribute on UserToken model
 *
 * Class UserTokenType
 * @package phycom\common\models\attributes
 */
class UserTokenType extends EnumAttribute
{
	const PASSWORD_RESET = 'password_reset';
	const ACCESS_TOKEN = 'access';
	const REGISTRATION_REQUEST = 'reg_request';
}