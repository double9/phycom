<?php

namespace phycom\common\models\attributes;

use yii;
/**
 * Represents the status attribute on Shop model
 *
 * Class ShopStatus
 * @package phycom\common\models\attributes
 */
class ShopStatus extends EnumAttribute
{
	const OPEN = 'open';                    // orders can be made all the time 24/7
	const OPEN_BY_SCHEDULE = 'scheduled';   // visible in public web orders can be made by the open/close schedule
	const CLOSED = 'closed';                // visible in public web but cannot make an order
	const INACTIVE = 'inactive';            // not visible in public web
	const DELETED = 'deleted';              // not visible in public and shop users cannot login to backend


    public function attributeLabels()
    {
        return [
            self::OPEN             => Yii::t('common/shop', 'Open'),
            self::OPEN_BY_SCHEDULE => Yii::t('common/shop', 'Scheduled'),
            self::CLOSED           => Yii::t('common/shop', 'Closed'),
            self::INACTIVE         => Yii::t('common/shop', 'Inactive'),
        ];
    }

    public function getLabelClass()
	{
		switch ($this->value) {
			case self::OPEN:
			case self::OPEN_BY_SCHEDULE:
				return $this->cssClassPrefix . 'success';
			case self::CLOSED:
			case self::DELETED:
				return $this->cssClassPrefix . 'danger';
			default:
				return $this->cssClassPrefix . 'default';
		}
	}
}