<?php

namespace phycom\common\models\attributes;

use yii;

/**
 * Represents the status attribute on all Translation models
 *
 * Class TranslationStatus
 * @package phycom\common\models\attributes
 */
class TranslationStatus extends EnumAttribute
{
	const DRAFT = 'draft';
	const PUBLISHED = 'published';
	const DELETED = 'deleted';

    public function attributeLabels()
    {
        return [
            self::DRAFT     => Yii::t('common/translation', 'Draft'),
            self::PUBLISHED => Yii::t('common/translation', 'Published'),
            self::DELETED   => Yii::t('common/translation', 'Deleted')
        ];
    }
}