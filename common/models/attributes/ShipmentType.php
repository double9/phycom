<?php

namespace phycom\common\models\attributes;


use yii;
/**
 * Represents the status attribute on Shipment model
 *
 * Class ShipmentType
 * @package phycom\common\models\attributes
 */
class ShipmentType extends EnumAttribute
{
	const DELIVERY = 'delivery';
	const SELF_PICKUP = 'pickup';

    public function attributeLabels()
    {
        return [
            self::DELIVERY    => Yii::t('common/shipment', 'Delivery'),
            self::SELF_PICKUP => Yii::t('common/shipment', 'Self pickup'),
        ];
    }
}