<?php

namespace phycom\common\models\attributes;

use yii;

/**
 * Represents the status attribute on Product model
 *
 * Class ProductStatus
 * @package phycom\common\models\attributes
 */
class ProductStatus extends EnumAttribute
{
	const DRAFT = 'draft';
	const ACTIVE = 'active';
	const INACTIVE = 'inactive';
	const OUT_OF_STOCK = 'out_of_stock';
	const HIDDEN = 'hidden';
	const DELETED = 'deleted';

	public function getLabelClass()
	{
		switch ($this->value) {
			case self::ACTIVE:
				return $this->cssClassPrefix . 'success';
			case self::OUT_OF_STOCK:
				return $this->cssClassPrefix . 'danger';
			default:
				return $this->cssClassPrefix . 'default';
		}
	}

	public function attributeLabels()
    {
        return [
            self::DRAFT        => Yii::t('common/product-status', 'Draft'),
            self::ACTIVE       => Yii::t('common/product-status', 'Active'),
            self::INACTIVE     => Yii::t('common/product-status', 'Inactive'),
            self::OUT_OF_STOCK => Yii::t('common/product-status', 'Out Of Stock'),
            self::HIDDEN       => Yii::t('common/product-status', 'Hidden'),
            self::DELETED      => Yii::t('common/product-status', 'Deleted')
        ];
    }
}