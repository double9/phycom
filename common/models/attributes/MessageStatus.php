<?php

namespace phycom\common\models\attributes;

/**
 * Represents the status attribute on Message model
 *
 * Class MessageStatus
 * @package phycom\common\models\attributes
 */
class MessageStatus extends EnumAttribute
{
	const NEW = 'new';
	const QUEUED = 'queued';
	const DISPATCHED = 'dispatched';
	const BOUNCED = 'bounced';
	const SENT = 'sent';
	const DELIVERED = 'delivered';
	const DELETED = 'deleted';
 }