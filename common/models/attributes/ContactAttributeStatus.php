<?php

namespace phycom\common\models\attributes;

use yii;
/**
 * Represents the status attribute on models representing a contact attribute like Email, Address, Phone
 *
 * Class ContactAttributeStatus
 * @package phycom\common\models\attributes
 */
class ContactAttributeStatus extends EnumAttribute
{
	const UNVERIFIED = 'unverified';
	const VERIFIED = 'verified';
	const DELETED = 'deleted';

    public function attributeLabels()
    {
        return [
            self::UNVERIFIED => Yii::t('common/main', 'Unverified'),
            self::VERIFIED   => Yii::t('common/main', 'Verified'),
        ];
    }
}