<?php

namespace phycom\common\models\attributes;

use yii;
/**
 * Represents the status attribute on DiscountCard model
 *
 * Class DiscountCardStatus
 * @package phycom\common\models\attributes
 */
class DiscountRuleStatus extends EnumAttribute
{
	const ACTIVE = 'active';
	const CLOSED = 'closed';
	const DELETED = 'deleted';

	public function attributeLabels()
    {
        return [
            self::ACTIVE => Yii::t('common/discount', 'Active'),
            self::CLOSED => Yii::t('common/discount', 'Closed'),
        ];
    }
}