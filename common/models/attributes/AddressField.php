<?php

namespace phycom\common\models\attributes;

use phycom\common\models\Country;

use Yii;

/**
 * Class AddressField
 * @package phycom\common\models
 */
class AddressField extends JsonAttribute
{
    ### Primary address fields
	public $country;
	public $state;
	public $province;
	public $locality;
	public $city;
	public $district;
	public $street;
	public $house;
	public $room;
	public $postcode;

	### Contact Meta fields
    public $name;
    public $firstName;
    public $lastName;
    public $company;
    public $email;
    public $phone;

    ### Other Meta fields
    public $lat;
    public $lng;
    public $mapUrl;
    public $info;       // custom description to hint the accurate place

    /**
     * @var bool - In most of times we want o export only those address fields that have a value not to write empty fields to database
     */
	protected bool $exportOnlyPopulated = true;


	public function attributeLabels()
	{
        return [
            'country'  => Yii::t('common/main', 'Country'),
            'state'    => Yii::t('common/main', 'State'),
            'province' => Yii::t('common/main', 'Province'),
            'locality' => Yii::t('common/main', 'Locality'),
            'city'     => Yii::t('common/main', 'City'),
            'district' => Yii::t('common/main', 'District'),
            'street'   => Yii::t('common/main', 'Street'),
            'house'    => Yii::t('common/main', 'House'),
            'room'     => Yii::t('common/main', 'Room'),
            'postcode' => Yii::t('common/main', 'Postcode'),

            'name'      => Yii::t('common/main', 'Name'),
            'firstName' => Yii::t('common/main', 'First Name'),
            'lastName'  => Yii::t('common/main', 'Last Name'),
            'company'   => Yii::t('common/main', 'Company'),
            'email'     => Yii::t('common/main', 'Email'),
            'phone'     => Yii::t('common/main', 'Phone'),

            'lat'    => Yii::t('common/main', 'Latitude'),
            'lng'    => Yii::t('common/main', 'Longitude'),
            'mapUrl' => Yii::t('common/main', 'Map url'),
            'info'   => Yii::t('common/main', 'Info')
        ];
	}

	public function rules()
	{
		return [array_keys($this->attributes), 'safe'];
	}

	public function assignDefaults()
	{
		$this->country = (string)Yii::$app->country;
	}


    /**
     * @param bool $fetchCountryName
     * @return array
     */
    public function exportArray(bool $fetchCountryName = false)
    {
        $address = [$this->street];

        if ($this->house) {
            $number = $this->house;
            if ($this->room) {
                $number .= '-' . $this->room;
            }
            $address[] = $number;
        }

        if ($this->district) {
            $address[] = $this->district;
        }
        if ($this->city) {
            $address[] = $this->city;
        }
        if ($this->locality) {
            $address[] = $this->locality;
        }
        if ($this->province) {
            $address[] = $this->province;
        }
        if ($this->state) {
            $address[] = $this->state;
        }
        if ($this->postcode) {
            $address[] = $this->postcode;
        }
        if ($this->country !== (string)Yii::$app->country && $fetchCountryName) {
            if ($country = Country::findOne(['code' => $this->country])) {
                $address[] = $country->name;
            }
        }
        return $address;
    }
}
