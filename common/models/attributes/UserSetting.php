<?php

namespace phycom\common\models\attributes;

use phycom\common\models\User;
use yii\helpers\ArrayHelper;
use yii;

/**
 * Class UserSetting
 * @package phycom\common\models\attributes
 *
 * @property-read array $clientAttributes
 * @property-read array $adminAttributes
 */
class UserSetting extends JsonAttribute
{
	public $country;
	public $language;
	public $currency;
	public $timezone;
	public $formatDate;
	public $formatDatetime;
	public $advertise;

	public $receiveOrderNotifications;
    public $receiveErrorNotifications;
	public $receiveShipmentNotifications;

    /**
     * @var User
     */
	protected $owner;
    /**
     * @var array
     */
    protected $adminAttributeNames = [
        'receiveOrderNotifications',
        'receiveErrorNotifications',
        'receiveShipmentNotifications'
    ];

    public function attributeLabels()
	{
        return [
            'country'                      => Yii::t('common/main', 'Country'),
            'language'                     => Yii::t('common/main', 'Language'),
            'currency'                     => Yii::t('common/main', 'Currency'),
            'timezone'                     => Yii::t('common/main', 'Timezone'),
            'formatDate'                   => Yii::t('common/main', 'Date format'),
            'formatDatetime'               => Yii::t('common/main', 'Datetime format'),
            'advertise'                    => Yii::t('common/main', 'Advertise'),
            'receiveOrderNotifications'    => Yii::t('common/main', 'Receive order notifications'),
            'receiveErrorNotifications'    => Yii::t('common/main', 'Receive error notifications'),
            'receiveShipmentNotifications' => Yii::t('common/main', 'Receive shipment notifications'),
        ];
	}

	public function getClientAttributes(array $except = [])
    {
        $except = ArrayHelper::merge($except, $this->adminAttributeNames);
        return $this->getAttributes(null, $except);
    }

    public function getAdminAttributes(array $except = [])
    {
        return $this->getAttributes($this->adminAttributeNames, $except);
    }

    public function getAttributes($names = null, $except = [])
    {
        if (!$this->owner || (string)$this->owner->type === UserType::CLIENT) {
            $except = ArrayHelper::merge($except, $this->adminAttributeNames);
        }
        return parent::getAttributes($names, $except);
    }


    public function assignDefaults()
	{
		$this->timezone = Yii::$app->timeZone;
		$this->language = Yii::$app->language;

		if ($this->owner && $this->owner->type->isAdmin) {
		    $this->receiveOrderNotifications = true;
            $this->receiveErrorNotifications = true;
		    $this->receiveShipmentNotifications = true;
        }
	}
}