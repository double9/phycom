<?php

namespace phycom\common\models\attributes;

/**
 * Represents the type attribute on ProductCategoryRelation model
 *
 * Class RelatedCategoryType
 * @package phycom\common\models\attributes
 */
class RelatedCategoryType extends EnumAttribute
{
	const SIMILAR = 'similar';
	const ACCESSORIES = 'accessories';
}