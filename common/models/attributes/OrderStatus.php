<?php

namespace phycom\common\models\attributes;

use yii;
/**
 * Represents the status attribute on Order model
 *
 * Class OrderStatus
 * @package phycom\common\models\attributes
 */
class OrderStatus extends EnumAttribute
{
	const NEW = 'new'; //customer started the checkout process, but did not complete it.
	const PENDING_PAYMENT = 'pending_payment'; // customer has completed checkout process, but payment has yet to be confirmed.
	const EXPIRED = 'expired'; // no payment received after n days

	const PAYMENT_COMPLETE = 'payment_complete'; // customer has completed the checkout process and payment has been confirmed

	const PENDING = 'pending'; // customer has completed the checkout process but immediate payment is not required (payment on delivery)

	const PROCESSING = 'processing'; // order is being processed
	const PROCESSING_COMPLETE = 'processing_complete'; // processing of the order has been completed

	const SHIPPED = 'shipped'; // order has been forwarded to courier;
	const COMPLETE = 'complete'; //  order has been shipped/picked up/delivered

	// special cases
	const PARTIALLY_SHIPPED = 'partially_shipped'; // only some items in the order have been shipped, due to some products being pre-order only or other reasons
	const CLOSED = 'closed'; // order has been refunded or partially refunded
	const ON_HOLD = 'on_hold';  // order is put on hold
	const CANCELED = 'canceled'; // order is canceled
	const DELETED = 'deleted'; // order is deleted


	public function getLabelClass()
	{
		if (in_array($this->value, [static::NEW, static::PROCESSING, static::PENDING_PAYMENT])) {
			return $this->cssClassPrefix . 'warning';
		}
		if (in_array($this->value, [static::PENDING])) {
		    return $this->cssClassPrefix . 'info';
        }
		if (in_array($this->value, [static::DELETED])) {
			return $this->cssClassPrefix . 'danger';
		}
        if (in_array($this->value, [static::PAYMENT_COMPLETE])) {
            return $this->cssClassPrefix . 'primary';
        }
		if (in_array($this->value, [static::SHIPPED, static::COMPLETE, static::CLOSED])) {
			return $this->cssClassPrefix . 'success';
		}
		return parent::getLabelClass();
	}

	public function attributeLabels()
    {
        return [
            static::NEW                 => Yii::t('common/order', 'New'),
            static::PENDING_PAYMENT     => Yii::t('common/order', 'Pending payment'),
            static::EXPIRED             => Yii::t('common/order', 'Expired'),
            static::PAYMENT_COMPLETE    => Yii::t('common/order', 'Payment complete'),
            static::PENDING             => Yii::t('common/order', 'Pending'),
            static::PROCESSING          => Yii::t('common/order', 'Processing'),
            static::PROCESSING_COMPLETE => Yii::t('common/order', 'Processing complete'),
            static::SHIPPED             => Yii::t('common/order', 'Shipped'),
            static::COMPLETE            => Yii::t('common/order', 'Complete'),
            static::PARTIALLY_SHIPPED   => Yii::t('common/order', 'Partially shipped'),
            static::CLOSED              => Yii::t('common/order', 'Closed'),
            static::ON_HOLD             => Yii::t('common/order', 'On hold'),
            static::CANCELED            => Yii::t('common/order', 'Cancelled')
        ];
    }
}