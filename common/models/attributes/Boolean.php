<?php

namespace phycom\common\models\attributes;

use yii\base\BaseObject;
use Yii;

/**
 * Class Boolean
 * @package phycom\common\models\attributes
 * @property-read bool value
 */
class Boolean extends BaseObject
{
    /**
     * Creates a new Boolean from string
     *
     * @param $string
     * @return static|object
     * @throws \yii\base\InvalidConfigException
     */
	public static function create($string)
	{
		switch ($string) {
			case '1':
				$value = true;
				break;
			case '0':
				$value = false;
				break;
			default:
				$value = null;
		}
		return Yii::createObject(static::class, [$value]);
	}

	/**
	 * @var bool
	 */
	private $value;

	public function __construct($value, array $config = [])
	{
		$this->value = $value;
		parent::__construct($config);
	}

	/**
	 * @return bool
	 */
	public function getValue()
	{
		return $this->value;
	}

	public function __toString()
	{
		if ($this->value) {
			return '1';
		}
		if ($this->value === false || $this->value === 0) {
			return '0';
		}
		return '';
	}
}
