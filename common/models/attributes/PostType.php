<?php

namespace phycom\common\models\attributes;

use yii;

/**
 * Represents the type attribute on Post model
 *
 * Class PostType
 * @package phycom\common\models\attributes
 */
class PostType extends EnumAttribute
{
	const POST = 'post';    // blog post
	const PAGE = 'page';    // regular page
	const LAND = 'land';    // landing page
    const SHOP = 'shop';    // shop information

    public function attributeLabels()
    {
        return [
            self::POST => Yii::t('common/post', 'Post'),
            self::PAGE => Yii::t('common/post', 'Page'),
            self::LAND => Yii::t('common/main', 'Landing page'),
        ];
    }
}