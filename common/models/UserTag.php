<?php

namespace phycom\common\models;

use phycom\common\models\traits\ClassConstantTrait;
use yii;

/**
 * Class UserTag
 * @package phycom\common\models\product
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $value
 * @property \DateTime $created_at
 *
 * @property User $user
 */
class UserTag extends \phycom\common\models\ActiveRecord
{
	use ClassConstantTrait;

	const TAG_MIGRATED = 'migrated';
	const TAG_PENDING_PASSWORD_CHANGE = 'pending_pwd_change';

	const TAG_REGISTERED_WITH_FACEBOOK_APP = 'reg_facebook_app';
	const TAG_REGISTERED_WITH_FACEBOOK = 'reg_facebook';
	const TAG_REGISTERED_WITH_GOOGLE = 'reg_google';
	const TAG_REGISTERED_WITH_PAYTAILOR = 'reg_paytailor';

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'user_tag';
	}

	public static function all()
	{
		return static::getConstants('TAG_');
	}

	public function rules()
	{
		return [
			[['user_id'], 'integer'],
			[['value'], 'string', 'max' => 255],
			[['created_at'], 'safe'],
			[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['user_id' => 'id']]
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser()
	{
		return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'user_id']);
	}

}