<?php

namespace phycom\common\models;

use phycom\common\helpers\PhoneHelper;
use phycom\common\models\attributes\ContactAttributeStatus;
use phycom\common\validators\PhoneInputValidator;
use libphonenumber\PhoneNumberUtil;

use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "phone".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $vendor_id
 * @property integer $shop_id
 * @property string $country_code
 * @property string $phone_nr
 * @property ContactAttributeStatus $status
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property Vendor $vendor
 * @property Shop $shop
 * @property User $user
 *
 * @property-read string $msisdn
 * @property-read string $fullNumber
 */
class Phone extends ActiveRecord
{

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'phone';
	}

	public static function create($fullNumber)
	{
		return (new static())->setPhoneNumber($fullNumber);
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'dynamic-attribute' => [
				'attributes' => [
					'status' => ContactAttributeStatus::class,
				]
			]
		]);
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'vendor_id', 'shop_id'], 'integer'],
            [['country_code', 'phone_nr', 'status'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['country_code'], 'string', 'max' => 10],
            [['phone_nr'], 'string', 'max' => 255],
	        ['status', 'in', 'range' => ContactAttributeStatus::all()],
            [['vendor_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getVendor()), 'targetAttribute' => ['vendor_id' => 'id']],
            [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getShop()), 'targetAttribute' => ['shop_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/main', 'ID'),
            'user_id' => Yii::t('common/main', 'User ID'),
            'vendor_id' => Yii::t('common/main', 'Vendor ID'),
            'shop_id' => Yii::t('common/main', 'Shop ID'),
            'country_code' => Yii::t('common/main', 'Country Code'),
            'phone_nr' => Yii::t('common/main', 'Phone Nr'),
            'status' => Yii::t('common/main', 'Status'),
            'created_at' => Yii::t('common/main', 'Created At'),
            'updated_at' => Yii::t('common/main', 'Updated At'),
        ];
    }

	/**
	 * http://en.wikipedia.org/wiki/MSISDN
	 * @return string
	 */
	public function getMsisdn()
	{
		return $this->country_code . $this->phone_nr;
	}

    public function getFullNumber()
    {
        return '+' . $this->country_code . $this->phone_nr;
    }

    public function __toString()
    {
	    return $this->phone_nr;
    }

    /**
     * @param string $fullNumber - full international phone number (can be prefixed with + sigh or just plain msisdn)
     * @return static
     */
    public function setPhoneNumber($fullNumber)
    {
        $this->phone_nr = PhoneHelper::getNationalPhoneNumber($fullNumber);
        $this->country_code = PhoneHelper::getPhoneCode($fullNumber);
        return $this;
    }

	/**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getVendor()), ['id' => 'vendor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShop()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getShop()), ['id' => 'shop_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'user_id']);
    }
}
