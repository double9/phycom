<?php

namespace phycom\common\models;

use phycom\common\helpers\Date;
use phycom\common\helpers\f;
use phycom\common\models\attributes\ContactAttributeStatus;
use phycom\common\models\attributes\PostStatus;
use phycom\common\models\attributes\PostType;
use phycom\common\models\attributes\ReviewStatus;
use phycom\common\models\attributes\ShopSetting;
use phycom\common\models\behaviors\JsonAttributeBehavior;
use phycom\common\models\product\ProductCategory;
use phycom\common\models\attributes\ShopStatus;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii;

/**
 * This is the model class for table "shop".
 *
 * @property integer $id
 * @property string $name
 * @property integer $vendor_id
 * @property ShopStatus $status
 * @property ShopSetting $settings
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property Address $address
 * @property Address[] $addresses
 * @property Comment[] $comments
 * @property DiscountRule[] $discountCards
 * @property Email $email
 * @property Email[] $emails
 * @property Order[] $orders
 * @property Phone $phone
 * @property Phone[] $phones
 * @property Post[] $posts
 * @property ProductCategory[] $productCategories
 * @property Vendor $vendor
 * @property Post $content
 * @property ShopClosed[] $shopClosed
 * @property ShopOpen[] $shopOpen
 * @property ShopSupply[] $shopSupply
 * @property Review[] $reviews
 * @property Review[] $approvedReviews
 *
 * @property-read array $openTimeGroups
 * @property-read string $openTime
 * @property-read string $openTimeTable
 */
class Shop extends ActiveRecord
{
	protected $openTimeGroups;

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'dynamic-attribute' => [
				'attributes' => [
					'status' => ShopStatus::class
				]
			],
			'json-attribute' => [
				'class' => JsonAttributeBehavior::class,
				'attributes' => ['settings' => ShopSetting::class]
			]
		]);
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'vendor_id', 'status'], 'required'],
            [['vendor_id'], 'integer'],
            [['created_at', 'updated_at', 'status', 'settings'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['vendor_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getVendor()), 'targetAttribute' => ['vendor_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => Yii::t('common/main', 'ID'),
            'name'          => Yii::t('common/main', 'Name'),
            'vendor_id'     => Yii::t('common/main', 'Vendor ID'),
            'status'        => Yii::t('common/main', 'Status'),
            'settings'      => Yii::t('common/main', 'Settings'),
            'openTime'      => Yii::t('common/main', 'Opening hours'),
            'openTimeTable' => Yii::t('common/main', 'Opening hours'),
            'created_at'    => Yii::t('common/main', 'Created At'),
            'updated_at'    => Yii::t('common/main', 'Updated At'),
        ];
    }

    /**
     * Check if shop is open at the given time
     *
     * @param \DateTime|null $time
     * @return bool|ShopOpen
     * @throws \Exception
     */
    public function isOpen(\DateTime $time = null)
    {
        switch ((string) $this->status) {
            case ShopStatus::OPEN_BY_SCHEDULE:
                if (!$time) {
                    $time = new \DateTime('now');
                }
                $closedDays = $this->getShopClosed()->andWhere(['date' => $time->format('Y-m-d')]);
                foreach ($closedDays->all() as $closed) {
                    /**
                     * @var ShopClosed $closed
                     */
                    if ($closed->from <= $time && $closed->to > $time) {
                        return false;
                    }
                }
                /**
                 * @var ShopOpen $shopOpen
                 */
                if ($shopOpen = $this->getShopOpen()->andWhere(['day_of_week' => $time->format('N')])->one()) {

                    $openFrom = clone $time;
                    $openFrom->setTime($shopOpen->opened_at->format('H'), $shopOpen->opened_at->format('i'));

                    $openTo = clone $time;
                    // if the closing time is on next day
                    if ($daysPassed = Date::getDaysPassed($shopOpen->opened_at, $shopOpen->closed_at)) {
                        $openTo->add(new \DateInterval('P' . $daysPassed . 'D'));
                    }
                    $openTo->setTime($shopOpen->closed_at->format('H'), $shopOpen->closed_at->format('i'));

                    return ($openFrom <= $time && $openTo >= $time) ? $shopOpen : false;
                } else {
                    return false;
                }
            case ShopStatus::OPEN:
                return true;
            default:
                return false;
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isClosedToday()
    {
        switch ((string) $this->status) {
            case ShopStatus::OPEN_BY_SCHEDULE:
            case ShopStatus::OPEN:
                $time = new \DateTime('now');
                $closedDays = $this->getShopClosed()->andWhere(['date' => $time->format('Y-m-d')]);
                foreach ($closedDays->all() as $closed) {
                    /**
                     * @var ShopClosed $closed
                     */
                    if ($closed->from <= $time && $closed->to > $time) {
                        return true;
                    }
                }
                return false;
            default:
                return true;
        }
    }

	/**
     * @param bool|string $closedLabel - print this vale when particular day is closed
	 * @return string
	 */
    public function getOpenTime($closedLabel = false)
    {
        if (true === $closedLabel) {
            $closedLabel = Yii::t('common/main', 'Closed');
        }
    	$times = [];
		foreach ($this->getOpenTimeGroups((bool) $closedLabel) as $t) {
			$timeStr = $t[1] ? f::weekday($t[0]) . ' - ' . f::weekday($t[1]) : f::weekday($t[0]);
			$timeStr .= ' ' . $t[2]->format('H:i') . ' - ' . $t[3]->format('H:i');
			$times[] = Html::tag('span', $timeStr);
		}
		return implode(' ', $times);
    }

	/**
     * @param bool|string $closedLabel - print this vale when particular day is closed
	 * @return string - an html table with formatted opening times
	 */
	public function getOpenTimeTable($closedLabel = false)
	{
	    if (true === $closedLabel) {
	        $closedLabel = Yii::t('common/main', 'Closed');
        }
		$html = Html::beginTag('table', ['class' => 'shop-opening-hours']);
		foreach ($this->getOpenTimeGroups((bool) $closedLabel) as $t) {
			$html .= Html::beginTag('tr');
			$html .= Html::tag('td', $t[1]
                ? f::weekday($t[0]) . ' - ' . f::weekday($t[1])
                : f::weekday($t[0])
            );
			$html .= Html::tag('td', ($t[2] && $t[3])
                ? $t[2]->format('H:i') . ' - ' . $t[3]->format('H:i')
                : $closedLabel
            );
			$html .= Html::endTag('tr');
		}
		$html .= Html::endTag('table');
		return $html;
	}

	/**
     * @param bool $includeClosed
	 * @return array
	 */
    public function getOpenTimeGroups($includeClosed = false)
    {
	   // if (!$this->openTimeGroups) {

		    $groups = [];

		    $from = null;
		    $time = [];
		    $to = null;

		    $prevDayOfWeek = null;
		    foreach ($this->shopOpen as $open) {

		        if ($includeClosed) {
                    if (null === $prevDayOfWeek && 2 === $open->day_of_week) {
                        $groups[] = [1, null, null, null];
                    }
                    if (null !== $prevDayOfWeek && ($open->day_of_week - $prevDayOfWeek) > 1) {
                        $groups[] = [$open->day_of_week - 1, null, null, null];
                    }
                }

			    if ($from) {
				    if ($open->opened_at->format('H:i') === $time[0]->format('H:i') && $open->closed_at->format('H:i') === $time[1]->format('H:i')) {
					    $to = $open->day_of_week;
				    } else {
					    $groups[] = [$from, $to, $time[0], $time[1]];
					    $from = $open->day_of_week;
					    $to = null;
				    }
			    } else {
				    $from = $open->day_of_week;
			    }
			    $time = [$open->opened_at, $open->closed_at];
                $prevDayOfWeek = $open->day_of_week;
		    }
		    if (!empty($this->shopOpen)) {
			    $groups[] = [$from, $to, $time[0], $time[1]];
		    }
            if ($includeClosed && 6 === $prevDayOfWeek) {
                $groups[] = [7, null, null, null];
            }
		    $this->openTimeGroups = $groups;
	   // }
	    return $this->openTimeGroups;
    }


	public function getAddress()
	{
		return !empty($this->addresses) ? $this->addresses[0] : null;
	}

	public function getPhone()
	{
		return !empty($this->phones) ? $this->phones[0] : null;
	}

	public function getEmail()
	{
		return !empty($this->emails) ? $this->emails[0] : null;
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::class, ['shop_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscountCards()
    {
        return $this->hasMany(DiscountRule::class, ['shop_id' => 'id']);
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEmails()
	{
		return $this->hasMany(Email::class, ['shop_id' => 'id'])->orderBy(['created_at' => SORT_DESC])->andWhere(['not', ['email.status' => ContactAttributeStatus::DELETED]]);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPhones()
	{
		return $this->hasMany(Phone::class, ['shop_id' => 'id'])->orderBy(['created_at' => SORT_DESC])->andWhere(['not', ['phone.status' => ContactAttributeStatus::DELETED]]);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAddresses()
	{
		return $this->hasMany(Address::class, ['shop_id' => 'id'])->orderBy(['created_at' => SORT_DESC])->andWhere(['not', ['address.status' => ContactAttributeStatus::DELETED]]);
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::class, ['shop_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContent()
    {
        return $this->hasOne(Post::class, ['shop_id' => 'id'])
            ->andWhere(['type' => PostType::SHOP])
            ->andWhere(['not', ['post.status' => PostStatus::DELETED]])
            ->orderBy(['created_at' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::class, ['shop_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategories()
    {
        return $this->hasMany(ProductCategory::class, ['shop_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getVendor()), ['id' => 'vendor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopClosed()
    {
        return $this->hasMany(ShopClosed::class, ['shop_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopOpen()
    {
        return $this->hasMany(ShopOpen::class, ['shop_id' => 'id'])->orderBy(['day_of_week' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopSupply()
    {
        return $this->hasMany(ShopSupply::class, ['shop_id' => 'id'])->orderBy(['day_of_week' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws yii\base\InvalidConfigException
     */
    public function getReviews()
    {
        return $this->hasMany(Review::class, ['id' => 'review_id'])->viaTable(ShopReview::tableName(), ['shop_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws yii\base\InvalidConfigException
     */
    public function getApprovedReviews()
    {
        return $this->getReviews()->where(['review.status' => ReviewStatus::APPROVED]);
    }
}
