<?php

namespace phycom\common\models;


use yii\helpers\Inflector;
use yii;

/**
 * This is the ActiveRecord class for the database table "post_tag".
 *
 * Class PostTag
 * @package phycom\common\models
 *
 * @property integer $id
 * @property integer $post_id
 * @property string $value
 * @property integer $created_by
 * @property \DateTime $created_at
 *
 * @property User $createdBy
 * @property Post $post
 * @property-read string $label
 * @property-read bool $isVisible
 */
class PostTag extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'post_tag';
	}

	public function rules()
	{
		return [
			[['post_id', 'created_by'], 'integer'],
			[['value'], 'string', 'max' => 255],
			[['created_at'], 'safe'],
			[['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => Post::class, 'targetAttribute' => ['post_id' => 'id']],
			[['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['created_by' => 'id']],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => Yii::t('common/post', 'ID'),
			'post_id' => Yii::t('common/post', 'Post ID'),
			'value' => Yii::t('common/post', 'Value'),
			'label' => Yii::t('common/post', 'Label'),
			'isVisible' => Yii::t('common/post', 'Is Visible'),
			'created_by' => Yii::t('common/post', 'Created By'),
			'created_at' => Yii::t('common/post', 'Created At'),
		];
	}

	public function getLabel()
	{
		return Inflector::titleize($this->value);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCreatedBy()
	{
		return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'created_by']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPost()
	{
		return $this->hasOne(Post::class, ['id' => 'post_id']);
	}
}