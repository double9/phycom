<?php

namespace phycom\common\models;

use phycom\common\models\attributes\ContactAttributeStatus;
use phycom\common\models\attributes\SubscriptionStatus;
use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "newsletter_subscription".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property integer $emails_sent
 * @property SubscriptionStatus $status
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property User $user
 */
class Subscription extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'dynamic-attribute' => [
				'attributes' => [
					'status' => SubscriptionStatus::class,
				]
			]
		]);
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'newsletter_subscription';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'status'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
	        [['emails_sent'], 'integer'],
            [['email', 'first_name', 'last_name'], 'string', 'max' => 255],
	        ['status', 'in', 'range' => SubscriptionStatus::all()],
	        ['email', 'unique', 'message' => Yii::t('common/main', '{email} is already subscribed', ['email' => $this->email])]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/main', 'ID'),
	        'first_name' => Yii::t('common/main', 'First name'),
	        'last_name' => Yii::t('common/main', 'Last name'),
            'email' => Yii::t('common/main', 'Email'),
	        'emails_sent' => Yii::t('common/main', 'Emails sent'),
            'status' => Yii::t('common/main', 'Status'),
            'created_at' => Yii::t('common/main', 'Created At'),
            'updated_at' => Yii::t('common/main', 'Updated At'),
        ];
    }

    public function __toString()
    {
	    return $this->email;
    }

//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getUser()
//    {
//        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'user_id']);
//    }
}
