<?php

namespace phycom\common\models;

use yii;

/**
 * This is the model class for table "shop_review".
 *
 * @property integer $shop_id
 * @property integer $review_id
 *
 * @property Shop $shop
 * @property Review $review
 */
class ShopReview extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_review';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shop_id', 'review_id'], 'required'],
            [['shop_id', 'review_id'], 'integer'],
            [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getShop()), 'targetAttribute' => ['shop_id' => 'id']],
            [['review_id'], 'exist', 'skipOnError' => true, 'targetClass' => Review::class, 'targetAttribute' => ['review_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'shop_id' => Yii::t('common/main', 'Shop ID'),
            'review_id' => Yii::t('common/main', 'Review ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShop()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getShop()), ['id' => 'shop_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReview()
    {
        return $this->hasOne(Review::class, ['id' => 'review_id']);
    }
}
