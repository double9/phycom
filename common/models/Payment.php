<?php

namespace phycom\common\models;

use phycom\common\events\StatusUpdateEvent;
use phycom\common\models\attributes\InvoiceStatus;
use phycom\common\modules\payment\interfaces\PaymentMethodInterface;
use phycom\common\modules\payment\Module as PaymentModule;
use phycom\common\models\attributes\PaymentStatus;
use phycom\common\models\behaviors\CurrencyBehavior;
use phycom\common\models\behaviors\JsonAttributeBehavior;
use phycom\common\models\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "payment".
 *
 * @property integer $id
 * @property integer $invoice_id
 * @property string $payment_method
 * @property string $reference_number
 * @property string $remitter
 * @property int $amount
 * @property PaymentStatus $status
 * @property string $explanation
 * @property string $transaction_id
 * @property \DateTime $transaction_time
 * @property array $transaction_data
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property AccountBalance[] $accountBalanceTrail
 * @property Invoice $invoice
 * @property-read PaymentMethodInterface $paymentMethod
 */
class Payment extends ActiveRecord
{

	const EVENT_AFTER_STATUS_UPDATE = 'afterStatusUpdate';

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'payment';
	}

	/**
	 * @param $transactionId
	 * @return null|static|yii\db\ActiveRecordInterface
	 */
	public static function findByTransactionId($transactionId)
	{
		return static::find()
			->where(['transaction_id' => $transactionId])
			->andWhere(['not', ['status' => PaymentStatus::DELETED]])
			->one();
	}

	public function init()
	{
		parent::init();
		$this->on(self::EVENT_AFTER_STATUS_UPDATE, function ($event) {
			/**
			 * @var StatusUpdateEvent $event
			 */
			switch ((string) $this->status) {
				case PaymentStatus::COMPLETED:
					$this->invoice->checkPaid();
					break;
			}
		});
	}

	public function afterSave($insert, $changedAttributes)
	{
		parent::afterSave($insert, $changedAttributes);
		if (in_array('status', array_keys($changedAttributes))) {
			$this->trigger(self::EVENT_AFTER_STATUS_UPDATE, new StatusUpdateEvent([
				'prevStatus' => $changedAttributes['status']
			]));
		}
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'timestamp' => [
				'class' => TimestampBehavior::class,
				'attributes' => [
					['created_at', ActiveRecord::EVENT_BEFORE_INSERT],
					['updated_at', ActiveRecord::EVENT_BEFORE_INSERT, ActiveRecord::EVENT_BEFORE_UPDATE],
					'transaction_time'
				]
			],
			'dynamic-attribute' => [
				'attributes' => [
					'status' => PaymentStatus::class,
				]
			],
			'currency' => [
				'class' => CurrencyBehavior::class,
				'attributes' => ['amount']
			],
		]);
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_id', 'payment_method', 'amount', 'status'], 'required'],
            [['invoice_id'], 'integer'],
            [['amount'], 'number'],
            [['explanation','remitter'], 'string'],
            [['transaction_data', 'status','transaction_time', 'created_at', 'updated_at'], 'safe'],
            [['transaction_id', 'payment_method', 'reference_number'], 'string', 'max' => 255],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getInvoice()), 'targetAttribute' => ['invoice_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => Yii::t('common/main', 'ID'),
            'invoice_id'       => Yii::t('common/main', 'Invoice ID'),
            'invoice'          => Yii::t('common/main', 'Invoice'),
            'payment_method'   => Yii::t('common/main', 'Payment Method'),
            'reference_number' => Yii::t('common/main', 'Reference Number'),
            'remitter'         => Yii::t('common/main', 'Remitter'),
            'amount'           => Yii::t('common/main', 'Amount'),
            'status'           => Yii::t('common/main', 'Status'),
            'explanation'      => Yii::t('common/main', 'Explanation'),
            'transaction_id'   => Yii::t('common/main', 'Transaction ID'),
            'transaction_time' => Yii::t('common/main', 'Transaction Time'),
            'transaction_data' => Yii::t('common/main', 'Transaction Data'),
            'created_at'       => Yii::t('common/main', 'Created At'),
            'updated_at'       => Yii::t('common/main', 'Updated At'),
        ];
    }

	/**
	 * @return null|PaymentMethodInterface
	 */
	public function getPaymentMethod()
	{
		/**
		 * @var PaymentModule $paymentModule
		 */
		$paymentModule = Yii::$app->getModule('payment');
		return $paymentModule->getModule($this->payment_method);
	}

	public function createTransaction($paymentSubMethod = null)
	{
		return $this->paymentMethod->createTransaction($this, $paymentSubMethod);
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccountBalanceTrail()
    {
        return $this->hasMany(AccountBalance::class, ['payment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getInvoice()), ['id' => 'invoice_id']);
    }
}
