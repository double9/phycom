<?php

namespace phycom\common\models;

use yii;

/**
 * This is the model class for table "account_balance".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $payment_id
 * @property int $amount
 * @property \DateTime $created_at
 *
 * @property Payment $payment
 * @property User $user
 */
class AccountBalance extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'account_balance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'amount'], 'required'],
            [['user_id', 'payment_id'], 'integer'],
            [['amount'], 'number'],
            [['created_at'], 'safe'],
            [['payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getPayment()), 'targetAttribute' => ['payment_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/main', 'ID'),
            'user_id' => Yii::t('common/main', 'User ID'),
            'payment_id' => Yii::t('common/main', 'Payment ID'),
            'amount' => Yii::t('common/main', 'Amount'),
            'created_at' => Yii::t('common/main', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getPayment()), ['id' => 'payment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'user_id']);
    }
}
