<?php

namespace phycom\common\models;


use \yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[PartnerContract]].
 *
 * @see PartnerContract
 */
class PartnerContractQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function expired()
    {
        return $this->andWhere(['<', 'contract_end', "NOW()"]);
    }

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere(['>', 'contract_end', "NOW()"])->orWhere(['contract_end' => null]);
    }

    /**
     * @inheritdoc
     * @return PartnerContract[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PartnerContract|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
