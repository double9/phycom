<?php

namespace phycom\common\models;

use phycom\common\models\attributes\ContactAttributeStatus;

use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "email".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $vendor_id
 * @property integer $shop_id
 * @property string $email
 * @property ContactAttributeStatus $status
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property Vendor $vendor
 * @property Shop $shop
 * @property User $user
 */
class Email extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'dynamic-attribute' => [
				'attributes' => [
					'status' => ContactAttributeStatus::class,
				]
			]
		]);
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'vendor_id', 'shop_id'], 'integer'],
            [['email', 'status'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['email'], 'string', 'max' => 255],
	        ['status', 'in', 'range' => ContactAttributeStatus::all()],
            [['vendor_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getVendor()), 'targetAttribute' => ['vendor_id' => 'id']],
            [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getShop()), 'targetAttribute' => ['shop_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['user_id' => 'id']],
	        ['email', 'validateEmail']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/main', 'ID'),
            'user_id' => Yii::t('common/main', 'User ID'),
            'vendor_id' => Yii::t('common/main', 'Vendor ID'),
            'shop_id' => Yii::t('common/main', 'Shop ID'),
            'email' => Yii::t('common/main', 'Email'),
            'status' => Yii::t('common/main', 'Status'),
            'created_at' => Yii::t('common/main', 'Created At'),
            'updated_at' => Yii::t('common/main', 'Updated At'),
        ];
    }

    public function validateEmail($attribute, $params, $validator)
    {
		$emailValidator = new yii\validators\EmailValidator();
		$emailValidator->validateAttribute($this, $attribute);

        /**
         * For now allow all shops can use same email address.
         * TODO: add filter by vendor_id so that different vendors cannot use same shop email
         */
		foreach (['user_id', 'vendor_id'] as $column) {
			$uniqueValidator = new yii\validators\UniqueValidator();
			$uniqueValidator->filter = function ($query) use ($column) {
				/**
				 * @var yii\db\Query $query
				 */
				return $query->andWhere($column . ' IS NOT NULL')
					->andWhere(['not', [
						'and',
						[$column => $this->$column],
						['status' => ContactAttributeStatus::DELETED]
					]]);
			};
			if ($this->$column) {
				$uniqueValidator->validateAttribute($this, $attribute);
			}
		}
    }


    public function setVerified()
    {
		$this->status = ContactAttributeStatus::VERIFIED;
		return $this->save(false, ['status']);
    }

    public function __toString()
    {
	    return $this->email;
    }

	/**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getVendor()), ['id' => 'vendor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShop()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getShop()), ['id' => 'shop_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'user_id']);
    }
}
