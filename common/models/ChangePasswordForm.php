<?php

namespace phycom\common\models;

use yii\base\Model;
use yii;

/**
 * Class ChangePasswordForm
 * @package phycom\common\models
 */
class ChangePasswordForm extends Model
{
	public $password;
	public $repeatPassword;
	/**
	 * @var User
	 */
	protected $user;

	/**
	 * ChangePasswordForm constructor.
	 * @param User $user
	 * @param array $config
	 */
	public function __construct(User $user, array $config = [])
	{
		$this->user = $user;
		parent::__construct($config);
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['password', 'required', 'message' => Yii::t('common/error', 'This field can not be empty.')],
			['password', 'string', 'min' => 6, 'max' => 64],
			['repeatPassword', 'compare', 'compareAttribute' => 'password', 'message' => Yii::t('common/error', 'Password and re-entered password do not match.')],
		];
	}


	public function attributeLabels()
	{
		return [
			'password' => Yii::t('common/user', 'Password'),
			'repeatPassword' => Yii::t('common/user', 'Repeat Password'),
		];
	}

	/**
	 * Resets the password.
	 * @return bool
	 */
	public function resetPassword()
	{
		if ($this->validate()) {
			$this->user->setPassword($this->password);
			if ($this->user->save(false, ['password_hash'])) {
				$this->user->removePasswordResetToken();
				return true;
			}
		}
		return false;
	}
}