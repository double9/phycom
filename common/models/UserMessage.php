<?php

namespace phycom\common\models;


use yii\base\Model;

/**
 * Class UserMessage
 * @package phycom\common\models
 */
class UserMessage extends Model
{
    protected $user;

    public function __construct(User $user, array $config = [])
    {
        $this->user = $user;
        parent::__construct($config);
    }


}