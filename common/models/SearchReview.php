<?php

namespace phycom\common\models;


use phycom\common\models\attributes\ReviewStatus;

use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\base\Model;
use yii;

/**
 * Class SearchReview
 * @package phycom\common\models
 */
abstract class SearchReview extends Model
{
    /**
     * @var string
     */
    public $status = ReviewStatus::APPROVED;
    /**
     * @var bool
     */
    public $verifiedPurchase;
    /**
     * @var int
     */
    public $score;


    /**
     * @var string
     */
    protected $joinTableName;
    /**
     * @var string
     */
    protected $joinKey;
    /**
     * @var int
     */
    protected $joinValue;

    /**
     * SearchReview constructor.
     * @param int $value
     * @param array $config
     */
    public function __construct(int $value, array $config = [])
    {
        $this->joinValue = $value;
        parent::__construct($config);
    }


    public function rules()
    {
        return [
            [['status'], 'string'],
            [['verifiedPurchase'], 'boolean'],
            [['score'], 'integer']
        ];
    }

    /**
     * @return array
     */
    public function getGroups()
    {
        $query = (new Query())
            ->select('r.score, count(r.id) as count')
            ->from(['r' => Review::tableName()]);

        $query->innerJoin(['rr' => $this->joinTableName], ['and', 'rr.review_id = r.id', ['rr."' . $this->joinKey . '"' => $this->joinValue]]);
        $query->where(['r.status' => $this->status]);
        $query->groupBy(['r.score']);
        $query->orderBy(['r.score' => SORT_DESC]);

        return $query->all();
    }

    public function getTotalScore()
    {
        $query = (new Query())
            ->select('avg(r.score)')
            ->from(['r' => Review::tableName()]);

        $query->innerJoin(['rr' => $this->joinTableName], ['and', 'rr.review_id = r.id', ['rr."' . $this->joinKey . '"' => $this->joinValue]]);
        $query->where(['r.status' => $this->status]);
        return (float) $query->scalar();
    }

    /**
     * @return int|string
     */
    public function getTotalCount()
    {
        return (new Query())
            ->from(['r' => Review::tableName()])
            ->innerJoin(['rr' => $this->joinTableName], ['and', 'rr.review_id = r.id', ['rr."' . $this->joinKey . '"' => $this->joinValue]])
            ->where(['r.status' => $this->status])
            ->count('r.*');
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function getRecent(array $params = [])
    {
        $query = Review::find()
            ->select('r.*')
            ->alias('r')
            ->innerJoin(['rr' => $this->joinTableName], ['and', 'rr.review_id = r.id', ['rr."' . $this->joinKey . '"' => $this->joinValue]]);


        if (Yii::$app->user->isGuest) {
            $query->where(['r.status' => $this->status]);
        } else {
            $query->where([
               'or',
                ['r.status' => $this->status],
                ['r.created_by' => Yii::$app->user->id, 'r.status' => ReviewStatus::PENDING]
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['status' => SORT_DESC, 'created_at' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 20
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'r.verified_purchase' => $this->verifiedPurchase,
            'r.score' => $this->score
        ]);

        return $dataProvider;
    }
}