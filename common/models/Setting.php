<?php

namespace phycom\common\models;

use yii;

/**
 * This is the model class for table "setting".
 *
 * @property integer $id
 * @property string $key
 * @property string $value
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 */
class Setting extends ActiveRecord
{
	/**
	 * @param mixed $key
	 * @param null $defaultValue
	 * @return null|string
	 */
	public static function get($key, $defaultValue = null)
	{
		/**
		 * @var Setting $model
		 */
		$model = static::findByKey($key);
		return $model ? $model->value : $defaultValue;
	}

	/**
	 * @param mixed $key
	 * @param string $value
	 * @return static
	 */
	public static function set($key, $value)
	{
		$model = static::findByKey($key);
		if (!$model) {
			$model = new static;
			$model->key = $key;
		}
		$model->value = $value;
		$model->save();
		return $model;
	}

	/**
	 * @param $key
	 * @return static
	 */
	public static function findByKey($key)
	{
		return static::find()->where(['key' => $key])->one();
	}

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['key', 'value'], 'string', 'max' => 255],
            [['key'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('common/main', 'ID'),
            'key'        => Yii::t('common/main', 'Key'),
            'value'      => Yii::t('common/main', 'Value'),
            'created_at' => Yii::t('common/main', 'Created At'),
            'updated_at' => Yii::t('common/main', 'Updated At'),
        ];
    }
}
