<?php

namespace phycom\common\models;

use phycom\common\helpers\Gravatar;
use phycom\common\models\attributes\CommentStatus;
use phycom\common\models\product\Product;


use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property string $content
 * @property CommentStatus $status
 * @property string $author_name
 * @property string $author_email
 * @property string $author_ip
 * @property string $author_agent
 * @property integer $created_by
 * @property integer $approved_by
 * @property integer $parent_id
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property Comment $parent
 * @property Comment[] $replied
 * @property Post $post
 * @property Product $product
 * @property User $createdBy
 * @property User $approvedBy
 *
 * @property-read string $avatar
 */
class Comment extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'dynamic-attribute' => [
				'attributes' => [
					'status' => CommentStatus::class,
				]
			]
		]);
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['approved_by', 'created_by', 'parent_id'], 'integer'],
            [['content', 'status', 'author_name', 'author_email', 'author_ip', 'author_agent'], 'required'],
            [['content', 'author_agent'], 'string'],
            [['author_name', 'author_email', 'author_ip'], 'string', 'max' => 255],
            [['status', 'created_at', 'updated_at'], 'safe'],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => static::class, 'targetAttribute' => ['parent_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['created_by' => 'id']],
            [['approved_by'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['approved_by' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/main', 'ID'),
            'content' => Yii::t('common/main', 'Content'),
            'status' => Yii::t('common/main', 'Status'),
            'author_name' => Yii::t('common/main', 'Author Name'),
            'author_email' => Yii::t('common/main', 'Author Email'),
            'author_ip' => Yii::t('common/main', 'Author Ip'),
            'author_agent' => Yii::t('common/main', 'Author Agent'),
            'created_by' => Yii::t('common/main', 'Created By'),
            'approved_by' => Yii::t('common/main', 'Approved By'),
            'parent_id' => Yii::t('common/main', 'Parent ID'),
            'created_at' => Yii::t('common/main', 'Created At'),
            'updated_at' => Yii::t('common/main', 'Updated At'),
        ];
    }

    public function getAvatar($size = 64)
    {
        if ($this->createdBy) {
            return $this->createdBy->getAvatar($size);
        } else {
            return Gravatar::createUrl($this->author_email, ['size' => $size]);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(static::class, ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function geReplied()
    {
        return $this->hasMany(static::class, ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getProduct()), ['id' => 'product_id'])->viaTable('product_comment', ['comment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::class, ['id' => 'post_id'])->viaTable('post_comment', ['comment_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'approved_by']);
    }
}
