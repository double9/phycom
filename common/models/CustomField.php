<?php

namespace phycom\common\models;

use phycom\common\models\traits\ClassConstantTrait;

use yii\base\InvalidConfigException;
use yii\helpers\Inflector;
use yii\base\Model;
use yii;

/**
 * Class CustomField
 * @package phycom\common\models
 *
 * @property CustomFieldOption[] $options
 */
abstract class CustomField extends Model
{
	use ClassConstantTrait;

	### FIELD TYPES - define which kind of value the custom field holds

	const TYPE_NUMBER = 'number';           // attribute value is a number
	const TYPE_OPTION = 'option';           // attribute value is an option from predefined list
	const TYPE_SELECT = 'select';           // attribute value is multiple options from predefined list.
    const TYPE_TAGS = 'tags';               // attribute value is multiple custom options.
	const TYPE_TEXT = 'text';               // attribute value is an arbitrary text (max length is 255 chars)
	const TYPE_FILE = 'file';               // attribute value is a file
	const TYPE_BOOL = 'bool';               // attribute value is a boolean

	public $name;
    public $type;
	public $label;
	public $shortLabel;
	public $inputMask;

	public $min;
	public $max;
	public $step;
	public $optionValue;

	protected $options;

	private static $dataCache = [];

    /**
     * Finds model by name
     *
     * @param string $name
     * @return null|static
     * @throws yii\base\InvalidConfigException
     */
	public static function findByName($name)
	{
	    static::findAll();
	    return self::$dataCache[static::class][$name] ?? null;
	}

    /**
     * @param $keyword
     * @param array $excluded
     * @return static[]
     * @throws yii\base\InvalidConfigException
     */
	public static function searchByKeyword($keyword, array $excluded = [])
	{
		$matches = [];
        static::findAll();
		foreach (self::$dataCache[static::class] as $name => $value) {
			if (!in_array($name, $excluded) && strpos($name, $keyword) !== false) {
				$matches[] = static::$dataCache[static::class][$name];
			}
		}
		return $matches;
	}

    /**
     * @return static[]
     * @throws yii\base\InvalidConfigException
     * @var string $group
     */
	public static function findAll()
	{
	    if (!isset(self::$dataCache[static::class])) {
            $result = [];
            foreach (static::data() as $name => $data) {
                if ($data === false)
                    continue;
                $result[$name] = self::createInstance($name, $data);
            }
            self::$dataCache[static::class] = $result;
        }
		return array_values(self::$dataCache[static::class]);
	}

    /**
     * @param string $groupName
     * @return static[]
     * @throws InvalidConfigException
     */
	public static function findByGroup(string $groupName)
    {
        $params = isset(static::groups()[$groupName])
            ? static::groups()[$groupName]['items']
            : [];

        $result = [];
        foreach ($params as $paramName) {
            if ($param = static::findByName($paramName)) {
                $result[] = $param;
            }
        }
        return $result;
    }

    /**
     * @param string $type
     * @return static[]
     * @throws yii\base\InvalidConfigException
     */
	public static function findByType(string $type)
    {
        $result = [];
        foreach (static::findAll() as $attribute) {
            if ($attribute->type === $type) {
                $result[] = $attribute;
            }
        }
        return $result;
    }

    /**
     * @param string $name
     * @param array $data
     * @return static|object
     * @throws InvalidConfigException
     */
	protected static function createInstance(string $name, array $data)
	{
		$data['name'] = $name;
		$data['class'] = static::class;

		// every attribute should have at least one option.
        // If the options is not configured we'll create one with same key as the attribute name
		$options = $data['options'] ?? [$name => $data['label'] ?? Inflector::titleize($name)];

		if (isset($data['options'])) {
		    unset($data['options']);
        }
        /**
         * @var static $customField
         */
		$customField = Yii::createObject($data);

		$customFieldOptions = [];
        foreach ($options as $key => $optionConfig) {
            if (is_string($optionConfig)) {
                $optionConfig = ['label' => $optionConfig];
            }
            if (!is_array($optionConfig)) {
                throw new InvalidConfigException('Invalid field option config');
            }
            $optionConfig['key'] = $key;

            if (!isset($optionConfig['label'])) {
                $optionConfig['label'] = Inflector::titleize($key);
            }
            if (!isset($optionConfig['class'])) {
                $optionConfig['class'] = (new static())->getOptionClassName();
            }
            $optionConfig['customField'] = $customField;
            $customFieldOptions[] = Yii::createObject($optionConfig);
        }

        $customField->setOptions($customFieldOptions);

		return $customField;
	}

	/**
	 * @return array
	 */
	public static function getTypes()
	{
		return static::getConstants('TYPE_');
	}

	/**
	 * @return array
	 */
	public static function getNames()
	{
		return static::getConstants('F_');
	}

    /**
     * @return array
     */
    public static function getGroups()
    {
        return static::getConstants('GROUP_');
    }

    /**
     * @return bool
     */
	public function hasOneOption()
    {
        return !in_array($this->type, [self::TYPE_SELECT, self::TYPE_OPTION]);
    }

    /**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name', 'label', 'type'], 'required'],
			[['name', 'label', 'type'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
        return [
            'name'       => Yii::t('common/custom-field', 'Name'),
            'label'      => Yii::t('common/custom-field', 'Label'),
            'shortLabel' => Yii::t('common/custom-field', 'Short label'),
            'type'       => Yii::t('common/custom-field', 'Type'),
        ];
	}

    /**
     * @return string
     */
	public function getTypeLabel()
    {
        switch ($this->type) {
            case self::TYPE_NUMBER:
                return Yii::t('common/custom-field/type', 'Number');
            case self::TYPE_OPTION:
                return Yii::t('common/custom-field/type', 'Option list');
            case self::TYPE_SELECT:
                return Yii::t('common/custom-field/type', 'Multi-option list');
            case self::TYPE_TAGS:
                return Yii::t('common/custom-field/type', 'Tag list');
            case self::TYPE_TEXT:
                return Yii::t('common/custom-field/type', 'Text');
            case self::TYPE_FILE:
                return Yii::t('common/custom-field/type', 'File');
            case self::TYPE_BOOL:
                return Yii::t('common/custom-field/type', 'Boolean');
            default:
                return Inflector::titleize($this->type);
        }
    }

    /**
     * @param string $groupName
     * @return mixed|null
     */
    public function getGroupByName(string $groupName)
    {
        return static::groups()[$groupName] ?? null;
    }

    /**
     * @param CustomFieldOption[] $options
     * @throws InvalidConfigException
     */
	public function setOptions(array $options)
    {
        $OptionsClassName = $this->getOptionClassName();
        foreach ($options as $option) {
            if (!$option instanceof $OptionsClassName) {
                throw new InvalidConfigException('Invalid ' . __CLASS__ . ' option ' . $OptionsClassName);
            }
        }
        $this->options = $options;
    }

    /**
     * @return string|CustomFieldOption
     */
    public function getOptionClassName()
    {
        return CustomFieldOption::class;
    }

    /**
     * @return CustomFieldOption[]
     */
	public function getOptions()
    {
        return $this->options;
    }

	/**
	 * @param mixed $key
	 * @param mixed $defaultKey
	 * @return CustomFieldOption|null
	 */
	public function getOptionByKey($key, $defaultKey = null)
	{
		$default = null;
		foreach ($this->getOptions() as $option) {
			if ($option->key === $key) {
				return $option;
			}
			if ($defaultKey && $option->key === $defaultKey) {
				$default = $option;
			}
		}
		return $default;
	}

    /**
     * @return string
     */
	public function __toString()
    {
        return (string) $this->name;
    }

    /**
     * @return array
     */
    protected static function groups()
    {
        return [];
    }

    /**
	 * @return array
	 *
	 * [
	 *      {{ attribute id }} => [
	 *          ...
	 *      ],
	 *      ...
	 * ]
	 *
	 */
	protected static function data()
	{
		return [];
	}
}
