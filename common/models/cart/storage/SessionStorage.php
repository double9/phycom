<?php

namespace phycom\common\models\cart\storage;


use phycom\common\components\Cart;
use phycom\common\interfaces\CartStorageInterface;

use yii\base\BaseObject;
use yii;

/**
 * Class SessionStorage is a session adapter for cart data storage.
 *
 * @property \yii\web\Session session
 */
class SessionStorage extends BaseObject implements CartStorageInterface
{
    /**
     * @var string
     */
    public $key = 'cart';

    /**
     * @inheritdoc
     */
    public function load(Cart $cart)
    {
        $cartData = [];

        if (false !== ($session = ($this->session->get($this->key, false)))) {
            $cartData = unserialize($session);
        }

        return $cartData;
    }

    /**
     * @inheritdoc
     */
    public function save(Cart $cart)
    {
        $sessionData = serialize($cart->getItems());

        $this->session->set($this->key, $sessionData);
    }


    /**
     * @return \yii\web\Session
     * @throws yii\base\InvalidConfigException
     */
    public function getSession()
    {
        return Yii::$app->get('session');
    }
}
