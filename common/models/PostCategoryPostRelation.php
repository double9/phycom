<?php

namespace phycom\common\models;

use yii;

/**
 * This is the model class for table "post_in_post_category".
 *
 * @property integer $post_id
 * @property integer $category_id
 * @property integer $created_by
 * @property \DateTime $created_at
 *
 */
class PostCategoryPostRelation extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_in_post_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'post_id', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => PostCategory::class, 'targetAttribute' => ['category_id' => 'id']],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => Post::class, 'targetAttribute' => ['post_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    public function init()
    {
	    parent::init();
	    $this->on(static::EVENT_BEFORE_INSERT, function () {
			if (!$this->created_by) {
				$this->created_by = Yii::$app->user->id;
			}
	    });
    }

	/**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(PostCategory::class, ['id' => 'category_id']);
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPost()
	{
		return $this->hasOne(Post::class, ['id' => 'category_id']);
	}

}
