<?php

namespace phycom\common\models;

use yii;

/**
 * This is the model class for table "language".
 *
 * @property string $code
 * @property string $name
 * @property string $native
 * @property string $status
 *
 * @property-read string $ietf
 */
class Language extends ActiveRecord
{
	const DEFAULT_IETF_COUNTRY = 'EE';

	/**
	 * An array mapping language code and default country code for that particular language
	 * This is used when creating an IETF string, see more @ https://en.wikipedia.org/wiki/IETF_language_tag
	 * @var array
	 */
	protected $countryMap = [
		'en' => 'US',
		'et' => 'EE'
	];

	public function behaviors()
	{
		return [];
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'language';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name', 'status'], 'required'],
            [['code'], 'string', 'max' => 2],
            [['name', 'native'], 'string', 'max' => 255],
	        ['status', 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code'   => Yii::t('common/main', 'Code'),
            'name'   => Yii::t('common/main', 'Name'),
            'native' => Yii::t('common/main', 'Native'),
            'status' => Yii::t('common/main', 'Status')
        ];
    }

    public function __toString()
    {
	    return strtoupper($this->code) . ' (' . $this->native . ')';
    }

    public function getIetf()
    {
		return $this->code . '-' . ($this->countryMap[$this->code] ?? self::DEFAULT_IETF_COUNTRY);
    }
}
