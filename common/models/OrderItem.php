<?php

namespace phycom\common\models;

use phycom\common\interfaces\CartItemInterface;
use phycom\common\models\attributes\OrderItemMeta;
use phycom\common\models\attributes\OrderStatus;
use phycom\common\models\behaviors\JsonAttributeBehavior;
use phycom\common\models\product\Variant;
use phycom\common\models\product\VariantOption;
use phycom\common\models\product\Product;
use phycom\common\models\behaviors\CurrencyBehavior;
use phycom\common\helpers\f;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii;

/**
 * This is the model class for table "order_item".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $product_id
 * @property integer $parent_id
 * @property string $code
 * @property array $product_attributes
 * @property OrderItemMeta $meta
 * @property integer $quantity
 * @property int $price
 * @property float $discount
 * @property int $total
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property Order $order
 * @property Product $product
 * @property OrderItem $parent
 * @property ShipmentItem[] $shipmentItems
 * @property Shipment[] $shipments
 * @property string $label
 * @property-read float $weight
 * @property-read array $dimensions
 * @property-read float $parcelWeight
 * @property-read array $parcelDimensions
 * @property-read bool $hasSeparateParcel
 * @property-read int $totalPrice
 * @property-read int $discountPrice
 * @property-read int $discountAmount
 *
 */
class OrderItem extends ActiveRecord implements CartItemInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_item';
    }

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'currency' => [
				'class' => CurrencyBehavior::class,
				'attributes' => ['price', 'total']
			],
			'json-attribute' => [
				'class' => JsonAttributeBehavior::class,
				'attributes' => [
					'meta' => OrderItemMeta::class
				]
			]
		]);
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'code', 'price'], 'required'],
            [['order_id', 'product_id', 'parent_id', 'quantity'], 'integer'],
            [['price', 'total', 'discount'], 'number'],
            [['created_at', 'updated_at', 'meta', 'product_attributes'], 'safe'],
            [['code'], 'string', 'max' => 255],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getOrder()), 'targetAttribute' => ['order_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getProduct()), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                 => Yii::t('common/main', 'ID'),
            'order_id'           => Yii::t('common/main', 'Order ID'),
            'product_id'         => Yii::t('common/main', 'Product ID'),
            'parent_id'          => Yii::t('common/main', 'Parent ID'),
            'code'               => Yii::t('common/main', 'Code'),
            'title'              => Yii::t('common/main', 'Title'),
            'label'              => Yii::t('common/main', 'Label'),
            'product_attributes' => Yii::t('common/main', 'Attributes'),
            'meta'               => Yii::t('common/main', 'Meta'),
            'quantity'           => Yii::t('common/main', 'Quantity'),
            'price'              => Yii::t('common/main', 'Price'),
            'discount'           => Yii::t('common/main', 'Discount'),
            'total'              => Yii::t('common/main', 'Total'),
            'created_at'         => Yii::t('common/main', 'Created At'),
            'updated_at'         => Yii::t('common/main', 'Updated At'),
        ];
    }

    public function getUniqueId()
    {
	    return $this->code . '-' . base64_encode(md5(json_encode($this->product_attributes)));
    }

    public function getCode()
    {
    	return $this->code;
    }

	public function getTotalPrice()
    {
    	if ($this->total === null) {
    		$this->calculateTotal();
	    }
	    return $this->total;
    }

    public function getPrice()
    {
		return $this->price;
    }

    public function getDiscountPrice()
    {
        if (!$this->discount) {
            return null;
        }
        return (int) round(bcmul((string)(1 - $this->discount), $this->price));
    }

    public function getDiscountAmount()
    {
        return $this->discount ? $this->price - $this->getDiscountPrice() : 0;
    }

    public function getQuantity()
    {
	    return $this->quantity;
    }

	public function setQuantity(int $value)
	{
		$this->total = null;
		$this->quantity = $value;
	}

    /**
     * @param mixed $variantName
     * @param mixed $optionValue
     * @throws yii\base\InvalidConfigException
     */
    public function setOption($variantName, $optionValue)
    {
    	if (is_string($variantName)) {
		    $variant = Yii::$app->modelFactory->getVariant()::findByName($variantName);
		    if (!$variant) {
			    throw new yii\base\InvalidArgumentException('Variant "' . $variantName . '" was not found');
		    }
	    } else if ($variantName instanceof Variant) {
    		$variant = $variantName;
	    } else {
    		throw new yii\base\InvalidArgumentException('Invalid variant ' . json_encode($variantName));
	    }
	    $options = $this->product_attributes;
	    $options[$variant->name] = $optionValue;
        $this->product_attributes = $options;
    }

    /**
     * @param mixed $variant
     * @return VariantOption|mixed
     * @throws yii\base\InvalidConfigException
     */
    public function getOption($variant)
    {
    	if (!$variant instanceof Variant) {
    		$variant = Yii::$app->modelFactory->getVariant()::findByName($variant);
	    }
	    if (!$variant instanceof Variant) {
    		throw new yii\base\InvalidArgumentException('Invalid variant ' . json_encode($variant));
	    }
    	$productAttributes = $this->product_attributes;

    	if ($productAttributes && array_key_exists($variant->name, $productAttributes)) {
    	    $option = $variant->getOptionByKey($productAttributes[$variant->name], null);
    	    return $option ?: $productAttributes[$variant->name];
        }
    	return null;
    }

    /**
     * @param string $name
     * @return mixed|VariantOption
     * @throws yii\base\InvalidConfigException
     */
    public function getOptionValue($name)
    {
        $option = $this->getOption($name);
        return $option instanceof VariantOption ? $option->getValue() : $option;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
	    return $this->product_attributes;
    }

	public function setMeta($value)
    {
	    $this->meta = $value instanceof OrderItemMeta ? $value : new OrderItemMeta($value);
    }

    /**
     * @return float - product weight in grams
     */
    public function getWeight()
    {
        return null;
    }

    /**
     * @return float - total weight including package in grams
     */
    public function getParcelWeight()
    {
        return null;
    }

    /**
     * @return array - actual product dimensions in mm
     *
     *  0 - length
     *  1 - width
     *  2 - height
     */
    public function getDimensions()
    {
        return [null, null, null];
    }

    /**
     * @return array - shipment parcel outside dimensions in mm
     *
     *  0 - length
     *  1 - width
     *  2 - height
     */
    public function getParcelDimensions()
    {
        return [null, null, null];
    }

    /**
     * @return bool
     */
    public function getHasSeparateParcel()
    {
        return false;
    }


    public function beforeSave($insert)
    {
		$this->calculateTotal();
	    return parent::beforeSave($insert);
    }

    public function calculateTotal()
    {
	    $this->total = (int) bcmul((string)(1 - $this->discount), (string)($this->price * $this->quantity), 4);
    }

    /**
     * @deprecated use getTitle instead
     * @return string
     */
    public function getLabel()
    {
    	return $this->product ? $this->product->translation->title : $this->meta->title;
    }

    public function getTitle()
    {
        return $this->product ? $this->product->translation->title : $this->meta->title;
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getParent()
	{
		return $this->hasOne(static::class, ['id' => 'parent_id']);
	}

	/**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getOrder()), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getProduct()), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShipmentItems()
    {
        return $this->hasMany(get_class(Yii::$app->modelFactory->getShipmentItem()), ['order_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws yii\base\InvalidConfigException
     */
    public function getShipments()
    {
        return $this->hasMany(get_class(Yii::$app->modelFactory->getShipment()), ['id' => 'shipment_id'])->viaTable('shipment_item', ['order_item_id' => 'id']);
    }
}
