<?php

namespace phycom\common\models;

use phycom\common\models\behaviors\TimestampBehavior;

use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "shop_closed".
 *
 * @property integer $id
 * @property integer $shop_id
 * @property \DateTime $date
 * @property \DateTime $time_from
 * @property \DateTime $time_to
 * @property string $comment
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property Shop $shop
 *
 * @property-read \DateTime $from
 * @property-read \DateTime $to
 */
class ShopClosed extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'timestamp' => [
				'class' => TimestampBehavior::class,
				'attributes' => [
					['created_at', ActiveRecord::EVENT_BEFORE_INSERT],
					['updated_at', ActiveRecord::EVENT_BEFORE_INSERT, ActiveRecord::EVENT_BEFORE_UPDATE],
					'date',
					'time_from',
					'time_to'
				]
			]
		]);
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_closed';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shop_id', 'date'], 'required'],
            [['shop_id'], 'integer'],
            [['date', 'time_from', 'time_to', 'created_at', 'updated_at'], 'safe'],
            [['comment'], 'string'],
            [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getShop()), 'targetAttribute' => ['shop_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('common/main', 'ID'),
            'shop_id'    => Yii::t('common/main', 'Shop ID'),
            'date'       => Yii::t('common/main', 'Date'),
            'time_from'  => Yii::t('common/main', 'Closed From'),
            'time_to'    => Yii::t('common/main', 'Closed Until'),
            'comment'    => Yii::t('common/main', 'Comment'),
            'created_at' => Yii::t('common/main', 'Created At'),
            'updated_at' => Yii::t('common/main', 'Updated At'),
        ];
    }

    /**
     * @return \DateTime
     * @throws \Exception
     */
    public function getFrom()
    {
        $from = (new \DateTime($this->date->format('Y-m-d')));
        if ($this->time_from) {
            $from->setTime($this->time_from->format('H'), $this->time_from->format('i'), 0);
            return $from;
        }
        return $from->setTime(0,0,0);
    }

    /**
     * @return \DateTime
     * @throws \Exception
     */
    public function getTo()
    {
        $to = (new \DateTime($this->date->format('Y-m-d')));
        if ($this->time_to) {
            $to->setTime($this->time_from->format('H'), $this->time_from->format('i'), 0);
            return $to;
        }
        return $to->setTime(0,0,0)->add(new \DateInterval('P1D'));
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShop()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getShop()), ['id' => 'shop_id']);
    }
}
