<?php

namespace phycom\common\models;

use phycom\common\models\behaviors\TimestampBehavior;

use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "shop_supply".
 *
 * @property integer $id
 * @property integer $shop_id
 * @property int $day_of_week
 * @property \DateTime $dispatched_at
 * @property \DateTime $delivery_at
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 * @property bool $delivery
 *
 * @property Shop $shop
 */
class ShopSupply extends ActiveRecord
{
    private $delivery;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_supply';
    }


    /**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'timestamp' => [
				'class' => TimestampBehavior::class,
				'attributes' => [
					['created_at', ActiveRecord::EVENT_BEFORE_INSERT],
					['updated_at', ActiveRecord::EVENT_BEFORE_INSERT, ActiveRecord::EVENT_BEFORE_UPDATE],
					'dispatched_at',
					'delivery_at'
				]
			]
		]);
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shop_id', 'day_of_week', 'dispatched_at', 'delivery_at'], 'required'],
            [['shop_id', 'day_of_week'], 'integer'],
            [['delivery', 'dispatched_at', 'delivery_at', 'created_at', 'updated_at'], 'safe'],
            [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getShop()), 'targetAttribute' => ['shop_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/main', 'ID'),
            'shop_id' => Yii::t('common/main', 'Shop ID'),
            'day_of_week' => Yii::t('common/main', 'Day Of Week'),
            'dispatched_at' => Yii::t('common/main', 'Dispatch time'),
            'delivery_at' => Yii::t('common/main', 'Est. delivery time'),
            'created_at' => Yii::t('common/main', 'Created At'),
            'updated_at' => Yii::t('common/main', 'Updated At'),
            'delivery' => Yii::t('common/main', 'Is Delivery'),
        ];
    }

    public function getDelivery()
    {
        return $this->delivery;
    }

    public function setDelivery($value)
    {
        $this->delivery = $value ? 1 : 0;
    }


    public function afterFind()
    {
        parent::afterFind();
        $this->delivery = 1;
    }

    public function loadDefaultValues($skipIfSet = true)
    {
        parent::loadDefaultValues($skipIfSet);
        if (!$this->dispatched_at || !$skipIfSet) {
            $this->dispatched_at = \DateTime::createFromFormat('H:i', '11:00', new \DateTimeZone('UTC'));
        }
        if (!$this->delivery_at || !$skipIfSet) {
            $this->delivery_at = \DateTime::createFromFormat('H:i', '14:00', new \DateTimeZone('UTC'));
        }
        $this->delivery = 0;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShop()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getShop()), ['id' => 'shop_id']);
    }
}
