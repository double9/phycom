<?php

namespace phycom\common\models;


use phycom\common\models\behaviors\TimestampBehavior;
use yii;

/**
 * This is the model class for table "user_activity".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $action
 * @property string $ip
 * @property string $user_agent
 * @property \DateTime $created_at
 *
 * @property User $user
 */
class UserActivity extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_activity';
    }

	/**
	 * Log current user URL action
	 * @return bool - is success
	 */
	public static function logEffectiveUserUrl()
	{
		return static::create(Yii::$app->request->method . ' ' . Yii::$app->request->url);
	}

	/**
	 * Creates an user activity record
	 *
	 * @param int $userId
	 * @param string $action
	 * @return bool
	 */
	public static function create($action, $userId = null)
	{
        $userId = $userId ?: Yii::$app->user->id;

		if (!$userId || !is_numeric($userId)) {
			Yii::warning('Error creating user activity "'.$action.'". User is required');
			return false;
		}

		$model = new static();
		$model->action = $action;
		$model->user_id = $userId;

		if (is_subclass_of(Yii::$app, 'yii\console\Application')) {
			$model->ip = '127.0.0.1';
		} else {
			$model->ip = Yii::$app->request->userIP;
			$model->user_agent = Yii::$app->request->userAgent;
		}
		if (!$model->save()) {
			Yii::error('Error saving user activity:');
			Yii::error($model->errors);
			return false;
		} else {
			return true;
		}
	}

	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::class,
				'attributes' => [
					['created_at', ActiveRecord::EVENT_BEFORE_INSERT]
				]
			]
		];
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'action', 'ip'], 'required'],
            [['user_id'], 'integer'],
            [['action', 'user_agent'], 'string'],
            [['created_at'], 'safe'],
            [['ip'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('common/main', 'ID'),
            'user_id'    => Yii::t('common/main', 'User ID'),
            'action'     => Yii::t('common/main', 'Action'),
            'ip'         => Yii::t('common/main', 'Ip'),
            'user_agent' => Yii::t('common/main', 'User Agent'),
            'created_at' => Yii::t('common/main', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'user_id']);
    }
}
