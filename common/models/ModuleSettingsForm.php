<?php

namespace phycom\common\models;

use phycom\common\helpers\Date;
use phycom\common\models\traits\ModelTrait;
use phycom\common\components\Formatter;

use yii\helpers\Inflector;
use yii\base\Module;
use yii\base\Model;
use yii;

/**
 * Class Settings
 * @package phycom\common\models
 *
 * @property-read Module $module
 */
abstract class ModuleSettingsForm extends Model
{
    const TYPE_BOOLEAN = 'boolean';
    const TYPE_OPTION = 'option';
    const TYPE_INTEGER = 'integer';
    const TYPE_DECIMAL = 'decimal';
    const TYPE_DATETIME = 'datetime';
    const TYPE_TEXT = 'text';

    use ModelTrait;

    protected $module;
    /**
     * @var Formatter
     */
    protected $formatter;

    public function __construct(Module $module, array $config = [])
    {
        $this->module = $module;
        parent::__construct($config);
    }

    public function formLabel()
    {
        return Inflector::titleize($this->module->id) . ' ' . Yii::t('backend/settings', 'Settings');
    }

    public function formName()
    {
        $n = [$this->module->id];
        if ($this->module->module) {
            $n[] = ucfirst($this->module->module->id);
        }
        $n[] = 'Settings';
        return implode('', $n);
    }

    public function getModule()
    {
        return $this->module;
    }

    public function init()
    {
        parent::init();
        $this->formatter = Yii::createObject([
            'class' => Formatter::class,
            'nullDisplay' => null,
            'decimalSeparator' => '.',
            'thousandSeparator' => '',
            'currencyCode' => null,
            'booleanFormat' => ['0', '1'],
            'numberFormatterOptions' => [
                \NumberFormatter::MAX_FRACTION_DIGITS => 3,
                \NumberFormatter::MIN_FRACTION_DIGITS => 2,
                \NumberFormatter::GROUPING_USED => false,
            ],
            'numberFormatterTextOptions' => [
                \NumberFormatter::CURRENCY_CODE => '',
                \NumberFormatter::ZERO_DIGIT_SYMBOL => '.'
            ],
            'numberFormatterSymbols' => [
                \NumberFormatter::CURRENCY_SYMBOL => '',
                \NumberFormatter::PERCENT_SYMBOL => '',
                \NumberFormatter::DECIMAL_SEPARATOR_SYMBOL => '.'
            ]
        ]);
    }

    public function populateModule()
    {
        foreach ($this->attributes as $key => $value) {
            if (property_exists($this->module, $key)) {
                $format = $this->getAttributeType($key);
                $this->module->$key = $this->parse($format, $value);
            }
        }
    }

    public function populate()
    {
        foreach ($this->attributes as $name => $value) {

            $module = $this->module->module ? $this->module->module->id : $this->module->id;
            $submodule = $this->module->module ? $this->module->id : null;

            if ($setting = ModuleSetting::findByKey($name, $module, $submodule)) {
                $this->$name = $setting->value;
            } else {
                $type = $this->getAttributeType($name);
                $this->$name = $this->convertString($type, $this->module->$name);
            }
        };
        return $this;
    }

    /**
     * @return bool
     */
    public function save()
    {
        foreach ($this->attributes as $name => $value) {

            $module = $this->module->module ? $this->module->module->id : $this->module->id;
            $submodule = $this->module->module ? $this->module->id : null;
            $formattedValue = $this->format($this->getAttributeType($name), $value);

            $setting = ModuleSetting::set($name, $formattedValue, $module, $submodule);

            if ($setting->hasErrors()) {
                $this->addErrors($setting->errors);
                return false;
            }
        };
        return true;
    }

    public function getAttributeRules($attribute)
    {
        $rules = [];
        foreach ($this->rules() as $rule) {
            $attributes = is_string($rule[0]) ? [$rule[0]] : $rule[0];
            if (in_array($attribute, $attributes)) {
                array_shift($rule);
                $rules[] = $rule;
            }
        }
        return $rules;
    }

    public function getAttributeType($attribute)
    {
        $type = null;
        foreach ($this->rules() as $rule) {
            $attributes = is_string($rule[0]) ? [$rule[0]] : $rule[0];
            if (in_array($attribute, $attributes)) {
                switch ($rule[1]) {
                    case 'boolean':
                        return self::TYPE_BOOLEAN;
                    case 'in':
                        return self::TYPE_OPTION;
                    case 'date':
                    case 'time':
                        return self::TYPE_DATETIME;
                    case 'double':
                    case 'number':
                        return self::TYPE_DECIMAL;
                    case 'integer':
                        return self::TYPE_INTEGER;
                }
            }
        }
        return $type ?: self::TYPE_TEXT;
    }


    public function parse($type, $value)
    {
        switch ($type) {
            case self::TYPE_BOOLEAN:
                return (bool) $value;
            case self::TYPE_DATETIME:
                return Date::create($value, $this->formatter->dateFormat);
            case self::TYPE_DECIMAL:
                return (float) $value;
            case self::TYPE_INTEGER:
                return (int) $type;
            default:
                return $value;
        }
    }

    public function convertString($type, $value)
    {
        switch ($type) {
            case self::TYPE_BOOLEAN:
                $value = (bool) $value;
                return $value ? '1' : '0';
            case self::TYPE_DATETIME:
                return Date::create($value)->toDateStr();
            case self::TYPE_DECIMAL:
            case self::TYPE_INTEGER:
            default:
                return (string) $value;
        }
    }

    public function format($attributeType, $value)
    {
        $format = $attributeType;
        if ($attributeType === self::TYPE_OPTION) {
            $format = 'text';
        }
        $method = 'as' . ucfirst($format);
        return $this->formatter->$method($value);
    }
}