<?php

namespace phycom\common\models;

use phycom\common\components\AddressFormatter;
use phycom\common\models\attributes\AddressField;
use phycom\common\models\attributes\AddressType;
use phycom\common\models\attributes\ContactAttributeStatus;

use yii\helpers\Json;
use yii\helpers\Inflector;
use yii\helpers\ArrayHelper;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $vendor_id
 * @property integer $shop_id
 * @property string $country
 * @property string $state
 * @property string $province
 * @property string $city
 * @property string $locality
 * @property string $district
 * @property string $street
 * @property string $house;
 * @property string $room
 * @property string $postcode
 * @property AddressType $type
 * @property ContactAttributeStatus $status
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property Vendor $vendor
 * @property Shop $shop
 * @property User $user
 * @property AddressMeta $meta
 *
 * @property-read array $l2
 * @property-read array $l3
 */
class Address extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'dynamic-attribute' => [
				'attributes' => [
					'status' => ContactAttributeStatus::class,
					'type' => AddressType::class
				]
			]
		]);
	}

    /**
     * @param array|string $data - array of address attributes or json string
     * @return static
     * @throws yii\base\InvalidConfigException
     */
	public static function create($data)
	{
        /**
         * @var static $model
         */
		$model = Yii::createObject(static::class);
		$model->attributes = (Yii::createObject(AddressField::class, [$data]))->attributes;

		return $model;
	}

    /**
     * @param string $cityName
     * @return null|string
     */
	public static function generateCityCode(string $cityName)
    {
        $code = Inflector::transliterate($cityName);
        $code = substr($code, 0, 1) . substr($code, 2, 2);
        $code = preg_replace('/[\s+]|[\d+]/', '', strtoupper($code));
        return $code;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'vendor_id', 'shop_id'], 'integer'],
            [['country', 'street', 'type', 'status'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['country', 'state', 'province', 'locality', 'city', 'district', 'street', 'postcode', 'house', 'room'], 'string', 'max' => 255],
	        ['status', 'in', 'range' => ContactAttributeStatus::all()],
	        ['type', 'in', 'range' => AddressType::all()],
            [['vendor_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getVendor()), 'targetAttribute' => ['vendor_id' => 'id']],
            [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getShop()), 'targetAttribute' => ['shop_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('common/main', 'ID'),
            'user_id'    => Yii::t('common/main', 'User ID'),
            'vendor_id'  => Yii::t('common/main', 'Vendor ID'),
            'shop_id'    => Yii::t('common/main', 'Shop ID'),
            'country'    => Yii::t('common/main', 'Country'),
            'state'      => Yii::t('common/main', 'State'),
            'province'   => Yii::t('common/main', 'Province'),
            'locality'   => Yii::t('common/main', 'Locality'),
            'city'       => Yii::t('common/main', 'City'),
            'district'   => Yii::t('common/main', 'District'),
            'street'     => Yii::t('common/main', 'Street'),
            'house'      => Yii::t('common/main', 'House'),
            'room'       => Yii::t('common/main', 'Room'),
            'postcode'   => Yii::t('common/main', 'Postcode'),
            'type'       => Yii::t('common/main', 'Type'),
            'status'     => Yii::t('common/main', 'Status'),
            'created_at' => Yii::t('common/main', 'Created At'),
            'updated_at' => Yii::t('common/main', 'Updated At'),
        ];
    }

    public function updateJson($json)
    {
        $this->setAttributes(Json::decode($json));
    }

	/**
	 * Finds similar addresses from the db.
	 *
	 * @param null $relatedAttribute
	 * @param bool $strictComparison
	 * @return static[]
	 */
    public function getSimilar($relatedAttribute = null, $strictComparison = false)
    {
    	$query = static::find()
            ->where([
                'country'  => $this->country,
                'street'   => $this->street,
                'postcode' => $this->postcode
            ])
	        ->andWhere(['not', ['status' => ContactAttributeStatus::DELETED]]);

    	if ($relatedAttribute) {
    		$query->andWhere([$relatedAttribute => $this->$relatedAttribute]);
	    }
	    if ($strictComparison) {
            $query->andWhere([
                'province' => $this->province,
                'locality' => $this->locality,
                'city'     => $this->city,
                'district' => $this->district
            ]);
	    }
	    return $query->all();
    }

    /**
     * @return AddressField
     * @throws yii\base\InvalidConfigException
     */
    public function export()
    {
        /**
         * @var AddressField|object $a
         */
    	$a = Yii::createObject(AddressField::class);
    	$a->country = $this->country;
    	$a->state = $this->state;
    	$a->province = $this->province;
    	$a->city = $this->city;
    	$a->locality = $this->locality;
    	$a->district = $this->district;
    	$a->street = $this->street;
    	$a->house = $this->house;
    	$a->room = $this->room;
    	$a->postcode = $this->postcode;
    	return $a;
    }

    /**
     * @throws yii\base\InvalidConfigException
     */
    public function exportArray()
    {
	    $this->export()->exportArray(true);
    }

    /**
     * @return string
     * @throws yii\base\InvalidConfigException
     *
     * @deprecated  use Yii::$app->formatter->asAddress($address) instead
     */
    public function getLabel()
    {
	    return  $this->getFormatter()->asLabel();
    }

    /**
     * @return array
     * @throws yii\base\InvalidConfigException
     */
    public function getL2()
    {
        return $this->getFormatter()->asL2();
    }

    /**
     * @return array
     * @throws yii\base\InvalidConfigException
     */
    public function getL3()
    {
        return $this->getFormatter()->asL3();
    }

    /**
     * @return AddressFormatter|object
     * @throws yii\base\InvalidConfigException
     */
    protected function getFormatter()
    {
        return Yii::createObject(AddressFormatter::class, [$this->export()]);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        try {
            return implode(', ', $this->exportArray());
        } catch (yii\base\InvalidConfigException $e) {
            Yii::error('Error converting address ' . $this->id . ' to string: ' . $e->getMessage(), __METHOD__);
            return '';
        }
    }

	/**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getVendor()), ['id' => 'vendor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShop()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getShop()), ['id' => 'shop_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeta()
    {
        return $this->hasOne(AddressMeta::class, ['address_id' => 'id']);
    }
}
