<?php

namespace phycom\common\models;

use phycom\common\helpers\c;
use phycom\common\models\attributes\FileType;
use phycom\common\models\attributes\FileStatus;

use yii\helpers\ArrayHelper;
use yii\imagine\Image;
use yii\base\InvalidCallException;
use yii;

/**
 * This is the model class for table "file".
 *
 * @property integer $id
 * @property FileType $mime_type
 * @property string $name
 * @property string $filename
 * @property string $bucket
 * @property FileStatus $status
 * @property integer $created_by
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property-read string $thumbFilename
 * @property User $createdBy
 * @property-read string $url
 * @property-read string $thumbUrl
 */
class File extends ActiveRecord
{
	const DEFAULT_THUMB_QUALITY = 80;

	const THUMB_SIZE_SMALL = 'small';
	const THUMB_SIZE_MEDIUM = 'medium';
	const THUMB_SIZE_LARGE = 'large';
	const THUMB_SIZE_DEFAULT = self::THUMB_SIZE_MEDIUM;

	/**
	 * @var array
	 */
	protected $thumbSizes = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file';
    }

    public function init()
    {
        parent::init();
        $this->thumbSizes = c::param('thumbSizes', [
            self::THUMB_SIZE_SMALL  => [120, null],
            self::THUMB_SIZE_MEDIUM => [400, null],
            self::THUMB_SIZE_LARGE  => [640, null]
        ]);
    }

    /**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'dynamic-attribute' => [
				'attributes' => [
					'status' => FileStatus::class,
					'mime_type' => FileType::class
				]
			]
		]);
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mime_type', 'filename', 'status', 'bucket'], 'required'],
            [['name', 'filename'], 'string'],
            [['created_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['bucket'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('common/main', 'ID'),
            'mime_type'  => Yii::t('common/main', 'Mime Type'),
            'name'       => Yii::t('common/main', 'Name'),
            'filename'   => Yii::t('common/main', 'Filename'),
            'bucket'     => Yii::t('common/main', 'Bucket'),
            'status'     => Yii::t('common/main', 'Status'),
            'created_by' => Yii::t('common/main', 'Created By'),
            'created_at' => Yii::t('common/main', 'Created At'),
            'updated_at' => Yii::t('common/main', 'Updated At'),
        ];
    }

    public function getThumbFilename($size = self::THUMB_SIZE_DEFAULT)
    {
    	return pathinfo($this->filename, PATHINFO_FILENAME) . '_' . $size . '.' . pathinfo($this->filename, PATHINFO_EXTENSION);
    }

    public function getUrl()
    {
		return Yii::$app->fileStorage->getBucket($this->bucket)->getFileUrl($this->filename);
    }

    public function getThumbUrl($size = self::THUMB_SIZE_DEFAULT)
    {
	    if (!$this->mime_type->isImage) {
		    throw new InvalidCallException('Error creating thumbnail. File ' . $this->id . ' is not an image file');
	    }
	    return Yii::$app->fileStorage->getBucket($this->bucket)->getFileUrl($this->getThumbFilename($size));
    }


    public function generateThumbs()
    {
    	if (!$this->mime_type->isImage) {
    		throw new InvalidCallException('Error creating thumbnail. File ' . $this->id . ' is not an image file');
	    }
        $bucket = Yii::$app->fileStorage->getBucket($this->bucket);
        $file = Yii::getAlias('@files/' . $this->bucket . '/' . $this->filename);

        foreach ($this->thumbSizes as $size => [$width, $height]) {

        	if (Yii::$app->fileStorage->getBucket($this->bucket)->fileExists($this->getThumbFilename($size))) {
		        Yii::$app->fileStorage->getBucket($this->bucket)->deleteFile($this->getThumbFilename($size));
	        }

	        $tmpFilename = tempnam('/tmp', '_thumb_' . $size);

        	Image::thumbnail($file, $width, $height)
                ->save($tmpFilename, ['quality' => static::DEFAULT_THUMB_QUALITY]);

	        $bucket->copyFileIn($tmpFilename, $this->getThumbFilename($size));
        }
    }

	/**
	 * @return bool
	 */
    public function delete()
    {
    	$this->status = FileStatus::DELETED;
    	return $this->save();
    }

	/**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'created_by']);
    }

    public function __toString()
    {
        return $this->getUrl();
    }
}
