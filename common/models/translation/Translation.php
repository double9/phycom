<?php

namespace phycom\common\models\translation;


use phycom\common\models\attributes\TranslationStatus;
use phycom\common\models\Language;
use phycom\common\models\ActiveRecord;
use phycom\common\models\behaviors\UrlKeyBehavior;

use yii\helpers\ArrayHelper;
use yii;

/**
 * Base class for the Translation Active Record models
 *
 * Class Translation
 * @package phycom\common\models\translation
 *
 * @property integer $id
 * @property string $language
 * @property string $url_key
 * @property string $title
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property TranslationStatus $status
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property Language $languageModel
 * @property ActiveRecord $model
 *
 * @method generateUrlKey()
 */
abstract class Translation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'dynamic-attribute' => [
                'attributes' => [
                    'status' => TranslationStatus::class
                ]
            ],
            'url-key' => UrlKeyBehavior::class
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'status'], 'required', 'enableClientValidation' => false],
            [['title', 'meta_title', 'meta_keywords', 'meta_description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['language'], 'string', 'max' => 2],
            [['url_key'], 'unique'],
            [['url_key'], 'string', 'max' => 255],
            [['language'], 'exist', 'skipOnError' => true, 'targetClass' => Language::class, 'targetAttribute' => ['language' => 'code']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => Yii::t('common/translation', 'ID'),
            'language'         => Yii::t('common/translation', 'Language'),
            'url_key'          => Yii::t('common/translation', 'Url Key'),
            'title'            => Yii::t('common/translation', 'Title'),
            'meta_title'       => Yii::t('common/translation', 'Meta title'),
            'meta_keywords'    => Yii::t('common/translation', 'Meta keywords'),
            'meta_description' => Yii::t('common/translation', 'Meta description'),
            'status'           => Yii::t('common/translation', 'Translation status'),
            'created_at'       => Yii::t('common/translation', 'Created At'),
            'updated_at'       => Yii::t('common/translation', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguageModel()
    {
        return $this->hasOne(Language::class, ['code' => 'language']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    abstract public function getModel();
}