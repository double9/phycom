<?php

namespace phycom\common\models\translation;

use phycom\common\models\Language;
use phycom\common\models\product\ProductCategory;

use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "product_category_translation".
 *
 * @property integer $product_category_id
 * @property string $description
 *
 * @property ProductCategory $model
 * @property string fallbackLanguage
 */
class ProductCategoryTranslation extends Translation
{
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_category_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required', 'enableClientValidation' => false],
            [['product_category_id', 'language', 'status', 'url_key'], 'required'],
            [['product_category_id'], 'integer'],
            [['title', 'description', 'meta_title', 'meta_keywords', 'meta_description'], 'string'],
            [['created_at', 'updated_at', 'status'], 'safe'],
            [['language'], 'string', 'max' => 2],
	        [['url_key'], 'unique'],
            [['url_key'], 'string', 'max' => 255],
            [['language'], 'exist', 'skipOnError' => true, 'targetClass' => Language::class, 'targetAttribute' => ['language' => 'code']],
            [['product_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::class, 'targetAttribute' => ['product_category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'product_category_id' => Yii::t('common/translation', 'Product Category ID'),
            'description'         => Yii::t('common/translation', 'Description'),
        ]);
    }

    public function getFallbackLanguage()
    {
        $language = Language::findOne(['code' => $this->language]);
        return Yii::$app->lang->findFallbackLanguage($language)->code;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(ProductCategory::class, ['id' => 'product_category_id']);
    }
}
