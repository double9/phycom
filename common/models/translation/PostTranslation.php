<?php

namespace phycom\common\models\translation;

use phycom\common\models\attributes\PostType;
use phycom\common\models\Post;

use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "post_translation".
 *
 * @property integer $post_id
 * @property string $outline
 * @property string $content
 * @property array $meta
 * @property Post $model
 */
class PostTranslation extends Translation
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['title'], 'required', 'enableClientValidation' => false, 'when' => function () {
                return (string)$this->model->type !== PostType::SHOP;
            }],
            [['post_id', 'content'], 'required', 'enableClientValidation' => false],
            [['post_id'], 'integer'],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => Post::class, 'targetAttribute' => ['post_id' => 'id']],
            [['outline', 'content'], 'string'],
            [['meta'], 'safe']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'post_id' => Yii::t('common/translation', 'Post ID'),
            'outline' => Yii::t('common/translation', 'Outline'),
            'content' => Yii::t('common/translation', 'Content'),
            'meta'    => Yii::t('common/translation', 'Meta'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(Post::class, ['id' => 'post_id']);
    }
}
