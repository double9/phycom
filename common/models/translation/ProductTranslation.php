<?php

namespace phycom\common\models\translation;

use phycom\common\models\product\Product;
use phycom\common\models\Language;

use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "product_translation".
 *
 * @property integer $product_id
 * @property string $outline
 * @property string $description
 *
 * @property Product $model
 */
class ProductTranslation extends Translation
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required', 'enableClientValidation' => false],
            [['product_id', 'language', 'status', 'title'], 'required'],
            [['product_id'], 'integer'],
            [['title', 'outline', 'description', 'meta_title', 'meta_keywords', 'meta_description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['language'], 'string', 'max' => 2],
	        [['url_key'], 'unique'],
            [['url_key'], 'string', 'max' => 255],
            [['language'], 'exist', 'skipOnError' => true, 'targetClass' => Language::class, 'targetAttribute' => ['language' => 'code']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getProduct()), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'product_id'  => Yii::t('common/translation', 'Product ID'),
            'outline'     => Yii::t('common/translation', 'Outline'),
            'description' => Yii::t('common/translation', 'Description')
        ]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getProduct()), ['id' => 'product_id']);
    }
}
