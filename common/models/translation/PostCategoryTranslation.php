<?php

namespace phycom\common\models\translation;

use phycom\common\models\Language;
use phycom\common\models\PostCategory;

use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "post_category_translation".
 *
 * @property integer $post_category_id
 * @property string $description
 *
 * @property PostCategory $model
 * @property string fallbackLanguage
 */
class PostCategoryTranslation extends Translation
{
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_category_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required', 'enableClientValidation' => false],
            [['post_category_id', 'language', 'status', 'url_key'], 'required'],
            [['post_category_id'], 'integer'],
            [['title', 'description', 'meta_title', 'meta_keywords', 'meta_description'], 'string'],
            [['created_at', 'updated_at', 'status'], 'safe'],
            [['language'], 'string', 'max' => 2],
	        [['url_key'], 'unique'],
            [['url_key'], 'string', 'max' => 255],
            [['language'], 'exist', 'skipOnError' => true, 'targetClass' => Language::class, 'targetAttribute' => ['language' => 'code']],
            [['post_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => PostCategory::class, 'targetAttribute' => ['post_category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'post_category_id' => Yii::t('common/translation', 'post Category ID'),
            'description'      => Yii::t('common/translation', 'Description'),
        ]);
    }

    public function getFallbackLanguage()
    {
    	foreach (Yii::$app->lang->enabled as $language) {
    		if ($language->code !== $this->language) {
    			return $language->code;
		    }
	    }
	    return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(PostCategory::class, ['id' => 'post_category_id']);
    }
}
