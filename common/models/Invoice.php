<?php

namespace phycom\common\models;

use phycom\common\components\FileStorage;
use phycom\common\helpers\c;
use phycom\common\helpers\Checksum731;
use phycom\common\jobs\InvoicePdfJob;
use phycom\common\models\attributes\InvoiceStatus;
use phycom\common\models\attributes\CustomerType;
use phycom\common\models\attributes\OrderStatus;
use phycom\common\models\attributes\PaymentStatus;
use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "invoice".
 *
 * @property integer $id
 * @property string $number
 * @property integer $order_id
 * @property InvoiceStatus $status
 * @property string $file
 * @property string $customer
 * @property CustomerType $customer_type
 * @property string $reg_no
 * @property array $address
 * @property string $notes
 * @property \DateTime $created_at
 * @property \DateTime $due_date
 *
 * @property int $total
 * @property int $totalPaid
 * @property Order $order
 * @property Payment[] $payments
 * @property Payment $lastPendingPayment
 * @property Shipment[] $shipments
 * @property InvoiceItem[] $items
 * @property OrderItem[] $orderItems
 * @property string $fileUrl
 * @property bool $isPaid
 */
class Invoice extends ActiveRecord
{
    const EVENT_AFTER_INVOICE_CREATED = 'afterInvoiceCreated';

    /**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'dynamic-attribute' => [
				'attributes' => [
					'status' => InvoiceStatus::class,
					'customer_type' => CustomerType::class
				]
			],
            'timestamp' => [
                'attributes' => [
                    ['created_at', ActiveRecord::EVENT_BEFORE_INSERT],
                    ['updated_at', ActiveRecord::EVENT_BEFORE_INSERT, ActiveRecord::EVENT_BEFORE_UPDATE],
                    'due_date'
                ]
            ]
		]);
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        	[['order_id'], 'integer'],
            [['status', 'customer', 'customer_type', 'order_id'], 'required'],
            [['file', 'notes'], 'string'],
            [['address', 'created_at', 'due_date', 'status', 'customer_type'], 'safe'],
            [['number', 'customer', 'reg_no'], 'string', 'max' => 255],
            [['number'], 'unique'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getOrder()), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => Yii::t('common/main', 'ID'),
            'number'        => Yii::t('common/main', 'Number'),
            'order_id'      => Yii::t('common/main', 'Order ID'),
            'status'        => Yii::t('common/main', 'Status'),
            'file'          => Yii::t('common/main', 'File'),
            'customer'      => Yii::t('common/main', 'Customer'),
            'customer_type' => Yii::t('common/main', 'Customer Type'),
            'reg_no'        => Yii::t('common/main', 'Reg No'),
            'notes'         => Yii::t('common/main', 'Notes'),
            'address'       => Yii::t('common/main', 'Address'),
            'created_at'    => Yii::t('common/main', 'Created At'),
            'due_date'      => Yii::t('common/main', 'Due date'),
        ];
    }

    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_INVOICE_CREATED, function ($e) {
            $job = new InvoicePdfJob();
            $job->invoiceNumber = $this->number;
            $jobId = Yii::$app->queue2->delay(1)->push($job);
            Yii::info('Invoice pdf job ' . $jobId . ' queued', __METHOD__);
        });
    }

    /**
	 * Creates the pdf invoice document
	 * TODO
	 */
    public function createFile()
    {

    }

	/**
	 * Sends the invoice to user email
	 * TODO
	 */
    public function send()
    {

    }

	/**
	 * @param mixed $status
	 * @return bool
	 */
	public function updateStatus($status)
	{
		$this->status = (string) $status;
		if (!$this->update(true, ['status'])) {
			Yii::error('Error updating invoice ' . $this->id . ' status: ' . json_encode($this->errors), __METHOD__);
			return false;
		}
		return true;
	}

	public function generateNumber()
	{
        Yii::trace('Generating invoice number', __METHOD__);
		if (!$this->isNewRecord) {
			throw new yii\base\InvalidCallException('Can\'t generate invoice number for existing invoice ' . $this->id);
		}
		if ($lastInvoice = Yii::$app->modelFactory->getInvoice()::find()->orderBy(['id' => SORT_DESC])->one()) {
			/**
			 * @var Invoice $lastInvoice
			 */
			$lastInvoiceNr = (int) substr($lastInvoice->number, 5, -1);
		} else {
			$lastInvoiceNr = c::param('invoiceNoBegin');
		}
		$today = new \DateTime();
		$invoiceNr = $today->format('ym') . '-' . (string)($lastInvoiceNr + 1);
		$this->number = $invoiceNr . Checksum731::calculate($invoiceNr);
	}

	public function getIsPaid()
	{
		return $this->status->is(InvoiceStatus::PAID);
	}

	/**
	 * Checks if invoice has been paid and updates invoice status
	 */
	public function checkPaid()
	{
		if (!$this->isPaid) {
			unset($this->payments);
			$totalPaid = $this->getTotalPaid();
			if ($totalPaid >= $this->getTotal()) {
				$this->updateStatus(InvoiceStatus::PAID);
				$order = $this->order;
				$order->status = OrderStatus::PAYMENT_COMPLETE;
				$order->paid_at = new \DateTime();
				$order->update();
			}
		}
	}

	public function getTotalPaid()
	{
		$paid = 0;
		/**
		 * @var Payment[] $payments
		 */
		$payments = $this->getPayments()->where(['status' => PaymentStatus::COMPLETED])->all();
		foreach ($payments as $payment) {
			$paid += $payment->amount;
		}
		return $paid;
	}

	public function getTotal()
	{
		$total = 0;
		foreach ($this->items as $line) {
			$total += $line->orderItem->total;
		}
		return $total;
	}

    /**
     * @param string $paymentMethod
     * @return array|Payment|null|yii\db\ActiveRecord
     * @throws \Throwable
     * @throws yii\base\Exception
     * @throws yii\db\StaleObjectException
     */
	public function createPayment($paymentMethod)
	{
		if ($this->isNewRecord) {
			throw new yii\base\InvalidCallException('Error creating payment. Invoice must be saved first');
		}
		if ($this->isPaid) {
			throw new yii\base\InvalidCallException('Error creating payment. Invoice is already paid');
		}
		/**
		 * the amount needed to be paid when subtracting other payments
		 */
		$amount = max(0, $this->total - $this->totalPaid);


		if ($payment = $this->getLastPendingPayment()) {
			$payment->payment_method = $paymentMethod;
			$payment->amount = $amount;
			$payment->update();
			return $payment;
		}

		$payment = Yii::$app->modelFactory->getPayment();
		$payment->payment_method = $paymentMethod;
		if ($this->order->user_id) {
            $payment->reference_number = (string) $this->order->user->reference_number;
        }
		$payment->invoice_id = $this->id;
		$payment->amount = $amount;
		$payment->status = PaymentStatus::PENDING;

		if (!$payment->save()) {
			throw new yii\base\Exception('Error saving payment: ' . json_encode($payment->errors));
		}

		return $payment;
	}



	public function getLastPendingPayment()
	{
		return $this->getPayments()->where(['status' => PaymentStatus::PENDING])->orderBy(['id' => SORT_DESC])->one();
	}

	/**
	 * @return string
	 */
	public function getFileUrl()
	{
		return Yii::$app->fileStorage->getBucket(FileStorage::BUCKET_INVOICES)->getFileUrl($this->file);
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getOrder()), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(get_class(Yii::$app->modelFactory->getPayment()), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShipments()
    {
        return $this->hasMany(get_class(Yii::$app->modelFactory->getShipment()), ['invoice_id' => 'id']);
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getItems()
	{
		return $this->hasMany(InvoiceItem::class, ['invoice_id' => 'id']);
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(get_class(Yii::$app->modelFactory->getOrderItem()), ['id' => 'order_item_id'])->viaTable('invoice_item', ['invoice_id' => 'id']);
    }
}
