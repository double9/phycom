<?php

namespace phycom\common\models;

use yii\helpers\Json;
use yii;

/**
 * This is the model class for table "module_setting".
 *
 * @property integer $id
 * @property string $module
 * @property string $submodule
 * @property string $key
 * @property string $value
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 */
class ModuleSetting extends ActiveRecord
{
	/**
	 * @param string $key
     * @param string $module
     * @param string $submodule
	 * @param null $defaultValue
	 * @return null|string
	 */
	public static function get($key, string $module, string $submodule = null, $defaultValue = null)
	{
		/**
		 * @var Setting $model
		 */
		if ($model = static::findByKey($key, $module, $submodule)) {
		    return $model->value;
        };
		return $defaultValue;
	}

	/**
	 * @param string $key
	 * @param mixed $value
     * @param string $module
     * @param string $submodule
	 * @return static
	 */
	public static function set(string $key, $value, string $module, string $submodule = null)
	{
		$model = static::findByKey($key, $module, $submodule);
		if (!$model) {
			$model = new static;
			$model->key = $key;
			$model->module = $module;
			$model->submodule = $submodule;
		}
		$model->value = $value;
		$model->save();
		return $model;
	}


	/**
	 * @param $key
     * @param string $module
     * @param string $submodule
	 * @return static
	 */
	public static function findByKey($key, string $module, string $submodule = null)
	{
		return static::findOne(['key' => $key, 'module' => $module, 'submodule' => $submodule]);
	}

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'module_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['key', 'value'], 'string', 'max' => 255],
            [['key'], 'unique', 'targetAttribute' => ['module', 'submodule', 'key']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/main', 'ID'),
            'key' => Yii::t('common/main', 'Key'),
            'module' => Yii::t('common/main', 'Module'),
            'submodule' => Yii::t('common/main', 'Submodule'),
            'value' => Yii::t('common/main', 'Value'),
            'created_at' => Yii::t('common/main', 'Created At'),
            'updated_at' => Yii::t('common/main', 'Updated At'),
        ];
    }
}
