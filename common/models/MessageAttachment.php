<?php

namespace phycom\common\models;

use phycom\common\models\attributes\FileType;
use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "message_attachment".
 *
 * @property integer $id
 * @property integer $message_id
 * @property FileType $mime_type
 * @property string $file_name
 * @property string $file_path
 * @property \DateTime $created_at
 *
 * @property Message $message
 */
class MessageAttachment extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'dynamic-attribute' => [
				'attributes' => [
					'mime_type' => FileType::class
				]
			]
		]);
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message_attachment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message_id', 'mime_type', 'file_path'], 'required'],
            [['message_id'], 'integer'],
            [['file_name', 'file_path'], 'string'],
            [['created_at'], 'safe'],
            [['mime_type'], 'string', 'max' => 255],
            [['message_id'], 'exist', 'skipOnError' => true, 'targetClass' => Message::class, 'targetAttribute' => ['message_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/main', 'ID'),
            'message_id' => Yii::t('common/main', 'Message ID'),
            'mime_type' => Yii::t('common/main', 'Mime Type'),
            'file_name' => Yii::t('common/main', 'File Name'),
            'file_path' => Yii::t('common/main', 'File Path'),
            'created_at' => Yii::t('common/main', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessage()
    {
        return $this->hasOne(Message::class, ['id' => 'message_id']);
    }
}
