<?php

namespace phycom\common\models;

use phycom\common\helpers\Date;

use yii\helpers\Inflector;
use yii\twig\ViewRenderer;
use yii\base\NotSupportedException;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii;

/**
 *
 * @property string $id
 * @property string $type
 * @property string $description
 * @property \DateTime $modified
 * @property string $filename
 * @property string $fullPath
 *
 * @property-write string $language
 */
class MessageTemplate extends Model
{
	const DEFAULT_LANGUAGE = "en";
	const FILE_EXTENSION = 'twig';
	const MODIFIED_DATE_FORMAT = 'Y-m-d';

	const TYPE_EMAIL = "email";
	const TYPE_MESSAGE = "message";
	const TYPE_FILE = "file";
	const TYPE_SMS = "sms";

	public $id;
	public $type;
	public $description;
	public $modified;
	public $basePath;

    /**
     * @var array
     */
	public array $basePathFallback = ['@phycom/templates/'];

    public static function types()
    {
        return [
            static::TYPE_EMAIL   => Yii::t('common/template', 'Email'),
            static::TYPE_MESSAGE => Yii::t('common/template', 'Message'),
            static::TYPE_FILE    => Yii::t('common/template', 'File'),
            static::TYPE_SMS     => Yii::t('common/template', 'Sms')
        ];
    }

    /**
     * Find MessageTemplate model by name and type (optional).
     *
     * @param string $name
     * @param string $type - template type
     *
     *
     * @return null|MessageTemplate
     * @throws \Exception
     */
    public static function getTemplate($name, $type = null)
    {
        if (isset(static::templates()[$name])) {
            $templateData = static::templates()[$name];
            $templateData['id'] = $name;
            $templateData['modified'] = isset($data['modified']) ? Date::create($templateData['modified'], self::MODIFIED_DATE_FORMAT)->setTime(0, 0, 0) : new \DateTime('today');
            if ($type !== null && $templateData['type'] !== $type) {
                return null;
            }
            return new static($templateData);
        }
        return null;
    }

    /**
     * @return array
     * @throws \Exception
     * @var string $type
     */
    public static function findAll($type = null)
    {
        $result = [];
        foreach (static::templates() as $id => $data) {
            if($data == false) continue;
            $data['id'] = $id;
            $data['modified'] = isset($data['modified']) ? Date::create($data['modified'], self::MODIFIED_DATE_FORMAT)->setTime(0, 0, 0) : new \DateTime('today');

            if (!$type || $type === $data['type']) {
                $result[] = new static($data);
            }
        }
        return $result;
    }

    /**
     * This method is used to translate messages from twig templates
     *
     * @param string $category
     * @param string $message
     * @param array $params
     * @return string
     */
    public static function translateMessage($category, $message, $params = [])
    {
        return \Yii::t($category, $message, $params, Yii::$app->language);
    }

	public function init()
	{
		parent::init();

		if (!empty($this->basePath)) {
		    if ('@' === substr($this->basePath, 0, 1)) {
                $this->basePath = Yii::getAlias($this->basePath);
            }
        } else {
            $this->basePath = Yii::getAlias('@templates');
        }
	}



	public function getTypeLabel()
	{
		return Inflector::titleize($this->type);
	}


	public function setLanguage($languageCode)
    {
        $this->language = $languageCode;
    }


	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'description', 'type'], 'required'],
			[['id', 'type'], 'string', 'max' => 255],
			[['description'], 'string', 'max' => 512]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'          => Yii::t('common/template', 'Id'),
			'description' => Yii::t('common/template', 'Description'),
			'type'        => Yii::t('common/template', 'Type'),
			'modified'    => Yii::t('common/template', 'Modified'),
		];
	}

    /**
     * @return null|string
     * @throws yii\base\Exception
     */
	public function getFullPath()
	{
		if (!$this->filename) {
			return null;
		}
		if (file_exists($this->basePath . DIRECTORY_SEPARATOR . $this->filename)) {
			return $this->basePath . DIRECTORY_SEPARATOR . $this->filename;
		}
		foreach ($this->basePathFallback as $alias) {
		    $fullPath = Yii::getAlias($alias . $this->filename);
		    if (file_exists($fullPath)) {
		        return $fullPath;
            }
        }
		throw new yii\base\Exception('Template ' . $this->filename . ' was not found');
	}

	/**
	 * @return null|string
	 */
	public function getFilename()
	{
		return $this->id ? $this->id . '.' . static::FILE_EXTENSION : null;
	}

	/**
	 * @return string
	 */
	public function openFile()
	{
		return file_get_contents($this->fullPath);
	}

	/**
	 * Renders a template.
	 *
	 * Uses yii\base\View class for rendering
	 * @param array $params - template parameters
     * @param string $language
	 *
	 * @return string
	 */
	public function render(array $params = [], $language = null)
	{
	    $currentLang = Yii::$app->language;
	    if ($language && $language !== $currentLang) {
	        Yii::$app->language = $language;
        }
		$result = Yii::$app->getView()->renderFile($this->fullPath, $params);
        Yii::$app->language = $currentLang;
        return $result;
	}

    /**
     * Renders a template "title" block in twig template.
     *
     * @param array $params - template parameters
     * @param string $language
     *
     * @return string
     * @throws NotSupportedException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \Twig_Error_Loader
     * @throws yii\base\InvalidConfigException
     */
	public function renderTitle(array $params = [], $language = null)
	{
        $currentLang = Yii::$app->language;
        if ($language && $language !== $currentLang) {
            Yii::$app->language = $language;
        }

		$view = Yii::$app->getView();
		$viewFile = Yii::getAlias($this->fullPath);
		$ext = pathinfo($viewFile, PATHINFO_EXTENSION);
		if ($ext === self::FILE_EXTENSION && isset($view->renderers[$ext])) {

			if (is_array($view->renderers[$ext]) || is_string($view->renderers[$ext])) {
				$view->renderers[$ext] = Yii::createObject($view->renderers[$ext]);
			}
			/* @var $renderer ViewRenderer */
			$renderer = $view->renderers[$ext];
			$renderer->twig->addGlobal('this', $view);
			$loader = new \Twig_Loader_Filesystem(dirname($viewFile));
			$this->addTwigAliases($loader, Yii::$aliases);
			$renderer->twig->setLoader($loader);
			/**
			 * @var \Twig_Template $template
			 */
			$template = $renderer->twig->loadTemplate(pathinfo($viewFile, PATHINFO_BASENAME));
			if ($template->hasBlock('title', $params)) {
                $result = trim($template->renderBlock('title', $params));
                Yii::$app->language = $currentLang;
                return $result;
            } else {
			    return null;
            }
		} else {
			throw new NotSupportedException( $ext . ' renderer not supported');
		}
	}

    /**
     * Used for overloading the protected method ViewRenderer::addAliases as it's needed in renderTitle method
     *
     * @param \Twig_Loader_Filesystem $loader
     * @param array $aliases
     * @throws \Twig_Error_Loader
     * @throws \Twig\Error\LoaderError
     */
	private function addTwigAliases(\Twig_Loader_Filesystem $loader, array $aliases)
	{
		foreach ($aliases as $alias => $path) {
			if (is_array($path)) {
				$this->addTwigAliases($loader, $path);
			} elseif (is_string($path) && is_dir($path)) {
				$loader->addPath($path, substr($alias, 1));
			}
		}
	}

    /**
     * Generates dummy parameters
     *
     * @return array
     * @throws \Exception
     */
	public function generateDummyParameters()
	{
		$content = file_get_contents($this->fullPath);
		$content = str_replace('{{', '', $content);
		$content = str_replace('}}', '', $content);
		preg_match_all('/{(\w+)}/', $content, $matches);

		$params = [];
		if (!empty($matches) && isset($matches[1])) {
			foreach ($matches[1] as $match) {
				$params[$match] = '{' . $match . '}';
			}
		}

        $additionalParams = [
            'appname'      => '{appname}',
            'link'         => '{link}',
            'payment_time' => new \DateTime(),
            'createdAt'    => new \DateTime(),
            'dueDate'      => new \DateTime()
        ];

		switch ($this->id) {
            case 'invoice':
                $additionalParams['invoice'] = Yii::$app->modelFactory->getInvoice()::find()->orderBy(['created_at' => SORT_DESC])->one();
                break;

        }

		return ArrayHelper::merge($params, $additionalParams);
	}


	/**
	 * @return array
	 *
	 * [
	 *      filename => [
	 *          ...
	 *          'modified' - A date when the template was last updated formatted like static::MODIFIED_DATE_FORMAT
	 *      ],
	 *      ...
	 * ]
	 *
	 */
	protected static function templates()
	{
		return [
			### E-MAIL MESSAGES ###
			'verify_email'       => ['type' => static::TYPE_EMAIL, 'description' => Yii::t('common/template/description', 'User account registration')],
			'change_password'    => ['type' => static::TYPE_EMAIL, 'description' => Yii::t('common/template/description', 'Verification email for password renewal')],
			'user_invitation'    => ['type' => static::TYPE_EMAIL, 'description' => Yii::t('common/template/description', 'User account invitation email')],

			'order_confirmed'    => ['type' => static::TYPE_EMAIL, 'description' => Yii::t('common/template/description', 'Order status changed to confirmed')],
			'order_processing'   => ['type' => static::TYPE_EMAIL, 'description' => Yii::t('common/template/description', 'Order status changed to processing')],
			'order_complete'     => ['type' => static::TYPE_EMAIL, 'description' => Yii::t('common/template/description', 'Order status changed to processing complete')],

            ### ADMIN E-MAIL NOTIFICATIONS ###
            'new_order'          => ['type' => static::TYPE_EMAIL, 'description' => Yii::t('common/template/description', 'Admin notification for new order received')],

            ### ADMIN SMS NOTIFICATIONS ###
            'sms/new_order'      => ['type' => static::TYPE_SMS, 'description' => Yii::t('common/template/description', 'Admin notification for new order received')],

            ### FILE TEMPLATES ###
            'invoice'            => ['type' => static::TYPE_FILE, 'description' => Yii::t('common/template/description', 'Order invoice')]
		];
	}
}
