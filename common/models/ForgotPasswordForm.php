<?php
namespace phycom\common\models;

use phycom\common\models\traits\ModelTrait;
use phycom\common\models\attributes\MessageStatus;
use phycom\common\models\attributes\MessageType;
use phycom\common\models\attributes\UserTokenType;

use yii\base\Model;
use yii;

/**
 * Forgot password form
 */
class ForgotPasswordForm extends Model
{
	use ModelTrait;

	public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
	        ['email', 'exist', 'skipOnError' => true, 'targetClass' => Email::class, 'targetAttribute' => 'email'],
        ];
    }

    /**
     * Sends the password reset link to user's email address
     * @return bool whether the reset link is sent successfully
     * @throws \Exception
     */
    public function sendResetLink()
    {
        if ($this->validate()) {

	        if (!$user = $this->getUser()) {
	            return false;
	        }

        	$transaction = Yii::$app->db->beginTransaction();
        	try {

		        $token = new UserToken();
		        $token->type = UserTokenType::PASSWORD_RESET;
		        $token->user_id = $user->id;
		        $token->generate()->populateRelation('user', $user);

		        if (!$token->save()) {
			        return $this->rollback($transaction, $token->errors);
		        }

		        $tpl = Yii::$app->modelFactory->getMessageTemplate()::getTemplate('change_password');
		        $msg = new Message();
		        $msg->type = MessageType::EMAIL;
		        $msg->status = MessageStatus::NEW;
		        $msg->user_from = Yii::$app->systemUser->id;
		        $msg->user_to = $token->user_id;

                $params = [
                    'appname'   => Yii::$app->name,
                    'firstname' => $token->user->first_name,
                    'link'      => Yii::$app->urlManager->createAbsoluteUrl(['/site/reset-password', 't' => $token->token])
                ];
		        $msg->subject = $tpl->renderTitle($params);
		        $msg->content = $tpl->render($params);

		        if (!$msg->save()) {
			        return $this->rollback($transaction, $msg->errors);
		        }
		        $transaction->commit();
		        $msg->queue();
		        return true;

	        } catch (\Exception $e) {
        		$transaction->rollBack();
        		throw $e;
	        }
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[email]]
     * @return User|null
     */
    protected function getUser()
    {
        return  Yii::$app->modelFactory->getUser()::findByEmail($this->email);
    }
}
