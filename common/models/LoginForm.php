<?php
namespace phycom\common\models;

use phycom\common\models\attributes\UserStatus;
use Yii;
use yii\base\Model;

/**
 * Class LoginForm
 * @package phycom\common\models
 *
 * @property string $username - can be username or email
 * @property string $password
 * @property bool $rememberMe
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;
    public $requireAdminUser = false;

    private $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
	    return [
	        'username' => Yii::t('common/user', 'Username'),
		    'password' => Yii::t('common/user', 'Password'),
		    'rememberMe' => Yii::t('common/user', 'Remember me'),
	    ];
    }

	/**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('common/error', 'Incorrect username or password.'));
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
        	$user = $this->getUser();
        	$loggedIn = Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0);
            return $loggedIn;
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
	    if ($this->user === null) {
		    $user = Yii::$app->modelFactory->getUser()::findByEmail($this->username);
            if (!$user) {
                $user = Yii::$app->modelFactory->getUser()::findByUsername($this->username);
            }
            if ($user && (!$this->requireAdminUser || ($this->requireAdminUser && $user->type->isAdmin))) {
                $this->user = $user;
            }
	    }
        return $this->user;
    }
}
