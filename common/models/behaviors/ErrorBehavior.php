<?php

namespace phycom\common\models\behaviors;

use yii\base\Behavior;


class ErrorBehavior extends Behavior
{
    /**
     * Get a last error message for Model
     * @return mixed|null
     */
    public function getLastError()
    {
        $errors = array_values($this->owner->getFirstErrors());
        if (!empty($errors)) {
            return end($errors);
        }
        return null;
    }
}