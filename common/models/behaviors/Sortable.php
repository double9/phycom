<?php

namespace phycom\common\models\behaviors;

use phycom\common\exceptions\UserException;

use yii\base\InvalidCallException;
use yii\base\Behavior;
use yii\db\Transaction;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii;

/**
 * Class Sortable
 * @package phycom\common\models\behaviors
 *
 * @property ActiveRecord $owner
 */
class Sortable extends Behavior
{
    /**
     * @var callable
     */
	public $condition;
	/**
	 * @var string
	 */
	public $orderAttribute = 'order';
	/**
	 * @var string
	 */
	public $parentAttribute = 'parent_id';


	public function events()
	{
		return [
			ActiveRecord::EVENT_BEFORE_INSERT => 'beforeInsert',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete'
		];
	}

    /**
     * Automatically fil the order attribute when record is created
     */
	public function beforeInsert()
	{
		$parentId = $this->parentAttribute ? $this->owner->{$this->parentAttribute} : null;
		$last = $this->getLast($parentId);

		if ($last) {
			$this->owner->{$this->orderAttribute} = ++$last->{$this->orderAttribute};
		} else {
			$this->owner->{$this->orderAttribute} = 1;
		}
	}

    /**
     * After record has been deleted whe shift down all order numbers greater then the deleted number
     * @throws yii\db\Exception
     */
	public function afterDelete()
    {
        $where = ['and', ['>', $this->orderAttribute, $this->owner->{$this->orderAttribute}]];
        if ($this->parentAttribute) {
            $where[] = [$this->parentAttribute => $this->owner->{$this->parentAttribute}];
        }
        $this->shiftDown($where)->execute();
    }

	/**
	 * @param int|null $parentId
	 * @return ActiveRecord|null
	 */
	protected function getLast($parentId = null)
	{
		return $this->createQuery($parentId)
			->orderBy([$this->orderAttribute => SORT_DESC])
			->limit(1)
			->one();
	}

    /**
     * Updates model order from order index value.
     * Example: if we have 2 items and want to move 1st item to be last then index should be 1,
     *
     * @param int $index - 0 based index
     * @throws \Exception
     */
	public function setOrderByIndex(int $index)
    {
        $target = $this->createQuery()
            ->orderBy([$this->orderAttribute => SORT_ASC])
            ->offset($index)
            ->one();

        if (!$target) {
            return;
        }
        $currentOrder = $this->owner->{$this->orderAttribute};
        $targetOrder = $target->{$this->orderAttribute};

        if ($currentOrder === $targetOrder) {
            return;
        } else if ($currentOrder < $targetOrder) {
            $this->insertAfter($target);
        } else {
            $this->insertBefore($target);
        }
    }

    private function createQuery($parentKey = null)
    {
        $query = $this->owner::find();
        if (is_callable($this->condition)) {
            $condition = $this->condition;
            $query->where($condition($this->owner));
        }
        if ($this->parentAttribute) {
            $query->andWhere([$this->parentAttribute => $parentKey]);
        }
        return $query;
    }


	public function insertBefore(ActiveRecord $target)
	{
		$orderA = $this->orderAttribute;
		$parentA = $this->parentAttribute;

		$transaction = Yii::$app->db->beginTransaction();
		$transaction->setIsolationLevel(Transaction::SERIALIZABLE);
		try {

			if (!$parentA || $this->owner->$parentA === $target->$parentA) {

				$params = ['current_key' => $this->owner->$orderA, 'target_key' => $target->$orderA];
				if ($target->$orderA < $this->owner->$orderA) {

				    $where = ['and', '"' . $orderA . '" >= :target_key', '"' . $orderA . '" < :current_key'];
                    if ($parentA) {
                        $where[] = [$parentA => $target->$parentA];
                    }

					$this->shiftUp($where, $params)->execute();

					$this->owner->$orderA = $target->$orderA;
					if (!$this->owner->save()) {
						Yii::error($this->owner->errors);
						throw new UserException('Error updating order');
					}

				} else if ($target->$orderA > $this->owner->$orderA) {

				    $where = ['and', '"' . $orderA . '" < :target_key', '"' . $orderA . '" > :current_key'];
				    if ($parentA) {
                        $where[] = [$parentA => $target->$parentA];
                    }

					$this->shiftDown($where, $params)->execute();

					$this->owner->$orderA = $target->$orderA - 1;

					if (!$this->owner->save()) {
						Yii::error($this->owner->errors);
						throw new UserException('Error updating order');
					}

				} else {
					throw new InvalidCallException('Invalid order. Model '.$this->owner->primaryKey.' has same order as target '.$target->primaryKey);
				}

			} else {
				// shift source order keys down by one
				$this->shiftDown(['and', '"' . $orderA . '" > :key', [$parentA => $this->owner->$parentA]], ['key' => $this->owner->$orderA])->execute();
				// shift destination order keys up by one
				$this->shiftUp(['and', '"' . $orderA . '" >= :key', [$parentA => $target->$parentA]], ['key' => $target->$orderA])->execute();

				$this->owner->$parentA = $target->$parentA;
				$this->owner->$orderA = $target->$orderA;

				if (!$this->owner->save()) {
					Yii::error($this->owner->errors);
					throw new UserException('Error updating order');
				}
			}
			$transaction->commit();

		} catch (\Exception $e) {
			$transaction->rollBack();
			throw $e;
		}
	}

	public function appendTo(ActiveRecord $target)
	{
		$transaction = Yii::$app->db->beginTransaction();
		$transaction->setIsolationLevel(Transaction::SERIALIZABLE);
		try {
			$oAttr = $this->orderAttribute;
			$pAttr = $this->parentAttribute;

			$where = ['and', '"' . $oAttr . '" > :current_key'];
			if ($pAttr) {
			    $where[] = [$pAttr => $this->owner->$pAttr];
            }

			$this->shiftDown($where, ['current_key' => $this->owner->$oAttr])->execute();

			// update the category
            if ($pAttr) {
                $this->owner->$pAttr = $target->primaryKey;
            }
			$last = $this->getLast($target->primaryKey);
			$this->owner->$oAttr = $last ? ++$last->$oAttr : 1;
			$this->owner->save();

			$transaction->commit();
		} catch (\Exception $e) {
			$transaction->rollBack();
			throw $e;
		}
	}

	public function insertAfter(ActiveRecord $target)
	{
		$orderA = $this->orderAttribute;
		$parentA = $this->parentAttribute;

		$transaction = Yii::$app->db->beginTransaction();
		$transaction->setIsolationLevel(Transaction::SERIALIZABLE);
		try {
			if (!$parentA || $this->owner->$parentA === $target->$parentA) {

				$params = ['current_key' => $this->owner->$orderA, 'target_key' => $target->$orderA];

				if ($target->$orderA < $this->owner->$orderA) {

				    $where = ['and', '"' . $orderA . '" > :target_key', '"' . $orderA . '" < :current_key'];
                    if ($parentA) {
                        $where[] = [$parentA => $target->$parentA];
                    }
					$this->shiftUp($where, $params)->execute();

					$this->owner->$orderA = $target->$orderA + 1;
					if (!$this->owner->save()) {
						Yii::error($this->owner->errors);
						throw new UserException('Error updating order');
					}

				} else if ($target->$orderA > $this->owner->$orderA) {

				    $where = ['and', '"' . $orderA . '" <= :target_key', '"' . $orderA . '" > :current_key'];
                    if ($parentA) {
                        $where[] = [$parentA => $target->$parentA];
                    }
					$this->shiftDown($where, $params)->execute();

					$this->owner->$orderA = $target->$orderA;

					if (!$this->owner->save()) {
						Yii::error($this->owner->errors);
						throw new UserException('Error updating order');
					}

				} else {
					throw new InvalidCallException('Invalid order. Model ' . $this->owner->primaryKey . ' has same order as target ' . $target->primaryKey);
				}

			} else {

				// shift source order keys down by one
				$this->shiftDown(['and', '"' . $orderA . '" > :key', [$parentA => $this->owner->$parentA]],['key' => $this->owner->$orderA])->execute();
				// shift destination order keys up by one
				$this->shiftUp(['and', '"' .  $orderA . '" > :key', [$parentA => $target->$parentA]], ['key' => $target->$orderA])->execute();

				$this->owner->$parentA = $target->$parentA;
				$this->owner->$orderA = $target->$orderA + 1;

				if (!$this->owner->save()) {
					Yii::error($this->owner->errors);
					throw new UserException('Error updating order');
				}
			}
            $transaction->commit();

		} catch (\Exception $e) {
			$transaction->rollBack();
			throw $e;
		}
	}

	/**
	 * @param mixed $where
	 * @param array $params
	 * @return yii\db\Command
	 */
	private function shiftDown($where = '', array $params = [])
	{
		return Yii::$app->db->createCommand()->update(
			$this->owner::tableName(),
			[$this->orderAttribute => new Expression('"' . $this->orderAttribute . '" - 1 ')],
            $this->createWhereCondition($where),
			$params
		);
	}

	/**
	 * @param mixed $where
	 * @param array $params
	 * @return yii\db\Command
	 */
	private function shiftUp($where = '', array $params = [])
	{
		return Yii::$app->db->createCommand()->update(
			$this->owner::tableName(),
			[$this->orderAttribute => new Expression('"' . $this->orderAttribute . '" + 1 ')],
			$this->createWhereCondition($where),
			$params
		);
	}

	private function createWhereCondition($where = '')
    {
        if (is_callable($this->condition)) {
            $condition = $this->condition;
            if ($where) {
                return [
                    'and',
                    $condition($this->owner),
                    $where
                ];
            } else {
                return $condition($this->owner);
            }
        }
        return $where;
    }
}