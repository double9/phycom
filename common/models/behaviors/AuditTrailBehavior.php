<?php

namespace phycom\common\models\behaviors;

use phycom\common\helpers\Date;
use phycom\common\models\AuditTrail;

use yii\base\Behavior;
use yii\base\Exception;
use yii\db\ActiveRecord;
use Yii;

/**
 * Class AuditTrailBehavior
 *
 * @package phycom\common\models\behaviors
 */
class AuditTrailBehavior extends Behavior
{
	public $allowed = [];
	public $ignored = [];
	public $ignoredClasses = [];

	public $dateFormat = 'Y-m-d H:i:s';
	public $userAttribute = null;

	public $storeTimestamp = false;
	public $skipNulls = true;

    private $oldattributes = [];

	public function events()
	{
		return [
			ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
			ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
			ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
			ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
		];
	}

    /**
     * @param $event
     * @throws \yii\base\InvalidConfigException
     */
	public function afterDelete($event)
	{
		$this->leaveTrail(AuditTrail::ACTION_DELETE);
	}

    /**
     * @param $event
     */
	public function afterFind($event)
	{
		$this->setOldAttributes($this->owner->getAttributes());
	}

    /**
     * @param $event
     * @throws \yii\base\InvalidConfigException
     */
	public function afterInsert($event)
	{
		$this->audit(true);
	}

    /**
     * @param $event
     * @throws \yii\base\InvalidConfigException
     */
	public function afterUpdate($event)
	{
		$this->audit(false);
	}

    /**
     * @param $insert
     * @throws \yii\base\InvalidConfigException
     */
	public function audit($insert)
	{
		$allowedFields = $this->allowed;
		$ignoredFields = $this->ignored;
		$ignoredClasses = $this->ignoredClasses;

		$newAttributes = $this->owner->getAttributes();
		$oldAttributes = $this->getOldAttributes();

		// Lets check if the whole class should be ignored
		if (sizeof($ignoredClasses) > 0) {
			if (array_search(get_class($this->owner), $ignoredClasses) !== false) return;
		}

		// Lets unset fields which are not allowed
		if (sizeof($allowedFields) > 0) {
			foreach ($newAttributes as $f => $v) {
				if (array_search($f, $allowedFields) === false) unset($newAttributes[$f]);
			}

			foreach ($oldAttributes as $f => $v) {
				if (array_search($f, $allowedFields) === false) unset($oldAttributes[$f]);
			}
		}

		// Lets unset fields which are ignored
		if (sizeof($ignoredFields) > 0) {
			foreach ($newAttributes as $f => $v) {
				if (array_search($f, $ignoredFields) !== false) unset($newAttributes[$f]);
			}

			foreach ($oldAttributes as $f => $v) {
				if (array_search($f, $ignoredFields) !== false) unset($oldAttributes[$f]);
			}
		}

        $this->normalizeAttributes($newAttributes);
        $this->normalizeAttributes($oldAttributes);

		// If no difference then WHY?
		// There is some kind of problem here that means "0" and 1 do not diff for array_diff so beware: stackoverflow.com/questions/12004231/php-array-diff-weirdness :S
        if (count(array_diff_assoc($newAttributes, $oldAttributes)) <= 0) return;


		// Now lets actually write the attributes
		$this->auditAttributes($insert, $newAttributes, $oldAttributes);

		// Reset old attributes to handle the case with the same model instance updated multiple times
		$this->setOldAttributes($this->owner->getAttributes());
	}

    /**
     * @param $insert
     * @param $newAttributes
     * @param array $oldAttributes
     * @throws \yii\base\InvalidConfigException
     */
	public function auditAttributes($insert, $newAttributes, $oldAttributes = array())
	{
		if ($newAttributes !== $oldAttributes) {
			// keep only attributes that changed
			$diff = array_diff($newAttributes, $oldAttributes);
			$old = array_intersect_key($oldAttributes, $diff);

			/**
			 * Replaces database timestamp expressions to actual value
			 * Might be not necessary as for actual use-case old value is of most importance
			 */
			foreach ($diff as $key => $value) {
				if ($value instanceof \yii\db\Expression) {
					if ($value->expression === "(NOW() at time zone 'utc')" || $value->expression === 'NOW()') {
						$diff[$key] = Date::now();
					}
				}
			}
			$this->leaveTrail($insert ? AuditTrail::ACTION_CREATE : AuditTrail::ACTION_CHANGE, $diff, $old);
		}
	}

	/**
	 * Convert input array to JSON string.
	 * If JSON is found as array value. It is decoded first
	 *
	 * @param array $array
	 * @return string - JSON
	 */
	protected function toJson(array $array)
	{
		if (empty($array)) {
			return null;
		}
		foreach ($array as $attribute => $value) {
			if (($data = @json_decode($value)) && is_object($data)) {
				$array[$attribute] = $data;
			}
		}
		return json_encode($array);
	}

    /**
     * @param $action
     * @param array $value
     * @param array $old_value
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
	public function leaveTrail($action, array $value = [], array $old_value = [])
	{
        /**
         * @var AuditTrail|object $trail
         */
		$trail = Yii::createObject(AuditTrail::class);
		$trail->old_value = $this->toJson($old_value);
		$trail->new_value = $this->toJson($value);
		$trail->action = $action;
		$trail->model = $this->owner->className(); // Gets a plain text version of the model name
		$trail->model_id = (string)$this->getNormalizedPk();
		$trail->created_at = $this->storeTimestamp ? time() : date($this->dateFormat); // If we are storing a timestamp lets get one else lets get the date
		$trail->user_id = (string)$this->getUserId(); // Lets get the user id

		return $trail->save();
	}

	public function getOldAttributes()
	{
		return $this->oldattributes;
	}

	public function setOldAttributes($value)
	{
		$this->oldattributes = $value;
	}

    /**
     * @return int|string|null
     */
	public function getUserId()
	{
		if (isset($this->userAttribute)) {
			$data = $this->owner->getAttributes();
			return isset($data[$this->userAttribute]) ? $data[$this->userAttribute] : null;
		} else {
			try {
				$userid = Yii::$app->user->id;
				return empty($userid) ? null : $userid;
			} catch (Exception $e) { //If we have no user object, this must be a command line program
				return null;
			}
		}
	}

    /**
     * @return false|string
     */
	protected function getNormalizedPk()
	{
		$pk = $this->owner->getPrimaryKey();
		return is_array($pk) ? json_encode($pk) : $pk;
	}

    /**
     * @param array $attributes
     */
	protected function normalizeAttributes(array &$attributes)
    {
        foreach ($attributes as $key => $value) {
            if (is_array($value) || is_object($value)) {
                $attributes[$key] = json_encode($value);
            }
        }
    }
}
