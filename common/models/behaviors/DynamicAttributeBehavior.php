<?php

namespace phycom\common\models\behaviors;

use phycom\common\interfaces\DynamicAttributeInterface;
use phycom\common\models\ActiveRecord;
use phycom\common\models\attributes\EnumAttribute;
use yii\base\Behavior;
use yii\base\InvalidArgumentException;

/**
 * This is the Behavior implementation used to load/transform the EnumAttributes
 *
 * Class DynamicAttributeBehavior
 * @package phycom\common\models\behaviors
 *
 * @property ActiveRecord $owner
 */
class DynamicAttributeBehavior extends Behavior
{
	private $_dynamicAttributes = [];

	public function setAttributes(array $attributes = [])
	{
		$this->_dynamicAttributes = $attributes;
	}

	public function attach($owner)
	{
		parent::attach($owner);

		/**
		 * @var EnumAttribute $className
		 */
		foreach ($this->_dynamicAttributes as $attribute => $className) {

			/**
			 * Transform the Enum value to string before the attribute is saved
			 */
			$toString = function ($e) {
				$attribute = $e->data;
				if ($this->owner->$attribute instanceof DynamicAttributeInterface) {
					$this->owner->setAttribute($attribute, (string)$this->owner->$attribute);
				}
			};
			$owner->on(ActiveRecord::EVENT_BEFORE_INSERT, $toString, $attribute);
			$owner->on(ActiveRecord::EVENT_BEFORE_UPDATE, $toString, $attribute);

			/**
			 *  Transform string to correct Enum class implementation whenever the attribute has been saved or populated
			 */
			$toObject = function ($e) use ($className) {
				$attribute = $e->data;
				if (!$this->owner->$attribute instanceof EnumAttribute) {
					$this->owner->$attribute = $className::create($this->owner->$attribute);
				}
			};
			$owner->on(ActiveRecord::EVENT_AFTER_INSERT, $toObject, $attribute);
			$owner->on(ActiveRecord::EVENT_AFTER_UPDATE, $toObject, $attribute);
			$owner->on(ActiveRecord::EVENT_AFTER_FIND, $toObject, $attribute);
		}
	}

	/**
	 * @param string $name
	 * @param string|EnumAttribute $value
	 * @return EnumAttribute
	 * @throws InvalidArgumentException
	 */
	public function createDynamicAttribute($name, $value)
	{
		foreach ($this->_dynamicAttributes as $attribute => $className) {
			if ($name === $attribute) {
				if (!$value instanceof $className) {
					$value = $className::create($value);
				}
				return $value;
			}
		}
		throw new InvalidArgumentException('Dynamic attribute ' . $name . ' is not found');
	}

	/**
	 * @param $name
	 * @return bool
	 */
	public function hasDynamicAttribute($name)
	{
		return isset($this->_dynamicAttributes[$name]) || in_array($name, $this->_dynamicAttributes, true);
	}
}