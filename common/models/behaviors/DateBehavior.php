<?php

namespace phycom\common\models\behaviors;


use phycom\common\helpers\Date;

use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\base\Behavior;


/**
 * Class DateBehavior
 * @package phycom\common\models\behaviors
 *
 * @property ActiveRecord $owner
 */
class DateBehavior extends Behavior
{
    private $_dateAttributes = [];

    public function setAttributes(array $attributes = [])
    {
        $this->_dateAttributes = $attributes;
    }

    public function attach($owner)
    {
	    /**
	     * @var ActiveRecord $owner
	     */
        parent::attach($owner);

        foreach ($this->_dateAttributes as $value) {

            if (is_string($value)) {
                $attribute = $value;
                $timestampEvents = [];
            } else if (is_array($value)) {
                $attribute = array_shift($value);
                $timestampEvents = $value;
            } else {
                throw new Exception('Invalid attribute value');
            }

	        if (!$owner->hasAttribute($attribute)) {
            	continue;
	        }

            /**
             *  Create timestamp at assigned events
             */
            foreach ($timestampEvents as $eventName) {
                $owner->on($eventName, function($e) use ($attribute) {
                    $this->owner->$attribute = $this->timestamp();
                });
            }

            /**
             *  Transform \DateTime to proper db timestamp string before the attribute is saved
             */
	        $toTimestamp = function ($e) {
		        $attribute = $e->data;
		        if ($this->owner->$attribute instanceof \DateTime) {
			        $this->owner->$attribute = $this->toString($this->owner->$attribute);
		        }
	        };
            $owner->on(ActiveRecord::EVENT_BEFORE_INSERT, $toTimestamp, $attribute);
            $owner->on(ActiveRecord::EVENT_BEFORE_UPDATE, $toTimestamp, $attribute);


            /**
             *  Transform timestamp back to to \DateTime whenever the attribute has been saved or populated
             */
	        $toDateTime = function ($e) {
		        $attribute = $e->data;
		        if ($this->owner->$attribute && !$this->owner->$attribute instanceof \DateTime) {
			        $this->owner->$attribute = $this->toDateTime($this->owner->$attribute);
		        }
	        };
            $owner->on(ActiveRecord::EVENT_AFTER_INSERT, $toDateTime, $attribute);
            $owner->on(ActiveRecord::EVENT_AFTER_UPDATE, $toDateTime, $attribute);
            $owner->on(ActiveRecord::EVENT_AFTER_FIND, $toDateTime, $attribute);
        }
    }

	/**
     * Creates a Datetime object from database timestamp string
     * @param $dateStr
     * @return \DateTime|null
     */
    protected function toDateTime($dateStr)
    {
    	return Date::create($dateStr)->dateTime;
    }

    /**
     * Creates a database timestamp from DateTime
     * @param \DateTime $date
     * @return string
     */
    protected function toString(\DateTime $date)
    {
    	return Date::create($date)->getDbDate();
    }

    /**
     * Creates a current database timestamp
     * @return string
     */
    protected function timestamp()
    {
        return Date::create('now')->getDbDate();
    }
}