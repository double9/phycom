<?php

namespace phycom\common\models\behaviors;

use yii\helpers\Inflector;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii;

/**
 * Class StatusBehavior
 * @package phycom\common\models\behaviors
 *
 * @property ActiveRecord $owner
 */
class StatusBehavior extends Behavior
{
    /**
     * @var string - status attribute name used
     */
    private $attribute = 'status';


    public function attach($owner)
    {
        parent::attach($owner);

        if (!$this->owner->hasAttribute($this->attribute)) {
            throw new InvalidConfigException('Model does not have status is attribute');
        }
    }

    public function setAttribute(string $attributeName)
    {
        $this->attribute = $attributeName;
    }

    /**
     * Updates model status
     *
     * @param mixed $status
     * @param bool $save
     *
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     */
    public function updateStatus($status, $save = true)
    {
        $attribute = $this->attribute;
        $this->owner->$attribute = (string) $status;

        if ($save && !$this->owner->update(true, [$attribute])) {
            $reflect = new \ReflectionClass($this->owner);
            $objectName = Inflector::humanize($reflect->getShortName());

			Yii::error('Error updating ' . $objectName . ' ' . $this->owner->getPrimaryKey() . ' ' . $attribute . ': ' . json_encode($this->owner->errors), __METHOD__);
			return false;
		}
        return true;
    }
}