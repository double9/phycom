<?php

namespace phycom\common\models\behaviors;

use phycom\common\models\ActiveRecord;
use phycom\common\models\attributes\JsonAttribute;

use yii\base\Behavior;
use yii\base\InvalidArgumentException;
use Yii;

/**
 *
 * Class DynamicAttributeBehavior
 * @package phycom\common\models\behaviors
 *
 * @property ActiveRecord $owner
 */
class JsonAttributeBehavior extends Behavior
{
	private $jsonAttributes = [];
	private $passOwner = false;

	public function setAttributes(array $attributes = [])
	{
		$this->jsonAttributes = $attributes;
	}

	public function setPassOwner($value)
    {
        $this->passOwner = (bool) $value;
    }

    /**
     * @param \yii\base\Component $owner
     */
	public function attach($owner)
	{
		parent::attach($owner);

		/**
		 * @var JsonAttribute $className
		 */
		foreach ($this->jsonAttributes as $attribute => $className) {

			/**
			 * Transform the JsonAttribute to string before the attribute is saved
			 */
			$toString = function ($e) use ($className) {
				$attribute = $e->data;
				if ($this->owner->$attribute instanceof $className) {
					$this->owner->setAttribute($attribute, $this->owner->$attribute->toArray());
				}
			};
			$owner->on(ActiveRecord::EVENT_BEFORE_INSERT, $toString, $attribute);
			$owner->on(ActiveRecord::EVENT_BEFORE_UPDATE, $toString, $attribute);

			/**
			 *  Transform string to correct JsonAttribute instance whenever the attribute has been saved or populated
			 */
			$toObject = function ($e) use ($className) {
				$attribute = $e->data;
				if (!$this->owner->$attribute instanceof $className) {

				    if ($this->passOwner) {
                        $this->owner->$attribute = Yii::createObject($className, [$this->owner->$attribute, ['owner' => $this->owner]]);
                    } else {
                        $this->owner->$attribute = Yii::createObject($className, [$this->owner->$attribute]);
                    }
				}
			};
			$owner->on(ActiveRecord::EVENT_AFTER_INSERT, $toObject, $attribute);
			$owner->on(ActiveRecord::EVENT_AFTER_UPDATE, $toObject, $attribute);
			$owner->on(ActiveRecord::EVENT_AFTER_FIND, $toObject, $attribute);
		}
	}

    /**
     * @param string $name
     * @param string|JsonAttribute $value
     * @return JsonAttribute
     * @throws InvalidArgumentException
     * @throws \yii\base\InvalidConfigException
     */
	public function createDynamicAttribute($name, $value)
	{
		foreach ($this->jsonAttributes as $attribute => $className) {
			if ($name === $attribute) {
				if (!$value instanceof $className) {
                    if ($this->passOwner) {
                        $value = Yii::createObject($className, [$value, ['owner' => $this->owner]]);
                    } else {
                        $value = Yii::createObject($className, [$value]);
                    }
				}
				return $value;
			}
		}
		throw new InvalidArgumentException('Json attribute ' . $name . ' is not found');
	}

	/**
	 * @param $name
	 * @return bool
	 */
	public function hasJsonAttribute($name)
	{
		return isset($this->jsonAttributes[$name]) || in_array($name, $this->jsonAttributes, true);
	}
}
