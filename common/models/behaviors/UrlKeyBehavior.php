<?php

namespace phycom\common\models\behaviors;

use yii\base\InvalidValueException;
use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\helpers\Inflector;

/**
 * Class UrlKeyBehavior
 * @package phycom\common\models\behaviors
 *
 * @property ActiveRecord $owner
 */
class UrlKeyBehavior extends Behavior
{
	protected $titleAttribute = 'title';
	protected $attribute = 'url_key';

	public function setTitleAttribute($value)
	{
		if (!is_string($value)) {
			throw new InvalidValueException('Url key title attribute name should be a string ' . gettype($value) . ' given');
		}
		$this->titleAttribute = $value;
	}

	public function setAttribute($value)
	{
		if (!is_string($value)) {
			throw new InvalidValueException('Url key attribute name should be a string ' . gettype($value) . ' given');
		}
		$this->attribute = $value;
	}

	public function attach($owner)
	{
		parent::attach($owner);

		$attribute = $this->attribute;
		$checkCreateUrlKey = function ($e) use ($attribute) {
			if (!$this->owner->$attribute) {
				$this->generateUrlKey();
			}
		};
		$owner->on(ActiveRecord::EVENT_BEFORE_INSERT, $checkCreateUrlKey);
		$owner->on(ActiveRecord::EVENT_BEFORE_UPDATE, $checkCreateUrlKey);
	}

	/**
	 * @return string
	 */
	public function generateUrlKey()
	{
		$urlKey = Inflector::slug($this->owner->{$this->titleAttribute});
		$version = 0;
		while ($this->owner::findOne([$this->attribute => $urlKey])) {
			$version++;

			if ($version > 1) {
				$keyParts = explode('-', $urlKey);
				array_pop($keyParts);
				$urlKey = implode('-', $keyParts);
			}

			$urlKey = $urlKey . '-' . $version;
		}
		return $this->owner->{$this->attribute} = $urlKey;
	}
}