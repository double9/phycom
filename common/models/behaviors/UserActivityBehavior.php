<?php

namespace phycom\common\models\behaviors;

use yii;
use phycom\common\models\UserActivity;
use yii\base\Behavior;

class UserActivityBehavior extends Behavior
{
    private $userReference; // model attribute name for User id
    private $actions = [];

    public function setActions($actions=[])
    {
        $this->actions = $actions;
    }

    public function attach($owner)
    {
        parent::attach($owner);
        if (!$this->userReference && isset($owner->user_id)) {
            $this->userReference = 'user_id';
        }
        foreach ($this->actions as $e => $action) {
            $owner->on($e, [$this, 'saveActivity'], $action);
        }
    }

    public function saveActivity($event)
    {
        if (($userId = $this->getUserId()) && $event->data) {
            $action = Yii::t('common\user', $event->data, $this->owner->attributes);
            UserActivity::create($userId, $action);
        }
    }

    public function getUserId()
    {
        return $this->owner->getVariant($this->userReference);
    }

    public function setUserReference($value)
    {
        return $this->userReference = (string) $value;
    }

}
