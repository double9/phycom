<?php
namespace phycom\common\models\traits;

use yii\base\Model;

/**
 * Class TreeBuilder
 * @package phycom\common\models\traits
 */
trait TreeBuilder
{
    /**
     * @param Model[] $models
     * @param null $parent
     * @param bool $setParentUrl - set url parameter on parent node even if the parent has child items
     * @return array
     */
    protected function buildTree(array $models, $parent = null, $setParentUrl = false)
    {
        $tree = [];
        foreach ($models as $model) {
            if ($model->parent_id === $parent) {
                $item = [
                    'label'  => $model->title,
                    'items' => $this->buildTree($models, $model->id),
                ];
                if (empty($item['items']) || $setParentUrl) {
                    $item['url'] = $model->getRoute();
                }
                $tree[] = $item;
            }
        }
        return $tree;
    }
}