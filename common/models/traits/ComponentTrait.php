<?php

namespace phycom\common\models\traits;

use phycom\common\interfaces\PhycomComponentInterface;

use yii\base\InvalidArgumentException;
use yii\web\NotFoundHttpException;

/**
 * Trait ComponentTrait
 *
 * @package phycom\common\models\traits
 */
trait ComponentTrait
{
    /**
     * @param PhycomComponentInterface|PhycomComponentInterface[] $component
     * @return bool
     */
    protected function isComponentEnabled($component)
    {
        if (is_array($component)) {
            foreach ($component as $item) {
                if (!$item instanceof PhycomComponentInterface) {
                    throw new InvalidArgumentException('Invalid Phycom component ' . get_class($item));
                }
                if (!$item->isEnabled()) {
                    return false;
                }
            }
        } else if (!$component instanceof PhycomComponentInterface) {
            throw new InvalidArgumentException('Invalid Phycom component ' . get_class($component));
        } else if (!$component->isEnabled()) {
            return false;
        }
        return true;
    }
}
