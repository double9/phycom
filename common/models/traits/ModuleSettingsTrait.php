<?php

namespace phycom\common\models\traits;


use phycom\common\models\ModuleSettingsForm;

use Yii;

/**
 * Trait ModuleSettingsTrait
 * @package phycom\common\models\traits
 *
 * @property bool $useSettings
 * @property-read ModuleSettingsForm $settings
 */
trait ModuleSettingsTrait
{
    private bool $useSettings = false;
    private $settings;

    public function init()
    {
        if ($this->useSettings) {
            $this->getSettings()->populateModule();
        }
        parent::init();
    }

    public function setUseSettings(bool $value)
    {
        $this->useSettings = $value;
    }

    public function getUseSettings()
    {
        return $this->useSettings;
    }

    /**
     * @return ModuleSettingsForm|object
     * @throws \yii\base\InvalidConfigException
     */
    public function getSettings()
    {
        if ($this->useSettings && !$this->settings) {
            $SettingsForm = $this->getSettingsFormClassName();
            /**
             * @var ModuleSettingsForm $settings
             */
            $settings = Yii::createObject($SettingsForm, [$this]);
            $this->settings = $settings->populate();
        }
        return $this->settings;
    }
}
