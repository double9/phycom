<?php

namespace phycom\common\models\traits;

use yii;

/**
 * Class ApplicationFactoryTrait
 * For bootstrapping correct application code
 *
 * NOTE: properties here used for IDE code auto-completion for both common Web and Console application:
 *
 *
 * @property-read \phycom\common\components\ModelFactory $modelFactory
 * @property-read \phycom\common\components\AuthManager $authManager
 * @property-read \phycom\common\components\Formatter $formatter
 * @property-read \phycom\common\components\User $user
 * @property-read \phycom\common\components\FileStorage $fileStorage
 * @property-read \phycom\common\components\queue\Beanstalk $queue1
 * @property-read \phycom\common\components\queue\Beanstalk $queue2
 * @property-read \phycom\common\models\User $systemUser
 * @property-read \phycom\common\components\LanguageManager $lang
 * @property-read \phycom\common\components\Country $country
 * @property-read \phycom\common\components\PageSpaceCollection $pages
 * @property-read \phycom\common\components\commerce\Commerce $commerce
 * @property-read \phycom\common\components\Blog $blog
 * @property-read \phycom\common\components\Review $reviews
 * @property-read \phycom\common\components\Subscription $subscription
 * @property-read \phycom\common\components\Comment $comments
 * @property-read \phycom\common\components\PartnerContract $partnerContracts
 * @property-read \phycom\common\models\Vendor $vendor
 * @property-read yii\i18n\Locale $locale
 *
 * @property-read \lysenkobv\GeoIP\GeoIP $geoip
 *
 * @property-read \yii\web\UrlManager $urlManagerFrontend The URL manager for this application.
 * @property-read \yii\web\UrlManager $urlManagerBackend The URL manager for this application.
 */
trait ApplicationFactoryTrait
{
    public $shortName;

    public $frameworkName;

    public $frameworkShortName;

    public $vendorName;

    public $vendorId;

    private $namespace = 'phycom';

    private $_vendor;

    private $_systemUser;

    private $_settings = [];

    /**
     * @return \phycom\common\models\User|null
     */
    public function getSystemUser()
    {
        if (!$this->_systemUser) {
            $this->_systemUser = $this->modelFactory->getUser()::findOne(['username' => \yii\helpers\Inflector::slug($this->name) . '_system']);
        }
        return $this->_systemUser;
    }

    public function getVendor()
    {
        if (!$this->_vendor) {
            if ($this->vendorId) {
                $this->_vendor = \phycom\common\models\Vendor::findOne(['id' => $this->vendorId]);
            } elseif ($this->vendorName) {
                $this->_vendor = \phycom\common\models\Vendor::findOne(['name' => $this->vendorName]);
            }
            if (!$this->_vendor) {
                $this->_vendor = \phycom\common\models\Vendor::findOne(['name' => $this->name]);
            }
        }
        return $this->_vendor;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getSetting($key)
    {
        if (!isset($this->_settings[$key])) {
            $setting = \phycom\common\models\Setting::findOne(['key' => $key]);
            $this->_settings[$key] = $setting ? $setting->value : null;
        }
        return $this->_settings[$key];
    }

    public function unsetSettings()
    {
        $this->_settings = [];
    }

    public function createControllerByID($id)
    {
        $pos = strrpos($id, '/');
        if ($pos === false) {
            $prefix = '';
            $className = $id;
        } else {
            $prefix = substr($id, 0, $pos + 1);
            $className = substr($id, $pos + 1);
        }

        if (!preg_match('%^[a-z][a-z0-9\\-_]*$%', $className)) {
            return null;
        }
        if ($prefix !== '' && !preg_match('%^[a-z0-9_/]+$%i', $prefix)) {
            return null;
        }

        $className = str_replace(' ', '', ucwords(str_replace('-', ' ', $className))) . 'Controller';
        $className = ltrim($this->controllerNamespace . '\\' . str_replace('/', '\\', $prefix)  . $className, '\\');
        $originalControllerNamespace = $this->controllerNamespace;

        if (strpos($className, '-') !== false || !class_exists($className)) {
            // check if this is already base namespace
            $namespaceTokens = explode('\\', $className);
            if ($namespaceTokens[0] === $this->namespace || ($namespaceTokens[0] === '' && $namespaceTokens[1] === $this->namespace)) {
                return null;
            }
            // in not then fallback to base namespace
            $className = $this->namespace . '\\' . $className;
            $this->controllerNamespace = $this->namespace . '\\' . $this->controllerNamespace;

            if (strpos($className, '-') !== false || !class_exists($className)) {
                $this->controllerNamespace = $originalControllerNamespace;
                return null;
            }
        }

        if (is_subclass_of($className, 'yii\base\Controller')) {
            return Yii::createObject($className, [$id, $this]);
        } elseif (YII_DEBUG) {
            throw new \yii\base\InvalidConfigException("Controller class must extend from \\yii\\base\\Controller.");
        } else {
            return null;
        }
    }

    public function setBasePath($path)
    {
        parent::setBasePath($path);
        Yii::setAlias('@base-app', Yii::getAlias('@' . $this->namespace) . DIRECTORY_SEPARATOR . basename($path));
    }
}
