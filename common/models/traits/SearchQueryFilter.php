<?php
namespace phycom\common\models\traits;


use phycom\common\components\ActiveQuery;

use yii\base\Exception;

/**
 * Class SearchQueryFilter
 * @package phycom\common\models\traits
 */
trait SearchQueryFilter
{
	/**
	 * @return ActiveQuery
     * @throws Exception
	 */
	public static function find()
	{
		return \Yii::createObject(ActiveQuery::class, [get_called_class()]);
	}
}