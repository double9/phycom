<?php

namespace phycom\common\models\traits;


use phycom\common\models\translation\Translation;
use phycom\common\models\attributes\TranslationStatus;

use yii\base\InvalidArgumentException;
use yii;

/**
 * Trait ModelTranslationTrait
 * @package phycom\common\models\traits
 */
trait ModelTranslationTrait
{
    /**
     * @param string $translationModel - class name of the translation model used
     * @param string $languageCode
     * @param bool $fallback - fallback order: default language, any other language
     * @param string $linkedAttributeName - foreign key name of a translation table that links to the main AR
     * @return Translation
     */
    public function getTranslationModel(string $translationModel, $languageCode = null, $fallback = true, $linkedAttributeName = null)
    {
        if (!class_exists($translationModel)) {
            throw new InvalidArgumentException('Translation model ' . $translationModel . ' was not found');
        }
        $languages = [$languageCode ?? Yii::$app->lang->current->code];
        if ($fallback) {
            $languages[] = Yii::$app->lang->source->code;
        }
        if (!$linkedAttributeName) {
            $linkedAttributeName = static::tableName() . '_id';
        }
        /**
         * @var Translation $translationModel
         */
        $translations = $translationModel::find()
            ->where(['language' => $languages, $linkedAttributeName => $this->id])
            ->andWhere(['not', ['status' => TranslationStatus::DELETED]])
            ->all();

        if (empty($translations) && $fallback) {
            $translations = $translationModel::find()
                ->where([$linkedAttributeName => $this->id])
                ->andWhere(['not', ['status' => TranslationStatus::DELETED]])
                ->all();
        }
        if (empty($translations)) {
            return null;
        }
        foreach ($languages as $language) {
            /**
             * @var Translation[] $translations
             */
            foreach ($translations as $translation) {
                if ($translation->language === $language) {
                    return $translation;
                }
            }
        }
        return $translations[0] ?? null;
    }
}