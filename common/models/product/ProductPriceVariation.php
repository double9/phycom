<?php

namespace phycom\common\models\product;

use phycom\common\models\behaviors\CurrencyBehavior;

use phycom\common\models\CustomFieldOption;
use yii\base\InvalidArgumentException;
use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "product_price_variation".
 *
 * @property integer $product_price_id
 * @property string $variant_name
 * @property string $option_key
 * @property integer $price
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property ProductPrice $productPrice
 * @property string $key
 * @property string $label
 *
 * @property-read Variant $variant
 * @property-read VariantOption $variantOption
 */
class ProductPriceVariation extends \phycom\common\models\ActiveRecord
{
    /**
     * @var Variant
     */
	private $variant;
    /**
     * @var VariantOption
     */
	private $variantOption;

    /**
     * @param string $key
     * @param ProductPrice|null $productPrice
     * @return static
     * @throws yii\base\InvalidConfigException
     */
	public static function create($key, ProductPrice $productPrice = null)
    {
        $parts = explode(CustomFieldOption::COMPOSITE_KEY_DELIMITER, $key);
        $variantName = $parts[0];
        $optionKey = $parts[1] ?? null;

        $variant = Yii::$app->modelFactory->getVariant()::findByName($variantName);
        if (!$variant) {
            throw new InvalidArgumentException('Variant "' . $variantName . '" not found');
        }
        if ($optionKey !== null && !$variant->getOptionByKey($optionKey, null)) {
            throw new InvalidArgumentException('Variant option "' . $optionKey . '" not found');
        }
        /**
         * @var static|object $model
         */
        $model = Yii::createObject(static::class);
        $model->variant_name = $variantName;
        $model->option_key = $optionKey;

        if ($productPrice) {
            $model->product_price_id = $productPrice->id;
            $model->populateRelation('productPrice', $productPrice);
        }
        return $model;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_price_variation';
    }

    public static function primaryKey()
    {
        return ['product_price_id', 'variant_name', 'option_key'];
    }

    /**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'currency' => [
				'class' => CurrencyBehavior::class,
				'attributes' => ['price']
			]
		]);
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_price_id', 'variant_name'], 'required'],
            [['product_price_id'], 'integer'],
            [['price'], 'number'],
            [['price'], 'default', 'value' => null],
            [['created_at', 'updated_at'], 'safe'],
            [['variant_name','option_key'], 'string', 'max' => 255],
            [['product_price_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getProductPrice()), 'targetAttribute' => ['product_price_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_price_id' => Yii::t('common/main', 'Product Price'),
            'variant_name'     => Yii::t('common/main', 'Variant'),
            'option_key'       => Yii::t('common/main', 'Option'),
            'price'            => Yii::t('common/main', 'Price'),
            'created_at'       => Yii::t('common/main', 'Created At'),
            'updated_at'       => Yii::t('common/main', 'Updated At'),
        ];
    }

    /**
     * @return string
     * @throws yii\base\InvalidConfigException
     */
	public function getKey()
	{
	    return $this->option_key ? $this->getVariantOption()->getCompositeKey() : $this->variant_name;
	}

    /**
     * @return string|null
     * @throws yii\base\InvalidConfigException
     */
	public function getLabel()
	{
	    if ($this->getVariantOption()) {
	        return $this->getVariantOption()->label;
        }
	    if ($this->getVariant()) {
	        return $this->getVariant()->label;
        }
		return null;
	}

    /**
     * @return Variant|null
     * @throws yii\base\InvalidConfigException
     */
	public function getVariant()
    {
        if (!$this->variant_name) {
            return null;
        }
        return $this->variant ?: $this->variant = Yii::$app->modelFactory->getVariant()::findByName($this->variant_name);
    }

    /**
     * @return VariantOption|null
     * @throws yii\base\InvalidConfigException
     */
    public function getVariantOption()
    {
        if (!$this->variant_name || !$this->option_key) {
            return null;
        }
        return $this->variantOption ?: $this->variantOption = Yii::$app->modelFactory->getVariantOption()::findByKey($this->variant_name, $this->option_key);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPrice()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getProductPrice()), ['id' => 'product_price_id']);
    }
}
