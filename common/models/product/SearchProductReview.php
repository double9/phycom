<?php

namespace phycom\common\models\product;



use phycom\common\models\SearchReview;

/**
 * Class SearchProductReview
 * @package phycom\common\models\product
 */
class SearchProductReview extends SearchReview
{
    public function init()
    {
        parent::init();
        $this->joinTableName = ProductReview::tableName();
        $this->joinKey = 'product_id';
    }
}