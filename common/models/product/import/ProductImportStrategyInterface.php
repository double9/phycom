<?php

namespace phycom\common\models\product\import;

/**
 * Interface ProductImportStrategyInterface
 *
 * @package phycom\common\models\product\import
 */
interface ProductImportStrategyInterface
{
    /**
     * @return $this
     */
    public function prepare();

    /**
     * @param callable|null $onProgress
     * @return bool
     */
    public function import(callable $onProgress = null);
}
