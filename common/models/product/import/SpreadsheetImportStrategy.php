<?php

namespace phycom\common\models\product\import;


use phycom\common\exceptions\DataImporterException;
use phycom\common\helpers\Currency;
use phycom\common\models\attributes\CategoryStatus;
use phycom\common\models\attributes\ProductStatus;
use phycom\common\models\attributes\TranslationStatus;
use phycom\common\models\attributes\UnitType;
use phycom\common\models\product\Product;
use phycom\common\models\product\ProductCategory;
use phycom\common\models\product\ProductCategoryProductRelation;
use phycom\common\models\product\ProductParam;
use phycom\common\models\translation\ProductTranslation;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Row;

use yii\base\InvalidArgumentException;
use yii\helpers\Json;
use Yii;

/**
 * Class SpreadsheetImportStrategy
 *
 * @package phycom\common\models\product\import
 */
class SpreadsheetImportStrategy extends ImportStrategy implements ProductImportStrategyInterface
{
    /**
     * @var int - zero based index of sheet to use for import
     */
    public int $sheetIndex = 0;
    /**
     * @var int
     */
    public int $firstRow = 2;
    /**
     * @var array - maps spreadsheet columns with product fields. Array where key is field and value is column in spreadsheet
     */
    public array $productFieldMap = [self::FIELD_PRODUCT_SKU  => 'A'];
    /**
     * @var array - maps spreadsheet columns with product params where array key is param name and value is column in spreadsheet
     */
    public array $productParamMap = [];
    /**
     * @var array - maps spreadsheet columns with product tags where array key is tag name and value is column in spreadsheet
     */
    public array $productTagMap = [];
    /**
     * @var string
     */
    public string $category1Col;
    /**
     * @var string
     */
    public string $category2Col;
    /**
     * @var string
     */
    public string $defaultProductStatus = ProductStatus::DRAFT;
    /**
     * @var string
     */
    public string $readerType = 'Xlsx';
    /**
     * @var string
     */
    public string $languageCode;
    /**
     * @var string
     */
    public string $arrayDelimiter = ',';
    /**
     * @var string
     */
    protected string $filename;
    /**
     * @var Spreadsheet
     */
    protected Spreadsheet $spreadsheet;
    /**
     * @var array
     */
    protected array $allCategories = [];

    /**
     * SpreadsheetImportStrategy constructor.
     *
     * @param string $filename
     * @param array $config
     */
    public function __construct(string $filename, $config = [])
    {
        if (!file_exists($filename)) {
            throw new InvalidArgumentException('File ' . $filename . ' was not found');
        }
        $this->filename = $filename;
        parent::__construct($config);
    }

    /**
     * @return $this|ProductImportStrategyInterface
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function prepare() : ProductImportStrategyInterface
    {
        $reader = IOFactory::createReader($this->readerType);
        $this->spreadsheet = $reader->load($this->filename);
        $this->spreadsheet->setActiveSheetIndex($this->sheetIndex);

        if (!isset($this->languageCode)) {
            $this->languageCode = Yii::$app->lang->default->code;
        }

        $this->allCategories = ProductCategory::find()->where(['not', ['status' => CategoryStatus::DELETED]])->all();

        return $this;
    }

    /**
     * @param callable|null $onProgress
     * @return bool
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \Throwable
     */
    public function import(callable $onProgress = null)
    {
        $activeSheet = $this->spreadsheet->getActiveSheet();
        $rowCount = max(0, $activeSheet->getHighestRow() - $this->firstRow) + 1;
        $i = 0;
        if (null !== $onProgress) {
            $onProgress($i, $rowCount, null);
        }

        foreach ($activeSheet->getRowIterator($this->firstRow) AS $row) {

            $product = $this->importProduct($row);
            $i++;
            if (null !== $onProgress) {
                $onProgress($i, $rowCount, $product);
            }
        }
        return true;
    }

    /**
     * @param Row $row
     * @return Product
     * @throws \Throwable
     */
    protected function importProduct(Row $row)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $sku = (string)$this->getCell($row, $this->productFieldMap[self::FIELD_PRODUCT_SKU]);
            $status = $this->productFieldMap[self::FIELD_PRODUCT_STATUS]
                ? $this->getCell($row, $this->productFieldMap[self::FIELD_PRODUCT_STATUS])
                : $this->defaultProductStatus;

            $product = $this->createOrUpdateProduct($sku, $status);

            $this->updateTranslation($product, $row);
            $this->updateCategories($product, $row);
            $this->updateTags($product, $row);
            $this->updateParams($product, $row);
            $this->updatePrice($product, $row);

            $transaction->commit();

            return $product;

        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @param string $sku
     * @param string $status
     * @return Product
     * @throws DataImporterException
     * @throws \yii\base\InvalidConfigException
     */
    protected function createOrUpdateProduct(string $sku, string $status) : Product
    {
        if (empty($sku)) {
            throw new DataImporterException('required param SKU is empty');
        }
        if (!$product = Yii::$app->modelFactory->getProduct()::findOne(['sku' => $sku])) {
            $product = Yii::$app->modelFactory->getProduct(['sku' => $sku]);
        }

        $product->status = Yii::createObject(ProductStatus::class, [$status]);
        $product->vendor_id = Yii::$app->vendor->id;

        if (!$product->save()) {
            Yii::error('Error saving product: ' . Json::encode($product->errors), Yii::$app->commerce->productImporter->logCategory);
            throw new DataImporterException('Error saving product');
        }
        return $product;
    }

    /**
     * @param Product $product
     * @param Row $row
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws DataImporterException
     */
    protected function updateTranslation(Product $product, Row $row)
    {
        if (!$translation = $product->getTranslation($this->languageCode)) {
            $translation = new ProductTranslation();
            $translation->product_id = $product->id;
            $translation->language = $this->languageCode;
            $translation->status = TranslationStatus::PUBLISHED;
        }
        $translation->title = $this->getProductFieldValue($row, self::FIELD_PRODUCT_TITLE);
        $translation->outline = $this->getProductFieldValue($row, self::FIELD_PRODUCT_OUTLINE);
        $translation->description = $this->getProductFieldValue($row, self::FIELD_PRODUCT_DESCRIPTION);

        if (isset($this->productFieldMap[self::FIELD_PRODUCT_URL_KEY]) && $this->productFieldMap[self::FIELD_PRODUCT_URL_KEY]) {
            $translation->url_key = $this->getProductFieldValue($row, self::FIELD_PRODUCT_URL_KEY);
        } else {
            $translation->generateUrlKey();
        }
        if (!$translation->save()) {
            Yii::error('Error saving ' . get_class($translation) . ': ' . Json::encode($translation->errors), Yii::$app->commerce->productImporter->logCategory);
            throw new DataImporterException('Error saving product translation');
        }
    }

    /**
     * @param Product $product
     * @param Row $row
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws DataImporterException
     */
    protected function updateCategories(Product $product, Row $row)
    {
        // categories have already hierarchy so we need to assign just 1 category to product
        if (empty($categoryName = $this->getCellValue($row, $this->category2Col))) {
            if (empty($categoryName = $this->getCellValue($row, $this->category1Col))) {
                $categoryName = null;
            }
        }

        if ($categoryName) {
            $categoryName = trim($categoryName);
            $category = null;

            foreach ($this->allCategories as $productCategory) {
                $translation = $productCategory->getTranslation($this->languageCode);
                if (trim(mb_strtolower($translation->title)) === mb_strtolower($categoryName)) {
                    $category = $productCategory;
                    break;
                }
            }

            if (!$category) {
                throw new DataImporterException('Category ' . $categoryName . ' was not found');
            }

            if (!$categoryRelation = ProductCategoryProductRelation::find()->where(['product_id' => $product->id, 'category_id' => $category->id])) {
                $categoryRelation = new ProductCategoryProductRelation();
                $categoryRelation->product_id = $product->id;
                $categoryRelation->category_id = $category->id;

                if (!$categoryRelation->save()) {
                    Yii::error('Error saving ' . get_class($categoryRelation) . ': ' . Json::encode($categoryRelation->errors), Yii::$app->commerce->productImporter->logCategory);
                    throw new DataImporterException('Error saving product category');
                }
            }
        }
    }

    /**
     * @param Product $product
     * @param Row $row
     * @throws DataImporterException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    protected function updateTags(Product $product, Row $row)
    {
        foreach ($this->productTagMap as $name => $column) {
            $value = $this->getCellValue($row, $column);

            if (empty($value)) {
                Yii::info('No action for tag ' . $name, Yii::$app->commerce->productImporter->logCategory);
                continue;
            }

            $tag = null;
            foreach ($product->tags as $productTag) {
                if ($productTag->value === $name) {
                    $tag = $productTag;
                    break;
                }
            }

            $addTag = (bool) $value;

            if ($addTag && !$tag) {
                Yii::info('Add tag ' . $name, Yii::$app->commerce->productImporter->logCategory);
                $product->addTag($name, Yii::$app->systemUser->id);
            }
            if (!$addTag && $tag) {
                Yii::info('Remove tag ' . $name, Yii::$app->commerce->productImporter->logCategory);
                $tag->delete();
            }
        }
    }

    /**
     * @param Product $product
     * @param Row $row
     * @throws DataImporterException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \yii\base\InvalidConfigException
     */
    protected function updateParams(Product $product, Row $row)
    {
        foreach ($this->productParamMap as $paramName => $column) {
            $value = $this->getCellValue($row, $column);
            $value = trim($value);
            $value = preg_replace('/\xc2\xa0/', '', $value);

            if (empty($value)) {
                Yii::info('No action for param ' . $paramName, Yii::$app->commerce->productImporter->logCategory);
                continue;
            }

            $productParam = null;
            foreach ($product->params as $model) {
                if ($model->name === $paramName) {
                    $productParam = $model;
                    break;
                }
            }

            if (!$productParam) {
                $productParam = new ProductParam();
                $productParam->name = $paramName;
                $productParam->product_id = $product->id;
                $productParam->is_public = true;
            }

            $param = $productParam->getParam();

            if (in_array($param->type, [$param::TYPE_TAGS, $param::TYPE_SELECT])) {
                $valueArray = [];
                foreach(explode($this->arrayDelimiter, $value) as $itemValue) {
                    $itemValue = trim($itemValue);
                    if (is_numeric($itemValue)) {
                        $itemValue = is_float($itemValue) ? (float) $itemValue : (int) $itemValue;
                    }
                    if ($itemValue === 'true' || $itemValue === 'false') {
                        $itemValue = (bool) $itemValue;
                    }
                    $valueArray[] = $itemValue;
                }
                $value = $valueArray;
            }
            $productParam->value = $param->parseValue($value);

            if (!$productParam->save()) {
                Yii::error('Error saving ' . get_class($productParam) . ': ' . Json::encode($productParam->errors), Yii::$app->commerce->productImporter->logCategory);
                throw new DataImporterException('Error saving product param');
            }
        }
    }

    /**
     * @param Product $product
     * @param Row $row
     * @throws DataImporterException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    protected function updatePrice(Product $product, Row $row)
    {
        $price = $this->getProductFieldValue($row, self::FIELD_PRODUCT_PRICE);

        if (empty($price)) {
            return;
        }

        $productPrice = $product->prices[0] ?? null;

        if (!$productPrice) {
            $productPrice = Yii::$app->modelFactory->getProductPrice();
            $productPrice->product_id = $product->id;
            $productPrice->unit_type = UnitType::PIECE;
            $productPrice->num_units = 1;
        }

        $productPrice->price = Currency::toInteger($this->getProductFieldValue($row, self::FIELD_PRODUCT_PRICE));

        if (!$productPrice->save()) {
            Yii::error('Error saving ProductPrice: ' . Json::encode($productPrice->errors), Yii::$app->commerce->productImporter->logCategory);
            throw new DataImporterException('Error saving price');
        }
    }

    /**
     * @param Row $row
     * @param string $field
     * @param string|null $format
     * @return mixed|null
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws DataImporterException
     */
    protected function getProductFieldValue(Row $row, string $field, string $format = null)
    {
        if (array_key_exists($field, $this->productFieldMap) && !empty($this->productFieldMap[$field])) {
            return $this->getCellValue($row, $this->productFieldMap[$field], $format);
        }
        return null;
    }

    /**
     * @param Row $row
     * @param string $column
     * @param string $format
     * @return mixed
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws DataImporterException
     */
    protected function getCellValue(Row $row, string $column, string $format = null)
    {
        if (!$cell = $this->getCell($row, $column)) {
            throw new DataImporterException('Cell not found');
        }
        return trim($cell->getValue());
    }

    /**
     * @param Row $row
     * @param string $column
     * @return \PhpOffice\PhpSpreadsheet\Cell\Cell|null
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    protected function getCell(Row $row, string $column) : ?\PhpOffice\PhpSpreadsheet\Cell\Cell
    {
        return $this->spreadsheet->getActiveSheet()->getCell($column . $row->getRowIndex());
    }
}
