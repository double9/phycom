<?php

namespace phycom\common\models\product\import;


use phycom\common\models\traits\ClassConstantTrait;
use yii\base\BaseObject;

/**
 * Class ImportStrategy
 *
 * @package phycom\common\models\product\import
 */
abstract class ImportStrategy extends BaseObject
{
    use ClassConstantTrait;

    const FIELD_PRODUCT_SKU = 'productSku';
    const FIELD_PRODUCT_STATUS = 'productStatus';
    const FIELD_PRODUCT_TITLE = 'productTitle';
    const FIELD_ITEMS_IN_PACK = 'itemsInPack';
    const FIELD_PRODUCT_WEIGHT = 'productWeight';
    const FIELD_PRODUCT_PRICE = 'productPrice';
    const FIELD_PRODUCT_OUTLINE = 'productOutline';
    const FIELD_PRODUCT_URL_KEY = 'productUrlKey';
    const FIELD_PRODUCT_DESCRIPTION = 'productDescription';

    const FORMAT_DATE = 'date';
    const FORMAT_TEXT = 'text';
    const FORMAT_NUMBER = 'number';
}
