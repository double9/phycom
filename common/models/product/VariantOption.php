<?php

namespace phycom\common\models\product;

use phycom\common\models\CustomFieldOption;
use phycom\common\helpers\Currency;

use yii\helpers\ArrayHelper;
use yii;

/**
 * Class VariantOption
 *
 * @package \phycom\commmon\models\product
 *
 * @property Variant $customField
 */
class VariantOption extends CustomFieldOption
{
    /**
     * @var string
     */
    public $icon;
    /**
     * @var mixed
     */
    public $meta;
    /**
     * @var int
     */
	public $price;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
	    $rules = parent::rules();
	    $rules[] = ['price', 'required'];
		return $rules;
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
        return ArrayHelper::merge(parent::attributeLabels(), [
            'price' => Yii::t('common/product/option', 'Price')
        ]);
	}

    /**
     * @return mixed|string
     */
	public function getCustomFieldClassName()
	{
		return get_class(Yii::$app->modelFactory->getVariant());
	}

    /**
     * @param array $config
     * @return array
     */
	protected function loadConfig(array $config)
    {
        $config = parent::loadConfig($config);
        if (isset($config['price'])) {
            $config['price'] = Currency::toInteger($config['price']);
        }
        return $config;
    }
}
