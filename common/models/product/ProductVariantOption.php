<?php

namespace phycom\common\models\product;

use phycom\common\models\ActiveRecord;

use yii\helpers\Inflector;
use Yii;

/**
 * This is the model class for table "product_variant_option".
 *
 * @property integer $id
 * @property integer $product_variant_id
 * @property string $key
 * @property mixed $default_value
 * @property string $sku
 * @property float $stock
 * @property bool $is_visible
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property-read ProductVariant $productVariant
 * @property-read Product $product
 *
 * @property-read VariantOption $option
 * @property-read string $label
 */
class ProductVariantOption extends ActiveRecord
{
    /**
     * @var VariantOption
     */
    private $option;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_variant_option';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_variant_id', 'key'], 'required'],
            [['product_variant_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['is_visible'], 'boolean'],
            [['stock'], 'number'],
            [['key', 'sku'], 'string', 'max' => 255],
            [['sku', 'default_value'], 'default', 'value' => null],
            [['product_variant_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductVariant::class, 'targetAttribute' => ['product_variant_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                 => Yii::t('common/product/variant', 'ID'),
            'product_variant_id' => Yii::t('common/product/variant', 'Product Variant ID'),
            'key'                => Yii::t('common/product/variant', 'Option'),
            'default_value'      => Yii::t('common/product/variant', 'Default Value'),
            'sku'                => Yii::t('common/product/variant', 'SKU'),
            'stock'              => Yii::t('common/product/variant', 'Stock'),
            'is_visible'         => Yii::t('common/product/variant', 'Visible'),
            'created_at'         => Yii::t('common/product/variant', 'Created At'),
            'updated_at'         => Yii::t('common/product/variant', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if (is_numeric($this->default_value)) {
            if (is_int($this->default_value + 0)) {
                $this->default_value = (int) $this->default_value;
            } else {
                $this->default_value = (float) $this->default_value;
            }
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getLabel()
    {
        return $this->getOption() ? $this->getOption()->label : Inflector::humanize($this->key);
    }

    /**
     * @return VariantOption|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getOption()
    {
        if (!$this->productVariant) {
            return null;
        }
        return $this->option ?: $this->option = Yii::$app->modelFactory->getVariantOption()::findByKey($this->productVariant->name, $this->key);
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function getProductVariant()
    {
        return $this->hasOne(ProductVariant::class, ['id' => 'product_variant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws yii\base\InvalidConfigException
     */
    public function getProduct()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getProduct()), ['id' => 'product_id'])->viaTable(ProductVariant::tableName(), ['id' => 'product_variant_id']);
    }
}
