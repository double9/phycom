<?php

namespace phycom\common\models\product;

use yii;

/**
 * This is the model class for table "product_in_product_category".
 *
 * @property integer $product_id
 * @property integer $category_id
 * @property integer $created_by
 * @property \DateTime $created_at
 *
 */
class ProductCategoryProductRelation extends \phycom\common\models\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_in_product_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'product_id', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::class, 'targetAttribute' => ['category_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getProduct()), 'targetAttribute' => ['product_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    public function init()
    {
	    parent::init();
	    $this->on(static::EVENT_BEFORE_INSERT, function () {
			if (!$this->created_by) {
				$this->created_by = Yii::$app->user->id;
			}
	    });
    }

	/**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProductCategory::class, ['id' => 'category_id']);
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProduct()
	{
		return $this->hasOne(get_class(Yii::$app->modelFactory->getProduct()), ['id' => 'category_id']);
	}

}
