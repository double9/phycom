<?php

namespace phycom\common\models\product;

use phycom\common\models\traits\ClassConstantTrait;
use phycom\common\models\User;

use yii\helpers\Inflector;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the ActiveRecord class for the database table "product_tag".
 * The value of the record is an enum defined by the model constants with the prefix TAG_
 *
 * Class ProductTag
 * @package phycom\common\models\product
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $value
 * @property integer $created_by
 * @property \DateTime $created_at
 *
 * @property User $createdBy
 * @property Product $product
 * @property-read string $label
 * @property-read bool $isVisible
 */
class ProductTag extends \phycom\common\models\ActiveRecord
{
	use ClassConstantTrait;

	const TAG_NEW = 'new';
	const TAG_DISCOUNTED = 'discounted';
	const TAG_FEATURED = 'featured';
	const TAG_BESTSELLER = 'bestseller';
    const TAG_BARGAIN = 'bargain';

	protected $label;
	protected $visible;

	public static function allTags()
	{
		return static::getConstants('TAG_');
	}

    /**
     * @return static[]
     * @throws \yii\base\InvalidConfigException
     */
	public static function visible()
	{
		$models = [];
		foreach (static::tags() as $key => $config) {
			if (isset($config['visible']) && $config['visible'] === true) {
				$config['value'] = $key;
				$models[] = Yii::createObject(static::class, [$config]);
			}
		}
		return $models;
	}

    /**
     * @return static[]
     * @throws \yii\base\InvalidConfigException
     */
	public static function all()
	{
		$models = [];
		$tags = ArrayHelper::merge(
			array_fill_keys(static::allTags(), []),
			static::tags()
		);
		foreach ($tags as $key => $config) {
			$config['value'] = $key;
			$models[] = Yii::createObject(static::class, [$config]);
		}
		return $models;
	}


	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'product_tag';
	}

	/**
	 * Applies the config defined in static::tags()
	 */
	public function initTag()
	{
		$config = isset(static::tags()[$this->value]) ? static::tags()[$this->value] : [];
		foreach ($config as $attribute => $value) {
			$this->$attribute = $value;
		}
		return $this;
	}

	public function rules()
	{
		return [
			[['product_id', 'created_by'], 'integer'],
			[['value'], 'string', 'max' => 255],
			[['created_at'], 'safe'],
			[['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getProduct()), 'targetAttribute' => ['product_id' => 'id']],
			[['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['created_by' => 'id']],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => Yii::t('common/product', 'ID'),
			'product_id' => Yii::t('common/product', 'Product ID'),
			'value' => Yii::t('common/product', 'Value'),
			'label' => Yii::t('common/product', 'Label'),
			'isVisible' => Yii::t('common/product', 'Is Visible'),
			'created_by' => Yii::t('common/product', 'Created By'),
			'created_at' => Yii::t('common/product', 'Created At'),
		];
	}

	public function setValue($value)
	{
		if (in_array(!$value, static::getConstants('TAG_'))) {
			throw new yii\base\InvalidArgumentException('Invalid product tag ' . json_encode($value));
		}
		$this->value = $value;
		$this->initTag();
		return $this;
	}

	public function afterFind()
	{
		parent::afterFind();
		$this->initTag();
	}

	public function getLabel()
	{
		return $this->label ?: Inflector::titleize($this->value);
	}

	public function setLabel(string $value)
	{
		$this->label = $value;
	}

	public function setVisible(bool $value)
	{
		$this->visible = $value;
	}

	public function getIsVisible()
	{
		return $this->visible === true;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCreatedBy()
	{
		return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'created_by']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProduct()
	{
		return $this->hasOne(get_class(Yii::$app->modelFactory->getProduct()), ['id' => 'product_id']);
	}

	/**
	 * Define additional tag params here if needed.
	 * Only the visible tags are shown in admin panel and can be assigned manually.
	 * @return array
	 */
	protected static function tags()
	{
		return [
			self::TAG_NEW => [
				'label' => Yii::t('common/main', 'New'),
				'visible' => true
			],
			self::TAG_DISCOUNTED => [
				'label' => Yii::t('common/main', 'Discounted'),
				'visible' => true
			],
			self::TAG_FEATURED => [
				'label' => Yii::t('common/main', 'Featured'),
				'visible' => true
			],
            self::TAG_BARGAIN => [
                'label' => Yii::t('common/main', 'Best Deal'),
                'visible' => true
            ],
			self::TAG_BESTSELLER => [
				'label' => Yii::t('common/main', 'Best selling'),
				'visible' => false
			]
		];
	}
}
