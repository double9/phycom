<?php

namespace phycom\common\models\product;

use phycom\common\models\ActiveRecord;
use phycom\common\models\attributes\ProductStatus;
use phycom\common\models\attributes\TranslationStatus;
use phycom\common\models\behaviors\Sortable;
use phycom\common\models\Shop;
use phycom\common\models\traits\ModelTranslationTrait;
use phycom\common\models\translation\Translation;
use phycom\common\models\User;
use phycom\common\models\attributes\CategoryStatus;
use phycom\common\models\translation\ProductCategoryTranslation;
use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "product_category".
 *
 * @property integer $id
 * @property integer $shop_id
 * @property integer $parent_id
 * @property CategoryStatus $status
 * @property bool $featured
 * @property integer $order
 * @property integer $created_by
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property ProductCategory $parent
 * @property ProductCategory[] $children
 * @property Shop $shop
 * @property User $createdBy
 * @property ProductCategory[] $relatedCategories
 * @property ProductCategoryTranslation[] $translations
 * @property Product[] $products
 * @property Product[] $activeProducts
 *
 * @method insertBefore(ProductCategory $target)
 * @method appendTo(ProductCategory $target)
 * @method insertAfter(ProductCategory $target)
 * @method setOrderByIndex(int $index)
 */
class ProductCategory extends \phycom\common\models\ActiveRecord
{
    use ModelTranslationTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_category';
    }

    /**
     * @param string urlKey
     * @return static|null
     */
    public static function findByUrlKey(string $urlKey)
    {
        if (!strlen($urlKey)) {
            throw new yii\base\InvalidArgumentException('Url key cannot be empty');
        }
        $query = static::find()
            ->select('c.*')
            ->from(['ct' => ProductCategoryTranslation::tableName()])
            ->where(['ct.url_key' => $urlKey])
            ->innerJoin(['c' => static::tableName()], [
                'and',
                'c.id = ct.product_category_id',
                ['not', ['c.status' => CategoryStatus::DELETED]]
            ]);

        return $query->one();
    }


	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'dynamic-attribute' => [
				'attributes' => [
					'status' => CategoryStatus::class
				]
			],
			'sortable' => ['class' => Sortable::class],
		]);
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shop_id', 'parent_id', 'created_by', 'order'], 'integer'],
            [['status'], 'required'],
            [['featured'], 'boolean', 'trueValue' => true, 'falseValue' => false, 'strict' => true],
            [['created_at', 'updated_at', 'status', 'featured'], 'safe'],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => static::class, 'targetAttribute' => ['parent_id' => 'id']],
            [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getShop()), 'targetAttribute' => ['shop_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('common/main', 'ID'),
            'shop_id'    => Yii::t('common/main', 'Shop ID'),
            'parent_id'  => Yii::t('common/main', 'Parent ID'),
            'status'     => Yii::t('common/main', 'Status'),
            'featured'   => Yii::t('common/main', 'Featured'),
            'order'      => Yii::t('common/main', 'Order'),
            'created_by' => Yii::t('common/main', 'Created By'),
            'created_at' => Yii::t('common/main', 'Created At'),
            'updated_at' => Yii::t('common/main', 'Updated At'),
        ];
    }

    /**
     * @return bool|false|int
     */
    public function delete()
    {
	    $this->status = CategoryStatus::DELETED;
	    return $this->save();
    }

    /**
     * @return bool|false|int
     * @throws \Exception
     */
    public function deleteRecursive()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($this->children as $category) {
                $category->delete();
            }
            $result = $this->delete();
            $transaction->commit();
            return $result;

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }


    /**
     * @param string $languageCode
     * @param bool $fallback
     * @return ProductCategoryTranslation|Translation
     */
    public function getTranslation($languageCode = null, $fallback = true)
    {
        return $this->getTranslationModel(ProductCategoryTranslation::class, $languageCode, $fallback);
    }

    /**
     * @param null $languageCode
     * @return string|null
     */
	public function getTitle($languageCode = null)
    {
        $translation = $this->getTranslation($languageCode);
        return $translation ? $translation->title : null;
    }

    /**
     * @param null $languageCode
     * @return string|null
     */
    public function getDescription($languageCode = null)
    {
        $translation = $this->getTranslation($languageCode);
        return $translation ? $translation->description : null;
    }

    /**
     * @param null $languageCode
     * @return string|null
     */
    public function getUrlKey($languageCode = null)
    {
        $translation = $this->getTranslation($languageCode);
        return $translation ? $translation->url_key : null;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(static::class, ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(ProductCategory::class, ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShop()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getShop()), ['id' => 'shop_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelatedCategories()
    {
        return $this->hasMany(ProductCategory::class, ['id' => 'category_id'])->viaTable('product_category_relation', ['related_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProductCategoryTranslation::class, ['product_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(get_class(Yii::$app->modelFactory->getProduct()), ['id' => 'product_id'])
	        ->viaTable('product_in_product_category', ['category_id' => 'id'])
	        ->andWhere(['not', ['product.status' => ProductStatus::DELETED]]);
    }

    public function getActiveProducts()
    {
    	return $this->getProducts()
		    ->andWhere(['product.status' => ProductStatus::ACTIVE])
		    ->orderBy(['product.created_at' => SORT_DESC]);
    }
}
