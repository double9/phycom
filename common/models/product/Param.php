<?php

namespace phycom\common\models\product;


use phycom\common\models\CustomField;

use Yii;
use yii\base\InvalidValueException;
use yii\helpers\ArrayHelper;

/**
 * Class Param
 *
 * @package phycom\common\models\product
 */
class Param extends CustomField
{
    public $unit;
    /**
     * @return array
     */
    public static function getNames()
    {
        return static::getConstants('P_');
    }

    public function getOptionClassName()
    {
        return get_class(Yii::$app->modelFactory->getParamOption());
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    public function parseValue($value)
    {
        switch ($this->type) {
            case self::TYPE_NUMBER:
                $value = str_replace([',', ' '], ['.',''], $value);
                $floatVal = floatval($value);

                return ($floatVal && intval($floatVal) != $floatVal)
                    ? $floatVal
                    : intval($value);

            case self::TYPE_TEXT:
            case self::TYPE_FILE:
                return (string) $value;
            case self::TYPE_BOOL:
                return (bool) $value;

            case self::TYPE_SELECT:
                if (!is_array($value)) {
                    throw new InvalidValueException('Invalid param ' . $this->name . ', value must be an array');
                }
                $options = ArrayHelper::getColumn($this->options, 'key');
                foreach ($value as $item) {
                    if (!in_array($item, $options)) {
                        throw new InvalidValueException('Invalid param ' . $this->name . ', option was not found');
                    }
                }
                return $value;

            case self::TYPE_TAGS:
                if (!is_array($value)) {
                    throw new InvalidValueException('Invalid param ' . $this->name . ', value must be an array');
                }
                foreach ($value as $item) {
                    if (!is_scalar($item)) {
                        throw new InvalidValueException('Invalid param ' . $this->name . ', value must be an array of scalar');
                    }
                }
                return $value;

            case self::TYPE_OPTION:
                foreach ($this->options as $option) {
                    if ($option->key == $value) {
                        return $option->key;
                    }
                }
                throw new InvalidValueException('Invalid param ' . $this->name . ', option was not found');
            default:
                return $value;
        }
    }
}
