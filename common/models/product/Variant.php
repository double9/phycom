<?php

namespace phycom\common\models\product;

use phycom\common\models\CustomField;
use phycom\common\helpers\Currency;

use yii\helpers\ArrayHelper;
use yii;

/**
 * Class Variant
 *
 * @package phycom\common\models\product
 *
 * @property VariantOption[] $options
 */
class Variant extends CustomField
{
    /**
     * @var number
     */
	public $price;
	/**
	 * @var bool - when true the variant can be configured for each product attachment file
	 */
	public $attachmentOption = false;
	/**
	 * @var bool - works only with the TYPE_OPTION variants. When set true a custom price can be defined for every option
	 */
	public $priceOption = false;

    /**
     * @return static[]
     * @throws yii\base\InvalidConfigException
     */
	public static function findAttachmentVariants()
	{
		$matches = [];
		foreach (static::data() as $name => $value) {
			if (isset($value['attachmentOption'])) {
				$matches[] = static::createInstance($name, $value);
			}
		}
		return $matches;
	}

    /**
     * @param string $name
     * @param array $data
     * @return static|CustomField
     * @throws yii\base\InvalidConfigException
     */
	protected static function createInstance(string $name, array $data)
	{
//		if (isset($data['priceOption']) && (!isset($data['type']) || $data['type'] !== self::TYPE_OPTION)) {
//			unset($data['priceOption']);
//		}
		if (isset($data['price'])) {
			$data['price'] = Currency::toInteger($data['price']);
		}
		return parent::createInstance($name, $data);
	}

	/**
	 * @return array
	 */
	public static function getNames()
	{
		return static::getConstants('V_');
	}

    /**
     * @return \phycom\common\models\CustomFieldOption|string
     */
    public function getOptionClassName()
    {
        return get_class(Yii::$app->modelFactory->getVariantOption());
    }

    /**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name', 'label', 'type'], 'required'],
			['price', 'number', 'min' => 0],
			[['name', 'label', 'type'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
        return ArrayHelper::merge(parent::attributeLabels(), [
            'price' => Yii::t('common/product/variant', 'Price')
        ]);
	}
}
