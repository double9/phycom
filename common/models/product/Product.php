<?php

namespace phycom\common\models\product;

use phycom\common\helpers\c;
use phycom\common\helpers\Url;
use phycom\common\models\{AttachmentUrl,
    attributes\CommentStatus,
    attributes\ReviewStatus,
    attributes\TranslationStatus,
    attributes\UnitType,
    attributes\PriceUnitMode,
    Language,
    Review,
    traits\ModelTranslationTrait,
    translation\ProductTranslation,
    attributes\ProductStatus,
    Comment,
    OrderItem,
    File,
    translation\Translation,
    User,
    Vendor};

use yii\helpers\StringHelper;
use yii\helpers\ArrayHelper;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii;


/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $sku
 * @property integer $vendor_id
 * @property integer $price_id
 * @property boolean $include_vat
 * @property UnitType $price_unit
 * @property PriceUnitMode $price_unit_mode
 * @property bool $discount
 * @property integer $stock
 * @property ProductStatus $status
 * @property integer $image_id
 * @property integer $created_by
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property-read \phycom\common\helpers\ProductPrice $price
 * @property-read array $defaultProductAttributes
 *
 * @property Vendor $vendor
 * @property ProductPrice[] $prices
 * @property ProductVariant[] $productVariants
 * @property ProductStatistics $productStatistics
 * @property OrderItem[] $orderItems
 * @property File $image
 * @property User $createdBy
 * @property ProductAttachment[] $attachments
 * @property File[] $attachmentFiles
 * @property ProductCategory[] $categories
 * @property ProductTranslation[] $translations
 * @property ProductTranslation $translation
 * @property Language[] $languages
 * @property ProductTag[] $tags
 * @property ProductTag[] $visibleTags
 * @property ProductParam[] $params
 * @property ProductParam[] $publicParams
 * @property Review[] $reviews
 * @property Review[] $approvedReviews
 * @property Comment[] $comments
 * @property Comment[] $approvedComments
 * @property-read int $processingTime
 * @property bool $inStock
 */
class Product extends \phycom\common\models\ActiveRecord
{
    use ModelTranslationTrait;

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'dynamic-attribute' => [
				'attributes' => [
					'price_unit' => UnitType::class,
                    'price_unit_mode' => PriceUnitMode::class,
					'status' => ProductStatus::class
				]
			]
		]);
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sku', 'vendor_id', 'status'], 'required'],
            [['vendor_id', 'created_by', 'image_id', 'price_id', 'stock'], 'integer'],
            [['include_vat'], 'boolean'],
            [['created_at', 'updated_at'], 'safe'],
            [['sku'], 'string', 'max' => 255],
	        [['price_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getProductPrice()), 'targetAttribute' => ['price_id' => 'id']],
            [['image_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::class, 'targetAttribute' => ['image_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => Yii::t('common/product', 'ID'),
            'sku'             => Yii::t('common/product', 'SKU'),
            'vendor_id'       => Yii::t('common/product', 'Vendor'),
            'discount'        => Yii::t('common/product', 'Discount'),
            'include_vat'     => Yii::t('common/product', 'Include Vat'),
            'price_unit'      => Yii::t('common/product', 'Unit type'),
            'price_unit_mode' => Yii::t('common/product', 'Mode'),
            'stock'           => Yii::t('common/product', 'Stock'),
            'status'          => Yii::t('common/product', 'Status'),
            'created_by'      => Yii::t('common/product', 'Created By'),
            'created_at'      => Yii::t('common/product', 'Created At'),
            'updated_at'      => Yii::t('common/product', 'Updated At'),
            'image_id'        => Yii::t('common/product', 'Image'),
            'image'           => Yii::t('common/product', 'Image'),
        ];
    }

	public function loadDefaultValues($skipIfSet = true)
	{
		parent::loadDefaultValues($skipIfSet);
		if (!$this->price_unit) {
			$this->price_unit = UnitType::PIECE;
		}
		if (!$this->price_unit_mode) {
		    $this->price_unit_mode = PriceUnitMode::STRICT;
        }
		if (!$this->status) {
			$this->status = ProductStatus::DRAFT;
		}
		if (is_null($this->include_vat)) {
			$this->include_vat = false;
		}
		return $this;
	}

	public function init()
    {
        parent::init();
        /**
         * Create empty ProductStatistics Model whenever new product is added
         */
        $this->on(self::EVENT_AFTER_INSERT, function ($e) {
            $productStatistics = Yii::$app->modelFactory->getProductStatistics();
            $productStatistics->product_id = $this->id;
            $productStatistics->save();
        });
    }

    /**
	 * @return array - assoc. array where key is the attribute name anv value is the default option value
	 */
	public function getDefaultProductAttributes()
	{
		return [];
	}

	public function getProcessingTime()
	{
		return null;
	}

	public function getLabel()
	{
		return $this->translation->title;
	}

	public function getThumbUrl()
	{
		return $this->getImageUrl('thumb');
	}

    /**
     * @param string|null $metaKey - if set then searches attachment by meta key
     * @param string|null $metaValue - also required when meta key is present
     * @return AttachmentUrl|object
     * @throws yii\base\InvalidConfigException
     */
	public function getImageUrl($metaKey = null, $metaValue = null)
	{
		$attachment = null;
		if ($metaKey && $metaValue) {
			$query = File::find()->alias('f')->innerJoin(['a' => ProductAttachment::tableName()], [
					'and',
					'a.file_id = f.id',
					'(a.meta #>> :mkey) :: TEXT = :mvalue',
					['a.product_id' => $this->id]
				], ['mkey' => '{'.$metaKey.'}', 'mvalue' => $metaValue])
                ->orderBy(['a.order' => SORT_ASC]);
			$attachment = $query->one();
		} else if ($this->attachmentFiles) {
			$attachment = $this->attachmentFiles[0];
		}

        return Yii::createObject([
            'class'    => AttachmentUrl::class,
            'bucket'   => $attachment ? $attachment->bucket : null,
            'filename' => $attachment ? $attachment->filename : null,
        ]);
	}

	/**
	 * @return bool
	 */
	public function getInStock() : bool
	{
		return ($this->stock === null || $this->stock > 0);
	}

	/**
	 * @param string $tag
     * @param int $createdBy
	 * @return bool
	 */
	public function addTag(string $tag, int $createdBy = null) : bool
	{
		if (!Yii::$app->modelFactory->getProductTag()::findOne(['product_id' => $this->id, 'value' => $tag])) {

		    if (null === $createdBy && Yii::$app->user) {
		        $createdBy = Yii::$app->user->id;
            }
			return Yii::$app->modelFactory->getProductTag([
                'product_id' => $this->id,
                'value'      => $tag,
                'created_by' => $createdBy
			])->save();
		}
		return false;
	}

	/**
	 * @return ProductTag[]
	 */
	public function getVisibleTags() : array
	{
		return array_filter($this->tags, function ($model) {return $model->isVisible;});
	}

    /**
     * @return ProductParam[]
     */
    public function getPublicParams() : array
    {
        return array_filter($this->params, function ($model) {return $model->is_public;});
    }

    /**
     * @param string $name
     * @return ProductParam|null
     */
    public function getParam(string $name) : ?ProductParam
    {
        foreach ($this->params as $param) {
            if ($param->name === $name) {
                return $param;
            }
        }
        return null;
    }

    /**
     * @param string $groupName
     * @return ProductParam[]
     */
    public function getParamGroup(string $groupName) : array
    {
        if ($paramGroup = Yii::$app->modelFactory->getParam()->getGroupByName($groupName)) {
            $params = [];
            foreach ($this->params as $param) {
                if (in_array($param->name, $paramGroup['items'])) {
                    $params[] = $param;
                }
            }
            return $params;
        }
        return [];
    }

    /**
     * @return Language[]
     * @throws yii\db\Exception
     */
    public function getLanguages() : array
    {
    	$productTranslations = (new Query())
		    ->select('language')
		    ->from(['t' => ProductTranslation::tableName()])
		    ->where(['t.product_id' => $this->id])
		    ->createCommand()
		    ->queryColumn();

    	$languages = array_merge(c::param('languages', []), $productTranslations);
	    return Language::find()->where(['in', 'code' => $languages])->all();
    }

    /**
     * @param string $languageCode
     * @param bool $fallback
     * @return ProductTranslation|Translation
     */
    public function getTranslation($languageCode = null, $fallback = true)
    {
        return $this->getTranslationModel(ProductTranslation::class, $languageCode, $fallback);
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return Url::toFeRoute(['/product/view', 'key' => $this->getTranslation()->url_key]);
    }


    /**
     * @param int $wordCount
     * @return string
     */
    public function getExcerpt($wordCount = null)
    {
        if ($wordCount === null) {
            $wordCount = c::param('excerptWords');
        }
        return StringHelper::truncateWords(strip_tags($this->getTranslation()->description), $wordCount);
    }

	/**
	 * @param string $name
	 * @return ProductVariant
	 */
    public function getProductVariant($name)
    {
    	return ProductVariant::findOne(['name' => $name, 'product_id' => $this->id]);
    }

    /**
     * @param number $units
     * @return \phycom\common\helpers\ProductPrice
     */
    public function getPrice($units = null)
    {
        return Yii::$app->modelFactory->getProductPriceHelper($this, $units);
    }

    /**
     * Determines if the product has custom price unit variants.
     * For example some products might be counted by weight or length etc. and price could be customized by the actual unit used.
     *
     * @return bool
     */
    public function hasCustomPriceUnits()
    {
        return (string) $this->price_unit !== UnitType::PIECE && count($this->prices) > 1;
    }

    /**
     * @inheritdoc
     */
    public function delete()
    {
        $this->status = ProductStatus::DELETED;
        return $this->save(true, ['status']);
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getVendor()
	{
		return $this->hasOne(get_class(Yii::$app->modelFactory->getVendor()), ['id' => 'vendor_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPrices()
	{
		return $this->hasMany(get_class(Yii::$app->modelFactory->getProductPrice()), ['product_id' => 'id'])->orderBy(['num_units' => SORT_ASC]);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProductVariants()
	{
		return $this->hasMany(ProductVariant::class, ['product_id' => 'id']);
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(get_class(Yii::$app->modelFactory->getOrderItem()), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws yii\base\InvalidConfigException
     */
    public function getImage()
    {
        return $this->hasOne(File::class, ['id' => 'file_id'])->viaTable(ProductAttachment::tableName(), ['product_id' => 'id'], function (ActiveQuery $query) {
            $query->onCondition(['order' => 1]);
        });
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductStatistics()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getProductStatistics()), ['product_id' => 'id']);
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTags()
	{
		return $this->hasMany(get_class(Yii::$app->modelFactory->getProductTag()), ['product_id' => 'id']);
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParams()
    {
        return $this->hasMany(ProductParam::class, ['product_id' => 'id']);
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAttachments()
	{
		return $this->hasMany(ProductAttachment::class, ['product_id' => 'id'])->orderBy(['order' => SORT_ASC]);
	}

    /**
     * @return \yii\db\ActiveQuery
     * @throws yii\base\InvalidConfigException
     */
    public function getAttachmentFiles()
    {
        return $this->hasMany(File::class, ['id' => 'file_id'])
            ->alias('f')
            ->viaTable('product_attachment', ['product_id' => 'id'])
            ->innerJoin(['a' => ProductAttachment::tableName()], [
                'and',
                'a.file_id = f.id',
                'a.is_visible = true'
            ]
        )->orderBy(['a.order' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws yii\base\InvalidConfigException
     */
    public function getCategories()
    {
        return $this->hasMany(ProductCategory::class, ['id' => 'category_id'])->viaTable('product_in_product_category', ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProductTranslation::class, ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws yii\base\InvalidConfigException
     */
    public function getReviews()
    {
        return $this->hasMany(Review::class, ['id' => 'review_id'])->viaTable(ProductReview::tableName(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws yii\base\InvalidConfigException
     */
    public function getApprovedReviews()
    {
        return $this->getReviews()->where(['review.status' => ReviewStatus::APPROVED]);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws yii\base\InvalidConfigException
     */
    public function getComments()
    {
        return $this->hasMany(Comment::class, ['id' => 'comment_id'])->viaTable(ProductComment::tableName(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws yii\base\InvalidConfigException
     */
    public function getApprovedComments()
    {
        return $this->getComments()->where(['comment.status' => CommentStatus::APPROVED]);
    }
}
