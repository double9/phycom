<?php

namespace phycom\common\models\product;

use phycom\common\models\behaviors\CurrencyBehavior;
use yii\helpers\ArrayHelper;
use yii;

/**
 * Class ProductStatistics
 * @package phycom\common\models\product
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $num_transactions                  - number of unique transactions (orders) having the product
 * @property integer $total_quantity_ordered            - total quantity of product ordered (order_item.quantity)
 * @property integer $revenue
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property Product $product
 */
class ProductStatistics extends \phycom\common\models\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'product_statistics';
	}

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'currency' => [
                'class' => CurrencyBehavior::class,
                'attributes' => ['revenue']
            ]
        ]);
    }

	public function rules()
	{
		return [
			[['product_id', 'num_transactions', 'total_quantity_ordered'], 'integer'],
            [['revenue'], 'number'],
			[['created_at', 'updated_at'], 'safe'],
			[['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getProduct()), 'targetAttribute' => ['product_id' => 'id']],
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProduct()
	{
		return $this->hasOne(get_class(Yii::$app->modelFactory->getProduct()), ['id' => 'product_id']);
	}
}