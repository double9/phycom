<?php

namespace phycom\common\models\product;


use phycom\common\models\CustomFieldOption;

use Yii;

/**
 * Class ParamOption
 *
 * @package phycom\common\models\product
 */
class ParamOption extends CustomFieldOption
{
    /**
     * @return mixed|string
     */
    public function getCustomFieldClassName()
    {
        return get_class(Yii::$app->modelFactory->getParam());
    }
}
