<?php

namespace phycom\common\models\product;

use phycom\common\models\User;

use yii\helpers\Json;
use yii\helpers\Inflector;
use Yii;

/**
 * This is the model class for table "product_param".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $product_variant_id
 * @property string $name
 * @property mixed $value
 * @property boolean $is_public
 * @property integer $created_by
 * @property integer $updated_by
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property-read string|null $label
 * @property-read Param $param
 *
 * @property Product $product
 * @property User $createdBy
 * @property User $updatedBy
 */
class ProductParam extends \phycom\common\models\ActiveRecord
{
    const EVENT_AFTER_CREATE = 'afterCreate';

    private Param $param;

    /**
     * @param Product $product
     * @return static|object
     * @throws \yii\base\InvalidConfigException
     */
    public static function create(Product $product)
    {
        $model = Yii::createObject(static::class);
        $model->product_id = $product->id;
        $model->populateRelation('product', $product);
        $model->trigger(static::EVENT_AFTER_CREATE);
        return $model;
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_param';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'name', 'value'], 'required'],
            [['product_id', 'product_variant_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at', 'value'], 'safe'],
            [['is_public'], 'boolean'],
            [['name'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getProduct()), 'targetAttribute' => ['product_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                   => Yii::t('common/main', 'ID'),
            'product_id'           => Yii::t('common/main', 'Product'),
            'product_variant_id'   => Yii::t('common/main', 'Variant'),
            'name'                 => Yii::t('common/main', 'Name'),
            'value'                => Yii::t('common/main', 'Value'),
            'is_public'            => Yii::t('common/main', 'Is Public'),
            'created_by'           => Yii::t('common/main', 'Created By'),
            'updated_by'           => Yii::t('common/main', 'Updated By'),
            'created_at'           => Yii::t('common/main', 'Created At'),
            'updated_at'           => Yii::t('common/main', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if (is_numeric($this->value)) {
            if (is_int($this->value + 0)) {
                $this->value = (int) $this->value;
            } else {
                $this->value = (float) $this->value;
            }
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return string|null
     * @throws yii\base\InvalidConfigException
     */
    public function getLabel()
    {
        if ($this->name) {
            if ($param = $this->getParam()) {
                return $param->label;
            }
            return Inflector::titleize($this->name);
        }
        return null;

    }

    /**
     * @return Param|null
     * @throws yii\base\InvalidConfigException
     */
    public function getParam()
    {
        if (!$this->name) {
            return null;
        }
        if (!isset($this->param)) {
            $this->param = Yii::$app->modelFactory->getParam()::findByName($this->name);
        }
        return $this->param;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return is_array($this->value) ? Json::encode($this->value) : (string) $this->value;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getProduct()), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'updated_by']);
    }
}
