<?php

namespace phycom\common\models\product;

use phycom\common\models\ActiveRecord;
use phycom\common\models\attributes\CommentStatus;
use phycom\common\models\Comment;

use yii\data\ActiveDataProvider;
use yii;

/**
 * This is the model class for table "product_comment".
 *
 * @property integer $product_id
 * @property integer $comment_id
 *
 * @property Product $product
 * @property Comment $comment
 */
class ProductComment extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_comment';
    }

    /**
     * @param $productId
     * @param null|string $status
     * @return ActiveDataProvider
     */
    public static function search($productId, $status = null)
    {
        $query = Comment::find()
            ->select('c.*')
            ->from(['p' => self::tableName()])
            ->innerJoin(['c' => Comment::tableName()], 'p.comment_id = c.id')
            ->where(['p.product_id' => $productId]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['status' => SORT_DESC, 'created_at' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 20
            ],
        ]);
        if ($status) {
            $query->andWhere(['c.status' => $status]);
        }
        return $dataProvider;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'comment_id'], 'required'],
            [['product_id', 'comment_id'], 'integer'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getProduct()), 'targetAttribute' => ['product_id' => 'id']],
            [['comment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Comment::class, 'targetAttribute' => ['comment_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => Yii::t('common/main', 'Product ID'),
            'comment_id' => Yii::t('common/main', 'Comment ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getProduct()), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComment()
    {
        return $this->hasOne(Comment::class, ['id' => 'comment_id']);
    }
}
