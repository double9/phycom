<?php

namespace phycom\common\models\product;


use phycom\common\models\attributes\UnitType;
use phycom\common\models\behaviors\CurrencyBehavior;

use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "product_price".
 *
 * @property integer $id
 * @property integer $product_id
 * @property float $num_units
 * @property UnitType $unit_type
 * @property string $sku
 * @property integer $price
 * @property integer $discount_amount
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property Product $product
 * @property ProductPriceVariation[] $priceVariations
 * @property-read string $unitLabel
 *
 * @method initCurrencyAttributes() - see CurrencyBehavior class
 */
class ProductPrice extends \phycom\common\models\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'product_price';
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'dynamic-attribute' => [
				'attributes' => [
					'unit_type' => UnitType::class
				]
			],
			'currency' => [
				'class' => CurrencyBehavior::class,
				'attributes' => ['price', 'discount_amount']
			]
		]);
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'num_units', 'price'], 'required'],
            [['product_id'], 'integer'],
            [['price', 'discount_amount', 'num_units'], 'number'],
            [['created_at', 'updated_at', 'unit_type'], 'safe'],
            [['sku'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getProduct()), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => Yii::t('common/main', 'ID'),
            'product_id'      => Yii::t('common/main', 'Product'),
            'num_units'       => Yii::t('common/main', 'Units'),
            'unit_type'       => Yii::t('common/main', 'Unit Type'),
            'sku'             => Yii::t('common/main', 'SKU'),
            'price'           => Yii::t('common/main', 'Price'),
            'discount_amount' => Yii::t('common/main', 'Discount By'),
            'created_at'      => Yii::t('common/main', 'Created At'),
            'updated_at'      => Yii::t('common/main', 'Updated At'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getUnitLabel()
    {
        if (array_key_exists((string)$this->unit_type, Yii::$app->commerce->unitLabels)) {
            foreach (Yii::$app->commerce->unitLabels[(string)$this->unit_type] as $key => $label) {
                if ($this->num_units == $key) {
                    return $label;
                }
            }
        }
        return '';
    }


	/**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getProduct()), ['id' => 'product_id']);
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPriceVariations()
	{
		return $this->hasMany(get_class(Yii::$app->modelFactory->getProductPriceVariation()), ['product_price_id' => 'id']);
	}
}
