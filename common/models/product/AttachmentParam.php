<?php

namespace phycom\common\models\product;


use phycom\common\models\CustomField;

use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class AttachmentParam
 *
 * @package phycom\common\models\product
 */
class AttachmentParam extends CustomField
{
    /**
     * @var string
     */
    public $hint;
    /**
     * @var string
     */
    public $enabledByVariant;
    /**
     * @return array
     */
    public static function getNames()
    {
        return static::getConstants('P_');
    }

    public function getOptionClassName()
    {
        return get_class(Yii::$app->modelFactory->getAttachmentParamOption());
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'hint'             => Yii::t('common/custom-field', 'Hint'),
            'enabledByVariant' => Yii::t('common/custom-field', 'Enabled by variant'),
        ]);
    }
}
