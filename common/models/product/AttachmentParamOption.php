<?php

namespace phycom\common\models\product;


use phycom\common\models\CustomFieldOption;

use Yii;

/**
 * Class AttachmentParamOption
 *
 * @package phycom\common\models\product
 */
class AttachmentParamOption extends CustomFieldOption
{
    /**
     * @return mixed|string
     */
    public function getCustomFieldClassName()
    {
        return get_class(Yii::$app->modelFactory->getParam());
    }
}
