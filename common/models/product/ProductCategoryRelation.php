<?php

namespace phycom\common\models\product;

use phycom\common\models\attributes\CategoryStatus;
use phycom\common\models\attributes\RelatedCategoryType;
use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "product_category_relation".
 *
 * @property integer $category_id
 * @property integer $related_category_id
 * @property RelatedCategoryType $relation_type
 * @property CategoryStatus $status
 * @property integer $created_by
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property ProductCategory $category
 * @property ProductCategory $relatedCategory
 */
class ProductCategoryRelation extends \phycom\common\models\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'dynamic-attribute' => [
				'attributes' => [
					'status' => CategoryStatus::class,
					'relation_type' => RelatedCategoryType::class
				]
			]
		]);
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_category_relation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'related_category_id', 'relation_type', 'status'], 'required'],
            [['category_id', 'related_category_id', 'created_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['relation_type', 'status'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::class, 'targetAttribute' => ['category_id' => 'id']],
            [['related_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::class, 'targetAttribute' => ['related_category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => Yii::t('common/main', 'Category ID'),
            'related_category_id' => Yii::t('common/main', 'Related Category ID'),
            'relation_type' => Yii::t('common/main', 'Relation Type'),
            'status' => Yii::t('common/main', 'Status'),
            'created_by' => Yii::t('common/main', 'Created By'),
            'created_at' => Yii::t('common/main', 'Created At'),
            'updated_at' => Yii::t('common/main', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProductCategory::class, ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelatedCategory()
    {
        return $this->hasOne(ProductCategory::class, ['id' => 'related_category_id']);
    }
}
