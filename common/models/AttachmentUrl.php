<?php

namespace phycom\common\models;

use phycom\common\components\FileStorage;

use yii\base\BaseObject;
use yii\web\UrlManager;
use Yii;

/**
 * Class AttachmentUrl
 *
 * @package phycom\common\models
 *
 * @property-read AttachmentUrl $public
 * @property-read string|null $url
 * @property-read string|null $default
 * @property-read AttachmentUrl $small
 * @property-read AttachmentUrl $medium
 * @property-read AttachmentUrl $large
 * @property-read AttachmentUrl $full
 */
class AttachmentUrl extends BaseObject
{
    /**
     * @var string
     */
    public $bucket;
    /**
     * @var string
     */
    public $filename;
    /**
     * @var string
     */
    public $size = File::THUMB_SIZE_DEFAULT;
    /**
     * @var string
     */
    public $defaultFilename = '/images/default-image.png';
    /**
     * @var UrlManager
     */
    private $urlManager;
    /**
     * @var FileStorage
     */
    private $fileStorage;


    public function init()
    {
        parent::init();
        $this->urlManager = Yii::$app->urlManager;
        $this->fileStorage = Yii::$app->fileStorage;
    }

    /**
     * @return $this
     * @throws \yii\base\InvalidConfigException
     */
    public function public()
    {
        $this->urlManager = Yii::$app->urlManagerFrontend;
        $this->fileStorage->setBaseUrl($this->getBaseUrl());
        return $this;
    }

    /**
     * @return $this
     * @throws \yii\base\InvalidConfigException
     */
    public function getPublic()
    {
        return $this->public();
    }

    /**
     * @return $this
     */
    public function getSmall()
    {
        $this->size = File::THUMB_SIZE_SMALL;
        return $this;
    }

    /**
     * @return $this
     */
    public function getMedium()
    {
        $this->size = File::THUMB_SIZE_MEDIUM;
        return $this;
    }

    /**
     * @return $this
     */
    public function getLarge()
    {
        $this->size = File::THUMB_SIZE_LARGE;
        return $this;
    }

    /**
     * @return $this
     */
    public function getFull()
    {
        $this->size = null;
        return $this;
    }

    /**
     * @param $size
     * @return string|null
     */
    public function size($size)
    {
        $this->size = $size;
        return $this->getUrl();
    }

    /**
     * @return string|null
     */
    public function getUrl()
    {
        if ($this->bucket && $this->filename) {
            $bucket = $this->fileStorage->getBucket($this->bucket);
            $filename = $this->getFilenameForSize();
            if ($bucket->fileExists($filename)) {
                return $bucket->getFileUrl($filename);
            }
        }
        return null;
    }

    /**
     * @return string
     */
    public function getDefault()
    {
        return $this->urlManager->createAbsoluteUrl([$this->defaultFilename]);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $url = $this->getUrl();
        return $url ?: $this->getDefault();
    }

    /**
     * @return string
     */
    protected function getFilenameForSize()
    {
        $size = $this->size ? '_' . $this->size : '';
        return pathinfo($this->filename, PATHINFO_FILENAME) . $size . '.' . pathinfo($this->filename, PATHINFO_EXTENSION);
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    protected function getBaseUrl()
    {
        $baseUrl = $this->urlManager->getBaseUrl() . '/file/download';
        if (strpos($baseUrl, '://') === false) {
            $hostInfo = $this->urlManager->getHostInfo();
            if (strncmp($baseUrl, '//', 2) === 0) {
                $baseUrl = substr($hostInfo, 0, strpos($hostInfo, '://')) . ':' . $baseUrl;
            } else {
                $baseUrl = $hostInfo . $baseUrl;
            }
        }
        return $baseUrl;
    }
}
