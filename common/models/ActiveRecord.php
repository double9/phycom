<?php

namespace phycom\common\models;


use phycom\common\models\traits\ClassConstantTrait;
use phycom\common\models\behaviors\TimestampBehavior;

/**
 * This is the base Active Record class all AR models should extend.
 * It uses the AuditTrail, Enum and Error Behavior
 *
 * Class ActiveRecord
 * @package phycom\common\models
 *
 * @method hasDynamicAttribute($name)
 * @method createDynamicAttribute($name, $value)
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
	use ClassConstantTrait;

	/**
	 * @return array
	 */
	public function modelEvents()
	{
		return static::getConstants('EVENT_');
	}

	public function behaviors()
	{
		return [
			'audit-trail' => [
				'class' => behaviors\AuditTrailBehavior::class,
				'ignored' => [
					'created_at',
					'updated_at',
				],
				'dateFormat' => 'Y-m-d H:i:s'
			],
			'timestamp' => [
				'class' => TimestampBehavior::class,
				'attributes' => [
					['created_at', ActiveRecord::EVENT_BEFORE_INSERT],
					['updated_at', ActiveRecord::EVENT_BEFORE_INSERT, ActiveRecord::EVENT_BEFORE_UPDATE]
				]
			],
			'dynamic-attribute' => [
				'class' => behaviors\DynamicAttributeBehavior::class
			],
			'error' => [
				'class' => behaviors\ErrorBehavior::class
			]
		];
	}

	/**
	 * Check here if dynamic attribute behavior is used and overload the set method accordingly.
	 * This is so that we can set strings instead of objects as dynamic attribute
	 *
	 * @param string $name
	 * @param mixed $value
	 */
	public function __set($name, $value)
	{
		$hasDABehavior = false;
		foreach ($this->behaviors as $behavior) {
			if ($behavior instanceof behaviors\DynamicAttributeBehavior) {
				$hasDABehavior = true;
				break;
			}
		}
		if ($hasDABehavior && $this->hasDynamicAttribute($name)) {
			$value = $this->createDynamicAttribute($name, $value);
		}
		parent::__set($name, $value);
	}
}