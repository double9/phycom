<?php
namespace phycom\common\widgets;

use phycom\common\helpers\c;

use yii\base\Widget;
use yii\helpers\Html;
use yii;

/**
 *
 * FlashAlert widget renders a message from session flash. All flash messages are displayed
 * in the sequence they were assigned using setFlash. You can set message as following:
 *
 * ```php
 * Yii::$app->session->setFlash('error', 'This is the message');
 * Yii::$app->session->setFlash('success', 'This is the message');
 * Yii::$app->session->setFlash('info', 'This is the message');
 * ```
 *
 * Multiple messages could be set as follows:
 *
 * ```php
 * Yii::$app->session->setFlash('error', ['Error 1', 'Error 2']);
 * ```
 *
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @author Alexander Makarov <sam@rmcreative.ru>
 */
class FlashAlert extends Widget
{
    /**
     * @var array
     */
    public $options = [];
    /**
     * @var array the alert types configuration for the flash messages.
     * This array is setup as $key => $value, where:
     * - $key is the name of the session flash variable
     * - $value is the bootstrap alert type (i.e. danger, success, info, warning)
     */
    public $alertTypes = [
        'error'   => 'alert-danger',
        'danger'  => 'alert-danger',
        'success' => 'alert-success',
        'info'    => 'alert-info',
        'warning' => 'alert-warning'
    ];
    /**
     * @var array the options for rendering the close button tag.
     */
    public $closeButton = [];


    public $alertClassName;


    public function init()
    {
        parent::init();

        $session = Yii::$app->session;
        $flashes = $session->getAllFlashes();

        if (!$this->alertClassName) {
            $this->alertClassName = c::param('alertWidget', yii\bootstrap\Alert::class);
        }

        foreach ($flashes as $type => $data) {
            if (isset($this->alertTypes[$type])) {
                $data = (array) $data;
                foreach ($data as $i => $message) {

                    $options = $this->options;
                    $options['id'] = $this->getId() . '-' . $type . '-' . $i;
                    Html::addCssClass($options, $this->alertTypes[$type]);

                    echo $this->renderAlert($message, $options);
                }
                $session->removeFlash($type);
            }
        }
    }

    /**
     * @param $message
     * @param $options
     * @return string
     * @throws \Exception
     */
    protected function renderAlert($message, array $options = [])
    {
        /**
         * @var yii\bootstrap\Alert $Alert
         */
        $Alert = $this->alertClassName;
        return $Alert::widget([
            'body' => $message,
            'closeButton' => $this->closeButton,
            'options' => $options,
        ]);
    }
}
