<?php

namespace phycom\common\widgets;

use phycom\common\helpers\c;
use phycom\common\models\Address;

use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class Map
 * @package phycom\common\widgets
 */
class Map extends Widget
{
    public string $baseUrl = 'https://www.google.com/maps/embed/v1/place';
    /**
     * @var Address
     */
    public Address $address;
    /**
     * @var array
     */
    public array $options = [];
    /**
     * @var array
     */
    public array $frameOptions = [];
    /**
     * @var float
     */
    public ?float $relativeHeight = 0.9;
    /**
     * @var float
     */
    public ?float $height = null;

    /**
     * @return string|void
     */
    public function run()
    {
        $frameStyles = 'border: 0; position: absolute; top: 0; left:0; width: 100% !important; height: 100% !important;';
        $frameOptions = ArrayHelper::merge([
            'allowfullscreen' => 1,
            'frameborder' => 0,
            'style' => $frameStyles
        ], $this->frameOptions, [
            'src' => $this->baseUrl . '?key=' . c::param('googleApiKey') . '&q=' . $this->getQueryString()
        ]);

        $styles = 'position: relative; overflow: hidden;';
        if ($this->height) {
            $styles .= ' height: ' . $this->height .'px;';
        } else if ($this->relativeHeight) {
            $styles .= ' height: 0; padding-bottom: ' . (100 * $this->relativeHeight) .'%;';
        }
        $options = ArrayHelper::merge([
            'style' => $styles
        ], $this->options);
        Html::addCssClass($options, 'google-maps');

        echo Html::tag('div', Html::tag('iframe', '', $frameOptions), $options);
    }

    /**
     * @return string
     */
    protected function getQueryString()
    {
        return urlencode(implode(', ', $this->address->exportArray()));
    }
}
