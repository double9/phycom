<?php

namespace phycom\common\helpers;

/**
 * Gives ability to using js: prefix
 *
 * Class Json
 * @package phycom\common\helpers
 */
class Json extends \yii\helpers\Json
{
    /**
     * @inheritdoc
     */
    public static function encode($value, $options = 320)
    {
        if (is_array($value)) {
            array_walk_recursive($value, function (&$item) {
                if (is_string($item) && (strncasecmp($item, 'js:', 3) === 0)) {
                    $item = new JsExpression(substr($item, 3));
                }
            });
        }
        return parent::encode($value, $options);
    }
}