<?php

namespace phycom\common\helpers;


use phycom\common\models\attributes\PostType;
use phycom\common\models\Post;

/**
 * Class Page
 * @package phycom\common\helpers
 */
class Page
{
    public static function findByKey($key)
    {
        return Post::findOne(['key' => $key, 'type' => PostType::PAGE]);
    }
}