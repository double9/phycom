<?php

namespace phycom\common\helpers;

use yii\base\BaseObject;

/**
 * Class ChildProcess
 *
 * @package phycom\common\helpers
 */
class ChildProcess extends BaseObject
{
    /**
     * @param string $command
     * @param callable|null $output
     * @param callable|null $errOutput
     * @return int
     */
    public static function execute(string $command, callable $output = null, callable $errOutput = null)
    {
        if (null === $output) {
            $output = function ($line) {fwrite(STDOUT, $line . PHP_EOL);};
        };
        if (null === $errOutput) {
            $errOutput = function ($line) {fwrite(STDERR, $line . PHP_EOL);};
        };
        $proc = proc_open($command, [['pipe','r'],['pipe','w'],['pipe','w']], $pipes);
        while(($line = fgets($pipes[1])) !== false) {
            $output(rtrim($line));
        }
        while(($line = fgets($pipes[2])) !== false) {
            $errOutput(rtrim($line));
        }
        fclose($pipes[0]);
        fclose($pipes[1]);
        fclose($pipes[2]);
        return proc_close($proc);
    }
}
