<?php

namespace phycom\common\helpers;

use libphonenumber\PhoneNumberUtil;

/**
 * Class Phone
 * @package phycom\common\helpers
 */
class PhoneHelper
{
    /**
     * @param $fullNumber
     * @return string|null
     */
    public static function getPhoneCode($fullNumber)
    {
        if (substr($fullNumber, 0, 1) === '+') {
            $fullNumber = substr($fullNumber, 1);
        }
        $phoneUtil = PhoneNumberUtil::getInstance();
        $phoneNumber = '';
        $phoneCode = $phoneUtil->extractCountryCode($fullNumber, $phoneNumber);
        return $phoneCode ? (string) $phoneCode : null;
    }

    /**
     * @param $fullNumber
     * @return string|null
     */
    public static function getNationalPhoneNumber($fullNumber)
    {
        if (substr($fullNumber, 0, 1) === '+') {
            $fullNumber = substr($fullNumber, 1);
        }
        $phoneUtil = PhoneNumberUtil::getInstance();
        $phoneNumber = '';
        $phoneUtil->extractCountryCode($fullNumber, $phoneNumber);
        return strlen($phoneNumber) ? $phoneNumber : null;
    }
}