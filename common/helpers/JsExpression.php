<?php

namespace phycom\common\helpers;

/**
 * Allows heredoc expression
 *
 * Class JsExpression
 * @package phycom\common\helpers
 */
class JsExpression extends \yii\web\JsExpression
{
	public static function create($expression, array $config = [])
	{
		return new static($expression, $config);
	}

	public function __construct($expression, array $config = [])
	{
		$expression = (string) $expression;
		parent::__construct($expression, $config);
	}
}