<?php

namespace phycom\common\helpers;

use dosamigos\datepicker\DateRangePicker;
use dosamigos\datepicker\DatePicker;

use kartik\select2\Select2;
use rmrevin\yii\fontawesome\FAR;
use rmrevin\yii\fontawesome\FAS;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii;

/**
 * Class Filter
 * @package phycom\common\helpers
 */
class Filter
{
    /**
     * Renders a datePicker for a Model
     *
     * @param $model
     * @param string $attributeName - model attribute name for date
     * @param array $options
     * @return string
     * @throws yii\base\InvalidConfigException
     */
    public static function datePicker($model, $attributeName, array $options = [])
    {

        $classPath = explode('\\',basename(get_class($model)));
        $modelName = array_pop($classPath);

        $value = $model->$attributeName;
        if ($value instanceof \DateTime) {
            $value = Yii::$app->formatter->asDate($value);
        }

        return  DatePicker::widget([
            'name' => $modelName . '['.$attributeName.']',
            'value' => $value,
            'options' => $options,
            'clientOptions' => [
//                'calendarWeeks' => true,
                'clearBtn' => true,
//                'todayBtn' => true,
                'todayHighlight' => true,
                'autoclose' => true,
                'format' => Yii::$app->formatter->jsDateFormat
            ]
        ]);
    }

    /**
     * Renders a dateRangePicker for a Model
     *
     * @param object $model - Search model
     * @param string $from - model attribute name for date range starting
     * @param string $to - model attribute name for date range ending
     * @param array $options
     * @return string
     * @throws \Exception
     */
    public static function dateRangePicker($model, $from='from', $to='to', array $options = [])
    {

        $classPath = explode('\\',basename(get_class($model)));
        $modelName = array_pop($classPath);

        return  DateRangePicker::widget([
            'name' => $modelName . '['.$from.']',
            'value' => $model->$from,
            'nameTo' => $modelName . '['.$to.']',
            'valueTo' => $model->$to,
            'clientOptions' => ArrayHelper::merge([
                'clearBtn' => true,
                'todayHighlight' => true,
                'autoclose' => true,
                'format' => Yii::$app->formatter->jsDateFormat
            ], $options)
        ]);
    }

    /**
     * @param $model
     * @param $attribute
     * @param $items
     * @param array $options
     * @return string
     */
    public static function dropDown($model, $attribute, $items, $options = [])
    {
        return Html::activeDropDownList(
            $model,
            $attribute,
            $items,
            ArrayHelper::merge([
                'class'=>'control-field form-control',
                'prompt' => Yii::t('common/main', 'Select')
            ], $options)
        );
    }

    /**
     * @param yii\base\Model $model
     * @param string $attribute
     * @param array $items
     * @param array $options
     * @return string
     * @throws \Exception
     */
    public static function selectDropDown($model, $attribute, $items, $options = [])
    {
        $value = $model->$attribute;
//        $value = is_array($value) ? array_keys($value) : [$value];

        return Select2::widget([
            'data' => $items,
            'model' => $model,
            'attribute' => $attribute,
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => ArrayHelper::merge([
                'value' => $value,
                'placeholder' => Yii::t('common/main', 'Select'),
            ], $options),
            'pluginOptions' => [
                'multiple' => false,
                'allowClear' => true,
                'closeOnSelect' => false
            ]
        ]);
    }

    /**
     * @param $model
     * @param $attribute
     * @param $items
     * @param array $options
     * @return string
     * @throws \Exception
     */
    public static function multiSelectDropDown($model, $attribute, $items, $options = [])
    {
        $value = $model->$attribute;
//        $value = is_array($value) ? array_keys($value) : [$value];

        return Select2::widget([
            'data' => $items,
            'model' => $model,
            'attribute' => $attribute,
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => ArrayHelper::merge([
                'value' => $value,
            ], $options),
            'pluginOptions' => [
                'multiple' => true,
                'closeOnSelect' => false
            ]
        ]);
    }

    /**
     * Extract email domain from provided email address
     *
     * @param $email
     * @return bool|string
     */
    public static function emailDomain($email)
    {
        $ex = explode('@', $email);
        $ex = array_map('trim', $ex);

        if (count($ex) !== 2) {
            return false;
        }

        if (empty($ex[0])) {
            return false;
        }

        return $ex[1];
    }
}
