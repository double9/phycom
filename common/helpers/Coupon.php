<?php

namespace phycom\common\helpers;


use yii\base\BaseObject;
use yii\base\InvalidArgumentException;

/**
 * Class Coupon
 * @package phycom\common\helpers
 *
 * Used for generating random coupon/voucher codes
 *
 * MASK FORMAT [XXX-XXX]
 * 'X' this is random symbols
 * '-' this is separator
 *
 * @property-write string $prefix
 * @property-write string $suffix
 */
class Coupon extends BaseObject
{
    const MIN_LENGTH = 8;

    public $useLetters = true;
    public $useNumbers = false;
    public $useSymbols = false;
    public $useMixedCase = false;
    public $mask = false;

    protected $prefix = '';
    protected $suffix = '';
    protected $uppercase = ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M'];
    protected $lowercase = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm'];
    protected $numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    protected $symbols = ['`', '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '=', '+', '\\', '|', '/', '[', ']', '{', '}', '"', "'", ';', ':', '<', '>', ',', '.', '?'];

    /**
     * @param string $value
     */
    public function setPrefix($value)
    {
        if (!is_string($value)) {
            throw new InvalidArgumentException('Invalid prefix ' . json_encode($value));
        }
        $this->prefix = $this->useMixedCase ? $value : strtoupper($value);
    }

    /**
     * @param string $value
     */
    public function setSuffix($value)
    {
        if (!is_string($value)) {
            throw new InvalidArgumentException('Invalid suffix ' . json_encode($value));
        }
        $this->suffix = $this->useMixedCase ? $value : strtoupper($value);
    }

    /**
     * @param null $length
     * @return string
     */
    public function generateOne($length = null) : string
    {
        return $this->generateCoupon($length);
    }

    /**
     * @param int $maxNumberOfCoupons
     * @param null $length
     * @return array
     */
    public function generate($maxNumberOfCoupons = 1, $length = null) : array
    {
        $coupons = [];
        for ($i = 0; $i < $maxNumberOfCoupons; $i++) {
            $temp = $this->generateCoupon($length);
            $coupons[] = $temp;
        }
        return $coupons;
    }

    /**
     * @param int $length
     * @return string
     */
    protected function generateCoupon($length = null) : string
    {
        if (!$length) {
            $length = self::MIN_LENGTH;
        } else {
            $length = max(self::MIN_LENGTH, $length);
        }

        $characters   = [];
        $coupon = '';

        if ($this->useLetters) {
            if ($this->useMixedCase) {
                $characters = array_merge($characters, $this->lowercase, $this->uppercase);
            } else {
                $characters = array_merge($characters, $this->uppercase);
            }
        }

        if ($this->useNumbers) {
            $characters = array_merge($characters, $this->numbers);
        }

        if ($this->useSymbols) {
            $characters = array_merge($characters, $this->symbols);
        }

        if ($this->mask) {
            for ($i = 0; $i < strlen($this->mask); $i++) {
                if ($this->mask[$i] === 'X') {
                    $coupon .= $characters[mt_rand(0, count($characters) - 1)];
                } else {
                    $coupon .= $this->mask[$i];
                }
            }
        } else {
            for ($i = 0; $i < $length; $i++) {
                $coupon .= $characters[mt_rand(0, count($characters) - 1)];
            }
        }
        return $this->prefix . $coupon . $this->suffix;
    }
}