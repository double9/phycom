<?php

namespace phycom\common\helpers;

/**
 * Class FormatConverter
 * @package phycom\common\helpers
 */
class FormatConverter extends \yii\helpers\FormatConverter
{
	/*
     * Matches each symbol of PHP date format standard
     * with datepicker equivalent codeword
     *
     * @author Tristan Jahier
     */
	public static function formatJS($phpFormat)
	{
		$SYMBOLS_MATCHING = array(
			// Day
			'd' => 'dd',
			'D' => 'D',
			'j' => 'd',
			'l' => 'DD',
			'N' => '',
			'S' => '',
			'w' => '',
			'z' => 'o',
			// Week
			'W' => '',
			// Month
			'F' => 'MM',
			'm' => 'mm',
			'M' => 'M',
			'n' => 'm',
			't' => '',
			// Year
			'L' => '',
			'o' => '',
			'Y' => 'yyyy',
			'y' => 'yy',
			// Time
			'a' => '',
			'A' => '',
			'B' => '',
			'g' => '',
			'G' => '',
			'h' => '',
			'H' => '',
			'i' => '',
			's' => '',
			'u' => ''
		);

		$jsFormat = "";
		$escaping = false;

		for ($i = 0; $i < strlen($phpFormat); $i++) {

			$char = $phpFormat[$i];
			// PHP date format escaping character
			if ($char === '\\') {
				$i++;
				if ($escaping)
					$jsFormat .= $phpFormat[$i];
				else
					$jsFormat .= '\'' . $phpFormat[$i];

				$escaping = true;

			} else {

				if ($escaping) {
					$jsFormat .= "'"; $escaping = false;
				}

				if (isset($SYMBOLS_MATCHING[$char]))
					$jsFormat .= $SYMBOLS_MATCHING[$char];
				else
					$jsFormat .= $char;
			}
		}
		return $jsFormat;
	}
}