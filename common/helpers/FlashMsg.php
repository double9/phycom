<?php

namespace phycom\common\helpers;

use yii;

/**
 * Class FlashMsg
 * @package phycom\common\helpers
 */
class FlashMsg
{
    /**
     * Sets success flash message
     * @param string|array $msg - error message/messages
     * @return void
     */
    public static function success($msg)
    {
        $success = self::firstMessage($msg);
        if (!$success || !strlen($success)) {
            $success = Yii::t('common/main','Success');
        }
        self::appendFlash('success', $success);
    }


    /**
     * Sets error flash message
     * @param string|array $msg - error message/messages
     * @return void
     */
    public static function error($msg)
    {
        $error = self::firstMessage($msg);
        if (!$error || !strlen($error)) {
            $error = Yii::t('common/main','Something went wrong');
        }
        self::appendFlash('error', $error);
    }

    /**
     * Sets a fraud notification message
     * @param string $msg
     */
    public static function fraudNotification($msg)
    {
        self::appendFlash('fraud', $msg);
    }


    /**
     * Sets warning flash message
     * @param string|array $msg - warning message/messages
     * @return void
     */
    public static function warning($msg)
    {
        $warn = self::firstMessage($msg);
        if (!$warn || !strlen($warn)) {
            return;
        }
        self::appendFlash('warning', $warn);
    }


    /**
     * Sets info flash message
     * @param string|array $msg - info message/messages
     * @return void
     */
    public static function info($msg)
    {
        $info = self::firstMessage($msg);
        if (!$info || !strlen($info)) {
            return;
        }
        self::appendFlash('info', $info);
    }

    /**
     * Finds first message from array of messages.
     * @param $arr
     * @return array|mixed
     */
    protected static function firstMessage($arr)
    {
        if (is_array($arr)) {
            $val = array_shift($arr);
            return is_array($val) ? self::firstMessage($val) : $val;
        }
        return $arr;
    }

    protected static function appendFlash($key, $value)
    {
        if (!$prev = Yii::$app->session->getFlash($key)) {
            Yii::$app->session->setFlash($key, $value);
            return;
        }
        $value = is_array($value) ? $value : [$value];
        $prev = is_array($prev) ? $prev : [$prev];
        Yii::$app->session->setFlash($key, array_merge($prev, $value));
    }

}