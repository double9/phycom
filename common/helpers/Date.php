<?php

namespace phycom\common\helpers;

use yii\db\Expression;
use yii;

/**
 * This is a helper class for date related calculations
 *
 * Class Date
 * @package phycom\common\helpers
 *
 * @property \DateTime dateTime
 * @property string dbTimestamp
 * @property string timestamp
 * @property int quarter
 * @property float age
 */
class Date extends yii\base\BaseObject
{
	/**
	 * @var \DateTimeZone|string - default timezone database dates are stored
	 */
	protected $dbTimezone = 'UTC';
	/**
	 * @var \DateTime
	 */
	private $_dateTime;

	/**
	 * @return \DateTime
	 */
	public static function now()
	{
		return new \DateTime('now');
	}
	/**
	 * Creates current timestamp database expression
	 * @return Expression
	 */
	public static function dbNow()
	{
		return new Expression('(NOW() at time zone \'utc\')');
	}

	/**
	 * Finds the number of date changes between two dates at the local timezone.
	 *
	 * For example for:
	 *      $time1 = 2015-05-12 23:59:59
	 *      $time2 = 2015-05-13 00:00:00
	 *
	 * The return value is 1
	 *
	 *
	 * @param \DateTime $time1
	 * @param \DateTime|null $time2
	 * @return int
	 */
	public static function getDaysPassed(\DateTime $time1, \DateTime $time2)
	{
		$date1 = clone $time1;
		$date1->setTimezone(new \DateTimeZone(Yii::$app->timeZone))->setTime(0,0,0);

		$date2 = clone $time2;
		$date2->setTimezone(new \DateTimeZone(Yii::$app->timeZone))->setTime(0,0,0);
		return (int) $date1->diff($date2)->format('%r%a');
	}


	/**
	 * @param mixed|string $dateStr
	 * @param null|string $format
	 * @param null|\DateTimeZone $timezone
	 * @return Date
	 */
	public static function create($dateStr, $format = null, \DateTimeZone $timezone = null)
	{
		if ($dateStr instanceof \DateTime) {
			return new static($dateStr);
		}
		$dateTime = null;
		if (!$timezone) {
			$timezone = new \DateTimeZone(Yii::$app->timeZone);
		}
		if ($format) {
			if (strncmp($format, 'php:', 4) === 0) {
				$format = substr($format, 4);
			}
			if ($date = \DateTime::createFromFormat($format, $dateStr, $timezone)) {
				return new static($date);
			}
			if ($date = \DateTime::createFromFormat($format.'.u', $dateStr, $timezone)) {
			    return new static($date);
            }
            throw new yii\base\InvalidArgumentException('Error creating date from string ' . $dateStr);
		}
		return new static(new \DateTime($dateStr, $timezone));
	}

	/**
	 * Date constructor.
	 * @param \DateTime $dateTime
	 * @param array $config
	 */
	public function __construct(\DateTime $dateTime, array $config = [])
	{
		$this->_dateTime = clone $dateTime;
		parent::__construct($config);
	}

	public function init()
	{
		parent::init();
		$this->dbTimezone = new \DateTimeZone($this->dbTimezone);
	}

	/**
	 * @param string $dateFormat
	 * @return string
	 */
	public function toDateStr($dateFormat = 'Y-m-d')
	{
		return $this->_dateTime->format($dateFormat);
	}

	/**
	 * @return \DateTime
	 */
	public function getDateTime()
	{
		return $this->_dateTime;
	}

	/**
	 * @return string
	 */
	public function getDbTimestamp()
	{
		return $this->_dateTime->setTimezone($this->dbTimezone)->format('Y-m-d H:i:s') . '+00';
	}

    /**
     * @return string
     */
    public function getDbDate()
    {
        return $this->_dateTime->format('Y-m-d');
    }

    /**
	 * @return string
	 */
	public function getTimestamp()
	{
		return $this->_dateTime->format('U');
	}

    /**
     * @return int
     */
    public function getQuarter()
    {
        $monthNo = (int) $this->_dateTime->format('n');
        if ($monthNo <= 3) {
            return 1;
        } else if ($monthNo <= 6) {
            return 2;
        } else if ($monthNo <= 9) {
            return 3;
        } else if ($monthNo <= 12) {
            return 4;
        }
    }

    /**
     * @return float|null
     */
    public function getAge()
    {
        try {
            return $this->_dateTime->diff(new \DateTime())->y;
        } catch (\Exception $e) {
        	Yii::error($e->getMessage(), __METHOD__);
            return null;
        }
    }

	/**
	 * @param string $interval
	 * @return \DateTime
	 */
    public function addInterval($interval)
    {
    	return $this->_dateTime->add(new \DateInterval($interval));
    }
}