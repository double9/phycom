<?php

namespace phycom\common\helpers;

/**
 * Calculate checksum using 7-3-1 algorithm
 * @see http://www.pangaliit.ee/en/settlements-and-standards/example-of-the-use-of-method-7-3-1
 */
class Checksum731
{
    /**
     * Calculate check digit according to 7-3-1 algorithm
     *
     * @param string $nr
     * @return integer
     */
    public static function calculate($nr)
    {
        $nr = (string) $nr;
        $weights = [7, 3, 1];
        $sl = $st = strlen($nr);
        $total = 0;
        while ($sl > 0 and substr($nr, --$sl, 1) >= '0') {
            $total += substr($nr, ($st - 1) - $sl, 1) * $weights[($sl % 3)];
        }
        $checksum = ((ceil(($total / 10)) * 10) - $total);
        return $checksum;
    }

    /**
     * Validate number against check digit
     *
     * @param string $number
     * @param integer $digit
     * @return boolean
     */
    public static function validate($number, $digit)
    {
        $calculated = self::calculate($number);
        if ($digit == $calculated) return true;
        else return false;
    }
}