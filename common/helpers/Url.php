<?php

namespace phycom\common\helpers;

use yii;

/**
 * Class Url
 * @package phycom\common\helpers
 */
class Url extends \yii\helpers\Url
{
    public static function cachedTo($url = '', $scheme = false)
    {
        $path = Yii::getAlias($url);
        $url = parent::to($url, $scheme);

        if (file_exists($path)) {
            $url .= '?' . filemtime($path);
        } else {
            $path = Yii::getAlias('@root') . $path;
            if (file_exists($path)) {
                $url .= '?' . filemtime($path);
            }
        }
        return $url;
    }

    /**
     * @param $url
     * @return string
     */
    public static function feFull($url)
    {
        return Yii::$app->urlManagerFrontend->createAbsoluteUrl($url);
    }

    public static function fe($url = '', $scheme = false)
    {
        static::$urlManager = Yii::$app->urlManagerFrontend;
        $url = static::cachedTo($url, $scheme);
        static::$urlManager = null;
        return $url;
    }

    public static function toFeRoute($route, $scheme = false)
    {
        static::$urlManager = Yii::$app->urlManagerFrontend;
        $url = static::toRoute($route, $scheme);
        static::$urlManager = null;
        return $url;
    }

    public static function be($url = '', $scheme = false)
    {
        static::$urlManager = Yii::$app->urlManagerBackend;
        $url = static::cachedTo($url, $scheme);
        static::$urlManager = null;
        return $url;
    }

    /**
     * @param mixed $url
     * @return string
     */
    public static function beFull($url)
    {
        return Yii::$app->urlManagerFrontend->createAbsoluteUrl($url);
    }

    public static function toBeRoute($route, $scheme = false)
    {
        static::$urlManager = Yii::$app->urlManagerBackend;
        $url = static::toRoute($route, $scheme);
        static::$urlManager = null;
        return $url;
    }
}
