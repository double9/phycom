<?php

namespace phycom\common\helpers;


use yii\base\BaseObject;
use yii\base\InvalidArgumentException;
use yii\helpers\ArrayHelper;

/**
 * Class Gravatar
 * @package phycom\common\helpers
 */
class Gravatar extends BaseObject
{
    const BASE_URL = 'http://gravatar.com';
    const MIN_SIZE = 1;
    const MAX_SIZE = 2048;
    const DEFAULT_AVATAR = 'retro';

    /**
     * @param string $email
     * @param array $params
     * @return string
     */
    public static function createUrl($email, array $params = [])
    {
        $email = (string) $email;
        $hash = md5(strtolower(trim($email)));
        $params = ArrayHelper::merge(['size' => 64, 'default' => static::DEFAULT_AVATAR], $params);

        if (isset($params['size'])) {
            $params['size'] = (int) $params['size'];
            if ($params['size'] < static::MIN_SIZE || $params['size'] > static::MAX_SIZE) {
                throw new InvalidArgumentException('Invalid gravatar size ' . $params['size']);
            }
        }

        return static::BASE_URL . '/avatar/' . $hash . '?' . http_build_query($params);
    }
}