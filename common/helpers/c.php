<?php

namespace phycom\common\helpers;

use yii\base\BaseObject;
use yii;

/**
 * Class c
 * @package phycom\common\helpers
 */
class c extends BaseObject
{

	/**
	 * @param string $key - yii config key to search. can use dot notation
	 * @param mixed|null $default - default value
	 * @return mixed|null
	 */
	public static function param($key, $default = null)
	{
		if (isset(static::dynamicConf()[$key])) {
			return call_user_func(static::dynamicConf()[$key]);
		}
		if (isset(Yii::$app->params[$key])) {
			return Yii::$app->params[$key];
		}
		$keys = explode('.', $key);
		$path = &Yii::$app->params;


		foreach ( $keys as $key ) {
			if (!isset($path[$key])) {
				return $default;
			}
			$path = &$path[$key];
		}
		return $path;
	}

	protected static function dynamicConf()
	{
		return [
			'publicUrl' => function () {
				$urlManager = Yii::$app->urlManagerFrontend;
				return $urlManager->getHostInfo() . $urlManager->getBaseUrl();
			},
			'adminUrl' => function () {
				$urlManager = Yii::$app->urlManagerBackend;
				return $urlManager->getHostInfo() . $urlManager->getBaseUrl();
			}
		];
	}
}