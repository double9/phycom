<?php

namespace phycom\common\helpers;


use phycom\backend\models\product\SearchProductCategory;
use phycom\common\models\product\ProductCategory;

use yii\base\BaseObject;
use yii;

/**
 * Class ProductCategoryLabel
 * @package phycom\common\helpers
 */
class ProductCategoryLabel extends BaseObject
{
    public $delimiter = ' → ';
    /**
     * @var ProductCategory[]
     */
    public $allCategories;

    /**
     * @return ProductCategory[]
     */
    public function fetchAllCategories()
    {
        $model = new SearchProductCategory();
        $model->language = Yii::$app->lang->current;
        $dataProvider = $model->search();
        $dataProvider->pagination = false;

        return $dataProvider->getModels();
    }

    public function generateLabels(array $categories = [])
    {
        $labels = [];

        if (null === $this->allCategories) {
            $this->allCategories = $this->fetchAllCategories();
        }
        if (empty($categories)) {
            $categories = $this->allCategories;
        }
        /**
         * @var ProductCategory[] $categories
         */
        foreach ($categories as $category) {
            $labels[$category->id] = $this->createLabel($category);
        }
        return $labels;
    }

    protected function createLabel(ProductCategory $category)
    {
        $label = $category->getTitle();
        if ($category->parent_id && ($parentLabel = $this->getLabelById($category->parent_id))) {
            $label = $parentLabel . $this->delimiter . $label;
        }
        return $label;
    }

    protected function getLabelById($id)
    {
        foreach ($this->allCategories as $category) {
            if ($category->id === $id) {
                return $this->createLabel($category);
            }
        }
        return null;
    }
}