<?php

namespace phycom\common\helpers;


use phycom\common\exceptions\TransactionFailedException;

use yii\base\BaseObject;
use yii\db\Transaction;
use yii;

/**
 * Class TransactionHelper
 * @package phycom\common\helpers
 */
class TransactionHelper extends BaseObject
{
	public $maxRetryAttempts = 20;
	public $isolationLevel = Transaction::SERIALIZABLE;
	public $autoCommit = true;

	/**
	 * @param callable $callable
	 * @return mixed
	 * @throws \Exception
	 */
	public function run(callable $callable)
	{
		$count = 0;
		$transaction = Yii::$app->db->beginTransaction();
		$transaction->setIsolationLevel($this->isolationLevel);
		try {
			while ($count < $this->maxRetryAttempts) {
				try {
					$res = $callable($transaction);
					if ($this->autoCommit && $transaction->getIsActive()) {
						$transaction->commit();
					}
					return $res;
				} catch (yii\db\Exception $e) {
                    $code = $e->errorInfo[0] ?? null;
					// Catch concurrency issues and retry after random scaled back-off delay.
					if ($code == '40001' || $code === '40P01') {
						// Serialized transaction failure or deadlock.
						Yii::info('transaction failure or deadlock', 'transaction');
						usleep(rand(100, 200) * ($count + 1));
					} elseif ($code === '25P02') {
						// "In failed sql transaction. Rollback and try again"
						$transaction->rollback();
						usleep(rand(100, 200) * ($count + 1));
						Yii::info('transaction in failed', 'transaction');
						$transaction = Yii::$app->db->beginTransaction();
						$transaction->setIsolationLevel($this->isolationLevel);
					} else {
						throw $e;
					}
				}
				$count++;
			}
			throw new TransactionFailedException("Could not commit transaction. Max num operations exceeded " . $count);
		} catch(\Exception $e) {
			$transaction->rollBack();
			throw $e;
		}
	}
}
