<?php

namespace phycom\common\exceptions;

/**
 * Class TransactionFailedException
 * @package phycom\common\exceptions
 */
class TransactionFailedException extends \yii\db\Exception
{

}