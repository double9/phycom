<?php

namespace phycom\common\exceptions;
use yii\base\Exception;

/**
 * Class TranslationMissingException
 * @package phycom\common\exceptions
 */
class TranslationMissingException extends Exception
{
    /**
     * @return string the user-friendly name of this exception
     */
    public function getName()
    {
        return 'Missing translation';
    }
}