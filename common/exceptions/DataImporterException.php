<?php

namespace phycom\common\exceptions;

use yii\base\Exception;

/**
 * Class DataImporterException
 *
 * @package phycom\common\exceptions
 */
class DataImporterException extends Exception
{

}
