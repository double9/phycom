<?php

use phycom\common\components\commerce\ClientCard;
use phycom\common\components\commerce\Param;
use phycom\common\components\commerce\PromoCode;
use phycom\common\components\commerce\Variant;
use phycom\common\components\commerce\Shop;
use phycom\common\components\commerce\ShopScheduleOpen;
use phycom\common\components\commerce\ShopScheduleClosed;
use phycom\common\components\commerce\ShopSupply;
use phycom\common\components\commerce\ShopContent;
use phycom\common\components\commerce\ProductImporter;


return [
    'vendorPath' => ROOT_PATH . '/vendor',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
	'modules' => [
		'email' => [
			'class' => phycom\common\modules\email\Module::class,
			'modules' => [
                phycom\common\modules\email\Module::PROVIDER_MAILER => [
                    'mailer' => [
                        'class' => \yii\swiftmailer\Mailer::class,
                        'useFileTransport' => false,
                        'transport' => [
                            'class'      => Swift_SmtpTransport::class,
                            'encryption' => 'tls',
                            'host'       => getenv(ENV . '_SMTP_HOST'),
                            'port'       => getenv(ENV . '_SMTP_PORT'),
                            'username'   => getenv(ENV . '_SMTP_USER'),
                            'password'   => getenv(ENV . '_SMTP_PASS'),
                        ],
                    ]
                ]
			],
			'defaultProvider' => phycom\common\modules\email\Module::PROVIDER_MAILER,
			'testMode' => false,
		],
        'sms' => [
            'class' => phycom\common\modules\sms\Module::class,
            'modules' => [],
            'testMode' => false,
        ],
		'delivery' => [
			'class' => phycom\common\modules\delivery\Module::class,
			'modules' => []
		],
		'payment' => [
			'class' => phycom\common\modules\payment\Module::class,
			'modules' => [],
			'defaultMethod' => phycom\common\modules\payment\methods\invoice\Module::ID
		],
        'auth' => [
            'class' => phycom\common\modules\auth\Module::class,
            'modules' => [],
        ]
	],
    'components' => [
	    'redis' => [
		    'class' => yii\redis\Connection::class,
		    'hostname' => getenv(ENV . '_REDIS_HOST'),
		    'port' => getenv(ENV . '_REDIS_PORT'),
		    'database' => 0,
	    ],
	    'session' => [
		    'class' => yii\redis\Session::class,
		    'redis' => [
			    'hostname' => getenv(ENV . '_REDIS_HOST'),
			    'port' => getenv(ENV . '_REDIS_PORT'),
			    'database' => 1,
		    ],
	    ],
	    'country' => [
	    	'class' => \phycom\common\components\Country::class,
		    'countries' => [],
		    'preferredCountries' => []
	    ],
        'locale' => \yii\i18n\Locale::class,
        'cache' => [
	        'class' => yii\redis\Cache::class,
	        'keyPrefix' => APP . '_ecommerce',
        ],
	    'modelFactory' => [
	        'class' => phycom\common\components\ModelFactory::class
        ],
	    'db' => [
		    'class' => \yii\db\Connection::class,
		    'dsn' => 'pgsql:host=' . getenv(ENV . '_DB_HOST') . ';port=' . getenv(ENV . '_DB_PORT') . ';dbname=' . getenv(ENV . '_DB_NAME'), // PostgreSQL
		    'username' => getenv(ENV . '_DB_USER'),
		    'password' => getenv(ENV . '_DB_PASS'),
		    'charset' => 'utf8',
		    'enableQueryCache' => true,
		    'enableSchemaCache' => true,
		    'on afterOpen' => function ($e) {
			    $e->sender->createCommand('SET TIMEZONE TO \'' . Yii::$app->timeZone . '\'')->execute();
		    }
	    ],
        'authManager' => [
            'class'           => phycom\common\components\AuthManager::class,
            'defaultRoles'    => ['user', 'guest'],
            'rootRoles'       => ['system'],
            'sessionCache'    => true,
            'itemTable'       => 'auth_item',
            'ruleTable'       => 'auth_rule',
            'itemChildTable'  => 'auth_item_hierarchy',
            'assignmentTable' => 'user_auth_item'
        ],
        'formatter' => [
            'class'          => phycom\common\components\Formatter::class,
            'locale'         => 'et-EE',
            'currencyCode'   => 'EUR',
            'timeFormat'     => 'short',
            'dateFormat'     => 'php:Y-m-d',
            'datetimeFormat' => 'php:Y-m-d H:i:s',
            'jsDateFormat'   => 'yyyy-mm-dd'
        ],
        'commerce' => [
            'class'      => \phycom\common\components\commerce\Commerce::class,
            'enabled'    => true,
            'components' => [
                'clientCards' => [
                    'class'   => ClientCard::class,
                    'enabled' => true
                ],
                'promoCodes'  => [
                    'class'   => PromoCode::class,
                    'enabled' => true
                ],
                'variants'    => [
                    'class'   => Variant::class,
                    'enabled' => true
                ],
                'params'      => [
                    'class'   => Param::class,
                    'enabled' => true
                ],
                'shop' => [
                    'class'   => Shop::class,
                    'enabled' => true
                ],
                'shopOpen'    => [
                    'class'   => ShopScheduleOpen::class,
                    'enabled' => true
                ],
                'shopClosed'  => [
                    'class'   => ShopScheduleClosed::class,
                    'enabled' => true
                ],
                'shopSupply'  => [
                    'class'   => ShopSupply::class,
                    'enabled' => true
                ],
                'shopContent' => [
                    'class'   => ShopContent::class,
                    'enabled' => true
                ],
                'productImporter' => [
                    'class'   => ProductImporter::class,
                    'enabled' => true
                ]
            ]
        ],
        'blog' => [
            'class'   => \phycom\common\components\Blog::class,
            'enabled' => false,
        ],
        'reviews' => [
            'class'   => \phycom\common\components\Review::class,
            'enabled' => false,
        ],
        'comments' => [
            'class'   => \phycom\common\components\Comment::class,
            'enabled' => false
        ],
        'subscription' => [
            'class'   => \phycom\common\components\Subscription::class,
            'enabled' => false,
        ],
        'partnerContracts' => [
            'class'   => \phycom\common\components\PartnerContract::class,
            'enabled' => false
        ],
	    'urlManagerBackend' => require(__DIR__ . '/../../backend/config/url-manager.php'),
        'urlManagerFrontend' => require(__DIR__ . '/../../frontend/config/url-manager.php'),
	    'view' => [
		    'class' => 'yii\web\View',
		    'renderers' => [
			    'twig' => [
				    'class' => yii\twig\ViewRenderer::class,
				    'cachePath' => false,//'@runtime/Twig/cache',
				    'options' => ['autoescape' => false],
                    'globals' => [
                        'app'        => 'Yii::$app',
                        'Html'       => '\yii\helpers\Html',
                        'ActiveForm' => '\yii\widgets\ActiveForm',
                    ],
                    'functions' => [
                        't'            => '\phycom\common\models\MessageTemplate::translateMessage',
                        'conf'         => '\phycom\common\helpers\c::param',
                        'url'          => '\phycom\common\helpers\Url::feFull',
                        'route'        => '\phycom\common\helpers\Url::toFeRoute',
                        'backendUrl'   => '\phycom\common\helpers\Url::beFull',
                        'backendRoute' => '\phycom\common\helpers\Url::toBeRoute',
                    ],
                    'filters'   => [
                        'a'        => '\yii\helpers\Html::a',
                        'url'      => '\phycom\common\helpers\f::url',
                        'currency' => '\phycom\common\helpers\f::currency',
                        'date'     => '\phycom\common\helpers\f::date',
                        'phone'    => '\phycom\common\helpers\f::phone',
                        'datetime' => '\phycom\common\helpers\f::datetime'
                    ]
			    ]
		    ],
	    ],
	    'lang' => [
	        'class' => phycom\common\components\LanguageManager::class
        ],
	    'log' => [
		    'traceLevel' => YII_DEBUG ? 3 : 0,
		    'targets' => require(__DIR__ . '/logtarget.php')
	    ],
	    'i18n' => [
		    'class' => phycom\common\components\i18N::class,
		    'termBegin' => '',
		    'termClose' => '',
		    'translations' => [
			    '*' => [
				    'class' => yii\i18n\GettextMessageSource::class,
				    'basePath' => ['@translations', '@phycom/translations'],
				    'sourceLanguage' => 'en',
				    'useMoFile' => false
			    ],
                'common/modules*' => [
                    'class' => phycom\common\components\MessageSource::class,
                    'basePath' => ['@translations', '@phycom/translations'],
                    'catalog' => 'modules'
                ],
			    'common*' => [
				    'class' => phycom\common\components\MessageSource::class,
				    'basePath' => ['@translations', '@phycom/translations'],
				    'catalog' => 'common'
			    ],
			    'backend*' => [
				    'class' => phycom\common\components\MessageSource::class,
				    'basePath' => ['@translations', '@phycom/translations'],
				    'catalog' => 'backend'
			    ],
			    'public*' => [
				    'class' => phycom\common\components\MessageSource::class,
				    'basePath' => ['@translations', '@phycom/translations'],
				    'catalog' => 'public'
			    ],
			    'email*' => [
				    'class' => phycom\common\components\MessageSource::class,
				    'basePath' => ['@translations', '@phycom/translations'],
				    'catalog' => 'template'
			    ],
			    'message*' => [
				    'class' => phycom\common\components\MessageSource::class,
				    'basePath' => ['@translations', '@phycom/translations'],
				    'catalog' => 'template'
			    ],
			    'template*' => [
				    'class' => phycom\common\components\MessageSource::class,
				    'basePath' => ['@translations', '@phycom/translations'],
				    'catalog' => 'template'
			    ]
		    ],
	    ],
	    'fileStorage' => [
		    'class' => phycom\common\components\FileStorage::class,
		    'storages' => [
			    'local' => [
				    'class' => \yii2tech\filestorage\local\Storage::class,
				    'basePath' => '@files',
				    'baseUrl' => '@web/file/download',
				    'filePermission' => 0775,
				    'buckets' => [
					    phycom\common\components\FileStorage::BUCKET_TEMP,
					    phycom\common\components\FileStorage::BUCKET_AVATARS,
					    phycom\common\components\FileStorage::BUCKET_PRODUCT_FILES,
                        phycom\common\components\FileStorage::BUCKET_CONTENT_FILES,
					    phycom\common\components\FileStorage::BUCKET_INVOICES,
					    phycom\common\components\FileStorage::BUCKET_ORDER
				    ]
			    ]
		    ]
	    ],
	    // this is the high priority queue
	    'queue1' => [
		    'class' => phycom\common\components\queue\Beanstalk::class,
            'host' => getenv(ENV . '_BEANSTALK_HOST'),
            'port' => getenv(ENV . '_BEANSTALK_PORT'),
		    'tube' => APP . '-high-priority',
            'ttr' => 5 * 60, // Max time for anything job handling
            'attempts' => 3, // Max number of attempts
	    ],
	    // this is for low priority jobs
	    'queue2' => [
		    'class' => phycom\common\components\queue\Beanstalk::class,
            'host' => getenv(ENV . '_BEANSTALK_HOST'),
            'port' => getenv(ENV . '_BEANSTALK_PORT'),
		    'tube' => APP . '-default',
            'ttr' => 5 * 60, // Max time for anything job handling
            'attempts' => 3, // Max number of attempts
	    ],
        'pages' => [
            'class' => \phycom\common\components\PageSpaceCollection::class,
            'items' => []
        ],
	    'bootstrap' => phycom\common\components\Bootstrap::class
    ],
	'bootstrap' => [
		'bootstrap',
		'queue1', // high priority queue registers own console commands
		'queue2', // low priority queue registers own console commands
	],
];
