<?php
return [
    'error' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error'],
        'except' => ['yii\web\HttpException:404'],
        'logFile' => '@logs/error.log'
    ],
    '404' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error'],
        'categories' => ['yii\web\HttpException:404', 'invalid-request'],
        'logVars' => ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION'],
        'logFile' => '@logs/404.log'
    ],
    'main' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['warning', 'info'],
        'logVars' => [],
        'except' => [
            'yii\*',
            'email', 'phycom\common\modules\email\*',
            'payment', '\phycom\common\modules\payment\*',
            'delivery', 'phycom\common\modules\delivery\*',
            'transaction'
        ],
        'logFile' => '@logs/main.log',
        'maxLogFiles' => 30,
        'maxFileSize' => 20480
    ],
    'db' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['warning', 'info'],
        'logVars' => [],
        'categories' => ['yii\db\*'],
        'logFile' => '@logs/db.log',
        'maxLogFiles' => 3,
        'maxFileSize' => 20480
    ],
    'yii' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['warning', 'info'],
        'logVars' => [],
        'categories' => ['yii\*'],
        'except' => ['yii\db\*'],
        'logFile' => '@logs/trace.log',
        'maxLogFiles' => 2,
    ],
    'trace' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['trace'],
        'logVars' => [],
        'logFile' => '@logs/trace.log',
        'maxLogFiles' => 2,
        'maxFileSize' => 20480
    ],
    'redis' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error'],
        'categories' => ['yii\redis\*'],
        'logFile' => '@logs/redis/error.log'
    ],
    'order' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['warning', 'info', 'trace'],
        'categories' => [
            'phycom\common\models\Order*',
            'phycom\frontend\models\Order*',
            'common\models\Order*',
            'order',
            'phycom\common\models\Invoice*',
            'common\models\Invoice*',
            'phycom\common\jobs\Invoice*',
            'invoice'
        ],
        'logFile' => '@logs/order/order.log'
    ],
    'order-error' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error'],
        'categories' => [
            'phycom\common\models\Order*',
            'phycom\frontend\models\Order*',
            'common\models\Order*',
            'order',
            'phycom\common\models\Invoice*',
            'common\models\Invoice*',
            'phycom\common\jobs\Invoice*',
            'invoice'
        ],
        'logFile' => '@logs/order/error.log'
    ],
    'product-import' => [
        'class' => 'yii\log\FileTarget',
        'exportInterval' => 1,
        'categories' => [
            'product-import',
            'phycom\common\components\commerce\ProductImporter',
            'phycom\common\models\product\import*',
            'common\models\product\import*',
        ],
        'logFile' => '@logs/product-import.log',
        'logVars' => [],
    ],
    'email' => [
        'class' => 'yii\log\FileTarget',
        'exportInterval' => 1,
        'levels' => ['warning', 'info', 'trace'],
        'categories' => ['email', 'phycom\common\modules\email\*'],
        'logFile' => '@logs/email/email.log',
        'logVars' => [],
    ],
    'email-error' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error'],
        'categories' => ['email', 'phycom\common\modules\email\*'],
        'logFile' => '@logs/email/error.log',
        'logVars' => []
    ],
    'sms' => [
        'class' => 'yii\log\FileTarget',
        'exportInterval' => 1,
        'levels' => ['warning', 'info', 'trace'],
        'categories' => ['sms', 'phycom\common\modules\sms\*'],
        'logFile' => '@logs/sms/sms.log',
        'logVars' => [],
    ],
    'sms-error' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error'],
        'categories' => ['sms', 'phycom\common\modules\sms\*'],
        'logFile' => '@logs/sms/error.log',
        'logVars' => []
    ],
    'payment' => [
        'class' => 'yii\log\FileTarget',
        'exportInterval' => 1,
        'levels' => ['warning', 'info', 'trace'],
        'categories' => ['payment', '\phycom\common\modules\payment\*'],
        'logFile' => '@logs/payment/payment.log',
        'logVars' => [],
    ],
    'payment-error' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error'],
        'categories' => ['payment', '\phycom\common\modules\payment\*'],
        'logFile' => '@logs/payment/error.log',
        'logVars' => []
    ],
    'delivery' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['warning','info','trace'],
        'categories' => ['delivery', 'phycom\common\modules\delivery\*'],
        'logFile' => '@logs/delivery/delivery.log',
        'logVars' => ['_REQUEST'],
    ],
    'delivery-error' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error'],
        'categories' => ['delivery', 'phycom\common\modules\delivery\*'],
        'logFile' => '@logs/delivery/error.log',
        'logVars' => []
    ],
    'auth' => [
        'class' => 'yii\log\FileTarget',
        'exportInterval' => 1,
        'levels' => ['warning', 'info', 'trace'],
        'categories' => ['auth', '\phycom\common\modules\auth\*'],
        'logFile' => '@logs/auth/auth.log',
        'logVars' => [],
    ],
    'auth-error' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error'],
        'categories' => ['auth', '\phycom\common\modules\auth\*'],
        'logFile' => '@logs/auth/error.log',
        'logVars' => []
    ],
    'transaction' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error', 'warning', 'info', 'trace'],
        'categories' => ['transaction'],
        'logFile' => '@logs/transaction/transaction.log',
        'logVars' => []
    ]
];
