<?php

use \phycom\common\models\File;

return [
    'user.passwordResetTokenExpire' => 3600,
    'languages'                     => ['en', 'et', 'ru'],
    'orderNoBegin'                  => 78000000,
    'invoiceNoBegin'                => 3462,
    'sendOrderNotificationSms'      => false,
    'autoPostageLabel'              => false,
    'promotionCodesEnabled'         => true,
    'excerptWords'                  => 16,
    'googleApiKey'                  => getenv(ENV . '_GOOGLE_API_KEY'),
    'thumbSizes' => [
        File::THUMB_SIZE_SMALL  => [120, null],
        File::THUMB_SIZE_MEDIUM => [400, null],
        File::THUMB_SIZE_LARGE  => [640, null]
    ]
];
