<?php

/**
 * This constant defines the application installation directory.
 */
defined('ROOT_PATH') or define('ROOT_PATH', dirname(dirname(dirname(dirname(dirname(__DIR__))))));
/**
 * This constant defines the phycom installation directory.
 */
defined('PHYCOM_PATH') or define('PHYCOM_PATH', dirname(dirname(__DIR__)));


Yii::setAlias('@root', ROOT_PATH);

Yii::setAlias('@logs', ROOT_PATH . '/logs');
Yii::setAlias('@files', ROOT_PATH . '/files');
Yii::setAlias('@common', ROOT_PATH . '/common');
Yii::setAlias('@frontend', ROOT_PATH . '/frontend');
Yii::setAlias('@backend', ROOT_PATH . '/backend');
Yii::setAlias('@console', ROOT_PATH . '/console');
Yii::setAlias('@translations', ROOT_PATH . '/translations');
Yii::setAlias('@templates', ROOT_PATH . '/templates');
Yii::setAlias('@bower', ROOT_PATH . '/vendor/bower');
Yii::setAlias('@npm', ROOT_PATH . '/vendor/npm');
Yii::setAlias('@phycom', PHYCOM_PATH);
