<?php

namespace phycom\backend\helpers;

use phycom\common\models\User;

use yii\base\BaseObject;
use Yii;

/**
 * Class UserLanguage
 *
 * @package phycom\backend\helpers
 */
class UserLanguage extends BaseObject
{
    protected $user;

    public function __construct(User $user, $config = [])
    {
        $this->user = $user;
        parent::__construct($config);
    }

    public function check()
    {
        if (Yii::$app->language !== $this->user->settings->language) {
            Yii::$app->language = $this->user->settings->language;
        }
    }
}
