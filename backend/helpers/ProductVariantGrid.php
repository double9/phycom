<?php

namespace phycom\backend\helpers;


use phycom\backend\models\product\ProductVariantForm;
use phycom\backend\models\product\SearchProductVariant;
use phycom\backend\widgets\MultiFormGrid;
use phycom\backend\widgets\ActiveField;
use phycom\backend\widgets\MultiFormGridDataColumn;

use phycom\common\helpers\f;
use phycom\common\helpers\Url;
use phycom\common\models\attributes\ProductVariantStatus;
use phycom\common\models\product\ProductVariant;

use yii\base\BaseObject;
use yii\data\ArrayDataProvider;
use yii\data\DataProviderInterface;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;

/**
 * Class ProductVariantGrid
 *
 * @package phycom\backend\helpers
 */
class ProductVariantGrid extends BaseObject
{
    /**
     * @var ProductVariantForm
     */
    protected $form;

    public function __construct(ProductVariantForm $productParamForm, $config = [])
    {
        $this->form = $productParamForm;
        parent::__construct($config);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function render()
    {
        return MultiFormGrid::widget($this->createConfiguration());
    }

    /**
     * @return MultiFormGrid
     * @throws \yii\base\Exception
     */
    public function instance()
    {
        return new MultiFormGrid($this->createConfiguration());
    }

    /**
     * @return array
     * @throws \yii\base\Exception
     */
    protected function createConfiguration()
    {
        $dataProvider = $this->getDataProvider();

        return [
            'id'           => 'product-variant-grid',
            'dataProvider' => $dataProvider,
            'formModel'    => $this->form,
            'tableOptions' => ['class' => 'table table-responsive table-striped no-margin'],
            'formOptions'  => [
                'validateOnBlur'   => false,
                'validateOnChange' => false
            ],
            'rowOptions'   => function ($model, $key, $index, $grid) {
                /**
                 * @var ProductVariant $model
                 * @var mixed $key
                 * @var int $index
                 * @var MultiFormGrid $grid
                 */
                if ($variant = Yii::$app->modelFactory->getVariant()::findByName($model->name)) {
                    return [
                        'data-field-name'  => $variant->name,
                        'data-field-label' => $variant->label
                    ];
                }
            },
            'columns'      => $this->createColumns(),
            'newRow'       => $this->newRow($dataProvider)
        ];
    }

    /**
     * @return DataProviderInterface
     * @throws \yii\base\Exception
     */
    protected function getDataProvider()
    {
        if ($this->form->product->isNewRecord) {
            return new ArrayDataProvider([
                'allModels'  => $this->form->getModels(),
                'modelClass' => ProductVariant::class
            ]);
        } else {
            $searchModel = new SearchProductVariant();
            $searchModel->product_id = $this->form->product->id;
            $searchModel->populateRelation('product', $this->form->product);

            return $searchModel->search();
        }
    }

    /**
     * @param $value
     * @param string $type
     * @return string
     */
    protected function renderLabel($value, $type = 'default')
    {
        return Html::tag('span', $value, ['class' => 'field-label label label-' . $type]);
    }

    /**
     * @param string $label
     * @param array $items
     * @return string
     */
    protected function renderDropdown(string $label, array $items)
    {
        $id = uniqid();
        $html = '<div class="dropdown">
          <button class="btn btn-default dropdown-toggle" type="button" id="' . $id . '-variant-type-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            ' . $label . '
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" aria-labelledby="' . $id . '-variant-type-list">';
            foreach ($items as $item) {
                $html .= '<li><a href="#">' . (string) $item . '</a></li>';
            };
        $html .= '</ul></div>';

        return $html;
    }

    /**
     * @param ProductVariant $model
     * @param string $key
     * @param MultiFormGridDataColumn $column
     * @return string
     */
    protected function renderOptionList(ProductVariant $model, $key, MultiFormGridDataColumn $column)
    {
        $view = $model->variant->hasOneOption()
            ? '/product/partials/variant-option'
            : '/product/partials/variant-option-list';

        return Yii::$app->view->render($view, [
            'form'           => $this->form,
            'productVariant' => $model,
            'key'            => $key,
            'column'         => $column
        ]);
    }

    /**
     * @return array
     */
    protected function createColumns()
    {
        return [
            [
                'attribute'     => 'name',
                'format'        => 'raw',
                'enableSorting' => false,
                'inputField'    => function ($field, $model, $key) {
                    /**
                     * @var ProductVariant $model
                     * @var ActiveField $field
                     */
                    return $field->hiddenInput() . $this->renderLabel($model->label);
                },
                'options' => ['width' => 140],
            ],
            [
                'label'         => Yii::t('backend/main', 'Type'),
                'format'        => 'raw',
                'enableSorting' => false,
                'inputField'    => false,
                'value'         => function ($model, $key, $index, $column) {
                    /**
                     * @var ProductVariant $model
                     * @var ActiveField $field
                     */
                    return $model->variant
                        ? Html::tag('span', $model->variant->getTypeLabel(), ['class' => 'badge field-label'])
                        : f::text(null);
                },
            ],
            [
                'label'         => false,
                'format'        => 'raw',
                'enableSorting' => false,
                'inputField'    => false,
                'value'         => function ($model, $key, $index, $column) {
                    /**
                     * @var ProductVariant $model
                     * @var ActiveField $field
                     */
                    return $model->variant ? $this->renderOptionList($model, $key, $column) : f::text(null);
                },
            ],
            [
                'attribute'     => 'status',
                'format'        => 'raw',
                'enableSorting' => false,
                'inputField'    => function ($field, $model, $key) {
                    /**
                     * @var ProductVariant $model
                     * @var ActiveField $field
                     */
                    return $field->dropDownList(ProductVariantStatus::displayValues());
                },
                'options' => ['width' => 200],
            ],
            [
                'attribute'     => 'user_select',
                'format'        => 'boolean',
                'enableSorting' => false,
                'inputField'    => function ($field, $model, $key) {
                    /**
                     * @var ProductVariant $model
                     * @var ActiveField $field
                     */
                    return $field->boolean();
                },
                'options' => ['width' => 140],
            ],
            [
                'attribute'     => 'created_at',
                'format'        => 'date',
                'inputField'    => false,
                'enableSorting' => false,
                'options' => ['width' => 140]
            ],
            [
                'attribute'     => 'updated_at',
                'format'        => 'date',
                'inputField'    => false,
                'enableSorting' => false,
                'options' => ['width' => 140]
            ]
        ];
    }

    /**
     * @param DataProviderInterface $dataProvider
     * @return \Closure
     */
    protected function newRow(DataProviderInterface $dataProvider)
    {
        return function() use ($dataProvider) {

            $items = [];
            $options = [];

            $selected = ArrayHelper::getColumn($dataProvider->models, 'name');
            foreach (Yii::$app->modelFactory->getVariant()::findAll() as $variant) {
                if (!in_array($variant->name, $selected)) {
                    $items[$variant->name] = $variant->label;
                    $options[$variant->name] = [
                        'data-label' => $variant->label,
                    ];
                }
            }
            $btnOptions  = [
                'class' => 'add-row btn btn-flat btn-default pull-left',
                'style' => 'margin-right: 10px;'
            ];
            $dropdownOptions = [
                'class'    => 'field-name form-control',
                'data-url' => Url::toBeRoute(['/product/get-variant-row']),
                'options'  => $options
            ];

            if (empty($items)) {
                Html::addCssClass($btnOptions, 'disabled');
                $dropdownOptions['disabled'] = 'disabled';
            }

            $btn = Html::button('<span class="fas fa-plus"></span>&nbsp;&nbsp;' . Yii::t('backend/product', 'Add Variant'), $btnOptions);
            $dropDown = Html::tag('div', Html::dropDownList('name', null, $items, $dropdownOptions), ['class' => 'form-group pull-left', 'style' => 'width: 260px; margin-bottom: 0;']);

            return Html::tag('div', $btn . $dropDown, ['class' => 'new-row-form clearfix']);
        };
    }
}
