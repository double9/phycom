<?php

namespace phycom\backend\helpers;

use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii;

/**
 * Class Html
 * @package phycom\backend\helpers
 */
class Html extends \yii\helpers\Html
{
    public static function staticField(Model $model, $attribute, $value = null, $format = 'text', array $options = [])
    {
        $label = static::tag('strong', $model->getAttributeLabel($attribute) . ':', ['class' => 'attribute-label']);
        $value = static::tag('span', Yii::$app->formatter->format($value ?: $model->$attribute, $format));

        $defaultOptions = ['class' => 'static-field'];

        return static::tag('div', $label . $value, ArrayHelper::merge($defaultOptions, $options));
    }

    public static function staticDate(Model $model, $attribute, $value = null, array $options = [])
    {
        return static::staticField($model, $attribute, $value, 'date', $options);
    }

    public static function staticDatetime(Model $model, $attribute, $value = null, array $options = [])
    {
        return static::staticField($model, $attribute, $value, 'datetime', $options);
    }
}