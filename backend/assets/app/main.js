"use strict";
(function($) {

    /**
     * replace dropdown to dropup in case close to bottom of a screen
     */
    $('.dropdown, .dropup').on('shown.bs.dropdown', function() {
        let c = $(this).find('.dropdown-menu').offset();
        if (($(window).height()-c.top) < 30) {
            $(this).addClass('dropup');
        }
    });

    /**
     * close success alert messages automatically
     */
    window.setTimeout(function () {
        $('#system-messages .alert-success').each(function(i, el) {
            let $el = $(el);
            $el.fadeOut(2000, function () {
                $el.alert('close');
            });
        });
    }, 10000);

})(jQuery);

