<?php

namespace phycom\backend\assets;

use yii\web\AssetBundle;

/**
 * Class ShopAddressFormAsset
 * @package phycom\backend\assets
 */
class ShopAddressFormAsset extends AssetBundle
{
    public $sourcePath = '@phycom/backend/assets/shop-address-form';
    public $js = [
        'main.js'
    ];
    public $publishOptions = ['except' => ['*.less']];
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}