"use strict";
var AlertMessage = (function ($) {

    let Alert = function (el) {
        this.el = el;
        this.$el = $(el);

        if (!this.$el.length) {
            throw el + ' is not found';
        }
    };

    Alert.init = function (el) {
        return new Alert(el);
    };

    Alert.prototype.showMessage = function(msg, type) {
        if (msg.length) {
            type = type || 'info';
            let alert = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                '<span class="msg">' + msg + '</span></div>';
            this.$el.append(alert);
        }
    };

    Alert.prototype.showPlaintextError = function (html) {
        this.showError(this.extractContent(html));
    };

    Alert.prototype.extractContent = function(s) {
        let span = document.createElement('span');
        span.innerHTML = s;
        return span.textContent || span.innerText;
    };

    Alert.prototype.showError = function(errorMsg) {
        let alert = '<div class="alert alert-danger alert-dismissible" role="alert">' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
            '<strong style="margin-right: 8px;">' + (typeof i18n !== 'undefined' ? i18n.error : 'Error') + ': </strong><span class="error-msg">' + errorMsg + '</span></div>';

        let $err = this.$el.find('.alert.alert-danger');
        let duplicateMsg = false;

        if ($err.length) {
            $err.each(function (i, el) {
                $(el).find('span.error-msg').each(function (n, err) {
                    if ($(err).html() === errorMsg)  {
                        duplicateMsg = true;
                    }
                });
            });
            if (!duplicateMsg) {
                $err.append('<span class="error-msg">' + errorMsg + '</span>');
            }
            if (!$err.hasClass('gr')) {
                $err.addClass('gr');
            }
        } else {
            this.$el.append(alert);
        }
    };

    Alert.prototype.clearErrors = function() {
        this.$el.html('');
    };

    return Alert;

})(jQuery);
