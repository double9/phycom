var FormSubmitBtn = (function ($) {
    'use strict';

    const SubmitButton = function (el, params) {
        this.$el = $(el);

        if (!this.$el.length) {
            throw 'Element ' + el + ' not found';
        }
        this.el = this.$el[0];

        /**
         * @type {FormCollection}
         */
        this.form = null;

        if (params.formCollection) {
            this.setFormCollection(params.formCollection);
        }
    };

    SubmitButton.prototype.setFormCollection = function (el) {
        this.form = $(el).data('formCollection') || null;
        if (!this.form) {
            throw new Error('Invalid or not initialized FormCollection ' + el);
        }
        this.reset().init();
    };

    SubmitButton.prototype.reset = function () {
        this.$el.off('click');
        return this;
    };

    SubmitButton.prototype.init = function () {
        let self = this;

        this.$el.on('click', '.btn-save.save:not(disabled)', function (e) {
            self.save($(this), FormCollectionActions.SAVE, e);
        });
        this.$el.on('click', '.btn-save.save-and-new:not(disabled)', function (e) {
            self.save($(this), FormCollectionActions.SAVE_NEW, e);
        });
        this.$el.on('click', '.btn-save.save-and-duplicate:not(disabled)', function (e) {
            self.save($(this), FormCollectionActions.SAVE_DUPLICATE, e);
        });
        this.$el.on('click', '.btn-save.save-and-close:not(disabled)', function (e) {
            self.save($(this), FormCollectionActions.SAVE_CLOSE, e);
        });

        return this;
    };

    SubmitButton.prototype.save = function ($btn, action, e) {
        e.preventDefault();
        this.showLoader();
        this.form.submit(action)
            .fail(() => this.hideLoader());
    };

    SubmitButton.prototype.showLoader = function () {
        this.$el.find('.save').button('loading');
        this.$el.find('button:not(.save)').prop('disabled', true);
    };

    SubmitButton.prototype.hideLoader = function () {
        this.$el.find('.save').button('reset');
        this.$el.find('button:not(.save)').prop('disabled', false);
    };

    return {
        init: function (el, params) {
            let submitButton = new SubmitButton(el, params);
            $(el).data('submitButton', submitButton);
        }
    };

})(jQuery);
