const SubmitButton = (function ($) {

    let Btn2 = function (el, formContainer) {
        this.el = el;
        this.$el = $(el);

        if (!this.$el.length) {
            throw el + ' is not found';
        }
        this.container = formContainer;
        if (!$(this.container).length) {
            throw formContainer + ' is not found';
        }
        this.msg = AlertMessage.init($('#system-messages'));
        this.onAfterSubmit = function (data) {return $.Deferred().resolve(data).promise();};
        this.init();
    };

    Btn2.init = function(btn, formContainer) {
        return new Btn2(btn, formContainer);
    };

    Btn2.prototype.submitForm = function (url, params, method) {
        method = method || 'post'; // Set method to post by default if not specified.

        // The rest of this code assumes you are not using a library.
        // It can be made less wordy if you use one.
        let form = document.createElement('form');
        form.setAttribute('method', method);
        form.setAttribute('action', url);

        for (let key in params) {
            if (params.hasOwnProperty(key)) {
                let hiddenField = document.createElement('input');
                hiddenField.setAttribute('type', 'hidden');
                hiddenField.setAttribute('name', key);
                hiddenField.setAttribute('value', params[key]);
                form.appendChild(hiddenField);
            }
        }
        document.body.appendChild(form);
        form.submit();
    };

    Btn2.prototype.submitRequest = function (url, params, method) {
        method = method || 'post';
        this.msg.clearErrors();
        return $.ajax({url: url, type: method, data: params}).done((data) => {

            let msg = data.msg || false;
            if (msg) {
                this.msg.showMessage(data.msg, 'success');
            }
            if (data.error) {
                this.msg.showError(data.error);
                this.reset();
                return new $.Deferred().reject(data.error).promise();
            }
            return data;

        }).fail((xhr) => {
            console.log(xhr);
            let err = xhr.status + ' ' + xhr.statusText + '<br /><span><pre>' + JSON.stringify(xhr.responseJSON, null, 2) + '</pre></span>';
            this.msg.showError(err);
            this.reset();
            return xhr;
        });
    };

    Btn2.prototype.reset = function () {
        this.$el.button('reset');
    };

    Btn2.prototype.init = function () {
        this.$el.data('submit-btn', this);
        this.$el.on('click', (e) => {
            e.preventDefault();
            let isCollection = Boolean($(this.container).attr('data-form-collection')),
                $form = isCollection ? $(this.container).find('form') : $(this.container).find('form').first(),
                action = $(this.container).attr('data-action') || null,
                ajax = Boolean($(this.container).attr('data-form-ajax'));

            if (!$form.length) {
                return;
            }
            this.$el.button('loading');

            // remove all forms that are not activeforms
            let i = $form.length;
            while (i--) {
                let $f = $($form[i]);
                if (typeof $f.data('yiiActiveForm') === 'undefined') {
                    $form.splice(i, 1);
                }
            }

            if (isCollection) {
                let promises = [];
                for (let i=0; i<$form.length; i++) {
                    let $f = $($form[i]);

                    promises.push(ActiveFormHelper.validate($f, (errorMessages) => {
                        if (typeof errorMessages !== 'undefined' && errorMessages.length) {
                            this.msg.showError(errorMessages[0]);
                        }
                        this.hideLoader();
                    }));
                }

                $.when.apply(null, promises).then(() => {

                    let formCollection = $(),
                        formData = {};

                    $form.each(function (i, el) {
                        formCollection = formCollection.add($(el));
                        if (!action) {
                            action = $(el).attr('action');
                        }
                    });

                    $.each(formCollection.serializeArray(), function() {
                        formData[this.name] = this.value;
                    });

                    if (ajax) {
                        this.submitRequest(action, formData).then(this.onAfterSubmit).done(() => this.reset());
                    } else {
                        this.submitForm(action, formData);
                    }

                }).fail(() => this.reset());


            } else {
                ActiveFormHelper.validate($form).then(() => {
                    $form.submit();
                }).fail(() => this.reset());
            }
            return false;
        });
    };

    return Btn2;

})(jQuery);
