"use strict";
(function ($) {

    var FormCollection = window.FormCollection = function (el, options) {

        this.el = el;
        this.$el = $(el);

        if (!this.$el.length) {
            throw 'Element ' + el + ' not found';
        }
        let $msgContainer = $('.system-messages', this.el);
        if (!$msgContainer.length) {
            throw 'Required element ".system-messages" was not found';
        }
        this.msg = AlertMessage.init($msgContainer);
        this.submitUrl = options.submitUrl || this.$el.data('submit-url');

        this.forms = [];
        this.$forms = [];
        this.onAfterSubmit = function (data) {return $.Deferred().resolve(data).promise();};
        this.onValidationError = options.onValidationError || undefined;

        this.$el.data('formCollection', this);
    };

    /**
     * @param {String} [action]
     * @returns {*}
     */
    FormCollection.prototype.submit = function(action) {
        this.showLoader();
        action = action || FormCollectionActions.SAVE;
        return this.validateAndSubmit(action).then(this.onAfterSubmit).done((data) => {
            if (typeof data.url !== 'undefined') {
                this.reload(data.url);
            } else {
                this.hideLoader();
            }
        });
    };

    FormCollection.prototype.add = function (form) {

        if (_.isObject(form)) {
            $(form.selector).data('label', form.label);
            form = form.selector;
        }

        this.forms.push(form);
        this.$forms.push($(form));
        return this;
    };

    FormCollection.prototype.addAttachmentForm = function (container) {

        if (_.isObject(container)) {
            $(container.selector).data('label', container.label);
            container = container.selector;
        }

        this.onAfterSubmit = (data) => {
            let deferred = $.Deferred();
            if (data.error) {
                return deferred.reject(data.error);
            }
            let el = $(container),
                fileInput = el.data('fileinput'),
                totalFiles = Object.keys(el.fileinput('getFileStack')).length,
                numFilesUploaded = 0;

            if (totalFiles === 0) {
                return deferred.resolve(data);
            }

            el.on('fileuploaded', (e, uploadedData, previewId, index) => {
                numFilesUploaded++;
                if (numFilesUploaded >= totalFiles) {
                    if (typeof data.url !== 'undefined') {
                        this.reload(data.url);
                    }
                    return deferred.resolve(data);
                }
            });

            fileInput.uploadUrl = data.uploadUrl;
            el.fileinput('upload');

            return deferred.promise();
        };
        return this;
    };

    FormCollection.prototype.reload = function (url) {
        if (_.isArray(url)) {
            // we want to make a POST request with payload
            return this.sendRequest(url[0], url[1]);
        }
        if (window.location.href === url) {
            // we have same url so we need to reload the current page
            window.location.reload();
        } else if (url.includes('#') && window.location.href === url.split('#')[0]) {
            // we have only hash difference
            window.location.replace(url);
        } else {
            window.location.href = url;
        }
    };

    FormCollection.prototype.sendRequest = function (url, params, method) {
        method = method || 'post'; // Set method to post by default if not specified.

        // The rest of this code assumes you are not using a library.
        // It can be made less wordy if you use one.
        let form = document.createElement('form');
        form.setAttribute('method', method);
        form.setAttribute('action', url);

        for (let key in params) {
            if (params.hasOwnProperty(key)) {
                let hiddenField = document.createElement('input');
                hiddenField.setAttribute('type', 'hidden');
                hiddenField.setAttribute('name', key);
                hiddenField.setAttribute('value', params[key]);
                form.appendChild(hiddenField);
            }
        }
        document.body.appendChild(form);
        form.submit();
    };


    FormCollection.prototype.submitRequest = function (action) {
        var self = this,
            $forms = null;

        for (var i=0; i<this.forms.length; i++) {
            $forms = !$forms ? this.$forms[i] : $forms.add(this.$forms[i]);
        }
        var formData = $forms.serialize() + '&afterSave=' + action + '&hash=' + encodeURIComponent(location.hash);

        this.showLoader();
        return $.ajax({
            url: this.submitUrl,
            type: 'POST',
            data: formData
        }).then(function (data, textStatus, xhr) {

            return $.Deferred((deferred) => {
                if (data.error) {
                    return deferred.reject(xhr, data, data.error);
                }
                let msg = data.msg || false;
                if (msg) {
                    self.msg.showMessage(data.msg, 'success');
                }
                deferred.resolve(data);
            }).promise();

        }).fail(function (xhr, textStatus, errorThrown) {

            if (errorThrown && errorThrown.length) {
                self.msg.showPlaintextError(errorThrown);
            } else {
                self.msg.showError(xhr.status + ' ' + xhr.statusText + '<br /><span><pre>' + JSON.stringify(xhr.responseJSON, null, 2) + '</pre></span>');
            }
            self.hideLoader();
            return xhr;
        });
    };

    FormCollection.prototype.validateAndSubmit = function (action) {
        this.msg.clearErrors();
        let promises = [];
        for (let i = 0; i < this.forms.length; i++) {
            let $form = this.$forms[i];
            promises.push(ActiveFormHelper.validate($form, (errs) => this.validationError($form, errs)));
        }
        return $.when.apply(null, promises).then(() => {
            return this.submitRequest(action);
        });
    };

    FormCollection.prototype.validationError = function ($form, messages) {
        messages = messages || [];
        if (messages.length) {
            for (let n = 0; n < messages.length; n++) {
                this.msg.showError(this.createFormErrorMessage($form, messages[n]));
            }
        }
        if (typeof this.onValidationError === 'function') {
            this.onValidationError.call(this, $form, messages);
        }
        this.hideLoader();
    };

    FormCollection.prototype.createFormErrorMessage = function ($form, msg) {
        let parts = [];
        let section = $form.data('label') || false;

        if (section) {
            parts.push('<span class="label label-warning">' + section + '</span>');
        }
        parts.push(msg);
        return parts.join(' ');
    };

    FormCollection.prototype.showLoader = function () {
        if (typeof window.spinner !== 'undefined') {
            spinner.show(this.loadingMessage);
        } else if (typeof window.Pace !== 'undefined') {
            Pace.stop();
            Pace.bar.render();
        } else {
            console.log('Spinner not found');
        }
    };

    FormCollection.prototype.hideLoader = function () {
        if (typeof window.spinner !== 'undefined') {
            spinner.hide();
        } else if (typeof window.Pace !== 'undefined') {
            Pace.stop();
        } else {
            console.log('Spinner not found');
        }
    };

})(jQuery);
