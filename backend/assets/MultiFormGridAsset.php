<?php

namespace phycom\backend\assets;

use yii\web\AssetBundle;

/**
 * MultiFormGrid widget asset bundle.
 */
class MultiFormGridAsset extends AssetBundle
{
	public $sourcePath = '@phycom/backend/assets/multi-form-grid';
	public $css = [
		'main.css'
	];
	public $js = [
		'main.js'
	];
	public $publishOptions = ['except' => ['*.less']];
	public $depends = [
		'yii\grid\GridViewAsset'
	];
}