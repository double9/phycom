"use strict";

var addressForm = (function ($) {

    var AddressForm = function (modelName, target) {
        this.modelName = modelName;
        var formId = '#' + modelName.toLowerCase();

        this.$form = $(formId);
        this.$modal = this.$form.closest('.modal');
        this.$target = $(target);
        this.$countrySelect = $(formId + '-country');
        this.$provinceSelect = $(formId + '-province');

        this.init();
    };


    AddressForm.prototype.init = function () {

        var self = this;
        this.$target.on('change', function (e) {
            var value = $(this).val(),
                url = $(this).data('label-url'),
                $container = $(this).closest('.address-field').find('.address');

            console.log('change');
            console.log(value);

            $.post(url, {address: value}, function (label) {
                $container.html(label);
            });
        });

        this.$form.on('click', 'button.submit:not(.disabled)', function (e) {
            e.preventDefault();
            self.submitForm($(this));
            return false;
        });

        this.$countrySelect.on('change', function () {
            var countryCode = $(this).val();
            console.log(countryCode);
            $.post(self.$countrySelect.data('url'), {country: countryCode}, function (data) {
                var options = '';
                console.log(data);
                for (var key in data) {
                    options += '<option value="'+key+'">' + data[key] + '</option>';
                }
                self.$provinceSelect.html(options);
            });
        });
    };

    AddressForm.prototype.destroy = function () {
        this.$target.off('change');
        this.$form.off('click', 'button.submit:not(.disabled)');
        this.$countrySelect.off('change');
    };

    AddressForm.prototype.validateForm = function () {
        var $form = this.$form;
        var deferred = $.Deferred();

        // validate form but not submit
        $form.data('yiiActiveForm').submitting = true;
        $form.yiiActiveForm('validate');
        $form.one('submit', function (e) {
            e.preventDefault();
            return false;
        });

        $form.one('afterValidate', function (e, messages, errorAttributes) {
            for (var attribute in messages) {
                if (messages.hasOwnProperty(attribute) && messages[attribute].length) {
                    return deferred.reject(messages);
                }
            }
            return deferred.resolve();
        });
        return deferred.promise();
    };


    AddressForm.prototype.submitForm = function ($btn) {
        var self = this,
            $msgContainer = $('.address-form-messages', this.$form),
            data = this.$form.serializeArray();

        $btn.button('loading');
        this.validateForm().then(function () {

            var address = {};
            $(data).each(function(index, obj){
                var regexp = new RegExp('('+self.modelName+'\\[|\\])', 'g');
                if (obj.name.match(regexp)) {
                    var attribute = obj.name.replace(regexp, '');
                    address[attribute] = obj.value;
                }
            });

            self.$target.val(JSON.stringify(address));
            self.$target.trigger('change');

            var $targetFieldContainer = self.$target.parent('div');
                $targetFieldContainer.removeClass('has-error');
                $targetFieldContainer.find('.help-block').html('').removeClass('help-block-error');

            $msgContainer.html('');
            self.$form.yiiActiveForm('resetForm');
            $btn.button('reset');

            window.setTimeout(function () {
                self.$modal.modal('hide');
            }, 1000);

        }).fail(function (err) {
            console.log(err);
            $btn.button('reset');
        });
    };

    return {
        init: function(modelName, field) {
            var form = new AddressForm(modelName, field);
        }
    };
})(jQuery);