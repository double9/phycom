<?php

namespace phycom\backend\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Tabs asset bundle.
 */
class TabsAsset extends AssetBundle
{
	public $sourcePath = '@phycom/backend/assets/tabs';
	public $css = [

	];
	public $publishOptions = ['except' => ['*.less']];
	public $js = [
		'sticky-tabs.js'
	];
	public $depends = [
        JqueryAsset::class
	];
}
