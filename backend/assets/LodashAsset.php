<?php

namespace phycom\backend\assets;

use yii\web\AssetBundle;

/**
 * Lodash js asset bundle.
 */
class LodashAsset extends AssetBundle
{
    public $sourcePath = '@vendor/npm-asset/lodash';
    public $css = [];
    public $js = [
        'lodash.min.js'
    ];
    public $publishOptions = ['except' => ['*.less']];
    public $depends = [];
}
