<?php

namespace phycom\backend\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\bootstrap\BootstrapAsset;

/**
 * DeliveryArea form related assets
 */
class DeliveryAreaAsset extends AssetBundle
{
    public $sourcePath = '@phycom/backend/assets/delivery-area';
    public $js = [
        'main.js'
    ];
    public $publishOptions = ['except' => ['*.less']];
    public $depends = [
        JqueryAsset::class,
        BootstrapAsset::class
    ];
}