<?php

namespace phycom\backend\assets;

use yii\web\AssetBundle;
use yii\grid\GridViewAsset;

/**
 * AjaxGrid widget asset bundle.
 */
class AjaxGridAsset extends AssetBundle
{
	public $sourcePath = '@phycom/backend/assets/grid';
	public $css = [
		'ajax-grid.css'
	];
	public $js = [
		'ajax-grid.js'
	];
	public $publishOptions = ['except' => ['*.less']];
	public $depends = [
        GridViewAsset::class
	];
}