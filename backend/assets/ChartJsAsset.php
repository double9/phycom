<?php

namespace phycom\backend\assets;


use yii\web\AssetBundle;

/**
 * ChartJS asset bundle.
 */
class ChartJsAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower-asset/chartjs/dist';
    public $css = [
        'Chart.min.css',
    ];
    public $js = [
        'Chart.bundle.min.js',
    ];
    public $publishOptions = ['except' => ['*.less']];
}