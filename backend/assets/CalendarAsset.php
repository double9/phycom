<?php

namespace phycom\backend\assets;

use yii\web\AssetBundle;
use yii;

/**
 * Calendar asset bundle.
 */
class CalendarAsset extends AssetBundle
{
    public $sourcePath = '@phycom/backend/assets/calendar';
    public $css = [
        'calendar.css'
    ];
    public $js = [
        'multicalendar.js'
    ];
    public $publishOptions = ['except' => ['*.less']];
    public $depends = [
        'phycom\backend\assets\FullCalendarAsset',
    ];
}