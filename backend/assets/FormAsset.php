<?php

namespace phycom\backend\assets;

use phycom\common\assets\ActiveFormHelperAsset;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\bootstrap\BootstrapAsset;

/**
 * Handles multiple forms submitting by js
 */
class FormAsset extends AssetBundle
{
	public $sourcePath = '@phycom/backend/assets/form';
	public $js = [
		'main.js',
        'btn.js'
	];
	public $publishOptions = ['except' => ['*.less']];
	public $depends = [
        FormActionsAsset::class,
	    AlertMessageAsset::class,
	    ActiveFormHelperAsset::class,
        JqueryAsset::class,
        LodashAsset::class,
        BootstrapAsset::class
	];
}
