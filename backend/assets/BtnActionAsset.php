<?php

namespace phycom\backend\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class BtnActionAsset
 * @package phycom\backend\assets
 */
class BtnActionAsset extends AssetBundle
{
	public $sourcePath = '@phycom/backend/assets/btn-action';
	public $js = [
		'main.js'
	];
	public $publishOptions = ['except' => ['*.less']];
	public $depends = [
        JqueryAsset::class
	];
}
