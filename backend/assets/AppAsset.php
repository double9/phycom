<?php

namespace phycom\backend\assets;

use dmstr\web\AdminLteAsset;
use phycom\common\assets\md\AssetBundle as MdAsset;

use yii\web\YiiAsset;
use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@phycom/backend/assets/app';
    public $css = [
        'admin.css',
	    'adminlte-skin-custom.css',
        'lazy-load-images.css'
    ];
    public $js = [
        'lazy-load-images.js',
    	'main.js',
	    //'product.js',
    ];
	public $publishOptions = ['except' => ['*.less']];
    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
	    AdminLteAsset::class,
        AdminLtePluginsAsset::class,
        MdAsset::class
    ];
}
