<?php

namespace phycom\backend\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\bootstrap\BootstrapAsset;

/**
 * Handles printing of alert messages by js
 */
class AlertMessageAsset extends AssetBundle
{
	public $sourcePath = '@phycom/backend/assets/form';
	public $js = [
		'alert-message.js'
	];
	public $publishOptions = ['except' => ['*.less']];
	public $depends = [
        JqueryAsset::class,
        BootstrapAsset::class
	];
}