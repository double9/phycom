ajaxGrid = (function ($) {
    return {
        init: function (el) {
            var self = this,
                $grid = $(el, '.ajax-grid'),
                excluded = '.actions, .row-checkbox, .clickable';

            $grid.on('click','.pagination a', function(e) {
                e.preventDefault();
                var url = $(this).attr('href');
                ajaxGrid.showSpinner($grid);
                $.get(url, function(data) {
                    ajaxGrid.hideSpinner($grid);
                    $grid.html($(data).contents());
                })
            });

            $grid.on('click','.ajax-actions a:not([data-ajax="false"])', function(e) {
                e.preventDefault();
                var url = $(this).attr('href');
                $(this).button('loading');
                //ajaxGrid.showSpinner($grid);
                $.get(url, function(data) {
                    if (data.error) {
                        console.log(data);
                    } else {
                        ajaxGrid.reload($grid);
                    }
                })
            });

            $grid.on('click', 'tr.row-link > td:not('+ excluded +')', function(e) {
                e.preventDefault();
                var url = $(this).closest('tr').attr('data-url');
                window.location = url;
                // window.open(url);
            });
        },

        reload: function($el) {
            var url = $el.closest('.ajax-grid').attr('data-url');
            $.get(url, function(data) {
                ajaxGrid.hideSpinner($el);
                $el.html($(data).contents());
            })
        },

        showSpinner: function($el) {
            var $html = $('<div class="loading"><div class="loading-mask"></div><div class="spinner"><span>Loading...</span></div></div>');
            $el.append($html);
            $html.fadeIn(300);
        },
        hideSpinner: function($el) {
            $('.loading', $el).fadeOut(300, function() {
                $(this).remove();
            });
        }
    };
})(jQuery);