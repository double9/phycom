grid = (function ($) {
    return {
        init: function (el) {
            var self = this,
                $grid = $(el, '.data-grid'),
                excluded = '.actions, .row-checkbox, .clickable';

            $grid.on('click', 'tr.row-link > td:not('+ excluded +')', function(e) {
                e.preventDefault();
                var url = $(this).closest('tr').attr('data-url');
                window.location = url;
                // window.open(url);
            });

            $grid.on('change', 'input[type="checkbox"]', function(e) {

                let $form = $('#bulk-edit-form');
                let $keysContainer = $form.find('.keys');
                let keyFieldName = $keysContainer.attr('data-name');
                if (!$form.length) {
                    return;
                }

                let selected = $grid.yiiGridView('getSelectedRows');
                if (selected.length) {
                    $form.slideDown(200);

                    let html = '';
                    for (let i = 0; i<selected.length; i++) {
                        html += '<input type="hidden" name="' + keyFieldName + '" value="' + selected[i] + '" />';
                    }
                    $keysContainer.html(html);

                } else {
                    $keysContainer.html('');
                    $form.slideUp(200);
                }

            });
        },
    };
})(jQuery);