<?php

namespace phycom\backend\assets;

use dmstr\web\AdminLteAsset;
use yii\web\AssetBundle;

/**
 * Class AdminLtePluginsAsset
 * @package phycom\backend\assets
 */
class AdminLtePluginsAsset extends AssetBundle
{
	public $sourcePath = '@vendor/almasaeed2010/adminlte/plugins';
	public $css = [
		'pace/pace.css'
	];
	public $js = [
		'pace/pace.js'
	];
	public $depends = [
        AdminLteAsset::class,
	];
}