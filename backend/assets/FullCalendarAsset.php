<?php

namespace phycom\backend\assets;

use phycom\backend\assets\MomentAsset;

use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii;

/**
 * FullCalendar asset bundle.
 */
class FullCalendarAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower-asset/fullcalendar/dist';
    public $css = [
        'fullcalendar.min.css',
//        'fullcalendar.print.min.css',
    ];
    public $js = [
        'fullcalendar.min.js',
//        'locale-all.js',
//        'gcal.js'
    ];
    public $publishOptions = ['except' => ['*.less']];
    public $depends = [
        MomentAsset::class,
        JqueryAsset::class,
        BootstrapAsset::class
    ];
}