<?php

namespace phycom\backend\assets;

use phycom\common\assets\ActiveFormHelperAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\bootstrap\BootstrapAsset;

/**
 * Handles multiple forms submitting by js
 */
class FormSubmitAsset extends AssetBundle
{
	public $sourcePath = '@phycom/backend/assets/form';
	public $js = [
		'submit-btn.js'
	];
	public $publishOptions = ['except' => ['*.less']];
	public $depends = [
	    AlertMessageAsset::class,
        ActiveFormHelperAsset::class,
	    JqueryAsset::class,
        BootstrapAsset::class
	];
}