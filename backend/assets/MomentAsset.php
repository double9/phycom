<?php

namespace phycom\backend\assets;

use yii\web\AssetBundle;

/**
 * moment js asset bundle.
 */
class MomentAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower-asset/moment/min';
    public $css = [];
    public $js = [
        'moment-with-locales.min.js',
    ];
    public $publishOptions = ['except' => ['*.less']];
    public $depends = [];
}