<?php

namespace phycom\backend\assets;

use yii\web\AssetBundle;
use yii;

/**
 * AttributeGrid widget asset bundle.
 */
class FileInputAsset extends AssetBundle
{
    public $sourcePath = '@phycom/backend/assets/file-input';
    public $css = [
//        'main.css'
    ];
    public $js = [
        'main.js'
    ];
    public $publishOptions = ['except' => ['*.less']];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset'
    ];
}