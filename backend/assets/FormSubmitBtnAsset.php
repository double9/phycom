<?php

namespace phycom\backend\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class FormSubmitBtnAsset
 * @package phycom\backend\assets
 */
class FormSubmitBtnAsset extends AssetBundle
{
	public $sourcePath = '@phycom/backend/assets/form';
	public $js = [
		'btn.js'
	];
	public $publishOptions = ['except' => ['*.less']];
	public $depends = [
	    FormActionsAsset::class,
        JqueryAsset::class
	];
}
