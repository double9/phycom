<?php

namespace phycom\backend\assets;

use yii\web\AssetBundle;

/**
 * Tree grid widget asset bundle.
 */
class TreeAsset extends AssetBundle
{
	public $sourcePath = '@phycom/backend/assets/tree';
	public $css = [
		'main.css'
	];
	public $publishOptions = ['except' => ['*.less']];
//	public $js = [
//		'main.js'
//	];
//	public $depends = [
//		'yii\grid\GridViewAsset'
//	];
}
