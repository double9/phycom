"use strict";

class DailySalesChart extends ChartWidget {

    constructor(props) {
        super(props);
        this.label = props.label || 'Money received';
    }

    parseData(data) {
        let labels = Array.from(data, x => x.date);
        let incomeReceived = Array.from(data, x => x.revenue);

        return {
            labels: labels,
            datasets: [
                {
                    label: this.label,
                    data: incomeReceived,
                    backgroundColor: 'rgba(255, 206, 86, 0.2)',
                    borderColor: 'rgba(255, 206, 86, 1)',
                    lineTension: 0.2,
                    fill: true,
                    borderWidth: 1
                }
            ]
        };
    }

}