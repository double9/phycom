"use strict";

class SalesIndicators extends StatisticsReport {

    constructor(conf) {
        super(conf);
        this.$context = $(conf.context);
        this.emptyValue = conf.emptyValue || '-';
    }

    init(data) {

        this.d = _(data).map(item => Number(item.revenue)).value();     // daily date
        this.w = this.groupData(data, 'isoWeek');                       // weekly data
        this.m = this.groupData(data, 'month');                         // monthly data

        this.totalRevenue = this.calculateTotalRevenue(this.d);

        this.lastDayRevenue = this.d.length ? this.d[this.d.length - 1] : null;
        this.prevDayRevenue = this.d.length > 1 ? this.d[this.d.length - 2] : null;
        this.avgDailyRevenue = this.calculateAvgRevenue(this.d);
        this.dailyGrowth = this.calculateGrowth(this.d);

        this.lastWeekRevenue = this.w.length ? this.w[this.w.length - 1] : null;
        this.prevWeekRevenue = this.w.length > 1 ? this.w[this.w.length - 2] : null;
        this.avgWeeklyRevenue = this.calculateAvgRevenue(this.w);
        this.weeklyGrowth = this.calculateGrowth(this.w);

        this.lastMonthRevenue = this.m.length ? this.m[this.m.length - 1] : null;
        this.prevMonthRevenue = this.m.length > 1 ? this.m[this.m.length - 2] : null;
        this.avgMonthlyRevenue = this.calculateAvgRevenue(this.m);
        this.monthlyGrowth = this.calculateGrowth(this.m);

        this.attributes = {
            totalRevenue:      'currency',
            lastDayRevenue:    'currency',
            prevDayRevenue:    'currency',
            avgDailyRevenue:   'currency',
            dailyGrowth:       'percent',
            lastWeekRevenue:   'currency',
            prevWeekRevenue:   'currency',
            avgWeeklyRevenue:  'currency',
            weeklyGrowth:      'percent',
            lastMonthRevenue:  'currency',
            prevMonthRevenue:  'currency',
            avgMonthlyRevenue: 'currency',
            monthlyGrowth:     'percent'
        };
        console.log(this);
        this.writeValues();
    }

    groupData(rawData, startOf) {
        return _(rawData).groupBy((result) => moment(result.date, 'YYYY-MM-DD').startOf(startOf)).map(function(items, date) {
            return items.reduce((accumulator, currentValue) => accumulator + Number(currentValue.revenue), 0);
        }).value();
    }

    calculateAvgRevenue(data) {
        if (!data.length) {
            return 0;
        }
        return data.reduce((accumulator, currentValue) => accumulator + currentValue, 0) / data.length;
    }

    calculateGrowth(data) {
        if (data.length < 2) {
            return null;
        }
        let last = data[data.length - 1];
        let prev = data[data.length - 2];
        let increase = last - prev;

        return increase / prev;
    }

    calculateTotalRevenue(data) {
        return data.reduce((accumulator, currentValue) => accumulator + currentValue, 0);
    }


    writeValues() {
        if (this.$context.length) {
            for (let attribute in this.attributes) {
                let $target = this.$context.find('[data-attribute="' + attribute + '"] .value');
                if ($target.length) {

                    let value = this[attribute];

                    if (value === null) {
                        $target.html(this.emptyValue);
                        continue;
                    }

                    let formatterName = 'as' + this.constructor.capitalize(this.attributes[attribute]);
                    let formattedValue = this[formatterName](this[attribute]);

                    $target.html(formattedValue);
                }
            }
        }
    }
}

