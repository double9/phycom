"use strict";

class StatisticsDataStore {

    static get instance() {
        if (!this.__instance) {
            this.__instance = new this();
        }
        return this.__instance;
    }

    constructor() {
        this._data = [];
    }

    fetchData(url) {

        let key = this.findKeyByUrl(url);
        if (key > -1) {
            if (this._data[key][1] === null) {
                return new Promise((resolve) => {
                    let checkData = () => {
                        let timer = setTimeout(() => {
                            if (this._data[key][1] === null) {
                                checkData();
                            } else {
                                resolve(this._data[key][1]);
                            }
                            timer && clearTimeout(timer);
                        }, 300);
                    }
                });
            }
            return new Promise((resolve) => resolve(this._data[key][1]));
        }
        key = (this._data.push([url, null])) - 1;

        return fetch(url.href, {
            headers: {'Content-Type': 'application/json', 'X-Requested-With': 'XMLHttpRequest'},
        })
        .then(response => response.json())
        .then(json => {
            this._data[key][1] = json;
            return json;
        });
    }

    addListener() {

    }

    findKeyByUrl(url) {
        for (let i=0; i<this._data.length; i++) {
            if (this._data[i][0] === url.href) {
                return i;
            }
        }
        return -1;
    }

}
StatisticsDataStore.__instance = null;