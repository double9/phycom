"use strict";

class StatisticsReport {

    static create(conf) {
        let instance = new this(conf);
            instance.fetchData().then(data => instance.init(data));
        return instance;
    }

    constructor(conf) {
        this.url = conf.url;
        this.from = ('from' in conf && conf.from) ? moment(conf.from, 'YYYY-MM-DD') : null;
        this.to = ('to' in conf && conf.to) ? moment(conf.to, 'YYYY-MM-DD') : null;
    }

    fetchData() {
        let url = new URL(this.url);

        if (this.from) {
            url.searchParams.set('from', this.from.format('YYYY-MM-DD'));
        }
        if (this.to) {
            url.searchParams.set('to', this.to.format('YYYY-MM-DD'));
        }
        return StatisticsDataStore.instance.fetchData(url);
    }

    init(data) {

    }

    /**
     * Capitalize the first letter in a string
     * @param {string} str
     * @returns {string}
     */
    static capitalize(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    /**
     * @param {number} value
     * @param {int} decimals
     * @returns {number}
     */
    static round(value, decimals = 5) {
        return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
    }

    /**
     * @param {number} value
     * @param {int} decimals
     * @param {string} symbol
     * @returns {number}
     */
    static percent(value, decimals = 2) {
        return Number((value * 100).toFixed(decimals));
    }

    /**
     * @param value
     * @param {string} delimiter
     * @return {string}
     */
    asCurrency(value, delimiter = ' ') {
        return this.constructor.round(value, 2).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1" + delimiter);
    }

    /**
     * @param value
     * @return {string}
     */
    asPercent(value) {
        return this.constructor.percent(value) + ' %';
    }

    /**
     * @param value
     * @param decimalPlaces
     * @return {number}
     */
    asDecimal(value, decimalPlaces) {
        if (_.isObject(decimalPlaces)) {
            decimalPlaces = 2;
        }
        return Number(this.constructor.round(value, decimalPlaces).toFixed(decimalPlaces));
    }
}