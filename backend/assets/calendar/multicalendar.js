var multiCalendarDatePicker = (function ($) {

    var MultiCalendarDatePicker = function (el, formField, months) {
        months = months || 1;

        this.$el = $(el);

        if (!this.$el.length) {
            throw 'Element ' + el + ' not found';
        }

        this.$field = $(formField);

        if (!this.$field.length) {
            throw 'Form field ' + formField + ' not found';
        }

        this.$form = this.$field.closest('form');

        if (!this.$form.length) {
            throw 'Form ' + form + ' not found';
        }

        this.el = this.$el[0] || null;
        this.$el.addClass('multi-calendar');
        this.calendars = {};
        this.startDate = moment();

        for (var i = 1; i <= months; i++) {
            var $el = $('<div id="' + this.el.id + '-' + i + '" class="calendar col-lg-4 col-md-6"></div>');
            this.$el.append($el);
            this.calendars[i] = this.createCalendar(i, $el);
        }

        this.init();
    };

    MultiCalendarDatePicker.prototype.init = function () {
        var self = this;
        this.$form.on('submit', function(e){
            self.$field.val(JSON.stringify(self.exportDates()));
        });
    };

    MultiCalendarDatePicker.prototype.exportDates = function () {
        var dates = [];
        var events = this.calendars[1].clientEvents();
        for (var i=0; i<events.length; i++) {
            if (events[i].end) {
                var datesBetween = this.getDatesBetween(events[i].start, events[i].end);
                for (var n=0; n < datesBetween.length; n++) {
                    dates.push(datesBetween[n].format('YYYY-MM-DD'));
                }
            } else {
                dates.push(events[i].start.format('YYYY-MM-DD'));
            }
        }
        console.log(dates);
        return dates;
    };

    MultiCalendarDatePicker.prototype.getDatesBetween = function (start, end) {
        var dates = [],
            currentDate = start.clone();
        while (+currentDate < +end) {
            dates.push(currentDate.clone());
            currentDate.add(1, 'day');
        }
        return dates;
    };


    MultiCalendarDatePicker.prototype.importDates = function () {
        var dates = JSON.parse(this.$field.val());
        var events = [];

        for (var i=0; i<dates.length; i++) {
            events.push({
                id: '_' + Math.random().toString(36).substr(2, 9),
                title: '',
                start: moment(dates[i]),
                end: moment(dates[i]).add(1, 'day'),
                rendering: 'background'
            });
        }
        return events;
    };

    MultiCalendarDatePicker.prototype.createEvent = function (start, end) {
        end = end || null;
        return {id: '_' + Math.random().toString(36).substr(2, 9), title: '', start: start, end: end, rendering: 'background'};
    };

    MultiCalendarDatePicker.prototype.getCommonOptions = function () {
        var self = this;
        return {
            header: {
                left: 'title',
                center: '',
                right: ''
            },
            height: 'auto',
            selectable: true,
            eventOverlap: false,
            // selectOverlap: false,
            allDayDefault: true,
            selectHelper: true,
            editable: true,
            events: this.importDates(),
            showNonCurrentDates: false,
            select: function(start, end, e) {

                var today = moment(0, 'HH');
                for (var k in self.calendars) {

                    if (self.calendars.hasOwnProperty(k)) {
                        var cal = self.calendars[k],
                            allEvents = cal.clientEvents(),
                            events = $.grep(allEvents, function (v) {
                                return (+start >= +v.start && +end <= +v.end) || (+v.start >= +start && +v.end <= +end);
                            });

                        if (events.length) {
                            for (var i = 0; i < events.length; i++) {

                                if (events[i].start >= today) {
                                    cal.removeEvents(events[i].id);
                                }
                            }
                        } else {
                            if (start >= today) {
                                cal.renderEvent(self.createEvent(start, end), true);
                            }
                        }
                    }
                }
                this.calendar.unselect();
            },
        };
    };


    MultiCalendarDatePicker.prototype.createCalendar = function (i, $el) {
        var self = this,
            options = {},
            commonOptions = this.getCommonOptions();

        if (i === 1) {
            options = $.extend(commonOptions, {
                header: {
                    right: 'prev,next today'
                },
                defaultDate: self.startDate,
                viewRender: function (view, element) {
                    var startDate = view.intervalStart;
                    for (var key in self.calendars) {
                        if (self.calendars.hasOwnProperty(key) && key > 1) {
                            var date = moment(startDate).add('months', key - 1);
                            self.calendars[key].gotoDate(date);
                        }
                    }
                }
            });
        } else {
            var date = this.startDate.clone();
                date.startOf('month').add(i - 1, 'month');
                options = $.extend(commonOptions, {defaultDate: date});
        }
        $el.fullCalendar(options);
        return $el.data('fullCalendar');
    };


    return {
        create: function (el, formField, months) {
           return new MultiCalendarDatePicker(el, formField, months);
        }
    }
})(jQuery);