"use strict";

jQuery(function($){
    var $parent = $('.file-input-form');

    $('.attachments-modal-container').on('click', '.btn-save', function (e) {

        var $submitBtn = $(this),
            $modal = $submitBtn.closest('.modal'),
            $msgContainer = $modal.find('.messages'),
            $form = $modal.find('form'),
            url = $form.attr('action'),
            params = $form.serialize();

        e.preventDefault();

        $submitBtn.button('loading');
        $.post(url, params, function (data) {
            $msgContainer.html(data.msg);
            $submitBtn.button('reset');
            window.setTimeout(function () {
                $msgContainer.hide(300, function() {
                    $msgContainer.html('').show();
                })
            }, 2000);
        });
    });


    $('.attachments-modal-container').on('click', '.btn-close', function (e) {
        e.preventDefault();
        $(this).closest('.modal').modal('hide');
    });


    $parent.on('click', '.attachment-item.initial-item .item-content', function (e) {
        var fileId = $(this).closest('.attachment-item.initial-item').data('file-id');
        var $modal = $('#attachment_' + fileId + '_modal');
        $modal.modal({show: true});
    });

    $parent.on('filesorted', 'input.attachment-form-input', function(event, params) {
        var file = params.stack[params.newIndex];
        var $el = $parent.find('.file-sortable .attachment-item[data-file-id="' + file.key + '"]');
        if (!$el.length) {
            throw 'File ' + file.key + ' not found';
        }
        $.post($el.data('update-url'), {order: parseInt(params.newIndex)});
    });
});