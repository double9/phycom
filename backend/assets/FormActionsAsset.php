<?php

namespace phycom\backend\assets;

use yii\web\AssetBundle;

/**
 * Class FormActionsAsset
 * @package phycom\backend\assets
 */
class FormActionsAsset extends AssetBundle
{
	public $sourcePath = '@phycom/backend/assets/form';
	public $js = [
		'actions.js'
	];
}
