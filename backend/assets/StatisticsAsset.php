<?php

namespace phycom\backend\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\bootstrap\BootstrapAsset;

/**
 * Statistics asset bundle
 */
class StatisticsAsset extends AssetBundle
{
    public $sourcePath = '@phycom/backend/assets/statistics';
    public $js = [
        'data-store.js',
        'report.js',
        'chart.js',
        'chart-orders.js',
        'chart-sales.js',
        'main.js'
    ];
    public $css = [
        'styles.css'
    ];
    public $publishOptions = ['except' => ['*.less']];
    public $depends = [
        JqueryAsset::class,
        BootstrapAsset::class,
        ChartJsAsset::class,
        MomentAsset::class,
        LodashAsset::class
    ];
}