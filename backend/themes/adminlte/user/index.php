<?php

use phycom\common\models\attributes\UserStatus;
use phycom\common\models\attributes\UserType;
use phycom\backend\widgets\Box;
use phycom\backend\widgets\DataGrid;
use phycom\common\helpers\Filter;
use phycom\common\helpers\f;

use yii\helpers\Url;

/**
 * @var $this yii\web\View
 * @var \phycom\backend\models\SearchUser $model
 * @var \yii\data\ActiveDataProvider $dataProvider
 */
$this->title = Yii::t('backend/user', 'Users');
$this->params['breadcrumbs'][] = $this->title
?>
<div class="row">
    <div class="col-md-12">
		<?= Box::begin([
		        'options' => ['class' => 'box box-default'],
		        'showHeader' => false,
			    'bodyOptions' => ['class' => 'box-body']
            ]);
		?>

        <?= DataGrid::widget([
            'id'           => 'user-grid',
            'dataProvider' => $dataProvider,
            'filterModel'  => $model,
            'rowOptions'   => function ($model) {
                return Yii::$app->user->can('view_user') || (Yii::$app->user->id === $model->id && Yii::$app->user->can('view_self', $model->id)) ? [
                    'class'    => 'row-link',
                    'data-url' => Url::toRoute(['user/profile', 'id' => $model->id])
                ] : [];
            },
            'actions'      => [
                [
                    'label'   => '<span class="fas fa-user-plus"></span>&nbsp;&nbsp;' . Yii::t('backend/user', 'Add backend user'),
                    'url'     => ['user/invite'],
                    'options' => ['class' => 'btn btn-flat btn-success'],
                    'visible' => Yii::$app->user->can('invite_backend_users')
                ]
            ],
            'columns'      => [
                [
                    'attribute'      => 'id',
                    'format'         => 'integer',
                    'options'        => ['width' => '100'],
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'filterOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'headerOptions'  => ['class' => 'hidden-sm hidden-xs']
                ],
//	                [
//		                'attribute' => 'username',
//		                'format' => 'text',
//		                'options' => ['width' => '200']
//	                ],
                [
                    'attribute' => 'email',
                    'format'    => 'text',
                ],
                [
                    'attribute'      => 'name',
                    'format'         => 'raw',
                    'value'          => function ($model) {
                        return f::titleCase($model->name) . ($model->id === Yii::$app->user->id ? '&nbsp;&nbsp;<i class="fas fa-user"></i>' : '');
                    },
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'filterOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'headerOptions'  => ['class' => 'hidden-sm hidden-xs']
                ],
                [
                    'attribute'      => 'company_name',
                    'format'         => 'text',
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'filterOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'headerOptions'  => ['class' => 'hidden-sm hidden-xs']
                ],
                [
                    'attribute'      => 'type',
                    'filter'         => Filter::dropDown($model, 'type', UserType::displayValues()),
                    'format'         => 'raw',
                    'value'          => function ($model) {
                        return '<span class="label ' . $model->type->labelClass . '">' . $model->type->label . '</span>';
                    },
                    'options'        => ['width' => '160'],
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'filterOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'headerOptions'  => ['class' => 'hidden-sm hidden-xs']
                ],
                [
                    'attribute' => 'status',
                    'filter'    => Filter::dropDown($model, 'status', UserStatus::displayValues([UserStatus::DELETED])),
                    'format'    => 'raw',
                    'value'     => function ($model) {
                        return '<span class="label ' . $model->status->labelClass . '">' . $model->status->label . '</span>';
                    },
                    'options'   => ['width' => '160']
                ],
                [
                    'attribute'      => 'created_at',
                    'filter'         => Filter::daterangepicker($model, 'createdFrom', 'createdTo'),
                    'format'         => 'datetime',
                    'options'        => ['width' => '220'],
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'filterOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'headerOptions'  => ['class' => 'hidden-sm hidden-xs']
                ],
                [
                    'attribute'      => 'updated_at',
                    'filter'         => Filter::daterangepicker($model, 'updatedFrom', 'updatedTo'),
                    'format'         => 'datetime',
                    'options'        => ['width' => '220'],
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'filterOptions'  => ['class' => 'hidden-sm hidden-xs'],
                    'headerOptions'  => ['class' => 'hidden-sm hidden-xs']
                ],
                [
                    'label'          => false,
                    'format'         => 'raw',
                    'content'        => function ($model) {

                        if (Yii::$app->user->id === $model->id || $model->status->is(UserStatus::BLOCKED)) {
                            return '';
                        }

                        return $this->render('/partials/delete-dropdown', [
                            'label'        => '<i class="fas fa-ban"></i>',
                            'confirmLabel' => Yii::t('backend/user', 'Block'),
                            'url'          => ['user/block', 'id' => $model->id],
                            'options'      => ['class' => 'toggle-hover'],
                            'side'         => 'right'
                        ]);
                    },
                    'options'        => ['width' => 40],
                    'contentOptions' => ['style' => 'padding: 2px 0 0 0;', 'class' => 'clickable']
                ]
            ]
            ]);
		?>

		<?= Box::end(); ?>
	</div>
</div>
