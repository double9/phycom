<?php

use phycom\backend\widgets\AjaxGrid;
use yii\helpers\Html;
/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 */
?>

<?= AjaxGrid::widget([
	'id' => 'user-invitations-grid',
	'dataProvider' => $dataProvider,
	'route' => 'user/pending-invitations',
	'columns' => [
		[
			'attribute' => 'name',
			'enableSorting' => false,
			'format' => 'titleCase',
		],
		[
			'attribute' => 'email',
			'enableSorting' => false,
			'format' => 'text',
		],
		[
			'attribute' => 'created_at',
			'format' => 'datetime',
			'enableSorting' => false,
			'options' => ['width' => '180']
		],
		[
			'attribute' => 'tokenCount',
			'format' => 'integer',
			'enableSorting' => false,
			'content' => function ($model) {
				return '<span class="badge default">' . $model->tokenCount . '</span>';
			},
			'options' => ['width' => '100']
		],
		[
			'label' => '',
			'content' => function($model) {
				$resendBtn = Html::a(
					'<span class="fas fa-paper-plane"></span>&nbsp;&nbsp;' . Yii::t('backend/user', 'Resend invitation'),
					['user/resend-invitation', 'id' => $model->id],
					['class' => 'btn btn-flat btn-sm btn-default']
				);
                $removeBtn = $this->render('/partials/delete-dropdown', [
                    'label' => '<span class="far fa-trash-alt"></span>',
                    'url' => ['user/remove-invitation', 'id' => $model->id],
                    'options' => ['class' => 'toggle-hover pull-right'],
                    'buttonOptions' => ['class' => 'btn-sm', 'style' => 'padding-top: 4px; padding-bottom: 3px;'],
                    'side' => 'right'
                ]);
				return $resendBtn . $removeBtn;
			},
			'options' => ['width' => '200'],
			'contentOptions' => ['class' => 'actions ajax-actions clearfix']
		],
	]
]);
?>