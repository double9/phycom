<?php

use phycom\backend\widgets\AjaxGrid;
/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 */
?>

<?= AjaxGrid::widget([
	'id' => 'user-activity-grid',
	'dataProvider' => $dataProvider,
	'route' => 'user/activity',
	'columns' => [
		[
			'attribute' => 'created_at',
			'format' => 'datetime',
			'enableSorting' => false,
			'options' => ['width' => '180']
		],
		[
			'attribute' => 'action',
			'enableSorting' => false,
			'format' => 'text',
			'contentOptions' => ['class' => 'text-clipped']
		],
		[
			'attribute' => 'ip',
			'format' => 'text',
			'enableSorting' => false,
			'options' => ['width' => '120']
		],
//	                        [
//		                        'attribute' => 'user_agent',
//		                        'format' => 'text',
//		                        'enableSorting' => false
//	                        ],
	]
]);
?>
