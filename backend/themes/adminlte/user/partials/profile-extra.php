<?php
/**
 * @var $this yii\web\View
 * @var $model \phycom\common\models\attributes\UserMeta;
 */

use phycom\backend\widgets\ActiveForm;
?>

<div class="box box-primary">
    <?php $form = ActiveForm::begin(['id' => 'profile-extra']); ?>

    <div class="box-body">

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'about')->editor(['options' => ['rows' => 4]]); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'profession')->textInput(); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'facebook')->textInput(); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'google')->textInput(); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'linkedIn')->textInput(); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'twitter')->textInput(); ?>
            </div>
        </div>

    </div>

    <div class="box-footer">
        <button type="submit" class="btn btn-primary btn-flat"><?= Yii::t('backend/user', 'Update'); ?></button>
    </div>

    <?php ActiveForm::end(); ?>
</div>
