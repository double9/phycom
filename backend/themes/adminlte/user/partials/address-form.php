<?php
/**
 * @var $this yii\web\View
 * @var $model phycom\backend\models\UserAddressForm
 */

use phycom\backend\widgets\ActiveForm;
use yii\helpers\Html;
?>


<?php $form = ActiveForm::begin(['id' => 'address-form']); ?>

<div class="row">
	<div class="col-md-6">
		<?= $form->field($model, 'country')->dropDownList($model->countries); ?>
		<?= $form->field($model, 'district')->textInput(); ?>
		<?= $form->field($model, 'street')->textInput(); ?>
	</div>

	<div class="col-md-6">
		<?= $form->field($model, 'province')->textInput(); ?>
		<?= $form->field($model, 'city')->textInput(); ?>
		<?= $form->field($model, 'postcode')->textInput(); ?>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<?= Html::submitButton(Yii::t('backend/user', 'Submit'), ['class' => 'btn btn-flat btn-primary']); ?>
	</div>
</div>

<?php ActiveForm::end(); ?>
