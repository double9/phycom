<?php
/**
 * @var $this yii\web\View
 * @var \phycom\common\models\User $user
 */
use phycom\common\helpers\f;
use yii\helpers\Html;
use phycom\backend\widgets\Modal;
?>


<div class="box box-default">

	<div class="box-header with-border">
		<h3 class="box-title"><?= Yii::t('backend/user', 'Addresses'); ?></h3>
	</div>

	<div class="box-body no-padding">
        <table class="table table-striped">
            <tbody>
                <?php foreach ($user->addresses as $address): ?>
                    <tr>
                        <td width="60"><?= f::integer($address->id); ?></td>
                        <td><?= f::address($address); ?></td>
                        <td><span class="label label-default"><?= f::text($address->type->label); ?></span></td>
                        <td width="200"><?= f::datetime($address->created_at); ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
	</div>

	<div class="box-footer with-border">

        <?= Modal::widget([
                'id' => 'address-modal',
                'title' => Yii::t('backend/user','Add address'),
                'content' => $this->render('address-form', ['model' => $model])
            ]);
        ?>
        <?= Html::a(
	        '<span class="fas fa-map-marker-alt"></span>&nbsp;&nbsp;' . Yii::t('backend/user', 'Create new'),
	        ['user/create-address', 'id' => $user->id],
	        ['class' => 'btn btn-flat btn-primary', 'data-toggle' => 'modal', 'data-target' => '#address-modal']
        ); ?>
	</div>
</div>
