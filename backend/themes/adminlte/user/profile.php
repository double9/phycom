<?php

use phycom\backend\widgets\Box;
/**
 * @var $this yii\web\View
 * @var $profileForm phycom\backend\models\UserProfileForm
 * @var $profileImageForm phycom\backend\models\UserProfileImageForm
 * @var $addressForm \phycom\backend\models\UserAddressForm
 * @var \phycom\common\models\User $user
 * @var \phycom\backend\models\SearchUserActivity $activity
 */
$this->title = Yii::$app->user->id === $user->id ? Yii::t('backend/user', 'My profile') : Yii::t('backend/user', 'User {user_id} profile', ['user_id' => $user->id]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/user', 'Users'), 'url' => ['user/index']];
$this->params['breadcrumbs'][] = $this->title
?>
<div class="row">

    <div class="col-md-8">

        <div class="row">
            <div class="col-md-5">
		        <?= $this->render('partials/profile-overview', ['user' => $user, 'model' => $profileImageForm]); ?>
            </div>
            <div class="col-md-7">
		        <?= $this->render('partials/profile-form', ['user' => $user, 'model' => $profileForm]); ?>
            </div>
        </div>

        <?php if ($user->type->isAdmin): ?>
            <div class="row">
                <div class="col-md-12">
                    <?= $this->render('partials/profile-extra', ['user' => $user, 'model' => $user->meta]); ?>
                </div>
            </div>
        <?php endif; ?>


        <div class="row">
            <div class="col-md-12">
		        <?= $this->render('partials/address-box', ['user' => $user, 'model' => $addressForm]); ?>
            </div>
        </div>

        <?php if(Yii::$app->user->can('search_user_activity')): ?>
            <div class="row">
                <div class="col-md-12">

                    <?= Box::begin(['title' => Yii::t('backend/user', 'User activity log')]); ?>
                    <?= $this->render('partials/user-activity', ['dataProvider' => $activity->search([])]); ?>
                    <?= Box::end(); ?>

                </div>
            </div>
        <?php endif; ?>
    </div>

    <div class="col-md-4">
	    <?= $this->render('partials/notes', ['user' => $user]); ?>
	    <?= $this->render('/order/widget', ['user' => $user, 'title' => Yii::t('backend/user', 'Order history')]); ?>
    </div>
</div>
