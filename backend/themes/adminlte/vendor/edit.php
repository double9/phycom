<?php

/**
 * @var $this yii\web\View
 * @var \phycom\backend\models\VendorForm $model
 */

use phycom\backend\widgets\Modal;
use phycom\backend\widgets\ActiveForm;

use phycom\common\helpers\f;
use phycom\common\widgets\Map;

use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use yii\helpers\Html;

$fieldOpt = ['template' => '{input}{error}', 'options' => ['class' => 'no-margin']];
$fieldOptMd = ArrayHelper::merge($fieldOpt, ['options' => ['style' => 'max-width: 300px;']]);
$fieldOptSm = ArrayHelper::merge($fieldOpt, ['options' => ['style' => 'max-width: 200px;']]);

$reflect = new \ReflectionClass($model->addressForm);
$addressModelName = $reflect->getShortName();

\phycom\backend\assets\ShopAddressFormAsset::register($this);
$this->registerJs('addressForm.init("' . $addressModelName . '", "#vendorform-address");');

?>


<div class="row">
    <div class="col-md-8">

        <?php $form = ActiveForm::begin(['id' => 'vendor-form']); ?>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [

                [
                    'attribute' => 'name',
                    'format' => 'raw',
                    'value' => $form->field($model, 'name', $fieldOpt)->label(false)->textInput(['class' => 'form-control no-margin'])
                ],
                [
                    'attribute' => 'legalName',
                    'format' => 'raw',
                    'value' => $form->field($model, 'legalName', $fieldOpt)->label(false)->textInput(['class' => 'form-control no-margin'])
                ],
                [
                    'attribute' => 'regNumber',
                    'format' => 'raw',
                    'value' => $form->field($model, 'regNumber', $fieldOpt)->label(false)->textInput(['class' => 'form-control no-margin'])
                ],
                [
                    'attribute' => 'vatNumber',
                    'format' => 'raw',
                    'value' => $form->field($model, 'vatNumber', $fieldOpt)->label(false)->textInput(['class' => 'form-control no-margin'])
                ],
                [
                    'attribute' => 'email',
                    'format' => 'raw',
                    'value' => $form->field($model, 'email', $fieldOpt)->label(false)->textInput(['class' => 'form-control no-margin'])
                ],
                [
                    'attribute' => 'address',
                    'format' => 'raw',
                    'value' => function ($model) use ($form, $fieldOpt) {
                        return $form->field($model, 'address', ['options' => ['class' => 'no-margin']])->addressField('vendor-address-modal');
                    }
                ],
                [
                    'attribute' => 'phone',
                    'format' => 'raw',
                    'value' => $form->field($model, 'phone', $fieldOptMd)->label(false)->phoneInput2()
                ],
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => $form->field($model, 'status', $fieldOptSm)->dropDownList($model->statuses)
                ],
                [
                    'label' => $model->vendor->getAttributeLabel('created_at'),
                    'format' => 'datetime',
                    'value' => $model->vendor->created_at,
                    'visible' => !$model->vendor->isNewRecord
                ],
                [
                    'label' => $model->vendor->getAttributeLabel('updated_at'),
                    'format' => 'datetime',
                    'value' => $model->vendor->updated_at,
                    'visible' => !$model->vendor->isNewRecord
                ]
            ],
        ]);
        ?>
        <?php ActiveForm::end(); ?>


        <?= Modal::widget([
            'id' => 'vendor-address-modal',
            'title' => $model->vendor->address ? Yii::t('backend/vendor','Update address') : Yii::t('backend/vendor','Add address'),
            'content' => $this->render('/partials/address-form', ['model' => $model->addressForm])
        ]);
        ?>

    </div>

    <div class="col-md-4">
        <?php if (!$model->vendor->isNewRecord): ?>
            <?= Map::widget(['address' => $model->vendor->address]); ?>
        <?php endif; ?>
    </div>

</div>

