<?php

/**
 * @var $this yii\web\View
 * @var \phycom\common\models\PartnerContract $model
 */

use phycom\backend\helpers\Html;
use phycom\backend\widgets\Box;
use phycom\backend\widgets\Modal;
use phycom\backend\widgets\ActiveForm;

use phycom\common\models\PartnerContract;
use phycom\common\helpers\Url;
use phycom\common\helpers\f;


$this->title = $model->isNewRecord ? Yii::t('backend/gdpr', 'Add New Contract') : Yii::t('backend/gdpr', 'Contract # {id}', ['id' => $model->id]);


$this->params['breadcrumbs'][] = Yii::t('backend/main', 'Other');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/gdpr', 'Partner contracts'), 'url' => ['/gdpr']];
$this->params['breadcrumbs'][] = $this->title;

$addressForm = new \phycom\backend\models\AddressFieldForm();
$reflect = new \ReflectionClass($addressForm);
$addressModelName = $reflect->getShortName();

\phycom\backend\assets\ShopAddressFormAsset::register($this);
$this->registerJs(';addressForm.init("' . $addressModelName . '", "#partnercontract-address");');

?>

<?php $form = ActiveForm::begin(); ?>

<div class="row">

    <div class="col-lg-8 col-md-12">

        <?= Box::begin([
            'showHeader' => false,
            'options' => ['class' => 'box box-default'],
            'bodyOptions' => ['class' => 'box-body']
        ]);
        ?>

        <div class="row">
            <div class="col-md-8">

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'company_name')->textInput([
                            'maxlength' => true,
                            'readonly' => Yii::$app->controller->action->id == 'update' ? true : false
                        ]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'address')->addressField('contract-address-modal'); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'contact_email')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'contract_start')->datePicker(); ?>
                    </div>

                    <div class="col-md-6">
                        <?= $form->field($model, 'contract_end')->datePicker(); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'max_consent_days')->textInput() ?>
                    </div>
                    <div class="col-md-6">
                        <div class="row">&nbsp;</div>
                        <?= $form->field($model, 'mandatory')->checkbox() ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'category')->dropDownList(
                            PartnerContract::getCategories(),
                            ['prompt' => Yii::t('backend/gdpr', 'Select')]
                        ); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'consent_required_on')->dropDownList(
                            PartnerContract::getRequiredForFields(),
                            ['prompt' => Yii::t('backend/gdpr', 'Select')]
                        ); ?>
                    </div>
                </div>

            </div>
        </div>

        <?= Box::end(); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-8 col-md-12">
        <?= Box::begin([
            'showHeader' => false,
            'options' => ['class' => 'box box-default'],
            'bodyOptions' => ['class' => 'box-body']
        ]);
        ?>
        <div class="clearfix">
            <div class="pull-left"><?= $this->render('/partials/back-btn', ['size' => 'lg']); ?></div>
            <div class="pull-right">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('backend/gdpr', 'Create') : Yii::t('backend/gdpr', 'Update'),
                    [
                        'class' => $model->isNewRecord ? 'btn btn-success btn-lg btn-flat' : 'btn btn-primary btn-lg btn-flat'
                    ]
                ) ?>
            </div>
        </div>
        <?= Box::end(); ?>
    </div>
</div>


<?php $form::end(); ?>


<?= Modal::widget([
    'id' => 'contract-address-modal',
    'title' => $model->isNewRecord ? Yii::t('backend/gdpr','Add address') : Yii::t('backend/gdpr','Update address'),
    'content' => $this->render('/partials/address-form', ['model' => $addressForm])
]);
?>

