<?php

use phycom\common\models\attributes\DiscountRuleStatus;
use phycom\backend\widgets\Box;
use phycom\backend\widgets\DataGrid;
use phycom\common\helpers\Filter;
use yii\helpers\Url;
use phycom\common\helpers\f;

/**
 * @var $this yii\web\View
 * @var \phycom\backend\models\SearchDiscountRule $model
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('backend/marketing', 'Client cards');
$this->params['breadcrumbs'][] = Yii::t('backend/marketing', 'Marketing');
$this->params['breadcrumbs'][] = $this->title
?>
<div class="row">
	<div class="col-md-12">
		<?= Box::begin([
			'options' => ['class' => 'box box-default'],
			'showHeader' => false,
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

		<?= DataGrid::widget([
			'id' => 'user-grid',
			'dataProvider' => $dataProvider,
			'filterModel' => $model,
            'bulkEditForm' => 'bulk-edit',
			'rowOptions' => function ($model) {
				return [
					'class' => 'row-link',
					'data-url' => Url::toRoute(['client-card/edit','id' => $model->id])
				];
			},
            'actions' => [
                [
                    'label' => '<span class="fas fa-plus"></span>&nbsp;&nbsp;' . Yii::t('backend/user', 'Add new client card'),
                    'url' => ['client-card/add'],
                    'options' => ['class' => 'btn btn-flat btn-success']
                ]
            ],
			'columns' => [
				[
					'attribute' => 'code',
					'format' => 'text',
					'options' => ['width' => '200']
				],
                [
	                'attribute' => 'name',
	                'format' => 'text'
                ],
				[
					'attribute' => 'discount_rate',
					'format' => 'percent',
					'options' => ['width' => '200']
				],
				[
					'attribute' => 'birthday_discount_rate',
					'format' => 'percent',
					'options' => ['width' => '220']
				],
				[
					'attribute' => 'birthday',
					'filter' => Filter::daterangepicker($model, 'birthdayFrom', 'birthdayTo'),
                    'format' => 'date',
					'options' => ['width' => '220']
				],
				[
					'attribute' => 'used',
					'format' => 'integer',
					'options' => ['width' => '120']
				],
				[
					'attribute' => 'status',
					'filter' => Filter::dropDown($model, 'status', DiscountRuleStatus::displayValues()),
					'format' => 'raw',
					'value' => function ($model) {
						return '<span class="label '.$model->status->labelClass.'">' . $model->status->label . '</span>';
					},
					'options' => ['width' => '120']
				],
				[
					'attribute' => 'created_at',
					'filter' => Filter::daterangepicker($model, 'createdFrom', 'createdTo'),
					'format' => 'datetime',
					'options' => ['width' => '220']
				]
			]
		]);
		?>

		<?= Box::end(); ?>
	</div>
</div>
