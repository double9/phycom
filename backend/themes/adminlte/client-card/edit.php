<?php

/**
 * @var $this yii\web\View
 * @var \phycom\backend\models\DiscountRuleForm $model
 */

use yii\widgets\DetailView;

use phycom\backend\widgets\Box;
use phycom\backend\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = $model->discountRule->isNewRecord ? Yii::t('backend/main', 'Create new client card') : Yii::t('backend/main', 'Client card {id}', ['id' => $model->discountRule->id]);
$this->params['breadcrumbs'][] = Yii::t('backend/main', 'Sale');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/main', 'Client cards'), 'url' => ['/client-card']];
$this->params['breadcrumbs'][] = $this->title
?>


<div id="post-form-container">
    <?php $form = ActiveForm::begin(['id' => 'client-card-form']); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="system-messages"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= Box::begin([
                            'showHeader' => false,
                            'options' => ['class' => 'box box-default'],
                            'bodyOptions' => ['class' => 'box-body']
                        ]);
                    ?>

                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'code')->textInput(); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <hr>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">

                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($model, 'name')->textInput(); ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'personalCode')->textInput(); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($model, 'rate')->textInput(['type' => 'number', 'max' => 100, 'min' => 0, 'step' => 0.01]); ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'birthdayRate')->textInput(['type' => 'number', 'max' => 100, 'min' => 0, 'step' => 0.01]); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($model, 'expires')->datePicker(); ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'birthday')->datePicker(); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, 'categories')->multiSelect($model->allCategories); ?>
                                </div>
                            </div>

                        </div>


                        <div class="col-md-4">
                            <?php if (!$model->discountRule->isNewRecord): ?>

                                <?= DetailView::widget([
                                    'model' => $model->discountRule,
                                    'options' => ['class' => 'table table-striped table-bordered detail-view', 'style' => 'margin-top: 25px;'],
                                    'attributes' => [
                                        [
                                            'attribute' => 'status',
                                            'format' => 'html',
                                            'value' => '<span class="label ' . $model->discountRule->status->labelClass . '">'.$model->discountRule->status->label.'</span>',
                                        ],
                                        [
                                            'attribute' => 'used',
                                            'format' => 'integer',
                                        ],
                                        [
                                            'attribute' => 'created_at',
                                            'format' => 'datetime',
                                        ],
                                        [
                                            'attribute' => 'updated_at',
                                            'format' => 'datetime',
                                        ],
                                        [
                                            'attribute' => 'created_by',
                                            'value' => $model->discountRule->createdBy->fullName,
                                        ],
                                    ]]);
                                ?>
                            <?php endif; ?>
                        </div>

                    </div>

                    <?php Box::end(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= Box::begin([
                'showHeader' => false,
                'options' => ['class' => 'box box-default'],
                'bodyOptions' => ['class' => 'box-body']
            ]);
            ?>
            <div class="clearfix">

                <?= $this->render('/partials/back-btn', ['size' => 'lg']); ?>
                <?php if (!$model->discountRule->isNewRecord): ?>
                    <?= $this->render('/partials/delete-dropdown', [
                            'url' => ['client-card/delete', 'id' => $model->discountRule->id],
                            'options' => ['class' => 'pull-right', 'style' => 'margin-left: 10px;']
                        ]);
                    ?>
                <?php endif; ?>
                <?= $this->render('/partials/save-btn', ['class' => 'pull-right']); ?>
                <?php if (!$model->discountRule->isNewRecord): ?>
                    <?= Html::a(Yii::t('backend/main', 'New'), ['client-card/add'], ['class' => 'btn btn-lg btn-success btn-flat pull-right', 'style' => 'margin-right: 10px;']); ?>
                <?php endif; ?>
            </div>

            <?= Box::end(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <br>
            <br>
            <br>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
