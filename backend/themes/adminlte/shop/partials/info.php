<?php

/**
 * @var $this yii\web\View
 * @var \phycom\backend\models\ShopForm $model
 */

use phycom\backend\widgets\Modal;
use phycom\backend\widgets\ActiveForm;

use phycom\common\helpers\f;
use phycom\common\widgets\Map;
use phycom\common\models\attributes\ShopStatus;
use phycom\common\models\Address;

use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use yii\helpers\Html;

$tab = $tab ?? '';

$fieldOpt = ['template' => '{input}{error}', 'options' => ['class' => 'no-margin']];
$fieldOptMd = ArrayHelper::merge($fieldOpt, ['options' => ['style' => 'max-width: 300px;']]);
$fieldOptSm = ArrayHelper::merge($fieldOpt, ['options' => ['style' => 'max-width: 200px;']]);

$reflect = new \ReflectionClass($model->addressForm);
$addressModelName = $reflect->getShortName();

\phycom\backend\assets\ShopAddressFormAsset::register($this);
$this->registerJs(';addressForm.init("' . $addressModelName . '", "#shopform-address");');

?>


<div class="row">
    <div class="col-md-9 col-lg-8">

        <?php $form = ActiveForm::begin(['id' => 'shop-form']); ?>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [

                [
                    'attribute' => 'name',
                    'format' => 'raw',
                    'value' => $form->field($model, 'name', $fieldOpt)->label(false)->textInput(['class' => 'form-control no-margin'])
                ],
                [
                    'attribute' => 'email',
                    'format' => 'raw',
                    'value' => $form->field($model, 'email', $fieldOpt)->label(false)->textInput(['class' => 'form-control no-margin'])
                ],
                [
                    'attribute' => 'address',
                    'format' => 'raw',
                    'value' => function ($model) use ($form, $fieldOpt) {
                        return $form->field($model, 'address', ['options' => ['class' => 'no-margin']])->addressField('shop-address-modal');
                    }
                ],
                [
                    'attribute' => 'deliveryArea',
                    'format' => 'raw',
                    'value' => $form->field($model, 'deliveryArea', $fieldOptMd)->label(false)->boolean()
                ],
                [
                    'attribute' => 'phone',
                    'format' => 'raw',
                    'value' => $form->field($model, 'phone', $fieldOptMd)->label(false)->phoneInput2()
                ],
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => $form->field($model, 'status', $fieldOptSm)->dropDownList(ShopStatus::displayValues())
                ],
                [
                    'label' => $model->shop->getAttributeLabel('created_at'),
                    'format' => 'datetime',
                    'value' => $model->shop->created_at,
                    'visible' => !$model->shop->isNewRecord
                ],
                [
                    'label' => $model->shop->getAttributeLabel('updated_at'),
                    'format' => 'datetime',
                    'value' => $model->shop->updated_at,
                    'visible' => !$model->shop->isNewRecord
                ]
            ],
        ]);
        ?>
        <input type="hidden" name="tab" value="<?= $tab; ?>" />

        <?php ActiveForm::end(); ?>

        <?= Modal::widget([
            'id' => 'shop-address-modal',
            'title' => $model->shop->address ? Yii::t('backend/shop','Update address') : Yii::t('backend/shop','Add address'),
            'content' => $this->render('/partials/address-form', ['model' => $model->addressForm])
        ]);
        ?>

    </div>

    <div class="col-lg-4 col-md-3">
        <?php if (!$model->shop->isNewRecord): ?>
            <?= Map::widget(['address' => Address::create($model->address)]); ?>
        <?php endif; ?>
    </div>

</div>

