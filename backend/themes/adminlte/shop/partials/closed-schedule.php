<?php

/**
 * @var $this yii\web\View
 * @var $model \phycom\backend\models\ShopClosedScheduleForm
 */

use phycom\backend\widgets\ActiveForm;
use phycom\backend\assets\CalendarAsset;

CalendarAsset::register($this);

$tab = $tab ?? '';

$model->populateEvents();
$this->registerJs("multiCalendarDatePicker.create('#closed-schedule-calendar', '#shopclosedscheduleform-events', 3);");
?>

<div class="row">
    <div class="col-md-12">
        <div id="closed-schedule-calendar"></div>
        <?php $form = ActiveForm::begin(['id' => 'closed-schedule-form']); ?>
        <?= $form->field($model, 'events', ['template' => '{input}'])->hiddenInput()->label(false); ?>
        <input type="hidden" name="tab" value="<?= $tab; ?>" />
        <?php ActiveForm::end(); ?>
    </div>
</div>
