<?php

/**
 * @var $this yii\web\View
 * @var $model \phycom\common\models\attributes\ShopSetting
 */

use phycom\backend\widgets\ActiveForm;

?>


<div class="row">
    <div class="col-md-8 col-lg-6">

        <?php $form = ActiveForm::begin(['id' => 'shop-settings-form']); ?>

        <?= $form->field($model, 'closedMessage')->textarea(['rows' => 5]); ?>
        <?= $form->field($model, 'defaultClosedMessage')->textarea(['rows' => 5]); ?>

        <?php ActiveForm::end(); ?>

    </div>
</div>

