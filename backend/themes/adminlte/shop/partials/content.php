<?php

/**
 * @var $this yii\web\View
 * @var \phycom\backend\models\PostForm $model
 */
use phycom\backend\assets\FormAsset;
use phycom\backend\widgets\ActiveForm;

FormAsset::register($this);


$this->registerJs(<<<JS
    jQuery(function ($) {
    
        let submitBtn = $('#save-shop-data').data('submit-btn');
    
        submitBtn.onAfterSubmit = function (data) {

            var deferred = $.Deferred();

            if (data.error) {
                submitBtn.msg.showError(data.error);
                submitBtn.reset();
                return deferred.resolve(data);
            }

            let el = $('#postattachmentform-file'),
                fileInput = el.data('fileinput'),
                totalFiles = Object.keys(el.fileinput('getFileStack')).length,
                numFilesUploaded = 0;
            
            if (totalFiles === 0) {
                window.location.reload(true);
                return deferred.resolve(data);
            }

            el.on('fileuploaded', function (e, uploadedData, previewId, index) {
                numFilesUploaded++;
                if (numFilesUploaded >= totalFiles) {
                    window.location.reload(true);
                    return deferred.resolve(data);
                }
            });
            
            fileInput.uploadUrl = data.uploadUrl;
            el.fileinput('upload');

            return deferred.promise();
        };
    });
JS
    , \yii\web\View::POS_READY);


$tab = $tab ?? '';

?>


<div class="row">
    <div id="post-form-container" class="col-md-12">

        <?php $form = ActiveForm::begin(['id' => 'content-form']); ?>
            <input type="hidden" name="tab" value="<?= $tab; ?>" />
        <?php ActiveForm::end(); ?>

        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->render('/post/partials/translation-form', [
                                'model'          => $model->getTranslationForm(),
                                'activeLanguage' => Yii::$app->lang->default,
                                'box'            => false,
                                'title'          => false,
                                'seoAttributes'  => false
                            ]);
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->render('/post/partials/post-form', [
                            'model'      => $model,
                            'attributes' => [
                                'status',
                                'created_by',
                                'created_at',
                                'updated_at',
                                'published_at'
                            ]
                        ]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->render('/post/partials/attachment-form', ['model' => $model->getAttachmentForm()]); ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

