<?php

/**
 * @var $this yii\web\View
 * @var $formModel \phycom\backend\models\ShopOpenScheduleCollectionForm
 */

use phycom\backend\widgets\ActiveForm;
use phycom\common\helpers\f;

$m = new \phycom\common\models\ShopOpen();
$tab = $tab ?? '';
?>


    <div class="row">
        <div class="col-md-12" style="max-width: 660px;">

            <?php $form = ActiveForm::begin(['id' => 'open-weekdays-form']); ?>

            <table class="table table-striped warning">
                <thead>
                    <tr>
                        <th><?= $m->getAttributeLabel('day_of_week'); ?></th>
                        <th><?= $m->getAttributeLabel('open'); ?></th>
                        <th><?= $m->getAttributeLabel('opened_at'); ?></th>
                        <th><?= $m->getAttributeLabel('closed_at'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($formModel->models as $key => $model): ?>
                        <tr>
                            <td>
                                <strong><?= f::weekday($model->day_of_week, '%A'); ?></strong>
                            </td>
                            <td>
                                <?= $form->mField($formModel, $model, 'open')->boolean(false, [], [
                                        '1' => Yii::t('backend/shop', 'Open'),
                                        '0' => Yii::t('backend/shop', 'Closed'),
                                    ]);
                                ?>
                            </td>
                            <td style="width: 140px;">
                                <?= $form->mField($formModel, $model, 'opened_at')->textInput(['value' => $model->opened_at instanceof \DateTime ? $model->opened_at->format('H:i') : $model->opened_at]); ?>
                            </td>
                            <td style="width: 140px;">
                                <?= $form->mField($formModel, $model, 'closed_at')->textInput(['value' => $model->closed_at instanceof \DateTime ? $model->closed_at->format('H:i') : $model->closed_at]); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <input type="hidden" name="tab" value="<?= $tab; ?>" />

            <?php ActiveForm::end(); ?>

        </div>
    </div>

