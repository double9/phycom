<?php

use phycom\backend\widgets\Box;
use phycom\backend\widgets\DataGrid;
use phycom\common\helpers\Filter;
use yii\helpers\Url;

/**
 * @var $this yii\web\View
 * @var \phycom\backend\models\SearchShop $model
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('backend/main', 'Shops');
$this->params['breadcrumbs'][] = $this->title
?>
<div class="row">
	<div class="col-md-12">
		<?= Box::begin([
			'options' => ['class' => 'box box-default'],
			'showHeader' => false,
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

		<?= DataGrid::widget([
			'id' => 'user-grid',
			'dataProvider' => $dataProvider,
			'filterModel' => $model,
			'rowOptions' => function ($model) {
				return [
					'class' => 'row-link',
					'data-url' => Url::toRoute(['shop/edit','id' => $model->id])
				];
			},
			'actions' => [
				[
					'label' => '<span class="fas fa-plus"></span>&nbsp;&nbsp;' . Yii::t('backend/main', 'Add shop'),
					'url' => ['shop/add'],
                    'options' => ['class' => 'btn btn-flat btn-success']
				]
			],
			'columns' => [
				[
					'attribute' => 'id',
					'format' => 'integer',
					'options' => ['width' => '120'],
					'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				],
                [
	                'attribute' => 'name',
	                'format' => 'text',
                ],
				[
					'attribute' => 'address',
					'format' => 'text',
					'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				],
				[
					'attribute' => 'phone',
					'format' => 'text',
					'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				],
				[
					'attribute' => 'status',
					'filter' => Filter::dropDown($model, 'status', \phycom\common\models\attributes\ShopStatus::displayValues()),
					'format' => 'raw',
					'value' => function ($model) {
						return '<span class="label '.$model->status->labelClass.'">' . $model->status->label . '</span>';
					},
					'options' => ['width' => '120']
				],
				[
					'attribute' => 'created_at',
					'filter' => Filter::daterangepicker($model, 'createdFrom', 'createdTo'),
					'format' => 'datetime',
					'options' => ['width' => '220'],
					'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				],
				[
					'attribute' => 'updated_at',
					'filter' => Filter::daterangepicker($model, 'updatedFrom', 'updatedTo'),
					'format' => 'datetime',
					'options' => ['width' => '220'],
					'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				],
			]
		]);
		?>

		<?= Box::end(); ?>
	</div>
</div>
