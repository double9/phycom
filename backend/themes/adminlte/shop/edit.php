<?php

/**
 * @var $this yii\web\View
 * @var \phycom\backend\models\ShopForm $model
 */

use phycom\backend\widgets\Box;
use phycom\backend\widgets\Tabs2;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/shop', 'Shops'), 'url' => ['/shop/index']];

$items = $items ?? [];
$active = $active ?? null;

?>

<div class="row">

	<div class="col-md-12">

        <?= Box::begin([
            'showHeader'  => false,
            'options'     => ['class' => 'box box-default'],
            'bodyOptions' => ['class' => 'box-body no-padding']
        ]);
        ?>
        <?= Tabs2::widget([
            'id'             => 'shop-tabs',
            'items'          => $items,
            'activeTab'      => $active,
            'contentOptions' => ['class' => 'tab-content col-md-10']
        ]);
        ?>
        <?= Box::end(); ?>

	</div>
</div>


<div class="row">
    <div class="col-md-12">
        <?= Box::begin([
            'showHeader'  => false,
            'options'     => ['class' => 'box box-default'],
            'bodyOptions' => ['class' => 'box-body']
        ]);
        ?>
        <div class="clearfix">
            <div class="pull-left"><?= $this->render('/partials/back-btn', ['size' => 'lg']); ?></div>
            <?= $this->render('/partials/save-btn-2', [
                'id'           => 'save-shop-data',
                'class'        => 'pull-right btn-lg',
                'formSelector' => '#shop-tabs .tab-content > .tab-pane.active'
            ]);
            ?>
        </div>
		<?= Box::end(); ?>
    </div>
</div>



