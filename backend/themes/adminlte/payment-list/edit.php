<?php

use phycom\backend\widgets\Box;
use phycom\common\models\attributes\PaymentStatus;

use rmrevin\yii\fontawesome\FAR;

use yii\helpers\Html;
use yii\widgets\DetailView;


/**
 * @var $this yii\web\View
 * @var \phycom\backend\models\PaymentForm $model
 * @var \phycom\common\models\Payment $payment
 */

$this->title = Yii::t('backend/main', 'Payment {id}', ['id' => $payment->id]);
$this->params['breadcrumbs'][] = Yii::t('backend/main', 'Sale');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/main', 'Payments'), 'url' => ['/payment']];
$this->params['breadcrumbs'][] = $this->title
?>

<div class="row">
    <div class="col-md-12 col-lg-8">


		<?= Box::begin([
			'showHeader' => false,
			'options' => ['class' => 'box box-default'],
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

		<?= DetailView::widget([
            'id' => 'payment-details',
			'model' => $payment,
			'options' => ['class' => 'table table-striped table-bordered detail-view table-responsive'],
			'attributes' => [
                [
                    'attribute' => 'invoice',
                    'format' => 'raw',
                    'value' => function ($model) {
		                return Html::a(
			                    '# ' . $model->invoice->number . '&nbsp;&nbsp;' . FAR::i(FAR::_FILE_PDF),
                                ['/invoice/edit', 'id' => $model->invoice->id],
                                ['class' => 'btn btn-flat btn-default btn-sm']
                        );
                    }
                ],
			    'transaction_id:text',
				'payment_method:text',
                'amount:currency',
				'remitter:text',
				[
				    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => function ($model) {
	                    return '<span class="label '.$model->status->labelClass.'">' . $model->status->label . '</span>';
                    }
                ],
				'explanation:text',
				'transaction_time:datetime',
				'transaction_data:json'
			],
		]);

		?>

        <br />

        <div class="row">
            <div class="col-md-6">
			    <?= $this->render('/partials/back-btn', ['size' => 'lg']); ?>
            </div>
            <div class="col-md-6 clearfix">
                <?php if ($model->payment->status->is(PaymentStatus::PENDING)): ?>
                    <?= Html::a(
                            Yii::t('backend/main', 'Set as complete'),
                            ['payment-list/update-status', 'id' => $model->payment->id, 'status' => PaymentStatus::COMPLETED],
                            ['class' => 'btn btn-lg btn-flat btn-success pull-right']
                        );
                    ?>
                <?php endif; ?>
            </div>
        </div>

	    <?= Box::end(); ?>

    </div>
</div>

