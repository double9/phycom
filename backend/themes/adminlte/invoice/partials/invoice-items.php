<?php


use phycom\common\helpers\f;

use yii\helpers\Html;

/**
 * @var \phycom\common\models\Invoice $invoice
 */

$orderItem = new \phycom\common\models\OrderItem();

/**
 * @var $deliveryModule \phycom\common\modules\delivery\Module;
 */
$deliveryModule = Yii::$app->getModule('delivery');
/**
 * @var $couriers \phycom\common\modules\delivery\interfaces\DeliveryMethodInterface[]
 */
$couriers = $deliveryModule->allMethods;

?>

<table class="order-table table table-striped">
	<colgroup>
		<col width="200">
		<col>
		<col width="200">
		<col width="200">
		<col width="200">
		<col width="200">
		<col width="50">
	</colgroup>
	<thead>
		<tr>
			<th><?= $orderItem->getAttributeLabel('code'); ?></th>
			<th><?= $orderItem->getAttributeLabel('title'); ?></th>
			<th><?= $orderItem->getAttributeLabel('quantity'); ?></th>
			<th><?= $orderItem->getAttributeLabel('price'); ?></th>
			<th><?= $orderItem->getAttributeLabel('discount'); ?></th>
			<th><?= $orderItem->getAttributeLabel('total'); ?></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($invoice->items as $model): ?>

			<tr>
				<td><?= f::text($model->orderItem->code); ?></td>
				<td>
                    <?php
                        $title = f::text($model->orderItem->title);

                        if (!$model->orderItem->product) {
                            $attributes = $model->orderItem->product_attributes ? json_decode($model->orderItem->product_attributes, true) : [];
                            $Option = Yii::$app->modelFactory->getVariantOption();

	                        if (!empty($attributes)) {
		                        $title .= ' - ';
	                        }

	                        foreach ($attributes as $attributeName => $attributeValue) {

		                        if ($option = $Option::findByKey($attributeValue)) {
			                        $attributeValue = Html::tag('span', $option->label, ['class' => 'label label-default']);
		                        } else if (filter_var($attributeValue, FILTER_VALIDATE_URL)) {
		                            $attributeValue = Html::a($attributeValue, $attributeValue, ['target' => '_blank']);
                                }
		                        $title .= Html::tag('span', $attributeValue, ['class' => 'line-attribute', 'data-attribute' => $attributeName]);
	                        }


//	                        foreach ($couriers as $courier) {
//	                            foreach ($courier->getAreas() as $area) {
//	                                if ($area->getUniqueId() === $model->code) {
//	                                    if ($courier->getType()->value === \phycom\common\modules\delivery\models\CourierType::HOME_DELIVERY) {
//                                            $title .= ' - <span class="delivery-address">' . (string) \phycom\common\models\Address::create($model->order->delivery_address) . '</span>';
//                                        }
//	                                    break 2;
//                                    }
//                                }
//                            }
                        }

                        echo $title;
                    ?>
                </td>
				<td><?= f::integer($model->quantity); ?></td>
				<td><?= f::currency($model->orderItem->price); ?></td>
				<td><?= f::percent($model->orderItem->discount); ?></td>
				<td><?= f::currency($model->orderItem->total); ?></td>
				<td>
                </td>

			</tr>

		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="4"></td>
			<td><strong><?= Yii::t('backend/order', 'Total amount'); ?></strong></td>
            <td><h4 class="no-margin"><strong><?= f::currency($invoice->total); ?></strong></h4></td>
			<td></td>
		</tr>
	</tfoot>
</table>

