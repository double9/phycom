<?php

use phycom\backend\widgets\Box;
use phycom\backend\widgets\DataGrid;
use phycom\common\helpers\Filter;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var \phycom\backend\models\SearchInvoice $model
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('backend/main', 'Invoices');
$this->params['breadcrumbs'][] = Yii::t('backend/main', 'Sale');
$this->params['breadcrumbs'][] = $this->title
?>
<div class="row">
	<div class="col-md-12">
		<?= Box::begin([
			'options' => ['class' => 'box box-default'],
			'showHeader' => false,
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

		<?= DataGrid::widget([
			'id' => 'user-grid',
			'dataProvider' => $dataProvider,
			'filterModel' => $model,
			'rowOptions' => function ($model) {
				return [
					'class' => 'row-link',
					'data-url' => Url::toRoute(['invoice/edit','id' => $model->id])
				];
			},
			'actions' => [],
			'columns' => [
				[
					'attribute' => 'number',
					'format' => 'text',
					'options' => ['width' => '200']
				],
				[
					'attribute' => 'customer',
					'format' => 'text',
					'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				],
				[
                    'attribute' => 'orderNumber',
					'format' => 'raw',
                    'value' => function ($model) {
                        return Html::a('<span class="fas fa-info"></span>&nbsp;&nbsp;' . $model->orderNumber, ['/order/edit', 'id' => $model->order_id]);
                    },
					'options' => ['width' => '200'],
					'contentOptions' => ['class' => 'clickable hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				],
				[
					'attribute' => 'total',
					'format' => 'currencyDisplay',
					'options' => ['width' => '200']
				],
				[
					'attribute' => 'status',
					'filter' => Filter::dropDown($model, 'status', \phycom\common\models\attributes\InvoiceStatus::displayValues()),
					'format' => 'raw',
					'value' => function ($model) {
						return '<span class="label '.$model->status->labelClass.'">' . $model->status->label . '</span>';
					},
					'options' => ['width' => '200']
				],
				[
					'attribute' => 'created_at',
					'filter' => Filter::daterangepicker($model, 'createdFrom', 'createdTo'),
					'format' => 'datetime',
					'options' => ['width' => '220'],
					'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				],
                [
                    'attribute' => 'file',
                    'format' => 'raw',
                    'enableSorting' => false,
                    'value' => function ($model) {
		                return Html::a('<span class="far fa-file-pdf"></span>', $model->fileUrl, ['target' => '_blank']);
                    },
	                'options' => ['width' => '20'],
	                'contentOptions' => ['class' => 'clickable hidden-sm hidden-xs'],
	                'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
	                'headerOptions' => ['class' => 'hidden-sm hidden-xs']
                ]
			]
		]);
		?>

		<?= Box::end(); ?>
	</div>
</div>
