<?php

use phycom\backend\widgets\ActiveForm;
use phycom\common\models\attributes\DiscountRuleStatus;

use yii\helpers\Html;

$model = new \phycom\backend\models\DiscountRuleBulkUpdateForm();
$modelName = (new \ReflectionClass($model))->getShortName();

$form = ActiveForm::begin();
?>
    <div class="keys" data-name="<?= $modelName . "[keys][]" ?>"></div>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'minPurchase')
                ->numberInput(['placeholder' => $model->getAttributeLabel('minPurchase')])
                ->label(false)
            ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'startDate')
                ->datePicker(['placeholder' => $model->getAttributeLabel('startDate')])
                ->label(false)
            ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'maxUsage')
                ->numberInput(['placeholder' => $model->getAttributeLabel('maxUsage')])
                ->label(false)
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'discountRate')
                ->numberInput(['placeholder' => $model->getAttributeLabel('discountRate')])
                ->inputAppend('%')
                ->label(false)
            ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'expirationDate')
                ->datePicker(['placeholder' => $model->getAttributeLabel('expirationDate')])
                ->label(false)
            ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'status')
                    ->dropDownList(DiscountRuleStatus::displayValues(), [
                        'prompt' => Yii::t('backend/main', 'Select {attribute}', [
                            'attribute' => $model->getAttributeLabel('status')
                        ])
                    ])
                    ->label(false)
            ?>
        </div>
        <div class="col-md-3">
            <?= Html::submitButton('<span class="fas fa-exclamation-circle"></span>&nbsp;&nbsp;' . Yii::t('backend/main', 'Bulk update selected rows'), ['class' => 'btn btn-md btn-flat btn-primary']) ?>
        </div>
    </div>


<?php ActiveForm::end(); ?>
