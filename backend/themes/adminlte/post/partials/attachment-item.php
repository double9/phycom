<?php

/**
 * @var $this yii\web\View
 * @var $model phycom\backend\models\PostAttachmentForm
 * @var $form \phycom\backend\widgets\ActiveForm
 * @var $attachment \phycom\common\models\PostAttachment
 */

use yii\helpers\Html;
use yii\helpers\Url;
use phycom\common\models\File;

?>

<?= Html::beginTag('div', [
        'class' => 'attachment-item initial-item',
        'data-update-url' => Url::toRoute(['/post/update-attachment', 'id' => $attachment->post_id, 'fileId' => $attachment->file_id]),
        'data-toggle' => 'tooltip',
        'title' => $attachment->file->name,
        'data-placement' => 'top',
        'data-container' => 'body',
        'data-file-id' => $attachment->file_id
    ])
?>
    <div class="item-content">
	    <?= Html::img(null, ['data-src' => $attachment->file->getThumbUrl(File::THUMB_SIZE_SMALL), 'class' => 'file-preview-image']); ?>
    </div>
    <div class="item-options">
    </div>

<?= Html::endTag('div'); ?>