<?php
/**
 * @var $this yii\web\View
 * @var $model phycom\backend\models\PostAttachmentForm
 */

use phycom\backend\widgets\ActiveForm;
use phycom\backend\assets\FileInputAsset;
use phycom\backend\widgets\Modal;

use rmrevin\yii\fontawesome\FAS;

use yii\helpers\Url;
use yii\helpers\Html;

FileInputAsset::register($this);


echo Html::beginTag('div', ['class' => 'attachments-modal-container']);
foreach ($model->post->attachments as $attachment) {
    echo Modal::widget([
        'id' => 'attachment_' . $attachment->file_id . '_modal',
        'title' => $attachment->file->filename,
        'content' => $this->render('attachment-item-options', ['attachment' => $attachment]),
        'actions' => [
            [
                'title' => FAS::i(FAS::_CHECK) . '&nbsp;&nbsp;' . Yii::t('backend/main', 'Save'),
                'class' => 'btn-primary btn-save',
                'url' => ['post/update-attachment', 'id' => $attachment->post_id, 'fileId' => $attachment->file_id]
            ],
            [
                'title' => FAS::i(FAS::_TIMES) . '&nbsp;&nbsp;' .Yii::t('backend/main', 'Close'),
                'class' => 'btn-default btn-close'
            ]
        ],
        'options' => ['size' => 'lg']
    ]);
}
echo Html::endTag('div');


$form = ActiveForm::begin(['id' => 'post-attachment-form', 'options' => ['class' => 'file-input-form']]);

$initialPreview = [];
$initialPreviewConfig = [];


foreach ($model->post->attachments as $attachment) {
	$initialPreview[] = $this->render('attachment-item', [
		'model' => $model,
		'form' => $form,
		'attachment' => $attachment
	]);
	$initialPreviewConfig[] = [
		'caption' => $attachment->file->name,
		'width' => '120px',
		'key' => $attachment->file->id,
		'previewAsData' => false
	];
}

echo $form->field($model, 'file')->fileInput([
		'options' => [
            'multiple' => true,
            'class' => 'attachment-form-input',
            'accept' => 'image/*'
        ],
		'pluginOptions' => [
			'theme' => 'fa',
			'browseOnZoneClick' => true,
			'uploadAsync' => true,
            'enableResumableUpload' => false,
			'autoReplace' => false,
			'overwriteInitial' => false,
			'maxFileCount' => $model->maxFiles,
			'allowedFileExtensions' => $model->allowedExtensions,
			'uploadUrl' => Url::toRoute(['post/upload-attachment', 'id' => $model->post->id]),
			'deleteUrl' => Url::toRoute(['post/delete-attachment', 'id' => $model->post->id]),
			'showClose'  => false,
			'showRemove'  => false,
			'showCaption' =>  false,
			'showBrowse' => false,
			'preferIconicPreview' => false,
			'browseClass' => 'btn btn-sm btn-primary btn-flat',
			'browseIcon' => '<span class="far fa-image"></span> ',
			'browseLabel' => Yii::t('backend/main','Browse'),
			'uploadClass' => 'btn btn-sm btn-default btn-flat file-upload-btn',
			'uploadLabel' => Yii::t('backend/main','Save'),
			'initialPreviewShowDelete' => true,
			'initialPreviewAsData' => true,
			'initialPreview' => $initialPreview,
            'initialPreviewConfig' => $initialPreviewConfig,
			'previewTemplates' => [
				'image' => $this->render('attachment-item-preview')
			],
			'previewSettings' => [
			    'image' => ['width' => "auto", 'height' => "auto", 'style' => 'max-height: 200px;'],
                'key' => 1
            ],
			'layoutTemplates' => [
				'main2' => '{preview}',
				'actions' => '<div class="file-footer-buttons">{delete}</div>{drag}',
				'progress' => '',
				'footer' => $this->render('attachment-item-footer')
			],
			'fileActionSettings' => [
				'showDrag' => true,
				'dragIcon' => '<span class="fas fa-bars"></span> ',
				'dragClass' => 'drag',
				'dragTitle' => Yii::t('backend/main','Drag')
			],
			'customPreviewTags' => [],
		]
	]);

ActiveForm::end();


$this->registerJs(<<<JS

[].forEach.call(document.querySelectorAll('img[data-src]'),    function(img) {
    img.setAttribute('src', img.getAttribute('data-src'));
    img.onload = function() {
        img.removeAttribute('data-src');
    };
});
JS
, \yii\web\View::POS_READY);

