<?php

/**
 * @var $this yii\web\View
 * @var $attachment \phycom\common\models\PostAttachment
 */

use common\models\product\Variant;

use phycom\backend\widgets\ActiveForm;
use phycom\common\models\File;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;


$productVariant = new \phycom\backend\models\PostAttachmentOptionsForm($attachment);


$form = ActiveForm::begin([
    'id' => 'attachment_' . $attachment->file_id . '_form',
    'action' => ['/post/update-attachment', 'id' => $attachment->post_id, 'fileId' => $attachment->file_id],
    'options' => ['class' => 'product-attachment-options']
]);
?>
<div class="row">
    <div class="col-md-12">
        <div class="messages"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <?= Html::img(null, ['data-src' => $attachment->file->getThumbUrl(File::THUMB_SIZE_LARGE)]); ?>
    </div>
    <div class="col-md-6">

        <table class="table table-striped no-margin">
            <colgroup>
                <col width="200">
                <col>
            </colgroup>

            <tr>
                <td><?= Yii::t('backend/main', 'Imported filename') ?>:</td>
                <td><?= $attachment->file->name; ?></td>
            </tr>

            <?php if ($attachment): ?>


            <?php endif; ?>

        </table>
    </div>
</div>
<?php ActiveForm::end(); ?>
