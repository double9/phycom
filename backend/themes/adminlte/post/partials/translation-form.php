<?php
/**
 * @var $this yii\web\View
 * @var $model phycom\backend\models\PostTranslationForm
 * @var \phycom\common\models\Language $activeLanguage
 */
use phycom\backend\widgets\ActiveForm;

use phycom\common\models\attributes\TranslationStatus;
use phycom\common\helpers\f;

use yii\helpers\Html;
use yii\helpers\Url;

$modelName = (new \ReflectionClass($model))->getShortName();

$form = ActiveForm::begin([
    'id'              => 'post-translation-form',
    'action'          => ['post/edit'],
    'successCssClass' => 'has-success-1' // disable success colors
]);

$linkField = $linkField ?? false;
$box = $box ?? true;
$title = (bool) ($title ?? true);
$seoAttributes = (bool) ($seoAttributes ?? true);

$activeLanguage = $activeLanguage ?? Yii::$app->lang->default;
$languageCount = count($model->models);

?>


<div class="nav-tabs-custom" <?= $box ? '' : 'style="box-shadow: none; margin-bottom: 0;"' ?>>
    <ul class="nav nav-tabs" role="tablist">
        <?php foreach($model->models as $language => $translation): ?>
            <li role="presentation" <?= ($activeLanguage->code === $language ? 'class="active"' : '') ?>>
                <a data-target="#content-language-<?= $language; ?>" aria-expanded="true" role="tab" data-toggle="tab">
                    <?= f::html('<b>' . strtoupper($translation->languageModel->code) . '</b> (' . $translation->languageModel->native . ')'); ?>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
    <div class="tab-content" <?= $box ? '' : 'style="border-left: 0; border-right: 0; border-bottom: 0; padding-left: 0; padding-right: 0; padding-bottom: 0;"' ?>>
	    <?php foreach($model->models as $language => $translation): ?>
            <div role="tabpanel" class="tab-pane<?= ($activeLanguage->code === $language ? ' active' : '') ?>" id="content-language-<?= $language; ?>">

                <?php if ($title || $seoAttributes): ?>
                    <div class="row">
                        <div class="col-md-12 clearfix">

                            <?php if ($title): ?>
                                <div <?= ($seoAttributes ? 'style="width: 79.5%; float: left;"' : '') ?>>
                                    <?= $form->field($translation, "title", ['enableClientValidation' => false])
                                        ->textInput([
                                            'id' => "translations-$language-title",
                                            'name' => $modelName . "[models][$language][title]"
                                        ])
                                        ->label($translation->getAttributeLabel('title'));
                                    ?>
                                </div>
                            <?php endif; ?>

                            <?php if ($seoAttributes): ?>
                                <div <?= ($title ? 'style="width: 19.5%; float: right;"' : '') ?>>
                                    <?= Html::a(Yii::t('backend/post', 'Toggle SEO attributes'), '#seo-' . $language, [
                                        'class' => 'collapsed btn btn-default btn-flat pull-right',
                                        'aria-expanded' => 'false',
                                        'data-parent' => '#seo-container-' . $language,
                                        'data-toggle' => 'collapse',
                                        'style' => $title ? 'margin-top: 25px; width: 100%;' : 'margin-bottom: 10px; width: 100%;'
                                    ]);
                                    ?>
                                </div>
                            <?php endif; ?>

                        </div>

                        <div class="col-lg-11 col-md-10">

                        </div>
                        <div class="col-lg-1 col-md-2 clearfix">

                        </div>
                    </div>
                <?php endif; ?>

                <?php if ($seoAttributes): ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="seo-container-<?= $language; ?>">
                                <div id="seo-<?= $language; ?>" class="collapse" aria-expanded="false" style="height:0; max-width: 800px;">
                                    <div style="margin: 30px 0 50px 0;">
                                    <?= $form->field($translation, 'url_key')
                                        ->textInput([
                                            'id' => "translations-$language-url_key",
                                            'name' =>  $modelName . "[models][$language][url_key]"
                                        ])
                                        ->label($translation->getAttributeLabel('url_key'));
                                    ?>
                                    <?= $form->field($translation, 'meta_title')
                                        ->textInput([
                                            'id' => "translations-$language-meta_title",
                                            'name' =>  $modelName . "[models][$language][meta_title]"
                                        ])
                                        ->label($translation->getAttributeLabel('meta_title'));
                                    ?>
                                    <?= $form->field($translation, 'meta_keywords')
                                        ->textInput([
                                            'id' => "translations-$language-meta_keywords",
                                            'name' =>  $modelName . "[models][$language][meta_keywords]"
                                        ])
                                        ->label($translation->getAttributeLabel('meta_keywords'));
                                    ?>
                                    <?= $form->field($translation, 'meta_description')
                                        ->textInput([
                                            'id' => "translations-$language-meta_description",
                                            'name' =>  $modelName . "[models][$language][meta_description]"
                                        ])
                                        ->label($translation->getAttributeLabel('meta_description'));
                                    ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($translation, 'content')
                            ->editor([
                                'options' => [
                                    'rows' => 5,
                                    'id'   => "translations-$language-content",
                                    'name' => $modelName . "[models][$language][content]"
                                ],
                                'uploadUrl' => Url::toRoute(['post/editor-upload', 'id' => $model->getPost()->id])
                            ])
                            ->label($translation->getAttributeLabel('content'));
                        ?>
                    </div>
                </div>

                <?php if ($languageCount === 1): ?>
                    <?= $form->field($translation, 'status')->hiddenInput([
                        'id' => "translations-$language-status",
                        'name' =>  $modelName . "[models][$language][status]",
                        'value' => TranslationStatus::PUBLISHED
                    ])->label(false); ?>
                <?php endif; ?>

                <?php if ($languageCount > 1 || $linkField): ?>
                    <div class="row">

                        <?php if ($languageCount > 1): ?>
                            <div class="col-lg-4 col-md-5">
                                <?= $form->field($translation, 'status')
                                    ->dropDownList(TranslationStatus::displayValues(), [
                                        'id' => "translations-$language-status",
                                        'name' =>  $modelName . "[models][$language][status]"
                                    ])
                                    ->label(Yii::t('backend/post', 'Translation status'));
                                ?>
                            </div>
                        <?php endif; ?>

                        <?php if ($linkField): ?>

                            <div class="col-lg-4 col-md-5">
                                <?= $form->field($translation, 'meta[link]')
                                    ->textInput([
                                        'id' => "translations-$language-meta-link",
                                        'name' =>  $modelName . "[models][$language][meta][link]"
                                    ])
                                    ->label(Yii::t('backend/post', 'Link to a page'));
                                ?>
                            </div>

                        <?php endif; ?>
                    </div>
                <?php endif; ?>

            </div>
	    <?php endforeach; ?>
    </div>
</div>

<?php ActiveForm::end(); ?>


