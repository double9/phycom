<?php

use phycom\backend\widgets\Box;
use phycom\backend\widgets\FormCollection;
use phycom\backend\widgets\FormSubmitBtn;

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $this yii\web\View
 * @var \phycom\backend\models\PostForm $model
 * @var \phycom\backend\models\PostTranslationForm $translationForm
 */
$this->title = $model->post->isNewRecord ? Yii::t('backend/post', 'Add Post') : $model->post->id . ' - ' . $model->post->translation->title;

$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/post', 'Posts'), 'url' => ['post/index']];
$this->params['breadcrumbs'][] = $model->post->isNewRecord ? $this->title : $model->post->id;

$formCollection = FormCollection::begin([
    'id'              => 'post-form-container',
    'submitUrl'       => $model->post->isNewRecord ? Url::toRoute(['/post/add']) : Url::toRoute(['/post/edit', 'id' => $model->post->id]),
    'forms'           => ['#post-translation-form', '#post-form'],
    'attachmentForms' => ['#postattachmentform-file']
]);

?>

    <div class="row">
        <div class="col-md-12">
            <div class="system-messages"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-8">
                    <?= $this->render('partials/translation-form', ['model' => $model->getTranslationForm()]); ?>
                </div>
                <div class="col-md-4">
                    <?= Box::begin([
                        'showHeader' => false,
                        'options' => ['class' => 'box box-default', 'id' => 'page-params-box'],
                        'bodyOptions' => ['class' => 'box-body']
                    ]);
                    ?>

                    <?= $this->render('partials/post-form', ['model' => $model]); ?>

                    <?= Box::end(); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= Box::begin([
                        'showHeader' => false,
                        'options' => ['class' => 'box box-default'],
                        'bodyOptions' => ['class' => 'box-body']
                    ]);
                    ?>

                    <?= $this->render('partials/attachment-form', ['model' => $model->getAttachmentForm()]); ?>

                    <?= Box::end(); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= Box::begin([
                    'showHeader' => false,
                    'options' => ['class' => 'box box-default'],
                    'bodyOptions' => ['class' => 'box-body']
                ]);
            ?>
            <div class="clearfix">

	            <?= $this->render('/partials/back-btn', ['size' => 'lg']); ?>

                <?= FormSubmitBtn::widget(['formCollection' => $formCollection]) ?>

	            <?= Html::a('<span class="fas fa-plus"></span>&nbsp;&nbsp;' . Yii::t('backend/post', 'New'), ['post/add'], [
	                    'class' => 'btn btn-flat btn-success btn-lg pull-right',
                        'style' => 'margin-right: 10px;'
                ]); ?>

            </div>

            <?= Box::end(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <br>
            <br>
            <br>
        </div>
    </div>

<?php $formCollection::end(); ?>

