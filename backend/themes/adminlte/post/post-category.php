<?php

use phycom\common\helpers\f;
use phycom\backend\models\product\ProductCategoryForm;
use phycom\backend\widgets\Tree;
use phycom\backend\widgets\Tabs;

use yii\helpers\Url;
use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var \phycom\backend\models\product\SearchProductCategory $model
 * @var ProductCategoryForm $formModel
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \phycom\common\models\Language $activeLanguage
 */
$this->title = Yii::t('backend/category', 'Post Categories');

$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/category', 'Post categories')];

if ($formModel) {
	$this->params['breadcrumbs'][] = $model->category->isNewRecord ? Yii::t('backend/category', 'Add category') : $model->category->id;
}

$activeLanguage = $model->language;

$tabItems = [];
foreach (Yii::$app->lang->enabled as $language) {
    $tabItems[] = [
        'id'    => 'content-language-' . $language->code,
        'label' => f::html('<b>' . strtoupper($language->code) . '</b> (' . $language->native . ')'),
        'url'   => Url::current(['lang' => $language->code])
    ];
}

Tabs::begin([
    'id'           => 'post-category-language-tabs',
    'items'        => $tabItems,
    'active'       => 'content-language-' . $activeLanguage->code,
    'ajax'         => false,
    'pajaxOptions' => [
        'id'                 => 'post-category-pajax',
        'enablePushState'    => false,
        'enableReplaceState' => true
    ]
]);
?>

    <div class="row">
        <div class="col-md-6">

            <br />

			<?= Tree::widget([
                    'pajaxId' => 'post-category-pajax',
                    'dataProvider' => $dataProvider,
                    'initialNode' => $model->category,
				    'moveAction' => ['move'],
                    'rowOptions' => function ($model, $key, $index, $grid) {

	                    /**
	                     * @var $model \phycom\backend\models\SearchPostCategory
	                     */
                        $options = [];

                        $currentRoute = [
	                        '/' . Yii::$app->controller->getRoute(),
                            'id' => Yii::$app->getRequest()->getQueryParam('id', null)
                        ];

                        if ($model->status->isHidden) {
                            Html::addCssClass($options, 'hidden-category');
                        }

	                    if (Url::toRoute($currentRoute) === Url::toRoute(['post-category/edit', 'id' => $model->id])) {
		                    Html::addCssClass($options, 'current');
                        }
                        return $options;
                    },
                    'columns' => [
                        [
                            'attribute' => 'title',
                            'format' => 'raw',
                            'content' => function($model) use ($activeLanguage) {
                                return Html::a($model->title, ['post-category/edit', 'id' => $model->id, 'lang' => $activeLanguage->code]);
                            }
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'text',
                            'content' => function($model) {
                                return Html::tag('span', $model->status, ['class' => 'label ' . $model->status->labelClass]);
                            }
                        ],
                        [
                            'label' => false,
                            'format' => 'raw',
                            'content' => function($model) {

                                return $this->render('/partials/delete-dropdown', [
                                    'label' => '<span class="far fa-trash-alt"></span>',
                                    'url' => ['post-category/delete', 'id' => $model->id],
                                    'options' => ['class' => 'toggle-hover']
                                ]);
                            },
                            'options' => ['width' => 40],
                            'contentOptions' => ['style' => 'padding: 2px 0 0 0;']
                        ],
                    ],
                ]);
			?>
        </div>
        <div class="col-md-6">

            <br />
            <div class="frame">

				<?php if ($formModel): ?>
					<?= $this->render('partials/category-form', [
						'model' => $formModel,
						'action' => $model->category->isNewRecord ?
							Url::toRoute(['post-category/create', 'lang' => $activeLanguage->code]) :
							Url::toRoute(['post-category/edit', 'id' => $model->category->id, 'lang' => $activeLanguage->code])
					]);
					?>
				<?php else: ?>
                    <table class="table table-striped">
                        <tr>
                            <th><?= Yii::t('backend/category', 'Total number of categories'); ?>: </th>
                            <td><?= f::integer($model->numTotal); ?></td>
                        </tr>
                        <tr>
                            <th><?= Yii::t('backend/category', 'Visible'); ?>: </th>
                            <td><?= f::integer($model->numVisible); ?></td>
                        </tr>
                        <tr>
                            <th><?= Yii::t('backend/category', 'Hidden'); ?>: </th>
                            <td><?= f::integer($model->numHidden); ?></td>
                        </tr>
                        <tr>
                            <th><?= Yii::t('backend/category', 'Translated'); ?>: </th>
                            <td><?= f::integer($model->numTranslated); ?></td>
                        </tr>
                        <tr>
                            <th><?= Yii::t('backend/category', 'Translation pending'); ?>: </th>
                            <td><?= f::integer($model->numTranslationPending); ?></td>
                        </tr>

                    </table>
				<?php endif; ?>
            </div>

            <div class="clearfix">
                <hr />
		        <?php if ($formModel): ?>
			        <?= $this->render('/partials/save-btn', ['class' => 'pull-right save-btn']); ?>
		        <?php endif; ?>

		        <?= Html::a(
			        '<span class="fas fa-plus"></span>&nbsp;&nbsp;' . Yii::t('backend/category', 'New'),
			        ['post-category/create', 'lang' => $activeLanguage->code],
			        [
				        'class' => 'btn btn-flat btn-success btn-lg pull-right',
				        'style' => $formModel ? 'margin-right: 10px;' : ''
			        ]
		        );
		        ?>
            </div>

        </div>
    </div>

<?php Tabs::end(); ?>


<?php

$this->registerJs(
        <<<JS
    $(function () {
        $('.save-btn').on('click', '.save', function (e) {
            e.preventDefault();
            $('.post-category-form').yiiActiveForm('submitForm');
        });
    });
JS
	, yii\web\View::POS_END);
?>
