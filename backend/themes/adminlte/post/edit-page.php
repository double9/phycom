<?php

use phycom\backend\widgets\Box;
use phycom\backend\widgets\ActiveForm;
use phycom\backend\widgets\FormCollection;
use phycom\backend\widgets\FormSubmitBtn;

use phycom\common\models\attributes\PostStatus;
use phycom\common\helpers\f;

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $this yii\web\View
 * @var \phycom\backend\models\PostForm $model
 */
$this->title = $model->post->isNewRecord ? Yii::t('backend/post', 'Add Page') : $model->post->id . ' - ' . $model->post->translation->title;

$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/post', 'Pages'), 'url' => ['post/pages']];
$this->params['breadcrumbs'][] = $model->post->isNewRecord ? $this->title : $model->post->id;

$formCollection = FormCollection::begin([
    'id'              => 'post-form-container',
    'submitUrl'       => $model->post->isNewRecord ? Url::toRoute(['/post/add-page']) : Url::toRoute(['/post/edit', 'id' => $model->post->id]),
    'forms'           => ['#post-translation-form', '#post-form']
]);

?>

    <div class="row">
        <div class="col-md-12">
            <div class="system-messages"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-8">
                    <?= $this->render('partials/translation-form', ['model' => $model->getTranslationForm()]); ?>
                </div>

                <div class="col-md-4">

                    <?= Box::begin([
                        'showHeader' => false,
                        'options' => ['class' => 'box box-default', 'id' => 'page-params-box'],
                        'bodyOptions' => ['class' => 'box-body']
                    ]);
                    ?>


                    <?php $form = ActiveForm::begin(['id' => 'post-form']); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered table-striped">
                                <colgroup>
                                    <col width="200">
                                    <col>
                                </colgroup>
                                <tbody>
                                <tr>
                                    <td><strong><?= $model->post->getAttributeLabel('created_by'); ?></strong></td>
                                    <td><?= f::text($model->post->createdBy ? $model->post->createdBy->fullName : null); ?></td>
                                </tr>
                                <tr>
                                    <td><strong><?= $model->post->getAttributeLabel('created_at'); ?></strong></td>
                                    <td><?= f::datetime($model->post->created_at ?? null); ?></td>
                                </tr>
                                <tr>
                                    <td><strong><?= $model->post->getAttributeLabel('updated_at'); ?></strong></td>
                                    <td><?= f::datetime($model->post->updated_at ?? null); ?></td>
                                </tr>
                                <tr>
                                    <td><strong><?= $model->getAttributeLabel('status'); ?></strong></td>
                                    <td><?= $form->field($model, 'status')->dropDownList(PostStatus::displayValues())->label(false); ?></td>
                                </tr>
                                <tr>
                                    <td><strong><?= $model->getAttributeLabel('identifier'); ?></strong></td>
                                    <td>
                                        <?= $form->field($model, 'identifier')->dropDownList($model->getIdentifiers(), [
                                            'prompt' => Yii::t('backend/main', 'Select {attribute}', ['attribute' => $model->getAttributeLabel('identifier')])
                                        ])->label(false);
                                        ?>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>


                    <?= Box::end(); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= Box::begin([
                    'showHeader' => false,
                    'options' => ['class' => 'box box-default'],
                    'bodyOptions' => ['class' => 'box-body']
                ]);
            ?>
            <div class="clearfix">

	            <?= $this->render('/partials/back-btn', ['size' => 'lg']); ?>

                <?= FormSubmitBtn::widget(['formCollection' => $formCollection]) ?>

	            <?= Html::a('<span class="fas fa-plus"></span>&nbsp;&nbsp;' . Yii::t('backend/post', 'New'), ['post/add-page'], [
	                    'class' => 'btn btn-flat btn-success btn-lg pull-right',
                        'style' => 'margin-right: 10px;'
                ]); ?>

            </div>

            <?= Box::end(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <br>
            <br>
            <br>
        </div>
    </div>

<?php $formCollection::end(); ?>

