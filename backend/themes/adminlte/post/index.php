<?php

use phycom\backend\widgets\Box;
use phycom\backend\widgets\DataGrid;
use phycom\common\helpers\Filter;

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $this yii\web\View
 * @var \phycom\backend\models\SearchPost $model
 * @var \yii\data\ActiveDataProvider $dataProvider
 */
$this->title = Yii::t('backend/post', 'Posts');
$this->params['breadcrumbs'][] = Yii::t('backend/post', 'Content');
?>
<div class="row">
	<div class="col-md-12">
		<?= Box::begin([
			'options' => ['class' => 'box box-default'],
			'showHeader' => false,
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

		<?= DataGrid::widget([
			'id' => 'posts-grid',
			'dataProvider' => $dataProvider,
			'filterModel' => $model,
			'rowOptions' => function ($model) {
				return [
					'class' => 'row-link',
					'data-url' => Url::toRoute(['/post/edit','id' => $model->id])
				];
			},
			'actions' => [
				[
					'label' => '<span class="fas fa-plus"></span>&nbsp;&nbsp;' . Yii::t('backend/post', 'Add post'),
					'url' => ['post/add'],
                    'options' => ['class' => 'btn btn-flat btn-success']
				]
			],
			'columns' => [
                [
	                'attribute' => 'id',
	                'format' => 'text',
	                'options' => ['width' => 80]
                ],
				[
					'attribute' => 'image',
					'format' => 'raw',
					'value' => function ($model) {
                        /**
                         * @var \phycom\backend\models\SearchPost $model
                         */
						return Html::img($model->getImageUrl()->medium, ['style' => 'max-width: 80px', 'height' => 45]);
					},
					'options' => ['width' => 80],
					'contentOptions' => ['style' => 'text-align: center; padding: 0;']
				],
				[
					'attribute' => 'title',
					'format' => 'text',
					'contentOptions' => ['style' => 'font-weight: bold;']
				],
				[
					'attribute' => 'status',
					'filter' => Filter::dropDown($model, 'status', \phycom\common\models\attributes\PostStatus::displayValues()),
					'format' => 'raw',
					'value' => function ($model) {
						return '<span class="label '.$model->status->labelClass.'">' . $model->status->label . '</span>';
					},
					'options' => ['width' => 200]
				],
				[
					'attribute' => 'created_at',
					'filter' => Filter::daterangepicker($model, 'createdFrom', 'createdTo'),
					'format' => 'datetime',
					'options' => ['width' => '220'],
					'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				],
				[
					'attribute' => 'updated_at',
					'filter' => Filter::daterangepicker($model, 'updatedFrom', 'updatedTo'),
					'format' => 'datetime',
					'options' => ['width' => '220'],
					'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				],
                [
                    'label' => false,
                    'format' => 'raw',
                    'content' => function($model) {

                        return $this->render('/partials/delete-dropdown', [
                            'label' => '<span class="far fa-trash-alt"></span>',
                            'url' => ['post/delete', 'id' => $model->id],
                            'options' => ['class' => 'toggle-hover'],
                            'side' => 'right'
                        ]);
                    },
                    'options' => ['width' => 40],
                    'contentOptions' => ['style' => 'padding: 2px 0 0 0;', 'class' => 'clickable']
                ]
			]
		]);
		?>

		<?= Box::end(); ?>
	</div>
</div>
