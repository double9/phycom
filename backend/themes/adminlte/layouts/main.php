<?php
use yii\helpers\Html;
use yii\helpers\Json;

/* @var $this \yii\web\View */
/* @var $content string */

phycom\backend\assets\AppAsset::register($this);
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');

$messages = Json::encode([
    'error' => Yii::t('backend/main', 'Error'),
]);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script type="text/javascript">
        var baseUrl = '<?= Yii::$app->urlManager->baseUrl; ?>';
        var i18n = JSON.parse('<?= $messages ?>');
    </script>
</head>
<body class="skin-custom hold-transition sidebar-mini fixed">
<?php $this->beginBody() ?>

<div class="wrapper">
    <?= $this->render('header', ['directoryAsset' => $directoryAsset]) ?>
    <?= $this->render('left', ['directoryAsset' => $directoryAsset]) ?>
    <?= $this->render('content', ['content' => $content, 'directoryAsset' => $directoryAsset]) ?>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
