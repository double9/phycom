<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

$user = Yii::$app->user->identity;
?>

<header class="main-header">

    <a href="<?= Url::home() ?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><span class="name"><?= Yii::$app->shortName ?></span></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><?= Yii::$app->name ?></span>
    </a>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <i class="fas fa-bars"></i>
            <span class="sr-only">Toggle navigation</span>
        </a>

        <?= $this->render('//search/search-form'); ?>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <?php //echo $this->render('header-messages-menu', ['directoryAsset' => $directoryAsset]); ?>
	            <?php //echo $this->render('header-notifications-menu', ['directoryAsset' => $directoryAsset]); ?>
	            <?php //echo $this->render('header-tasks-menu', ['directoryAsset' => $directoryAsset]); ?>


	            <?php echo \phycom\backend\widgets\Clock::widget(['tag' => 'li']); ?>

                <!-- User Account: style can be found in dropdown.less -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= $user->avatar ?>" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"><?= $user->fullName; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?= $user->avatar ?>" class="img-circle" alt="User Image"/>
                            <p>
	                            <?= $user->fullName; ?> - Admin
                                <small><?= Yii::t('backend/user','Member since'); ?> <?= $user->created_at->format('M\. Y'); ?></small>
                            </p>
                        </li>
                        <!-- Menu Body -->
<!--                        <li class="user-body">-->
<!--                            <div class="col-xs-4 text-center">-->
<!--                                <a href="#">Followers</a>-->
<!--                            </div>-->
<!--                            <div class="col-xs-4 text-center">-->
<!--                                <a href="#">Sales</a>-->
<!--                            </div>-->
<!--                            <div class="col-xs-4 text-center">-->
<!--                                <a href="#">Friends</a>-->
<!--                            </div>-->
<!--                        </li>-->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <?= Html::a(
	                                '<span class="far fa-user"></span>&nbsp;&nbsp;' .  Yii::t('backend/user', 'Profile'),
	                                ['user/profile', 'id' => $user->id],
                                    ['class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
	                                '<span class="fas fa-sign-out-alt"></span>&nbsp;&nbsp;' . Yii::t('backend/user', 'Sign out'),
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li>
                    <a href="#" class="disabled" data-toggle="control-sidebar"><i class="fas fa-cogs text-muted"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
