<aside class="main-sidebar">

    <section class="sidebar">

        <?= phycom\backend\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu', 'data-widget' => 'tree', 'data-animation-speed' => '100'],
                'items' => [
                    [
                        'label'   => Yii::t('backend/main', 'Dashboard'),
                        'icon'    => 'home',
                        'url'     => ['/site/index'],
                        'visible' => Yii::$app->dashboard->isEnabled()
                    ],
	                [
                        'label' => Yii::t('backend/main', 'Products'),
                        'icon' => 'cube',
                        'items' => [
                            [
                                'label'         => Yii::t('backend/main', 'Catalog'),
                                'icon'          => 'tags',
                                'url'           => ['/product/'],
                                'exclude_child' => ['/product/add'],
                                'visible'       => Yii::$app->user->can('search_products')
                            ],
                            [
                                'label'   => Yii::t('backend/main', 'Categories'),
                                'icon'    => 'th-list',
                                'url'     => ['/product-category/'],
                                'visible' => Yii::$app->user->can('search_product_categories')
                            ]
                        ],
                        'visible' => (
                            Yii::$app->commerce->isEnabled() &&
                            (
                                Yii::$app->user->can('search_products')
                                || Yii::$app->user->can('search_product_categories')
                                || Yii::$app->user->can('create_product')
                            )
                        )
                    ],
	                [
		                'label' => Yii::t('backend/main', 'Sale'),
		                'icon' => 'cart-arrow-down',
		                'items' => [
                            [
                                'label'   => Yii::t('backend/main', 'Orders'),
                                'icon'    => 'cart-plus',
                                'url'     => ['/order'],
                                'visible' => Yii::$app->user->can('search_orders')
                            ],
                            [
                                'label'   => Yii::t('backend/main', 'Invoices'),
                                'icon'    => 'file-alt',
                                'url'     => ['/invoice'],
                                'visible' => Yii::$app->user->can('search_invoices')
                            ],
                            [
                                'label'   => Yii::t('backend/main', 'Payments'),
                                'icon'    => 'euro-sign',
                                'url'     => ['/payment-list'],
                                'visible' => Yii::$app->user->can('search_payments')
                            ],
                            [
                                'label'   => Yii::t('backend/main', 'Statistics'),
                                'icon'    => 'chart-bar',
                                'url'     => ['/site/statistics'],
                                'visible' => Yii::$app->statistics->isEnabled() && Yii::$app->user->can('search_statistics')
                            ],
                            [
                                'label'   => Yii::t('backend/main', 'Reports'),
                                'icon'    => 'calculator',
                                'url'     => ['/report'],
                                'visible' => (bool)Yii::$app->getModule('report') && Yii::$app->user->can('search_reports')
                            ],
		                ],
                        'visible' => (
                            Yii::$app->commerce->isEnabled() && Yii::$app->commerce->sale->isEnabled() &&
                            (
                                Yii::$app->user->can('search_orders')
                                || Yii::$app->user->can('search_invoices')
                                || Yii::$app->user->can('search_payments')
                                || (Yii::$app->params['showStatistics'] && Yii::$app->user->can('search_statistics'))
                                || ((bool) Yii::$app->getModule('report') && Yii::$app->user->can('search_reports'))
                            )
                        ),
	                ],
                    [
                        'label' => Yii::t('backend/main', 'Delivery'),
                        'icon' => 'mdi-local-shipping',
                        'items' => [
                            [
                                'label'   => Yii::t('backend/main', 'Shipments'),
                                'icon'    => 'truck',
                                'url'     => ['/shipment'],
                                'visible' => Yii::$app->user->can('search_shipments')
                            ],
                            [
                                'label'   => Yii::t('backend/main', 'Delivery areas'),
                                'icon'    => 'mdi-place',
                                'url'     => ['/delivery-area'],
                                'visible' => Yii::$app->user->can('search_delivery_areas')
                            ]
                        ],
                        'visible' => (
                            Yii::$app->commerce->isEnabled() && Yii::$app->commerce->delivery->isEnabled() &&
                            (
                                Yii::$app->user->can('search_shipments')
                                || Yii::$app->user->can('search_delivery_areas')
                            )
                        )
                    ],
	                [
		                'label' => Yii::t('backend/main', 'Marketing'),
		                'icon' => 'bullhorn',
		                'items' => [
                            [
                                'label'   => Yii::t('backend/main', 'Promotion codes'),
                                'icon'    => 'qrcode',
                                'url'     => ['/promotion-code/'],
                                'visible' => Yii::$app->commerce->isEnabled() && Yii::$app->commerce->promoCodes->isEnabled() && Yii::$app->user->can('search_promotion_codes')
                            ],
                            [
                                'label'   => Yii::t('backend/main', 'Client cards'),
                                'icon'    => 'sticky-note',
                                'url'     => ['/client-card/'],
                                'visible' => Yii::$app->commerce->isEnabled() && Yii::$app->commerce->clientCards->isEnabled() && Yii::$app->user->can('search_client_cards')
                            ],
                            [
                                'label'   => Yii::t('backend/main', 'Subscriptions'),
                                'icon'    => 'far fa-bookmark',
                                'url'     => ['/subscription/'],
                                'visible' => Yii::$app->subscription->isEnabled() && Yii::$app->user->can('search_newsletter_subscriptions')
                            ],
		                ],
                        'visible' => (
                            Yii::$app->commerce->isEnabled() &&
                            (
                                (Yii::$app->commerce->promoCodes->isEnabled() && Yii::$app->user->can('search_promotion_codes'))
                                || (Yii::$app->commerce->clientCards->isEnabled() && Yii::$app->user->can('search_client_cards'))
                            )
                        ) || (Yii::$app->subscription->isEnabled() && Yii::$app->user->can('search_newsletter_subscriptions'))

                    ],
	                [
		                'label' => Yii::t('backend/main', 'Shops'),
		                'icon' => 'mdi-store',
		                'url' => ['/shop'],
                        'visible' => Yii::$app->commerce->isEnabled() && Yii::$app->commerce->shop->isEnabled() && Yii::$app->user->can('search_shops')
	                ],
	                [
		                'label' => Yii::t('backend/main', 'Content'),
		                'icon' => 'mdi-description',
		                'items' => [
                            [
                                'label'  => Yii::t('backend/main', 'Landing pages'),
                                'icon'   => 'desktop',
                                'url'    => ['/post/landing-pages'],
                                'active' => function ($item, $hasActiveChild, $isItemActive, $widget) {
                                    return $isItemActive || $hasActiveChild || in_array($widget->route, ['post/add-landing-page']);
                                }
                            ],
                            [
                                'label'  => Yii::t('backend/main', 'Web Pages'),
                                'icon'   => 'edit',
                                'url'    => ['/post/pages'],
                                'active' => function ($item, $hasActiveChild, $isItemActive, $widget) {
                                    return $isItemActive || $hasActiveChild || in_array($widget->route, ['post/add-page']);
                                }
                            ],
                            [
                                'label'   => Yii::t('backend/main', 'Blog posts'),
                                'icon'    => 'quote-right',
                                'url'     => ['/post/index'],
                                'active'  => function ($item, $hasActiveChild, $isItemActive, $widget) {
                                    return $isItemActive || $hasActiveChild || in_array($widget->route, ['post/add']);
                                },
                                'visible' => Yii::$app->blog->isEnabled()
                            ],
                            [
                                'label'   => Yii::t('backend/main', 'Blog Categories'),
                                'icon'    => 'th-list',
                                'url'     => ['/post-category/index'],
                                'visible' => Yii::$app->blog->isEnabled()
                            ],
		                ],
                        'active' => function ($item, $hasActiveChild, $isItemActive, $widget) {
                            return $isItemActive || $hasActiveChild || in_array($widget->route, [
                                    'post/add',
                                    'post/add-page',
                                    'post/add-landing-page',
                                    'post/landing-pages',
                                    'post/pages',
                                    'post/index'
                                ]);
                        }
	                ],
	                [
		                'label' => Yii::t('backend/main', 'Users'),
		                'icon' => 'users',
		                'url' => ['/user']
	                ],
                    [
                        'label' => Yii::t('backend/main', 'Other'),
                        'icon' => 'mdi-all-inclusive',
                        'items' => [
                            [
                                'label'   => Yii::t('backend/main', 'Comments'),
                                'icon'    => 'mdi-comment',
                                'url'     => ['/comment'],
                                'visible' => Yii::$app->comments->isEnabled()
                            ],
                            [
                                'label'   => Yii::t('backend/main', 'Reviews'),
                                'icon'    => 'star',
                                'url'     => ['/review'],
                                'visible' => Yii::$app->reviews->isEnabled()
                            ],
                            [
                                'label'   => Yii::t('backend/main', 'Partner contracts'),
                                'icon'    => 'mdi-accessibility',
                                'url'     => ['/gdpr'],
                                'visible' => Yii::$app->partnerContracts->isEnabled()
                            ],
                        ],
                    ],
	                [
		                'label' => Yii::t('backend/main', 'Settings'),
		                'icon' => 'cogs',
		                'url' => ['/settings'],
                        'visible' => Yii::$app->user->can('update_global_settings')
	                ]
                ],
            ]
        ) ?>

    </section>

</aside>
