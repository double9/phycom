<?php
/**
 * @var string $content
 */
use dmstr\widgets\Alert;

use yii\widgets\Breadcrumbs;
use yii\helpers\Inflector;
use yii\helpers\Html;
?>
<div class="content-wrapper">
    <section class="content-header">
        <?php if (isset($this->blocks['content-header'])): ?>
            <h1 id="main-content-title"><?= $this->blocks['content-header'] ?></h1>
        <?php else : ?>
            <h1 id="main-content-title">
                <?php
                    if ($this->title !== null) {
                        if (isset($this->params['titleLabel'])) {
                            echo $this->params['titleLabel'] . ' - ';
                        }
                        echo Html::encode($this->title);
                    } else {
                        echo Inflector::camel2words(Inflector::id2camel($this->context->module->id));
                        echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                    }
                ?>
            </h1>
        <?php endif; ?>

	    <?= Breadcrumbs::widget([
	            'homeLink' => [
	                'label' => '<span class="fas fa-home"></span>' . '&nbsp;' . Yii::t('backend/main', 'Home'),
                    'url' => ['/site/index'],
		            'encode' => false,
                ],
			    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		    ])
        ?>
    </section>

    <section id="main-content" class="content clearfix">
        <div id="system-messages">
	        <?= Alert::widget() ?>
        </div>
        <?= $content ?>
    </section>
</div>

<!--<footer class="main-footer"></footer>-->

<?php //echo $this->render('right-sidebar'); ?>
