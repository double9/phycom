<?php

use phycom\backend\widgets\Box;
use phycom\backend\widgets\DataGrid;

use phycom\common\helpers\ProductCategoryLabel;
use phycom\common\models\attributes\CategoryStatus;
use phycom\common\helpers\Filter;

use yii\helpers\Json;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/**
 * @var $this yii\web\View
 * @var \phycom\backend\models\product\SearchProduct $model
 * @var \yii\data\ActiveDataProvider $dataProvider
 */
$this->title = Yii::t('backend/product', 'Catalog');
$this->params['breadcrumbs'][] = Yii::t('backend/product', 'Product catalog');
?>
<div class="row">
	<div class="col-md-12">
		<?= Box::begin([
			'options' => ['class' => 'box box-default'],
			'showHeader' => false,
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

		<?= DataGrid::widget([
			'id' => 'product-grid',
			'dataProvider' => $dataProvider,
			'filterModel' => $model,
            'bulkEditForm' => 'bulk-edit',
			'rowOptions' => function ($model) {
				return [
					'class' => 'row-link',
					'data-url' => Url::toRoute(['/product/edit','id' => $model->id])
				];
			},
			'actions' => [
                [
					'label' => '<span class="fas fa-plus"></span>&nbsp;&nbsp;' . Yii::t('backend/product', 'Add product'),
					'url' => ['/product/add'],
                    'options' => ['class' => 'btn btn-flat btn-success', 'style' => 'margin-left: 5px;']
				],
                [
                    'label' => '<span class="far fa-save"></span>&nbsp;&nbsp;' . Yii::t('backend/product', 'Export'),
                    'url' => ArrayHelper::merge(['/product'], Yii::$app->request->getQueryParams(), ['export' => 1]),
                    'options' => ['class' => 'btn btn-flat btn-default']
                ]
			],
			'columns' => [
                [
	                'attribute' => 'sku',
	                'format' => 'text',
	                'options' => ['width' => 140]
                ],
				[
					'attribute' => 'image',
					'format' => 'raw',
					'value' => function ($model) {
                        /**
                         * @var \backend\models\product\SearchProduct $model
                         */
						return Html::img($model->getImageUrl()->medium, ['style' => 'max-width: 80px', 'height' => 45]);
					},
					'options' => ['width' => 80],
					'contentOptions' => ['style' => 'text-align: center; padding: 0;']
				],

				[
					'attribute' => 'title',
					'format' => 'text',
					'contentOptions' => ['style' => 'font-weight: bold;']
				],
                [
                    'label' => Yii::t('backend/product', 'Categories'),
                    'filter' => Filter::selectDropDown($model, 'categoryIds', (new ProductCategoryLabel())->generateLabels()),
                    'format' => 'html',
                    'value' => function ($model) {
                        $html = '';
                        if (!empty($model->categoryData)) {
                            foreach (Json::decode($model->categoryData) as $id => list($label, $status)) {
                                $html .= '<span class="label ' . (new CategoryStatus($status))->labelClass . '" data-id="' . $id . '" style="margin-right: 3px;">' . $label . '</span>';
                            }
                        }
                        return $html;
                    },
                    'options' => ['width' => 340]
                ],
                [
                    'label' => Yii::t('backend/product', 'Tags'),
                    'filter' => Filter::selectDropDown($model, 'tags', ArrayHelper::map(Yii::$app->modelFactory->getProductTag()::all(), 'value', 'label')),
                    'format' => 'html',
                    'value' => function ($model) {
                        $html = '';
                        if (!empty($model->tags)) {
                            foreach ($model->tags as $value) {
                                $tag = Yii::$app->modelFactory->getProductTag()->setValue($value);
                                $html .= '<span class="label label-default" data-tag="' . $value .  '" style="margin-right: 3px;">' . $tag->label . '</span>';
                            }
                        }
                        return $html;
                    },
                    'options' => ['width' => 180]
                ],
				[
					'attribute' => 'price',
					'format' => 'currency',
					'options' => ['width' => '120'],
					'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				],
				[
					'attribute' => 'status',
					'filter' => Filter::selectDropDown($model, 'status', \phycom\common\models\attributes\ProductStatus::displayValues()),
					'format' => 'raw',
					'value' => function ($model) {
						return '<span class="label '.$model->status->labelClass.'">' . $model->status->label . '</span>';
					},
					'options' => ['width' => 200]
				],
				[
					'attribute' => 'created_at',
					'filter' => Filter::daterangepicker($model, 'createdFrom', 'createdTo'),
					'format' => 'datetime',
					'options' => ['width' => '220'],
					'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				],
				[
					'attribute' => 'updated_at',
					'filter' => Filter::daterangepicker($model, 'updatedFrom', 'updatedTo'),
					'format' => 'datetime',
					'options' => ['width' => '220'],
					'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				],
                [
                    'label' => false,
                    'format' => 'raw',
                    'content' => function($model) {

                        return $this->render('/partials/delete-dropdown', [
                            'label' => '<span class="far fa-trash-alt"></span>',
                            'url' => ['product/delete', 'id' => $model->id],
                            'options' => ['class' => 'toggle-hover'],
                            'side' => 'right'
                        ]);
                    },
                    'options' => ['width' => 40],
                    'contentOptions' => ['style' => 'padding: 2px 0 0 0;', 'class' => 'clickable']
                ]
			]
		]);
		?>

		<?= Box::end(); ?>
	</div>
</div>
