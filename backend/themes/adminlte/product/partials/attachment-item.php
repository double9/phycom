<?php

/**
 * @var $this yii\web\View
 * @var $model phycom\backend\models\product\ProductAttachmentForm
 * @var $form \phycom\backend\widgets\ActiveForm
 * @var $attachment \phycom\common\models\product\ProductAttachment
 */

use phycom\common\models\File;

use yii\helpers\Html;
use yii\helpers\Url;

?>

<?= Html::beginTag('div', [
    'class'           => 'attachment-item initial-item',
    'data-update-url' => Url::toRoute(['/product/update-attachment', 'id' => $attachment->product_id, 'fileId' => $attachment->file_id]),
    'data-toggle'     => 'tooltip',
    'title'           => $attachment->file->name,
    'data-placement'  => 'top',
    'data-container'  => 'body',
    'data-file-id'    => $attachment->file_id,
])
?>
    <div class="item-content">
	    <?= Html::img(null, ['data-src' => $attachment->file->getThumbUrl(File::THUMB_SIZE_SMALL), 'class' => 'file-preview-image']); ?>
    </div>
    <div class="item-options">
        <?php if (!$attachment->is_visible): ?>
            <span class="label label-default"><?= Yii::t('backend/main', 'Hidden'); ?></span>
        <?php endif; ?>
    </div>

<?= Html::endTag('div'); ?>
