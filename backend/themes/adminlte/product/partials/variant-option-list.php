<?php

/**
 * @var string $key
 * @var ProductVariant $productVariant
 * @var ProductVariantForm $form
 * @var MultiFormGridDataColumn $column
 */
use phycom\backend\widgets\Modal;
use phycom\backend\widgets\MultiFormGridDataColumn;
use phycom\backend\models\product\ProductVariantForm;

use phycom\common\models\product\ProductVariant;
use phycom\common\models\product\ProductVariantOption;

use yii\helpers\Inflector;

$option = new ProductVariantOption();
$formName = $form->formName();


$modal = Modal::begin([
    'id'     => uniqid(),
    'title'  => $productVariant->label,
    'button' => [
        'label' => '<i class="fas fa-wrench"></i>',
        'class' => 'btn btn-primary pull-right',
    ]
]);
?>


    <table class="table table-condensed table-bordered no-margin field-option-table" style="background-color: #fafafa;">
        <colgroup>
            <col width="40">
            <col width="176">
            <col>
            <col width="40">
        </colgroup>
        <thead>
        <tr>
            <th><?= $option->getAttributeLabel('is_visible') ?></th>
            <th><?= $option->getAttributeLabel('key') ?></th>
            <th><?= $option->getAttributeLabel('sku') ?></th>
            <th><?= $form->getAttributeLabel('defaultOption') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($form->getMergedModelOptions($key) as $i => $option): ?>
            <tr>
                <td>
                    <?= $column->grid->form->field($option, 'is_visible', [
                        'template' => "<div class=\"checkbox\">\n{input}\n{hint}\n</div>",
                        'options'  => ['style' => 'margin-bottom: 0;']
                    ])->checkbox([
                        'id'    => Inflector::camel2id($formName) . '-' . $key . '-options-' . $i . '-is_visible',
                        'name'  => $formName . "[options][$key][$i][is_visible]",
                    ], false) ?>
                </td>
                <td>
                    <span class="label label-default option-label"><?= $option->getLabel() ?></span>
                    <?= $column->grid->form->field($option, 'key', [
                        'template' => '{input}',
                        'options'  => ['style' => 'margin-bottom: 0;']
                    ])->hiddenInput([
                        'id'          => Inflector::camel2id($formName) . '-' . $key . '-options-' . $i . '-key',
                        'name'        => $formName . "[options][$key][$i][key]"
                    ])->label(false) ?>
                </td>
                <td>
                    <?= $column->grid->form->field($option, 'sku', [
                        'template' => '{input}',
                        'options'  => ['style' => 'margin-bottom: 0;']
                    ])->textInput([
                        'id'          => Inflector::camel2id($formName) . '-' . $key . '-options-' . $i . '-sku',
                        'name'        => $formName . "[options][$key][$i][sku]",
                        'placeholder' => $option->getAttributeLabel('sku'),
                        'class'       => 'form-control input-sm'
                    ])->label(false) ?>
                </td>
                <td>
                    <?= $column->grid->form->field($productVariant, 'defaultOption', [
                        'template' => "{input}",
                        'options'  => ['style' => 'margin-bottom: 0;']
                    ])->radio([
                        'id'      => Inflector::camel2id($formName) . '-' . $key . '-default-option-' . $i,
                        'name'    => $formName . "[defaultOption][$key]",
                        'value'   => $option->key,
                        'uncheck' => null,
                        'checked' => $form->getDefaultOption($key) === $option->key
                    ], false) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

<?php $modal::end() ?>

