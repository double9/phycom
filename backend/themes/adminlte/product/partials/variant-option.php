<?php

/**
 * @var string $key
 * @var ProductVariant $productVariant
 * @var ProductVariantForm $form
 * @var MultiFormGridDataColumn $column
 */

use phycom\backend\widgets\Modal;
use phycom\backend\widgets\MultiFormGridDataColumn;
use phycom\backend\models\product\ProductVariantForm;

use phycom\common\models\product\Variant;
use phycom\common\models\product\ProductVariant;
use phycom\common\models\product\ProductVariantOption;

use yii\helpers\Inflector;

$option = new ProductVariantOption();
$formName = $form->formName();

$modal = Modal::begin([
    'id'     => uniqid(),
    'title'  => $productVariant->label,
    'button' => [
        'label' => '<i class="fas fa-wrench"></i>',
        'class' => 'btn btn-primary pull-right',
    ]
]);
?>


    <table class="table table-condensed table-bordered no-margin field-option-table single-option" style="background-color: #fafafa;">
        <colgroup>
            <col width="200">
            <col>
        </colgroup>
        <thead>
        <tr>
            <th><?= $productVariant->variant->getAttributeLabel('type') ?></th>
            <th><?= $option->getAttributeLabel('sku') ?></th>

            <?php if (in_array($productVariant->variant->type, [Variant::TYPE_NUMBER, Variant::TYPE_TEXT, Variant::TYPE_BOOL])): ?>
                <th><?= $option->getAttributeLabel('default_value') ?></th>
            <?php endif; ?>
        </tr>
        </thead>
        <tbody>
            <?php foreach ($form->getMergedModelOptions($key) as $i => $option): ?>
                <tr>
                    <td>
                        <span class="label label-warning field-type-label"><?= $productVariant->variant->getTypeLabel(); ?></span>
                    </td>
                    <td>

                        <?= $column->grid->form->field($option, 'key', [
                            'template' => '{input}',
                            'options'  => ['style' => 'margin-bottom: 0;']
                        ])->hiddenInput([
                            'id'          => Inflector::camel2id($formName) . '-' . $key . '-options-' . $i . '-key',
                            'name'        => $formName . "[options][$key][$i][key]"
                        ])->label(false) ?>


                        <?= $column->grid->form->field($option, 'is_visible', [
                            'template' => '{input}',
                            'options'  => ['style' => 'margin-bottom: 0;']
                        ])->hiddenInput([
                            'id'    => Inflector::camel2id($formName) . '-' . $key . '-options-' . $i . '-is_visible',
                            'name'  => $formName . "[options][$key][$i][is_visible]",
                            'value' => '1'
                        ])->label(false) ?>


                        <?= $column->grid->form->field($productVariant, 'defaultOption', [
                            'template' => "{input}",
                            'options'  => ['style' => 'margin-bottom: 0;']
                        ])->hiddenInput([
                            'id'      => Inflector::camel2id($formName) . '-' . $key . '-default-option-' . $i,
                            'name'    => $formName . "[defaultOption][$key]",
                            'value'   => $option->key,
                        ])->label(false) ?>


                        <?= $column->grid->form->field($option, 'sku', [
                            'template' => '{input}',
                            'options'  => ['style' => 'margin-bottom: 0;']
                        ])->textInput([
                            'id'          => Inflector::camel2id($formName) . '-' . $key . '-options-' . $i . '-sku',
                            'name'        => $formName . "[options][$key][$i][sku]",
                            'placeholder' => $option->getAttributeLabel('sku'),
                            'class'       => 'form-control input-sm'
                        ])->label(false) ?>

                    </td>

                    <?php if (in_array($option->productVariant->variant->type, [Variant::TYPE_NUMBER, Variant::TYPE_TEXT])): ?>
                        <td>
                            <?= $column->grid->form->field($option, 'default_value', [
                                'template' => '{input}',
                                'options'  => ['style' => 'margin-bottom: 0;']
                            ])->textInput([
                                'id'          => Inflector::camel2id($formName) . '-' . $key . '-options-' . $i . '-default_value',
                                'name'        => $formName . "[options][$key][$i][default_value]",
                                'placeholder' => $option->getAttributeLabel('default_value'),
                                'class'       => 'form-control input-sm'
                            ])->label(false) ?>
                        </td>
                    <?php elseif ($option->productVariant->variant->type === Variant::TYPE_BOOL): ?>
                        <td>
                            <?= $column->grid->form->field($option, 'default_value', [
                                'template' => '{input}',
                                'options'  => ['style' => 'margin-bottom: 0;']
                            ])->boolean(true, [
                                'id'          => Inflector::camel2id($formName) . '-' . $key . '-options-' . $i . '-default_value',
                                'name'        => $formName . "[options][$key][$i][default_value]",
                                'class'       => 'form-control input-sm'
                            ])->label(false) ?>
                        </td>
                    <?php endif; ?>

                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

<?php $modal::end() ?>
