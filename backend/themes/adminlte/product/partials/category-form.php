<?php

use phycom\backend\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \phycom\backend\models\product\ProductCategoryForm $model
 * @var string $action
 */
?>

<div class="category-form">

	<?php $form = ActiveForm::begin([
	        'id' => 'product-category-form-' . $model->language,
            'options' => ['class' => 'product-category-form', 'data-language' => $model->language],
            'action' => $action
        ]);
	?>


    <div class="row">
        <div class="col-md-12">
            <h2 class="category-title" style="margin-top: 0;">
                <?php if ($model->category->isNewRecord): ?>
                    <?= Yii::t('backend/product', 'Create new category') ?>
                <?php else: ?>
                    <?= $model->category->id . ' - ' . $model->title; ?>
                <?php endif; ?>
            </h2>
        </div>
    </div>


	<?= $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-md-6">
	        <?= $form->field($model, 'parent')->dropDownList($model->parents, [
                    'prompt' => Yii::t('backend/main', 'None'),
                    'class' => 'form-control common-attribute'
                ]);
	        ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'featured')->boolean(); ?>
        </div>
    </div>

    <?= $form->field($model, 'title')->textInput(); ?>
	<?= $form->field($model, 'description')->smallEditor(['rows' => 3], 'product-category-pajax'); ?>

    <div id="seo-container-<?= $model->language; ?>">

		<?= Html::a(Yii::t('backend/product', 'Toggle SEO attributes'), '#seo-' . $model->language, [
                'class' => 'collapsed btn btn-default btn-flat',
                'aria-expanded' => 'false',
                'data-parent' => '#seo-container-' . $model->language,
                'data-toggle' => 'collapse'
            ]);
		?>

        <div id="seo-<?= $model->language; ?>" class="collapse" aria-expanded="false" style="height:0;">
            <br />
			<?= $form->field($model, 'urlKey')->textInput(); ?>
			<?= $form->field($model, 'metaTitle')->textInput(); ?>
			<?= $form->field($model, 'metaKeywords')->textInput(); ?>
			<?= $form->field($model, 'metaDescription')->textInput(); ?>
        </div>

    </div>

    <div class="row">
        <br />
        <div class="col-md-4">
			<?= $form->field($model, 'translationStatus')->dropDownList($model->translationStatuses); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
			<?= $form->field($model, 'status')->dropDownList($model->statuses, [
                    'class' => 'form-control common-attribute'
                ]);
			?>
        </div>
    </div>

	<?php ActiveForm::end(); ?>

</div>
