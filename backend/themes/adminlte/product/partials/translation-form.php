<?php
/**
 * @var $this yii\web\View
 * @var $model phycom\backend\models\product\ProductTranslationForm
 * @var \phycom\common\models\Language $activeLanguage
 */

use phycom\backend\widgets\ActiveForm;

use phycom\common\models\attributes\TranslationStatus;
use phycom\common\helpers\f;

use yii\helpers\Html;
use yii\helpers\Url;

$box = $box ?? true;
$activeLanguage = $activeLanguage ?? Yii::$app->lang->default;
$modelName = (new \ReflectionClass($model))->getShortName();

$form = ActiveForm::begin([
    'id'              => 'product-translation-form',
    'action'          => ['product/edit'],
    'successCssClass' => 'has-success-1' // disable success colors
]);
?>


<div class="nav-tabs-custom" <?= $box ? '' : 'style="box-shadow: none; margin-bottom: 0;"' ?>>
    <ul class="nav nav-tabs" role="tablist">
        <?php foreach($model->translations as $language => $translation): ?>
            <li role="presentation" <?= ($activeLanguage->code === $language ? 'class="active"' : '') ?>>
                <a data-target="#content-language-<?= $language; ?>" aria-expanded="true" role="tab" data-toggle="tab">
                    <?= f::html('<b>' . strtoupper($translation->languageModel->code) . '</b> (' . $translation->languageModel->native . ')'); ?>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
    <div class="tab-content" <?= $box ? '' : 'style="border-left: 0; border-right: 0; border-bottom: 0; padding-left: 0; padding-right: 0; padding-bottom: 0;"' ?>>
	    <?php foreach($model->translations as $language => $translation): ?>
            <div role="tabpanel" class="tab-pane<?= ($activeLanguage->code === $language ? ' active' : '') ?>" id="content-language-<?= $language; ?>">


	            <?= $form->field($translation, 'title', ['enableClientValidation' => false])
                        ->textInput([
                            'id' => "translations-$language-title",
                            'name' => $modelName . "[translations][$language][title]"
                        ])
                        ->label($translation->getAttributeLabel('title'));
	            ?>
                <?= $form->field($translation, 'outline')
                    ->textarea([
                        'id' => "translations-$language-outline",
                        'name' => $modelName . "[translations][$language][outline]",
                        'style' => 'min-width: 100%; min-height: 30px;'
                    ])
                    ->label($translation->getAttributeLabel('outline'));
                ?>
	            <?= $form->field($translation, 'description')
                        ->editor([
                            'options' => [
                                'rows' => 5,
                                'id'   => "translations-$language-description",
                                'name' => $modelName . "[translations][$language][description]"
                            ]
                        ])
                        ->label($translation->getAttributeLabel('description'));
	            ?>


                <div class="row">
                    <div class="col-lg-4 col-md-5">
	                    <?= $form->field($translation, 'status')
                                ->dropDownList(TranslationStatus::displayValues(), [
                                    'id' => "translations-$language-status",
                                    'name' =>  $modelName . "[translations][$language][status]"
                                ])
                                ->label($translation->getAttributeLabel('status'));
	                    ?>
                    </div>
                    <div class="col-lg-8 col-md-7 clearfix">
	                    <?= Html::a(Yii::t('backend/product', 'Toggle SEO attributes'), '#seo-' . $language, [
                                'class' => 'collapsed btn btn-default btn-flat pull-right',
                                'aria-expanded' => 'false',
                                'data-parent' => '#seo-container-' . $language,
                                'data-toggle' => 'collapse',
                                'style' => 'margin-top: 25px;'
                            ]);
	                    ?>
                    </div>
                </div>

                <div id="seo-container-<?= $language; ?>">

                    <div id="seo-<?= $language; ?>" class="collapse" aria-expanded="false" style="height:0;">
                        <br />
			            <?= $form->field($translation, 'url_key')
                                ->textInput([
                                    'id' => "translations-$language-url_key",
                                    'name' =>  $modelName . "[translations][$language][url_key]"
                                ])
                                ->label($translation->getAttributeLabel('url_key'));
			            ?>
			            <?= $form->field($translation, 'meta_title')
                                ->textInput([
                                    'id' => "translations-$language-meta_title",
                                    'name' =>  $modelName . "[translations][$language][meta_title]"
                                ])
                                ->label($translation->getAttributeLabel('meta_title'));
			            ?>
			            <?= $form->field($translation, 'meta_keywords')
                                ->textInput([
                                    'id' => "translations-$language-meta_keywords",
                                    'name' =>  $modelName . "[translations][$language][meta_keywords]"
                                ])
                                ->label($translation->getAttributeLabel('meta_keywords'));
			            ?>
			            <?= $form->field($translation, 'meta_description')
                            ->textInput([
                                'id' => "translations-$language-meta_description",
                                'name' =>  $modelName . "[translations][$language][meta_description]"
                            ])
                            ->label($translation->getAttributeLabel('meta_description'));
			            ?>
                    </div>

                </div>

            </div>
	    <?php endforeach; ?>
    </div>
</div>

<?php ActiveForm::end(); ?>


