<?php
/**
 * @var $this yii\web\View
 * @var $model \phycom\backend\models\product\ProductPricingForm
 */
use phycom\backend\widgets\MultiFormGrid;
use phycom\backend\models\product\SearchProductPrice;

use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

$searchModel = SearchProductPrice::create($model->product);

if ($model->getProduct()->isNewRecord) {
	$dataProvider = new ArrayDataProvider([
		'allModels'  => $model->getModels(),
		'modelClass' => get_class(Yii::$app->modelFactory->getProductPrice())
	]);
} else {
	$dataProvider = $searchModel->search();
}

$currencySymbol = Yii::$app->locale->getCurrencySymbol(Yii::$app->formatter->currencyCode);

echo MultiFormGrid::widget([
	'id'                 => 'product-price-grid',
	'dataProvider'       => $dataProvider,
	'formModel'          => $model,
	'formModelAttribute' => 'models',
	'templateModel'      => SearchProductPrice::create($model->product),
	'formOptions'        => [
		'validateOnBlur'   => false,
		'validateOnChange' => false
	],
	'columns'            => ArrayHelper::merge(
		[
			[
				'attribute'     => 'sku',
				'enableSorting' => false,
				'format'        => 'text',
				'options'       => ['width' => 200],
				'visible'		=> Yii::$app->commerce->usePricingSkuPerUnit
			],
			[
				'attribute'     => 'num_units',
				'enableSorting' => false,
				'format'        => 'decimalValue',
				'inputAppend'   => function ($model) {
					/**
					 * @var SearchProductPrice $model
					 */
					if ($model->isNewRecord && $model->product) {
						$unit = $model->product->price_unit;
					} else if (!$model->product) {
						$unit = Yii::$app->modelFactory->getProduct()->loadDefaultValues()->price_unit;
					} else {
						$unit = $model->unit_type;
					}
					return $unit->unitLabel;
				},
				'options'       => ['width' => 120]
			],
			[
				'attribute'     => 'price',
				'enableSorting' => false,
				'format'        => 'currencyValue',
				'inputAppend'   => $currencySymbol,
				'options'       => ['width' => 130]
			],
			[
				'attribute'     => 'discount_amount',
				'enableSorting' => false,
				'format'        => 'currencyValue',
				'inputAppend'   => $currencySymbol,
				'options'       => ['width' => 130]
			]
		],
		$searchModel->getOptionFieldColumns(
			[
				'inputAppend'   => $currencySymbol,
				'enableSorting' => false,
				'options'       => ['width' => 130],
				'headerOptions' => ['class' => 'variation']
			]
		),
		[
			[
				'label'         => '',
				'enableSorting' => false,
				'value'         => '',
			]
		]
	)
]);
?>
