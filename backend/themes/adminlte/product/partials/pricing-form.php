<?php
/**
 * @var \yii\web\View $this
 * @var \phycom\backend\models\product\ProductPricingForm $formModel
 * @var \phycom\backend\models\product\ProductPricingOptionsForm $optionsForm
 */
?>

<div class="row">
    <div class="col-md-12">
        <?= $this->render('pricing-options', ['model' => $optionsForm]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $this->render('pricing-grid', ['model' => $formModel]) ?>
    </div>
</div>




