<?php
/**
 * @var $this yii\web\View
 * @var $model phycom\backend\models\product\ProductAttachmentForm
 */
use phycom\backend\widgets\ActiveForm;
use phycom\backend\assets\FileInputAsset;
use phycom\backend\widgets\Modal;

use rmrevin\yii\fontawesome\FAS;

use yii\helpers\Url;
use yii\helpers\Html;

FileInputAsset::register($this);

$modelName = (new \ReflectionClass($model))->getShortName();

echo Html::beginTag('div', ['class' => 'attachments-modal-container']);
foreach ($model->getAttachments() as $attachment) {
    echo Modal::widget([
        'id'      => 'attachment_' . $attachment->file_id . '_modal',
        'title'   => $attachment->file->filename,
        'content' => $this->render('attachment-item-options', ['attachment' => $attachment]),
        'actions' => [
            [
                'title' => FAS::i(FAS::_CHECK) . '&nbsp;&nbsp;' . Yii::t('backend/main', 'Save'),
                'class' => 'btn-primary btn-save',
                'url'   => ['product/update-attachment', 'id' => $attachment->product_id, 'fileId' => $attachment->file_id]
            ],
            [
                'title' => FAS::i(FAS::_TIMES) . '&nbsp;&nbsp;' . Yii::t('backend/main', 'Close'),
                'class' => 'btn-default btn-close'
            ]
        ],
        'options' => ['size' => 'lg']
    ]);
}
echo Html::endTag('div');


$form = ActiveForm::begin(['id' => 'product-attachment-form', 'options' => ['class' => 'file-input-form']]);

$initialPreview = [];
$initialPreviewConfig = [];

foreach ($model->getAttachments() as $key => $attachment) {
    $initialPreview[] = $this->render('attachment-item', [
        'model'      => $model,
        'form'       => $form,
        'attachment' => $attachment
    ]);
    $initialPreviewConfig[] = [
        'caption'       => $attachment->file->name,
        'width'         => '120px',
        'key'           => $attachment->file->id,
        'previewAsData' => false
    ];

    if ($attachment->isNewRecord) {
        echo $form->field($attachment, 'file_id')->hiddenInput([
            'id'   => "attachments-$key-file-id",
            'name' => $modelName . "[attachments][$key][file_id]"
        ])->label(false);
        echo $form->field($attachment, 'order')->hiddenInput([
            'id'   => "attachments-$key-order",
            'name' => $modelName . "[attachments][$key][order]"
        ])->label(false);
    }
}

echo $form->field($model, 'file')->fileInput([
		'options' => [
		    'multiple' => true,
            'class' => 'attachment-form-input',
            'accept' => 'image/*',
        ],
		'pluginOptions' => [
			'theme' => 'fa',
			'browseOnZoneClick' => true,
			'uploadAsync' => true,
            'enableResumableUpload' => false,
			'autoReplace' => false,
			'overwriteInitial' => false,
			'maxFileCount' => $model->maxFiles,
			'allowedFileExtensions' => $model->allowedExtensions,
			'uploadUrl' => Url::toRoute(['product/upload-attachment', 'id' => $model->product->id]),
			'deleteUrl' => Url::toRoute(['product/delete-attachment', 'id' => $model->product->id]),
			'showClose'  => false,
			'showRemove'  => false,
			'showCaption' =>  false,
			'showBrowse' => false,
			'preferIconicPreview' => false,
			'browseClass' => 'btn btn-sm btn-primary btn-flat',
			'browseIcon' => '<span class="far fa-image"></span> ',
			'browseLabel' => Yii::t('backend/main','Browse'),
			'uploadClass' => 'btn btn-sm btn-default btn-flat file-upload-btn',
			'uploadLabel' => Yii::t('backend/main','Save'),
			'initialPreviewShowDelete' => true,
			'initialPreviewAsData' => true,
			'initialPreview' => $initialPreview,
            'initialPreviewConfig' => $initialPreviewConfig,
			'previewTemplates' => [
				'image' => $this->render('attachment-item-preview')
			],
			'previewSettings' => [
			    'image' => ['width' => "auto", 'height' => "auto", 'style' => 'max-height: 200px;'],
                'key' => 1
            ],
			'layoutTemplates' => [
				'main2' => '{preview}',
				'actions' => '<div class="file-footer-buttons">{delete}</div>{drag}',
				'progress' => '',
				'footer' => $this->render('attachment-item-footer')
			],
			'fileActionSettings' => [
				'showDrag' => true,
				'dragIcon' => '<span class="fas fa-bars"></span> ',
				'dragClass' => 'drag',
				'dragTitle' => Yii::t('backend/main','Drag'),
//				'dragSettings' => []
			],
			'customPreviewTags' => [],
//			'uploadExtraData' => \phycom\common\helpers\JsExpression::create(<<<JS
//				function (previewId, index) {
//                    var obj = {};
//                    $("#"+previewId).find(".attachment-extra-param").each(function() {
//						var key = $(this).attr("data-name"),
//                            val = $(this).val();
//                        obj[key] = val;
//                    });
//                    return obj;
//				}
//JS
//			)
		]
	]);

ActiveForm::end();


$this->registerJs(<<<JS

[].forEach.call(document.querySelectorAll('img[data-src]'),    function(img) {
    img.setAttribute('src', img.getAttribute('data-src'));
    img.onload = function() {
        img.removeAttribute('data-src');
    };
});
JS
, \yii\web\View::POS_READY);

