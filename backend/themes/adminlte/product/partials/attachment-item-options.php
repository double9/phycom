<?php

/**
 * @var $this yii\web\View
 * @var $attachment \phycom\common\models\product\ProductAttachment
 */

use phycom\backend\models\product\ProductAttachmentOptionsForm;
use phycom\backend\widgets\ActiveForm;
use phycom\common\models\File;

use yii\helpers\Html;



$model = new ProductAttachmentOptionsForm($attachment);

$form = ActiveForm::begin([
    'id' => 'attachment_' . $attachment->file_id . '_form',
    'action' => ['/product/update-attachment', 'id' => $attachment->product_id, 'fileId' => $attachment->file_id],
    'options' => ['class' => 'product-attachment-options']
]);
?>
<div class="row">
    <div class="col-md-12">
        <div class="messages"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <?= Html::img(null, ['data-src' => $attachment->file->getThumbUrl(File::THUMB_SIZE_LARGE)]); ?>
    </div>
    <div class="col-md-6">

        <table class="table table-striped no-margin">
            <colgroup>
                <col width="200">
                <col>
            </colgroup>

            <tr>
                <td><?= Yii::t('backend/main', 'Imported filename') ?>:</td>
                <td><?= $attachment->file->name; ?></td>
            </tr>

            <?php if ($attachment): ?>
                <tr>
                    <td><?= $model->getAttributeLabel('isVisible'); ?>:</td>
                    <td><?= $form->field($model, 'isVisible')->boolean()->label(false) ?></td>
                </tr>
            <?php endif; ?>

            <?php
            foreach ($model->params as $param) {

                echo Html::beginTag('tr');
                echo Html::tag('td', $param->label . ':');
                echo Html::beginTag('td');

                switch ($param->type) {
                    case $param::TYPE_BOOL:
                        echo $form->field($model, 'meta[' . $param->name . ']')->boolean(false, ['value' => (int) $model->meta[$param->name]])->label(false);
                        break;
                    case $param::TYPE_NUMBER:
                        echo $form->field($model, 'meta[' . $param->name . ']')->textInput(['type' => 'number'])->label(false);
                        break;
                    default:
                        echo $form->field($model, 'meta[' . $param->name . ']')->textInput()->label(false);
                }

                echo Html::endTag('td');
                echo Html::endTag('tr');
            }
            ?>
        </table>
    </div>
</div>
<?php ActiveForm::end(); ?>
