<?php

/**
 * @var $this yii\web\View
 * @var $model phycom\backend\models\product\ProductForm
 */
use phycom\backend\widgets\ActiveForm;
use phycom\common\models\attributes\ProductStatus;
?>

<?php $form = ActiveForm::begin(['id' => 'product-form']); ?>

	<?= $form->field($model, 'sku')->textInput(); ?>
	<?= $form->field($model, 'categories')->multiSelect($model->allCategories); ?>
    <?= $form->field($model, 'tags')->multiSelect($model->allTags); ?>

	<div class="row">
		<div class="col-md-4">
			<?= $form->field($model, 'status')->dropDownList(ProductStatus::displayValues()); ?>
		</div>
        <div class="col-md-8">

        </div>
	</div>

<?php ActiveForm::end(); ?>
