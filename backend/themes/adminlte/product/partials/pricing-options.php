<?php
/**
 * @var $this yii\web\View
 * @var $model ProductPricingOptionsForm
 */

use phycom\backend\widgets\ActiveForm;
use phycom\backend\models\product\ProductPricingOptionsForm;

use phycom\common\models\attributes\UnitType;
use phycom\common\models\attributes\PriceUnitMode;

$form = ActiveForm::begin([
    'id' => 'product-pricing-options',
    'options' => ['class' => 'clearfix']
]);
?>

<div class="col-sm-4 col-md-3 col-lg-2">
    <?= $form->field($model, 'priceUnit')->dropDownList(UnitType::displayValues()); ?>
</div>
<div class="col-sm-4 col-md-3 col-lg-2">
    <?= $form->field($model, 'priceUnitMode')->dropDownList(PriceUnitMode::displayValues()); ?>
</div>
<div class="col-sm-4 col-md-3 col-lg-2">
    <?= $form->field($model, 'discount')->boolean(); ?>
</div>

<?php ActiveForm::end(); ?>



