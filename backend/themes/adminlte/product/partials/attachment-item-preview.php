<?php

/**
 * @var $this yii\web\View
 * @var $model phycom\backend\models\product\ProductAttachmentForm
 * @var $form \phycom\backend\widgets\ActiveForm
 * @var $attachment \phycom\common\models\product\ProductAttachment
 */

?>

<div class="file-preview-frame {frameClass}" id="{previewId}" data-fileid="{fileid}" data-fileindex="{fileindex}" data-template="{template}">
    <div class="kv-file-content">
        <div class="attachment-item">
            <img src="{data}" class="kv-preview-data file-preview-image" title="{caption}" alt="{caption}" {style} />
            <div class="item-options"></div>
        </div>
    </div>
    {footer}
</div>
