<?php

use phycom\backend\widgets\ActiveForm;
use phycom\common\models\attributes\ProductStatus;

use yii\helpers\Html;

$model = new \phycom\backend\models\product\ProductBulkUpdateForm();
$modelName = (new \ReflectionClass($model))->getShortName();

$form = ActiveForm::begin();
?>
    <div class="keys" data-name="<?= $modelName . "[keys][]" ?>"></div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'tags')->multiSelect($model->getAllTags(), ['placeholder' => $model->getAttributeLabel('tags')])->label(false); ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'tagAction')
                ->dropDownList($model->getFieldActions(), ['prompt' => $model->getAttributeLabel('tagAction')])
                ->label(false)
            ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'price')
                ->textInput(['placeholder' => $model->getAttributeLabel('price')])
                ->label(false)
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'categories')->multiSelect($model->getAllCategories(), ['placeholder' => $model->getAttributeLabel('categories')])->label(false); ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'categoryAction')
                ->dropDownList($model->getFieldActions(), ['prompt' => $model->getAttributeLabel('categoryAction')])
                ->label(false)
            ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'status')
                ->dropDownList(ProductStatus::displayValues(), ['prompt' => $model->getAttributeLabel('status')])
                ->label(false)
            ?>
        </div>
        <div class="col-md-3">
            <?= Html::submitButton('<span class="fas fa-exclamation-circle"></span>&nbsp;&nbsp;' . Yii::t('backend/main', 'Bulk update selected rows'),
                [
                    'class' => 'btn btn-md btn-flat btn-primary',
                    'style' => 'margin-bottom: 15px;'
                ]
            ) ?>
        </div>
    </div>


<?php ActiveForm::end(); ?>
