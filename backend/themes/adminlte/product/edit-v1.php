<?php

use phycom\backend\helpers\ProductParamGrid;
use phycom\backend\helpers\ProductVariantGrid;
use phycom\backend\widgets\FormCollection;
use phycom\backend\widgets\Box;
use phycom\backend\widgets\FormSubmitBtn;

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $this yii\web\View
 * @var \phycom\backend\models\product\ProductForm $productForm
 * @var \phycom\backend\models\product\ProductTranslationForm $translationForm
 * @var \phycom\backend\models\product\ProductAttachmentForm $attachmentForm
 * @var \phycom\backend\models\product\SearchProductOption $options
 */
$this->title = $productForm->product->isNewRecord ? Yii::t('backend/product', 'Add product') : $productForm->product->translation->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/product', 'Product catalog'), 'url' => ['product/catalog']];
$this->params['breadcrumbs'][] = $productForm->product->isNewRecord ? $this->title : $productForm->product->id;
$this->params['titleLabel'] = $productForm->product->isNewRecord ? null : '<span class="label label-primary">' . Html::encode($productForm->product->sku) . '</span>';

$formCollectionForms = [
    '#product-form'              => Yii::t('backend/product', 'Content'),
    '#product-translation-form'  => Yii::t('backend/product', 'Content'),
    '#product-pricing-options'   => Yii::t('backend/product', 'Pricing'),
    '#product-price-grid-form'   => Yii::t('backend/product', 'Pricing'),
    '#product-attachment-form'   => Yii::t('backend/product', 'Attachments')
];
if (Yii::$app->commerce->params->isEnabled()) {
    $formCollectionForms['#product-param-grid-form'] = Yii::t('backend/product', 'Params');
}
if (Yii::$app->commerce->variants->isEnabled()) {
    $formCollectionForms['#product-variant-grid-form'] = Yii::t('backend/product', 'Variants');
}

$formCollection = FormCollection::begin([
    'id'              => 'product-form-container',
    'submitUrl'       => $productForm->product->isNewRecord ? Url::toRoute(['/product/add']) : Url::toRoute(['/product/edit', 'id' => $productForm->product->id]),
    'forms'           => $formCollectionForms,
    'attachmentForms' => ['#productattachmentform-file' => Yii::t('backend/product', 'Attachments')],
    'tabs'            => false
]);

?>
    <div class="row">
        <div class="col-md-12">
            <div class="system-messages"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">
            <?= Box::begin([
                    'showHeader' => false,
                    'options' => ['class' => 'box box-default'],
                    'bodyOptions' => ['class' => 'box-body']
                ]);
            ?>
            <?= $this->render('partials/product-form', ['model' => $productForm]); ?>
            <?= (new ProductVariantGrid($productForm->variantForm))->render() ?>
            <br />
            <?= $this->render('partials/attachment-form', ['model' => $productForm->attachmentForm]); ?>
            <?= Box::end(); ?>
        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col-md-12">
                    <?= $this->render('partials/translation-form', ['model' => $productForm->translationForm]); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= Box::begin([
                        'title' => '<span class="fab fa-buffer"></span>&nbsp;&nbsp;' . Yii::t('backend/product', 'Params'),
                        'options' => ['class' => 'box box-default'],
                    ]);
                    ?>
                    <?= (new ProductParamGrid($productForm->paramForm))->render() ?>
                    <?= Box::end(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= Box::begin([
                'title' => '<span class="fas fa-euro-sign"></span>&nbsp;&nbsp;' . Yii::t('backend/product', 'Pricing'),
                'options' => ['class' => 'box box-default'],
            ]);
            ?>
            <div class="col-md-12" style="margin-top: 20px;">
                <?= $this->render('partials/pricing-options', ['model' => $productForm->pricingOptionsForm]); ?>
            </div>
            <?= $this->render('partials/pricing-grid', ['model' => $productForm->pricingForm]); ?>
            <?= Box::end(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= Box::begin([
                    'showHeader' => false,
                    'options' => ['class' => 'box box-default'],
                    'bodyOptions' => ['class' => 'box-body']
                ]);
            ?>
            <div class="clearfix">

	            <?= $this->render('/partials/back-btn', ['size' => 'lg']); ?>

                <?= FormSubmitBtn::widget(['formCollection' => $formCollection]) ?>

	            <?= Html::a('<span class="fas fa-plus"></span>&nbsp;&nbsp;' . Yii::t('backend/product', 'New'), ['product/add'], [
	                    'class' => 'btn btn-flat btn-success btn-lg pull-right',
                        'style' => 'margin-right: 10px;'
                ]); ?>

            </div>

            <?= Box::end(); ?>
        </div>
    </div>

<?php $formCollection::end(); ?>
