<?php

use phycom\backend\widgets\Box;
use phycom\backend\widgets\Tabs2;
use phycom\backend\widgets\FormCollection;
use phycom\backend\widgets\FormSubmitBtn;
use phycom\common\helpers\Url;

use yii\helpers\Html;


$items = $items ?? [];
$active = $active ?? null;

/**
 * @var $this yii\web\View
 * @var \phycom\backend\models\product\ProductForm $productForm
 * @var \phycom\backend\models\product\ProductTranslationForm $translationForm
 * @var \phycom\backend\models\product\ProductAttachmentForm $attachmentForm
 * @var \phycom\backend\models\product\SearchProductOption $options
 */
$this->title = $productForm->product->isNewRecord ? Yii::t('backend/product', 'Add product') : $productForm->product->translation->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/product', 'Product catalog'), 'url' => ['product/catalog']];
$this->params['breadcrumbs'][] = $productForm->product->isNewRecord ? $this->title : $productForm->product->id;
$this->params['titleLabel'] = $productForm->product->isNewRecord ? null : '<span class="label label-primary">' . Html::encode($productForm->product->sku) . '</span>';

$formCollectionForms = [
    '#product-form'              => Yii::t('backend/product', 'Content'),
    '#product-translation-form'  => Yii::t('backend/product', 'Content'),
    '#product-pricing-options'   => Yii::t('backend/product', 'Pricing'),
    '#product-price-grid-form'   => Yii::t('backend/product', 'Pricing'),
    '#product-attachment-form'   => Yii::t('backend/product', 'Attachments')
];
if (Yii::$app->commerce->params->isEnabled()) {
    $formCollectionForms['#product-param-grid-form'] = Yii::t('backend/product', 'Params');
}
if (Yii::$app->commerce->variants->isEnabled()) {
    $formCollectionForms['#product-variant-grid-form'] = Yii::t('backend/product', 'Variants');
}

$formCollection = FormCollection::begin([
    'id'              => 'product-form-container',
    'submitUrl'       => $productForm->product->isNewRecord ? Url::toRoute(['/product/add']) : Url::toRoute(['/product/edit', 'id' => $productForm->product->id]),
    'forms'           => $formCollectionForms,
    'attachmentForms' => ['#productattachmentform-file' => Yii::t('backend/product', 'Attachments')],
    'tabs'            => true
]);

?>
    <div class="row">
        <div class="col-md-12">
            <div class="system-messages"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= Box::begin([
                'showHeader'  => false,
                'options'     => ['class' => 'box box-default'],
                'bodyOptions' => ['class' => 'box-body no-padding']
            ]);
            ?>
            <?= Tabs2::widget([
                'id'               => 'product-tabs',
                'contentBeforeNav' => '<div class="img-center h-200">
                    <img src="' . $productForm->attachmentForm->getImageUrl()->medium . '" />
                </div>',
                'items'            => $items,
                'activeTab'        => $active,
                'contentOptions'   => ['class' => 'tab-content col-md-10']
            ]);
            ?>
            <?= Box::end(); ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <?= Box::begin([
                'showHeader'  => false,
                'options'     => ['class' => 'box box-default'],
                'bodyOptions' => ['class' => 'box-body']
            ]);
            ?>
            <div class="clearfix">
                <div class="pull-left"><?= $this->render('/partials/back-btn', ['size' => 'lg']); ?></div>
                <?= FormSubmitBtn::widget(['formCollection' => $formCollection]) ?>
            </div>
            <?= Box::end(); ?>
        </div>
    </div>

<?php $formCollection::end(); ?>
