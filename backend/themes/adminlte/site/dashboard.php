<?php

use phycom\backend\widgets\Box;

use yii\helpers\Json;

/* @var $this yii\web\View */

$this->title = Yii::t('backend/main', 'Dashboard');

\phycom\backend\assets\StatisticsAsset::register($this);
$date = new \DateTime();
$from = (clone $date)->sub(new \DateInterval('P1M'))->format('Y-m-d');
$to = (clone $date)->format('Y-m-d');
?>
<div class="dashboard">

    <?php if (Yii::$app->user->can('search_statistics')): ?>
        <div class="row">
            <div class="col-md-6">
                <?= Box::begin([
                        'title' => Yii::t('backend/statistics', 'Daily Orders'),
                        'bodyOptions' => ['class' => 'box-body']
                    ]);
                ?>

                <canvas id="orders-chart" width="400" height="100"></canvas>
                <?php
                    $chartOptions = Json::encode([
                        'id'             => 'orders-chart',
                        'countLabel'     => Yii::t('backend/statistics', 'Num. orders'),
                        'abandonedLabel' => Yii::t('backend/statistics', 'Abandoned orders'),
                        'locale'         => Yii::$app->lang->current->code,
                        'from'           => $from,
                        'to'             => $to,
                        'url'            => Yii::$app->urlManagerBackend->createAbsoluteUrl('/order/statistics')
                    ]);
                    $this->registerJs(";DailyOrdersChart.create($chartOptions);");
                ?>

                <?= Box::end(); ?>
            </div>
            <div class="col-md-6">
                <?= Box::begin([
                    'title' => Yii::t('backend/statistics', 'Daily Sales'),
                    'bodyOptions' => ['class' => 'box-body']
                ]);
                ?>

                <canvas id="payments-chart" width="400" height="100"></canvas>
                <?php
                    $chartOptions = Json::encode([
                        'id'             => 'payments-chart',
                        'label'          => Yii::t('backend/statistics', 'Money received'),
                        'locale'         => Yii::$app->lang->current->code,
                        'from'           => $from,
                        'to'             => $to,
                        'url'            => Yii::$app->urlManagerBackend->createAbsoluteUrl('/payment-list/statistics')
                    ]);
                    $this->registerJs(";DailySalesChart.create($chartOptions);");
                ?>

                <?= Box::end(); ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-6">
            <?= Box::begin([
                'title' => Yii::t('backend/order', 'Recent Orders'),
                'bodyOptions' => ['class' => 'box-body']
            ]);
            ?>
            <?= $this->render('/order/partials/recent-orders'); ?>
            <?= Box::end(); ?>
        </div>

        <?php if (Yii::$app->user->can('search_statistics')): ?>
            <div class="col-md-6">
                <?= Box::begin([
                    'title' => Yii::t('backend/statistics', 'Sales overview'),
                    'bodyOptions' => ['class' => 'box-body']
                ]);
                ?>

                <div id="sales-overview" class="row">

                    <div class="col-md-6">

                        <div class="info-box bg-blue">
                            <span class="info-box-icon"><i class="fas fa-calendar"></i></span>
                            <div class="info-box-content">
                                <table>
                                    <tr>
                                        <td data-attribute="lastDayRevenue">
                                            <div class="info-box-text"><?= Yii::t('backend/statistics', 'Last Day Revenue') ?></div>
                                            <div class="info-box-number value"></div>
                                        </td>
                                        <td data-attribute="prevDayRevenue">
                                            <div class="info-box-text"><?= Yii::t('backend/statistics', 'Prev. Day Revenue') ?></div>
                                            <div class="info-box-number value"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td data-attribute="avgDailyRevenue">
                                            <div class="info-box-text"><?= Yii::t('backend/statistics', 'Avg Daily Revenue') ?></div>
                                            <div class="info-box-number value"></div>
                                        </td>
                                        <td data-attribute="dailyGrowth">
                                            <div class="info-box-text"><?= Yii::t('backend/statistics', 'Last Day Growth') ?></div>
                                            <div class="info-box-number value"></div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">

                        <div class="info-box bg-aqua">
                            <span class="info-box-icon"><i class="fas fa-calendar"></i></span>
                            <div class="info-box-content">
                                <table>
                                    <tr>
                                        <td data-attribute="lastWeekRevenue">
                                            <div class="info-box-text"><?= Yii::t('backend/statistics', 'Last Week Revenue') ?></div>
                                            <div class="info-box-number value"></div>
                                        </td>
                                        <td data-attribute="prevWeekRevenue">
                                            <div class="info-box-text"><?= Yii::t('backend/statistics', 'Prev. Week Revenue') ?></div>
                                            <div class="info-box-number value"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td data-attribute="avgWeeklyRevenue">
                                            <div class="info-box-text"><?= Yii::t('backend/statistics', 'Avg Weekly Revenue') ?></div>
                                            <div class="info-box-number value"></div>
                                        </td>
                                        <td data-attribute="weeklyGrowth">
                                            <div class="info-box-text"><?= Yii::t('backend/statistics', 'Last Week Growth') ?></div>
                                            <div class="info-box-number value"></div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>

                <?php
                    $reportOptions = Json::encode([
                        'from'    => $from,
                        'to'      => $to,
                        'url'     => Yii::$app->urlManagerBackend->createAbsoluteUrl('/payment-list/statistics'),
                        'context' => '#sales-overview'
                    ]);
                    $this->registerJs(";SalesIndicators.create($reportOptions);");
                ?>
                <?= Box::end(); ?>
            </div>
        <?php endif; ?>

    </div>

</div>
