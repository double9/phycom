<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model phycom\common\models\ForgotPasswordForm */

$this->title = Yii::t('backend/user', 'Forgot password');
?>

<div class="login-box">

    <div class="login-logo">
        <a href="<?= Url::toRoute(['site/index']) ?>"><b><?= Yii::$app->name; ?></b></a>
    </div>

    <div class="login-box-body">
        <p class="login-box-msg"><?= Yii::t('backend/main', 'You can recover your password by sending the password recovery link to your email address') ?></p>

        <?php $form = ActiveForm::begin(['id' => 'forgot-password-form', 'enableClientValidation' => false]); ?>

        <?= $form
            ->field($model, 'email', [
	            'options' => ['class' => 'form-group has-feedback'],
	            'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
            ])
            ->label(false)
            ->textInput(['placeholder' => Yii::t('backend/user','Email')])
        ?>

        <div class="row">
            <div class="col-xs-12">
                <?= Html::submitButton(Yii::t('backend/main','Send password recovery email'), ['class' => 'btn btn-primary btn-block btn-flat']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
        <br>
        <a href="<?= Url::toRoute(['site/login']) ?>"><?= Yii::t('backend/user', 'Back to login page') ?></a>
    </div>
</div>
