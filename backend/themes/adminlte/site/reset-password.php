<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model phycom\backend\models\ChangePasswordForm */

$this->title = Yii::t('backend/user', 'Reset password');
?>

<div class="login-box">

	<div class="login-logo">
		<a href="<?= Url::toRoute(['site/index']) ?>"><b><?= Yii::$app->name; ?></b></a>
	</div>

	<div class="login-box-body">
		<p class="login-box-msg"><?= Yii::t('backend/main', 'Choose your new password') ?></p>

		<?php $form = ActiveForm::begin(['id' => 'registration-form', 'enableClientValidation' => false]); ?>

		<?= $form
			->field($model, 'password', [
				'options' => ['class' => 'form-group has-feedback'],
				'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
			])
			->label(false)
			->passwordInput(['placeholder' => $model->getAttributeLabel('password')])
		?>

		<?= $form
			->field($model, 'repeatPassword', [
				'options' => ['class' => 'form-group has-feedback'],
				'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
			])
			->label(false)
			->passwordInput(['placeholder' => $model->getAttributeLabel('repeatPassword')])
		?>

		<div class="row">
			<div class="col-xs-12">
				<?= Html::submitButton(Yii::t('backend/main','Submit'), ['class' => 'btn btn-primary btn-block btn-flat']) ?>
			</div>
		</div>

		<?php ActiveForm::end(); ?>
		<br>
		<a href="<?= Url::toRoute(['site/login']) ?>"><?= Yii::t('backend/user', 'Back to login page') ?></a>
	</div>
</div>
