<?php

use phycom\backend\widgets\ActiveForm;
use phycom\backend\widgets\Box;

use dosamigos\datepicker\DateRangePicker;

use yii\helpers\Html;
use yii\helpers\Json;

/**
 * @var $this yii\web\View
 * @var \phycom\backend\models\StatisticsForm $model
 */

$this->title = Yii::t('backend/main', 'Statistics');

\phycom\backend\assets\StatisticsAsset::register($this);
?>
<div class="statistics">

    <div class="row">
        <div class="col-md-12">
            <?= Box::begin([
                'title' => Yii::t('backend/statistics', 'Sales overview'),
                'bodyOptions' => ['class' => 'box-body']
            ]);
            ?>

            <?php $form = ActiveForm::begin(['options' => ['class' => 'row', 'autocomplete' => 'off']]); ?>

                <div class="col-md-4">
                    <?= $form->field($model, 'from')->widget(DateRangePicker::class, [
                            'attributeTo'   => 'to',
                            'form'          => $form, // best for correct client validation
                            'language'      => Yii::$app->lang->current->code,
                            'labelTo'       => Yii::t('backend/main', 'to'),
                            'clientOptions' => [
                                'todayHighlight' => true,
                                'autoclose'      => true,
                                'format'         => Yii::$app->formatter->jsDateFormat
                            ]
                        ])->label(false);
                    ?>
                </div>
                <div class="col-md-2">
                    <?= Html::submitButton('<i class="fas fa-sync"></i>&nbsp;&nbsp;' . Yii::t('backend/statistics', 'Reload'),
                        [
                            'class' => 'btn btn-md btn-flat btn-primary',
                        ]
                    ) ?>
                </div>


            <?php $form::end(); ?>

            <hr>

            <div class="row">
                <div id="sales-overview" class="col-md-12">

                    <div class="row">
                        <div class="col-md-3">

                            <div class="info-box bg-green">
                                <span class="info-box-icon"><i class="fas fa-calculator"></i></span>
                                <div class="info-box-content">
                                    <div data-attribute="totalRevenue">
                                        <div class="info-box-text"><?= Yii::t('backend/statistics', 'Total Revenue') ?></div>
                                        <div class="info-box-number value"></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-3">

                            <div class="info-box bg-blue">
                                <span class="info-box-icon"><i class="fas fa-calendar"></i></span>
                                <div class="info-box-content">
                                    <table>
                                        <tr>
                                            <td data-attribute="lastDayRevenue">
                                                <div class="info-box-text"><?= Yii::t('backend/statistics', 'Last Day Revenue') ?></div>
                                                <div class="info-box-number value"></div>
                                            </td>
                                            <td data-attribute="prevDayRevenue">
                                                <div class="info-box-text"><?= Yii::t('backend/statistics', 'Prev. Day Revenue') ?></div>
                                                <div class="info-box-number value"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td data-attribute="avgDailyRevenue">
                                                <div class="info-box-text"><?= Yii::t('backend/statistics', 'Avg Daily Revenue') ?></div>
                                                <div class="info-box-number value"></div>
                                            </td>
                                            <td data-attribute="dailyGrowth">
                                                <div class="info-box-text"><?= Yii::t('backend/statistics', 'Last Day Growth') ?></div>
                                                <div class="info-box-number value"></div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-3">

                            <div class="info-box bg-aqua">
                                <span class="info-box-icon"><i class="fas fa-calendar"></i></span>
                                <div class="info-box-content">
                                    <table>
                                        <tr>
                                            <td data-attribute="lastWeekRevenue">
                                                <div class="info-box-text"><?= Yii::t('backend/statistics', 'Last Week Revenue') ?></div>
                                                <div class="info-box-number value"></div>
                                            </td>
                                            <td data-attribute="prevWeekRevenue">
                                                <div class="info-box-text"><?= Yii::t('backend/statistics', 'Prev. Week Revenue') ?></div>
                                                <div class="info-box-number value"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td data-attribute="avgWeeklyRevenue">
                                                <div class="info-box-text"><?= Yii::t('backend/statistics', 'Avg Weekly Revenue') ?></div>
                                                <div class="info-box-number value"></div>
                                            </td>
                                            <td data-attribute="weeklyGrowth">
                                                <div class="info-box-text"><?= Yii::t('backend/statistics', 'Last Week Growth') ?></div>
                                                <div class="info-box-number value"></div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-3">

                            <div class="info-box bg-teal">
                                <span class="info-box-icon"><i class="fas fa-calendar"></i></span>
                                <div class="info-box-content">
                                    <table>
                                        <tr>
                                            <td data-attribute="lastMonthRevenue">
                                                <div class="info-box-text"><?= Yii::t('backend/statistics', 'Last Month Revenue') ?></div>
                                                <div class="info-box-number value"></div>
                                            </td>
                                            <td data-attribute="prevMonthRevenue">
                                                <div class="info-box-text"><?= Yii::t('backend/statistics', 'Prev. Month Revenue') ?></div>
                                                <div class="info-box-number value"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td data-attribute="avgMonthlyRevenue">
                                                <div class="info-box-text"><?= Yii::t('backend/statistics', 'Avg Monthly Revenue') ?></div>
                                                <div class="info-box-number value"></div>
                                            </td>
                                            <td data-attribute="monthlyGrowth">
                                                <div class="info-box-text"><?= Yii::t('backend/statistics', 'Last Month Growth') ?></div>
                                                <div class="info-box-number value"></div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>

            <?php
                $reportOptions = Json::encode([
                    'from'    => $model->from,
                    'to'      => $model->to,
                    'url'     => Yii::$app->urlManagerBackend->createAbsoluteUrl('/payment-list/statistics'),
                    'context' => '#sales-overview'
                ]);
                $this->registerJs(";SalesIndicators.create($reportOptions);");
            ?>

            <?= Box::end(); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
	        <?= Box::begin([
                    'title' => Yii::t('backend/statistics', 'Daily Orders'),
                    'bodyOptions' => ['class' => 'box-body']
                ]);
	        ?>

            <canvas id="orders-chart" width="400" height="100"></canvas>
            <?php
                $chartOptions = Json::encode([
                    'id'             => 'orders-chart',
                    'countLabel'     => Yii::t('backend/statistics', 'Num. orders'),
                    'abandonedLabel' => Yii::t('backend/statistics', 'Abandoned orders'),
                    'locale'         => Yii::$app->lang->current->code,
                    'from'           => $model->from,
                    'to'             => $model->to,
                    'url'            => Yii::$app->urlManagerBackend->createAbsoluteUrl('/order/statistics')
                ]);
                $this->registerJs(";DailyOrdersChart.create($chartOptions);");
            ?>

            <?= Box::end(); ?>
        </div>

        <div class="col-md-6">
            <?= Box::begin([
                'title' => Yii::t('backend/statistics', 'Daily Sales'),
                'bodyOptions' => ['class' => 'box-body']
            ]);
            ?>

            <canvas id="payments-chart" width="400" height="100"></canvas>
            <?php
                $chartOptions = Json::encode([
                    'id'             => 'payments-chart',
                    'label'          => Yii::t('backend/statistics', 'Money received'),
                    'locale'         => Yii::$app->lang->current->code,
                    'from'           => $model->from,
                    'to'             => $model->to,
                    'url'            => Yii::$app->urlManagerBackend->createAbsoluteUrl('/payment-list/statistics')
                ]);
                $this->registerJs(";DailySalesChart.create($chartOptions);");
            ?>

            <?= Box::end(); ?>
        </div>
    </div>
</div>
