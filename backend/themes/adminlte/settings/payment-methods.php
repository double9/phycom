<?php

/**
 * @var $this yii\web\View
 * @var $models \phycom\common\modules\payment\Settings[]
 */

use phycom\backend\widgets\ModuleSettings;

use yii\helpers\Html;

?>
<div id="payment-method-settings">
    <div class="row">
        <ul class="col-md-12">
            <?php foreach ($models as $form): ?>
                <li id="<?= $form->module->id; ?>" class="payment-method">
                    <table class="table table-striped">
                        <thead>
                            <tr style="background-color: #f6f6f6; border-bottom: 2px solid #ddd;">
                                <td>
                                    <h3><?= $form->module->getLabel(); ?></h3>
                                </td>
                                <td class="clearfix">
                                    <?= Html::img($form->module->getLogo(), ['style' => 'height: 57px; float: right']) ?>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="2">
                                    <?= ModuleSettings::widget(['model' => $form]); ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
