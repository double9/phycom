<?php

/**
 * @var $this yii\web\View
 * @var $models \phycom\common\modules\delivery\models\DeliveryMethodSettings[]
 */
use phycom\common\modules\delivery\models\Settings;
use phycom\backend\widgets\ModuleSettings;


$settings = null;
if (isset($models[0]) && $models[0] instanceof Settings) {
    $settings = array_shift($models);
}
?>
<div id="delivery-method-settings">




    <div class="row">
        <ul class="col-md-12">

            <?php if ($settings): ?>
                <li id="<?= $settings->module->id; ?>" class="delivery" style="max-width: 915px; display: block;">
                    <table class="table table-striped">
                        <thead>
                        <tr style="background-color: #f6f6f6; border-bottom: 2px solid #ddd;">
                            <td colspan="2">
                                <h3><?= Yii::t('backend/settings', 'Common delivery options') ?></h3>
                            </td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="2">
                                <?= ModuleSettings::widget(['model' => $settings]); ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </li>
            <?php endif; ?>


            <?php foreach ($models as $form): ?>
                <li id="<?= $form->module->id; ?>" class="delivery-method">
                    <table class="table table-striped">
                        <thead>
                            <tr style="background-color: #f6f6f6; border-bottom: 2px solid #ddd;">
                                <td colspan="2">
                                    <h3><?= $form->module->getLabel(); ?></h3>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="2">
                                    <?= ModuleSettings::widget(['model' => $form]); ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
