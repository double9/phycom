<?php

/**
 * @var $this yii\web\View
 * @var array $items
 * @var string $active
 */

use phycom\backend\widgets\Box;
use phycom\backend\widgets\Tabs2;

$this->title = Yii::t('backend/settings', 'Settings');
?>

<div class="row">

    <div class="col-md-12">

        <?= Box::begin([
            'showHeader' => false,
            'options' => ['class' => 'box box-default'],
            'bodyOptions' => ['class' => 'box-body no-padding']
        ]);
        ?>
        <?= Tabs2::widget([
                'id' => 'settings-tabs',
                'items' => $items,
                'activeTab' => $active,
                'contentOptions' => ['class' => 'tab-content col-md-10']
            ]);
        ?>
        <?= Box::end(); ?>

    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <?= Box::begin([
            'showHeader' => false,
            'options' => ['class' => 'box box-default'],
            'bodyOptions' => ['class' => 'box-body']
        ]);
        ?>
        <div class="clearfix">
            <div class="pull-left"><?= $this->render('/partials/back-btn', ['size' => 'lg']); ?></div>
            <?= $this->render('/partials/save-btn-2', [
                'class' => 'pull-right btn-lg',
                'formSelector' => '#settings-tabs .tab-content > .tab-pane.active'
            ]);
            ?>
        </div>
        <?= Box::end(); ?>
    </div>
</div>
