<?php

/**
 * @var \phycom\backend\models\SearchAll $model
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

use phycom\backend\widgets\Box;
use phycom\backend\widgets\LinkPager;
use phycom\common\models\attributes\SearchResultType;

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\StringHelper;

$this->title = Yii::t('backend/main', 'Search results');
$this->params['breadcrumbs'][] = Yii::t('backend/main', 'Search');

?>


<div class="row">
	<div class="col-md-12">

        <?= Box::begin([
            'showHeader' => false,
            'options' => ['class' => 'box box-default'],
            'bodyOptions' => ['class' => 'box-body']
        ]);
        ?>

        <div class="row">
            <div class="col-md-12">
                <form id="search-results-form" class="form-inline" action="<?= Url::toRoute('/search'); ?>" method="get">
                    <?= Html::input('text','q', $model->q, ['class' => 'form-control']); ?>
                    <?= Html::dropDownList(
                            'type',
                            $model->type,
                            SearchResultType::displayValues(),
                            [
                                'class' => 'form-control',
                                'prompt' => '-'
                            ]
                    ); ?>
                    <?= Html::submitButton(Yii::t('backend/main', 'Search'), ['class' => 'btn btn-primary btn-flat']); ?>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <p class="text-muted text-sm" style="margin-top: 5px;">
                    <?= Yii::t('backend/main', '{n} results found', ['n' => $dataProvider->totalCount]); ?>
                </p>
            </div>
        </div>

        <div class="row">
            <hr class="no-margin">
        </div>

        <div class="row">
            <div class="col-md-12">
                <ul id="search-results">
                <?php foreach($dataProvider->getModels() as $record): ?>
                    <?php
                    /**
                     * @var \phycom\backend\models\SearchAll $record
                     */
                    ?>
                    <li>
                        <a href="<?= $record->url ?>">
                            <h5><?= $record->type->icon . '&nbsp;&nbsp;' . $record->title ?></h5>
                            <p><?= StringHelper::truncateWords($record->description, 25) ?></p>
                        </a>
                    </li>
                <?php endforeach; ?>
                </ul>
                <?= LinkPager::small($dataProvider->pagination); ?>
            </div>
        </div>

        <?= Box::end(); ?>
	</div>
</div>