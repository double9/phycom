<?php

use phycom\backend\widgets\Box;
use yii\helpers\Html;
use yii\helpers\Url;
use phycom\backend\widgets\DataGrid;

/* @var $this yii\web\View */

$this->title = Yii::t('backend/main', 'Message templates');
$this->params['breadcrumbs'][] = $this->title;

$dataProvider = new \yii\data\ArrayDataProvider([
	'allModels' =>  Yii::$app->modelFactory->getMessageTemplate()::findAll(),
	'sort' => [
		'attributes' => ['id', 'type', 'description'],
	],
	'pagination' => false,
]);

?>
<div class="row">
	<div class="col-md-12">
		<?= Box::begin([
			'options' => ['class' => 'box box-default'],
			'showHeader' => false,
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

		<?= DataGrid::widget([
			'id' => 'user-grid',
			'dataProvider' => $dataProvider,
			'filterModel' => null,
			'rowOptions' => function ($model) {
				return [
					'class' => 'row-link',
					'data-url' => Url::toRoute(['template/view','id' => $model->id])
				];
			},
			'actions' => [

			],
			'columns' => [
				[
					'attribute' => 'id',
					'format' => 'text',
					'options' => ['width' => '200']
				],
				[
					'attribute' => 'type',
					'format' => 'text',
				],
				[
					'attribute' => 'description',
					'format' => 'text',
				]
			]
		]);
		?>

		<?= Box::end(); ?>
	</div>
</div>
