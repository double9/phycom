<?php

/**
 * @var $this yii\web\View
 * @var \phycom\common\models\MessageTemplate $model
 */

use phycom\backend\widgets\Box;
use yii\widgets\DetailView;
use yii\helpers\Url;

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/main', 'Templates'), 'url' => ['/template/index']];

?>

<div class="row">

	<div class="col-md-12">


		<?= Box::begin([
			'showHeader' => false,
			'options' => ['class' => 'box box-default'],
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

		<div class="row">
			<div class="col-lg-12">
				<div class="title"><h3><?= $model->renderTitle($model->generateDummyParameters()); ?></h3></div>
                <iframe src="<?= Url::toRoute(['/template/render', 'id' => $model->id]); ?>" width="100%" height="600" frameBorder="0"></iframe>
			</div>
		</div>

		<br />
		<br />
		<?= $this->render('/partials/back-btn', ['size' => 'lg']); ?>

		<?= Box::end(); ?>

	</div>

</div>
