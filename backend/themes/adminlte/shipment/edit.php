<?php

/**
 * @var $this yii\web\View
 * @var \phycom\backend\models\ShipmentForm $model
 */

use phycom\backend\widgets\Box;

use phycom\common\widgets\Map;
use phycom\common\models\Address;
use phycom\common\models\attributes\ShipmentStatus;

use rmrevin\yii\fontawesome\FA;
use rmrevin\yii\fontawesome\FAS;
use rmrevin\yii\fontawesome\FAR;

use yii\widgets\DetailView;
use yii\helpers\Html;

$shipment = $model->shipment;

$this->title = Yii::t('backend/main', 'Shipment - {id}', ['id' => $model->shipment->id]);
$this->params['breadcrumbs'][] = Yii::t('backend/main', 'Sale');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/main', 'Shipments'), 'url' => ['/shipment']];
$this->params['breadcrumbs'][] = $model->shipment->id
?>

<div class="row">

    <div class="col-lg-8 col-md-12">

		<?= Box::begin([
			'showHeader' => false,
			'options' => ['class' => 'box box-default'],
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

        <div class="row">
            <div class="col-md-12">
                <?= DetailView::widget([
                    'id' => 'shipment-detail',
                    'model' => $shipment,
                    'attributes' => [
                        [
                            'attribute' => 'type',
                            'format' => 'raw',
                            'value' => Html::tag('span', $shipment->type->label, ['class' => 'label ' . $shipment->type->labelClass])
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => Html::tag('span', $shipment->status->label, ['class' => 'label ' . $shipment->status->labelClass])
                        ],
                        [
                            'attribute' => 'dispatch_at',
                            'format' => 'datetime',
                            'value' => $shipment->dispatch_at,
                            'visible' => (bool) ($shipment->dispatch_at && !$shipment->shipped_at),
                        ],
                        [
                            'attribute' => 'tracking_number',
                            'format' => 'html',
                            'value' => Html::tag('strong', $shipment->tracking_number),
                            'visible' => (bool) $shipment->tracking_number
                        ],
	                    [
                            'attribute' => 'tracking_status',
                            'format' => 'html',
                            'value' => Html::tag('span', $shipment->tracking_status, ['class' => 'label label-default']),
                            'visible' => (bool) $shipment->tracking_status,
                        ],
                        [
                            'attribute' => 'tracking_url',
                            'format' => 'raw',
                            'value' => Html::a($shipment->tracking_url, $shipment->tracking_url, ['target' => '_blank']),
                            'visible' => (bool) $shipment->tracking_url,
                        ],
                        [
                            'attribute' => 'eta',
                            'format' => 'datetime',
                            'value' => $shipment->eta,
                            'visible' => (bool) $shipment->eta,
                        ],
                        [
                            'attribute' => 'method',
                            'format' => 'text',
                            'value' => $shipment->deliveryMethod ? mb_strtoupper($shipment->deliveryMethod->getName()) : $shipment->method,
                            'visible' => (bool) $shipment->deliveryMethod
                        ],
                        [
                            'attribute' => 'shipment_id',
                            'format' => 'text',
                            'visible' => (bool) $shipment->shipment_id,
                        ],
	                    [
		                    'attribute' => 'carrier_name',
		                    'format' => 'raw',
		                    'value' => $shipment->carrierNameLabel,
                            'visible' => (bool) $shipment->carrier_name,
	                    ],
                        [
                            'attribute' => 'carrier_service',
                            'format' => 'raw',
                            'value' => $shipment->deliveryMethod ? $shipment->deliveryMethod->getServiceLabel($shipment->carrier_service) : $shipment->carrier_service,
                            'visible' => (bool) $shipment->carrier_service
                        ],
                        [
                            'attribute' => 'carrier_delivery_days',
                            'format' => 'integer',
                            'value' => $shipment->carrier_delivery_days,
                            'visible' => (bool) $shipment->carrier_delivery_days,
                        ],
                        [
                            'attribute' => 'delivery_date',
                            'format' => 'date',
                            'value' => $shipment->delivery_date,
                            'visible' => (bool) $shipment->delivery_date,
                        ],
                        [
                            'attribute' => 'delivery_time',
                            'format' => 'text',
                            'value' => $shipment->delivery_time,
                            'visible' => (bool) $shipment->delivery_time,
                        ],
                        [
                            'attribute' => 'duration_terms',
                            'format' => 'text',
                            'value' => $shipment->duration_terms,
                            'visible' => (bool) $shipment->duration_terms
                        ],
                        [
                            'attribute' => 'carrier_area',
                            'format' => 'raw',
                            'value' =>  Html::tag('span', $shipment->carrier_area, ['class' => 'label label-default'])
                        ],
                        [
                            'attribute' => 'from_address',
                            'format' => 'text',
                            'value' => $shipment->from_address,
                            'visible' => (bool) $shipment->from_address
                        ],
                        [
                            'attribute' => 'to_address',
                            'format' => 'addressLink',
                            'value' => $shipment->to_address
                        ],
                        [
                            'attribute' => 'return_address',
                            'format' => 'text',
                            'value' => $shipment->return_address,
                            'visible' => (bool) $shipment->return_address
                        ],
	                    [
		                    'attribute' => 'recipientPhone',
		                    'format' => ['phone', true],
		                    'value' => $shipment->recipientPhone,
                            'visible' => (bool) $shipment->recipientPhone
	                    ],
                        [
                            'attribute' => 'shipped_at',
                            'format' => 'datetime',
                            'visible' => (bool) $shipment->shipped_at
                        ],
	                    'created_at:datetime',
	                    'updated_at:datetime'
                    ],
                ]);

                ?>
            </div>
        </div>

        <div class="clearfix">
            <div class="pull-left"><?= $this->render('/partials/back-btn', ['size' => 'lg']); ?></div>
            <div id="shipment-actions" class="pull-right">
	            <?= Html::a(FAS::i(FAS::_PRINT), '#', ['onclick' => 'window.print();return false;', 'class' => 'btn btn-flat btn-default btn-lg']); ?>
                <?php if ($model->shipment->status->in([ShipmentStatus::ASSEMBLED])): ?>
                    <?= Html::a(
                            FAS::i(FAS::_TRUCK) . '&nbsp;&nbsp;' .
                            Yii::t('backend/shipment', 'Dispatched'),
                            ['/shipment/update-status', 'id' => $model->shipment->id, 'status' => ShipmentStatus::DISPATCHED],
                            ['class' => 'btn btn-lg btn-flat btn-primary']
                    ); ?>
                <?php endif; ?>

                <?php if ($model->shipment->postage_label): ?>
                    <?= Html::a(
                        FAS::i(FAS::_BARCODE) . '&nbsp;&nbsp;' .
                        Yii::t('backend/shipment', 'Postage Label'),
                        ['/shipment/postage-label', 'id' => $model->shipment->id],
                        ['class' => 'btn btn-lg btn-flat btn-default']
                    ); ?>

                <?php elseif ($model->shipment->status->in([ShipmentStatus::PENDING, ShipmentStatus::ASSEMBLED]) && $model->shipment->deliveryMethod->postageLabel): ?>
                    <?= Html::a(
                        FAS::i(FAS::_BARCODE) . '&nbsp;&nbsp;' .
                        Yii::t('backend/shipment', 'Create Postage Label'),
                        ['/shipment/postage-label', 'id' => $model->shipment->id],
                        ['class' => 'btn btn-lg btn-flat btn-primary']
                    ); ?>
                <?php endif; ?>

                <?php if ($model->shipment->status->in([ShipmentStatus::ASSEMBLED, ShipmentStatus::DISPATCHED])): ?>
                    <?= Html::a(
		                    FAS::i(FAS::_CHECK) . '&nbsp;&nbsp;' .
                            Yii::t('backend/shipment', 'Delivered'),
                            ['/shipment/update-status', 'id' => $model->shipment->id, 'status' => ShipmentStatus::DELIVERED],
                            ['class' => 'btn btn-lg btn-flat btn-success']
                    ); ?>
                <?php endif; ?>

            </div>

        </div>

		<?= Box::end(); ?>
    </div>


    <?php if ($shipment->to_address): ?>
        <div class="col-md-12 col-lg-4">
            <?= Box::begin([
                'title' => Yii::t('backend/shipment', 'Destination'),
                'options' => ['class' => 'box box-default'],
                'bodyOptions' => ['class' => 'box-body']
            ]);
            ?>


            <div class="row">
                <div class="col-md-12">
                    <?= Map::widget(['address' => Address::create($shipment->to_address), 'height' => 502]); ?>
                </div>
            </div>

            <?php if ($model->shipment->status->is(ShipmentStatus::DISPATCHED)): ?>



            <?php endif; ?>

            <?= Box::end(); ?>
        </div>
    <?php endif; ?>

</div>


<?php

$this->registerJs(<<<JS
        jQuery(function($){        
            $('#shipment-actions').on('click', 'a.btn', function (e) {
                $(this).button('loading');
            });
        });
JS
, \yii\web\View::POS_READY);