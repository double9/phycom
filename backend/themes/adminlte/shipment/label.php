<?php

use yii\helpers\Html;
use phycom\backend\widgets\Box;
use phycom\common\helpers\f;
use rmrevin\yii\fontawesome\FA;
/**
 * @var $this yii\web\View
 * @var \phycom\common\models\Shipment $shipment
 */
$this->title = Yii::t('backend/shipment', 'Postage label #{id}', ['id' => $shipment->postageLabelData['id'] ?? '']);
$this->params['breadcrumbs'][] = Yii::t('backend/main', 'Sale');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/shipment', 'Shipments'), 'url' => ['/shipment']];
$this->params['breadcrumbs'][] = ['label' => $shipment->id, 'url' => ['/shipment/edit', 'id' => $shipment->id]];
$this->params['breadcrumbs'][] = Yii::t('backend/shipment', 'Postage label');
?>

<div class="row">
    <div class="col-md-12">
        <?= Box::begin([
            'showHeader' => false,
            'options' => ['class' => 'box box-default'],
            'bodyOptions' => ['class' => 'box-body']
        ]);
        ?>

        <div class="clearfix">

            <div class="postage-label-container">
                <?php
                    if ($shipment->postage_label) {

                        echo Html::tag('embed', null, [
                            'src' => $shipment->postageLabelData['url'],
                            'type' => 'application/pdf',
                            'class' => 'embed-postage-label'
                        ]);
                    }
                ?>

            </div>
            <br>
            <div class="row">
                <div class="col-md-12">

                    <table class="table table-striped table-bordered">
                        <colgroup>
                            <col width="200">
                            <col>
                            <col width="200">
                            <col>
                        </colgroup>
                        <tr>
                            <td><strong><?= Yii::t('backend/shipment', 'Metadata'); ?>:</strong></td>
                            <td><?= f::text($shipment->postageLabelData['metadata'] ?? null) ?></td>

                            <td><strong><?= Yii::t('backend/shipment', 'Created At'); ?>:</strong></td>
                            <td><?= f::text($shipment->postageLabelData['createdAt'] ?? null) ?></td>
                        </tr>
                        <tr>
                            <td><strong><?= Yii::t('backend/shipment', 'Commercial Invoice'); ?>:</strong></td>
                            <td><?= f::url($shipment->postageLabelData['commercialInvoiceUrl'] ?? null, ['target' => '_blank']) ?></td>

                            <td><strong><?= Yii::t('backend/shipment', 'Updated At'); ?>:</strong></td>
                            <td><?= f::text($shipment->postageLabelData['updatedAt'] ?? null) ?></td>
                        </tr>
                    </table>

                </div>
            </div>
            <?= $this->render('/partials/back-btn', ['size' => 'lg']); ?>
        </div>

        <?= Box::end(); ?>
    </div>
</div>


<?php

$this->registerJs(<<<JS
    jQuery(function($){
        var pageH = $('.content-wrapper').height();
        $('.postage-label-container').height(pageH - 275);
    });
JS
, \yii\web\View::POS_READY);