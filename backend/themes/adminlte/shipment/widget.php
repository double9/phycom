<?php

/**
 * @var \phycom\backend\models\SearchShipment $dataProvider
 */

use phycom\backend\widgets\AjaxGrid;
use phycom\backend\widgets\Box;
use yii\helpers\Html;
use yii\helpers\Url;


echo Box::begin([
	'title' => Yii::t('backend/order', 'Shipments'),
	'options' => ['class' => 'box box-default'],
	'bodyOptions' => ['class' => 'box-body']
]);

	echo AjaxGrid::widget([
		'id' => 'shipments-grid',
		'dataProvider' => $dataProvider,
		'route' => 'order/shipments',
		'rowOptions' => function ($model) {
			return [
				'class' => 'row-link',
				'data-url' => Url::toRoute(['shipment/edit','id' => $model->id])
			];
		},
		'columns' => [
			[
				'attribute' => 'tracking_number',
				'format' => 'text',
				'enableSorting' => false,
			],
			[
				'attribute' => 'type',
				'format' => 'raw',
				'enableSorting' => false,
				'value' => function ($model) {
					/**
					 * @var \phycom\backend\models\SearchShipment $model
					 */
					return Html::tag('span', $model->type->label, ['class' => 'label ' . $model->type->labelClass]);
				},
				'options' => ['width' => '180']
			],
			[
				'attribute' => 'status',
				'format' => 'raw',
				'enableSorting' => false,
				'value' => function ($model) {
					/**
					 * @var \phycom\backend\models\SearchShipment $model
					 */
					return Html::tag('span', $model->status->label, ['class' => 'label ' . $model->status->labelClass]);
				},
				'options' => ['width' => '180']
			],
			[
				'attribute' => 'shipped_at',
				'format' => 'datetime',
				'enableSorting' => false,
				'options' => ['width' => '180']
			]
		]
	]);

echo Box::end();