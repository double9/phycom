<?php

/**
 * @var $this yii\web\View
 * @var \phycom\common\modules\delivery\models\DeliveryArea $model
 */

use phycom\backend\widgets\Box;
use phycom\backend\widgets\BtnAction;
use phycom\backend\widgets\DeleteAction;

use phycom\common\modules\delivery\models\DeliveryAreaStatus;

use yii2mod\rating\StarRating;
use rmrevin\yii\fontawesome\FA;

use yii\widgets\DetailView;
use yii\helpers\Html;

$this->title = Yii::t('backend/main', 'Delivery area {id}', ['id' => $model->id]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/main', 'Delivery areas'), 'url' => ['/delivery-area/index']];

?>

<div class="row">

	<div class="col-md-9">


		<?= Box::begin([
			'showHeader' => false,
			'options' => ['class' => 'box box-default'],
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

        <div class="row">
            <div class="col-md-12">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id:integer',
                        [
                            'attribute' => 'code',
                            'format' => 'html',
                            'value' => '<span class="label label-primary">' . $model->code . '</span>'
                        ],
                        'price:currency',
                        'method:text',
                        'carrier:text',
                        'service:text',
                        [
                            'attribute' => 'status',
                            'format' => 'html',
                            'value' => '<span class="label ' . $model->status->labelClass . '">' . $model->status->label . '</span>'
                        ],
                        [
                            'attribute' => 'area_code',
                            'format' => 'html',
                            'value' => '<span class="label label-default">' . $model->area_code . '</span>'
                        ],
                        'area:shortArea',
                        [
                            'attribute' => 'delivery_time',
                            'format' => 'text',
                            'visible' => (bool) $model->delivery_time
                        ],
                        'created_at:datetime',
                        'updated_at:datetime'// creation date formatted as datetime
                    ],
                ]);

                ?>

            </div>
        </div>


        <div class="row">
            <div class="col-md-4">
                <?= $this->render('/partials/back-btn', ['size' => 'lg']); ?>
            </div>
            <div class="col-md-8">
                <div class="pull-right" style="display: inline-block">
                    <?php

                    if ($model->status->is(DeliveryAreaStatus::ACTIVE)) {

                        echo BtnAction::widget(['model' => $model, 'attributes' => ['status' => DeliveryAreaStatus::DISABLED],
                            'label' => '<i class="fas fa-power-off" style="margin-right: 10px;"></i>' . Yii::t('backend/main', 'Disable'),
                            'options' => ['style' => 'display: inline-block; margin-left: 10px;'],
                            'btnOptions' => ['class' => 'btn btn-flat btn-warning btn-lg']
                        ]);

                    } else if ($model->status->is(DeliveryAreaStatus::DISABLED)) {
                        echo BtnAction::widget(['model' => $model, 'attributes' => ['status' => DeliveryAreaStatus::ACTIVE],
                            'label' => '<i class="fas fa-check" style="margin-right: 10px;"></i>' . Yii::t('backend/main', 'Enable'),
                            'options' => ['style' => 'display: inline-block; margin-left: 10px;'],
                            'btnOptions' => ['class' => 'btn btn-flat btn-success btn-lg']
                        ]);
                    }

                    echo BtnAction::widget(['model' => $model, 'attributes' => ['area_code' => $model->generateAreaCode(), 'code' => $model->generateCode()],
                        'label' => '<i class="fas fa-redo" style="margin-right: 10px;"></i>' . Yii::t('backend/main', 'Regenerate area codes'),
                        'options' => ['style' => 'display: inline-block; margin-left: 10px;']
                    ]);

                    echo DeleteAction::widget(['model' => $model, 'options' => ['style' => 'display: inline-block; margin-left: 10px;']]);

                    ?>
                </div>
            </div>
        </div>


		<?= Box::end(); ?>

	</div>

</div>



