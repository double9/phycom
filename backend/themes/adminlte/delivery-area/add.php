<?php

/**
 * @var $this yii\web\View
 */

use phycom\backend\widgets\Box;
use phycom\backend\assets\DeliveryAreaAsset;
use phycom\backend\models\DeliveryAreaForm;
use phycom\backend\widgets\ActiveForm;

use phycom\common\modules\delivery\Module as DeliveryModule;
use phycom\common\modules\delivery\models\DeliveryAreaStatus;

use yii\helpers\Html;
use yii\helpers\Url;

$model = new DeliveryAreaForm();

$this->title = Yii::t('backend/main', 'Add delivery area');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/main', 'Delivery areas'), 'url' => ['/delivery-area/index']];

DeliveryAreaAsset::register($this);
?>

<div class="row">

	<div class="col-md-9">


		<?php

            Box::begin([
                'showHeader' => false,
                'options' => ['class' => 'box box-default'],
                'bodyOptions' => ['class' => 'box-body']
            ]);

            $form = ActiveForm::begin([
                'id' =>  'delivery-area-form'
            ]);
        ?>

        <div class="row">
            <div class="col-md-12">
                <h4><i class="fas fa-truck" style="margin-right: 10px;"></i><?= Yii::t('backend/main', 'Delivery method'); ?></h4>
                <p class="small text-muted"><?= Yii::t('backend/main', 'Delivery provider/partner parameters'); ?></p>
                <hr style="margin-top: 0;">
            </div>
        </div>


        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'method')->dropDownList($model->getDeliveryMethods(), ['prompt' => Yii::t('backend/main', 'Select delivery method'), 'id' => 'select-delivery-method']) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'carrier')->textInput() ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'service')->textInput() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <br>
                <br>
                <h4><i class="fas fa-map-marker-alt" style="margin-right: 10px;"></i><?= Yii::t('backend/main', 'Area'); ?></h4>
                <p class="small text-muted"><?= Yii::t('backend/main', 'Delivery area defines the region where the selected price applies. Only self pickup locations should have full street address and postal code assigned'); ?></p>
                <hr style="margin-top: 0;">
            </div>
        </div>

        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-5">
                        <?= $form->field($model, 'country')->dropDownList($model->getCountries(), [
                                'data-url' => Url::toRoute(['address/divisions'])
                            ])
                        ?>
                    </div>
                    <div class="col-md-6 col-md-offset-1">
                        <?= $form->field($model, 'province')->dropDownList($model->getDivisions(), ['prompt' => '-']) ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-9">

                <div class="row">
                    <div class="col-md-5">
                        <?= $form->field($model, 'city')->textInput() ?>
                    </div>
                    <div class="col-md-1 text-center">
                        <p style="margin-top: 30px;"> — <?= Yii::t('backend/main', 'Or') ?> — </p>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'locality')->textInput() ?>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-5">
                        <?= $form->field($model, 'district')->textInput() ?>
                    </div>
                </div>
            </div>
        </div>

        <div id="street-details" class="row" <?= $model->method !== DeliveryModule::METHOD_SELF_PICKUP ? 'style="display: none;"' : '' ?>>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-5">
                        <?= $form->field($model, 'postcode')->textInput() ?>
                    </div>
                    <div class="col-md-6 col-md-offset-1">
                        <?= $form->field($model, 'street')->textInput() ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <br>
                <br>
                <h4><i class="fas fa-wrench" style="margin-right: 10px;"></i><?= Yii::t('backend/main', 'Options'); ?></h4>
                <p class="small text-muted"><?= Yii::t('backend/main', 'Pricing record state and other options'); ?></p>
                <hr style="margin-top: 0;">
            </div>
        </div>

        <div class="row">
            <div class="col-md-9">

                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'status')->dropDownList(DeliveryAreaStatus::displayValues()) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'price')->textInput() ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'deliveryTime')->textInput() ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <br>
                <br>
            </div>
        </div>



        <div class="row">
            <div class="col-md-4">
                <?= $this->render('/partials/back-btn', ['size' => 'lg']); ?>
            </div>
            <div class="col-md-8">
                <div class="pull-right" style="display: inline-block">
                    <?= Html::submitButton(Yii::t('backend/main', 'Submit'), ['class' => 'btn btn-lg btn-flat btn-primary']) ?>
                </div>
            </div>
        </div>


        <?php ActiveForm::end(); ?>
		<?php Box::end(); ?>

	</div>

</div>



