<?php

use phycom\backend\widgets\Box;
use phycom\backend\widgets\DataGrid;
use phycom\common\helpers\Filter;

use phycom\common\modules\delivery\models\DeliveryAreaStatus;

use yii\helpers\Url;

/**
 * @var $this yii\web\View
 * @var \phycom\backend\models\SearchDeliveryArea $model
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('backend/main', 'Delivery areas');
$this->params['breadcrumbs'][] = $this->title
?>
<div class="row">
	<div class="col-md-12">
		<?= Box::begin([
			'options' => ['class' => 'box box-default'],
			'showHeader' => false,
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

		<?= DataGrid::widget([
			'id' => 'user-grid',
			'dataProvider' => $dataProvider,
			'filterModel' => $model,
			'bulkEditForm' => 'bulk-edit',
			'rowOptions' => function ($model) {
				return [
					'class' => 'row-link',
					'data-url' => Url::toRoute(['delivery-area/edit','id' => $model->id])
				];
			},
			'actions' => [
				[
					'label' => '<span class="fas fa-plus"></span>&nbsp;&nbsp;' . Yii::t('backend/main', 'Add delivery area'),
					'url' => ['delivery-area/add'],
                    'options' => ['class' => 'btn btn-flat btn-success']
				]
			],
			'columns' => [
                [
                    'attribute' => 'code',
                    'format' => 'html',
                    'value' => function ($model) {
                        return '<span class="label label-primary">' . $model->code . '</span>';
                    },
                    'options' => ['width' => '100']
                ],
                [
	                'attribute' => 'method',
	                'format' => 'text',
                    'options' => ['width' => '200']
                ],
                [
                    'attribute' => 'carrier',
                    'format' => 'text',
                ],
                [
                    'attribute' => 'service',
                    'format' => 'text',
                    'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
                    'headerOptions' => ['class' => 'hidden-sm hidden-xs']
                ],
                [
                    'attribute' => 'area_code',
                    'format' => 'html',
                    'value' => function ($model) {
                        return '<span class="label label-default">' . $model->area_code . '</span>';
                    },
                    'options' => ['width' => '120']
                ],
                [
                    'attribute' => 'area',
                    'format' => 'shortArea',
                ],
                [
                    'attribute' => 'price',
                    'format' => 'currency',
                    'options' => ['width' => '120']
                ],
				[
					'attribute' => 'status',
					'filter' => Filter::dropDown($model, 'status', DeliveryAreaStatus::displayValues()),
					'format' => 'raw',
					'value' => function ($model) {
						return '<span class="label '.$model->status->labelClass.'">' . $model->status->label . '</span>';
					},
					'options' => ['width' => '120']
				],
				[
					'attribute' => 'created_at',
					'filter' => Filter::daterangepicker($model, 'createdFrom', 'createdTo'),
					'format' => 'datetime',
					'options' => ['width' => '220'],
					'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				],
				[
					'attribute' => 'updated_at',
					'filter' => Filter::daterangepicker($model, 'updatedFrom', 'updatedTo'),
					'format' => 'datetime',
					'options' => ['width' => '220'],
					'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				],
                [
                    'label' => false,
                    'format' => 'raw',
                    'content' => function($model) {

                        return $this->render('/partials/delete-dropdown', [
                            'label' => '<span class="far fa-trash-alt"></span>',
                            'url' => ['delivery-area/delete', 'id' => $model->id],
                            'options' => ['class' => 'toggle-hover'],
                            'side' => 'right'
                        ]);
                    },
                    'options' => ['width' => 40],
                    'contentOptions' => ['style' => 'padding: 2px 0 0 0;', 'class' => 'clickable']
                ]
			]
		]);
		?>

		<?= Box::end(); ?>
	</div>
</div>
