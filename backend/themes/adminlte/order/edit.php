<?php

use phycom\backend\widgets\Box;

use phycom\common\helpers\f;

use rmrevin\yii\fontawesome\FAR;

use yii\widgets\DetailView;
use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var \phycom\backend\models\OrderForm $model
 * @var \phycom\common\models\Order $order
 */

$this->title = Yii::t('backend/main', 'Order #{no} - {customer}', ['no' => $order->number, 'customer' => $order->getFullName()]);
$this->params['breadcrumbs'][] = Yii::t('backend/main', 'Sale');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/main', 'Orders'), 'url' => ['/order']];
$this->params['breadcrumbs'][] = $order->number
?>

<div class="row">
    <div class="col-lg-8 col-md-12">

		<?= Box::begin([
			'showHeader' => false,
			'options' => ['class' => 'box box-default'],
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

        <div class="row">
            <div class="col-md-8">
	            <?= DetailView::widget([
		            'id' => 'order-detail',
		            'model' => $order,
		            'attributes' => [
			            'number:text',
			            [
				            'label' => Yii::t('backend/order', 'Customer'),
				            'format' => 'html',
				            'value' => function ($model) {
	                            if (!$model->user_id) {
	                                return null;
                                }
					            /**
					             * @var $model \phycom\common\models\Order
					             */
					            $html = '<span class="name">' . f::titleCase($model->getFullName()) .  '</span>';
                                $html .= Html::a(
                                    FAR::i(FAR::_USER),
                                    ['/user/profile', 'id' => $model->user_id],
                                    ['class' => 'btn btn-default btn-flat btn-sm pull-right']
                                );
					            return $html;
				            },
                            'captionOptions' => ['style' => 'width: 200px;']
			            ],
			            'promotion_code:text',
			            [
				            'label' => $order->getAttributeLabel('status'),
				            'format' => 'raw',
				            'value' => '<span class="label ' . $order->status->labelClass . '">' . $order->status->label . '</span>'
			            ],
                        [
                            'label' => Yii::t('backend/order', 'Phone number'),
                            'format' => ['phone', true],
                            'value' => $order->phone_code . $order->phone_number
                        ],
			            [
				            'label' => Yii::t('backend/order', 'Email'),
				            'format' => 'email',
				            'value' => $order->email
			            ],
                        [
                            'attribute' => 'comment',
                            'format' => 'text',
                            'visible' => (bool)$order->comment
                        ],
                        [
                            'label' => Yii::t('backend/order', 'Shipment type'),
                            'format' => 'raw',
                            'value' => '<span class="label ' . $order->shipment->type->labelClass . '">' . $order->shipment->type->label . '</span>',
                            'visible' => $order->shipment
                        ],
                        [
                            'label' => Yii::t('backend/order', 'Address'),
                            'format' => 'addressLink',
                            'value' => $order->shipment->to_address,
                            'visible' => $order->shipment && $order->shipment->isDelivery
                        ],
                        [
                            'attribute' => 'paid_at',
                            'format' => 'raw',
                            'value' => $order->paid_at ? f::datetime($order->paid_at) : Html::tag('span', Yii::t('backend/order', 'Payment not received'), ['class' => 'label label-danger'])
                        ],
			            'created_at:datetime',
			            'updated_at:datetime'// creation date formatted as datetime
		            ],
	            ]);

	            ?>
            </div>
            <div class="col-md-4 clearfix">
                <div class="btn-group pull-right">
                <?php foreach ($model->order->invoices as $invoice): ?>
	                <?= Html::a(
	                        Yii::t('backend/order', 'Invoice # {no}', ['no' => $invoice->number]),
                            ['/invoice/edit', 'id' => $invoice->id],
                            ['class' => 'btn btn-flat ' . $invoice->status->getCssClass('btn-')])
                    ?>
                <?php endforeach; ?>
                </div>
            </div>
        </div>



        <br />
        <br />
	    <?= $this->render('/order/partials/order-items', ['order' => $order]); ?>
        <br />
        <br />
        <br />

		<?= Box::end(); ?>

        <div class="row visible-lg">
            <div class="col-md-12">
			    <?= Box::begin([
				    'showHeader' => false,
				    'options' => ['class' => 'box box-default'],
				    'bodyOptions' => ['class' => 'box-body']
			    ]);
			    ?>
                <div class="clearfix">
                    <div class="pull-left"><?= $this->render('/partials/back-btn', ['size' => 'lg']); ?></div>
                </div>
			    <?= Box::end(); ?>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-12">
        <?php
            $dataProvider = (new \phycom\backend\models\SearchShipment(['order_id' => $order->id]))->search();
            $dataProvider->pagination = false;
            echo $this->render('/shipment/widget', ['dataProvider' => $dataProvider]);
        ?>
    </div>
    <?php if ($order->user_id): ?>
        <div class="col-lg-4 col-md-12">
            <?= $this->render('widget', [
                    'user' => $order->user,
                    'exclude' => [$order->id],
                    'title' => Yii::t('backend/order', 'Previous orders')
            ]); ?>
        </div>
    <?php endif; ?>
</div>
<div class="row hidden-lg">
    <div class="col-md-12">
		<?= Box::begin([
			'showHeader' => false,
			'options' => ['class' => 'box box-default'],
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>
        <div class="clearfix">
            <div class="pull-left"><?= $this->render('/partials/back-btn', ['size' => 'lg']); ?></div>
        </div>
		<?= Box::end(); ?>
    </div>
</div>

