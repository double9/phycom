<?php

use phycom\backend\widgets\AjaxGrid;
use phycom\common\helpers\Url;
/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 */
$dataProvider = $dataProvider ?? (new \phycom\backend\models\SearchOrder())->recent();

?>

<?= AjaxGrid::widget([
    'id' => uniqid('orders-widget-grid-'),
    'dataProvider' => $dataProvider,
    'route' => '/order/recent',
    'rowOptions' => function ($model) {
        return [
            'class' => 'row-link',
            'data-url' => Url::toRoute(['order/edit','id' => $model->id])
        ];
    },
    'columns' => [
        [
            'attribute' => 'number',
            'label' => '#',
            'format' => 'text',
            'enableSorting' => false,
            'options' => ['width' => '200'],
        ],
        [
            'attribute' => 'customer',
            'format' => 'text',
            'enableSorting' => false,
            'contentOptions' => ['class' => 'hidden-xs'],
            'filterOptions' => ['class' => 'hidden-xs'],
            'headerOptions' => ['class' => 'hidden-xs'],

        ],
        [
            'attribute' => 'total',
            'format' => 'currencyDisplay',
            'options' => ['width' => '200']
        ],
        [
            'attribute' => 'status',
            'format' => 'raw',
            'value' => function ($model) {
                return '<span class="label '.$model->status->labelClass.'">' . $model->status->label . '</span>';
            },
            'enableSorting' => false,
            'options' => ['width' => '200']
        ],
        [
            'attribute' => 'created_at',
            'format' => 'datetime',
            'enableSorting' => false,
            'options' => ['width' => '220'],
            'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
            'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
            'headerOptions' => ['class' => 'hidden-sm hidden-xs']
        ],
    ]
]);
?>