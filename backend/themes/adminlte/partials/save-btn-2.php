<?php


use rmrevin\yii\fontawesome\FAS;

use yii\helpers\Html;

/**
 * @var $this yii\web\View
 */
$beforeSubmit = $beforeSubmit ?? null;
$formSelector = $formSelector ?? null;
$label = $label ?? Yii::t('backend/main', 'Save');
$loadingText = $loadingText ?? Yii::t('backend/main', 'Loading...');

$containerOptions = [
    'type' => 'submit',
    'class' => 'btn btn-danger btn-flat save'
];
if (isset($id)) {
    $containerOptions['id'] = $id;
} elseif (isset($containerOptions['id'])) {
    $id = $containerOptions['id'];
} else {
    $id = uniqid();
    $containerOptions['id'] = $id;
}
if (isset($class)) {
    Html::addCssClass($containerOptions, $class);
}

echo Html::button(FAS::i(FAS::_CHECK, ['style' => 'margin-right: 10px;']) . $label, $containerOptions);

if ($formSelector) {
    \phycom\backend\assets\FormSubmitAsset::register($this);
    $this->registerJs("SubmitButton.init('#$id', '$formSelector');", \yii\web\View::POS_END);
}

?>



