<?php

use rmrevin\yii\fontawesome\FAS;
use rmrevin\yii\fontawesome\FAR;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/**
 * @var array|string $url
 * @var string $label
 */
$options = $options ?? [];
$label = $label ?? FAR::i(FAR::_TRASH_ALT);
$confirmLabel = $confirmLabel ?? Yii::t('backend/main', 'Delete');
$side = $side ?? 'left';

$dropdownMenuCssClass = $side === 'left' ? 'dropdown-menu-left' : 'dropdown-menu-right';
$buttonOptions = ArrayHelper::merge([
    'data-toggle' => 'dropdown',
    'aria-haspopup' => 'true',
    'aria-expanded' => 'false'
], ($buttonOptions ?? []));
Html::addCssClass($buttonOptions, ['btn', 'btn-flat', 'btn-default', 'dropdown-toggle', 'delete-confirm']);
Html::addCssClass($options, 'btn-group btn-group-lg delete');
?>

<?= Html::beginTag('div', $options); ?>
    <?= Html::button($label, $buttonOptions) ?>
	<ul class="dropdown-menu no-padding <?= $dropdownMenuCssClass ?>">
		<li class="confirm"><?= Html::a(FAS::i(FAS::_EXCLAMATION_CIRCLE, ['style' => 'margin-right: 10px;']) . $confirmLabel, $url); ?></li>
		<li class="cancel">
			<?= Html::a(FAS::i(FAS::_TIMES_CIRCLE, ['style' => 'margin-right: 10px;']) . Yii::t('backend/main', 'Cancel'), 'javascript:;', ['onclick' => 'return false;', 'data-ajax' => 'false']); ?>
		</li>
	</ul>
<?= Html::endTag('div'); ?>

