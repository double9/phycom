<?php

use yii\helpers\Html;

/**
 * @var $this yii\web\View
 */

$containerOptions = isset($containerOptions) && !empty($containerOptions) ? $containerOptions : ['class' => 'btn-group-lg pull-right'];
Html::addCssClass($containerOptions, ['btn-group', 'btn-save-container', 'dropdown']);

if (isset($submitUrl)) {
    $containerOptions['data-submit-url'] = $submitUrl;
}

if (isset($class)) {
    Html::addCssClass($containerOptions, $class);
}

?>

<?= Html::beginTag('div', $containerOptions); ?>
    <button type="button" class="btn btn-danger btn-flat btn-save save"><span class="fas fa-check"></span>&nbsp;&nbsp;<?= Yii::t('backend/main', 'Save') ?></button>
    <button type="button" class="btn btn-danger btn-flat dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="caret"></span>
        <span class="sr-only"><?= Yii::t('backend/product', 'Toggle Dropdown') ?></span>
    </button>
    <ul class="dropdown-menu">
        <li>
            <a class="btn-save save-and-new" href="#">
                <i class="fas fa-plus"></i><?= Yii::t('backend/main', 'Save & New') ?>
            </a>
        </li>
        <li>
            <a class="btn-save save-and-duplicate" href="#">
                <i class="far fa-clone"></i><?= Yii::t('backend/main', 'Save & Duplicate') ?>
            </a>
        </li>
        <li>
            <a class="btn-save save-and-close" href="#">
                <i class="far fa-window-close"></i><?= Yii::t('backend/main', 'Save & Close') ?>
            </a>
        </li>
    </ul>
<?= Html::endTag('div'); ?>

