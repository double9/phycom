<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
/**
 * @var $this yii\web\View
 */

$sizeClass = isset($frame) ? 'btn-' . $frame : 'btn-lg';
$defaultOptions = [
    'class' => 'btn btn-flat btn-default ' . $sizeClass,
    'onclick' => 'window.history.back();'
];
$options = $options ?? [];

if (isset($class)) {
    Html::addCssClass($options, $class);
}

echo Html::a('<span class="fas fa-arrow-left"></span>&nbsp;&nbsp;' . Yii::t('backend/main', 'Back'), Yii::$app->request->referrer, ArrayHelper::merge($defaultOptions, $options));

