<?php
/**
 * @var $this yii\web\View
 * @var $model phycom\common\models\AddressForm
 */

use phycom\backend\widgets\ActiveForm;


use yii\helpers\Url;
use yii\helpers\Html;

$reflect = new \ReflectionClass($model);
$modelId = strtolower($reflect->getShortName());

$id = $id ?? $modelId;
?>

<?php $form = ActiveForm::begin(['id' => $id]); ?>

<div class="row">
	<div class="col-md-6">
		<?= $form->field($model, 'country')->dropDownList($model->countries, ['data-url' => Url::toRoute('/address/divisions')]); ?>
        <?= $form->field($model, 'city')->textInput(); ?>
		<?= $form->field($model, 'street')->textInput(); ?>
	</div>

	<div class="col-md-6">
		<?= $form->field($model, 'province')->dropDownList($model->divisions); ?>
        <?= $form->field($model, 'district')->textInput(); ?>
		<?= $form->field($model, 'postcode')->textInput(); ?>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<?= Html::submitButton(Yii::t('backend/user', 'Submit'), ['class' => 'btn btn-flat btn-primary submit']); ?>
	</div>
</div>

<?php ActiveForm::end(); ?>
