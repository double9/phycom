<?php

/**
 * @var $this yii\web\View
 * @var \phycom\common\models\Review $model
 */

use phycom\backend\widgets\Box;
use phycom\backend\widgets\BtnAction;
use phycom\backend\widgets\DeleteAction;

use phycom\common\models\attributes\ReviewStatus;

use yii2mod\rating\StarRating;
use rmrevin\yii\fontawesome\FAR;

use yii\widgets\DetailView;
use yii\helpers\Html;

$this->title = Yii::t('backend/review', 'Review {id}', ['id' => $model->id]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/review', 'Reviews'), 'url' => ['/review/index']];

?>

<div class="row">

	<div class="col-md-9">


		<?= Box::begin([
			'showHeader' => false,
			'options' => ['class' => 'box box-default'],
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

        <div class="row">
            <div class="col-md-12">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [
                            'label' => 	Yii::t('backend/review', 'Target'),
                            'format' => 'html',
                            'value' => function ($model) {
                                /**
                                 * @var \phycom\common\models\Review $model
                                 */
                                if ($model->product) {
                                    return Html::a(
                                            '<span class="fas fa-link"></span>&nbsp;&nbsp;' . Yii::t('backend/product', 'Product {id}', ['id' => $model->product->id]) . ' - ' . $model->product->translation->title,
                                            ['product/edit', 'id' => $model->product->id]
                                    );
                                }
                                return '';
                            },
                            'captionOptions' => ['style' => 'width: 200px;']
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'html',
                            'value' => '<span class="label ' . $model->status->labelClass . '">' . $model->status->label . '</span>'
                        ],
                        [
                            'attribute' => 'approved_by',
                            'format' => 'html',
                            'value' => function ($model) {
                                if ($model->approved_by) {
                                    return $model->approvedBy->fullName . '&nbsp;&nbsp;' .
                                        Html::a(FAR::i(FAR::_USER),
                                            ['/user/profile', 'id' => $model->approved_by],
                                            ['class' => 'btn btn-default btn-flat btn-sm']
                                        );
                                }
                                return null;
                            },
                            'visible' => (bool) $model->approved_by
                        ],
                        [
                            'attribute' => 'score',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return StarRating::widget([
                                    'name' => 'review-' . $model->id . '-score',
                                    'value' => $model->score,
                                    'options' => [],
                                    'clientOptions' => ['readOnly' => true],
                                ]);
                            }
                        ],
                        'title:text',
                        'description:html',    // description attribute in HTML
                        [
                            'attribute' => 'created_by',
                            'format' => 'html',
                            'value' => $model->createdBy->fullName . '&nbsp;&nbsp;' . Html::a(
                                FAR::i(FAR::_USER),
                                ['/user/profile', 'id' => $model->created_by],
                                ['class' => 'btn btn-default btn-flat btn-sm']
                            )
                        ],
                        'created_at:datetime',
                        'updated_at:datetime'// creation date formatted as datetime
                    ],
                ]);

                ?>

            </div>
        </div>


        <div class="row">
            <div class="col-md-4">
                <?= $this->render('/partials/back-btn', ['size' => 'lg']); ?>
            </div>
            <div class="col-md-8">
                <div class="pull-right" style="display: inline-block">
                    <?php



                    if ($model->status->is(ReviewStatus::PENDING)) {

                        echo BtnAction::widget(['model' => $model, 'attributes' => ['status' => ReviewStatus::REJECTED],
                            'label' => '<i class="fas fa-ban" style="margin-right: 10px;"></i>' . Yii::t('backend/review', 'Reject'),
                            'options' => ['style' => 'display: inline-block; margin-left: 10px;']
                        ]);

                        echo BtnAction::widget(['model' => $model, 'attributes' => ['status' => ReviewStatus::APPROVED],
                            'label' => '<i class="fas fa-check" style="margin-right: 10px;"></i>' . Yii::t('backend/review', 'Approve'),
                            'options' => ['style' => 'display: inline-block; margin-left: 10px;'],
                            'btnOptions' => ['class' => 'btn btn-flat bg-maroon btn-lg']
                        ]);
                    }

//                    echo DeleteAction::widget(['model' => $model, 'options' => ['style' => 'display: inline-block; margin-left: 10px;']]);

                    ?>
                </div>
            </div>
        </div>


		<?= Box::end(); ?>

	</div>

</div>



