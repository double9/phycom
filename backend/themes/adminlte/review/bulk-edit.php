<?php

use phycom\backend\widgets\ActiveForm;
use phycom\common\models\attributes\ReviewStatus;

use yii\helpers\Html;

$model = new \phycom\backend\models\review\ReviewBulkUpdateForm();
$modelName = (new \ReflectionClass($model))->getShortName();

$form = ActiveForm::begin();
?>
    <div class="keys" data-name="<?= $modelName . "[keys][]" ?>"></div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'status')
                    ->dropDownList(ReviewStatus::displayValues([ReviewStatus::DELETED, ReviewStatus::PENDING]), ['prompt' => Yii::t('backend/main', 'Select')])
                    ->label(false)
            ?>
        </div>
        <div class="col-md-3">
            <?= Html::submitButton('<span class="fas fa-exclamation-circle"></span>&nbsp;&nbsp;' . Yii::t('backend/main', 'Bulk update selected rows'), ['class' => 'btn btn-md btn-flat btn-primary']) ?>
        </div>
    </div>


<?php ActiveForm::end(); ?>
