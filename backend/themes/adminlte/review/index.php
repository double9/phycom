<?php

use phycom\backend\widgets\Box;
use phycom\backend\widgets\DataGrid;

use phycom\common\helpers\Filter;

use yii2mod\rating\StarRating;

use yii\helpers\Url;

/**
 * @var $this yii\web\View
 * @var \phycom\backend\models\review\SearchReview $model
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('backend/main', 'Reviews');
$this->params['breadcrumbs'][] = Yii::t('backend/main', 'Other');
$this->params['breadcrumbs'][] = $this->title
?>
<div class="row">
	<div class="col-md-12">
		<?= Box::begin([
			'options' => ['class' => 'box box-default'],
			'showHeader' => false,
			'bodyOptions' => ['class' => 'box-body']
		]);
		?>

		<?= DataGrid::widget([
			'id' => 'user-grid',
			'dataProvider' => $dataProvider,
			'filterModel' => $model,
            'bulkEditForm' => 'bulk-edit',
            'bulkEditCondition' => function ($model) {
                /**
                 * @var \phycom\backend\models\review\SearchReview $model
                 */
                return $model->status->isPending();
            },
			'rowOptions' => function ($model) {
				return [
					'class' => 'row-link',
					'data-url' => Url::toRoute(['review/edit','id' => $model->id])
				];
			},
			'columns' => [
				[
					'attribute' => 'id',
					'format' => 'integer',
					'options' => ['width' => '120'],
					'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				],
                [
                    'attribute' => 'author',
                    'format' => 'text',
                    'options' => ['width' => '200']
                ],
                [
	                'attribute' => 'score',
	                'format' => 'raw',
                    'value' => function ($model) {
                        /**
                         * @var $model \phycom\backend\models\SearchReview
                         */
		                return StarRating::widget([
                            'name' => 'review-' . $model->id . '-score',
                            'value' => $model->score,
                            'options' => [],
                            'clientOptions' => ['readOnly' => true],
                        ]);
                    },
	                'options' => ['width' => '140']
                ],
				[
					'attribute' => 'title',
					'format' => 'text',
					'contentOptions' => ['class' => 'text-clipped'],
				],
				[
					'attribute' => 'status',
					'filter' => Filter::dropDown($model, 'status', \phycom\common\models\attributes\ReviewStatus::displayValues()),
					'format' => 'raw',
					'value' => function ($model) {
						return '<span class="label '.$model->status->labelClass.'">' . $model->status->label . '</span>';
					},
					'options' => ['width' => '120']
				],
				[
					'attribute' => 'created_at',
					'filter' => Filter::daterangepicker($model, 'createdFrom', 'createdTo'),
					'format' => 'datetime',
					'options' => ['width' => '220'],
					'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				],
				[
					'attribute' => 'updated_at',
					'filter' => Filter::daterangepicker($model, 'updatedFrom', 'updatedTo'),
					'format' => 'datetime',
					'options' => ['width' => '220'],
					'contentOptions' => ['class' => 'hidden-sm hidden-xs'],
					'filterOptions' => ['class' => 'hidden-sm hidden-xs'],
					'headerOptions' => ['class' => 'hidden-sm hidden-xs']
				],
                [
                    'label' => false,
                    'format' => 'raw',
                    'content' => function($model) {

                        return $this->render('/partials/delete-dropdown', [
                            'label' => '<span class="far fa-trash-alt"></span>',
                            'url' => ['review/delete', 'id' => $model->id],
                            'options' => ['class' => 'toggle-hover'],
                            'side' => 'right'
                        ]);
                    },
                    'options' => ['width' => 40],
                    'contentOptions' => ['style' => 'padding: 2px 0 0 0;', 'class' => 'clickable']
                ]
			]
		]);
		?>

		<?= Box::end(); ?>
	</div>
</div>
