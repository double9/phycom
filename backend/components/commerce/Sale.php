<?php

namespace phycom\backend\components\commerce;

use phycom\common\interfaces\CommerceComponentInterface;

use yii\base\BaseObject;

/**
 * Class Sale
 *
 * @package phycom\backend\components\commerce
 */
class Sale extends BaseObject implements CommerceComponentInterface
{
    public bool $enabled = true;

    public function isEnabled()
    {
        return $this->enabled;
    }
}
