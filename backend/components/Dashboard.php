<?php

namespace phycom\backend\components;

use phycom\common\components\PhycomComponent;
use phycom\common\interfaces\PhycomComponentInterface;

/**
 * Class Dashboard
 *
 * @package phycom\backend\components
 */
class Dashboard extends PhycomComponent implements PhycomComponentInterface
{

}
