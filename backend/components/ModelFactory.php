<?php

namespace phycom\backend\components;

/**
 * Class ModelFactory
 * @package phycom\backend\components
 *
 * @method \phycom\backend\models\SearchInvoice getSearchInvoice(array $params = [])
 * @method \phycom\backend\models\product\SearchProduct getSearchProduct(array $params = [])
 */
class ModelFactory extends \phycom\common\components\ModelFactory
{
    protected array $definitions = [
        'getSearchInvoice' => 'backend\models\SearchInvoice',
        'getSearchProduct' => 'backend\models\product\SearchProduct',
        'getProductPrice'  => 'backend\models\product\ProductPrice'
    ];
}
