<?php
$params = array_merge(
    require(PHYCOM_PATH . '/common/config/params.php'),
    require(__DIR__ . '/params.php'),
	require(ROOT_PATH . '/common/config/params.php'),
	require(ROOT_PATH . '/backend/config/params.php')
);

return [
    'id' => 'app-backend',
    'basePath' => ROOT_PATH . '/backend',
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
    	'report' => [
    		'class' => \phycom\common\modules\report\Module::class,
	    ]
    ],
    'components' => [
        'modelFactory' => [
            'class' => phycom\backend\components\ModelFactory::class
        ],
        'request' => [
            'csrfParam' => '_csrf-ecom-backend',
	        'cookieValidationKey' => 'nNEt62zuWQid_RMOio!zK52HSZJ7ldGS', // a secret key required by cookie validation
        ],
        'user' => [
        	'class' => phycom\common\components\User::class,
            'identityClass' => phycom\common\models\User::class,
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-ecom-backend'],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => yii\log\FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'statistics' => [
            'class'   => \phycom\backend\components\Statistics::class,
            'enabled' => true
        ],
        'dashboard' => [
            'class'   => \phycom\backend\components\Dashboard::class,
            'enabled' => true
        ],
        'commerce' => [
            'components' => [
                'sale' => [
                    'class'   => \phycom\backend\components\commerce\Sale::class,
                    'enabled' => true
                ]
            ]
        ],
        'urlManager' => require(__DIR__ . '/url-manager.php'),
	    'assetManager' => [
		    'bundles' => [
			    'dmstr\web\AdminLteAsset' => [
				    'skin' => false,
			    ],
		    ],
	    ]
    ],
    'params' => $params,
];
