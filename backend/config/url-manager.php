<?php

return \yii\helpers\ArrayHelper::merge(
    [
        'class'           => yii\web\UrlManager::class,
        'baseUrl'         => '/',
        'enablePrettyUrl' => true,
        'showScriptName'  => false,
        'rules'           => [
            '/'                                      => 'site/index',
            '<controller:\w+>/<id:\d+>'              => '<controller>/edit',
            '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
            '<controller:\w+>/<action:\w+>/'         => '<controller>/<action>',
            'file/download/<bucket:\w+>/<filename>'  => 'file/download',
        ]
    ],
    require(ROOT_PATH . '/backend/config/url-manager.php')
);
