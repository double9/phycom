<?php
return [
    'adminEmail'           => 'admin@example.com',
    'showStatistics'       => true,                     // when true statistics menu item is visible - by default dashboard renders statistic page
    'productPageView'      => 'edit'                    // determines which view file to render on product detail page
];
