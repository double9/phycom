<?php

namespace phycom\backend\models;

use phycom\common\components\ActiveQuery;
use phycom\common\models\traits\SearchQueryFilter;
use phycom\common\interfaces\SearchModelInterface;

use phycom\common\modules\delivery\models\DeliveryArea;
use phycom\common\modules\delivery\models\DeliveryAreaStatus;

use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\Query;

/**
 * Class SearchDeliveryArea
 * @package phycom\backend\models
 */
class SearchDeliveryArea extends DeliveryArea implements SearchModelInterface
{
	use SearchQueryFilter;

	public $address;

	public $createdFrom;
	public $createdTo;
	public $updatedFrom;
	public $updatedTo;

	public function rules()
	{
		return [
		    [['id'], 'integer'],
            [['code', 'area_code', 'carrier', 'method', 'service'], 'string'],
            [['price'], 'number'],
            [['area', 'created_at', 'updated_at','createdFrom','createdTo','updatedFrom','updatedTo', 'status'], 'safe']
		];
	}

	/**
	 * Creates data provider instance with search query applied
	 * @param array $params
	 * @return ActiveDataProvider
	 */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
			'pagination' => [
				'pageSize' => 20
			],
		]);

		$this->sort($dataProvider->sort);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'da.id' => $this->id,
			'da.status' => (string)$this->status,
			'da.method' => $this->method
		]);

        $query->filterFullName(['da.area #>> \'{"country"}\'', 'da.area #>> \'{"province"}\'', 'da.area #>> \'{"city"}\'', 'da.area #>> \'{"district"}\''], (string)$this->area);
		$query->filterDateRange('da.created_at', $this->createdFrom, $this->createdTo);
		$query->filterDateRange('da.updated_at', $this->updatedFrom, $this->updatedTo);
		return $dataProvider;
	}

	protected function sort(Sort $sort)
	{

	}

	/**
	 * @return ActiveQuery
	 */
	protected function createSearchQuery()
	{
		$query = static::find();
		$query->select([
			'da.*'
		]);
		$query->from(['da' => DeliveryArea::tableName()]);
        $query->where(['not', ['da.status' => DeliveryAreaStatus::DELETED]]);

		return $query;
	}
}