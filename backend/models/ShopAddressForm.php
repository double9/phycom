<?php

namespace phycom\backend\models;

use phycom\common\models\Shop;
use phycom\common\models\AddressForm;

use yii;

class ShopAddressForm extends AddressForm
{
	protected $shop;

	public function __construct(Shop $shop, array $config = [])
	{
		$this->shop = $shop;
		parent::__construct($config);
	}

	public function init()
    {
        parent::init();
        $this->country = Yii::$app->country->defaultCountry;
    }

    public function getModel()
	{
		return $this->shop;
	}
}