<?php

namespace phycom\backend\models;

use phycom\common\components\ActiveQuery;
use phycom\common\models\traits\SearchQueryFilter;
use phycom\common\interfaces\SearchModelInterface;
use phycom\common\models\Address;
use phycom\common\models\attributes\ContactAttributeStatus;
use phycom\common\models\Phone;
use phycom\common\models\Vendor;

use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\Query;

/**
 * Class SearchVendor
 * @package phycom\backend\models
 */
class SearchVendor extends Vendor implements SearchModelInterface
{
	use SearchQueryFilter;

	public $address;
	public $phone;
	public $createdFrom;
	public $createdTo;
	public $updatedFrom;
	public $updatedTo;

	public function rules()
	{
		return [
			[['id', 'phone'], 'integer'],
            [['name', 'legal_name', 'reg_number'], 'string', 'max' => 255],
            [['address'], 'string'],
			[['created_at','updated_at','createdFrom','createdTo','updatedFrom','updatedTo', 'status'], 'safe'],
		];
	}

	/**
	 * Creates data provider instance with search query applied
	 * @param array $params
	 * @return ActiveDataProvider
	 */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
			'pagination' => [
				'pageSize' => 20
			],
		]);

		$this->sort($dataProvider->sort);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'v.id' => $this->id,
			'v.status' => (string)$this->status,
		]);

		$query->filterText('v.name', $this->name);
        $query->filterText('v.legal_name', $this->legal_name);
		$query->andFilterWhere(['like','p.phone_nr', $this->phone]);
		$query->filterFullName(['a.street', 'a.district', 'a.city'], $this->address);
		$query->filterDateRange('c.created_at', $this->createdFrom, $this->createdTo);
		$query->filterDateRange('c.updated_at', $this->updatedFrom, $this->updatedTo);
		return $dataProvider;
	}

	protected function sort(Sort $sort)
	{

	}

	/**
	 * @return ActiveQuery
	 */
	protected function createSearchQuery()
	{
		$query = static::find();
		$query->select([
			'v.*',
			'TRIM(CONCAT_WS(\', \', a.street, a.district, a.city)) AS address',
			'p.phone_nr AS phone'
		]);
		$query->from(['v' => Vendor::tableName()]);

		$query->leftJoin(['a' => 'LATERAL('.
			(new Query())
				->select('aa.*')
				->from(['aa' => Address::tableName()])
				->where('aa.vendor_id = v.id')
				->andWhere('aa.status != :status_deleted')
				->orderBy(['aa.id' => SORT_DESC . ' NULLS LAST','aa.updated_at' => SORT_DESC])
				->limit(1)
				->createCommand()
				->sql
			.')'
		], 'true', ['status_deleted' => ContactAttributeStatus::DELETED]);

		$query->leftJoin(['p' => 'LATERAL('.
			(new Query())
				->select('pp.*')
				->from(['pp' => Phone::tableName()])
				->where('pp.vendor_id = v.id')
				->andWhere('pp.status != :status_deleted')
				->orderBy(['pp.id' => SORT_DESC . ' NULLS LAST','pp.updated_at' => SORT_DESC])
				->limit(1)
				->createCommand()
				->sql
			.')'
		], 'true', ['status_deleted' => ContactAttributeStatus::DELETED]);

		return $query;
	}
}