<?php

namespace phycom\backend\models;

use phycom\common\models\traits\ModelTrait;
use phycom\common\models\attributes\SaleState;
use phycom\common\models\behaviors\DynamicAttributeBehavior;
use phycom\common\models\Setting;

use yii\helpers\ArrayHelper;
use yii\base\Model;
use yii;

/**
 * Class SiteSettingForm
 * @package phycom\backend\models
 */
class SiteSettingForm extends Model
{
	use ModelTrait;

	/**
	 * @var SaleState
	 */
	public $saleState;
	public $closedMessage;
	public $frontMessage;

	public function rules()
	{
		return [
			['saleState', 'required'],
			['closedMessage', 'trim'],
			['closedMessage', 'string'],
			['closedMessage',
				'required',
				'when' => function ($model) {
					/**
					 * @var static $model
					 */
					return (string) $model->saleState === SaleState::CLOSED;
				}
			],
			['frontMessage', 'trim'],
			['frontMessage', 'string'],
		];
	}

	public function attributeLabels()
	{
        return [
            'saleState'     => Yii::t('backend/setting', 'Sale state'),
            'closedMessage' => Yii::t('backend/setting', 'Closed Message'),
            'frontMessage'  => Yii::t('backend/setting', 'Front-page Message')
        ];
	}

	public function getDefaultValues()
	{
		return [
			'closedMessage' => 'Sorry but we are closed at the moment',
		];
	}

	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'dynamic-attribute' => [
				'class' => DynamicAttributeBehavior::class,
				'attributes' => [
					'saleState' => SaleState::class,
				]
			]
		]);
	}

	public function init()
	{
		parent::init();
		$this->loadValues();
	}

	public function loadValues()
	{
		/**
		 * @var Setting[] $settings
         */
        $settings = Setting::find()->where('')->all();
        $attributes = $this->activeAttributes();
        $defaults = $this->getDefaultValues();
        foreach ($settings as $setting) {
            $attribute = $setting->key;
            if (in_array($attribute, $attributes)) {
                $this->$attribute = $setting->value;
            }
        }
        foreach ($attributes as $attribute) {
            if ($this->$attribute === null && isset($defaults[$attribute])) {
                $this->$attribute = $defaults[$attribute];
            }
        }
	}

	public function update()
	{
		if ($this->validate()) {
			foreach ($this->attributes as $attribute => $value) {
				$setting = $this->getSetting($attribute);
				$setting->value = (string) $this->$attribute;
				if (!$setting->save()) {
					$this->addError('error', Yii::t('backend/setting', 'Error saving ' . $attribute));
				}
			}
			Yii::$app->unsetSettings();
			if ($this->hasErrors()) {
				return false;
			}
			return true;
		}
		return false;
	}

	protected function getSetting($key)
	{
		$setting = Setting::findOne(['key' => $key]);
		return $setting ?: new Setting(['key' => $key]);
	}
}
