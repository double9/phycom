<?php

namespace phycom\backend\models;

use phycom\common\models\PostCategory;
use phycom\common\models\PostCategoryPostRelation;
use phycom\common\models\PostTag;
use phycom\common\models\Shop;
use phycom\common\models\traits\ModelTrait;
use phycom\common\models\attributes\PostStatus;
use phycom\common\models\attributes\PostType;
use phycom\common\models\Post;

use yii\base\InvalidArgumentException;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use yii;

/**
 * Class PostForm
 *
 * @package phycom\backend\models
 *
 * @property-read Post $post
 * @property-read PostTranslationForm $translationForm
 * @property-read PostAttachmentForm $attachmentForm
 */
class PostForm extends Model
{
	use ModelTrait;

	public $identifier;
	public $status;
    public $categories = [];
    public $tags = [];
    public $shopId;

	/**
	 * @var Post
	 */
	protected $post;
    /**
     * @var PostAttachmentForm
     */
	protected $attachmentForm;
    /**
     * @var PostTranslationForm
     */
	protected $translationForm;
	/**
	 * PostForm constructor.
	 *
     * @param PostType $type
	 * @param Post|null $post
	 * @param array $config
	 */
	public function __construct(PostType $type, Post $post = null, array $config = [])
	{
		$this->post = $post ?? new Post(['type' => $type]);
		if ((string) $this->post->type !== (string) $type) {
		    throw new InvalidArgumentException('Invalid post type "' . (string) $type . '", "' . (string) $this->post->type . '" expected');
        }
		parent::__construct($config);
	}

	public function rules()
	{
		return [
			[['identifier', 'status'], 'string', 'max' => 255],
			['status', 'in', 'range' => PostStatus::all()],
            [['categories', 'tags'], 'safe'],
            ['categories', 'validateCategories'],
            ['shopId', 'exist', 'targetClass' => Shop::class, 'targetAttribute' => ['shopId' => 'id']]
		];
	}

	public function attributeLabels()
	{
		return [
			'identifier' => $this->post->getAttributeLabel('identifier'),
			'status' => $this->post->getAttributeLabel('status')
		];
	}

	public function init()
	{
		parent::init();

		if ($this->post->isNewRecord) {
			$this->post->loadDefaultValues();
		}

		$this->identifier = $this->post->identifier;
		$this->status = (string)$this->post->status;
		$this->shopId = $this->post->shop_id;
        $this->categories = $this->getPostCategories();
        $this->tags = $this->getPostTags();
	}

    public function getPostCategories()
    {
        $model = new SearchPostCategory();
        $model->language = Yii::$app->lang->current;

        $dataProvider = $model->search();
        $dataProvider->query->andWhere(['c.id' => ArrayHelper::getColumn($this->post->categories, 'id')]);

        return ArrayHelper::map($dataProvider->getModels(), 'id', 'title');
    }

    public function getAllCategories()
    {
        $model = new SearchPostCategory();
        $model->language = Yii::$app->lang->current;
        $dataProvider = $model->search();

        return ArrayHelper::map($dataProvider->getModels(), 'id', 'title');
    }

    public function validateCategories($attribute)
    {
        foreach ($this->$attribute as $key => $categoryId) {
            if (!$category = PostCategory::findOne(['id' => $categoryId])) {
                $this->addError($attribute, Yii::t('backend/error', 'Category {id} not found', ['id' => $categoryId]));
            }
        }
    }

    public function getPostTags()
    {
        return ArrayHelper::map($this->post->tags, 'value', 'value');
    }


	public function getPost()
	{
		return $this->post;
	}

    public function getAttachmentForm()
    {
        if (!$this->attachmentForm) {
            $this->attachmentForm = new PostAttachmentForm($this->post);
        }
        return $this->attachmentForm;
    }

    /**
     * @return PostTranslationForm
     */
    public function getTranslationForm()
    {
        if (!$this->translationForm) {
            $this->translationForm = new PostTranslationForm($this->post);
        }
        return $this->translationForm;
    }

    /**
     * Available page keys not being used.
     * @return array
     */
    public function getIdentifiers()
    {
        $keys = [];
        $pageSpaceItems = Yii::$app->pages->getItems();
        $allIdentifiers = [];
        $uniqueIdentifiers = [];
        foreach ($pageSpaceItems as $pageSpace) {
            if (false === $pageSpace->multiple) {
                $uniqueIdentifiers[] = $pageSpace->identifier;
            }
            $allIdentifiers[] = $pageSpace->identifier;
        }
        $identifiersUsed = (new Query())
            ->select('distinct(p.identifier) AS identifier')
            ->from(['p' => Post::tableName()])
            ->where(['identifier' => $uniqueIdentifiers])
            ->andWhere(['not', ['p.status' => PostStatus::DELETED]])
            ->column();

        $diff = array_diff($allIdentifiers, $identifiersUsed);

        if (!$this->post->isNewRecord && $this->post->identifier && !in_array($this->post->identifier, $diff)) {
            array_unshift($diff, $this->post->identifier);
        }

        foreach (Yii::$app->pages->getItems() as $pageSpace) {
            if (in_array($pageSpace->identifier, $diff)) {
                $keys[$pageSpace->identifier] = $pageSpace->name;
            }
        }
        return $keys;
    }


	public function update()
	{
		if ($this->validate()) {
			$transaction = Yii::$app->db->beginTransaction();
			try {
				$this->post->identifier = $this->identifier ?: $this->generateIdentifier();
				$this->post->status = new PostStatus($this->status);

				// mark other published landing pages as hidden as there should be only one landing page
				if ($this->post->type->is(PostType::LAND) && $this->post->status->is(PostStatus::PUBLISHED)) {
                    if ($this->post->isNewRecord) {
                        $command = Yii::$app->db->createCommand()
                            ->update(Post::tableName(), ['status' => PostStatus::HIDDEN], 'type = :type AND status = :status', [
                                'type'   => PostType::LAND,
                                'status' => PostStatus::PUBLISHED
                            ]);

                    } else {
                        $command = Yii::$app->db->createCommand()
                            ->update(Post::tableName(), ['status' => PostStatus::HIDDEN], 'type = :type AND status = :status AND id != :id', [
                                'type'   => PostType::LAND,
                                'status' => PostStatus::PUBLISHED,
                                'id' => $this->post->id
                            ]);
                    }
                    $command->execute();
                }

                $this->post->shop_id = $this->shopId;
				$this->post->vendor_id = Yii::$app->user->vendor->id;


				if ($this->post->isNewRecord) {
				    $this->post->created_by = Yii::$app->user->id;
                }

				if (!$this->post->save()) {
					return $this->rollback($transaction, $this->post->errors);
				}

                if (!$this->updateCategories($transaction)) {
				    return false;
                }
                if (!$this->updateTags($transaction)) {
                    return false;
                }

				$transaction->commit();
				return true;

			} catch (\Exception $e) {
				$transaction->rollBack();
				throw $e;
			}
		}
		return false;
	}


	protected function generateIdentifier()
    {
        return uniqid();
    }

    /**
     * @param yii\db\Transaction $transaction
     * @return bool
     * @throws yii\db\Exception
     */
    protected function updateCategories(yii\db\Transaction $transaction)
    {
        // remove category relations
        $deleteCondition = ['and', ['post_id' => $this->post->id]];
        if (!empty($this->categories)) {
            $deleteCondition[] = ['not in', 'category_id', $this->categories];
        }
        Yii::$app->db->createCommand()->delete(PostCategoryPostRelation::tableName(), $deleteCondition)->execute();

        if ($this->categories) {
            // reassign categories
            foreach ($this->categories as $categoryId) {

                $categoryRelation = PostCategoryPostRelation::findOne(['post_id' => $this->post->id, 'category_id' => $categoryId]);

                if (!$categoryRelation) {
                    $categoryRelation = new PostCategoryPostRelation();
                    $categoryRelation->category_id = (int)$categoryId;
                    $categoryRelation->post_id = $this->post->id;

                    if (!$categoryRelation->save()) {
                        return $this->rollback($transaction, $categoryRelation->errors);
                    }
                }
            }
        }
        return true;
    }

    /**
     * @param yii\db\Transaction $transaction
     * @return bool
     * @throws yii\db\Exception
     */
    protected function updateTags(yii\db\Transaction $transaction)
    {
        // remove existing product tags
        $deleteCondition = ['and', ['post_id' => $this->post->id]];
        if (!empty($this->tags)) {
            $deleteCondition[] = ['not in', 'value', $this->tags];
        }
        Yii::$app->db->createCommand()->delete(PostTag::tableName(), $deleteCondition)->execute();

        if ($this->tags) {
            // reassign categories
            foreach ($this->tags as $tag) {
                $productTag = PostTag::findOne(['post_id' => $this->post->id, 'value' => $tag]);
                if (!$productTag) {
                    $productTag = new PostTag();
                    $productTag->value = $tag;
                    $productTag->post_id = $this->post->id;
                    $productTag->created_by = Yii::$app->user->id;

                    if (!$productTag->save()) {
                        return $this->rollback($transaction, $productTag->errors);
                    }
                }
            }
        }
        return true;
    }
}
