<?php

namespace phycom\backend\models;

use phycom\common\components\ActiveQuery;
use phycom\common\models\attributes\ShopStatus;
use phycom\common\models\traits\SearchQueryFilter;
use phycom\common\interfaces\SearchModelInterface;
use phycom\common\models\Address;
use phycom\common\models\attributes\ContactAttributeStatus;
use phycom\common\models\Phone;
use phycom\common\models\Shop;

use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\Query;

/**
 * Class SearchShop
 * @package phycom\backend\models
 */
class SearchShop extends Shop implements SearchModelInterface
{
	use SearchQueryFilter;

	public $address;
	public $phone;
	public $createdFrom;
	public $createdTo;
	public $updatedFrom;
	public $updatedTo;

	public function rules()
	{
		return [
			[['id','vendor_id', 'phone'], 'integer'],
			[['address'], 'string'],
			[['created_at','updated_at','createdFrom','createdTo','updatedFrom','updatedTo', 'status'], 'safe'],
			[['name'], 'string', 'max' => 255]
		];
	}

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     * @throws \yii\base\Exception
     */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
			'pagination' => [
				'pageSize' => 20
			],
		]);

		$this->sort($dataProvider->sort);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			's.id' => $this->id,
			's.status' => (string)$this->status,
			's.vendor_id' => $this->vendor_id
		]);


		$query->filterText('s.name', $this->name);
		$query->andFilterWhere(['like','p.phone_nr', $this->phone]);
		$query->filterFullName(['a.street', 'a.district', 'a.city'], $this->address);
		$query->filterDateRange('c.created_at', $this->createdFrom, $this->createdTo);
		$query->filterDateRange('c.updated_at', $this->updatedFrom, $this->updatedTo);
		return $dataProvider;
	}

	protected function sort(Sort $sort)
	{

	}

    /**
     * @return ActiveQuery
     * @throws \yii\base\Exception
     */
	protected function createSearchQuery()
	{
		$query = static::find();
		$query->select([
			's.*',
			'TRIM(CONCAT_WS(\', \', a.street, a.district, a.city)) AS address',
			'CONCAT(p.country_code, p.phone_nr) AS phone'
		]);
		$query->from(['s' => Shop::tableName()]);

		$query->leftJoin(['a' => 'LATERAL('.
			(new Query())
				->select('aa.*')
				->from(['aa' => Address::tableName()])
				->where('aa.shop_id = s.id')
				->andWhere('aa.status != :status_deleted')
				->orderBy(['aa.id' => SORT_DESC . ' NULLS LAST','aa.updated_at' => SORT_DESC])
				->limit(1)
				->createCommand()
				->sql
			.')'
		], 'true', ['status_deleted' => ContactAttributeStatus::DELETED]);

		$query->leftJoin(['p' => 'LATERAL('.
			(new Query())
				->select('pp.*')
				->from(['pp' => Phone::tableName()])
				->where('pp.shop_id = s.id')
				->andWhere('pp.status != :status_deleted')
				->orderBy(['pp.id' => SORT_DESC . ' NULLS LAST','pp.updated_at' => SORT_DESC])
				->limit(1)
				->createCommand()
				->sql
			.')'
		], 'true', ['status_deleted' => ContactAttributeStatus::DELETED]);

        $query->where(['not', ['s.status' => ShopStatus::DELETED]]);

		return $query;
	}
}
