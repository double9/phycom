<?php

namespace phycom\backend\models;


use phycom\common\helpers\Currency;
use phycom\common\models\traits\ModelTrait;
use phycom\common\validators\RequiredOneValidator;
use phycom\common\modules\delivery\Module as DeliveryModule;
use phycom\common\modules\delivery\models\DeliveryArea;

use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii;

/**
 * Class DeliveryAreaBulkUpdateForm
 * @package phycom\backend\models
 */
class DeliveryAreaBulkUpdateForm extends Model
{
    use ModelTrait;

    public $price;
    public $status;
    public $keys = [];

    public function rules()
    {
        return [
            [['keys'], 'required'],
            [['price', 'status'], RequiredOneValidator::class],
            [['keys'], 'each', 'rule' => ['integer']],
            [['price'], 'number'],
            [['status'], 'string']
        ];
    }

    public function getDeliveryMethods()
    {
        /**
         * @var DeliveryModule $delivery
         */
        $delivery = Yii::$app->getModule('delivery');
        return ArrayHelper::map($delivery->getAllMethods(), 'id', 'name');
    }

    public function update()
    {
        if ($this->validate()) {
            $count = 0;
            foreach (DeliveryArea::find()->where(['id' => $this->keys])->batch() as $deliveryAreas) {
                /**
                 * @var DeliveryArea[] $deliveryAreas
                 */
                foreach ($deliveryAreas as $deliveryArea) {
                    if (!empty($this->price)) {
                        $deliveryArea->price = Currency::toInteger($this->price);
                    }
                    if (!empty($this->status)) {
                        $deliveryArea->status = $this->status;
                    }
                    if ($deliveryArea->save()) {
                        $count++;
                    } else {
                        $errors = $deliveryArea->getFirstErrors();
                        foreach ($errors as $attribute => $error) {
                            $this->addError('keys', $error);
                        }
                    }
                }
            }
            return $count;
        }
        return false;
    }
}