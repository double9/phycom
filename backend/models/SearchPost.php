<?php

namespace phycom\backend\models;

use phycom\common\models\AttachmentUrl;
use phycom\common\models\attributes\PostStatus;
use phycom\common\components\ActiveQuery;
use phycom\common\models\traits\SearchQueryFilter;
use phycom\common\interfaces\SearchModelInterface;
use phycom\common\models\File;
use phycom\common\models\Language;
use phycom\common\models\Post;
use phycom\common\models\PostAttachment;
use phycom\common\models\translation\PostTranslation;

use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\Query;
use yii\db\Expression;
use yii;


/**
 * Class SearchPost
 * @package phycom\backend\models
 */
class SearchPost extends Post implements SearchModelInterface
{
	use SearchQueryFilter;

	/**
	 * @var Language
	 */
	public $language;

	public $title;
	public $urlKey;

	public $fileName;
	public $fileBucket;

	public $updatedFrom;
	public $updatedTo;

	public $createdFrom;
	public $createdTo;


	public function rules()
	{
		return [
			[['id', 'vendor_id', 'created_by'], 'integer'],
			[['title'], 'string'],
			[['created_at','updated_at','createdFrom','createdTo','updatedFrom','updatedTo', 'status'], 'safe'],
			[['key', 'urlKey'], 'string', 'max' => 255],
		];
	}

	public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'title' => Yii::t('backend/post', 'Title')
        ]);
    }

    /**
     * @param string|null $metaKey
     * @param string|null $metaValue
     * @return AttachmentUrl|object
     * @throws yii\base\InvalidConfigException
     */
	public function getImageUrl($metaKey = null, $metaValue = null)
	{
        return Yii::createObject([
            'class'    => AttachmentUrl::class,
            'bucket'   => $this->fileBucket,
            'filename' => $this->fileName
        ]);
	}


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     * @throws yii\base\Exception
     */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
			'pagination' => [
				'pageSize' => 20
			],
		]);

		$this->sort($dataProvider->sort);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'p.id' => $this->id,
			'p.key' => $this->identifier,
			'p.status' => (string)$this->status,
		]);

        $query->filterMultiAttribute(['t1.url_key','t2.url_key'], $this->urlKey);
		$query->filterMultiAttribute(['t1.title','t2.title'], $this->title);
		$query->filterDateRange('p.created_at', $this->createdFrom, $this->createdTo);
		$query->filterDateRange('p.updated_at', $this->updatedFrom, $this->updatedTo);

		return $dataProvider;
	}

	protected function sort(Sort $sort)
	{
		$sort->attributes['title'] = [
			'asc' => [new Expression('COALESCE(t1.title, t2.title) ASC')],
			'desc' => [new Expression('COALESCE(t1.title, t2.title) DESC')],
		];
	}

    /**
     * @return ActiveQuery
     * @throws yii\base\Exception
     */
	protected function createSearchQuery()
	{
		$query = static::find();
		$query->select([
			'p.*',
			'CASE WHEN (t1.title IS NOT NULL) THEN t1.title ELSE t2.title END AS title',
            'CASE WHEN (t1.url_key IS NOT NULL) THEN t1.url_key ELSE t2.url_key END AS urlKey',
			'file.filename AS fileName',
			'file.bucket AS fileBucket'
		]);
		$query->from(['p' => Post::tableName()]);
		$query->where(['not', ['p.status' => PostStatus::DELETED]]);

		$query->leftJoin(['t1' => PostTranslation::tableName()], 't1.post_id = p.id AND t1.language = :lang');
		$query->leftJoin(['t2' => PostTranslation::tableName()], 't2.post_id = p.id AND t2.language = :fallback_lang AND t2.language != :lang');

        $query->addParams([
            'lang'          => $this->language->code,
            'fallback_lang' => Yii::$app->lang->findFallbackLanguage($this->language)->code
        ]);

		$query->leftJoin(
			['file' => 'LATERAL(' .
				(new Query())
					->select('f.*')
					->from(['pa' => PostAttachment::tableName()])
					->innerJoin(['f' => File::tableName()], 'f.id = pa.file_id')
					->where('pa.post_id = p.id')
					->orderBy(['pa.order' => SORT_ASC])
					->limit(1)
					->createCommand()
					->sql . ')'],
			'true'
		);

		if ($this->type) {
		    $query->andWhere(['p.type' => $this->type]);
        }

		return $query;
	}
}
