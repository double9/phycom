<?php

namespace phycom\backend\models;

use phycom\common\components\ActiveQuery;
use phycom\common\models\traits\SearchQueryFilter;
use phycom\common\interfaces\SearchModelInterface;
use phycom\common\models\Payment;

use yii\data\ActiveDataProvider;
use yii\data\Sort;

/**
 * Class SearchPayment
 * @package phycom\backend\models
 */
class SearchPayment extends Payment implements SearchModelInterface
{
	use SearchQueryFilter;

	public $createdFrom;
	public $createdTo;
	public $updatedFrom;
	public $updatedTo;
	public $transactionFrom;
	public $transactionTo;

	public function rules()
	{
        return [
            [['id', 'invoice_id'], 'integer'],
            [['amount'], 'number'],
            [['explanation','remitter','transaction_data'], 'string'],
            [['status','transaction_time', 'created_at', 'updated_at', 'createdFrom','createdTo','updatedFrom','updatedTo','transactionFrom','transactionTo'], 'safe'],
            [['transaction_id', 'payment_method', 'reference_number'], 'string', 'max' => 255],
        ];
	}

	/**
	 * Creates data provider instance with search query applied
	 * @param array $params
	 * @return ActiveDataProvider
	 */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
			'pagination' => [
				'pageSize' => 20
			],
		]);

		if ($this->invoice_id) {
			$query->andWhere(['p.invoice_id' => $this->invoice_id]);
		}

		$this->sort($dataProvider->sort);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'p.id' => $this->id,
			'p.payment_method' => $this->payment_method,
			'p.reference_number' => $this->reference_number,
			'p.status' => (string)$this->status,
			'p.amount' => $this->amount,
		]);

		$query->filterText('p.remitter', $this->remitter);
		$query->filterText('p.explanation', $this->explanation);
		$query->filterDateRange('p.created_at', $this->createdFrom, $this->createdTo);
		$query->filterDateRange('p.updated_at', $this->updatedFrom, $this->updatedTo);
		$query->filterDateRange('p.transaction_time', $this->transactionFrom, $this->transactionTo);
		return $dataProvider;
	}

	protected function sort(Sort $sort)
	{

	}

	/**
	 * @return ActiveQuery
	 */
	protected function createSearchQuery()
	{
		$query = static::find();
		$query->select([
			'p.*'
		]);
		$query->from(['p' => Payment::tableName()]);
		return $query;
	}
}