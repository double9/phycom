<?php

namespace phycom\backend\models;

use phycom\common\helpers\Date;
use phycom\common\models\attributes\InvoiceStatus;
use phycom\common\models\attributes\OrderStatus;
use phycom\common\models\attributes\PaymentStatus;
use phycom\common\models\Invoice;
use phycom\common\modules\payment\Module as PaymentModule;
use phycom\common\helpers\Currency;
use phycom\common\helpers\f;
use phycom\common\models\traits\ModelTrait;
use phycom\common\models\Payment;

use yii\base\Model;
use yii;

/**
 * Class PaymentForm
 * @package phycom\backend\models
 *
 * @property-read Payment $payment
 * @property-read array $paymentMethods
 * @property-write Invoice $invoice
 */
class PaymentForm extends Model
{
	use ModelTrait;

	public $amount;
	public $invoiceId;
	public $method;
	public $remitter;
	public $referenceNumber;
	public $explanation;
	public $transactionId;
	public $transactionDate;

	protected $invoice;
	protected $payment;

	public function __construct(Payment $payment = null, array $config = [])
	{
		$this->payment = $payment ?: new Payment();
		parent::__construct($config);
	}

	public function init()
	{
		parent::init();
		if ($this->payment->isNewRecord) {
			$this->setDefaultValues();
		} else {
			$this->amount = Currency::toDecimal($this->payment->amount);
			$this->invoiceId = $this->payment->invoice_id;
			$this->method = $this->payment->payment_method;
			$this->remitter = $this->payment->remitter;
			$this->referenceNumber = $this->payment->reference_number;
			$this->explanation = $this->payment->explanation;
			$this->transactionDate = f::date($this->payment->transaction_time);
			$this->transactionId = $this->payment->transaction_id;
		}
	}

	public function setDefaultValues()
	{
		$time = $this->invoice ? $this->invoice->order->updated_at : new \DateTime();
		$this->transactionDate = f::date($time);
		$this->transactionId = $time->format('U') . '_' . uniqid();
		$this->method = PaymentModule::METHOD_CASH;

		if ($this->invoice) {
			$this->remitter = $this->invoice->order->user->fullName;
			$this->referenceNumber = $this->invoice->order->user->reference_number;
			$this->amount = number_format(Currency::toDecimal($this->invoice->total), 2);
		}
	}

	public function rules()
	{
		return [
			[['amount', 'method', 'invoiceId', 'remitter'], 'required'],
			['amount', 'number'],
			['amount', 'validateAmount'],
			['invoiceId', 'integer'],
			[['explanation', 'remitter', 'method', 'transactionId'], 'string'],
			[
				'transactionDate',
				'date',
				'format' => Yii::$app->formatter->dateFormat
			],
		];
	}

	public function attributeLabels()
	{
		return [
			'amount' => $this->payment->getAttributeLabel('amount'),
			'invoiceId' => $this->payment->getAttributeLabel('invoice_id'),
			'method' => $this->payment->getAttributeLabel('payment_method'),
			'remitter' => $this->payment->getAttributeLabel('remitter'),
			'referenceNumber' => $this->payment->getAttributeLabel('reference_number'),
			'explanation' => $this->payment->getAttributeLabel('explanation'),
			'transactionId' => $this->payment->getAttributeLabel('transaction_id'),
			'transactionDate' => Yii::t('backend/payment', 'Transaction Date'),
		];
	}

	public function beforeValidate()
	{
		$this->amount = str_replace(',','.', $this->amount);
		return parent::beforeValidate();
	}

	public function validateAmount($attribute)
	{
		if ((float)$this->$attribute <= 0) {
			$this->addError($attribute, Yii::t('backend/payment', 'Invalid amount.'));
		}
		$decimals = strlen(substr(strrchr($this->$attribute, "."), 1));
		if ($decimals !== 2) {
			$this->addError($attribute, Yii::t('backend/payment', 'Amount must have 2 decimal digits'));
		}
	}

	public function getPayment()
	{
		return $this->payment;
	}

	public function getPaymentMethods()
	{
		/**
		 * @var PaymentModule $payment
		 */
		$payment = Yii::$app->getModule('payment');
		$methods = $payment->methods;

		return yii\helpers\ArrayHelper::map($methods, 'id', 'label');
	}

	public function setInvoice(Invoice $invoice)
	{
		$this->invoiceId = $invoice->id;
		$this->invoice = $invoice;

		if ($this->payment->isNewRecord) {
			$this->setDefaultValues();
		}
	}

	/**
	 * @return \DateTime
	 */
	public function getTransactionTime()
	{
		if ($this->payment->transaction_time) {
			return Date::create($this->transactionDate . 'T' . $this->payment->transaction_time->format('H:i:s'))->dateTime;
		} else {
			$curTime = new \DateTime();
			$transactionDate = Date::create($this->transactionDate, Yii::$app->formatter->dateFormat)->dateTime;
			if ((int) $curTime->format('Ymd') > (int) $transactionDate->format('Ymd')) {
				return $transactionDate->setTime(23,59,59);
			} else {
				return Date::create($this->transactionDate . 'T' . $curTime->format('H:i:s'))->dateTime;
			}
		}
	}

	public function save()
	{
		if ($this->validate()) {

			$transaction = Yii::$app->db->beginTransaction();

			try {
				$this->payment->amount = Currency::toInteger($this->amount);
				$this->payment->invoice_id = $this->invoiceId;
				$this->payment->reference_number = $this->referenceNumber;
				$this->payment->transaction_time = $this->getTransactionTime();
				$this->payment->transaction_id = $this->transactionId;
				$this->payment->remitter = $this->remitter;
				$this->payment->explanation = $this->explanation;
				$this->payment->payment_method = $this->method;
				$this->payment->status = PaymentStatus::COMPLETED;

				if (!$this->payment->save()) {
					return $this->rollback($transaction);
				}
				unset($this->invoice->payments);

				if ($this->invoice->totalPaid >= $this->invoice->total) {
					$this->invoice->updateStatus(InvoiceStatus::PAID);
				}

				if ($this->invoice->order->isPaid()) {
					switch ($this->invoice->order->status->value) {
						case OrderStatus::PENDING:
							$this->invoice->order->updateStatus(OrderStatus::PAYMENT_COMPLETE);
							break;
						case OrderStatus::SHIPPED:
							if ($this->invoice->order->shipmentsDelivered()) {
								$this->invoice->order->updateStatus(OrderStatus::COMPLETE);
							}
					}
				}

				$transaction->commit();
				return $this->payment;

			} catch (\Exception $e) {
				$transaction->rollBack();
				throw $e;
			}
		}
		return false;
	}
}