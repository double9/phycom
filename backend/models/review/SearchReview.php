<?php

namespace phycom\backend\models\review;

use phycom\common\components\ActiveQuery;
use phycom\common\models\product\ProductReview;
use phycom\common\models\Review;
use phycom\common\models\ShopReview;
use phycom\common\models\traits\SearchQueryFilter;
use phycom\common\interfaces\SearchModelInterface;

use phycom\common\models\User;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\data\Sort;

/**
 * Class SearchReview
 * @package phycom\backend\models
 */
class SearchReview extends Review implements SearchModelInterface
{
	use SearchQueryFilter;

	public $author;
	public $productId;
	public $shopId;

	public $createdFrom;
	public $createdTo;
	public $updatedFrom;
	public $updatedTo;

	public function rules()
	{
		return [
            [['id', 'approved_by', 'created_by', 'score', 'productId', 'shopId'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['description', 'author'], 'string'],
            [['status', 'created_at', 'updated_at', 'createdFrom', 'createdTo', 'updatedFrom', 'updatedTo'], 'safe'],
		];
	}

	/**
	 * Creates data provider instance with search query applied
	 * @param array $params
	 * @return ActiveDataProvider
     * @throws Exception
	 */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
			'pagination' => [
				'pageSize' => 20
			],
		]);

		$this->sort($dataProvider->sort);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'r.id' => $this->id,
			'r.score' => $this->score,
			'r.status' => (string)$this->status,
            'r.created_by' => $this->created_by,
            'p.product_id' => $this->productId,
            's.shop_id' => $this->shopId
		]);

        $query->filterFullName(['u.first_name', 'u.last_name'], $this->author);
        $query->filterText('r.title', $this->title);
		$query->filterText('r.description', $this->description);
		$query->filterDateRange('r.created_at', $this->createdFrom, $this->createdTo);
		$query->filterDateRange('r.updated_at', $this->updatedFrom, $this->updatedTo);
		return $dataProvider;
	}

	protected function sort(Sort $sort)
	{

	}

	/**
	 * @return ActiveQuery
     * @throws Exception
	 */
	protected function createSearchQuery()
	{
		$query = static::find();
		$query->select([
		    'r.*',
            'TRIM(CONCAT_WS(\' \', u.first_name, u.last_name)) as author',
            'p.product_id AS "productId"',
            's.shop_id AS "shopId"'
        ]);
		$query->from(['r' => Review::tableName()]);
		$query->innerJoin(['u' => User::tableName()], 'r.created_by = u.id');
        $query->leftJoin(['p' => ProductReview::tableName()], 'p.review_id = r.id');
        $query->leftJoin(['s' => ShopReview::tableName()], 's.review_id = r.id');

        return $query;
	}
}
