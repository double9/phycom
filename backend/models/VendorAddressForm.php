<?php

namespace phycom\backend\models;

use phycom\common\models\Vendor;
use phycom\common\models\AddressForm;

class VendorAddressForm extends AddressForm
{
	protected $vendor;

	public function getModel()
	{
		return $this->vendor;
	}

	public function __construct(Vendor $vendor, array $config = [])
	{
		$this->vendor = $vendor;
		parent::__construct($config);
	}
}