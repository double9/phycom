<?php

namespace phycom\backend\models\product;


use phycom\common\helpers\Currency;
use phycom\common\helpers\ProductCategoryLabel;
use phycom\common\models\attributes\ProductStatus;
use phycom\common\models\product\Product;
use phycom\common\models\product\ProductCategory;
use phycom\common\models\product\ProductCategoryProductRelation;
use phycom\common\models\product\ProductTag;
use phycom\common\models\traits\ClassConstantTrait;
use phycom\common\models\traits\ModelTrait;

use yii\helpers\ArrayHelper;
use yii\base\Model;
use yii;

/**
 * Class ProductBulkUpdateForm
 * @package phycom\backend\models\product
 */
class ProductBulkUpdateForm extends Model
{
    const FIELD_ACTION_ADD = 'add';
    const FIELD_ACTION_REPLACE = 'replace';
    const FIELD_ACTION_REMOVE = 'remove';

    use ClassConstantTrait;
    use ModelTrait;

    public $price;
    public $status;

    public $tags = [];
    public $tagAction;

    public $categories = [];
    public $categoryAction;

    public $keys = [];

    public function rules()
    {
        return [
            [['keys'], 'required'],
            [['price'], 'number'],
            [['status'], 'string'],
            [['status'], 'in', 'range' => ProductStatus::all()],
            [['keys', 'categories'], 'each', 'rule' => ['integer']],
            [['categories'], 'validateCategories'],
            [['tags'], 'each', 'rule' => ['string']],
            [['tags'], 'in', 'range' => Yii::$app->modelFactory->getProductTag()::allTags(), 'allowArray' => true],
            [['categoryAction'], 'required', 'when' => function () {
                return !empty($this->categories);
            }, 'whenClient' => "(attribute, value) => jQuery('#productbulkupdateform-categories').val().length"],
            [['tagAction'], 'required', 'when' => function () {
                return !empty($this->tags);
            }, 'whenClient' => "(attribute, value) => jQuery('#productbulkupdateform-tags').val().length"
            ],
            [['categoryAction', 'tagAction'], 'in', 'range' => static::getConstants('FIELD_ACTION')]
        ];
    }

    public function attributeLabels()
    {
        return [
            'price'          => Yii::t('backend/main', 'Price'),
            'status'         => Yii::t('backend/main', 'Select status'),
            'tags'           => Yii::t('backend/product', 'Tags'),
            'tagAction'      => Yii::t('backend/product', 'Select tag action'),
            'categories'     => Yii::t('backend/product', 'Categories'),
            'categoryAction' => Yii::t('backend/product', 'Select category action')
        ];
    }

    /**
     * @return array
     */
    public function getFieldActions()
    {
        return [
            static::FIELD_ACTION_ADD => Yii::t('backend/main', 'Add'),
            static::FIELD_ACTION_REPLACE => Yii::t('backend/main', 'Replace'),
            static::FIELD_ACTION_REMOVE => Yii::t('backend/main', 'Remove'),
        ];
    }


    public function validateCategories($attribute)
    {
        foreach ($this->$attribute as $key => $categoryId) {
            if (!$category = ProductCategory::findOne($categoryId)) {
                $this->addError($attribute, Yii::t('backend/error', 'Category {id} not found', ['id' => $categoryId]));
            }
        }
    }

    public function getAllCategories()
    {
        return (new ProductCategoryLabel())->generateLabels();
    }

    public function getAllTags()
    {
        return ArrayHelper::map(Yii::$app->modelFactory->getProductTag()::visible(), 'value', 'label');
    }


    public function update()
    {
        if ($this->validate()) {
            $count = 0;
            foreach ($this->createQuery()->batch() as $products) {
                /**
                 * @var Product[] $products
                 */
                foreach ($products as $product) {

                    if (empty($this->status) && empty($this->price) && empty($this->tags) && empty($this->categories)) {
                        continue;
                    }

                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        if (!empty($this->price)) {
                            $productPrice = $product->prices[0];
                            $productPrice->price = Currency::toInteger($this->price);
                            if (!$productPrice->save()) {
                                $errors = $productPrice->getFirstErrors();
                                foreach ($errors as $attribute => $error) {
                                    $this->addError('price', $error);
                                }
                                $transaction->rollBack();
                                continue;
                            }
                        }

                        if (!empty($this->status)) {
                            $product->status = $this->status;

                            if (!$product->save()){
                                $errors = $product->getFirstErrors();
                                foreach ($errors as $attribute => $error) {
                                    $this->addError('keys', $error);
                                }
                                $transaction->rollBack();
                                continue;
                            }
                        }

                        if (!empty($this->tags)) {
                            switch ($this->tagAction) {

                                case self::FIELD_ACTION_ADD:
                                    if ($this->addTags($product) === false) {
                                        $transaction->rollBack();
                                        continue 2;
                                    }
                                    break;
                                case self::FIELD_ACTION_REPLACE:
                                    if ($this->replaceTags($product) === false) {
                                        $transaction->rollBack();
                                        continue 2;
                                    }
                                    break;
                                case self::FIELD_ACTION_REMOVE:
                                    $this->removeTags($product);
                                    break;

                            }
                        }

                        if (!empty($this->categories)) {
                            switch ($this->categoryAction) {

                                case self::FIELD_ACTION_ADD:
                                    if ($this->addCategories($product) === false) {
                                        $transaction->rollBack();
                                        continue 2;
                                    }
                                    break;
                                case self::FIELD_ACTION_REPLACE:
                                    if ($this->replaceCategories($product) === false) {
                                        $transaction->rollBack();
                                        continue 2;
                                    }
                                    break;
                                case self::FIELD_ACTION_REMOVE:
                                    $this->removeCategories($product);
                                    break;

                            }
                        }

                        $transaction->commit();
                        $count++;

                    } catch (\Exception $e) {
                        $transaction->rollBack();
                        throw $e;
                    }
                }
            }
            return $count;
        }
    }

    protected function addCategories(Product $product)
    {
        // reassign categories
        foreach ($this->categories as $categoryId) {

            $categoryRelation = ProductCategoryProductRelation::findOne(['product_id' => $product->id, 'category_id' => $categoryId]);

            if (!$categoryRelation) {
                $categoryRelation = new ProductCategoryProductRelation();
                $categoryRelation->category_id = (int)$categoryId;
                $categoryRelation->product_id = $product->id;

                if (!$categoryRelation->save()) {
                    Yii::error($categoryRelation->errors, __METHOD__);
                    return false;
                }
            }
        }
        return true;
    }

    protected function replaceCategories(Product $product)
    {
        // remove category relations that does not exist in our new categories list
        Yii::$app->db->createCommand()->delete(ProductCategoryProductRelation::tableName(), [
            'and',
            ['product_id' => $product->id],
            ['not in', 'category_id', $this->categories]
        ])->execute();

        return $this->addCategories($product);
    }


    protected function removeCategories(Product $product)
    {
        return Yii::$app->db->createCommand()->delete(ProductCategoryProductRelation::tableName(), [
            'and',
            ['product_id' => $product->id],
            ['in', 'category_id', $this->categories]
        ])->execute();
    }


    protected function addTags(Product $product)
    {
        foreach ($this->tags as $tag) {
            $productTag = Yii::$app->modelFactory->getProductTag()::findOne(['product_id' => $product->id, 'value' => $tag]);
            if (!$productTag) {
                $productTag = Yii::$app->modelFactory->getProductTag();
                $productTag->value = $tag;
                $productTag->product_id = $product->id;
                $productTag->created_by = Yii::$app->user->id;

                if (!$productTag->save()) {
                    Yii::error($productTag->errors, __METHOD__);
                    return false;
                }
            }
        }
        return true;
    }

    protected function replaceTags(Product $product)
    {
        // remove existing product tags that does not exist in our new tag list
        Yii::$app->db->createCommand()->delete(ProductTag::tableName(), [
            'and',
            ['product_id' => $product->id],
            ['not in', 'value', $this->tags]
        ])->execute();
        return $this->addTags($product);
    }

    protected function removeTags(Product $product)
    {
        return Yii::$app->db->createCommand()->delete(ProductTag::tableName(), [
            'and',
            ['product_id' => $product->id],
            ['in', 'value', $this->tags]
        ])->execute();
    }

    /**
     * @return yii\db\ActiveQuery
     */
    protected function createQuery()
    {
        return Yii::$app->modelFactory->getProduct()::find()->where(['id' => $this->keys]);
    }
}
