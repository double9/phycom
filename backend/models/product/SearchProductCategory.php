<?php

namespace phycom\backend\models\product;

use phycom\common\components\ActiveQuery;
use phycom\common\models\traits\SearchQueryFilter;
use phycom\common\interfaces\SearchModelInterface;
use phycom\common\models\attributes\CategoryStatus;
use phycom\common\models\attributes\ShopStatus;
use phycom\common\models\attributes\TranslationStatus;
use phycom\common\models\Language;
use phycom\common\models\product\ProductCategory;
use phycom\common\models\Shop;
use phycom\common\models\translation\ProductCategoryTranslation;

use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\Query;
use yii;

/**
 * Class SearchProductCategory
 * @package phycom\backend\models\product
 *
 * @property int $numTotal
 * @property int $numVisible
 * @property int $numHidden
 * @property int $numTranslated
 * @property int $numTranslationPending
 */
class SearchProductCategory extends ProductCategory implements SearchModelInterface
{
	use SearchQueryFilter;

	/**
	 * @var Language
	 */
	public $language;
	/**
	 * @var ProductCategory
	 */
	public $category;

	public $title;
	public $description;
	public $createdFrom;
	public $createdTo;

	public $vendorId;

	public $includeUnassignedShop = false;

	public function rules()
	{
		$rules = parent::rules();
		$rules[] = [['id','vendorId'], 'integer'];
		$rules[] = [['title', 'description'], 'string'];
		$rules[] = [['createdFrom','createdTo'],'safe'];
		return $rules;
	}

	public function getNumTotal()
	{
		return $this->createSearchQuery()->count();
	}

	public function getNumVisible()
	{
		return $this->createSearchQuery()->andWhere(['c.status' => CategoryStatus::VISIBLE])->count();
	}

	public function getNumHidden()
	{
		return $this->createSearchQuery()->andWhere(['c.status' => CategoryStatus::HIDDEN])->count();
	}

	public function getNumTranslated()
	{
		return $this->createSearchQuery()->andWhere([
			'and',
			't1.id IS NOT NULL',
			['t1.status' => TranslationStatus::PUBLISHED]
		])->count();
	}

	public function getNumTranslationPending()
	{
		return $this->createSearchQuery()->andWhere([
			'and',
			't1.id IS NOT NULL',
			['t1.status' => TranslationStatus::DRAFT]
		])->count();
	}


	/**
	 * Creates data provider instance with search query applied
	 * @param array $params
	 * @return ActiveDataProvider
	 */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => 20
			],
		]);

		$this->sort($dataProvider->sort);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'c.id' => $this->id,
		]);

		$query->filterText('t.title', $this->title);
		$query->filterText('t.description', $this->description);
		$query->filterDateRange('c.created_at', $this->createdFrom, $this->createdTo);

		return $dataProvider;
	}

    /**
     * @return ActiveQuery
     */
    public function createSearchQuery()
    {
        $query = static::find();
        $query->select([
            'c.*',
            'CASE WHEN (t1.title IS NOT NULL) THEN t1.title ELSE t2.title END AS title',
        ]);
        $query->from(['c' => ProductCategory::tableName()]);
        $query->where(['not', ['c.status' => CategoryStatus::DELETED]]);

        $query->leftJoin(['t1' => ProductCategoryTranslation::tableName()], 't1.product_category_id = c.id AND t1.language = :lang');
        $query->leftJoin(['t2' => ProductCategoryTranslation::tableName()], [
            'or',
            // fallback to default language
            [
                'and',
                't2.product_category_id = c.id',
                't2.language = :fallback_lang',
                't2.language != :lang',
                't2.title IS NOT NULL',
                't2.url_key IS NOT NULL'
            ],
            // or if there is no record with default language then any other language with content
            [
                'and',
                't2.product_category_id = c.id',
                't2.language != :lang',
                't2.title IS NOT NULL',
                't2.url_key IS NOT NULL'
            ]
        ]);
        $query->addParams([
            'lang' => $this->language->code,
            'fallback_lang' => Yii::$app->lang->findFallbackLanguage($this->language)->code
        ]);

        if ($this->vendorId) {

            $vendorQuery = (new Query)
                ->select('s.id')
                ->from(['s' => Shop::tableName()])
                ->where(['not', ['s.status' => ShopStatus::DELETED]])
                ->andWhere(['s.vendor_id' => $this->vendorId]);

            if ($this->includeUnassignedShop) {
                $query->andWhere([
                    'or',
                    ['c.shop_id' => $vendorQuery],
                    'c.shop_id IS NULL'
                ]);
            } else {
                $query->andWhere(['c.shop_id' => $vendorQuery]);
            }
        }

        if ($this->shop_id) {
            if ($this->includeUnassignedShop) {
                $query->andWhere([
                    'or',
                    ['c.shop_id' => $this->shop_id],
                    'c.shop_id IS NULL'
                ]);
            } else {
                $query->andWhere(['c.shop_id' => $this->shop_id]);
            }
        }

        return $query;
    }

	protected function sort(Sort $sort)
	{
		$sort->defaultOrder = ['parent_id' => SORT_ASC, 'order' => SORT_ASC]; //[new Expression('c.parent_id DESC NULLS FIRST'), new Expression('c.order DESC')];
	}


}
