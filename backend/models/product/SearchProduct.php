<?php

namespace phycom\backend\models\product;


use phycom\common\models\AttachmentUrl;
use phycom\common\models\attributes\CategoryStatus;
use phycom\common\models\attributes\ProductVariantStatus;
use phycom\common\models\attributes\ProductStatus;
use phycom\common\models\behaviors\CurrencyBehavior;
use phycom\common\components\ActiveQuery;
use phycom\common\models\product\Variant;
use phycom\common\models\product\ProductVariant;
use phycom\common\models\product\ProductCategory;
use phycom\common\models\product\ProductCategoryProductRelation;
use phycom\common\models\product\ProductTag;
use phycom\common\models\traits\SearchQueryFilter;
use phycom\common\interfaces\SearchModelInterface;
use phycom\common\models\File;
use phycom\common\models\Language;
use phycom\common\models\product\Product;
use phycom\common\models\product\ProductAttachment;
use phycom\common\models\product\ProductPrice;
use phycom\common\models\translation\ProductCategoryTranslation;
use phycom\common\models\translation\ProductTranslation;

use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use yii\db\Expression;
use yii;


/**
 * Class SearchProduct
 * @package phycom\backend\models\product
 *
 * @property Language $language
 * @property array $tags
 */
class SearchProduct extends Product implements SearchModelInterface
{
	use SearchQueryFilter;

	public $language;

	public $title;
    public $description;

	public $title1;
    public $description1;

    public $title2;
    public $description2;

	public $fileName;
	public $fileBucket;

	public $price;
	public $discountAmount;

	public $updatedFrom;
	public $updatedTo;

	public $createdFrom;
	public $createdTo;

	public $categoryData;
	public $categoryIds;

    private $tags = [];
    private $tagsValidation = false;

//	public $productAttributes;

    public function init()
    {
        parent::init();

        if (!$this->language) {
            $this->language = Yii::$app->lang->current;
        }
    }

    public function rules()
	{
		return [
			[['id', 'vendor_id', 'created_by', 'image_id', 'price_id'], 'integer'],
			[['include_vat'], 'boolean'],
			[['title', 'description', 'title1', 'description1', 'title2', 'description2'], 'string'],
			[['created_at','updated_at','createdFrom','createdTo','updatedFrom','updatedTo', 'status'], 'safe'],
			[['sku'], 'string', 'max' => 255],
            [['categoryIds'], 'each', 'rule' => ['integer'], 'when' => function () {
		        return is_array($this->categoryIds);
            }],
            [['categoryIds'], 'integer', 'when' => function () {
		        return !is_array($this->categoryIds);
            }],
            [['tags'], 'each', 'rule' => ['string']],
			[['price'], 'number']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'currency' => [
				'class' => CurrencyBehavior::class,
				'attributes' => ['price', 'discountAmount']
			]
		]);
	}

	public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'title'        => Yii::t('backend/product', 'Title'),
            'description'  => Yii::t('backend/product', 'Description'),
            'title1'       => Yii::t('backend/product', 'Title {lang}', ['lang' => $this->language->code]),
            'description1' => Yii::t('backend/product', 'Description {lang}', ['lang' => $this->language->code]),
            'title2'       => Yii::t('backend/product', 'Title {lang}', ['lang' => Yii::$app->lang->source->code]),
            'description2' => Yii::t('backend/product', 'Description {lang}', ['lang' => Yii::$app->lang->source->code]),
            'price'        => Yii::t('backend/product', 'Price'),
            'categoryIds'  => Yii::t('backend/product', 'Select Category'),
        ]);
    }

    public function setTags($value)
    {
        // this is for "each" validator as it internally sets tags value to string and then validates the value
        if ($this->tagsValidation) {
            $this->tags = $value;
            return;
        }

        if (empty($value)) {
            $this->tags = [];
            return;
        }
        if (is_array($value)) {
            $this->tags = $value;
            return;
        }
        if (is_string($value)) {
            $jsonData = json_decode($value, true);
            // JSON is valid and we have array of tags
            if (json_last_error() === JSON_ERROR_NONE && is_array($jsonData)) {
                $this->tags = $jsonData;
            } else {
                $this->tags = [$value];
            }
        }
    }

    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param string|null $metaKey
     * @param string|null $metaValue
     * @return AttachmentUrl|object
     * @throws yii\base\InvalidConfigException
     */
    public function getImageUrl($metaKey = null, $metaValue = null)
	{
        return Yii::createObject([
            'class'    => AttachmentUrl::class,
            'bucket'   => $this->fileBucket,
            'filename' => $this->fileName,
        ]);
	}

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     * @throws yii\base\Exception
     */
	public function search(array $params = [])
	{
        $this->load($params);

		$query = $this->createSearchQuery();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
			'pagination' => [
				'pageSize' => 20
			],
		]);

		$this->sort($dataProvider->sort);

		if (!$this->validate()) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'p.id' => $this->id,
			'price.price' => $this->price,
			'price.discount_amount' => $this->discountAmount,
			'p.status' => (string)$this->status,
		]);

        $query->filterText('p.sku', $this->sku);
		$query->filterMultiAttribute(['t1.title','t2.title'], $this->title);
		$query->filterDateRange('p.created_at', $this->createdFrom, $this->createdTo);
		$query->filterDateRange('p.updated_at', $this->updatedFrom, $this->updatedTo);

		return $dataProvider;
	}

    /**
     * @return array
     */
	public function getExporterColumns()
    {
        return [
            [
                'attribute' => 'sku',
                'contentOptions' => ['alignment' => ['horizontal' => 'left', 'vertical' => 'center']],
                'dimensionOptions' => ['autoSize' => true]
            ],
            [
                'attribute' => 'title',
                'contentOptions' => ['alignment' => ['horizontal' => 'left', 'vertical' => 'center']],
                'dimensionOptions' => ['autoSize' => true]
            ],
            [
                'attribute' => 'description',
                'value'     => function ($model) {
                    return html_entity_decode(strip_tags($model->description));
                },
                'contentOptions' => ['alignment' => ['horizontal' => 'left', 'vertical' => 'center']],
                'dimensionOptions' => ['autoSize' => true]
            ],
            [
                'attribute' => 'price',
                'format'    => 'currencyValue',
                'contentOptions' => ['alignment' => ['horizontal' => 'left', 'vertical' => 'center']],
                'dimensionOptions' => ['autoSize' => true]
            ],
            [
                'attribute' => 'categories',
                'format'    => 'text',
                'value'     => function ($model) {
                    $items = [];
                    /**
                     * @var static $model
                     */
                    if (!empty($model->categoryData)) {
                        foreach (yii\helpers\Json::decode($model->categoryData) as $id => list($title, $status)) {
                            if (!empty($title)) {
                                $items[] = $title;
                            }
                        }
                    }
                    return implode(',', $items);
                },
                'contentOptions' => ['alignment' => ['horizontal' => 'left', 'vertical' => 'center']],
                'dimensionOptions' => ['autoSize' => true]
            ],
            [
                'attribute' => 'status',
                'format'    => 'text',
                'contentOptions' => ['alignment' => ['horizontal' => 'left', 'vertical' => 'center']],
                'dimensionOptions' => ['autoSize' => true]
            ],
            [
                'attribute' => 'created_at',
                'format'    => 'date',
                'contentOptions' => ['alignment' => ['horizontal' => 'left', 'vertical' => 'center']],
                'dimensionOptions' => ['autoSize' => true]
            ]
        ];
    }



	protected function sort(Sort $sort)
	{
		$sort->attributes['title'] = [
			'asc' => [new Expression('COALESCE(t1.title, t2.title) ASC')],
			'desc' => [new Expression('COALESCE(t1.title, t2.title) DESC')],
		];
        $sort->attributes['title1'] = [
            'asc' => ['t1.title' => SORT_ASC],
            'desc' => ['t1.title' => SORT_DESC],
        ];
        $sort->attributes['title2'] = [
            'asc' => ['t2.title' => SORT_ASC],
            'desc' => ['t2.title' => SORT_DESC],
        ];
		$sort->attributes['price'] = [
			'asc' => ['price.price' => SORT_ASC],
			'desc' => ['price.price' => SORT_DESC],
		];
		$sort->attributes['discountAmount'] = [
			'asc' => ['price.discount_amount' => SORT_ASC],
			'desc' => ['price.discount_amount' => SORT_DESC],
		];
	}

    /**
     * @return ActiveQuery
     * @throws yii\base\Exception
     */
	protected function createSearchQuery()
	{
		$query = static::find();
		$query->select([
			'p.*',
            new Expression('CASE WHEN (t1.title IS NOT NULL) THEN t1.title ELSE t2.title END AS title'),
            new Expression('CASE WHEN (t1.description IS NOT NULL) THEN t1.description ELSE t2.description END AS description'),
			't1.title AS title1',
			't2.title AS title2',
			't1.description AS description1',
			't2.description AS description2',
			'price.price AS price',
			'price.discount_amount AS discountAmount',
			'file.filename AS fileName',
			'file.bucket AS fileBucket',
            'category.data AS categoryData',
            'all_tags.data AS tags'
//            'att.data AS "productAttributes"'
		]);
		$query->from(['p' => Product::tableName()]);
		$query->where(['not', ['p.status' => ProductStatus::DELETED]]);

		$query->leftJoin(['t1' => ProductTranslation::tableName()], 't1.product_id = p.id AND t1.language = :lang');
		$query->leftJoin(['t2' => ProductTranslation::tableName()], 't2.product_id = p.id AND t2.language = :fallback_lang AND t2.language != :lang');
        $query->addParams([
            'lang'          => $this->language->code,
            'fallback_lang' => Yii::$app->lang->findFallbackLanguage($this->language)->code
        ]);

		$this->filterByCategories($query);

        if (!empty($this->tags) && $this->validate('tags')) {
            $this->filterByTags($query);
        }

        $query->leftJoin(
            ['all_tags' => 'LATERAL(' .
                (new Query())
                    ->select(new Expression('jsonb_agg(pt.value) as data'))
                    ->from(['pt' => ProductTag::tableName()])
                    ->where('pt.product_id = p.id')
                    ->createCommand()->rawSql . ')'],
            'true'
        );

//        $this->filterByTags($query);
//        $this->filterByBooleanAttributes($query);

		$query->leftJoin(
			['price' => 'LATERAL(' .
				(new Query())
				->select('pp.*')
				->from(['pp' => ProductPrice::tableName()])
				->where('pp.product_id = p.id')
				->orderBy(['pp.num_units' => SORT_ASC])
				->limit(1)
				->createCommand()
				->sql . ')'],
			'true'
		);

		$query->leftJoin(
			['file' => 'LATERAL(' .
				(new Query())
					->select('f.*')
					->from(['pa' => ProductAttachment::tableName()])
					->innerJoin(['f' => File::tableName()], 'f.id = pa.file_id')
					->where('pa.product_id = p.id')
					->orderBy(['pa.order' => SORT_ASC])
					->limit(1)
					->createCommand()
					->sql . ')'],
			'true'
		);

		return $query;
	}

	protected function filterByCategories(ActiveQuery $query)
    {
        $joinCondition = [
            'and',
            'pc.category_id = c.id',
            'pc.product_id = p.id'
        ];

        if (!empty($this->categoryIds) && $this->validate('categoryIds')) {
            $query->andWhere('category.data is NOT NULL');
            $joinCondition[] = ['pc.category_id' => $this->categoryIds];
        }

        $sql = (new Query())
            ->select(new Expression('json_object_agg(c.id, array[CASE WHEN (ct1.title IS NOT NULL) THEN ct1.title ELSE ct2.title END, "c"."status"]) as data'))
            ->from(['c' => ProductCategory::tableName()])
            ->innerJoin(['pc' => ProductCategoryProductRelation::tableName()], $joinCondition)
            ->leftJoin(['ct1' => ProductCategoryTranslation::tableName()], 'ct1.product_category_id = c.id AND ct1.language = :lang')
            ->leftJoin(['ct2' => ProductCategoryTranslation::tableName()], 'ct2.product_category_id = c.id AND ct2.language = :fallback_lang AND ct2.language != :lang')
            ->where(['not', ['c.status' => CategoryStatus::DELETED]])
            ->addParams([
                'lang' => $this->language->code,
                'fallback_lang' => Yii::$app->lang->findFallbackLanguage($this->language)->code
            ])
            ->createCommand()
            ->rawSql;

        $query->leftJoin(['category' => 'LATERAL(' . $sql . ')'], 'true');
    }

//    protected function filterByBooleanAttributes(ActiveQuery $query)
//    {
//        $attributes = Yii::$app->modelFactory->getAttribute()::findAll();
//        $booleanAttributes = [];
//        $booleanAttributeNames = [];
//        foreach ($attributes as $attribute) {
//            if ($attribute->type === Attribute::TYPE_BOOL) {
//                $booleanAttributes[] = $attribute;
//                $booleanAttributeNames[] = $attribute->name;
//            }
//        }
//
//        $q = (new Query())
//            ->select('json_object_agg(at.attribute, array["at"."value", "at"."status"]) as data')
//            ->from(['at' => ProductAttribute::tableName()])
//            ->where('at.product_id = p.id')
//            ->andWhere(['at.attribute' => $booleanAttributeNames]);
//
//        if (!empty($this->productAttributes) && $this->validate('productAttributes')) {
//            $query->andWhere('att.data is NOT NULL');
//            $q->andWhere(['at.attribute' => $this->productAttributes]);
//            $q->andWhere('at.value = \'1\'');
//        }
//
//        $sql = $q->createCommand()->rawSql;
//        $query->leftJoin(['att' => 'LATERAL(' . $sql . ')'], 'true');
//    }

//    protected function filterByTags(ActiveQuery $query)
//    {
//        $q = (new Query())
//            ->select('json_agg(pt.value) as data')
//            ->from(['pt' => ProductTag::tableName()])
//            ->where('pt.product_id = p.id');
//
//        if (!empty($this->tags) && $this->validate('tags')) {
//            $query->andWhere('tag.data is NOT NULL');
//            $q->andWhere(['pt.value' => $this->tags]);
//        }
//        $query->leftJoin(['tag' => 'LATERAL(' . $q->createCommand()->rawSql . ')'], 'true');
//    }


    /**
     * @param ActiveQuery $query
     */
    protected function filterByTags(ActiveQuery $query)
    {
        foreach ($this->tags as $key => $tag ) {
            $paramKey = 'tag' . $key;
            $alias1 = 'ptf' . $key;
            $alias2 = 'tags' . $key . '_filter';
            $q = (new Query())
                ->select($alias1 . '.*')
                ->from([$alias1 => ProductTag::tableName()])
                ->where($alias1 . '.product_id = p.id')
                ->andWhere($alias1 . '.value = :' . $paramKey)
                ->limit(1);

            $query->innerJoin([$alias2 => 'LATERAL(' . $q->createCommand()->rawSql . ')'], 'true', [$paramKey => $tag]);
        }
    }

    /**
     * Added support for tags validation with "each" validator
     *
     * @param null $attributeNames
     * @param bool $clearErrors
     * @return bool
     */
    public function validate($attributeNames = null, $clearErrors = true)
    {
        if (null === $attributeNames || $attributeNames === 'tags' || (is_array($attributeNames) && in_array('tags', $attributeNames))) {
            $this->tagsValidation = true;
        }
        $result = parent::validate($attributeNames, $clearErrors);
        $this->tagsValidation = false;
        return $result;
    }

}
