<?php

namespace phycom\backend\models\product;


use phycom\common\models\product\ProductVariant;
use phycom\common\models\product\ProductVariantOption;
use phycom\common\models\product\Product;
use phycom\common\models\User;

use yii\db\ActiveRecordInterface;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class ProductVariantForm
 * @package phycom\backend\models\product
 *
 * @property ProductVariant[] $models
 * @property ProductVariantOption[] $options
 */
class ProductVariantForm extends ProductModelCollectionForm
{
    /**
     * @var User
     */
	protected $user;

    /**
     * @var array
     */
	private $options;

    /**
     * @var array
     */
	private $defaultOption;

	/**
	 * ProductVariantForm constructor.
     *
	 * @param Product $product
     * @param User $user
	 * @param array $config
	 */
	public function __construct(Product $product, User $user = null, array $config = [])
	{
        $this->user = $user;
        parent::__construct($product, $config);
	}

	public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_POPULATE_MODELS, [$this, 'loadSavedOptions']);
        $this->on(self::EVENT_AFTER_SAVE, [$this, 'resetOptions']);
    }

    /**
     * @return string|ProductVariant
     */
    public function getModelClassName()
    {
        return ProductVariant::class;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['models', 'options', 'defaultOption'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'defaultOption' => Yii::t('common/variant', 'Default')
        ];
    }

    /**
     * @param array $data
     */
    public function setDefaultOption(array $data)
    {
        $result = [];
        foreach ($data as $modelKey => $optionKey) {
            /**
             * @var ProductVariant $model
             */
            if (!$model = $this->getModelByKey($modelKey)) {
                continue;
            }
            $result[$modelKey] = $optionKey;
        }
        $this->defaultOption = $result;
    }

    /**
     * @param mixed|null $key
     * @return array|mixed|null
     */
    public function getDefaultOption($key = null)
    {
        if (null !== $key) {
            return $this->defaultOption[$key] ?? null;
        }
        return $this->defaultOption;
    }

    /**
     * @param array $options
     */
    public function setOptions(array $options)
    {
        $result = [];
        foreach ($options as $modelKey => $modelOptions) {
            /**
             * @var ProductVariant $model
             */
            if (!$model = $this->getModelByKey($modelKey)) {
                continue;
            }
            foreach ($modelOptions as $value) {
                if (!isset($value['key'])) {
                    continue;
                }
                if (!isset($result[$modelKey])) {
                    $result[$modelKey] = [];
                }
                if (is_array($value)) {

                    $productVariantOption = null;
                    if (!$model->isNewRecord) {
                        $productVariantOption = ProductVariantOption::findOne([
                            'product_variant_id' => $model->id,
                            'key'                => $value['key']
                        ]);
                    }
                    if (!$productVariantOption) {
                        $productVariantOption = new ProductVariantOption();
                    }
                    $productVariantOption->setAttributes($value);
                    $result[$modelKey][] = $productVariantOption;

                } elseif ($value instanceof ProductVariantOption) {
                    $result[$modelKey][] = $value;
                }
            }
        }
        $this->options = $result;
    }

    /**
     * @return ProductVariantOption[]
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param mixed $modelKey
     * @return array|ProductVariantOption[]
     */
    public function getModelOptions($modelKey)
    {
        $this->getModels();
        return $this->options[$modelKey] ?? [];
    }

    /**
     * @param mixed $modelKey
     * @return array|ProductVariantOption[]
     * @throws \yii\base\InvalidConfigException
     */
    public function getMergedModelOptions($modelKey)
    {
        $this->getModels();
        $options = $this->options[$modelKey] ?? [];
        /**
         * @var ProductVariant $model
         * @var ProductVariantOption[] $options
         */
        if ($model = $this->getModelByKey($modelKey)) {
            $out = [];
            $optionKeys = ArrayHelper::getColumn($options, 'key');
            foreach ($model->getAllOptions() as $variantOption) {

                if (!in_array($variantOption->key, $optionKeys)) {
                    $option = new ProductVariantOption(['key' => $variantOption->key]);
                    $option->populateRelation('productVariant', $model);
                    $out[] = $option;
                } else {
                    foreach ($options as $option) {
                        if ($option->key === (string)$variantOption->key) {
                            $option->populateRelation('productVariant', $model);
                            $out[] = $option;
                            break;
                        }
                    }
                }
            }
            return $out;
        }
        return $options;
    }

    /**
     * Called after we have successfully saved all our models and options
     */
    public function resetOptions()
    {
        $this->options = null;
        $this->defaultOption = null;
    }

    /**
     * @param array|null $modelAttributes
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function exportFormAttributes(array $modelAttributes = null)
    {
        $result = [];
        $formName = $this->formName();
        $index = 0;
        foreach ($this->getModels() as $modelKey => $model) {
            foreach ($model->getAttributes() as $attribute => $value) {
                if (null === $modelAttributes || in_array($attribute, $modelAttributes)) {
                    $result[$formName . "[models][$index][$attribute]"] = (string) $value;
                }
            }
            foreach ($this->getModelOptions($modelKey) as $optionKey => $option) {
                $result[$formName . "[options][$index][$optionKey][key]"] = $option->key;
                if (!empty($option->sku)) {
                    $result[$formName . "[options][$index][$optionKey][sku]"] = $option->sku;
                }
                $result[$formName . "[options][$index][$optionKey][is_visible]"] = $option->is_visible ? '1' : '0';
            }
            if ($defaultOptionKey = $this->getDefaultOption($modelKey)) {
                $result[$formName . "[defaultOption][$index]"] = $defaultOptionKey;
            }
            $index++;
        }
        return $result;
    }

    /**
     * Called after all models have been populated
     */
    protected function loadSavedOptions()
    {
        foreach ($this->models as $modelKey => $model) {
            if (!$model->isNewRecord) {
                /**
                 * @var ProductVariantOption[] $options
                 */
                $options = ProductVariantOption::find()->where(['product_variant_id' => $model->id])->all();
                if (!empty($options)) {
                    $this->options[$modelKey] = $options;
                }
                // populate saved default options
                if ($model->default_option_id) {
                    foreach ($options as $option) {
                        if ($option->id === $model->default_option_id) {
                            $this->defaultOption[$modelKey] = $option->key;
                            break;
                        }
                    }
                }
            }
        }
    }

    /**
     * @return array|ProductVariant[]
     */
    protected function loadSavedModels()
    {
        return $this->product->isNewRecord ? [] : ArrayHelper::index($this->product->productVariants, 'id');
    }


    /**
     * @param ActiveRecordInterface|ProductVariant $model
     * @param mixed $key
     * @return bool
     */
    protected function beforeSaveModel(ActiveRecordInterface $model, $key)
    {
        if ($model->isNewRecord && $existingModel = $this->findModelByCondition(['name' => $model->name, 'product_id' => $this->product->id])) {
            $existingModel->delete();
        }
        if ($this->user) {
            if ($model->isNewRecord) {
                $model->created_by = $this->user->id;
            }
            $model->updated_by = $this->user->id;
        }
        return parent::beforeSaveModel($model, $key);
    }

    /**
     * @param ActiveRecordInterface|ProductVariant $model
     * @param mixed $key
     * @return bool
     * @throws \Throwable
     */
    protected function afterSaveModel(ActiveRecordInterface $model, $key)
    {
        return $this->saveModelOptions($model, $key)
            && $this->saveDefaultOption($model, $key)
            && parent::afterSaveModel($model, $key);
    }

    /**
     * @param ActiveRecordInterface $model
     * @param mixed $key
     * @return bool
     */
    private function saveModelOptions(ActiveRecordInterface $model, $key)
    {
        $options = $this->getModelOptions($key);
        foreach ($options as $option) {
            if ($existingOption = ProductVariantOption::findOne(['product_variant_id' => $model->id, 'key' => $option->key])) {
                $existingOption->setAttributes($option->attributes);
                $option = $existingOption;
            } else {
                $option->product_variant_id = $model->id;
            }
            if (!$option->save()) {
                $this->setErrors($option->errors, 'options');
                return false;
            }
        }
        return true;
    }

    /**
     * @param ActiveRecordInterface $model
     * @param mixed $key
     * @return bool|int
     */
    private function saveDefaultOption(ActiveRecordInterface $model, $key)
    {
        if (isset($this->defaultOption[$key])) {
            foreach ($this->getModelOptions($key) as $option) {
                if ($option->key == $this->defaultOption[$key] && !$option->isNewRecord) {

                    if ($option->is_visible) {
                        $optionId = $option->id;
                    } elseif ($visibleOption = ProductVariantOption::findOne(['product_variant_id' => $model->id, 'is_visible' => true])) {
                        $optionId = $visibleOption->id;
                    } else {
                        $optionId = null;
                    }
                    if ($optionId !== $model->default_option_id) {
                        $model->default_option_id = $optionId;
                        return $model->update(true, ['default_option_id']);
                    }
                    break;
                }
            }
        }
        return true;
    }
}
