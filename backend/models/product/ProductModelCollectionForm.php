<?php

namespace phycom\backend\models\product;

use phycom\backend\models\ModelCollectionForm;
use phycom\common\events\ModelUpdateEvent;
use phycom\common\models\product\Product;

use yii\db\ActiveRecordInterface;

/**
 * Class ProductModelCollectionForm
 *
 * @package phycom\backend\models\product
 *
 * @property-read Product $product
 */
abstract class ProductModelCollectionForm extends ModelCollectionForm
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * ProductModelCollectionForm constructor.
     *
     * @param Product $product
     * @param array $config
     */
    public function __construct(Product $product,  array $config = [])
    {
        $this->product = $product;
        parent::__construct($config);
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return array
     */
    protected function getRelationMap()
    {
        return ['product_id' => $this->product->id];
    }

    /**
     * @param ActiveRecordInterface $model
     */
    protected function afterCreateModel(ActiveRecordInterface $model)
    {
        if (!$this->product->isNewRecord) {
            $model->product_id = $this->product->id;
            $model->populateRelation('product', $this->product);
        }
        parent::afterCreateModel($model);
    }
}
