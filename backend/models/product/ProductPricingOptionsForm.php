<?php

namespace phycom\backend\models\product;

use phycom\common\models\attributes\PriceUnitMode;
use phycom\common\models\traits\ModelTrait;
use phycom\common\models\attributes\UnitType;
use phycom\common\models\product\Product;

use yii\base\Model;
use yii;

/**
 * Class ProductPricingOptionsForm
 * @package phycom\backend\models\product
 *
 * @property-read Product $product
 */
class ProductPricingOptionsForm extends Model
{
	use ModelTrait;

	public $priceUnit;
	public $priceUnitMode;
	public $discount;

	/**
	 * @var Product
	 */
	protected $product;
	/**
	 * ProductForm constructor.
	 *
	 * @param Product|null $product
	 * @param array $config
	 */
	public function __construct(Product $product = null, array $config = [])
	{
		$this->product = $product ?? Yii::$app->modelFactory->getProduct();
		parent::__construct($config);
	}

	public function rules()
	{
		return [
			[['priceUnit', 'priceUnitMode'], 'required'],
            [['priceUnit', 'priceUnitMode'], 'string', 'max' => 255],
			['priceUnit', 'in', 'range' => UnitType::all()],
            ['priceUnitMode', 'in', 'range' => PriceUnitMode::all()],
            ['discount', 'boolean']
		];
	}

	public function attributeLabels()
	{
		return [
			'priceUnit' => $this->product->getAttributeLabel('price_unit'),
			'priceUnitMode' => $this->product->getAttributeLabel('price_unit_mode'),
            'discount' => $this->product->getAttributeLabel('discount'),
		];
	}

	public function init()
	{
		parent::init();

		if ($this->product->isNewRecord) {
			$this->product->loadDefaultValues();
		}
		$this->priceUnit = $this->product->price_unit;
		$this->priceUnitMode = $this->product->price_unit_mode;
		$this->discount = $this->product->discount ? 1 : 0;
	}

	public function getProduct()
	{
		return $this->product;
	}

    /**
     * @param array|null $attributes
     * @return array
     * @throws yii\base\InvalidConfigException
     */
    public function exportFormAttributes(array $attributes = null)
    {
        $result = [];
        $formName = $this->formName();
        foreach ($this->getAttributes($attributes) as $attribute => $value) {
            $result[$formName . "[$attribute]"] = (string) $value;
        }
        return $result;
    }


	public function update()
	{
		if ($this->validate()) {

			$transaction = Yii::$app->db->beginTransaction();
			try {

				$this->product->price_unit = new UnitType($this->priceUnit);
				$this->product->price_unit_mode = new PriceUnitMode($this->priceUnitMode);
                $this->product->discount = (bool)$this->discount;

				if (!$this->product->save()) {
					return $this->rollback($transaction, $this->product->errors);
				}

				$transaction->commit();
				return true;

			} catch (\Exception $e) {
				$transaction->rollBack();
				throw $e;
			}
		}
		return false;
	}

}
