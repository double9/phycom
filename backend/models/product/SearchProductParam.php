<?php

namespace phycom\backend\models\product;

use phycom\common\components\ActiveQuery;
use phycom\common\models\product\ProductParam;
use phycom\common\models\traits\SearchQueryFilter;
use phycom\common\interfaces\SearchModelInterface;

use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii;

/**
 * Class SearchProductParam
 * @package phycom\backend\models\product
 */
class SearchProductParam extends ProductParam implements SearchModelInterface
{
	use SearchQueryFilter;

	/**
	 * Creates data provider instance with search query applied
	 * @param array $params
	 * @return ActiveDataProvider
	 */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => ['defaultOrder' => ['created_at' => SORT_ASC]],
            'pagination' => ['pageSize' => 40],
        ]);

		$this->sort($dataProvider->sort);
		return $dataProvider;
	}

	protected function sort(Sort $sort)
	{

	}

	/**
	 * @return ActiveQuery
	 */
	protected function createSearchQuery()
	{
		$query = static::find();
		$query->select(['pp.*']);
		$query->from(['pp' => static::tableName()]);
        $query->andWhere(['pp.product_id' => $this->product_id]);

		return $query;
	}
}
