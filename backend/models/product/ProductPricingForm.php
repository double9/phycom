<?php

namespace phycom\backend\models\product;


use phycom\common\helpers\Currency;

use yii\db\ActiveRecordInterface;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class ProductPricingForm
 *
 * @package phycom\backend\models\product
 *
 * @property ProductPrice[] $models
 * @method ProductPrice createModel(array $attributes = [])
 */
class ProductPricingForm extends ProductModelCollectionForm
{
    /**
     * @var array
     */
    protected $currencyAttributes;


    public function init()
    {
        parent::init();
        $this->currencyAttributes = $this->getCurrencyAttributes();
    }

    /**
     * @return string|ProductPrice
     */
    public function getModelClassName()
    {
        return get_class(Yii::$app->modelFactory->getProductPrice());
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    protected function getCurrencyAttributes()
    {
        return ArrayHelper::merge(
            ['price', 'discount_amount'],
            $this->createModel()->populatePriceVariations()->getPriceVariationAttributes()
        );
    }

    /**
     * Exports single Model attribute
     *
     * @param ActiveRecordInterface|ProductPrice $model
     * @param string $attribute
     * @return float|mixed
     */
    protected function exportModelAttribute(ActiveRecordInterface $model, $attribute)
    {
        if (null !== $model->$attribute && in_array($attribute, $this->currencyAttributes)) {
            return Currency::toDecimal($model->$attribute);
        }
        return parent::exportModelAttribute($model, $attribute);
    }

    /**
     * @return array|ProductPrice[]
     */
    protected function loadSavedModels()
    {
        return $this->product->isNewRecord ? [] : $this->product->prices;
    }

    /**
     * @param ActiveRecordInterface|ProductPrice $model
     * @param mixed $key
     * @return bool
     */
    protected function beforeSaveModel(ActiveRecordInterface $model, $key)
    {
        if ($model->isNewRecord
            && $existingModel = $this->findModelByCondition([
                'num_units'  => $model->num_units,
                'unit_type'  => $model->unit_type,
                'product_id' => $this->product->id
            ])
        ) {
            $existingModel->delete();
        }
        $model->unit_type = $this->product->price_unit;
        return parent::beforeSaveModel($model, $key);
    }

    /**
     * @param ActiveRecordInterface|ProductPrice $model
     * @param mixed $key
     * @return bool
     * @throws Yii\base\Exception
     * @throws \Throwable
     */
    protected function afterSaveModel(ActiveRecordInterface $model, $key)
    {
        return $model->savePriceVariations() && parent::afterSaveModel($model, $key);
    }

    /**
     * @param ActiveRecordInterface|ProductPrice $model
     * @param string $attribute
     */
    protected function initModelAttribute(ActiveRecordInterface $model, $attribute)
    {
        if (null !== $model->$attribute && in_array($attribute, $this->currencyAttributes)) {
            $model->$attribute = Currency::toInteger($model->$attribute);
        }
    }

    /**
     * @param ActiveRecordInterface $model
     * @throws \yii\base\InvalidConfigException
     */
    protected function afterCreateModel(ActiveRecordInterface $model)
    {
        parent::afterCreateModel($model);
        /**
         * @var ProductPrice $model
         */
        $model->populatePriceVariations();
    }

}
