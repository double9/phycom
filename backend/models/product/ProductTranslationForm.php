<?php

namespace phycom\backend\models\product;

use phycom\common\models\traits\ModelTrait;
use phycom\common\models\product\Product;
use phycom\common\models\translation\ProductTranslation;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii;

/**
 * Class ProductTranslationForm
 * @package phycom\backend\models\product
 *
 * @property-read Product $product
 * @property ProductTranslation[] $translations
 * @property ProductTranslation[] $changedTranslations
 */
class ProductTranslationForm extends Model
{
	use ModelTrait;

	/**
	 * @var ProductTranslation[]
	 */
	private $translations;
	/**
	 * @var Product
	 */
	protected $product;

	/**
	 * ProductPricingForm constructor.
	 * @param Product $product
	 * @param array $config
	 */
	public function __construct(Product $product, array $config = [])
	{
		$this->product = $product;
		parent::__construct($config);
	}

	public function rules()
	{
		return [
			['translations', 'safe']
		];
	}

	public function getProduct()
    {
        return $this->product;
    }

    public function afterValidate()
	{
		$translations = $this->getChangedTranslations();
		if (empty($translations)) {
			$this->addError('translations', Yii::t('backend/error', 'Empty product content. At least one language should not be empty'));
		} else if (!Model::validateMultiple($translations)) {
			$this->addError(null); // add an empty error to prevent saving
		}
		parent::afterValidate();
	}

	public function save()
	{
		if (!$this->validate()) {
			return false;
		}
		$transaction = Yii::$app->db->beginTransaction();
		if (!$this->saveTranslations()) {
			$transaction->rollBack();
			return false;
		}
		$transaction->commit();
		return true;
	}

	public function saveTranslations()
	{
		$keep = [];
		foreach ($this->getChangedTranslations() as $translation) {
			$translation->product_id = $this->product->id;
			if (!$translation->save(false)) {
				return false;
			}
			$keep[] = $translation->id;
		}
		$query = ProductTranslation::find()->andWhere(['product_id' => $this->product->id]);
		if ($keep) {
			$query->andWhere(['not in', 'id', $keep]);
		}
		foreach ($query->all() as $translation) {
			$translation->delete();
		}
		return true;
	}

	public function getChangedTranslations()
	{
		$changed = [];
		foreach ($this->getTranslations() as $translation) {
			if ($translation->title || $translation->description || $translation->outline || !$translation->isNewRecord) {
				$changed[] = $translation;
			}
		}
		return $changed;
	}

    /**
     * @param array|null $modelAttributes
     * @return array
     * @throws yii\base\InvalidConfigException
     */
	public function exportFormAttributes(array $modelAttributes = null)
    {
        $result = [];
        $formName = $this->formName();
        foreach ($this->getTranslations() as $language => $translation) {
            foreach ($translation->getAttributes() as $attribute => $value) {
                if (null === $modelAttributes || in_array($attribute, $modelAttributes)) {
                    $result[$formName . "[translations][$language][$attribute]"] = (string) $value;
                }
            }
        }
        return $result;
    }

	public function getTranslations()
	{
		if ($this->translations === null) {
			$translations = $this->product->isNewRecord ? [] : ArrayHelper::index($this->product->translations, 'language');
			foreach (Yii::$app->lang->enabled as $language) {
				if (!isset($translations[$language->code])) {
					$translation = new ProductTranslation();
					$translation->language = $language->code;
					if (!$this->product->isNewRecord) {
						$translation->product_id = $this->product->id;
						$translation->populateRelation('product', $this->product);
					}
					$translations[$language->code] = $translation;
				}
			}
			$translationsSorted = [];
			foreach (Yii::$app->lang->enabled as $language) {
				$translationsSorted[$language->code] = $translations[$language->code];
			};
			$this->translations = $translationsSorted;
		}
		return $this->translations;
	}

	public function setTranslations(array $translations)
	{
		$this->translations = [];
		foreach ($translations as $language => $translation) {
			if (is_array($translation)) {
				$this->translations[$language] = $this->getTranslation($language);
				$this->translations[$language]->setAttributes($translation);
			} elseif ($translation instanceof ProductTranslation) {

				if ($translation->product_id && $translation->product_id != $this->product->id) {
					throw new yii\base\InvalidValueException('Invalid product translation ' . $translation->id);
				}
				$this->translations[$translation->language] = $translation;
			}
		}
	}

//	public function errorSummary($form)
//	{
//		$errorLists = [];
//		foreach ($this->getAllModels() as $id => $model) {
//			$errorList = $form->errorSummary($model, [
//				'header' => '<p>Please fix the following errors for <b>' . $id . '</b></p>',
//			]);
//			$errorList = str_replace('<li></li>', '', $errorList); // remove the empty error
//			$errorLists[] = $errorList;
//		}
//		return implode('', $errorLists);
//	}

	private function getTranslation($language)
	{
		$translation = $language && !$this->product->isNewRecord
			? ProductTranslation::findOne(['language' => $language, 'product_id' => $this->product->id])
			: false;

		if (!$translation) {
			$translation = new ProductTranslation();
			$translation->loadDefaultValues();
			$translation->language = $language;

			if (!$this->product->isNewRecord) {
				$translation->product_id = $this->product->id;
				$translation->populateRelation('product', $this->product);
			}
		}
		return $translation;
	}

}
