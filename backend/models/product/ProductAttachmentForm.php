<?php

namespace phycom\backend\models\product;

use phycom\common\components\FileStorage;
use phycom\common\models\AttachmentUrl;
use phycom\common\models\traits\ModelTrait;
use phycom\common\models\attributes\FileStatus;
use phycom\common\models\attributes\FileType;
use phycom\common\models\File;
use phycom\common\models\product\Product;
use phycom\common\models\product\ProductAttachment;

use yii\base\Model;
use yii;

/**
 * Class ProductAttachmentForm
 * @package phycom\backend\models\product
 *
 * @property ProductAttachment[] $attachments
 *
 * @property-read Product $product
 * @property-read array $allowedExtensions
 * @property-read int $maxFileSize
 * @property-read int $maxFiles
 * @property-read array $productFiles
 */
class ProductAttachmentForm extends Model
{
	use ModelTrait;
	/**
	 * @var yii\web\UploadedFile
	 */
	public $file;

	public $params;

	protected $product;

	private $attachments;

	public function __construct(Product $product, array $config = [])
	{
		$this->product = $product;
		parent::__construct($config);
	}

	public function rules()
	{
		return [
			[['file'],
				'file',
				'skipOnEmpty' => false,
				'extensions' => $this->allowedExtensions,
				'maxSize' => $this->maxFileSize,
				'maxFiles' => 1,
				'enableClientValidation' => false
			],
            ['attachments', 'safe']
		];
	}

	public function attributeLabels()
	{
		return [
			'file' => Yii::t('backend/main','Product attachments')
		];
	}

	public function getProduct()
	{
		return $this->product;
	}

	public function getAllowedExtensions()
	{
		return ['png', 'jpg', 'jpeg', 'gif', 'pdf'];
	}

	public function getMaxFiles()
	{
		return 10;
	}

	public function getMaxFileSize()
	{
		return 8*1024*1024;
	}

	public function getProductFiles()
	{
		return yii\helpers\ArrayHelper::getColumn($this->product->attachments, 'url');
	}

    /**
     * @return AttachmentUrl|object
     * @throws yii\base\InvalidConfigException
     */
	public function getImageUrl()
    {
        $attachment = $this->getAttachments()[0] ?? null;

        return Yii::createObject([
            'class'    => AttachmentUrl::class,
            'bucket'   => $attachment ? $attachment->file->bucket : null,
            'filename' => $attachment ? $attachment->file->filename : null,
        ]);
    }

    /**
     * @return array|ProductAttachment[]
     */
    public function getAttachments()
    {
        if (null === $this->attachments) {
            $this->attachments = $this->product->isNewRecord ? [] : $this->product->attachments;
        }
        return $this->attachments;
    }

    /**
     * @param array $attachments
     */
    public function setAttachments(array $attachments)
    {
        $this->attachments = [];
        foreach ($attachments as $attachment) {
            if (is_array($attachment)) {
                $productAttachment = new ProductAttachment();
                $productAttachment->setAttributes($attachment);
                $this->attachments[] = $productAttachment;
            } elseif ($attachment instanceof ProductAttachment) {
                $this->attachments[] = $attachment;
            }
        }
    }

    /**
     * @param array|null $modelAttributes
     * @return array
     * @throws yii\base\InvalidConfigException
     */
    public function exportFormAttributes(array $modelAttributes = null)
    {
        $result = [];
        $formName = $this->formName();
        $index = 0;
        foreach ($this->getAttachments() as $attachment) {
            foreach ($attachment->getAttributes() as $attribute => $value) {
                if (null === $modelAttributes || in_array($attribute, $modelAttributes)) {
                    $result[$formName . "[attachments][$index][$attribute]"] = (string) $value;
                }
            }
            $index++;
        }
        return $result;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function saveNew()
    {
        if ($this->validate(['attachments'])) {

            $attachments = [];
            foreach ($this->getAttachments() as $attachment) {
                if ($attachment->isNewRecord) {
                    $attachments[] = $attachment;
                }
            }

            if (empty($attachments)) {
                return true;
            }

            $transaction = Yii::$app->db->beginTransaction();
            try {
                foreach ($attachments as $attachment) {
                    $attachment->product_id = $this->product->id;
                    if (!$attachment->save()) {
                        $this->setErrors($attachment->errors, 'attachments');
                        $transaction->rollBack();
                        return false;
                    }
                }
                $transaction->commit();
                return true;
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
        return false;
    }

    /**
     * @return bool|File
     * @throws \Exception
     */
	public function upload()
	{
		if ($this->validate(['file'])) {

			$transaction = Yii::$app->db->beginTransaction();
			try {

				$file = new File();
				$file->mime_type = new FileType($this->file->type);
				$file->name = $this->file->name;
				$file->filename = time() . '_' . Yii::$app->security->generateRandomString(32) . '.' . $this->file->extension;
				$file->bucket = FileStorage::BUCKET_PRODUCT_FILES;
				$file->status = FileStatus::PROCESSING;
				$file->created_by = Yii::$app->user->id;

				if (!$file->save()) {
					$transaction->rollBack();
					$this->setErrors($file->errors);
					return false;
				}

				$file->filename = $file->id . '_' . $file->filename;
				$file->status = FileStatus::VISIBLE;
				if (!$file->save()) {
					$transaction->rollBack();
					$this->setErrors($file->errors);
					return false;
				}

				$productAttachment = new ProductAttachment();
				$productAttachment->file_id = $file->id;
				$productAttachment->product_id = $this->product->id;
				$productAttachment->meta = $this->params;

				if (!$productAttachment->save()) {
					$transaction->rollBack();
					$this->setErrors($productAttachment->errors);
					return false;
				}

				unset($this->product->attachments);

				$bucket = Yii::$app->fileStorage->getBucket($file->bucket);
				$bucket->copyFileIn($this->file->tempName, $file->filename);
				if (!$bucket->fileExists($file->filename)) {
					$transaction->rollBack();
					$this->addError('error', Yii::t('backend/error', 'Error saving file to bucket'));
					return false;
				}

				try {
                    $file->generateThumbs();
                } catch (\Exception $e) {
				    Yii::error('Error creating thumbnails: ' . $e->getMessage() . ' ' . $e->getFile() . ':' . $e->getLine(), __METHOD__);
                    Yii::error($e->getTraceAsString(), __METHOD__);
                    $this->addError('error', Yii::t('backend/error', 'Error creating thumbnails'));
                }

				$transaction->commit();
				return $file;

			} catch (\Exception $e) {
				$transaction->rollBack();
				throw $e;
			}

		} else {
			return false;
		}
	}
}
