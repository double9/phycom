<?php

namespace phycom\backend\models\product;


use phycom\common\models\product\Product;
use phycom\common\models\product\ProductPriceVariation;
use phycom\common\models\product\VariantOption;

use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class ProductPrice
 *
 * @package phycom\backend\models\product
 *
 * @property VariantOption[] $allPriceOptions
 */
class ProductPrice extends \phycom\common\models\product\ProductPrice
{
    const EVENT_AFTER_CREATE = 'afterCreate';

    private $_priceVariationList;
    private $_priceVariations = [];

    /**
     * @param Product $product
     * @return \phycom\common\models\product\ProductPrice
     */
    public static function create(Product $product)
    {
        $model = new static;
        $model->product_id = $product->id;
        $model->populateRelation('product', $product);
        $model->trigger(static::EVENT_AFTER_CREATE);
        return $model;
    }

    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_FIND, [$this, 'populatePriceVariations']);
        $this->on(self::EVENT_AFTER_CREATE, [$this, 'populatePriceVariations']);
    }

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [$this->getPriceVariationAttributes(), 'number'];
        $rules[] = [$this->getPriceVariationAttributes(), 'default', 'value' => null];
        return $rules;
    }

    /**
     * @param bool $skipIfSet
     * @return \phycom\common\models\product\ProductPrice
     * @throws \yii\base\InvalidConfigException
     */
    public function loadDefaultValues($skipIfSet = true)
    {
        $this->populatePriceVariations();
        return parent::loadDefaultValues($skipIfSet);
    }

    /**
     * Populates indexed Price variations
     * Includes all saved model but also keys for all possible options
     *
     * @return \phycom\common\models\product\ProductPrice
     * @throws \yii\base\InvalidConfigException
     */
    public function populatePriceVariations()
    {
        $optionKeys = array_map(function ($fieldOption) {
            /**
             * @var VariantOption $fieldOption
             */
            return $fieldOption->getCompositeKey();
        }, $this->getAllPriceOptions());

        $this->_priceVariations = array_fill_keys($optionKeys, null);

        foreach ($this->priceVariations as $model) {
            $this->_priceVariations[$model->getKey()] = $model->price;
        }
        return $this;
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            ArrayHelper::map($this->getAllPriceVariations(), 'key', 'label')
        );
    }

    /**
     * The list of All available price variations
     *
     * @return ProductPriceVariation[]
     * @throws \yii\base\InvalidConfigException
     */
    public function getAllPriceVariations()
    {
        if (!$this->_priceVariationList) {
            $models = [];
            foreach ($this->getAllPriceOptions() as $variantOption) {
                $model = Yii::$app->modelFactory->getProductPriceVariation([
                    'variant_name'     => $variantOption->customField->name,
                    'option_key'       => $variantOption->key,
                    'product_price_id' => $this->id
                ]);
                $model->populateRelation('productPrice', $this);
                $models[] = $model;
            }
            $this->_priceVariationList = $models;
        }
        return $this->_priceVariationList;
    }


    /**
     * Finds all product variant options for a price - options that are marked by "priceOption" flag
     *
     * @return VariantOption[]
     * @throws \yii\base\InvalidConfigException
     */
    protected function getAllPriceOptions()
    {
        $result = [];
        if ($this->product) {
            foreach ($this->product->productVariants as $productVariation) {
                $variant = $productVariation->getVariant();
                if ($variant->priceOption) {
                    foreach ($productVariation->options as $option) {
                        if ($option->is_visible) {
                            $result[] = $option->option;
                        }
                    }
                }
            }
        }
        return $result;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        if ($this->hasPriceVariation($name) && is_numeric($value)) {
            $this->_priceVariations[$name] = $value;
        } else {
            parent::__set($name, $value);
        }
    }

    /**
     * PHP getter magic method.
     * This method is overridden so that price option attributes can be accessed like properties.
     *
     * @param string $name property name
     * @throws \yii\base\InvalidArgumentException if relation name is wrong
     * @return mixed property value
     * @see getAttribute()
     */
    public function __get($name)
    {
        if ($this->hasPriceVariation($name)) {
            return $this->_priceVariations[$name];
        }
        return parent::__get($name);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasAttribute($name)
    {
        if ($this->hasPriceVariation($name)) {
            return true;
        }
        return parent::hasAttribute($name);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasPriceVariation($name)
    {
        return isset($this->_priceVariations[$name]) || array_key_exists($name, $this->_priceVariations);
    }

    /**
     * @return array
     */
    public function getPriceVariationAttributes()
    {
        return array_keys($this->_priceVariations);
    }

    /**
     * @param array $values
     * @param bool $safeOnly
     */
    public function setAttributes($values, $safeOnly = true)
    {
        foreach ($values as $key => $value) {
            if ($this->hasPriceVariation($key) && is_numeric($value)) {
                $this->_priceVariations[$key] = $value;
            }
        }
        parent::setAttributes($values, $safeOnly);
    }

    /**
     * @return bool
     * @throws Yii\base\Exception
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function savePriceVariations()
    {
        foreach ($this->_priceVariations as $key => $value) {
            $update = false;
            foreach ($this->priceVariations as $variation) {
                if ($variation->key === $key) {
                    if (null === $value) {
                        if (false === $variation->delete()) {
                            throw new yii\base\Exception('Error removing product price attribute ' . $key);
                        }
                    } else {
                        $variation->price = $value;
                        if (!$variation->update()) {
                            Yii::error($variation->errors, __METHOD__);
                            throw new yii\base\Exception('Error updating product price attribute ' . $key);
                        };
                    }
                    $update = true;
                    break;
                }
            }
            if (!$update && null !== $value) {
                $variation = Yii::$app->modelFactory->getProductPriceVariation()::create($key, $this);
                $variation->price = $value;
                if (!$variation->save()) {
                    Yii::error($variation->errors, __METHOD__);
                    throw new yii\base\Exception('Error creating product price attribute ' . $key);
                }
            }
        }
        return true;
    }
}
