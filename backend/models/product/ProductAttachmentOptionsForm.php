<?php

namespace phycom\backend\models\product;

use phycom\common\models\product\AttachmentParam;
use phycom\common\models\traits\ModelTrait;
use phycom\common\models\product\ProductAttachment;

use yii\base\Model;
use Yii;

/**
 * Class ProductAttachmentForm
 * @package phycom\backend\models\product
 *
 * @property int $primary
 * @property array $meta
 * @property AttachmentParam $params
 *
 * @property-read ProductAttachment $attachment
 */
class ProductAttachmentOptionsForm extends Model
{
	use ModelTrait;
    /**
     * @var bool
     */
	public $isVisible;
    /**
     * @var array
     */
	public $meta = [];
    /**
     * @var AttachmentParam[]
     */
	protected $params;
    /**
     * @var ProductAttachment
     */
	protected $attachment;

    /**
     * ProductAttachmentOptionsForm constructor.
     *
     * @param ProductAttachment $attachment
     * @param array $config
     */
	public function __construct(ProductAttachment $attachment, array $config = [])
	{
		$this->attachment = $attachment;
		parent::__construct($config);
	}

	public function init()
    {
        parent::init();
        $this->isVisible = $this->attachment->is_visible ? 1 : 0;
        $this->loadParams()->loadMeta();
    }

    public function rules()
	{
		return [
		    ['isVisible', 'boolean'],
            ['meta', 'safe']
		];
	}

	public function attributeLabels()
    {
        return [
            'isVisible' => Yii::t('backend/product/attachment', 'Is Visible')
        ];
    }

    /**
     * @return AttachmentParam
     */
	public function getParams()
    {
        return $this->params;
    }

    /**
     * @return ProductAttachment
     */
	public function getAttachment()
	{
		return $this->attachment;
	}

	public function save()
    {
        if ($this->validate()) {

            $this->attachment->is_visible = (bool) $this->isVisible;

            $meta = $this->attachment->meta ?: [];
            foreach ($this->meta as $key => $value) {
                $meta[$key] = $value;
            }
            foreach ($meta as $key => $value) {
                if (is_string($value)) {
                    $value = trim($value);
                    if (!$value) {
                        unset($meta[$key]);
                    }
                }
            }
            $this->attachment->meta = !empty($meta) ? $meta : null;
            return $this->attachment->save();
        }
        return false;
    }

    /**
     * @return $this
     * @throws yii\base\InvalidConfigException
     */
    public function loadParams()
    {
        $params = [];
        foreach (Yii::$app->modelFactory->getAttachmentParam()::findAll() as $param) {
            if ($param->enabledByVariant && !$this->attachment->product->getProductVariant($param->enabledByVariant)) {
                continue;
            }
            $params[] = $param;
        }
        $this->params = $params;
        return $this;
    }

    /**
     * @return $this
     */
    public function loadMeta()
    {
        $metaFields = $this->attachment->meta ?: [];
        foreach ($this->params as $param) {
            if (!array_key_exists($param->name, $metaFields)) {
                $metaFields[$param->name] = null;
            }
        }
        $this->meta = $metaFields;
        return $this;
    }
}
