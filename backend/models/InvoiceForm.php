<?php

namespace phycom\backend\models;


use phycom\common\models\traits\ModelTrait;
use phycom\common\models\Invoice;

use yii\base\Model;

/**
 * Class InvoiceForm
 * @package phycom\backend\models
 *
 * @property-read Invoice $invoice
 */
class InvoiceForm extends Model
{
	use ModelTrait;

	protected $invoice;

	public function __construct(Invoice $invoice = null, array $config = [])
	{
		$this->invoice = $invoice ?: new Invoice();
		parent::__construct($config);
	}

	public function getInvoice()
	{
		return $this->invoice;
	}
}