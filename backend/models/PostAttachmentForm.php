<?php

namespace phycom\backend\models;


use phycom\common\models\Post;
use phycom\common\models\File;
use phycom\common\models\PostAttachment;
use phycom\common\models\traits\ModelTrait;
use phycom\common\models\attributes\FileStatus;
use phycom\common\models\attributes\FileType;
use phycom\common\components\FileStorage;

use yii\base\Model;
use yii;

/**
 * Class PostAttachmentForm
 * @package phycom\backend\models
 *
 * @property-read Post $post
 * @property-read array $allowedExtensions
 * @property-read int $maxFileSize
 * @property-read int $maxFiles
 * @property-read array $postFiles
 */
class PostAttachmentForm extends Model
{
	use ModelTrait;
	/**
	 * @var yii\web\UploadedFile
	 */
	public $file;

	public $params;

	protected $post;

	public function __construct(Post $post, array $config = [])
	{
		$this->post = $post;
		parent::__construct($config);
	}

	public function rules()
	{
		return [
			[['file'],
				'file',
				'skipOnEmpty' => false,
				'extensions' => $this->allowedExtensions,
				'maxSize' => $this->maxFileSize,
				'maxFiles' => 1,
				'enableClientValidation' => false
			]
		];
	}

	public function attributeLabels()
	{
		return [
			'file' => Yii::t('backend/main','Attachment files')
		];
	}

	public function getPost()
	{
		return $this->post;
	}

	public function getAllowedExtensions()
	{
		return ['png', 'jpg', 'jpeg', 'gif', 'pdf'];
	}

	public function getMaxFiles()
	{
		return 10;
	}

	public function getMaxFileSize()
	{
		return 8*1024*1024;
	}

	public function getPostFiles()
	{
		return yii\helpers\ArrayHelper::getColumn($this->post->attachments, 'url');
	}

    /**
     * @return bool|File
     * @throws \Exception
     */
	public function upload()
	{
		if ($this->validate()) {

			$transaction = Yii::$app->db->beginTransaction();
			try {

				$file = new File();
				$file->mime_type = new FileType($this->file->type);
				$file->name = $this->file->name;
				$file->filename = time() . '_' . Yii::$app->security->generateRandomString(32) . '.' . $this->file->extension;
				$file->bucket = FileStorage::BUCKET_CONTENT_FILES;
				$file->status = FileStatus::PROCESSING;
				$file->created_by = Yii::$app->user->id;

				if (!$file->save()) {
					$transaction->rollBack();
					$this->setErrors($file->errors);
					return false;
				}

				$file->filename = $file->id . '_' . $file->filename;
				$file->status = FileStatus::VISIBLE;
				if (!$file->save()) {
					$transaction->rollBack();
					$this->setErrors($file->errors);
					return false;
				}

				$attachment = new PostAttachment();
				$attachment->file_id = $file->id;
				$attachment->post_id = $this->post->id;
				if (!empty($this->params)) {
                    $attachment->meta = $this->params;
                }

				if (!$attachment->save()) {
					$transaction->rollBack();
					$this->setErrors($attachment->errors);
					return false;
				}

				unset($this->post->attachments);

				$bucket = Yii::$app->fileStorage->getBucket($file->bucket);
				$bucket->copyFileIn($this->file->tempName, $file->filename);

				if (!$bucket->fileExists($file->filename)) {
					$transaction->rollBack();
					$this->addError('error', Yii::t('backend/error', 'Error saving file to bucket'));
					return false;
				}

                try {
                    $file->generateThumbs();
                } catch (\Exception $e) {
                    Yii::error('Error creating thumbnails: ' . $e->getMessage() . ' ' . $e->getFile() . ':' . $e->getLine(), __METHOD__);
                    Yii::error($e->getTraceAsString(), __METHOD__);
                    $this->addError('error', Yii::t('backend/error', 'Error creating thumbnails'));
                }

//                $this->checkPrimaryStatus($attachment);
//                $this->assignOrder($attachment);

                $transaction->commit();

                return $file;

			} catch (\Exception $e) {
				$transaction->rollBack();
				throw $e;
			}

		} else {
			return false;
		}
	}

//    /**
//     * @param PostAttachment $attachment
//     * @return false|int
//     * @throws \Throwable
//     * @throws yii\db\StaleObjectException
//     */
//    protected function assignOrder(PostAttachment $attachment)
//    {
//        /**
//         * @var PostAttachment $lastAttachment
//         */
//        $lastAttachment = $this->post->getAttachments()
//            ->orderBy(['order' => SORT_DESC])
//            ->one();
//
//        $attachment->order = $lastAttachment ? $lastAttachment->order + 1 : 1;
//        return $attachment->update(false, ['order']);
//    }
}
