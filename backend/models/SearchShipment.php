<?php

namespace phycom\backend\models;

use phycom\common\components\ActiveQuery;
use phycom\common\models\traits\SearchQueryFilter;
use phycom\common\interfaces\SearchModelInterface;
use phycom\common\models\Shipment;

use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\helpers\ArrayHelper;
use yii;

/**
 * Class SearchShipment
 * @package phycom\backend\models
 */
class SearchShipment extends Shipment implements SearchModelInterface
{
	use SearchQueryFilter;

	public $recipientName;
    public $recipientPhone;
    public $recipientEmail;
    public $recipientCompany;

	public $createdFrom;
	public $createdTo;
	public $updatedFrom;
	public $updatedTo;
	public $shippedFrom;
	public $shippedTo;

	public function rules()
	{
	    return [
            [['id','order_id','carrier_delivery_days'], 'integer'],
            [['from_address','to_address','return_address','postage_label', 'duration_terms'], 'string'],
            [['shipped_at','created_at','updated_at','status','eta','type','createdFrom','createdTo','updatedFrom','updatedTo','shippedFrom','shippedTo'], 'safe'],
            [['tracking_number','tracking_status','tracking_url','method','carrier_name','carrier_service','carrier_area','shipment_id','rate_id','delivery_time', 'recipientName'], 'string', 'max' => 255],
        ];
	}

	public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'recipientName'    => Yii::t('backend/shipment', 'Recipient Name'),
            'recipientPhone'   => Yii::t('backend/shipment', 'Recipient Phone'),
            'recipientEmail'   => Yii::t('backend/shipment', 'Recipient Email'),
            'recipientCompany' => Yii::t('backend/shipment', 'Recipient Company')
        ]);
    }

    /**
	 * Creates data provider instance with search query applied
	 * @param array $params
	 * @return ActiveDataProvider
	 */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
			'pagination' => [
				'pageSize' => 20
			],
		]);

		$this->sort($dataProvider->sort);

		if ($this->order_id) {
			$query->andWhere(['s.order_id' => $this->order_id]);
		}

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			's.id' => $this->id,
			's.tracking_number' => $this->tracking_number,
			's.status' => (string)$this->status,
		]);

		$query->filterText('s.carrier_name', $this->carrier_name);
		$query->filterText('s.carrier_area', $this->carrier_area);
		$query->filterText('(s.to_address #>> \'{"name"}\') :: TEXT', $this->recipientName);
        $query->filterText('(s.to_address #>> \'{"company"}\') :: TEXT', $this->recipientCompany);
        $query->filterText('(s.to_address #>> \'{"email"}\') :: TEXT', $this->recipientEmail);
        $query->filterText('(s.to_address #>> \'{"phone"}\') :: TEXT', $this->recipientPhone);

		$query->filterDateRange('s.created_at', $this->createdFrom, $this->createdTo);
		$query->filterDateRange('s.updated_at', $this->updatedFrom, $this->updatedTo);
		$query->filterDateRange('s.shipped_at', $this->shippedFrom, $this->shippedTo);
		return $dataProvider;
	}

	protected function sort(Sort $sort)
	{

	}

	/**
	 * @return ActiveQuery
	 */
	protected function createSearchQuery()
	{
		$query = static::find();
		$query->select([
			's.*',
            '(s.to_address #>> \'{"name"}\') as "recipientName"',
            '(s.to_address #>> \'{"phone"}\') as "recipientPhone"',
            '(s.to_address #>> \'{"email"}\') as "recipientEmail"',
            '(s.to_address #>> \'{"company"}\') as "recipientCompany"',
		]);
		$query->from(['s' => Shipment::tableName()]);
		return $query;
	}
}