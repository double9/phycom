<?php

namespace phycom\backend\models;


use phycom\common\helpers\Currency;
use phycom\common\models\attributes\AddressField;
use phycom\common\models\attributes\ShopStatus;
use phycom\common\models\Country;
use phycom\common\models\Shop;
use phycom\common\models\traits\ModelTrait;

use phycom\common\modules\delivery\Module as DeliveryModule;
use phycom\common\modules\delivery\models\DeliveryArea;

use yii\base\Model;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class DeliveryAreaForm
 * @package phycom\backend\models
 *
 * @property-read DeliveryArea $deliveryArea
 */
class DeliveryAreaForm extends Model
{
	use ModelTrait;

	public $method;
	public $carrier;
	public $service;
	public $country;
	public $province;
    public $locality;
	public $city;
	public $district;
	public $street;
	public $postcode;
	public $price;
	public $deliveryTime;
	public $status;

	protected $deliveryArea;

	public function __construct(DeliveryArea $area = null, array $config = [])
	{
		$this->deliveryArea = $area ?: new DeliveryArea();
		parent::__construct($config);
	}

	public function init()
    {
        parent::init();
        $this->country = (string) Yii::$app->country;
    }

	public function rules()
    {
        return [
            [['method','country','price','status'], 'required'],
            [['price','postcode','deliveryTime'], 'number'],
            [['method','carrier','service','country','province','locality','city','street','status'], 'string']
        ];
    }

    public function attributeHints()
    {
        return [];
    }

    public function getDeliveryArea()
	{
		return $this->deliveryArea;
	}

	public function getDeliveryMethods()
    {
        /**
         * @var DeliveryModule $delivery
         */
        $delivery = Yii::$app->getModule('delivery');
        return ArrayHelper::map($delivery->getAllMethods(), 'id', 'name');
    }

    public function getCountries()
    {
        $countries = Country::find()->orderBy(['name' => SORT_ASC]);
        return ArrayHelper::map($countries->all(), 'code', 'name');
    }

    public function getDivisions()
    {
        if ($this->country) {
            $country = Country::findOne(['code' => $this->country]);
            return array_map(function ($item) {return $item['name'];}, $country->divisions);
        }
        return [];
    }

    /**
     * @return Shop[]
     */
    public function getShops()
    {
        $Shop = Yii::$app->modelFactory->getShop();
        return $Shop::find()->where(['not', 'status', [ShopStatus::DELETED]])->all();
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function save()
    {
        if ($this->validate()) {
            $da = $this->deliveryArea;

            $da->method = $this->method;
            $da->carrier = $this->carrier ?: null;
            $da->service = $this->service ?: null;

            $divisions = $this->getDivisions();

            /**
             * @var AddressField $area
             */
            $area = Yii::createObject(AddressField::class, [[
                'country'  => $this->country,
                'province' => $divisions[$this->province] ?? null,
                'city'     => $this->city ?: null,
                'locality' => $this->locality ?: null,
                'district' => $this->district ?: null,
                'street'   => $this->street ?: null,
                'postcode' => $this->postcode ?: null,
            ]]);

            if ($shop = $this->checkIsShop($area)) {
                $da->shop_id = $shop->id;
            }

            $da->area = $area->toArray();
            $da->area_code = $da->generateAreaCode();
            $da->code = $da->generateCode();

            $da->price = Currency::toInteger($this->price);
            $da->delivery_time = $this->deliveryTime;
            $da->status = $this->status;

            return $da->save();
        }
    }

    /**
     * Checks if the particular delivery area has exactly the same address as a shop.
     *
     * @param AddressField $area
     * @return Shop|null
     */
    protected function checkIsShop(AddressField $area)
    {
        $areaJson = $area->toJson();
        foreach ($this->getShops() as $shop) {
            $shopAddressJson = $shop->address->export()->toJson([
                'country',
                'province',
                'city',
                'locality',
                'district',
                'street',
                'postcode'
            ]);

            if ($areaJson === $shopAddressJson) {
                return $shop;
            }
        }
        return null;
    }
}
