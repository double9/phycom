<?php

namespace phycom\backend\models;

use phycom\common\models\Language;
use phycom\common\models\PostCategory;
use phycom\common\models\translation\PostCategoryTranslation;
use phycom\common\models\traits\ModelTrait;
use phycom\common\models\attributes\CategoryStatus;
use phycom\common\models\attributes\TranslationStatus;

use yii\helpers\Inflector;
use yii\base\InvalidArgumentException;
use yii\base\Model;
use yii;

/**
 * Class PostCategoryForm
 * @package phycom\backend\models
 *
 * @property string $language
 *
 * @property-read PostCategory $category
 * @property-read PostCategoryTranslation $translation
 * @property-read array $statuses
 * @property-read array $translationStatuses
 * @property-read array $parents
 * @property-read array $allTags
 */
class PostCategoryForm extends Model
{
	use ModelTrait;

	public $status;
	public $parent;

	public $urlKey;
	public $title;
	public $description;
	public $translationStatus;
	public $metaTitle;
	public $metaDescription;
	public $metaKeywords;
	public $featured;

	/**
	 * @var PostCategory
	 */
	protected $category;
	/**
	 * @var PostCategoryTranslation
	 */
	protected $translation;

	/**
	 * PostCategoryForm constructor.
	 *
	 * @param Language|string $languageCode
	 * @param PostCategory $category
	 * @param array $config
	 */
	public function __construct($languageCode, PostCategory $category = null, array $config = [])
	{
		$this->category = $category ?: new PostCategory();
		$this->language = $languageCode;
		parent::__construct($config);
	}

	public function rules()
	{
		return [
			[['title'], 'required'],
			[['parent'], 'integer'],
			[['title', 'description', 'status', 'translationStatus', 'metaTitle', 'metaKeywords', 'metaDescription', 'language'], 'string'],
			[['urlKey','status', 'translationStatus'], 'string', 'max' => 255],
            [['featured'], 'boolean'],
		];
	}

	public function attributeLabels()
	{
		return [
			'language' => Yii::t('backend/main','Language'),
			'parent' => Yii::t('backend/main','Parent category'),
			'status' => $this->category->getAttributeLabel('status'),
			'urlKey' => $this->translation->getAttributeLabel('url_key'),
			'title' => $this->translation->getAttributeLabel('title'),
			'description' => $this->translation->getAttributeLabel('description'),
			'translationStatus' => Yii::t('backend/main','Translation status'),
			'metaTitle' => $this->translation->getAttributeLabel('meta_title'),
			'metaKeywords' => $this->translation->getAttributeLabel('meta_keywords'),
			'metaDescription' => $this->translation->getAttributeLabel('meta_description'),
            'featured' => $this->category->getAttributeLabel('featured')
		];
	}

	public function init()
	{
		parent::init();
		$this->parent = $this->category->parent_id;
		$this->urlKey = $this->translation->url_key;
		$this->title = $this->translation->title;
		$this->description = $this->translation->description;
		$this->status = $this->category->status;
        $this->featured = $this->category->featured ? 1 : 0;
		$this->translationStatus = $this->translation->status;
		$this->metaTitle = $this->translation->meta_title;
		$this->metaKeywords = $this->translation->meta_keywords;
		$this->metaDescription = $this->translation->meta_description;
	}

	public function getParents()
	{
		$query = (new yii\db\Query())
			->select([
				'c.id',
				'CASE WHEN (t1.title IS NOT NULL) THEN t1.title ELSE t2.title END as title'
			])
			->from(['c' => PostCategory::tableName()])
			->where(['not', ['c.status' => CategoryStatus::DELETED]])
			->leftJoin(['t1' => PostCategoryTranslation::tableName()], 't1.post_category_id = c.id AND t1.language = :lang')
			->leftJoin(['t2' => PostCategoryTranslation::tableName()], 't2.post_category_id = c.id AND t2.language = :fallback_lang AND t2.language != :lang')
			->addParams([
				'lang' => $this->language,
				'fallback_lang' => $this->translation->fallbackLanguage
			]);

		if (!$this->category->isNewRecord) {
			$query->andWhere(['not', ['c.id' => $this->category->id]]);
		};

		$parents = [];
		foreach ($query->all() as $category) {
			$parents[$category['id']] = $category['title'];
		}
		return $parents;
	}

	public function getTranslationStatuses()
	{
		$statuses = TranslationStatus::displayValues();
		unset($statuses[TranslationStatus::DELETED]);
		return $statuses;
	}

	public function getStatuses()
	{
		$statuses = CategoryStatus::displayValues();
		unset($statuses[CategoryStatus::DELETED]);
		return $statuses;
	}

	public function getCategory()
	{
		return $this->category;
	}

	public function getTranslation()
	{
		return $this->translation;
	}

	public function getLanguage()
	{
		return $this->translation->language;
	}

	public function setLanguage($languageCode)
	{
		$language = $languageCode instanceof Language ? $languageCode : Language::findOne(['code' => $languageCode]);
		if (!$language) {
			throw new InvalidArgumentException('Invalid language code ' . $languageCode);
		} else {
			$this->translation = $this->category->getTranslation($language->code, false) ??
				new PostCategoryTranslation([
					'post_category_id' => $this->category->id,
					'language' => $language->code,
					'status' => new TranslationStatus(TranslationStatus::DRAFT)
				]);
			$this->translation->populateRelation('languageModel', $language);
		}
	}

	public function update()
	{
		if ($this->validate()) {
			$transaction = Yii::$app->db->beginTransaction();
			try {
				$this->translation->title = $this->title;
				$this->translation->description = $this->description;
				$this->translation->meta_title = $this->metaTitle;
				$this->translation->meta_keywords = $this->metaKeywords;
				$this->translation->meta_description = $this->metaDescription;

				$this->translation->url_key = $this->urlKey ? Inflector::slug($this->urlKey) : Inflector::slug($this->title);


				if ($this->translationStatus) {
					$this->translation->status = $this->translationStatus;
				}
				$this->category->featured = (bool) $this->featured;
                $this->category->status = $this->status;
                if ((int) $this->parent) {
                    $this->category->parent_id = (int) $this->parent;
                }
                if (!$this->category->save()) {
                    return $this->rollback($transaction, $this->category->errors);
                }


				$this->translation->post_category_id = $this->category->id;

				if (!$this->translation->save()) {
					return $this->rollback($transaction, $this->translation->errors);
				}
				$transaction->commit();
				return true;

			} catch (\Exception $e) {
				$transaction->rollBack();
				throw $e;
			}
		}
		return false;
	}
}