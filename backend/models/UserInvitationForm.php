<?php

namespace phycom\backend\models;

use phycom\common\models\traits\ModelTrait;
use phycom\common\models\attributes\ContactAttributeStatus;
use phycom\common\models\attributes\MessagePriority;
use phycom\common\models\attributes\MessageStatus;
use phycom\common\models\attributes\MessageType;
use phycom\common\models\attributes\UserStatus;
use phycom\common\models\attributes\UserTokenType;
use phycom\common\models\attributes\UserType;
use phycom\common\models\attributes\VendorStatus;
use phycom\common\models\Email;
use phycom\common\models\Message;
use phycom\common\models\User;
use phycom\common\models\UserToken;
use phycom\common\models\Vendor;
use phycom\common\rbac\Role;

use yii\base\InvalidValueException;
use yii\helpers\ArrayHelper;
use yii\base\Model;
use yii;

/**
 * Class UserInvitationForm
 * @package phycom\backend\model
 *
 * @property-read Vendor[] $vendorList
 * @property-read int $vendorCount
 * @property-read Role[] $rolesList
 */
class UserInvitationForm extends Model
{
	use ModelTrait;

	public $vendorId;
	public $firstName;
	public $lastName;
	public $email;
	public $role;

	private $vendorCount;
	private $user;

	public function __construct(User $user = null, array $config = [])
	{
		if ($user) {
			if (!$user->status->is(UserStatus::PENDING)) {
				throw new InvalidValueException('User ' . $user->id . ' is not pending registration');
			}
			$this->user = $user;
		}
		parent::__construct($config);
	}

    /**
     * @inheritdoc
     */
	public function init()
	{
		parent::init();
		$this->vendorCount = Yii::$app->user->identity->getVendors()->count();
		if ($this->vendorCount === 1) {
			$this->vendorId = Yii::$app->user->identity->vendors[0]->id;
		}
	}

    /**
     * @inheritdoc
     */
	public function rules()
	{
		return [
			[['vendorId','firstName', 'lastName', 'email', 'role'], 'required'],
            ['role', 'in', 'range' => ArrayHelper::getColumn($this->getSelectableUserRoles(), 'name')],
			['vendorId', 'integer'],
			['vendorId', 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getVendor()), 'targetAttribute' => 'id'],
			[['firstName', 'lastName', 'email', 'role'], 'string', 'max' => 255],
			['email', 'email']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
        return [
            'vendorId'  => Yii::t('backend/user', 'Vendor'),
            'firstName' => Yii::t('backend/user', 'First name'),
            'lastName'  => Yii::t('backend/user', 'Last name'),
            'email'     => Yii::t('backend/user', 'Email'),
            'role'      => Yii::t('backend/user', 'Role')
        ];
	}


    /**
     * @return Role[]
     */
	public function getRolesList()
    {
        return array_reverse($this->getSelectableUserRoles());
    }

    /**
     * @return Role[]
     */
    protected function getSelectableUserRoles()
    {
        $exclude = ['user','guest'];
        $roles = [];
        $userRoles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);
        /**
         * @var Role[] $userRoles
         */
        foreach ($userRoles as $role) {
            /**
             * @var Role[] $childRoles
             */
            $childRoles = Yii::$app->authManager->getChildRoles($role->name);
            foreach ($childRoles as $childRole) {
                if (!in_array($childRole->name, $exclude)) {
                    $roles[] = $childRole;
                }
            }
        }
        return $roles;
    }

    /**
     * @return int
     */
	public function getVendorCount()
	{
		return $this->vendorCount;
	}

    /**
     * @return array
     */
	public function getVendorList()
	{
		return ArrayHelper::map(Yii::$app->user->identity->vendors, 'id', 'name');
	}

	/**
	 * @return bool|false
	 * @throws \Exception
	 */
	public function resendInvitation()
	{
		if (!$this->user) {
			$this->addError('error', Yii::t('backend/user', 'User not found'));
			return false;
		}

		$transaction = Yii::$app->db->beginTransaction();
		try {

			$token = $this->createToken($this->user);
			if (!$token->save()) {
				return $this->rollback($transaction, $token->errors);
			}

			$this->sendInvitation($token);
			$transaction->commit();
			return true;

		} catch(\Exception $e) {
			$transaction->rollBack();
			throw $e;
		}
	}

	public function create()
	{
		if ($this->validate()) {

			$transaction = Yii::$app->db->beginTransaction();
			try {

				// create user
				$user = Yii::$app->modelFactory->getUser();
				$user->status = UserStatus::PENDING;
				$user->type = UserType::ADMIN;
				$user->first_name = $this->firstName;
				$user->last_name = $this->lastName;
				$user->generateAuthKey();
				$user->setPassword(Yii::$app->security->generateRandomString());

				if (!$user->save()) {
					return $this->rollback($transaction, $user->errors);
				}
				$this->getVendor()->addUser($user);

				// create user email
				$email = new Email();
				$email->email = $this->email;
				$email->user_id = $user->id;
				$email->status = ContactAttributeStatus::UNVERIFIED;

				if (!$email->save()) {
					return $this->rollback($transaction, $email->errors, 'email');
				}

				// create token
				$token = $this->createToken($user);
				if (!$token->save()) {
					return $this->rollback($transaction, $token->errors);
				}

				// send the email with link
				$this->sendInvitation($token);
				$transaction->commit();
				return true;

			} catch(\Exception $e) {
				$transaction->rollBack();
				throw $e;
			}
		}
		return false;
	}

	/**
	 * @return yii\db\ActiveRecord|Vendor
	 */
	protected function getVendor()
	{
		return Yii::$app->modelFactory->getVendor()::find()->where(['id' => $this->vendorId])->andWhere(['not', ['vendor.status' => VendorStatus::DELETED]])->one();
	}

	protected function createToken(User $user)
	{
		$token = new UserToken();
		$token->data = $this->createTokenData();
		$token->type = UserTokenType::REGISTRATION_REQUEST;
		$token->user_id = $user->id;
		$token->generate()->populateRelation('user', $user);

		return $token;
	}

	protected function sendInvitation(UserToken $token)
	{
		$tpl = Yii::$app->modelFactory->getMessageTemplate()::getTemplate('user_invitation');
		$msg = new Message();
		$msg->type = MessageType::EMAIL;
		$msg->status = MessageStatus::NEW;
		$msg->user_from = Yii::$app->systemUser->id;
		$msg->user_to = $token->user_id;
		$msg->subject = $tpl->renderTitle(['appname' => Yii::$app->name, 'firstname' => $token->user->first_name]);
		$msg->content = $tpl->render([
			'appname' => Yii::$app->name,
			'firstname' => $token->user->first_name,
			'link' => Yii::$app->urlManagerBackend->createAbsoluteUrl(['/site/register', 't' => $token->token])
		]);
		$msg->priority = MessagePriority::PRIORITY_1;
		if (!$msg->save()) {
			$this->setErrors($msg->errors);
		}
		$msg->queue();
	}

    /**
     * @return array
     */
	protected function createTokenData()
    {
        return [
            'vendor' => $this->vendorId,
            'roles' => [$this->role]
        ];
    }

	/**
	 * @return string
	 */
	protected function serializeTokenData()
	{
		return json_encode($this->createTokenData());
	}
}
