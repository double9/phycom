<?php

namespace phycom\backend\models;

use phycom\common\components\ActiveQuery;
use phycom\common\models\traits\SearchQueryFilter;
use phycom\common\interfaces\SearchModelInterface;
use phycom\common\models\attributes\DiscountRuleStatus;
use phycom\common\models\DiscountRule;

use yii\data\ActiveDataProvider;
use yii\data\Sort;

/**
 * Class SearchDiscountCard
 * @package phycom\backend\models
 */
class SearchDiscountRule extends DiscountRule implements SearchModelInterface
{
	use SearchQueryFilter;

	public $createdFrom;
	public $createdTo;
	public $updatedFrom;
	public $updatedTo;
	public $expiresFrom;
	public $expiresTo;
	public $birthdayFrom;
	public $birthdayTo;

	public function rules()
	{
		return [
			[['shop_id', 'used', 'created_by'], 'integer'],
			[['discount_rate', 'birthday_discount_rate'], 'number'],
			[[
				'birthday',
				'expires_at',
				'created_at',
				'updated_at',
				'createdFrom',
				'createdTo',
				'updatedFrom',
				'updatedTo',
				'expiresFrom',
				'expiresTo',
				'birthdayFrom',
				'birthdayTo',
				'type',
				'status'
			], 'safe'],
			[['code', 'name'], 'string', 'max' => 255],

		];
	}

	/**
	 * Creates data provider instance with search query applied
	 * @param array $params
	 * @return ActiveDataProvider
	 */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
			'pagination' => [
				'pageSize' => 20
			],
		]);

		$this->sort($dataProvider->sort);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'd.id' => $this->id,
			'd.status' => (string)$this->status,
			'd.type' => (string)$this->type,
			'd.shop_id' => $this->shop_id,
			'd.discount_rate' => $this->discount_rate,
			'd.birthday_discount_rate' => $this->birthday_discount_rate
		]);

        $query->andFilterWhere(['ilike', 'd.code', $this->code]);
		$query->filterText('d.name', $this->name);
		$query->filterDateRange('d.created_at', $this->createdFrom, $this->createdTo);
		$query->filterDateRange('d.updated_at', $this->updatedFrom, $this->updatedTo);
		$query->filterDateRange('d.expires_at', $this->expiresFrom, $this->expiresTo);
		$query->filterDateRange('d.birthday', $this->birthdayFrom, $this->birthdayTo);
		return $dataProvider;
	}

	protected function sort(Sort $sort)
	{

	}

	/**
	 * @return ActiveQuery
	 */
	protected function createSearchQuery()
	{
		$query = static::find();
		$query->select(['d.*']);
		$query->from(['d' => DiscountRule::tableName()]);
		$query->where(['not', ['d.status' => DiscountRuleStatus::DELETED]]);
		if ($this->type) {
			$query->andWhere(['d.type' => $this->type]);
		}
		return $query;
	}
}