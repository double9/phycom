<?php

namespace phycom\backend\models;


use phycom\common\components\ActiveQuery;
use phycom\common\helpers\Url;
use phycom\common\models\ActiveRecord;
use phycom\common\models\attributes\ContactAttributeStatus;
use phycom\common\models\attributes\DiscountRuleType;
use phycom\common\models\attributes\PostStatus;
use phycom\common\models\attributes\PostType;
use phycom\common\models\attributes\ProductStatus;
use phycom\common\models\attributes\SearchResultType;
use phycom\common\models\attributes\UserStatus;
use phycom\common\models\Comment;
use phycom\common\models\DiscountRule;
use phycom\common\models\Email;
use phycom\common\models\Invoice;
use phycom\common\models\Language;
use phycom\common\models\Order;
use phycom\common\models\Payment;
use phycom\common\models\Phone;
use phycom\common\models\Post;
use phycom\common\models\product\Product;
use phycom\common\models\Shipment;
use phycom\common\models\Subscription;
use phycom\common\models\translation\PostTranslation;
use phycom\common\models\translation\ProductTranslation;
use phycom\common\models\User;

use yii\helpers\ArrayHelper;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use yii;

/**
 * Class SearchAll
 * @package phycom\backend\models
 *
 * @property SearchResultType $type
 * @property-read string $url
 */
class SearchAll extends ActiveRecord
{
	public $q;

    /**
     * @var Language
     */
    public $language;
    /**
     * @var SearchResultType
     */
    public $type;
    /**
     * @var int
     */
    public $id;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
	public $description;

    /**
     * @return string - primary table name
     */
	public static function tableName()
    {
        return 'product';
    }

    public static function primaryKey()
    {
        return ['id', 'type'];
    }

    public function attributes()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'dynamic-attribute' => [
                'attributes' => ['type' => SearchResultType::class]
            ]
        ]);
    }

    /**
	 * @return ActiveDataProvider
	 */
	public function search()
	{
        $dataProvider = new ActiveDataProvider([
            'query' => $this->createSearchQuery(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $dataProvider;
	}

	public function getUrl()
    {
        switch ((string)$this->type) {
            case SearchResultType::USER:
                return Url::toRoute(['/user/profile/', 'id' => $this->id]);
            case SearchResultType::PAYMENT:
                return Url::toRoute(['/payment-list/edit/', 'id' => $this->id]);
            case SearchResultType::COUPON:
                $model = DiscountRule::findOne($this->id);
                switch ((string) $model->type) {
                    case DiscountRuleType::CLIENT_CARD:
                        return Url::toRoute(['/client-card/edit/', 'id' => $this->id]);
                    case DiscountRuleType::COUPON:
                        return Url::toRoute(['/promotion-code/edit/', 'id' => $this->id]);
                    default:
                        return '#';
                }
            default:
                return Url::toRoute(['/'. (string)$this->type . '/edit/', 'id' => $this->id]);
        }
    }

    /**
     * @return ActiveQuery
     */
	protected function createSearchQuery()
    {
        switch ((string) $this->type) {

            case SearchResultType::PRODUCT:
                $subQuery = $this->searchProducts();
                break;
            case SearchResultType::ORDER:
                $subQuery = $this->searchOrders();
                break;
            case SearchResultType::USER:
                $subQuery = $this->searchUsers();
                break;
            case SearchResultType::COMMENT:
                $subQuery = $this->searchComments();
                break;
            case SearchResultType::POST:
                $subQuery = $this->searchPosts(PostType::POST);
                break;
            case SearchResultType::PAGE:
                $subQuery = $this->searchPosts(PostType::PAGE);
                break;
            case SearchResultType::PAYMENT:
                $subQuery = $this->searchPayments();
                break;
            case SearchResultType::INVOICE:
                $subQuery = $this->searchInvoices();
                break;
            case SearchResultType::SHIPMENT:
                $subQuery = $this->searchShipments();
                break;
//            case SearchResultType::SUBSCRIPTION:
//                $subQuery = $this->searchSubscriptions();
//                break;
            case SearchResultType::COUPON:
                $subQuery = $this->searchCoupons();
                break;
            default:
                $subQuery = $this->searchProducts()
                    ->union($this->searchOrders())
                    ->union($this->searchUsers())
                    ->union($this->searchPayments())
                    ->union($this->searchInvoices())
                    ->union($this->searchShipments())
//                    ->union($this->searchSubscriptions())
                    ->union($this->searchCoupons())
                    ->union($this->searchComments())
                    ->union($this->searchPosts(PostType::POST))
                    ->union($this->searchPosts(PostType::PAGE));
        }

        return (new ActiveQuery(static::class))->select('r.*')->from(['r' => $subQuery]);
    }

    /**
     * @return Query
     */
    protected function searchProducts()
    {
        return (new Query())
            ->select([
                'pr.id AS "id"',
                '\'' . SearchResultType::PRODUCT . '\' AS "type"',
                'CASE WHEN (t1.title IS NOT NULL) THEN t1.title ELSE t2.title END AS title',
                'CASE WHEN (t1.description IS NOT NULL) THEN t1.description ELSE t2.description END AS description',
            ])
            ->from(['pr' => Product::tableName()])
            ->where(['not', ['pr.status' => ProductStatus::DELETED]])
            ->leftJoin(['t1' => ProductTranslation::tableName()], 't1.product_id = pr.id AND t1.language = :lang')
            ->leftJoin(['t2' => ProductTranslation::tableName()], 't2.product_id = pr.id AND t2.language = :fallback_lang AND t2.language != :lang')
            ->andWhere([
                'or',
                '(t1.title ILIKE :search)',
                '(t2.title ILIKE :search)',
                '(t1.description ILIKE :search)',
                '(t2.description ILIKE :search)',
            ])
            ->addParams([
                'search' => '%' . trim($this->q) . '%',
                'lang' => $this->language->code,
                'fallback_lang' => Yii::$app->lang->findFallbackLanguage($this->language)->code
            ]);
    }

    /**
     * @return Query
     */
    protected function searchOrders()
    {
        $titleOrder = Yii::t('backend/search', 'Order');
        $titleBy = Yii::t('backend/search', 'by');

        return (new Query())
		    ->select([
                'o.id AS "id"',
                '\'' . SearchResultType::ORDER . '\' AS "type"',
                'TRIM(CONCAT_WS(\' \', \''.$titleOrder.'\', o.number, \''.$titleBy.'\', u.first_name, u.last_name)) AS title',
                'o.comment AS description'
            ])
            ->from(['o' => Order::tableName()])
		    ->innerJoin(['u' => User::tableName()], 'o.user_id = u.id')
            ->andWhere([
                'or',
                ['o.number' => trim($this->q)],
                ['o.phone_number' => trim($this->q)],
                '(o.comment ILIKE :search)',
                '(u.first_name ILIKE :search)',
                '(u.last_name ILIKE :search)',

            ], ['search' => '%' . trim($this->q) . '%']);
    }

    /**
     * @return Query
     */
    protected function searchComments()
    {
        $titleComment = Yii::t('backend/search', 'Comment');
        $titleBy = Yii::t('backend/search', 'by');

        return (new Query())
            ->select([
                'c.id AS "id"',
                '\'' . SearchResultType::COMMENT . '\' AS "type"',
                'TRIM(CONCAT_WS(\' \', \''.$titleComment.'\', c.id, \''.$titleBy.'\', c.author_email, c.author_name)) AS title',
                'c.content AS description'
            ])
            ->from(['c' => Comment::tableName()])
            ->andWhere([
                'or',
                ['c.author_name' => trim($this->q)],
                ['c.author_email' => trim($this->q)],
                '(c.content ILIKE :search)',

            ], ['search' => '%' . trim($this->q) . '%']);
    }

    /**
     * @param string $type
     * @return Query
     */
    protected function searchPosts($type = PostType::PAGE)
    {
        return (new Query())
            ->select([
                $type . '.id AS "id"',
                '\'' . $type . '\' AS "type"',
                'CASE WHEN (t1.title IS NOT NULL) THEN t1.title ELSE t2.title END AS title',
                'CASE WHEN (t1.content IS NOT NULL) THEN t1.content ELSE t2.content END AS description'
            ])
            ->from([$type => Post::tableName()])
            ->leftJoin(['t1' => PostTranslation::tableName()], 't1.post_id = '.$type.'.id AND t1.language = :lang')
            ->leftJoin(['t2' => PostTranslation::tableName()], 't2.post_id = '.$type.'.id AND t2.language = :fallback_lang AND t2.language != :lang')
            ->where(['not', [$type . '.status' => PostStatus::DELETED]])
            ->andWhere([$type . '.type' => $type])
            ->andWhere([
                'or',
                '(t1.title ILIKE :search)',
                '(t2.title ILIKE :search)',
                '(t1.content ILIKE :search)',
                '(t2.content ILIKE :search)',
            ])
            ->addParams([
                'search'        => '%' . trim($this->q) . '%',
                'lang'          => $this->language->code,
                'fallback_lang' => Yii::$app->lang->findFallbackLanguage($this->language)->code
            ]);
    }

    /**
     * @return Query
     */
    protected function searchUsers()
    {
        $q = trim($this->q);
        $qw = preg_replace('/\s+/', '', $q);
        return (new Query())
            ->select([
                'u.id',
                '\'' . SearchResultType::USER . '\' AS "type"',
                'CASE WHEN (u.company_name IS NOT NULL) THEN CONCAT_WS(\' \', u.first_name, u.last_name, \'-\', u.company_name) ELSE CONCAT_WS(\' \', u.first_name, u.last_name) END AS title',
                new yii\db\Expression('NULL AS description')
            ])
            ->from(['u' => User::tableName()])
            ->leftJoin(['e' => Email::tableName()], [
                'and',
                'e.user_id = u.id',
                'e.status != :status_deleted',
            ], ['status_deleted' => ContactAttributeStatus::DELETED])
            ->leftJoin(['ph' => Phone::tableName()], [
                'and',
                'ph.user_id = u.id',
                ['not', ['ph.status' => ContactAttributeStatus::DELETED]]
            ], ['status_deleted' => ContactAttributeStatus::DELETED])
            ->where(['not', ['u.status' => UserStatus::DELETED]])
            ->andWhere([
                'or',
                ['u.username' => $q],
                ['u.personal_code' => $q],
                ['u.reference_number' => $q],
                ['e.email' => $q],
                ['ph.phone_nr' => $qw],
                ['CONCAT(ph.country_code, ph.phone_nr)' => $qw],
                ['CONCAT(\'+\', ph.country_code, ph.phone_nr)' => $qw],
                'CONCAT_WS(\' \', u.first_name, u.last_name) ILIKE :search',
                'u.company_name ILIKE :search',
                'u.display_name ILIKE :search',
            ])
            ->groupBy(['u.id', 'e.email', 'ph.phone_nr'])
            ->addParams(['search' => '%' . $q . '%']);
    }


    protected function searchSubscriptions()
    {
        $q = trim($this->q);
        $qw = preg_replace('/\s+/', '', $q);

        return (new Query())
            ->select([
                'sb.id AS "id"',
                '\'' . SearchResultType::SUBSCRIPTION . '\' AS "type"',
                'sb.email AS title',
                'CONCAT_WS(\' \', sb.first_name, sb.last_name) AS description',
            ])
            ->from(['sb' => Subscription::tableName()])
            ->andWhere([
                'or',
                ['sb.email' => $qw],
                'CONCAT_WS(\' \', sb.first_name, sb.last_name) ILIKE :search',
            ], ['search' => '%' . $q . '%']);
    }

    protected function searchCoupons()
    {
        $q = trim($this->q);
        $qw = preg_replace('/\s+/', '', $q);

        return (new Query())
            ->select([
                'dr.id AS "id"',
                '\'' . SearchResultType::COUPON . '\' AS "type"',
                'dr.code AS title',
                'CONCAT_WS(\' \', dr.name) AS description',
            ])
            ->from(['dr' => DiscountRule::tableName()])
            ->andWhere([
                'or',
                ['dr.code' => $qw],
                ['dr.personal_code' => $qw],
                'dr.name ILIKE :search',
            ], ['search' => '%' . $q . '%']);
    }

    protected function searchPayments()
    {
        $titlePayment = Yii::t('backend/search', 'Payment');
        $titleBy = Yii::t('backend/search', 'by');
        $q = trim($this->q);
        $qw = preg_replace('/\s+/', '', $q);

        return (new Query())
            ->select([
                'pa.id AS "id"',
                '\'' . SearchResultType::PAYMENT . '\' AS "type"',
                'TRIM(CONCAT_WS(\' \', \''.$titlePayment.'\', ROUND(pa.amount, 2))) AS title',
                'TRIM(CONCAT_WS(\' \', pa.transaction_id, \''.$titleBy.'\', pa.remitter, pa.explanation)) AS description',
            ])
            ->from(['pa' => Payment::tableName()])
            ->andWhere([
                'or',
                ['pa.transaction_id' => $qw],
                ['pa.reference_number' => $qw],
                ['pa.payment_method' => $qw],
                '(pa.remitter ILIKE :search)',
                '(pa.explanation ILIKE :search)',
            ], ['search' => '%' . $q . '%']);
    }

    protected function searchInvoices()
    {
        $titleInvoice = Yii::t('backend/search', 'Invoice');
        $q = trim($this->q);
        $qw = preg_replace('/\s+/', '', $q);

        return (new Query())
            ->select([
                'i.id AS "id"',
                '\'' . SearchResultType::INVOICE . '\' AS "type"',
                'CONCAT_WS(\' \', \''.$titleInvoice.'\', i.number) AS title',
                'i.notes AS description',
            ])
            ->from(['i' => Invoice::tableName()])
            ->andWhere([
                'or',
                ['i.number' => $qw],
                ['i.reg_no' => $qw],
                '(i.customer ILIKE :search)',
                '(i.notes ILIKE :search)',
            ], ['search' => '%' . $q . '%']);
    }

    protected function searchShipments()
    {
        $q = trim($this->q);
        $qw = preg_replace('/\s+/', '', $q);

        return (new Query())
            ->select([
                'sh.id AS "id"',
                '\'' . SearchResultType::SHIPMENT . '\' AS "type"',
                'TRIM(CONCAT_WS(\' \', (sh.to_address #>> \'{"street"}\'), (sh.to_address #>> \'{"postcode"}\'), sh.tracking_number)) AS title',
                'CONCAT_WS(\' \', sh.method, sh.carrier_name) AS description',
            ])
            ->from(['sh' => Shipment::tableName()])
            ->andWhere([
                'or',
                ['sh.tracking_number' => $qw],
                ['sh.method' => $qw],
                ['sh.carrier_name' => $qw],
                ['sh.carrier_service' => $qw],
                ['sh.transaction_id' => $qw],
                ['sh.type' => $qw],
                ['(sh.to_address #>> \'{"postcode"}\')' => $qw],
                '((sh.to_address #>> \'{"name"}\') ILIKE :search)',
                '((sh.to_address #>> \'{"street"}\') ILIKE :search)',
                '((sh.to_address #>> \'{"city"}\') ILIKE :search)',
                '(sh.duration_terms ILIKE :search)',
            ], ['search' => '%' . $q . '%']);
    }
}
