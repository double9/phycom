<?php

namespace phycom\backend\models;

use phycom\common\models\traits\ModelTrait;
use phycom\common\events\ModelUpdateEvent;

use yii\db\ActiveRecord;
use yii\db\ActiveRecordInterface;
use yii\base\Model;
use yii;

/**
 * A Base class for a multi model form setup encapsulating a collection of models.
 *
 * Class ModelCollectionForm
 * @package phycom\backend\models
 *
 * @property ActiveRecordInterface[]|Model[] $models
 */
abstract class ModelCollectionForm extends Model
{
    const EVENT_AFTER_SAVE = 'afterSave';
    const EVENT_AFTER_CREATE_MODEL = 'afterCreateModel';
    const EVENT_BEFORE_SAVE_MODEL = 'beforeSaveModel';
    const EVENT_AFTER_SAVE_MODEL = 'afterSaveModel';
    const EVENT_AFTER_UPDATE_MODEL_ATTRIBUTES = 'afterUpdateModelAttributes';
    const EVENT_AFTER_POPULATE_MODELS = 'afterPopulateModels';

    const TEMPLATE_MODEL_KEY = '__tpl__';
    const NEW_MODEL_KEY = 'new';

    use ModelTrait;

    /**
     * @var ActiveRecordInterface[]|Model[]
     */
    private $models;


    protected $clearModelValidationErrors = true;


    public function rules()
    {
        return [
            ['models', 'safe']
        ];
    }

    /**
     * The class name of the model used in $this->models collection
     * @return string|ActiveRecordInterface|Model
     */
    abstract public function getModelClassName();

    /**
     * @return ActiveRecordInterface[]
     */
    abstract protected function loadSavedModels();

    /**
     * @return array
     * where array key is the model attribute name and array value is the attribute value
     */
    abstract protected function getRelationMap();

    /**
     * @return Model[]|ActiveRecordInterface[]
     */
    public function getModels()
    {
        if ($this->models === null) {
            $this->models = $this->loadSavedModels();
            $this->trigger(self::EVENT_AFTER_POPULATE_MODELS);
        }
        return $this->models;
    }

    /**
     * @return Model[]|ActiveRecordInterface[]
     */
    protected function getChangedModels()
    {
        $changed = [];
        foreach ($this->getModels() as $key => $model) {
            if ($this->hasChanged($model)) {
                $changed[$key] = $model;
            }
        }
        return $changed;
    }

    /**
     * @param Model $model
     * @return bool
     */
    protected function hasChanged(Model $model)
    {
        return true;
    }


    public function afterValidate()
    {
        $models = $this->getChangedModels();
        $valid = true;
        /* @var $model Model */
        foreach ($models as $model) {
            $valid = $model->validate(null, $this->clearModelValidationErrors) && $valid;
        }

        if (!$valid) {
            foreach ($models as $model) {
                if ($model->hasErrors()) {
                    $this->setErrors($model->errors, 'error');
                    break;
                }
            }
        }
        parent::afterValidate();
    }

    /**
     * @return bool
     * @throws yii\db\Exception
     * @throws \Throwable
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }
        $transaction = Yii::$app->db->beginTransaction();
        if (!$this->saveModels()) {
            $transaction->rollBack();
            return false;
        }
        $transaction->commit();
        $this->afterSave();
        return true;
    }

    /**
     * @return bool
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     */
    protected function saveModels()
    {
        $keep = [];
        foreach ($this->getChangedModels() as $key => $model) {

            if (!$this->beforeSaveModel($model, $key)) {
                continue;
            }
            if (!$model->save(false)) {
                return false;
            }
            if (!$this->afterSaveModel($model, $key)) {
                return false;
            }
            $keep[] = $model->getPrimaryKey();
        }
        foreach ($this->createDeleteQuery($keep)->all() as $model) {
            /**
             * @var ActiveRecord $model
             */
            $model->delete();
        }
        return true;
    }

    /**
     * @param array $exclude - array of model primary keys
     * @return yii\db\ActiveQueryInterface
     */
    protected function createDeleteQuery(array $exclude = [])
    {
        /**
         * @var ActiveRecordInterface|string
         */
        $ClassName = $this->getModelClassName();
        $query = $ClassName::find();
        if (!empty($this->getRelationMap())) {
            $query->where($this->getRelationMap());
        }
        if (!empty($exclude)) {
            $query->andWhere(['not in', $ClassName::primaryKey()[0], $exclude]);
        }
        return $query;
    }

    /**
     * Cleanup after all models are saved.
     */
    protected function afterSave()
    {
        if (!$this->clearModelValidationErrors) {
            $this->clearModelValidationErrors = true;
        }
        $this->models = null; // reset the model collection after successful save
        $this->trigger(self::EVENT_AFTER_SAVE);
    }

    /**
     * @param ActiveRecordInterface $model
     * @param mixed $key
     * @return bool
     */
    protected function beforeSaveModel(ActiveRecordInterface $model, $key)
    {
        foreach ($this->getRelationMap() as $attribute => $value) {
            $model->$attribute = $value;
        }
        $this->trigger(self::EVENT_BEFORE_SAVE_MODEL, new ModelUpdateEvent(['model' => $model, 'key' => $key]));
        return true;
    }

    /**
     * Called after model has been saved. Return value determines if the result is valid or we should discard the saved model
     * @param ActiveRecordInterface $model
     * @param mixed $key
     * @return bool
     */
    protected function afterSaveModel(ActiveRecordInterface $model, $key)
    {
        $this->trigger(self::EVENT_AFTER_SAVE_MODEL, new ModelUpdateEvent(['model' => $model, 'key' => $key]));
        return true;
    }

    /**
     * @param array|null $modelAttributes
     * @return array
     * @throws yii\base\InvalidConfigException
     */
    public function exportFormAttributes(array $modelAttributes = null)
    {
        $result = [];
        $formName = $this->formName();
        $index = 0;
        foreach ($this->getModels() as $modelKey => $model) {
            foreach ($model->getAttributes() as $attribute => $value) {
                if (null === $modelAttributes || in_array($attribute, $modelAttributes)) {
                    $result[$formName . "[models][$index][$attribute]"] = $this->exportModelAttribute($model, $attribute);
                }
            }

            $index++;
        }
        return $result;
    }

    /**
     * Exports single Model attribute
     *
     * @param ActiveRecordInterface $model
     * @param string $attribute
     *
     * @return mixed
     */
    protected function exportModelAttribute(ActiveRecordInterface $model, $attribute)
    {
        return (string) $model->$attribute;
    }

    /**
     * @param ActiveRecordInterface $model
     * @param string $attribute
     */
    protected function initModelAttribute(ActiveRecordInterface $model, $attribute)
    {

    }


    /**
     * @param array $models
     */
    public function setModels(array $models)
    {
        unset($models[static::TEMPLATE_MODEL_KEY]); // remove the hidden row used to generate rows
        $modelClassName = $this->getModelClassName();
        $result = [];

        foreach ($models as $key => $value) {
            if (is_array($value)) {

                if (!$model = $this->getModelByKey($key)) {
                    $model = $this->createModel($this->getRelationMap());
                }

                if ($this->clearModelValidationErrors) {
                    $model->clearErrors();
                    $this->clearModelValidationErrors = false;
                }

                $model->setAttributes($value);
                $this->afterUpdateModelAttributes($model, $value);
                $result[$key] = $model;

            } elseif ($value instanceof $modelClassName) {
                $modelKey = $value->getIsNewRecord() ? $key : $value->getPrimaryKey();
                $result[$modelKey] = $value;
            }
        }
        $this->models = $result;
    }

    /**
     * Called after model attributes are updated from user input
     *
     * @param ActiveRecordInterface|Model $model
     * @param array $attributes
     */
    protected function afterUpdateModelAttributes(ActiveRecordInterface $model, array $attributes = [])
    {
        foreach ($attributes as $attribute => $value) {
            $this->initModelAttribute($model, $attribute);
        }
        $this->trigger(self::EVENT_AFTER_UPDATE_MODEL_ATTRIBUTES, new ModelUpdateEvent(['model' => $model, 'attributes' => $attributes]));
    }

    /**
     * @param mixed $condition
     * @return ActiveRecordInterface
     */
    protected function findModelByCondition($condition)
    {
        return $this->getModelClassName()::findOne($condition);
    }

    /**
     * @param mixed $key
     * @return ActiveRecordInterface
     */
    protected function findModelByKey($key)
    {
        $Model = $this->getModelClassName();
        $primaryKeys = $Model::primaryKey();
        if (count($primaryKeys) > 1) {
            throw new yii\base\InvalidCallException('Composite primary keys are not supported');
        }
        $condition = $this->getRelationMap();
        $condition[$primaryKeys[0]] = $key;

        return $this->findModelByCondition($condition);
    }

    /**
     * Creates a new model
     *
     * @param array $attributes
     * @return ActiveRecordInterface
     */
    protected function createModel(array $attributes = [])
    {
        $modelClassName = $this->getModelClassName();
        /**
         * @var ActiveRecord $model
         */
        $model = new $modelClassName;
        $model->loadDefaultValues();

        if (!empty($attributes)) {
            $model->attributes = $attributes;
        }

        $this->afterCreateModel($model);
        return $model;
    }

    /**
     * @param ActiveRecordInterface $model
     */
    protected function afterCreateModel(ActiveRecordInterface $model)
    {
        $this->trigger(self::EVENT_AFTER_CREATE_MODEL, new ModelUpdateEvent(['model' => $model]));
    }

    /**
     * Finds the model from storage by key
     *
     * @param $key
     * @return ActiveRecordInterface|Model|null
     */
    protected function getModelByKey($key)
    {
        $model = $key && strpos($key, static::NEW_MODEL_KEY) === false ? $this->findModelByKey($key) : false;
        if (!$model) {
            $model = $this->getModels()[$key] ?? null;
        }
        return $model;
    }
}
