<?php

namespace phycom\backend\models;

use phycom\common\models\traits\ModelTrait;
use phycom\common\models\Shipment;
use yii\base\Model;

/**
 * Class ShipmentForm
 * @package phycom\backend\models
 *
 * @property-read Shipment $shipment
 */
class ShipmentForm extends Model
{
	use ModelTrait;

	protected $shipment;

	public function __construct(Shipment $shipment = null, array $config = [])
	{
		$this->shipment = $shipment ?: new Shipment();
		parent::__construct($config);
	}

	public function getShipment()
	{
		return $this->shipment;
	}
}