<?php

namespace phycom\backend\models;


use phycom\common\models\traits\ModelTrait;
use phycom\common\models\attributes\UserStatus;
use phycom\common\models\User;
use yii\base\Model;
use yii;

/**
 * Class RegistrationForm
 * @package phycom\backend\models
 */
class RegistrationForm extends Model
{
	use ModelTrait;

	public $username;
	public $password;
	public $repeatPassword;

    /**
     * @var array
     */
	protected $roles = [];
	/**
	 * @var User
	 */
	protected $user;

	/**
	 * RegistrationForm constructor.
	 * @param User $user
     * @param array $roles
	 * @param array $config
	 */
	public function __construct(User $user, array $roles, array $config = [])
	{
		$this->user = $user;
		$this->roles = $roles;
		parent::__construct($config);
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['password', 'username', 'repeatPassword'], 'required'],
			['username', 'string', 'min' => 4, 'max' => 32],
            ['username', 'unique',
                'targetClass' => get_class(Yii::$app->modelFactory->getUser()),
                'filter' => function ($query) {
                    /**
                     * @var yii\db\Query $query
                     */
                    return $query->andWhere(['not', ['status' => UserStatus::DELETED]]);
                },
                'message' => 'This username has already been taken.'
            ],
			['password', 'string', 'min' => 6, 'max' => 64],
			['repeatPassword', 'compare', 'compareAttribute' => 'password', 'message' => Yii::t('backend/error', 'Password and re-entered password do not match.')],
		];
	}


	public function attributeLabels()
	{
        return [
            'username'       => Yii::t('backend/user', 'Username'),
            'password'       => Yii::t('backend/user', 'Password'),
            'repeatPassword' => Yii::t('backend/user', 'Repeat Password'),
        ];
	}

	public function register()
	{
		if ($this->validate()) {

			$transaction = Yii::$app->db->beginTransaction();
			try {

				$this->user->username = $this->username;
				$this->user->setPassword($this->password);
				$this->user->status = UserStatus::ACTIVE;

				if (!$this->user->save()) {
					return $this->rollback($transaction, $this->user->errors);
				}

				if (!$this->user->email->setVerified()) {
					return $this->rollback($transaction, $this->user->email->errors);
				}

				foreach ($this->roles as $role) {
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($role), $this->user->id);
                }

				$transaction->commit();
				return true;

			} catch (\Exception $e) {
				$transaction->rollBack();
				throw $e;
			}
		}
		return false;
	}
}
