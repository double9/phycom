<?php

namespace phycom\backend\models;

use phycom\common\components\ActiveQuery;
use phycom\common\models\PartnerContract;
use phycom\common\models\traits\SearchQueryFilter;

use yii\data\ActiveDataProvider;

/**
 * SearchConsentContract represents the model behind the search form about `phycom\common\models\PartnerContract`.
 */
class SearchPartnerContract extends PartnerContract
{
    use SearchQueryFilter;

    public $contractStartFrom;
    public $contractStartTo;
    public $contractEndFrom;
    public $contractEndTo;
    public $createdFrom;
    public $createdTo;
    public $updatedFrom;
    public $updatedTo;

    const STATE_ACTIVE = 'active';
    const STATE_EXPIRED = 'expired';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'max_consent_days'], 'integer'],
            [['company_name', 'address', 'phone', 'contract_start', 'contract_end', 'consent_required_on', 'contact_email', 'category'], 'safe'],
            [['mandatory'], 'boolean'],
            [['createdFrom', 'createdTo', 'updatedFrom', 'updatedTo', 'contractEndFrom', 'contractEndTo', 'contractStartFrom', 'contractStartTo'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param string $state
     * @return ActiveDataProvider
     * @throws \yii\base\Exception
     */
    public function search(array $params, string $state = null)
    {
        /**
         * @var ActiveQuery $query
         */
        $query = static::find();

        $sortOrder = ['defaultOrder' => ['id' => SORT_DESC]];

        if ($state === static::STATE_ACTIVE) {
            $query->active();
        }

        if ($state === static::STATE_EXPIRED) {
            $query->expired();
            $sortOrder = ['defaultOrder' => ['contract_end' => SORT_DESC]];
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => $sortOrder
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'contract_start' => $this->contract_start,
            'contract_end' => $this->contract_end,
            'max_consent_days' => $this->max_consent_days,
            'mandatory' => $this->mandatory,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'company_name', $this->company_name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'consent_required_on', $this->consent_required_on])
            ->andFilterWhere(['like', 'contact_email', $this->contact_email])
            ->andFilterWhere(['like', 'category', $this->category]);

        $query->filterDaterange('contract_start', $this->contractStartFrom, $this->contractStartTo);
        $query->filterDaterange('contract_end', $this->contractEndFrom, $this->contractEndTo);

        return $dataProvider;
    }
}
