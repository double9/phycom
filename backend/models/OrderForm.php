<?php

namespace phycom\backend\models;


use phycom\common\models\traits\ModelTrait;
use phycom\common\models\Order;

use yii\base\Model;
use yii;

/**
 * Class OrderForm
 * @package phycom\backend\models
 *
 * @property-read Order $order
 */
class OrderForm extends Model
{
	use ModelTrait;

	public $promotionCode;
	public $status;

	protected $order;

	public function __construct(Order $order = null, array $config = [])
	{
		$this->order = $order ?: Yii::$app->modelFactory->getOrder();
		parent::__construct($config);
	}

	public function getOrder()
	{
		return $this->order;
	}
}