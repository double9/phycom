<?php

namespace phycom\backend\models;


use phycom\common\models\traits\ModelTrait;
use phycom\common\models\Shop;
use phycom\common\models\ShopSupply;

use yii\db\ActiveRecordInterface;
use yii;

/**
 * Class ShopSupplyScheduleForm
 * @package phycom\backend\models
 *
 * @property-read Shop $shop
 * @property-read ShopSupply[] $models
 */
class ShopSupplyScheduleCollectionForm extends ModelCollectionForm
{
    use ModelTrait;

    protected $shop;

    public function __construct(Shop $shop, array $config = [])
    {
        $this->shop = $shop;
        parent::__construct($config);
    }

    public function getWeekdays()
    {
        return [
            'mon' => Yii::t('backend/shop', 'Monday'),
            'tue' => Yii::t('backend/shop', 'Tuesday'),
            'wed' => Yii::t('backend/shop', 'Wednesday'),
            'thu' => Yii::t('backend/shop', 'Thursday'),
            'fri' => Yii::t('backend/shop', 'Friday'),
            'sat' => Yii::t('backend/shop', 'Saturday'),
            'sun' => Yii::t('backend/shop', 'Sunday')
        ];
    }

    public function getModelClassName()
    {
        return ShopSupply::class;
    }

    protected function loadSavedModels()
    {
        if ($this->shop->isNewRecord) {
            return [];
        }
        $models = [];
        for ($day=1; $day<=7; $day++) {

            $model = ShopSupply::findOne(['shop_id' => $this->shop->id, 'day_of_week' => $day]);

            if (!$model) {
                $model = $this->createModel();
                $model->day_of_week = $day;
                $models[static::NEW_MODEL_KEY . $day] = $model;
            } else {
                $models[$model->id] = $model;
            }
        }
        return $models;
    }

    /**
     * @param ShopSupply|ActiveRecordInterface $model
     * @param array $attributes
     */
    protected function afterUpdateModelAttributes(ActiveRecordInterface $model, array $attributes = [])
    {
        $this->formatTime($model, 'dispatched_at');
        $this->formatTime($model, 'delivery_at');
        $model->delivery = (bool) $model->delivery;
    }

    /**
     * @param ShopSupply $model
     * @param $attribute
     */
    private function formatTime($model, $attribute)
    {
        $value = $model->$attribute;
        if (!$value instanceof \DateTime) {
            if (is_string($value) && strlen($value)) {
                $dateTime = \DateTime::createFromFormat('H:i', $value, new \DateTimeZone('UTC'));
                if ($dateTime) {
                    $model->$attribute = $dateTime;
                    return;
                }
            }
            if (empty($value)) {
                $model->addError($attribute, Yii::t('backend/shop', '{attribute} cannot be empty', ['attribute' => $model->getAttributeLabel($attribute)]));
            } else {
                $model->addError($attribute, Yii::t('backend/shop', 'Invalid attribute format. {format} expected. ', ['format' => 'HH:MM']));
            }
        }
    }

    /**
     * @param ShopSupply|ActiveRecordInterface $model
     * @param mixed $key
     * @return bool
     */
    protected function beforeSaveModel(ActiveRecordInterface $model, $key)
    {
        return parent::beforeSaveModel($model, $key) && $model->delivery;
    }

    protected function getRelationMap()
    {
        return ['shop_id' => $this->shop->id];
    }

    protected function createModel(array $attributes = [])
    {
        /**
         * @var ShopSupply $model
         */
        $model = parent::createModel($attributes);
        if (!$this->shop->isNewRecord) {
            $model->shop_id = $this->shop->id;
            $model->populateRelation('shop', $this->shop);
        }
        return $model;
    }


}
