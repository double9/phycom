<?php

namespace phycom\backend\models;

use phycom\common\models\traits\ModelTrait;
use phycom\common\models\PostAttachment;

use yii\base\Model;
use yii;

/**
 * Class PostAttachmentForm
 * @package phycom\backend\models
 *
 * @property array $meta
 *
 * @property-read PostAttachment $attachment
 */
class PostAttachmentOptionsForm extends Model
{
	use ModelTrait;

	public $meta = [];

	protected $attachment;

	public function __construct(PostAttachment $attachment, array $config = [])
	{
		$this->attachment = $attachment;
		parent::__construct($config);
	}

	public function init()
    {
        parent::init();
        $this->meta = $this->attachment->meta ?: [];
    }

    public function rules()
	{
		return [
            ['meta', 'safe']
		];
	}

	public function getAttachment()
	{
		return $this->attachment;
	}

	public function save()
    {
        if ($this->validate()) {

            $meta = $this->attachment->meta ?: [];
            foreach ($this->meta as $key => $value) {
                $meta[$key] = $value;
            }
            foreach ($meta as $key => $value) {
                if (is_string($value)) {
                    $value = trim($value);
                    if (!$value) {
                        unset($meta[$key]);
                    }
                }
            }
            $this->attachment->meta = !empty($meta) ? $meta : null;
            return $this->attachment->save();
        }
        return false;
    }
}