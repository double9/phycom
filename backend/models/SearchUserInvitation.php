<?php

namespace phycom\backend\models;

use phycom\common\models\attributes\UserStatus;
use phycom\common\models\attributes\UserTokenType;
use phycom\common\models\UserToken;
use yii\db\Query;
use yii;

/**
 * Class SearchUserInvitation
 * @package phycom\backend\models
 */
class SearchUserInvitation extends SearchUser
{
	public $tokenCount;

	public function rules()
	{
		$rules = parent::rules();
		$rules[] = [['tokenCount'],'safe'];
		return $rules;
	}

	public function attributeLabels()
	{
		$labels = parent::attributeLabels();
		$labels['tokenCount'] = Yii::t('backend/user', 'Invitations');
		return $labels;
	}

	protected function createSearchQuery()
	{
		$query = parent::createSearchQuery();
		$query->addSelect([
			'token.count as tokenCount'
		]);
		$query->leftJoin(['token' => 'LATERAL('.
			(new Query())
				->select('count(*) as count')
				->from(['t' => UserToken::tableName()])
				->where('t.user_id = u.id AND t.type = :token_type')
				->createCommand()
				->sql
			.')'
		], 'true', ['token_type' => UserTokenType::REGISTRATION_REQUEST]);
		$query->addGroupBy(['token.count']);
		$query->andWhere(['u.status' => UserStatus::PENDING]);
		return $query;
	}
}