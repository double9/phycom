<?php

namespace phycom\backend\models;

use phycom\common\helpers\Diacritics;
use phycom\common\models\attributes\PostType;
use phycom\common\models\traits\ModelTrait;
use phycom\common\models\Address;
use phycom\common\models\attributes\AddressType;
use phycom\common\models\attributes\ContactAttributeStatus;
use phycom\common\models\attributes\ShopStatus;
use phycom\common\models\Email;
use phycom\common\helpers\PhoneHelper as PhoneHelper;
use phycom\common\models\Phone;
use phycom\common\models\Shop;
use phycom\common\modules\delivery\interfaces\DeliveryMethodInterface;
use phycom\common\modules\delivery\methods\DeliveryMethod;
use phycom\common\modules\delivery\methods\selfPickup\models\ServiceLevel;
use phycom\common\modules\delivery\models\DeliveryArea;
use phycom\common\modules\delivery\models\DeliveryAreaStatus;
use phycom\common\modules\delivery\Module as DeliveryModule;
use phycom\common\validators\PhoneInputValidator;

use yii\base\Model;
use yii;

/**
 * Class ShopForm
 *
 * @package phycom\backend\models
 *
 * @property-read Shop $shop
 * @property-read ShopAddressForm $addressForm
 * @property-read ShopOpenScheduleCollectionForm $openForm
 * @property-read ShopClosedScheduleForm $closedForm
 * @property-read ShopSupplyScheduleCollectionForm $supplyForm
 * @property-read PostForm $contentForm
 */
class ShopForm extends Model
{
	use ModelTrait;

	public $name;
	public $vendorId;
	public $status;
	public $settings;
	public $email;
	public $phone;
	public $phoneNumber;
	public $address;
	public $deliveryArea;

	public $defaultClosedMessage;
	public $closedMessage;
	public $courierStart;
	public $courierEnd;

	protected $shop;
    protected $openForm;
	protected $closedForm;
	protected $supplyForm;
	protected $contentForm;

	public function __construct(Shop $shop = null, array $config = [])
	{
		$this->shop = $shop ?: Yii::$app->modelFactory->getShop(['vendor_id' => Yii::$app->vendor->id, 'status' => ShopStatus::INACTIVE]);
		parent::__construct($config);
	}

	public function init()
	{
		parent::init();
		$this->name = $this->shop->name;
		$this->vendorId = !$this->shop->isNewRecord ? $this->shop->vendor_id : Yii::$app->vendor->id;
		$this->status = (string) $this->shop->status;
		$this->email = $this->shop->email->email ?? null;
		$this->phone = $this->shop->phone->phone_nr ?? null;
		$this->phoneNumber = $this->shop->phone->fullNumber ?? null;
		$this->address = $this->shop->address ? (string) $this->shop->address->export() : null;
		$this->deliveryArea = $this->shop->settings && isset($this->shop->settings['deliveryArea']) ? '1' : '0';
	}

	public function getShop()
	{
		return $this->shop;
	}


	public function rules()
	{
		return [
			[['name'], 'trim'],

			[['name','status','vendorId','phone'], 'required'],
			[['name','status','email','phone'], 'string', 'max' => 255],
			[['vendorId'], 'integer'],

			[['email'], 'trim'],
			[['email'], 'email'],

			[['phone'], 'trim'],
			[['phone'], PhoneInputValidator::class],
			[['phoneNumber'], 'string'],

            ['address', 'safe'],
            ['deliveryArea', 'boolean'],

			[['closedMessage', 'defaultClosedMessage'], 'trim'],
			[['closedMessage', 'defaultClosedMessage', 'courierStart', 'courierEnd'], 'string'],
//			['defaultClosedMessage', 'required'],
		];
	}

	public function attributeLabels()
	{
        return [
            'name'         => Yii::t('backend/shop', 'Name'),
            'vendorId'     => Yii::t('backend/shop', 'Vendor'),
            'status'       => Yii::t('backend/shop', 'Status'),
            'settings'     => Yii::t('backend/shop', 'Settings'),
            'email'        => Yii::t('backend/shop', 'Email'),
            'phone'        => Yii::t('backend/shop', 'Phone'),
            'address'      => Yii::t('backend/shop', 'Address'),
            'deliveryArea' => Yii::t('backend/shop', 'Is delivery area'),
        ];
	}

	public function getAddressForm()
    {
        if ($this->shop->address) {
            $addressForm = new ShopAddressForm($this->shop);
            $addressForm->populate($this->shop->address);
            return $addressForm;
        } else {
            return new ShopAddressForm($this->shop);
        }
    }

    public function getOpenForm()
    {
        if (!$this->openForm) {
            $this->openForm = new ShopOpenScheduleCollectionForm($this->shop);
        }
        return $this->openForm;
    }

    public function getClosedForm()
    {
        if (!$this->closedForm) {
            $this->closedForm = new ShopClosedScheduleForm($this->shop);
        }
        return $this->closedForm;
    }

    public function getSupplyForm()
    {
        if (!$this->supplyForm) {
            $this->supplyForm = new ShopSupplyScheduleCollectionForm($this->shop);
        }
        return $this->supplyForm;
    }

    public function getContentForm()
    {
        if (!$this->contentForm) {
            $this->contentForm = new PostForm(new PostType(PostType::SHOP), $this->shop->content);
            $this->contentForm->shopId = $this->shop->id;
        }
        return $this->contentForm;
    }

	public function update()
	{
		if ($this->validate()) {

		    $transaction = Yii::$app->db->beginTransaction();
		    try {

                $this->shop->name = $this->name;
                $this->shop->status = new ShopStatus($this->status);
                if (!$this->shop->save()) {
                    return $this->rollback($transaction, $this->shop->errors);
                }

                /**
                 * Update shop address
                 */
                if ($address = $this->shop->address) {
                    $address->updateJson($this->address);
                } else {
                    $address = Address::create($this->address);
                    $address->type = AddressType::MAIN;
                    $address->status = ContactAttributeStatus::UNVERIFIED;
                    $address->shop_id = $this->shop->id;
                }
                if (!$address->save()) {
                    return $this->rollback($transaction, $address->errors);
                }

                if ($address) {
                    $this->deliveryArea ?
                        $this->updateDeliveryArea($address) :
                        $this->revokeDeliveryArea($address);
                }

                if ($this->email) {
                    /**
                     *  Update shop email
                     */
                    if (!$email = $this->shop->email) {
                        $email = new Email();
                        $email->status = ContactAttributeStatus::UNVERIFIED;
                        $email->shop_id = $this->shop->id;
                    }
                    $email->email = $this->email;
                    if (!$email->save()) {
                        return $this->rollback($transaction, $email->errors);
                    }
                }

                /**
                 *  Update shop phone
                 */
                if (!$phone = $this->shop->phone) {
                    $phone = new Phone();
                    $phone->status = ContactAttributeStatus::UNVERIFIED;
                    $phone->shop_id = $this->shop->id;
                }
                $phone->phone_nr = PhoneHelper::getNationalPhoneNumber($this->phoneNumber);
                $phone->country_code = PhoneHelper::getPhoneCode($this->phoneNumber);
                if (!$phone->save()) {
                    return $this->rollback($transaction, $phone->errors);
                }

                $transaction->commit();
                return true;

		    } catch (\Exception $e) {
		        $transaction->rollBack();
		        throw $e;
            }
		}
		return false;
	}

	protected function updateDeliveryArea(Address $address)
    {
        if (!$area = $this->findDeliveryArea()) {
            $area = new DeliveryArea();
        }

        $addressField = $address->export();
        $addressField->name = $this->shop->name;

        $area->method = DeliveryModule::METHOD_SELF_PICKUP;
        $area->area = $addressField->toArray();
        $area->service = 'default';
        $area->price = 0;
        $area->status = DeliveryAreaStatus::ACTIVE;

        $area->area_code = $area->generateAreaCode();
        $area->code = $area->generateCode();

        if (!$area->save()) {
            Yii::error('Error saving shop ' . $this->shop->id . ' delivery area: ' . json_encode($area->errors), __METHOD__);
            return false;
        }
        $this->shop->settings->deliveryArea = $area->id;
        return $this->shop->update(false, ['settings']);
    }

    protected function revokeDeliveryArea(Address $address)
    {
        if ($area = $this->findDeliveryArea()) {
            $area->status = DeliveryAreaStatus::DISABLED;
            if ($area->update()) {
                return true;
            }
            Yii::error('Error revoking shop ' . $this->shop->id . ' delivery area ' . $area->id . ': ' . json_encode($area->errors), __METHOD__);
        }
        return false;
    }

    /**
     * @return null|DeliveryArea
     */
    private function findDeliveryArea()
    {
        if ($this->shop->settings->deliveryArea) {
            return DeliveryArea::findOne(['id' => $this->shop->settings->deliveryArea]);
        }
        return null;
    }
}
