<?php

namespace phycom\backend\models;

use phycom\common\components\ActiveQuery;
use phycom\common\models\traits\SearchQueryFilter;
use phycom\common\interfaces\SearchModelInterface;
use phycom\common\models\Invoice;
use phycom\common\models\InvoiceItem;
use phycom\common\models\Order;
use phycom\common\models\OrderItem;

use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\Expression;
use yii;

/**
 * Class SearchInvoice
 * @package phycom\backend\models
 *
 * @property string $fileUrl
 */
class SearchInvoice extends Invoice implements SearchModelInterface
{
	use SearchQueryFilter;

	public $total;
	public $orderNumber;

	public $createdFrom;
	public $createdTo;

	public function rules()
	{
		return [
			[['id', 'order_id'], 'integer'],
			[['address'], 'string'],
			[['number', 'customer', 'customer_type', 'reg_no'], 'string', 'max' => 255],
			[['created_at'], 'safe'],
			[['createdFrom','createdTo','total','orderNumber', 'status'],'safe']
		];
	}

	public function attributeLabels()
	{
		$labels = parent::attributeLabels();
		$labels['orderNumber'] = Yii::$app->modelFactory->getOrder()->getAttributeLabel('number');
        $labels['total'] = Yii::t('backend/invoice', 'Total');
		return $labels;
	}

	/**
	 * Creates data provider instance with search query applied
	 * @param array $params
	 * @return ActiveDataProvider
	 */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
			'pagination' => [
				'pageSize' => 20
			],
		]);

		$this->sort($dataProvider->sort);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'i.id' => $this->id,
			'i.number' => $this->number,
			'i.status' => (string)$this->status,
		]);

		if ($this->total) {
			$query->andHaving('sum(oi.total) = :total', ['total' => $this->total]);
		}
		$query->andFilterWhere(['like', 'o.number', $this->orderNumber]);
		$query->filterText('i.customer', $this->customer);
		$query->filterDateRange('i.created_at', $this->createdFrom, $this->createdTo);
		return $dataProvider;
	}

	protected function sort(Sort $sort)
	{
		$sort->attributes['orderNumber'] = [
			'asc' => ['o.number' => SORT_ASC],
			'desc' => ['o.number' => SORT_DESC],
		];
		$sort->attributes['total'] = [
			'asc' => [new Expression('sum(oi.total) ASC NULLS FIRST')],
			'desc' => [new Expression('sum(oi.total) DESC NULLS LAST')]
		];
	}

	/**
	 * @return ActiveQuery
	 */
	protected function createSearchQuery()
	{
		$query = static::find();
		$query->select([
			'i.*',
			'sum(oi.total) AS total',
			'o.number AS orderNumber'
		]);
		$query->from(['i' => Invoice::tableName()]);

		$query->innerJoin(['o' => Order::tableName()], 'i.order_id = o.id');
		$query->leftJoin(['ii' => InvoiceItem::tableName()], 'ii.invoice_id = i.id');
		$query->leftJoin(['oi' => OrderItem::tableName()], 'oi.id = ii.order_item_id');
		$query->groupBy(['i.id', 'o.number']);

		return $query;
	}
}