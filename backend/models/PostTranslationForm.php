<?php

namespace phycom\backend\models;

use phycom\common\models\Post;
use phycom\common\models\translation\PostTranslation;

use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use yii;

/**
 * Class PostTranslationForm
 * @package phycom\backend\models
 *
 * @property PostTranslation[] $models
 * @property PostTranslation[] $changedTranslations
 * @property-read Post $post
 */
class PostTranslationForm extends ModelCollectionForm
{
    protected $post;

    public function __construct(Post $post, array $config = [])
    {
        $this->post = $post;
        parent::__construct($config);
    }

    public function getPost()
    {
        return $this->post;
    }

    public function getModelClassName()
    {
        return PostTranslation::class;
    }

    protected function getRelationMap()
    {
        return ['post_id' => $this->post->id];
    }

    public function afterValidate()
    {
        $translations = $this->getChangedModels();
        if (empty($translations)) {
            $this->addError('translations', Yii::t('backend/error', 'Empty {type} content. At least one language should not be empty', ['type' => lcfirst($this->post->type->label)]));
        } else if (!Model::validateMultiple($translations)) {
            $this->addError(null); // add an empty error to prevent saving
        }
        parent::afterValidate();
    }

    /**
     * @return array|yii\db\ActiveRecordInterface[]
     */
    protected function loadSavedModels()
    {
        $translations = $this->post->isNewRecord ? [] : ArrayHelper::index($this->post->translations, 'language');
        foreach (Yii::$app->lang->enabled as $language) {
            if (!isset($translations[$language->code])) {
                $translations[$language->code] = $this->createModel(['language' => $language->code]);
            }
        }
        $translationsSorted = [];
        foreach (Yii::$app->lang->enabled as $language) {
            $translationsSorted[$language->code] = $translations[$language->code];
        };
        return $translationsSorted;
    }

    /**
     * @param PostTranslation|Model $model
     * @return bool
     */
    protected function hasChanged(Model $model)
    {
        return $model->title || $model->content || !$model->isNewRecord;
    }

    protected function findModelByKey($key)
    {
        return $this->getModelClassName()::findOne(['post_id' => $this->post->id, 'language' => $key]);
    }

    /**
     * @param array $attributes
     * @return PostTranslation
     */
    protected function createModel(array $attributes = [])
    {
        /**
         * @var PostTranslation|yii\db\ActiveRecord $model
         */
        $model = parent::createModel($attributes);

        // add some custom metadata when available
        if (isset($attributes['link'])) {
            $model->meta = ['link' => $attributes['link']];
        }

        if (!$this->post->isNewRecord) {
            $model->post_id = $this->post->id;
            $model->populateRelation('post', $this->post);
        }
        return $model;
    }
}
