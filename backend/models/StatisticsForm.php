<?php

namespace phycom\backend\models;

use yii\base\Model;
use yii;

/**
 * Class StatisticsForm
 * @package phycom\backend\models
 */
class StatisticsForm extends Model
{
    public $from;
    public $to;

    public function rules()
    {
        return [
            [['from','to'],'date', 'format' => 'php:Y-m-d']
        ];
    }

    public function attributeLabels()
    {
        return [
            'from' => Yii::t('backend/statistics', 'From'),
            'to' => Yii::t('backend/statistics', 'To')
        ];
    }

    /**
     * @return \DateTime|null
     * @throws \Exception
     */
    public function getFromDate()
    {
        return !empty($this->from) ? new \DateTime($this->from) : null;
    }

    /**
     * @return \DateTime|null
     * @throws \Exception
     */
    public function getToDate()
    {
        return !empty($this->to) ? new \DateTime($this->to) : null;
    }
}