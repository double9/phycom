<?php

namespace phycom\backend\models;

use phycom\common\components\ActiveQuery;
use phycom\common\models\traits\SearchQueryFilter;
use phycom\common\interfaces\SearchModelInterface;
use phycom\common\models\attributes\OrderStatus;
use phycom\common\models\Order;
use phycom\common\models\OrderItem;
use phycom\common\models\User;

use yii\db\Expression;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii;


/**
 * Class SearchOrder
 * @package phycom\backend\models
 */
class SearchOrder extends Order implements SearchModelInterface
{
	use SearchQueryFilter;

	public $customer;
	public $total;

	public $createdFrom;
	public $createdTo;
	public $updatedFrom;
	public $updatedTo;
	public $paidFrom;
	public $paidTo;

	public $hiddenStatus = [OrderStatus::DELETED, OrderStatus::EXPIRED];

	public function rules()
	{
		return [
			[['id', 'user_id', 'shop_id'], 'integer'],
			[['comment', 'customer'], 'string'],
			[['paid_at', 'created_at', 'updated_at'], 'safe'],
			[['number', 'promotion_code'], 'string', 'max' => 255],
			[['createdFrom','createdTo','updatedFrom','updatedTo','paidFrom','paidTo','total','status'],'safe']
		];
	}

	public function attributeLabels()
	{
		$labels = parent::attributeLabels();
        $labels['customer'] = Yii::t('backend/order', 'Customer');
        $labels['total'] = Yii::t('backend/order', 'Total');
		return $labels;
	}

    /**
     * @param array $params
     * @return ActiveDataProvider
     * @throws yii\base\Exception
     */
	public function recent(array $params = [])
    {
        $query = $this->createSearchQuery();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
            'pagination' => ['pageSize' => 10]
        ]);
        $query->andWhere('o.created_at > \'now\'::timestamp - \'1 week\'::interval');

        if (!empty($this->hiddenStatus)) {
            if (!$this->status || !$this->status->in($this->hiddenStatus)) {
                $query->andWhere(['not', ['o.status' => $this->hiddenStatus]]);
            }
        }
        $this->load($params);
        return $dataProvider;
    }

	/**
	 * Creates data provider instance with search query applied
	 * @param array $params
	 * @return ActiveDataProvider
     * @throws yii\base\Exception
	 */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
			'pagination' => [
				'pageSize' => 20
			],
		]);

		$this->sort($dataProvider->sort);

		if ($this->user_id) {
			$query->andWhere(['o.user_id' => $this->user_id]);
		}

		if (!($this->load($params) && $this->validate())) {
			if (!empty($this->hiddenStatus)) {
				$query->andWhere(['not', ['o.status' => $this->hiddenStatus]]);
			}
			return $dataProvider;
		}

		if (!empty($this->hiddenStatus)) {
			if (!$this->status || !$this->status->in($this->hiddenStatus)) {
				$query->andWhere(['not', ['o.status' => $this->hiddenStatus]]);
			}
		}

		$query->andFilterWhere([
			'o.id' => $this->id,
			'o.number' => $this->number,
			'o.status' => (string)$this->status,
		]);

		if ($this->total) {
			$query->andHaving('sum(oi.total) = :total', ['total' => $this->total]);
		}

		$query->filterFullName(['o.first_name', 'o.last_name'], $this->customer);
		$query->filterDateRange('o.created_at', $this->createdFrom, $this->createdTo);
		$query->filterDateRange('o.updated_at', $this->updatedFrom, $this->updatedTo);
		$query->filterDateRange('o.paid_at', $this->paidFrom, $this->paidTo);

		return $dataProvider;
	}

	protected function sort(Sort $sort)
	{
		$sort->attributes['customer'] = [
			'asc' => ['o.first_name' => SORT_ASC, 'o.last_name' => SORT_ASC],
			'desc' => ['o.first_name' => SORT_DESC, 'o.last_name' => SORT_DESC],
		];
		$sort->attributes['total'] = [
			'asc' => [new Expression('sum(oi.total) ASC NULLS FIRST')],
			'desc' => [new Expression('sum(oi.total) DESC NULLS LAST')]
		];
	}
	
	/**
	 * Creates data provider instance with search query applied
	 * @param mixed|array $statuses
	 * @return ActiveDataProvider
     * @throws yii\base\Exception
	 */
	public function searchByStatus($statuses)
	{
		$query = $this->createSearchQuery();
		$dataProvider = new ActiveDataProvider([
				'query' => $query,
				'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
				]);
	
		$query->andFilterWhere(['o.status' => $statuses]);
	
		return $dataProvider;
	}	

    /**
     * @return ActiveQuery
     * @throws yii\base\Exception
     */
	protected function createSearchQuery()
	{
		$query = static::find();
		$query->select([
			'TRIM(CONCAT_WS(\' \', o.first_name, o.last_name)) as customer',
			'sum(oi.total) AS total',
			'o.*'
		]);
		$query->from(['o' => Order::tableName()]);
		$query->leftJoin(['u' => User::tableName()], 'o.user_id = u.id');
		$query->leftJoin(['oi' => OrderItem::tableName()], 'oi.order_id = o.id');
		$query->groupBy([
			'o.id',
		]);
		return $query;
	}
}