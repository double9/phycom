<?php

namespace phycom\backend\models;

use phycom\common\models\traits\ModelTrait;
use phycom\common\models\UserActivity;
use phycom\common\validators\PhoneInputValidator;
use phycom\common\helpers\c;
use phycom\common\helpers\Date;
use phycom\common\models\attributes\Boolean;
use phycom\common\models\attributes\ContactAttributeStatus;
use phycom\common\models\Country;
use phycom\common\models\Email;
use phycom\common\models\Language;
use phycom\common\models\Phone;
use phycom\common\models\User;
use yii\base\Model;
use yii;

/**
 * Class UserProfileForm
 * @package phycom\backend\model
 *
 * @property-read User $user
 * @property-read array $timezones
 * @property-read array $languages
 */
class UserProfileForm extends Model
{
	use ModelTrait;

	public $firstName;
	public $lastName;
	public $companyName;
	public $email;
	public $phoneCode;
	public $phone;
	public $birthday;
	public $language;
	public $timezone;
	public $advertise;

	public $receiveOrderNotifications;
	public $receiveErrorNotifications;

	/**
	 * @var User
	 */
	protected $user;

	public function __construct(User $user, array $config = [])
	{
		$this->user = $user;
		$this->initUser();
		parent::__construct($config);
	}

	/**
	 * Sets the model attributes from the user model
	 */
	public function initUser()
	{
		$this->firstName = $this->user->first_name;
		$this->lastName = $this->user->last_name;
		$this->companyName = $this->user->company_name;
		$this->birthday = $this->user->birthday ? Date::create($this->user->birthday)->toDateStr() : null;
		$this->language = $this->user->settings->language;
		$this->timezone = $this->user->settings->timezone;
		$this->advertise = (string)(new Boolean($this->user->settings->advertise));
		$this->email = (string)$this->user->email;
		$this->phoneCode = $this->user->phone ? $this->user->phone->country_code : Yii::$app->formatter->country->phonecode;
		$this->phone = (string)$this->user->phone;
		$this->receiveOrderNotifications = $this->user->settings->receiveOrderNotifications;
		$this->receiveErrorNotifications = $this->user->settings->receiveErrorNotifications;
	}

	public function rules()
	{
		return [
			[['firstName','lastName', 'email'], 'required'],
			[['firstName','lastName','companyName', 'email', 'phone'], 'string', 'max' => 255],
			['birthday', 'date'],
			['email', 'email'],
			['email',
				'unique',
				'targetClass' => Email::class,
				'targetAttribute' => 'email',
				'filter' => function ($query) {
					/**
					 * @var yii\db\QueryInterface $query
					 */
					$query
						->andWhere('user_id IS NOT NULL')
						->andWhere(['not', ['user_id' => $this->user->id]])
						->andWhere(['not', ['status' => ContactAttributeStatus::DELETED]]);
				}
			],
			[['phoneCode'], 'string', 'max' => 4],
			[['phone'], 'string'],
			[['phone'], PhoneInputValidator::class, 'region' => $this->getPhonecodeRegion()],
			[['firstName','lastName','companyName','phone', 'email'], 'trim'],
			['language', 'string', 'length' => 5],
			['language', 'exist', 'targetClass' => Language::class, 'targetAttribute' => 'code', 'filter' => function ($query) {
				/**
				 * @var yii\db\Query $query
				 */
				$query->where(['code' => substr($this->language, 0, 2)]);
			}],
			['timezone', 'string'],
			[['advertise', 'receiveOrderNotifications', 'receiveErrorNotifications'], 'boolean']
		];
	}

	public function attributeLabels()
	{
		return [
			'firstName' => $this->user->getAttributeLabel('first_name'),
			'lastName' => $this->user->getAttributeLabel('last_name'),
			'birthday' => $this->user->getAttributeLabel('birthday'),
			'language' => $this->user->settings->getAttributeLabel('language'),
			'timezone' => $this->user->settings->getAttributeLabel('timezone'),
			'advertise' => $this->user->settings->getAttributeLabel('advertise'),
            'receiveOrderNotifications' => $this->user->settings->getAttributeLabel('receiveOrderNotifications'),
            'receiveErrorNotifications' => $this->user->settings->getAttributeLabel('receiveErrorNotifications'),
			'email' => Yii::t('backend/user', 'Email'),
			'phone' => Yii::t('backend/user', 'Phone'),
		];
	}

	public function getUser()
    {
        return $this->user;
    }

	public function getTimezones()
	{
		$timezones = \DateTimeZone::listIdentifiers(\DateTimeZone::ALL);
		return array_combine($timezones, $timezones);
	}

	public function getLanguages()
	{
		$query = Language::find()->where(['code' => c::param('languages')]);
		$languages = [];
		foreach ($query->all() as $language) {
			/**
			 * @var Language $language
			 */
			$languages[$language->ietf] = $language->name . ' (' . $language->native .')';
		}
		return $languages;
	}

	public function getPhonecodeRegion()
	{
		$country = Country::findOneByPhoneCode($this->phoneCode);
		if (!$country) {
			$this->addError('phone', Yii::t('backend/user', 'Invalid phone code {phonecode}', ['phonecode' => $this->phoneCode]));
		}
		return $country->code;
	}

	/**
	 * @return bool
	 * @throws \Exception
	 */
	public function update()
	{
		if ($this->validate()) {
			$transaction = Yii::$app->db->beginTransaction();
			$prevLanguage = $this->user->settings->language;
			try {
				$this->user->first_name = $this->firstName;
				$this->user->last_name = $this->lastName;
				$this->user->company_name = $this->companyName;
				if ($this->birthday) {
                    $this->user->birthday = Date::create($this->birthday)->dateTime;
                }
				$this->user->settings->language = $this->language;
				$this->user->settings->timezone = $this->timezone;
				$this->user->settings->advertise = Boolean::create($this->advertise)->value;
				$this->user->settings->receiveOrderNotifications = Boolean::create($this->receiveOrderNotifications)->value;
				$this->user->settings->receiveErrorNotifications = Boolean::create($this->receiveErrorNotifications)->value;

				if (!$this->updateEmail()) {
					$transaction->rollBack();
					return false;
				}

				if (!$this->updatePhone()) {
					$transaction->rollBack();
					return false;
				}

				if (!$this->user->save()) {
					$this->setErrors($this->user->settings->errors);
					$this->setErrors($this->user->errors);
					Yii::error('Error updating user profile: ' . json_encode($this->errors), __METHOD__);
					$transaction->rollBack();
					return false;
				}
			} catch (\Exception $e) {
				$transaction->rollBack();
				throw $e;
			}

			if ($prevLanguage != $this->language) {
				Yii::$app->language = $this->language;
			}
			$transaction->commit();
			if ($this->user->id === Yii::$app->user->id) {
				UserActivity::create('Updated profile');
			} else {
				UserActivity::create('Updated user ' . $this->user->id . ' profile');
			}
			return true;
		}
		return false;
	}

	/**
	 * @return bool
	 */
	protected function updateEmail()
	{
		if (!$email = $this->findEmail($this->email)) {
			$email = new Email([
				'user_id' => $this->user->id,
				'email' => $this->email
			]);
		}
		$email->status = ContactAttributeStatus::UNVERIFIED;
		$email->updated_at = Date::now();
		if (!$email->save()) {
			$this->setErrors($email->errors);
			return false;
		}
		return true;
	}

	/**
	 * @param string $email
	 * @return Email|null
	 */
	protected function findEmail($email)
	{
		return Email::find()
			->where([
				'email' => $email,
				'user_id' => $this->user->id
			])
			->andWhere(['not', ['status' => ContactAttributeStatus::DELETED]])
			->one();
	}

	/**
	 * @return bool
	 */
	protected function updatePhone()
	{
		if (!$phone = $this->findPhone($this->phoneCode, $this->phone)) {
			$phone = new Phone([
				'user_id' => $this->user->id,
				'phone_nr' => $this->phone,
				'country_code' => $this->phoneCode
			]);
		}
		$phone->status = ContactAttributeStatus::UNVERIFIED;
		$phone->updated_at = Date::now();
		if (!$phone->save()) {
			$this->setErrors($phone->errors);
			return false;
		}
		return true;
	}

	/**
	 * @param string $phoneCode
	 * @param string $phoneNr
	 * @return Phone|null
	 */
	protected function findPhone($phoneCode, $phoneNr)
	{
		return Phone::find()
			->where([
				'country_code' => $phoneCode,
				'phone_nr' => $phoneNr,
				'user_id' => $this->user->id
			])
			->andWhere(['not', ['status' => ContactAttributeStatus::DELETED]])
			->one();
	}
}