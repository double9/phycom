<?php

namespace phycom\backend\models;

use phycom\common\models\traits\ModelTrait;
use phycom\common\models\Address;
use phycom\common\models\attributes\AddressType;
use phycom\common\models\attributes\ContactAttributeStatus;
use phycom\common\models\attributes\VendorStatus;
use phycom\common\models\Email;
use phycom\common\helpers\PhoneHelper as PhoneHelper;
use phycom\common\models\Phone;
use phycom\common\models\Vendor;
use phycom\common\validators\PhoneInputValidator;

use yii\base\Model;
use yii;

/**
 * Class VendorForm
 * @package phycom\backend\models
 *
 * @property-read Vendor $vendor
 * @property-read VendorAddressForm $addressForm
 * @property-read array $statuses
 */
class VendorForm extends Model
{
	use ModelTrait;

	public ?string $name;
    public ?string $legalName;
    public ?string $regNumber;
    public ?string $vatNumber;
	public ?string $status;
	public ?string $email;
	public ?string $phone;
	public ?string $phoneNumber;
	public ?string $address;

	public ?string $defaultClosedMessage;
	public ?string $closedMessage;
	public ?string $courierStart;
	public ?string $courierEnd;

	protected Vendor $vendor;

	public function __construct(Vendor $vendor = null, array $config = [])
	{
		$this->vendor = $vendor ?: Yii::$app->modelFactory->getVendor(['status' => VendorStatus::ACTIVE]);
		parent::__construct($config);
	}

	public function init()
	{
		parent::init();
		$this->name = $this->vendor->name;
        $this->legalName = $this->vendor->legal_name;
        $this->regNumber = $this->vendor->reg_number;
        $this->vatNumber = $this->vendor->options->vatNumber;
		$this->status = (string) $this->vendor->status;
		$this->email = $this->vendor->email->email ?? null;
		$this->phone = $this->vendor->phone->phone_nr ?? null;
		$this->phoneNumber = $this->vendor->phone->fullNumber ?? null;
		$this->address = $this->vendor->address ? (string) $this->vendor->address->export() : null;

		$this->closedMessage = null;
		$this->defaultClosedMessage = null;
		$this->courierStart = null;
		$this->courierEnd = null;
	}

	public function getVendor()
	{
		return $this->vendor;
	}


	public function rules()
	{
		return [
			[['name', 'legalName', 'regNumber', 'vatNumber'], 'trim'],

			[['name','status','email','phone'], 'required'],
			[['name','status','email','phone', 'legalName', 'regNumber', 'vatNumber'], 'string', 'max' => 255],

			[['email'], 'trim'],
			[['email'], 'email'],

			[['phone'], 'trim'],
			[['phone'], PhoneInputValidator::class],
			[['phoneNumber'], 'string'],

            ['address', 'safe'],

			[['closedMessage', 'defaultClosedMessage'], 'trim'],
			[['closedMessage', 'defaultClosedMessage', 'courierStart', 'courierEnd'], 'string'],
		];
	}

	public function attributeLabels()
	{
        return [
            'name'      => Yii::t('backend/vendor', 'Name'),
            'legalName' => Yii::t('backend/vendor', 'Legal name'),
            'regNumber' => Yii::t('backend/vendor', 'Reg Number'),
            'vatNumber' => $this->vendor->options->getAttributeLabel('vatNumber'),
            'status'    => Yii::t('backend/vendor', 'Status'),
            'settings'  => Yii::t('backend/vendor', 'Settings'),
            'email'     => Yii::t('backend/vendor', 'Email'),
            'phone'     => Yii::t('backend/vendor', 'Phone'),
            'address'   => Yii::t('backend/vendor', 'Address'),
        ];
	}

	public function getStatuses()
    {
        return VendorStatus::displayValues([VendorStatus::DELETED]);
    }

	public function getAddressForm()
    {
        if ($this->vendor->address) {
            $addressForm = new VendorAddressForm($this->vendor);
            $addressForm->populate($this->vendor->address);
            return $addressForm;
        } else {
            return new VendorAddressForm($this->vendor);
        }
    }

	public function update()
	{
		if ($this->validate()) {

		    $transaction = Yii::$app->db->beginTransaction();
		    try {

                $this->vendor->name = $this->name;
                $this->vendor->legal_name = $this->legalName;
                $this->vendor->reg_number = $this->regNumber;
                $this->vendor->options->vatNumber = $this->vatNumber;
                $this->vendor->status = new VendorStatus($this->status);

                if (!$this->vendor->save()) {
                    return $this->rollback($transaction, $this->vendor->errors);
                }

                /**
                 * Update vendor address
                 */
                if ($address = $this->vendor->address) {
                    $address->updateJson($this->address);
                } else {
                    $address = Address::create($this->address);
                    $address->type = AddressType::MAIN;
                    $address->status = ContactAttributeStatus::UNVERIFIED;
                    $address->vendor_id = $this->vendor->id;
                }
                if (!$address->save()) {
                    return $this->rollback($transaction, $address->errors, 'address');
                }

                /**
                 *  Update vendor email
                 */
                if (!$email = $this->vendor->email) {
                    $email = new Email();
                    $email->status = ContactAttributeStatus::UNVERIFIED;
                    $email->vendor_id = $this->vendor->id;
                }
                $email->email = $this->email;
                if (!$email->save()) {
                    return $this->rollback($transaction, $email->errors, 'email');
                }

                /**
                 *  Update vendor phone
                 */
                if (!$phone = $this->vendor->phone) {
                    $phone = new Phone();
                    $phone->status = ContactAttributeStatus::UNVERIFIED;
                    $phone->vendor_id = $this->vendor->id;
                }
                $phone->phone_nr = PhoneHelper::getNationalPhoneNumber($this->phoneNumber);
                $phone->country_code = PhoneHelper::getPhoneCode($this->phoneNumber);
                if (!$phone->save()) {
                    return $this->rollback($transaction, $phone->errors, 'phone');
                }

                $transaction->commit();
                return true;

		    } catch (\Exception $e) {
		        $transaction->rollBack();
		        throw $e;
            }
		}
		return false;
	}
}
