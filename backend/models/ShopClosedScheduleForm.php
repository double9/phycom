<?php

namespace phycom\backend\models;


use phycom\common\helpers\Date;
use phycom\common\models\traits\ModelTrait;
use phycom\common\models\Shop;

use phycom\common\models\ShopClosed;
use yii\helpers\Json;
use yii\base\Model;
use yii;

/**
 * Class ShopClosedScheduleForm
 * @package phycom\backend\models
 *
 * @property-read Shop $shop
 */
class ShopClosedScheduleForm extends Model
{
    use ModelTrait;

    public $events;

    protected $shop;

    public function __construct(Shop $shop, array $config = [])
    {
        $this->shop = $shop;
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['events'], 'string']
        ];
    }

    public function getShop()
    {
        return $this->shop;
    }

    public function populateEvents()
    {
        $dates = [];
        $today = (new \DateTime())->setTime(0,0,0);
        foreach ($this->shop->shopClosed as $model) {
            if ($model->date >= $today) {
                $dates[] = $model->date->format('Y-m-d');
            }
        }
        $this->events = Json::encode($dates);
    }

    public function save()
    {
        if ($this->validate()) {
            if ($this->shop->isNewRecord) {
                throw new yii\base\InvalidCallException('Shop must be saved');
            }
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $events = Json::decode($this->events);

                $today = (new \DateTime())->setTime(0,0,0);
                Yii::$app->db->createCommand()->delete(ShopClosed::tableName(), [
                    'and',
                    ['shop_id' => $this->shop->id],
                    ['>=', 'date', $today->format('Y-m-d')]
                ])->execute();

                foreach ($events as $date) {
                    if (Date::create($date, 'Y-m-d') < $today) {
                        continue;
                    }

                    $model = new ShopClosed();
                    $model->shop_id = $this->shop->id;
                    $model->date = Date::create($date, 'Y-m-d', new \DateTimeZone('UTC'))->dateTime->setTime(0,0,0);
                    if (!$model->save()) {
                        Yii::error($model->errors, __METHOD__);
                        return $this->rollback($transaction, $model->errors, 'events');
                    }
                }
                $transaction->commit();
                return true;
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
        return false;
    }
}