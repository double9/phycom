<?php

namespace phycom\backend\models;

use phycom\common\interfaces\SearchModelInterface;
use phycom\common\models\UserActivity;
use phycom\common\models\traits\SearchQueryFilter;

use yii\data\ActiveDataProvider;
use yii\base\Model;

/**
 * SearchUserActivity represents the model behind the search form about `common\models\UserActivity`.
 */
class SearchUserActivity extends UserActivity implements SearchModelInterface
{
    use SearchQueryFilter;

    public $createdFrom;
    public $createdTo;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','user_id'], 'integer'],
            [['action','ip'], 'safe'],
            [['createdFrom','createdTo'],'safe']
        ];
    }

	public function __construct($userId = null, array $params = [])
	{
		if ((int)$userId) {
			$this->user_id = $userId;
		}
		parent::__construct($params);
	}
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

	/**
	 * Creates data provider instance with search query applied
	 * @param array $params
	 * @return ActiveDataProvider
	 */
	public function search(array $params = [])
	{
		$query = self::find();

		if ($this->user_id) {
			$query->filterWhere(['user_id' => $this->user_id]);
		}

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
			'pagination' => [
				'pageSize' => 10,
			],
		]);


		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query
			->andFilterWhere([
				'id' => $this->id,
				'user_id' => $this->user_id
			])
			->andFilterWhere(['like', 'action', $this->action])
			->andFilterWhere(['like', 'ip', $this->ip])
			->filterDateRange('created_at', $this->createdFrom, $this->createdTo);

		return $dataProvider;
	}
}
