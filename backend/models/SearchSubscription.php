<?php

namespace phycom\backend\models;

use phycom\common\components\ActiveQuery;
use phycom\common\models\traits\SearchQueryFilter;
use phycom\common\interfaces\SearchModelInterface;
use phycom\common\models\attributes\SubscriptionStatus;
use phycom\common\models\Subscription;

use yii\data\ActiveDataProvider;
use yii\data\Sort;
use Yii;

/**
 * Class SearchSubscription
 * @package phycom\backend\models
 */
class SearchSubscription extends Subscription implements SearchModelInterface
{
	use SearchQueryFilter;

	public $name;
	public $registeredUser;

	public $createdFrom;
	public $createdTo;
	public $updatedFrom;
	public $updatedTo;

	public function rules()
	{
		return [
			[['emails_sent'], 'integer'],
			[['first_name', 'last_name', 'name'], 'string'],
			[['email'], 'string', 'max' => 255],
			['registeredUser', 'boolean'],
			['status', 'in', 'range' => SubscriptionStatus::all()],
			[['created_at', 'updated_at','createdFrom','createdTo','updatedFrom','updatedTo'], 'safe'],
		];
	}

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     * @throws \yii\base\Exception
     */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
			'pagination' => [
				'pageSize' => 20
			],
		]);

		$this->sort($dataProvider->sort);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			's.id' => $this->id,
			's.status' => (string)$this->status,
		]);

		$query->filterFullName(['s.first_name', 's.last_name'], $this->name);
		$query->filterDateRange('s.created_at', $this->createdFrom, $this->createdTo);
		$query->filterDateRange('s.updated_at', $this->updatedFrom, $this->updatedTo);
		return $dataProvider;
	}

    /**
     * @return array
     */
    public function getExporterColumns()
    {
        return [
            [
                'label'            => Yii::t('backend/subscription', 'Name'),
                'contentOptions'   => ['alignment' => ['horizontal' => 'left', 'vertical' => 'center']],
                'dimensionOptions' => ['autoSize' => true],
                'format'           => 'text',
                'value'            => function ($model) {
                    /**
                     * @var Subscription $model
                     */
                    $names = [];
                    if ($model->first_name) {
                        $names[] = trim($model->first_name);
                    }
                    if ($model->last_name) {
                        $names[] = trim($model->last_name);
                    }

                    return !empty($names) ? implode(' ', $names) : null;
                }
            ],
            [
                'attribute'        => 'email',
                'contentOptions'   => ['alignment' => ['horizontal' => 'left', 'vertical' => 'center']],
                'dimensionOptions' => ['autoSize' => true]
            ],
            [
                'attribute'        => 'created_at',
                'label'            =>  Yii::t('backend/subscription', 'Subscribed At'),
                'format'           => 'date',
                'contentOptions'   => ['alignment' => ['horizontal' => 'left', 'vertical' => 'center']],
                'dimensionOptions' => ['autoSize' => true]
            ]
        ];
    }

	protected function sort(Sort $sort)
	{
		$sort->attributes['name'] = [
			'asc' => ['s.first_name' => SORT_ASC, 's.last_name' => SORT_ASC],
			'desc' => ['s.first_name' => SORT_DESC, 's.last_name' => SORT_DESC],
		];
	}

    /**
     * @return ActiveQuery
     * @throws \yii\base\Exception
     */
	protected function createSearchQuery()
	{
		$query = static::find();
		$query->select([
			's.*',
			'TRIM(CONCAT_WS(\' \', s.first_name, s.last_name)) as name',
		]);
		$query->from(['s' => Subscription::tableName()]);
		return $query;
	}
}
