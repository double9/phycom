<?php

namespace phycom\backend\models;


use phycom\common\models\Review;
use phycom\common\models\traits\ModelTrait;
use yii\base\Model;

/**
 * Class ReviewForm
 * @package phycom\backend\models
 */
class ReviewForm extends Model
{
    use ModelTrait;

    public $status;

    protected $review;

    public function __construct(Review $review, array $config = [])
    {
        $this->review = $review;
        parent::__construct($config);
    }


}