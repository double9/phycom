<?php

namespace phycom\backend\models;

use yii;

/**
 * Class AddressFieldForm
 * @package phycom\backend\models
 *
 * @property-read null $model
 */
class AddressFieldForm extends \phycom\common\models\AddressForm
{
    public function init()
    {
        parent::init();
        $this->country = Yii::$app->country->defaultCountry;
    }

    public function getModel()
    {
        return null;
    }

    public function update()
    {

    }
}