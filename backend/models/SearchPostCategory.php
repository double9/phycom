<?php

namespace phycom\backend\models;

use phycom\common\components\ActiveQuery;
use phycom\common\models\traits\SearchQueryFilter;
use phycom\common\interfaces\SearchModelInterface;
use phycom\common\models\attributes\CategoryStatus;
use phycom\common\models\attributes\TranslationStatus;
use phycom\common\models\Language;
use phycom\common\models\PostCategory;
use phycom\common\models\translation\PostCategoryTranslation;

use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\Query;
use yii;

/**
 * Class SearchPostCategory
 * @package phycom\backend\models
 *
 * @property int $numTotal
 * @property int $numVisible
 * @property int $numHidden
 * @property int $numTranslated
 * @property int $numTranslationPending
 */
class SearchPostCategory extends PostCategory implements SearchModelInterface
{
	use SearchQueryFilter;

	/**
	 * @var Language
	 */
	public $language;
	/**
	 * @var PostCategory
	 */
	public $category;

	public $title;
	public $description;
	public $createdFrom;
	public $createdTo;

	public function rules()
	{
		$rules = parent::rules();
		$rules[] = [['id'], 'integer'];
		$rules[] = [['title', 'description'], 'string'];
		$rules[] = [['createdFrom','createdTo'],'safe'];
		return $rules;
	}

	public function getNumTotal()
	{
		return $this->createSearchQuery()->count();
	}

	public function getNumVisible()
	{
		return $this->createSearchQuery()->andWhere(['c.status' => CategoryStatus::VISIBLE])->count();
	}

	public function getNumHidden()
	{
		return $this->createSearchQuery()->andWhere(['c.status' => CategoryStatus::HIDDEN])->count();
	}

	public function getNumTranslated()
	{
		return $this->createSearchQuery()->andWhere([
			'and',
			't1.id IS NOT NULL',
			['t1.status' => TranslationStatus::PUBLISHED]
		])->count();
	}

	public function getNumTranslationPending()
	{
		return $this->createSearchQuery()->andWhere([
			'and',
			't1.id IS NOT NULL',
			['t1.status' => TranslationStatus::DRAFT]
		])->count();
	}


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     * @throws yii\base\Exception
     */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => 20
			],
		]);

		$this->sort($dataProvider->sort);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'c.id' => $this->id,
		]);

		$query->filterText('t.title', $this->title);
		$query->filterText('t.description', $this->description);
		$query->filterDateRange('c.created_at', $this->createdFrom, $this->createdTo);

		return $dataProvider;
	}

    /**
     * @return ActiveQuery
     * @throws yii\base\Exception
     */
    public function createSearchQuery()
    {
        $query = static::find();
        $query->select([
            'c.*',
            'CASE WHEN (t1.title IS NOT NULL) THEN t1.title ELSE t2.title END AS title',
        ]);
        $query->from(['c' => PostCategory::tableName()]);
        $query->where(['not', ['c.status' => CategoryStatus::DELETED]]);

        $query->leftJoin(['t1' => PostCategoryTranslation::tableName()], 't1.post_category_id = c.id AND t1.language = :lang');
        $query->leftJoin(['t2' => PostCategoryTranslation::tableName()], [
            'or',
            // fallback to default language
            [
                'and',
                't2.post_category_id = c.id',
                't2.language = :fallback_lang',
                't2.language != :lang',
                't2.title IS NOT NULL',
                't2.url_key IS NOT NULL'
            ],
            // or if there is no record with default language then any other language with content
            [
                'and',
                't2.post_category_id = c.id',
                't2.language != :lang',
                't2.title IS NOT NULL',
                't2.url_key IS NOT NULL'
            ]
        ]);
        $query->addParams([
            'lang'          => $this->language->code,
            'fallback_lang' => Yii::$app->lang->findFallbackLanguage($this->language)->code
        ]);

        return $query;
    }

	protected function sort(Sort $sort)
	{
		$sort->defaultOrder = ['parent_id' => SORT_ASC, 'order' => SORT_ASC]; //[new Expression('c.parent_id DESC NULLS FIRST'), new Expression('c.order DESC')];
	}
}
