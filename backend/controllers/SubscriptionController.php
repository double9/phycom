<?php

namespace phycom\backend\controllers;

use PhpOffice\PhpSpreadsheet\Writer\Csv;
use phycom\backend\models\SearchSubscription;
use phycom\common\components\FileExportFormatter;
use phycom\common\models\attributes\SubscriptionStatus;
use phycom\common\models\Subscription;

use yii2tech\spreadsheet\Spreadsheet;

use yii\web\NotFoundHttpException;
use yii;


/**
 * Class SubscriptionController
 * @package phycom\backend\controllers
 */
class SubscriptionController extends BaseController
{
    /**
     * @param yii\base\Action $action
     * @return bool
     * @throws NotFoundHttpException
     */
    public function beforeAction($action)
    {
        $this->checkEnabled(Yii::$app->subscription);
        return parent::beforeAction($action);
    }

    /**
     * @param null $export
     * @return string|yii\web\Response
     * @throws yii\base\Exception
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionIndex($export = null)
	{
		$this->checkPermission('search_newsletter_subscriptions');

		$model = new SearchSubscription();

        if ($export) {
            $model->status = SubscriptionStatus::ACTIVE;
            $dataProvider = $model->search(Yii::$app->request->get());
            return (new Spreadsheet([
                'batchSize'    => 1000,
                'writerType'   => 'Csv',
                'dataProvider' => $dataProvider,
                'columns'      => $model->getExporterColumns(),
                'formatter'    => new FileExportFormatter([
                    'locale'         => Yii::$app->language,
                    'dateFormat'     => 'php:Y-m-d',
                    'datetimeFormat' => 'php:Y-m-d:H:i:s',
                    'timeFormat'     => 'php:H:i:s'
                ])
            ]))->send('newsletter_subscriptions_' . (new \DateTime())->format('Y-m-d\THis') . '.csv');
        } else {
            $dataProvider = $model->search(Yii::$app->request->get());
        }

		return $this->render('index', ['model' => $model, 'dataProvider' => $dataProvider]);
	}

	/**
	 * @param int $id
	 * @return Subscription
	 * @throws NotFoundHttpException
	 */
	protected function findSubscription($id)
	{
		$model = Subscription::findOne($id);
		if (!$model) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $model;
	}
}
