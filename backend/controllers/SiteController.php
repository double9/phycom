<?php
namespace phycom\backend\controllers;

use phycom\backend\models\RegistrationForm;
use phycom\backend\models\StatisticsForm;
use phycom\common\helpers\FlashMsg;
use phycom\common\models\attributes\UserStatus;
use phycom\common\models\attributes\UserTokenType;
use phycom\common\models\ForgotPasswordForm;
use phycom\common\models\LoginForm;
use phycom\common\models\User;
use phycom\common\models\UserToken;

use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii;

/**
 * Site controller
 */
class SiteController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
    	$behaviors = parent::behaviors();
	    $behaviors['access']['rules'] = [
		    ['allow' => true, 'actions' => [
			    'login',
			    'register',
			    'forgot-password',
			    'reset-password',
			    'error'
		    ]],
		    ['allow' => true, 'roles' => ['@'], 'actions' => ['logout']],
		    ['allow' => true, 'roles' => ['use_backend']],
		    ['allow' => false]
	    ];

    	$behaviors['verbs'] = [
		    'class' => VerbFilter::class,
		    'actions' => ['logout' => ['post','get']],
	    ];
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => ['class' => 'yii\web\ErrorAction'],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->dashboard->isEnabled()) {
            return $this->render('dashboard');
        }
        if (Yii::$app->commerce->isEnabled()) {
            return $this->redirect(['/product/catalog']);
        }
        return $this->redirect(['/post/pages']);
    }

    /**
     * @return string
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionStatistics()
    {
        $this->checkPermission('search_statistics');

        $form = new StatisticsForm();
        $form->from = (new \DateTime())->sub(new \DateInterval('P1M'))->format('Y-m-d');
        $form->to = (new \DateTime())->format('Y-m-d');

        if ($form->load(Yii::$app->request->post())) {
            if (!$form->validate()) {
                FlashMsg::error($form->errors);
            }
        }
        return $this->render('statistics', ['model' => $form]);
    }


    public function actionForgotPassword()
    {
	    $this->layout = 'main-login';
	    $model = new ForgotPasswordForm();

	    if ($model->load(Yii::$app->request->post())) {
	    	if ($model->sendResetLink()) {
			    FlashMsg::success(Yii::t('backend/user', 'Recovery link was sent. Please check your email'));
			    $model->email = null;
		    } else {
	    		Yii::error($model->errors, __METHOD__);
		    }
	    }
    	return $this->render('forgot-password', ['model' => $model]);
    }

    public function actionResetPassword($t)
    {
	    $this->layout = 'main-login';
	    if ($token = UserToken::findValid($t, UserTokenType::PASSWORD_RESET)) {
		    if ($user = User::findIdentity($token->user_id)) {
			    Yii::$app->user->switchIdentity($user);
			    $model = new \phycom\common\models\ChangePasswordForm($user);

			     if ($model->load(Yii::$app->request->post())) {
				    if ($model->resetPassword()) {
					    FlashMsg::success(Yii::t('backend/user', 'User {user_id} password changed', ['user_id' => $user->id]));
					    return $this->redirect(['/site']);
				    } else {
                        Yii::error($model->errors, __METHOD__);
				    }
			    }
			    return $this->render('reset-password', ['model' => $model]);
		    }
	    }
	    throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionRegister($t)
    {
	    $this->layout = 'main-login';
    	if ($token = UserToken::findValid($t, UserTokenType::REGISTRATION_REQUEST)) {
    		if ($user = User::findIdentity($token->user_id, UserStatus::PENDING)) {
			    Yii::$app->user->switchIdentity($user);

			    $roles = $token->getParam('roles');
			    $model = new RegistrationForm($user, $roles);

			    if ($model->load(Yii::$app->request->post())) {

				    if ($model->register()) {
					    FlashMsg::success(Yii::t('backend/user', 'User {user_id} successfully activated', ['user_id' => $user->id]));
					    return $this->redirect(['/site']);
				    } else {
                        Yii::error($model->errors, __METHOD__);
					    FlashMsg::error($model->errors);
				    }
			    }
			    return $this->render('register', ['model' => $model]);
		    }
	    }
	    throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $model->requireAdminUser = true;

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
        	Yii::$app->user->setReturnUrl(Url::toRoute(['/']));
            return $this->goBack();
        } else {
        	$this->layout = 'main-login';
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
}
