<?php

namespace phycom\backend\controllers;

use phycom\backend\models\SearchUser;
use phycom\backend\models\SearchUserActivity;
use phycom\backend\models\SearchUserInvitation;
use phycom\backend\models\UserAddressForm;
use phycom\backend\models\UserInvitationForm;
use phycom\backend\models\UserProfileForm;
use phycom\backend\models\UserProfileImageForm;
use phycom\common\helpers\FlashMsg;
use phycom\common\models\attributes\UserStatus;
use phycom\common\models\User;

use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii;

/**
 * Class UserController
 * @package phycom\backend\controllers
 */
class UserController extends BaseController
{
    /**
     * @return string
     * @throws yii\base\Exception
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionIndex()
	{
		$this->checkPermission('search_users');
		$model = new SearchUser();

		if (Yii::$app->user->can('search_unregistered_users')) {
		    $model->showUnregistered = true;
        }

		$dataProvider = $model->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model'        => $model
        ]);
	}

    /**
     * @return string
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionInvite()
	{
		$this->checkPermission('invite_backend_users');
		$searchModel = new SearchUserInvitation();
		$form = new UserInvitationForm();
		if ($form->load(Yii::$app->request->post())) {
			$form->create() ?
				FlashMsg::success(Yii::t('backend/user', 'Invitation message was queued')) :
				FlashMsg::error($form->errors);
		}
        return $this->render('invite', [
            'model'       => $form,
            'searchModel' => $searchModel
        ]);
	}

    /**
     * @return string
     * @throws NotFoundHttpException
     * @throws yii\base\Exception
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionPendingInvitations()
	{
		$this->checkPermission('invite_backend_users');
		$this->ajaxOnly();

		$searchModel = new SearchUserInvitation();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->renderPartial('partials/invitations', ['dataProvider' => $dataProvider]);
	}

    /**
     * @param int $id - user id
     * @return array
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionResendInvitation($id)
	{
		$this->checkPermission('invite_backend_users');
		$this->ajaxOnly(Response::FORMAT_JSON);
		$form = new UserInvitationForm($this->findUser($id));

		$successMessage = Yii::t('backend/user', 'Invitation successfully sent');
		return $form->resendInvitation() ? ['message' => $successMessage] : ['error' => $form->lastError];
	}

    /**
     * @param int $id - user id
     * @return array
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionRemoveInvitation($id)
    {
        $this->checkPermission('remove_pending_users');
        $this->ajaxOnly(Response::FORMAT_JSON);

        $user = $this->findUser($id);

        if ((string)$user->status === UserStatus::PENDING) {
            return $user->delete()
                ? ['message' => Yii::t('backend/user', 'User invitation {id} removed', ['id' => $user->id])]
                : ['error' => array_values($user->firstErrors)[0]];
        }
        return ['error' => Yii::t('backend/user', 'User {id} is not pending registration', ['id' => $user->id])];
    }

    /**
     * @param int $id - user id
     * @return Response
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionBlock($id)
    {
        $this->checkPermission('update_user', $id);

        $user = $this->findUser($id);
        $user->status = UserStatus::BLOCKED;

        if ($user->save()) {
            FlashMsg::success(Yii::t('backend/user', 'User {id} successfully blocked', ['id' => $id]));
        } else {
            FlashMsg::error($user->errors);
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id - user id
     * @return string
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionProfile($id)
	{
		$this->checkPermission('update_user', $id);
		$user = $this->findUser($id);
		$profileForm = new UserProfileForm($user);
		$profileImageForm = new UserProfileImageForm($user);
		$addressForm = new UserAddressForm($user);

		if ($profileForm->load(Yii::$app->request->post())) {
			$profileForm->update() ?
				FlashMsg::success(Yii::t('backend/user', 'User {user_id} profile successfully updated', ['user_id' => $user->id])) :
				FlashMsg::error($profileForm->errors);
		}

		if ($addressForm->load(Yii::$app->request->post())) {
			$addressForm->update() ?
				FlashMsg::success(Yii::t('backend/user', 'User {user_id} address successfully saved', ['user_id' => $user->id])) :
				FlashMsg::error($addressForm->errors);
		}

        if ($user->meta->load(Yii::$app->request->post())) {
		    if (!$user->meta->validate()) {
                FlashMsg::error($user->meta->errors);
            } else {
                $user->update() ?
                    FlashMsg::success(Yii::t('backend/user', 'User {user_id} meta successfully updated', ['user_id' => $user->id])) :
                    FlashMsg::error($user->errors);
            }
        }

		return $this->render('profile', [
			'user' => $user,
			'profileForm' => $profileForm,
			'profileImageForm' => $profileImageForm,
			'addressForm' => $addressForm,
			'activity' => new SearchUserActivity($id)
		]);
	}

    /**
     * @param int $id - user id
     * @return array
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionUploadAvatar($id)
	{
		$this->checkPermission('change_avatar', $id);
		if (Yii::$app->request->isPost) {

			Yii::$app->response->format = Response::FORMAT_JSON;
			$user = $this->findUser($id);
			$model = new UserProfileImageForm($user);
			$model->avatar = UploadedFile::getInstance($model, 'avatar');
			$image = $model->upload();

			return $image ? [
				'initialPreviewThumbnails' => [
					'<img src="' . $image . '" alt="' . $user->fullName . ' Avatar" title="' . $model->avatar->name . '"">',
				],
				'append' => false
			] : ['error' => $model->lastError];
		}
		throw new NotFoundHttpException('The requested page does not exist.');
	}

    /**
     * Renders the user activity table partial
     *
     * @param int $id - user id
     * @return string
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionActivity($id)
	{
		$this->checkPermission('search_user_activity');
		$this->ajaxOnly();

		$searchModel = new SearchUserActivity($id);
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->renderPartial('partials/user-activity', ['dataProvider' => $dataProvider]);
	}

    /**
     * @return string
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionPermissions()
	{
		$this->checkPermission('view_permissions');
		return $this->render('permissions');
	}

	/**
	 * @param $id - user id
	 * @return User
	 * @throws NotFoundHttpException
	 */
	protected function findUser($id)
	{
		$user = Yii::$app->modelFactory->getUser()::findOne($id);
		if (!$user) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $user;
	}
}
