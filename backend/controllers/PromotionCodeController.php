<?php

namespace phycom\backend\controllers;

use phycom\backend\models\DiscountRuleBulkUpdateForm;
use phycom\backend\models\DiscountRuleForm;
use phycom\backend\models\SearchDiscountRule;

use phycom\common\helpers\FlashMsg;
use phycom\common\models\attributes\DiscountRuleStatus;
use phycom\common\models\attributes\DiscountRuleType;
use phycom\common\models\DiscountRule;

use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii;

/**
 * Class PromotionCodeController
 * @package phycom\backend\controllers
 */
class PromotionCodeController extends BaseController
{
    /**
     * @param yii\base\Action $action
     * @return bool
     * @throws NotFoundHttpException
     */
    public function beforeAction($action)
    {
        $this->checkEnabled(Yii::$app->commerce->promoCodes);
        return parent::beforeAction($action);
    }

    /**
     * @return string
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionIndex()
    {
        $this->checkPermission('search_promotion_codes');

        $model = new SearchDiscountRule();
        $model->type = DiscountRuleType::COUPON;
        $dataProvider = $model->search(Yii::$app->request->get());

        $bulkUpdateModel = new DiscountRuleBulkUpdateForm();
        if ($bulkUpdateModel->load(Yii::$app->request->post())) {
            if ($bulkUpdateModel->validate() && ($count = $bulkUpdateModel->update())) {
                FlashMsg::success(Yii::t('backend/main', '{count} items successfully updated', ['count' => $count]));
            } else {
                FlashMsg::error($bulkUpdateModel->errors);
            }
        }

        return $this->render('index', ['model' => $model, 'dataProvider' => $dataProvider]);
    }

    /**
     * @return string|yii\web\Response
     * @throws yii\web\ForbiddenHttpException
     * @throws yii\base\InvalidConfigException
     */
    public function actionAdd()
    {
        $this->checkPermission('create_promotion_code');

        $model = new DiscountRule();
        $model->type = DiscountRuleType::COUPON;
        $model->status = DiscountRuleStatus::ACTIVE;

        $formModel = new DiscountRuleForm($model);

        if (Yii::$app->request->isGet) {
            $formModel->load(Yii::$app->request->get());
        }
        if ($formModel->load(Yii::$app->request->post())) {
            if ($count = $formModel->create()) {
                FlashMsg::success(Yii::t('backend/user', '{count} promotion code(s) successfully created', ['count' => $count]));
                return $this->afterSave($formModel);
            } else {
                FlashMsg::error($formModel->errors);
            }
        }
        return $this->render('edit', ['model' => $formModel]);
    }

    /**
     * @param $id
     * @return string|yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionEdit($id)
    {
        $this->checkPermission('update_promotion_code');

        $model = new DiscountRuleForm($this->findDiscountCard($id));

        if ($model->load(Yii::$app->request->post())) {
            if ($model->update()) {
                FlashMsg::success(Yii::t('backend/user', 'Promotion code {id} updated', ['id' => $model->discountRule->id]));
                return $this->afterSave($model);
            } else {
                FlashMsg::error($model->errors);
            }
        }
        return $this->render('edit', ['model' => $model]);
    }

    /**
     * @param int $id
     * @return yii\web\Response
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        $this->checkPermission('delete_promotion_code');

        $model = $this->findDiscountCard($id);
        if ($model->updateStatus(DiscountRuleStatus::DELETED)) {
            FlashMsg::success(Yii::t('backend/user', 'Promotion code {id} successfully deleted.', ['id' => $model->id]));
            return $this->redirect(Url::toRoute(['promotion-code/index']));
        } else {
            FlashMsg::error($model->errors);
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * @param DiscountRuleForm $form
     * @return yii\web\Response
     * @throws yii\base\InvalidConfigException
     */
    protected function afterSave(DiscountRuleForm $form)
    {
        switch (Yii::$app->request->post('afterSave')) {
            case self::AFTER_SAVE_NEW:
                return $this->redirect(Url::toRoute(['promotion-code/add']));
            case self::AFTER_SAVE_DUPLICATE:
                return $this->redirect($this->createDuplicateUrl($form));
            case self::AFTER_SAVE_CLOSE:
                return $this->redirect(Url::toRoute(['promotion-code/index']));
            default:
                return $this->redirect(Url::toRoute(['promotion-code/edit', 'id' => $form->discountRule->id]));
        }
    }

    /**
     * @param DiscountRuleForm $model
     * @return string
     * @throws yii\base\InvalidConfigException
     */
    protected function createDuplicateUrl(DiscountRuleForm $model)
    {
        $a = $model->attributes;

        unset($a['key']);
        unset($a['personalCode']);
        unset($a['birthday']);

        return Url::toRoute(ArrayHelper::merge(['promotion-code/add'], [$model->formName() => $a]));
    }

    /**
     * @param int $id
     * @return DiscountRule
     * @throws NotFoundHttpException
     */
    protected function findDiscountCard($id)
    {
        $model = DiscountRule::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $model;
    }
}
