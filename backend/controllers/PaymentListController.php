<?php

namespace phycom\backend\controllers;

use phycom\backend\models\PaymentForm;
use phycom\backend\models\SearchPayment;

use phycom\common\helpers\FlashMsg;
use phycom\common\models\attributes\PaymentStatus;
use phycom\common\models\Payment;
use phycom\common\models\statistics\SearchSalesStatistics;

use yii\web\NotFoundHttpException;
use yii;

/**
 * Class PaymentListController
 * @package phycom\backend\controllers
 */
class PaymentListController extends BaseController
{
    /**
     * @param yii\base\Action $action
     * @return bool
     * @throws NotFoundHttpException
     */
    public function beforeAction($action)
    {
        $this->checkEnabled(Yii::$app->commerce);
        return parent::beforeAction($action);
    }

    /**
     * @return string
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionIndex()
	{
		$this->checkPermission('search_payments');

		$model = new SearchPayment();
		$dataProvider = $model->search(Yii::$app->request->get());

		return $this->render('index', ['model' => $model, 'dataProvider' => $dataProvider]);
	}

    /**
     * @return string
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionAdd()
	{
		$this->checkPermission('create_payment');
		$model = new PaymentForm();

		return $this->render('add', ['model' => $model]);
	}

    /**
     * @param int $id
     * @return string
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionEdit($id)
	{
		$this->checkPermission('update_payment');
		$payment = $this->findPayment($id);
		$model = new PaymentForm($payment);

		return $this->render('edit', ['model' => $model, 'payment' => $payment]);
	}

    /**
     * @param int $id
     * @param string $status
     * @return yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionUpdateStatus($id, $status)
	{
		$this->checkPermission('update_payment');
		$payment = $this->findPayment($id);
		if (!in_array($status, PaymentStatus::all())) {
			throw new yii\base\InvalidArgumentException('Invalid payment status ' . $status);
		}
		$payment->status = $status;
		if (!$payment->update()) {
			FlashMsg::error($payment->errors);
			return $this->redirect(Yii::$app->request->referrer);
		}
		FlashMsg::success(Yii::t('backend/payment', 'Payment {id} status successfully updated', ['id' => $payment->id]));
		return $this->redirect(Yii::$app->request->referrer);
	}

    /**
     * @return array|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionStatistics()
    {
        $this->checkPermission('search_payments');
        $this->ajaxOnly();

        $model = new SearchSalesStatistics();
        $model->load(Yii::$app->request->get(), '');

        if ($model->validate()) {
            return $this->asJson($model->search()->allModels);
        }
        return $this->asJson(['error' => $model->firstErrors]);
    }

	/**
	 * @param int $id
	 * @return Payment
	 * @throws NotFoundHttpException
	 */
	protected function findPayment($id)
	{
		$payment = Payment::findOne($id);
		if (!$payment) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $payment;
	}
}
