<?php

namespace phycom\backend\controllers;


use phycom\backend\models\PostCategoryForm;
use phycom\backend\models\SearchPostCategory;
use phycom\common\helpers\FlashMsg;
use phycom\common\models\Language;
use phycom\common\models\PostCategory;

use yii\web\NotFoundHttpException;
use yii;

/**
 * Class PostCategoryController
 * @package phycom\backend\controllers
 */
class PostCategoryController extends BaseController
{
	public $defaultAction = 'index';


	public function actionIndex($lang = null)
	{
		$this->checkPermission('search_post_categories');
		$language = $this->getLanguage($lang ?? Yii::$app->lang->default->code);
		return $this->renderMain($language);
	}

	public function actionCreate($lang, $parentId = null)
	{
		$this->checkPermission('create_post_category');

		$form = new PostCategoryForm($lang);
		if ($form->load(Yii::$app->request->post())) {
			if (!$form->update()) {
				FlashMsg::error($form->lastError);
			} else {
				FlashMsg::success(Yii::t('backend/category', 'Category successfully created'));
				return $this->redirect(['post-category/edit', 'lang' => $form->language, 'id' => $form->category->id]);
			}
		}
		return $this->renderForm($form);
	}

	public function actionEdit($lang, $id)
	{
		$this->checkPermission('update_post_category');
		$form = new PostCategoryForm($lang, $this->findCategory($id));

		if ($form->load(Yii::$app->request->post())) {
			if (!$form->update()) {
				FlashMsg::error($form->lastError);
			} else {
				FlashMsg::success(Yii::t('backend/category', 'Category successfully updated'));
				return $this->redirect(['post-category/edit', 'id' => $form->category->id, 'lang' => $form->language]);
			}
		}
		return $this->renderForm($form);
	}

	public function actionMove($id, $target, $position)
	{
		$this->checkPermission('move_post_category');
		$model = $this->findCategory($id);
		$t = $this->findCategory($target);
		switch ($position) {
			case 0:
				$model->insertBefore($t);
				break;

			case 1:
				$model->appendTo($t);
				break;

			case 2:
				$model->insertAfter($t);
				break;
		}
	}

	public function actionDelete($id)
	{
		$this->checkPermission('delete_post_category');
		$category = $this->findCategory($id);
		if ($category->delete()) {
			FlashMsg::success(Yii::t('backend/category', 'Category {id} deleted', ['id' => $id]));
		} else {
			FlashMsg::error($category->errors);
		}
		return $this->redirect(['post-category/index']);
	}

	/**
	 * @param string $languageCode
	 * @return \phycom\common\models\Language|null
	 * @throws NotFoundHttpException
	 */
	protected function getLanguage($languageCode)
	{
		if (!$language = Yii::$app->lang->get($languageCode)) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $language;
	}

	/**
	 * Renders the main view with post category form
	 * @param PostCategoryForm $form
	 * @return string
	 */
	protected function renderForm(PostCategoryForm $form)
	{
		$model = new SearchPostCategory();
		$model->category = $form->category;
		$model->language = $form->translation->languageModel;

		$dataProvider = $model->search(Yii::$app->request->queryParams);

		return $this->render('/post/post-category', [
			'dataProvider' => $dataProvider,
			'model' => $model,
			'formModel' => $form
		]);
	}

	/**
	 * Renders the main view without the post category form
	 * @param Language|null $language
	 * @return string
	 */
	protected function renderMain(Language $language = null)
	{
		$model = new SearchPostCategory();
		$model->language = $language;

		$dataProvider = $model->search(Yii::$app->request->queryParams);

		return $this->render('/post/post-category', [
			'dataProvider' => $dataProvider,
			'model' => $model,
			'formModel' => null
		]);
	}


	/**
	 * @param $id
	 * @return PostCategory
	 * @throws NotFoundHttpException
	 */
	protected function findCategory($id)
	{
		$category = PostCategory::findOne(['id' => $id]);
		if (!$category) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $category;
	}
}
