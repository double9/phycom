<?php

namespace phycom\backend\controllers;


use yii2tech\filestorage\DownloadAction;

use yii\web\NotFoundHttpException;
use yii;

/**
 * Class FileDownloadAction
 * @package phycom\backend\controllers
 */
class FileDownloadAction extends DownloadAction
{
    public function run($bucket, $filename)
    {
        try {
            return parent::run($bucket, $filename);
        } catch (\Exception $e) {
            Yii::error($e->getMessage() . ' - ' . $e->getFile() . ':' . $e->getLine(), __METHOD__);
            Yii::error($e->getTraceAsString(), __METHOD__);
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}