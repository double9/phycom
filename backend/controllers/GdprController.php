<?php

namespace phycom\backend\controllers;


use phycom\backend\models\SearchPartnerContract;
use phycom\common\helpers\FlashMsg;
use phycom\common\helpers\Url;
use phycom\common\models\PartnerContract;

use yii\web\NotFoundHttpException;
use yii;

/**
 * Class GdprController
 * @package phycom\backend\controllers
 */
class GdprController extends BaseController
{
    /**
     * @param yii\base\Action $action
     * @return bool
     * @throws NotFoundHttpException
     */
    public function beforeAction($action)
    {
        $this->checkEnabled(Yii::$app->partnerContracts);
        return parent::beforeAction($action);
    }

    /**
     * @return string
     * @throws yii\base\Exception
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionIndex()
    {
        $this->checkPermission('search_partner_contracts');

        $model = new SearchPartnerContract();
        $dataProvider = $model->search(Yii::$app->request->get());

        return $this->render('index', ['model' => $model, 'dataProvider' => $dataProvider]);
    }

    /**
     * @param int $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionEdit($id)
    {
        $this->checkPermission('update_partner_contract');
        $contract = $this->findContract($id);
        if ($contract->load(Yii::$app->request->post())) {
            if ($contract->update()) {
                FlashMsg::success(Yii::t('backend/review', 'Contract {id} successfully updated', ['id' => $contract->id]));
            } else {
                FlashMsg::error($contract->errors);
            }
        }
        return $this->render('edit', ['model' => $contract]);
    }

    /**
     * @return string|yii\web\Response
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionAdd()
    {
        $this->checkPermission('create_partner_contract');
        $contract = new PartnerContract();
        if ($contract->load(Yii::$app->request->post())) {
            if ($contract->save()) {
                FlashMsg::success(Yii::t('backend/review', 'Contract {id} successfully created', ['id' => $contract->id]));
                return $this->redirect(Url::toRoute(['/gdpr/edit', 'id' => $contract->id]));
            } else {
                FlashMsg::error($contract->errors);
            }
        }
        return $this->render('edit', ['model' => $contract]);
    }

    /**
     * @param int $id
     * @return PartnerContract
     * @throws NotFoundHttpException
     */
    protected function findContract($id)
    {
        $contract = PartnerContract::findOne($id);
        if (!$contract) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $contract;
    }
}
