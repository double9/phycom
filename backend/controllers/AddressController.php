<?php

namespace phycom\backend\controllers;

use phycom\common\models\Address;
use phycom\common\models\Country;

use yii\web\Response;
use yii;

/**
 * Class CountryController
 * @package phycom\backend\controllers
 */
class AddressController extends BaseController
{
    /**
     * @return array
     * @throws yii\web\NotFoundHttpException
     */
    public function actionDivisions()
    {
        $this->ajaxOnly(Response::FORMAT_JSON);
        if (!$countryCode = Yii::$app->request->post('country')) {
            throw new yii\base\InvalidCallException('Invalid call');
        }

        if (!$country = Country::findOne(['code' => $countryCode])) {
            throw new yii\base\InvalidArgumentException('Invalid param country');
        }
        return array_map(function ($item) {return $item['name'];}, $country->divisions);
    }

    /**
     * @return mixed
     * @throws yii\web\NotFoundHttpException
     */
    public function actionLabel()
    {
        $this->ajaxOnly(Response::FORMAT_JSON);

        if (!$addressData = Yii::$app->request->post('address')) {
            throw new yii\base\InvalidCallException('Invalid call');
        }

        if (!$address = Address::create($addressData)) {
            throw new yii\base\InvalidArgumentException('Invalid address data');
        }

        return $address->label;
    }
}
