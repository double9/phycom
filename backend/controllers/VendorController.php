<?php

namespace phycom\backend\controllers;

use phycom\backend\models\SearchVendor;
use phycom\backend\models\VendorForm;

use phycom\common\helpers\FlashMsg;
use phycom\common\models\Vendor;

use yii\web\NotFoundHttpException;
use yii\helpers\Url;
use yii;

/**
 * Class VendorController
 * @package phycom\backend\controllers
 */
class VendorController extends BaseController
{
    /**
     * @return string
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionList()
    {
        $this->checkPermission('search_vendors');

        $model = new SearchVendor();
        $dataProvider = $model->search(Yii::$app->request->get());

        return $this->render('index', ['model' => $model, 'dataProvider' => $dataProvider]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionEdit($id)
	{
		$this->checkPermission('update_vendor');
		$vendor = $this->findVendor($id);
		$model = new VendorForm($vendor);

		if ($model->load(Yii::$app->request->post())) {
		    if ($model->update()) {
		        FlashMsg::success(Yii::t('backend/shop', 'Vendor info successfully updated'));
            } else {
                FlashMsg::error($model->hasErrors() ? $model->errors : Yii::t('backend/shop', 'Error updating vendor'));
            }
        }
		return $this->render('edit', ['model' => $model]);
	}

    /**
     * @return string|yii\web\Response
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionAdd()
	{
		$this->checkPermission('create_vendor');
        $model = new VendorForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->update()) {
                FlashMsg::success(Yii::t('backend/shop', 'Vendor {id} successfully created', ['id' => $model->vendor->id]));
                return $this->redirect(Url::toRoute(['vendor/edit', 'id' => $model->vendor->id]));
            } else {
                FlashMsg::error($model->hasErrors() ? $model->errors : Yii::t('backend/shop', 'Error creating new vendor'));
            }
        }
		return $this->render('add', ['model' => $model]);
	}

	/**
	 * @param int $id
	 * @return Vendor
	 * @throws NotFoundHttpException
	 */
	protected function findVendor($id)
	{
		$vendor = Vendor::findOne($id);
		if (!$vendor) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $vendor;
	}
}
