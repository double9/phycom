<?php

namespace phycom\backend\controllers;

use phycom\backend\helpers\ProductVariantGrid;
use phycom\backend\models\product\ProductAttachmentForm;
use phycom\backend\models\product\ProductAttachmentOptionsForm;
use phycom\backend\models\product\ProductBulkUpdateForm;
use phycom\backend\models\product\ProductForm;
use phycom\backend\models\product\ProductParamForm;
use phycom\backend\models\product\ProductVariantForm;
use phycom\backend\widgets\ActiveForm;
use phycom\backend\helpers\ProductParamGrid;

use phycom\common\components\FileExportFormatter;
use phycom\common\helpers\c;
use phycom\common\helpers\FlashMsg;
use phycom\common\models\product\Product;
use phycom\common\models\product\ProductAttachment;
use phycom\common\models\product\ProductVariant;
use phycom\common\models\product\ProductParam;

use yii2tech\spreadsheet\Spreadsheet;

use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii;

/**
 * Class ProductController
 * @package phycom\backend\controllers
 */
class ProductController extends BaseController
{
	public $defaultAction = 'catalog';

    public $activeTab = 'content';

    /**
     * @param ProductForm $model
     * @return array
     * @throws \Exception
     */
    public function getSubmenuItems(ProductForm $model)
    {
        $items = [
            'content' => [
                'label'  => '<span class="far fa-sticky-note" style="margin: 0 10px 0 2px;"></span>' . Yii::t('backend/product', 'Content'),
                'view'   => 'partials/content-form',
                'params' => ['model' => $model]
            ]
        ];

        $items['attachments'] = [
            'label'  => '<span class="far fa-image" style="margin-right: 10px;"></span>' . Yii::t('backend/product', 'Attachments'),
            'view'   => 'partials/attachment-form',
            'params' => ['model' => $model->attachmentForm]
        ];

        if (Yii::$app->commerce->params->isEnabled()) {
            $items['params'] = [
                'label'   => '<span class="fab fa-buffer" style="margin: 0 10px 0 1px;"></span>' . Yii::t('backend/product', 'Params'),
                'content' => (new ProductParamGrid($model->paramForm))->render(),
            ];
        }

        if (Yii::$app->commerce->variants->isEnabled()) {
            $items['variants'] = [
                'label'   => '<span class="fas fa-code-branch" style="margin: 0 10px 0 2px;"></span>' . Yii::t('backend/product', 'Variants'),
                'content' => (new ProductVariantGrid($model->variantForm))->render(),
            ];
        }

        $items['pricing'] = [
            'label'  => '<span class="fas fa-euro-sign" style="margin: 0 10px 0 4px;"></span>' . Yii::t('backend/product', 'Pricing'),
            'view'   => 'partials/pricing-form',
            'params' => ['formModel' => $model->pricingForm, 'optionsForm' => $model->pricingOptionsForm]
        ];

        return $items;
    }


    /**
     * @param bool $export
     * @return string
     * @throws yii\base\Exception
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionCatalog($export = null)
	{
		$this->checkPermission('search_products');

        $model = Yii::$app->modelFactory->getSearchProduct(['language' => Yii::$app->lang->current]);
        $dataProvider = $model->search(Yii::$app->request->queryParams);

        if ($export) {
            return (new Spreadsheet([
                'dataProvider' => $dataProvider,
                'columns' => $model->getExporterColumns(),
                'formatter' => new FileExportFormatter([
                    'locale' => Yii::$app->language,
                    'dateFormat' => 'php:Y-m-d',
                    'datetimeFormat' => 'php:Y-m-d:H:i:s',
                    'timeFormat' => 'php:H:i:s'
                ])
            ]))->send('product_catalog_' . (new \DateTime())->format('Y-m-d\THis') . '.xls');
        }

        $bulkUpdateModel = new ProductBulkUpdateForm();
        if ($bulkUpdateModel->load(Yii::$app->request->post())) {
            if ($bulkUpdateModel->validate() && $count = $bulkUpdateModel->update()) {
                FlashMsg::success(Yii::t('backend/main', '{count} items successfully updated', ['count' => $count]));
            } else {
                FlashMsg::error($bulkUpdateModel->errors);
            }
        }

		return $this->render('catalog', [
			'dataProvider' => $dataProvider,
			'model' => $model
		]);
	}

    /**
     * @param $id - product id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     * @throws \Throwable
     */
	public function actionEdit($id)
	{
		$this->checkPermission('update_product');
		$product = $this->findProduct($id);
		return $this->addEditProduct(new ProductForm($product, Yii::$app->user->identity));
	}

    /**
     * @return mixed
     * @throws yii\web\ForbiddenHttpException
     * @throws \Throwable
     */
	public function actionAdd()
	{
		$this->checkPermission('create_product');
		$form = new ProductForm(null, Yii::$app->user->identity);
		return $this->addEditProduct($form);
	}

    /**
     * @param $id - product id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        $this->checkPermission('delete_product');
        $product = $this->findProduct($id);

        if ($product->delete()) {
            FlashMsg::success(Yii::t('backend/product', 'Product {id} successfully deleted', ['id' => $id]));
        } else {
            FlashMsg::error($product->errors);
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id - product id
     * @return array
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionUploadAttachment($id)
	{
		$this->checkPermission('update_product');
		$this->ajaxOnly(Response::FORMAT_JSON);

		$post = Yii::$app->request->post();
		unset($post['file_id']);

		$product = $this->findProduct($id);
		$model = new ProductAttachmentForm($product);
		$model->file = UploadedFile::getInstance($model, 'file[0]');
		$model->params = $post;

		$uploadedFile = $model->upload();

		return $uploadedFile ? [
			'initialPreviewThumbnails' => [
				'<img src="' . $uploadedFile->getUrl() . '">',
			],
			'append' => true
		] : ['error' => $model->lastError];
	}

    /**
     * @param int $id       - product id
     * @param int $fileId   - file id
     *
     * @return array
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionUpdateAttachment($id, $fileId)
	{
		$this->checkPermission('update_product');
		$this->ajaxOnly(Response::FORMAT_JSON);
		$postData = Yii::$app->request->post();

		if (!$attachment = ProductAttachment::findOne(['product_id' => $id, 'file_id' => $fileId])) {
			return ['error' => 'Attachment file not found'];
		}
		$form = new ProductAttachmentOptionsForm($attachment);

		if ($form->load($postData)) {
		    if ($form->save()) {
                FlashMsg::success(Yii::t('backend/product', 'Attachment file {id} metadata successfully updated', ['id' => $attachment->file_id]));
            } else {
                return ['error' => $form->errors[0]];
            }
        }

        if (isset($postData['order'])) {
            $attachment->setOrderByIndex((int) $postData['order']);
        }
		return $this->ajaxSuccess();
	}

    /**
     * @param $id - product id
     * @return array
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionDeleteAttachment($id)
	{
		$this->checkPermission('update_product');
		$this->ajaxOnly(Response::FORMAT_JSON);

		$fileId = Yii::$app->request->post('key');
		$attachment = ProductAttachment::findOne(['product_id' => $id, 'file_id' => $fileId]);
		if (!$attachment) {
			FlashMsg::error(Yii::t('backend/product', 'Attachment file not found'));
			return $this->ajaxError();
		}
		if (!$attachment->delete()) {
			FlashMsg::error($attachment->errors);
			return $this->ajaxError();
		}
		return $this->ajaxSuccess(Yii::t('backend/product', 'Product attachment removed'));
	}

    /**
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     * @throws yii\base\InvalidConfigException
     */
	public function actionAttributes()
	{
		$this->checkPermission('search_product_attributes');
		$this->ajaxOnly(Response::FORMAT_JSON);
		$query = Yii::$app->request->get('query');
		$excluded = [];//ArrayHelper::getColumn(ProductAttribute::findAll(['product_id' => Yii::$app->request->get('product')]), 'attribute');
		$attributes = Yii::$app->modelFactory->getVariant()::searchByKeyword($query, $excluded);

		$data = [];
		foreach ($attributes as $attribute) {
			$row = $attribute->toArray();
			if ($attribute->type === $attribute::TYPE_OPTION) {
				$row['options'] = ArrayHelper::map($attribute->getOptions(), 'key', 'label');
			}
			$data[] = $row;
		}
		echo yii\helpers\Json::encode($data);
	}

    /**
     * Renders a product variant grid row for a variant
     *
     * @param string $key
     * @param string $name
     * @return string
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     * @throws yii\base\Exception
     */
	public function actionGetVariantRow(string $key, string $name)
    {
        $this->checkPermission('search_product_attributes');
        $this->ajaxOnly(Response::FORMAT_HTML);

        $variantForm = new ProductVariantForm(Yii::$app->modelFactory->getProduct());
        $variantGrid = (new ProductVariantGrid($variantForm))->instance();
        $variantGrid->form = new ActiveForm($variantGrid->formOptions);

        $model = new ProductVariant();
        $model->loadDefaultValues();
        $model->name = $name;
        $variantForm->models = [$key => $model];

        return $variantGrid->renderTableRow($model, $key, 0);
    }


    /**
     * Renders a product params grid row for a param
     *
     * @param string $key
     * @param string $name
     * @return string
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionGetParamRow(string $key, string $name)
    {
        $this->checkPermission('search_product_params');
        $this->ajaxOnly(Response::FORMAT_HTML);

        $paramForm = new ProductParamForm(Yii::$app->modelFactory->getProduct());
        $paramGrid = (new ProductParamGrid($paramForm))->instance();
        $paramGrid->form = new ActiveForm($paramGrid->formOptions);

        $model = new ProductParam();
        $model->loadDefaultValues();
        $model->name = $name;
        $paramForm->models = [$key => $model];

//        return $paramGrid->renderTableRow($model, $key, 0);
        return $paramGrid->renderTableRowAjax($model, $key, 0);
    }


    /**
     * @param ProductForm $form
     * @return array|int
     * @throws NotFoundHttpException
     */
	protected function createProduct(ProductForm $form)
	{
		$this->ajaxOnly(Response::FORMAT_JSON);
		if ($form->load(Yii::$app->request->post())) {
			if ($form->update()) {
				return $form->product->id;
			} else {
				return ['error' => $form->lastError];
			}
		} else {
			throw new yii\base\InvalidCallException('Nothing to update');
		}
	}

    /**
     * Base method for actionAdd and actionEdit
     *
     * @param ProductForm $productForm
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
	protected function addEditProduct(ProductForm $productForm)
	{
		if (Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$transaction = Yii::$app->db->beginTransaction();
			try {

			    if ($errors = $this->loadProductData($productForm, true)) {
                    $transaction->rollBack();
                    return $errors;
                };
				FlashMsg::success(Yii::t('backend/product', 'Product successfully updated'));
				$transaction->commit();
				$returnUrl = $this->createReturnUrl($productForm);
                return [
                    'id'        => $productForm->product->id,
                    'url'       => $returnUrl,
                    'uploadUrl' => Url::toRoute(['product/upload-attachment', 'id' => $productForm->product->id])
                ];

			} catch (\Exception $e) {
				$transaction->rollBack();
				throw $e;
			}
		} else if (Yii::$app->request->isPost && Yii::$app->request->post('action') === self::AFTER_SAVE_DUPLICATE) {
            $this->loadProductData($productForm);
		}

        return $this->render(c::param('productPageView', 'edit'), [
            'items'           => $this->getSubmenuItems($productForm),
            'active'          => $this->activeTab,
            'productForm'     => $productForm,
        ]);
	}

    /**
     * @param ProductForm $productForm
     * @param bool $save
     * @return array|null
     * @throws \Exception
     * @throws \Throwable
     */
	protected function loadProductData(ProductForm $productForm, $save = false)
    {
        $translationForm = $productForm->translationForm;
        $productParamForm = $productForm->paramForm;
        $variantForm = $productForm->variantForm;
        $pricingForm = $productForm->pricingForm;
        $pricingOptionsForm = $productForm->pricingOptionsForm;
        $attachmentForm = $productForm->attachmentForm;

        if ($productForm->load(Yii::$app->request->post()) && $save && !$productForm->update()) {
            return ['error' => $productForm->lastError];
        }
        if ($variantForm->load(Yii::$app->request->post()) && $save && !$variantForm->save()) {
            return ['error' => $variantForm->lastError];
        }
        if ($attachmentForm->load(Yii::$app->request->post()) && $save && !$attachmentForm->saveNew()) {
            return ['error' => $attachmentForm->lastError];
        }
        if ($translationForm->load(Yii::$app->request->post()) && $save && !$translationForm->save()) {
            return ['error' => $translationForm->lastError];
        }
        if ($pricingOptionsForm->load(Yii::$app->request->post()) && $save && !$pricingOptionsForm->update()) {
            return ['error' => $pricingOptionsForm->lastError];
        }
        if ($pricingForm->load(Yii::$app->request->post()) && $save && !$pricingForm->save()) {
            return ['error' => $pricingForm->lastError];
        }
        if ($productParamForm->load(Yii::$app->request->post()) && $save && !$productParamForm->save()) {
            return ['error' => $productParamForm->lastError];
        }
        return null;
    }

    /**
     * @param ProductForm $form
     * @return string|array|Response
     * @throws yii\base\InvalidConfigException
     */
    protected function createReturnUrl(ProductForm $form)
    {
        switch (Yii::$app->request->post('afterSave')) {
            case self::AFTER_SAVE_NEW:
                return Yii::$app->urlManager->createAbsoluteUrl(['/product/add']);
            case self::AFTER_SAVE_DUPLICATE:
                return [
                    Yii::$app->urlManager->createAbsoluteUrl(['/product/add']),
                    ArrayHelper::merge(
                        [
                            'action'                      => self::AFTER_SAVE_DUPLICATE,
                            Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                        ],
                        $form->exportFormAttributes(['categories', 'tags']),
                        $form->translationForm->exportFormAttributes(['language', 'title', 'outline', 'description', 'status', 'meta_title', 'meta_keywords', 'meta_description']),
                        $form->attachmentForm->exportFormAttributes(['file_id', 'order']),
                        $form->paramForm->exportFormAttributes(['name', 'value', 'is_public']),
                        $form->variantForm->exportFormAttributes(['name', 'status', 'user_select']),
                        $form->pricingForm->exportFormAttributes(['num_units', 'unit_type', 'price', 'discount_amount']),
                        $form->pricingOptionsForm->exportFormAttributes(['priceUnit', 'priceUnitMode', 'discount'])
                    )
                ];
            case self::AFTER_SAVE_CLOSE:
                return Yii::$app->urlManager->createAbsoluteUrl(['/product/catalog']);
            default:
                return Yii::$app->urlManager->createAbsoluteUrl(['/product/edit', 'id' => $form->product->id]) . Yii::$app->request->post('hash', '');
        }
    }

	/**
	 * @param $id
	 * @return Product
	 * @throws NotFoundHttpException
	 */
	protected function findProduct($id)
	{
		$product = Yii::$app->modelFactory->getProduct()::findOne($id);
		if (!$product) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $product;
	}
}
