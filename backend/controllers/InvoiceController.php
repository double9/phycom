<?php

namespace phycom\backend\controllers;

use phycom\backend\models\InvoiceForm;
use phycom\backend\models\PaymentForm;

use phycom\common\helpers\FlashMsg;
use phycom\common\models\Invoice;

use yii\web\NotFoundHttpException;
use yii;

/**
 * Class InvoiceController
 * @package phycom\backend\controllers
 */
class InvoiceController extends BaseController
{
    /**
     * @param yii\base\Action $action
     * @return bool
     * @throws NotFoundHttpException
     */
    public function beforeAction($action)
    {
        $this->checkEnabled(Yii::$app->commerce);
        return parent::beforeAction($action);
    }

    /**
     * @return string
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionIndex()
	{
		$this->checkPermission('search_invoices');

		$model = Yii::$app->modelFactory->getSearchInvoice();
		$dataProvider = $model->search(Yii::$app->request->get());

		return $this->render('index', ['model' => $model, 'dataProvider' => $dataProvider]);
	}

    /**
     * @return string
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionAdd()
	{
		$this->checkPermission('create_invoice');
		$model = new InvoiceForm();

		return $this->render('add', ['model' => $model]);
	}

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionEdit($id)
	{
		$this->checkPermission('update_invoice');
		$invoice = $this->findInvoice($id);
		$model = new InvoiceForm($invoice);

		return $this->render('edit', ['model' => $model]);
	}

    /**
     * @param $id
     * @return string|yii\web\Response
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionAddPayment($id)
	{
		$this->checkPermission('create_payment');
		$invoice = $this->findInvoice($id);

		$model = new PaymentForm();
		$model->invoice = $invoice;

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			FlashMsg::success(Yii::t('backend/payment', 'Payment successfully created'));
			return $this->redirect(['/invoice/edit', 'id' => $id]);
		}
		return $this->render('/payment-list/add', ['model' => $model]);
	}

	/**
	 * @param int $id
	 * @return Invoice
	 * @throws NotFoundHttpException
	 */
	protected function findInvoice($id)
	{
		$invoice = Yii::$app->modelFactory->getInvoice()::findOne($id);
		if (!$invoice) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $invoice;
	}
}
