<?php

namespace phycom\backend\controllers;

use phycom\backend\models\SearchShop;
use phycom\backend\models\ShopForm;
use phycom\common\helpers\FlashMsg;
use phycom\common\models\attributes\PostType;
use phycom\common\models\Shop;

use yii\web\NotFoundHttpException;
use yii\helpers\Url;
use yii;

/**
 * Class ShopController
 * @package phycom\backend\controllers
 */
class ShopController extends BaseController
{
    public $activeTab = 'info';

    /**
     * @param yii\base\Action $action
     * @return bool
     * @throws NotFoundHttpException
     */
    public function beforeAction($action)
    {
        $this->checkEnabled([Yii::$app->commerce]);
        return parent::beforeAction($action);
    }

    /**
     * @param ShopForm $model
     * @return array
     */
    public function getSubmenuItems(ShopForm $model)
    {
        $items = [
            'info' => [
                'label' => '<span class="fas fa-info" style="margin: 0 14px 0 4px;"></span>' . Yii::t('backend/shop', 'Shop info'),
                'view' => 'partials/info',
                'params' => ['model' => $model]
            ]
        ];
        if (!$model->shop->isNewRecord) {
            $items['open'] = [
                'label'   => '<span class="far fa-calendar" style="margin-right: 10px;"></span>' . Yii::t('backend/shop', 'Open weekdays'),
                'view'    => 'partials/open-weekdays',
                'params'  => ['formModel' => $model->openForm],
                'visible' => Yii::$app->commerce->shopOpen->isEnabled()
            ];
            $items['closed'] = [
                'label'   => '<span class="far fa-calendar" style="margin-right: 10px;"></span>' . Yii::t('backend/shop', 'Closed schedule'),
                'view'    => 'partials/closed-schedule',
                'params'  => ['model' => $model->closedForm],
                'visible' => Yii::$app->commerce->shopClosed->isEnabled()
            ];
            $items['supply'] = [
                'label'   => '<span class="fas fa-truck" style="margin-right: 10px;"></span>' . Yii::t('backend/shop', 'Supply days'),
                'view'    => 'partials/supply-weekdays',
                'params'  => ['formModel' => $model->supplyForm],
                'visible' => Yii::$app->commerce->shopSupply->isEnabled()
            ];
            $items['content'] = [
                'label'          => '<span class="far fa-sticky-note" style="margin: 0 10px 0 1px;"></span>' . Yii::t('backend/shop', 'Content'),
                'view'           => 'partials/content',
                'params'         => ['model' => $model->contentForm],
                'formCollection' => true,
                'formSubmitAjax' => true,
                'formAction'     => $model->contentForm->post->isNewRecord
                    ? Url::toRoute(['/post/add', 'type' => PostType::SHOP])
                    : Url::toRoute(['/post/edit', 'id' => $model->contentForm->post->id]),
                'visible'        => Yii::$app->commerce->shopContent->isEnabled()
            ];
        }
        return $items;
    }

    /**
     * @return string
     * @throws yii\base\Exception
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionIndex()
	{
		$this->checkPermission('search_shops');

		$model = new SearchShop();
		$dataProvider = $model->search(Yii::$app->request->get());

		return $this->render('index', ['model' => $model, 'dataProvider' => $dataProvider]);
	}

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws yii\db\Exception
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionEdit($id)
	{
		$this->checkPermission('update_shop');
		$shop = $this->findShop($id);
		$model = new ShopForm($shop);

		$this->checkActiveTab();

		if ($model->load(Yii::$app->request->post())) {
		    if ($model->update()) {
		        FlashMsg::success(Yii::t('backend/shop', 'Shop info successfully updated'));
            } else {
                FlashMsg::error($model->hasErrors() ? $model->errors : Yii::t('backend/shop', 'Error updating shop'));
            }
        }

        if ($model->closedForm->load(Yii::$app->request->post())) {
            if ($model->closedForm->save()) {
                FlashMsg::success(Yii::t('backend/shop', 'Closed schedule successfully updated'));
            } else {
                FlashMsg::error($model->closedForm->hasErrors() ? $model->closedForm->errors : Yii::t('backend/shop', 'Error updating schedule'));
            }
        }

        if ($model->openForm->load(Yii::$app->request->post())) {
            if ($model->openForm->save()) {
                FlashMsg::success(Yii::t('backend/shop', 'Open times successfully updated'));
            } else {
                FlashMsg::error($model->openForm->hasErrors() ? $model->openForm->errors : Yii::t('backend/shop', 'Error updating schedule'));
            }
        }

        if ($model->supplyForm->load(Yii::$app->request->post())) {
            if ($model->supplyForm->save()) {
                FlashMsg::success(Yii::t('backend/shop', 'Supply dates successfully updated'));
            } else {
                FlashMsg::error($model->supplyForm->hasErrors() ? $model->supplyForm->errors : Yii::t('backend/shop', 'Error updating schedule'));
            }
        }
        return $this->render('edit', ['items' => $this->getSubmenuItems($model), 'active' => $this->activeTab, 'model' => $model]);
	}

    /**
     * @return string|yii\web\Response
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionAdd()
	{
		$this->checkPermission('create_shop');
        $model = new ShopForm();

        $this->checkActiveTab();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->update()) {
                FlashMsg::success(Yii::t('backend/shop', 'Shop info successfully updated'));
                return $this->redirect(Url::toRoute(['shop/edit', 'id' => $model->shop->id]));
            } else {
                FlashMsg::error($model->hasErrors() ? $model->errors : Yii::t('backend/shop', 'Error updating shop'));
            }
        }
		return $this->render('add', ['items' => $this->getSubmenuItems($model), 'active' => 'info', 'model' => $model]);
	}

	protected function checkActiveTab()
    {
        if ($tab = Yii::$app->request->post('tab')) {
            $this->activeTab = $tab;
        }
    }

	/**
	 * @param int $id
	 * @return Shop
	 * @throws NotFoundHttpException
	 */
	protected function findShop($id)
	{
		$shop = Shop::findOne($id);
		if (!$shop) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $shop;
	}
}
