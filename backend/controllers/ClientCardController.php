<?php

namespace phycom\backend\controllers;

use phycom\backend\models\DiscountRuleBulkUpdateForm;
use phycom\backend\models\DiscountRuleForm;
use phycom\backend\models\SearchDiscountRule;

use phycom\common\helpers\FlashMsg;
use phycom\common\models\attributes\DiscountRuleStatus;
use phycom\common\models\attributes\DiscountRuleType;
use phycom\common\models\DiscountRule;

use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii;

/**
 * Class ClientCardController
 * @package phycom\backend\controllers
 */
class ClientCardController extends BaseController
{
    const AFTER_SAVE_NEW = 'new';
    const AFTER_SAVE_DUPLICATE = 'duplicate';
    const AFTER_SAVE_CLOSE = 'close';

    /**
     * @param yii\base\Action $action
     * @return bool
     * @throws NotFoundHttpException
     */
    public function beforeAction($action)
    {
        $this->checkEnabled(Yii::$app->commerce->clientCards);
        return parent::beforeAction($action);
    }

    /**
     * @return string
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionIndex()
	{
		$this->checkPermission('search_client_cards');

		$model = new SearchDiscountRule();
		$model->type = DiscountRuleType::CLIENT_CARD;
		$dataProvider = $model->search(Yii::$app->request->get());

        $bulkUpdateModel = new DiscountRuleBulkUpdateForm();
        if ($bulkUpdateModel->load(Yii::$app->request->post())) {
            if ($bulkUpdateModel->validate() && ($count = $bulkUpdateModel->update())) {
                FlashMsg::success(Yii::t('backend/main', '{count} items successfully updated', ['count' => $count]));
            } else {
                FlashMsg::error($bulkUpdateModel->errors);
            }
        }

		return $this->render('index', ['model' => $model, 'dataProvider' => $dataProvider]);
	}

    /**
     * @return string|yii\web\Response
     * @throws yii\base\InvalidConfigException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionAdd()
    {
        $this->checkPermission('create_client_card');

        $model = new DiscountRule();
        $model->type = DiscountRuleType::CLIENT_CARD;
        $model->status = DiscountRuleStatus::ACTIVE;

        return $this->edit($model, Yii::$app->request->post('afterSave'));
    }

    /**
     * @param $id
     * @return string|yii\web\Response
     * @throws NotFoundHttpException
     * @throws yii\base\InvalidConfigException
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionEdit($id)
    {
        $this->checkPermission('update_client_card');
        $model = $this->findDiscountCard($id);
        return $this->edit($model, Yii::$app->request->post('afterSave'));
    }

    /**
     * @param $id
     * @return yii\web\Response
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        $this->checkPermission('delete_client_card');

        $model = $this->findDiscountCard($id);
        if ($model->updateStatus(DiscountRuleStatus::DELETED)) {
            FlashMsg::success(Yii::t('backend/user', 'Client card {id} successfully deleted.', ['id' => $model->id]));
            return $this->redirect(Url::toRoute(['client-card/index']));
        } else {
            FlashMsg::error($model->errors);
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * @param DiscountRule $discountCard
     * @param null $afterSaveAction
     * @return string|yii\web\Response
     * @throws yii\base\InvalidConfigException
     */
    protected function edit(DiscountRule $discountCard, $afterSaveAction = null)
    {
        $formModel = new DiscountRuleForm($discountCard);
        $newRecord = $discountCard->isNewRecord;

        if (Yii::$app->request->isGet) {
            $formModel->load(Yii::$app->request->get());
        }
        if ($formModel->load(Yii::$app->request->post())) {
            if ($formModel->update()) {
                FlashMsg::success($newRecord ?
                    Yii::t('backend/user', 'Client card {id} successfully created', ['id' => $discountCard->id]) :
                    Yii::t('backend/user', 'Client card {id} updated', ['id' => $discountCard->id])
                );
            } else {
                FlashMsg::error($formModel->errors);
                return $this->render('edit', ['model' => $formModel]);
            }
        }

        switch ($afterSaveAction) {
            case self::AFTER_SAVE_NEW:
                return $this->redirect(Url::toRoute(['client-card/add']));
            case self::AFTER_SAVE_DUPLICATE:
                return $this->redirect($this->createDuplicateUrl($formModel));
            case self::AFTER_SAVE_CLOSE:
                return $this->redirect(Url::toRoute(['client-card/index']));
            default:
                if ($newRecord && !$discountCard->isNewRecord) {
                    return $this->redirect(Url::toRoute(['client-card/edit', 'id' => $discountCard->id]));
                }
                return $this->render('edit', ['model' => $formModel]);
        }
    }

    /**
     * @param DiscountRuleForm $model
     * @return string
     * @throws yii\base\InvalidConfigException
     */
    protected function createDuplicateUrl(DiscountRuleForm $model)
    {
        $a = $model->attributes;

        unset($a['code']);
        unset($a['personalCode']);
        unset($a['birthday']);

        return Url::toRoute(ArrayHelper::merge(['client-card/add'], [$model->formName() => $a]));
    }

	/**
	 * @param int $id
	 * @return DiscountRule
	 * @throws NotFoundHttpException
	 */
	protected function findDiscountCard($id)
	{
		$model = DiscountRule::findOne($id);
		if (!$model) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $model;
	}
}
