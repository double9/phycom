<?php

namespace phycom\backend\controllers;

use phycom\backend\models\comment\CommentBulkUpdateForm;
use phycom\backend\models\comment\SearchComment;

use phycom\common\helpers\FlashMsg;
use phycom\common\models\attributes\CommentStatus;
use phycom\common\models\Comment;

use yii\web\NotFoundHttpException;
use yii;

/**
 * Class CommentController
 * @package phycom\backend\controllers
 */
class CommentController extends BaseController
{
    /**
     * @param yii\base\Action $action
     * @return bool
     * @throws NotFoundHttpException
     */
    public function beforeAction($action)
    {
        $this->checkEnabled(Yii::$app->comments);
        return parent::beforeAction($action);
    }

    /**
     * @return string
     * @throws yii\web\ForbiddenHttpException
     * @throws yii\base\Exception
     */
	public function actionIndex()
	{
		$this->checkPermission('search_comments');

		$model = new SearchComment();
		$dataProvider = $model->search(Yii::$app->request->get());

        $bulkUpdateModel = new CommentBulkUpdateForm();
        if ($bulkUpdateModel->load(Yii::$app->request->post())) {
            if ($bulkUpdateModel->validate() && ($count = $bulkUpdateModel->update())) {
                FlashMsg::success(Yii::t('backend/main', '{count} items successfully updated', ['count' => $count]));
            } else {
                FlashMsg::error($bulkUpdateModel->errors);
            }
        }

		return $this->render('index', ['model' => $model, 'dataProvider' => $dataProvider]);
	}

    /**
     * @param int $id
     * @return string
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionEdit($id)
	{
		$this->checkPermission('update_comment');
		$comment = $this->findComment($id);

        if ($comment->load(Yii::$app->request->post())) {
            if ($comment->save(true, ['status'])) {
                FlashMsg::success(Yii::t('backend/comment', 'Comment {id} successfully updated', ['id' => $comment->id]));
            } else {
                FlashMsg::error($comment->errors);
            }
        }

		return $this->render('edit', ['model' => $comment]);
	}

    /**
     * @param int $id
     * @return yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionDelete($id)
    {
        $this->checkPermission('delete_comment');
        $comment = $this->findComment($id);
        $comment->status = CommentStatus::DELETED;

        if ($comment->update()) {
            FlashMsg::success(Yii::t('backend/comment', 'Comment {id} successfully deleted', ['id' => $id]));
        } else {
            FlashMsg::error($comment->errors);
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

	/**
	 * @param int $id
	 * @return Comment
	 * @throws NotFoundHttpException
	 */
	protected function findComment($id)
	{
		$comment = Comment::findOne($id);
		if (!$comment) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $comment;
	}
}
