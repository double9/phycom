<?php

namespace phycom\backend\controllers;


use phycom\backend\models\SiteSettingForm;
use phycom\backend\models\VendorForm;
use phycom\common\helpers\FlashMsg;
use phycom\common\models\Vendor;

use phycom\common\modules\payment\interfaces\PaymentMethodInterface;
use phycom\common\modules\payment\methods\PaymentMethod;
use phycom\common\modules\payment\Module as PaymentModule;

use phycom\common\modules\delivery\Module as DeliveryModule;
use phycom\common\modules\delivery\interfaces\DeliveryMethodInterface;
use phycom\common\modules\delivery\methods\DeliveryMethod;

use yii;

/**
 * Class SettingsController
 * @package phycom\backend\controllers
 *
 * @property-read array $submenuItems
 */
class SettingsController extends BaseController
{
    public $activeTab = 'vendor';

	public function getSubmenuItems()
	{
        $items = [
            'global' => [
                'label' => Yii::t('backend/settings', 'Global'),
                'view' => '/settings/global',
                'params' => ['model' => $this->getGlobalSettingsForm()],
            ],
            'vendor' => [
                'label' => Yii::t('backend/vendor', 'Vendor'),
                'view' => '/vendor/edit',
                'params' => ['model' => $this->getVendorForm(Yii::$app->vendor)]
            ]
		];

        if (Yii::$app->commerce->isEnabled() && Yii::$app->commerce->payment->isEnabled() && Yii::$app->user->can('update_payment_settings')) {
            $items['payment-methods'] = [
                'label' => Yii::t('backend/settings', 'Payment methods'),
                'view' => '/settings/payment-methods',
                'params' => ['models' => $this->getPaymentMethodSettings()],
                'formCollection' => true
            ];
        }

        if (Yii::$app->commerce->isEnabled() && Yii::$app->commerce->delivery->isEnabled() && Yii::$app->user->can('update_delivery_settings')) {
            $items['delivery-methods'] = [
                'label' => Yii::t('backend/settings', 'Delivery methods'),
                'view' => '/settings/delivery-methods',
                'params' => ['models' => $this->getDeliveryMethodSettings()],
                'formCollection' => true
            ];
        }

        return $items;
	}

    /**
     * @return string
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionIndex()
	{
	    $this->checkPermission('update_global_settings');
        return $this->render('index', ['items' => $this->getSubmenuItems(), 'active' => $this->activeTab]);
	}

	protected function getPaymentMethodSettings()
    {
        $models = [];
        /**
         * @var PaymentModule $payment
         */
        $payment = Yii::$app->getModule('payment');
        $modelUpdate = false;
        $err = 0;
        foreach ($payment->modules as $id => $moduleParams) {
            /**
             * @var PaymentMethod|PaymentMethodInterface $paymentMethod
             */
            $paymentMethod = $payment->getModule($id);
            if ($paymentMethod->useSettings) {
                $model = $paymentMethod->getSettings();
                if ($model->load(Yii::$app->request->post(), $model->formName())) {
                    $this->checkPermission('update_payment_settings');
                    $this->activeTab = 'payment-methods';
                    $modelUpdate = true;
                    if (!$model->save()) {
                        $err++;
                        FlashMsg::error(Yii::t('backend/settings', 'Error saving {form}', ['form' => $model->formLabel()]));
                    }
                }
                $models[] = $model;
            }
        }
        if ($modelUpdate && Yii::$app->request->isPost && !$err) {
            FlashMsg::success(Yii::t('backend/settings', 'Settings successfully updated'));
        }
        return $models;
    }


    protected function getDeliveryMethodSettings()
    {
        $models = [];
        /**
         * @var DeliveryModule $delivery
         */
        $delivery = Yii::$app->getModule('delivery');
        $modelUpdate = false;
        $err = 0;

        if ($delivery->useSettings) {

            $model = $delivery->getSettings();
            if ($model->load(Yii::$app->request->post(), $model->formName())) {
                $this->checkPermission('update_delivery_settings');
                $this->activeTab = 'delivery-methods';
                $modelUpdate = true;
                if (!$model->save()) {
                    $err++;
                    FlashMsg::error(Yii::t('backend/settings', 'Error saving {form}', ['form' => $model->formLabel()]));
                }
            }
            $models[] = $model;
        }

        foreach ($delivery->modules as $id => $moduleParams) {
            /**
             * @var DeliveryMethod|DeliveryMethodInterface $deliveryMethod
             */
            $deliveryMethod = $delivery->getModule($id);
            if ($deliveryMethod->useSettings) {
                $model = $deliveryMethod->getSettings();
                if ($model->load(Yii::$app->request->post(), $model->formName())) {
                    $this->checkPermission('update_delivery_settings');
                    $this->activeTab = 'delivery-methods';
                    $modelUpdate = true;
                    if (!$model->save()) {
                        $err++;
                        FlashMsg::error(Yii::t('backend/settings', 'Error saving {form}', ['form' => $model->formLabel()]));
                    }
                }
                $models[] = $model;
            }
        }
        if ($modelUpdate && Yii::$app->request->isPost && !$err) {
            FlashMsg::success(Yii::t('backend/settings', 'Settings successfully updated'));
        }
        return $models;
    }

    /**
     * @return SiteSettingForm
     */
	protected function getGlobalSettingsForm()
    {
        $model = new SiteSettingForm();
        if ($model->load(Yii::$app->request->post())) {
            $this->checkPermission('update_global_settings');
            $this->activeTab = 'global';

            if ($model->update()) {
                FlashMsg::success(Yii::t('backend/main', 'Settings successfully updated'));
            } else {
                FlashMsg::error($model->errors);
            }
        }
        return $model;
    }

    /**
     * @param Vendor|null $vendor
     * @return VendorForm
     * @throws \Exception
     */
    protected function getVendorForm(Vendor $vendor = null)
    {
        $model = new VendorForm($vendor);
        if ($model->load(Yii::$app->request->post())) {
            $this->checkPermission('update_vendor');
            $this->activeTab = 'vendor';

            if ($model->load(Yii::$app->request->post())) {
                if ($model->update()) {
                    FlashMsg::success(Yii::t('backend/shop', 'Vendor successfully updated'));
                } else {
                    FlashMsg::error($model->hasGlobalErrors() ? $model->globalErrors : Yii::t('backend/shop', 'Error updating vendor'));
                }
            }
        }
        return $model;
    }
}
