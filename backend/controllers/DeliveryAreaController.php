<?php

namespace phycom\backend\controllers;


use phycom\backend\models\DeliveryAreaBulkUpdateForm;
use phycom\backend\models\DeliveryAreaForm;
use phycom\backend\models\SearchDeliveryArea;

use phycom\common\helpers\FlashMsg;
use phycom\common\modules\delivery\models\DeliveryArea;

use yii\web\NotFoundHttpException;
use yii;

/**
 * Class DeliveryAreaController
 * @package phycom\backend\controllers
 */
class DeliveryAreaController extends BaseController
{
    /**
     * @param yii\base\Action $action
     * @return bool
     * @throws NotFoundHttpException
     */
    public function beforeAction($action)
    {
        $this->checkEnabled([Yii::$app->commerce, Yii::$app->commerce->delivery]);
        return parent::beforeAction($action);
    }

    /**
     * @return string
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionIndex()
	{
		$this->checkPermission('search_delivery_areas');

		$model = new SearchDeliveryArea();
		$dataProvider = $model->search(Yii::$app->request->get());


        $bulkUpdateModel = new DeliveryAreaBulkUpdateForm();
        if ($bulkUpdateModel->load(Yii::$app->request->post())) {
            if ($bulkUpdateModel->validate() && ($count = $bulkUpdateModel->update())) {
                FlashMsg::success(Yii::t('backend/main', '{count} items successfully updated', ['count' => $count]));
            } else {
                FlashMsg::error($bulkUpdateModel->errors);
            }
        }

		return $this->render('index', ['model' => $model, 'dataProvider' => $dataProvider]);
	}

    /**
     * @return string
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionAdd()
	{
		$this->checkPermission('create_delivery_area');
		$model = new DeliveryAreaForm();

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
		    if ($model->save()) {
                FlashMsg::success(Yii::t('backend/review', 'Delivery area {id} successfully created', ['id' => $model->deliveryArea->id]));
            } else {
                FlashMsg::error($model->errors);
            }
        }

		return $this->render('add', ['model' => $model]);
	}

    /**
     * @param int $id
     * @return string
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionEdit($id)
	{
		$this->checkPermission('update_delivery_area');
		$area = $this->findArea($id);

        if ($area->load(Yii::$app->request->post())) {
            if ($area->save(true, ['status', 'code', 'price', 'area_code'])) {
                FlashMsg::success(Yii::t('backend/review', 'Delivery area {id} successfully updated', ['id' => $area->id]));
            } else {
                FlashMsg::error($area->errors);
            }
        }

		return $this->render('edit', ['model' => $area]);
	}

    /**
     * @param int $id
     * @return yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        $this->checkPermission('delete_delivery_area');
        $area = $this->findArea($id);

        if ($area->delete()) {
            FlashMsg::success(Yii::t('backend/product', 'Delivery area {id} deleted', ['id' => $id]));
        } else {
            FlashMsg::error($area->errors);
        }
        return $this->redirect(Yii::$app->request->referrer);
    }


	/**
	 * @param int $id
	 * @return DeliveryArea
	 * @throws NotFoundHttpException
	 */
	protected function findArea($id)
	{
		$shipment = DeliveryArea::findOne($id);
		if (!$shipment) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $shipment;
	}
}
