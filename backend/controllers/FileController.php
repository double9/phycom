<?php

namespace phycom\backend\controllers;


class FileController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            ['allow' => true, 'actions' => ['download'], 'roles' => ['?','@']]
        ];
        return $behaviors;
    }

	public function actions()
	{
		return [
			'download' => [
				'class' => FileDownloadAction::class
			],
		];
	}
}