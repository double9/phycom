<?php

namespace phycom\backend\controllers;

use phycom\backend\models\PostAttachmentForm;
use phycom\backend\models\PostAttachmentOptionsForm;
use phycom\backend\models\PostForm;
use phycom\backend\models\SearchPost;

use phycom\common\helpers\FlashMsg;
use phycom\common\models\attributes\PostStatus;
use phycom\common\models\attributes\PostType;
use phycom\common\models\File;
use phycom\common\models\Post;
use phycom\common\models\PostAttachment;

use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\web\Response;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii;

/**
 * Class PostController
 * @package phycom\backend\controllers
 */
class PostController extends BaseController
{
    /**
     * @return string
     * @throws yii\base\Exception
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionIndex()
	{
        $this->checkEnabled(Yii::$app->blog) && $this->checkPermission('search_posts');

		$model = new SearchPost();
        $model->language = Yii::$app->lang->default;
		$model->type = PostType::POST;

		$dataProvider = $model->search(Yii::$app->request->get());

		return $this->render('index', ['model' => $model, 'dataProvider' => $dataProvider]);
	}

    /**
     * @return string
     * @throws yii\base\Exception
     * @throws yii\web\ForbiddenHttpException
     */
	public function actionPages()
    {
        $this->checkPermission('search_pages');

        $model = new SearchPost();
        $model->language = Yii::$app->lang->default;
        $model->type = PostType::PAGE;
        $dataProvider = $model->search(Yii::$app->request->get());

        return $this->render('pages', ['model' => $model, 'dataProvider' => $dataProvider]);
    }

    /**
     * @return string
     * @throws yii\base\Exception
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionLandingPages()
    {
        $this->checkPermission('search_landing_pages');

        $model = new SearchPost();
        $model->language = Yii::$app->lang->current;
        $model->type = PostType::LAND;
        $dataProvider = $model->search(Yii::$app->request->get());

        return $this->render('landing-pages', ['model' => $model, 'dataProvider' => $dataProvider]);
    }

    /**
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     * @throws \Throwable
     */
	public function actionEdit($id)
	{
		$post = $this->findPost($id);

        switch ((string)$post->type) {
            case PostType::PAGE:
                $this->checkPermission('update_page');
                break;
            case PostType::LAND:
                $this->checkPermission('update_landing_page');
                break;
            default:
                $this->checkPermission('update_post');
        }
        return $this->addEditPost(new PostForm($post->type, $post));
	}

    /**
     * @param string $type
     * @return mixed
     * @throws yii\web\ForbiddenHttpException
     * @throws \Throwable
     */
	public function actionAdd($type = PostType::POST)
	{
        switch ((string)$type) {
            case PostType::PAGE:
                $this->checkPermission('create_page');
                break;
            case PostType::LAND:
                $this->checkPermission('create_landing_page');
                break;
            default:
                $this->checkPermission('create_post');
        }
        return $this->addEditPost(new PostForm(new PostType($type)));
	}

    /**
     * @return mixed
     * @throws yii\web\ForbiddenHttpException
     * @throws \Throwable
     */
    public function actionAddPage()
    {
        $this->checkPermission('create_page');
        $type = new PostType(PostType::PAGE);
        return $this->addEditPost(new PostForm($type));
    }

    /**
     * @return mixed
     * @throws yii\web\ForbiddenHttpException
     * @throws \Throwable
     */
    public function actionAddLandingPage()
    {
        $this->checkPermission('create_landing_page');
        $type = new PostType(PostType::LAND);
        return $this->addEditPost(new PostForm($type));
    }

    /**
     * @param $id - post id
     * @return array
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionEditorUpload($id)
    {
        $this->checkPermission('update_post');
        Yii::$app->response->format = Response::FORMAT_JSON;

        $post = $this->findPost($id);
        $model = new PostAttachmentForm($post);
        $model->file = UploadedFile::getInstanceByName('upload');

        $uploadedFile = $model->upload();

        return $uploadedFile ? [
            'uploaded' => true,
            'url' => $uploadedFile->getUrl()
        ] : [
            'uploaded' => false,
            'error' => ['message' => $model->lastError]
        ];
    }


    /**
     * @param $id - post id
     * @return array
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionUploadAttachment($id)
    {
        $this->checkPermission('update_post');
        $this->ajaxOnly(Response::FORMAT_JSON);

        $postData = Yii::$app->request->post();
        unset($postData['file_id']);

        $post = $this->findPost($id);
        $model = new PostAttachmentForm($post);
        $model->file = UploadedFile::getInstance($model, 'file[0]');
        $model->params = $postData;

        $uploadedFile = $model->upload();

        return $uploadedFile ? [
            'initialPreviewThumbnails' => [
                '<img src="' . $uploadedFile->getUrl() . '">',
            ],
            'append' => true
        ] : ['error' => $model->lastError];
    }

    /**
     * @param $id
     * @param $fileId
     * @return array
     * @throws NotFoundHttpException
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionUpdateAttachment($id, $fileId)
    {
        $this->checkPermission('update_post');
        $this->ajaxOnly(Response::FORMAT_JSON);
        $postData = Yii::$app->request->post();

        if (!$attachment = PostAttachment::findOne(['post_id' => $id, 'file_id' => $fileId])) {
            return ['error' => 'Attachment file not found'];
        }
        $form = new PostAttachmentOptionsForm($attachment);

        if ($form->load($postData)) {
            if ($form->save()) {
                FlashMsg::success(Yii::t('backend/post', 'Attachment file {id} metadata successfully updated', ['id' => $attachment->file_id]));
            } else {
                return ['error' => $form->errors[0]];
            }
        }

        if (isset($postData['order'])) {
            $attachment->setOrderByIndex((int) $postData['order']);
        }
        return $this->ajaxSuccess();
    }

    /**
     * @param int $id
     * @return array
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionDeleteAttachment($id)
    {
        $this->checkPermission('update_post');
        $this->ajaxOnly(Response::FORMAT_JSON);

        $fileId = Yii::$app->request->post('key');
        $attachment = PostAttachment::findOne(['post_id' => $id, 'file_id' => $fileId]);
        if (!$attachment) {
            FlashMsg::error(Yii::t('backend/post', 'Attachment file not found'));
            return $this->ajaxError();
        }
        if (!$attachment->delete()) {
            FlashMsg::error($attachment->errors);
            return $this->ajaxError();
        }
        return $this->ajaxSuccess(Yii::t('backend/post', 'Post attachment removed'));
    }

    /**
     * @param int $id - post id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        $this->checkPermission('delete_post');
        $post = $this->findPost($id);

        if ($post->delete()) {
            FlashMsg::success(Yii::t('backend/post', '{post_type} {id} successfully deleted', ['post_type' => $post->type->label, 'id' => $id]));
        } else {
            FlashMsg::error($post->errors);
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Base method for actionAdd and actionEdit
     *
     * @param PostForm $postForm
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    protected function addEditPost(PostForm $postForm)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($postForm->load(Yii::$app->request->post())) {
                    if (!$postForm->update()) {
                        $transaction->rollBack();
                        return $this->ajaxError(null, ['error' => $postForm->lastError]);
                    }
                }
                if ($postForm->getTranslationForm()->load(Yii::$app->request->post())) {
                    if (!$postForm->getTranslationForm()->save()) {
                        $transaction->rollBack();
                        return $this->ajaxError(null, ['error' => $postForm->getTranslationForm()->lastError]);
                    }
                }
                FlashMsg::success(Yii::t('backend/post', '{type} successfully updated', ['type' => ucfirst($postForm->post->type->label)]));
                $transaction->commit();

                $response = [
                    'id'        => $postForm->post->id,
                    'url'       => $this->createReturnUrl($postForm),
                    'uploadUrl' => Yii::$app->urlManager->createAbsoluteUrl(['post/upload-attachment', 'id' => $postForm->post->id])
                ];
                return $response;

            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }

        switch ($postForm->post->type) {
            case PostType::LAND:
                $view = 'edit-landing-page';
                break;
            case PostType::PAGE:
                $view = 'edit-page';
                break;
            default:
                $view = 'edit-post';
        }

        return $this->render($view, ['model' => $postForm]);
    }

    /**
     * @param PostForm $form
     * @return string|Response
     * @throws yii\base\InvalidConfigException
     */
    protected function createReturnUrl(PostForm $form)
    {
        switch (Yii::$app->request->post('afterSave')) {
            case self::AFTER_SAVE_NEW:

                switch ($form->post->type) {
                    case PostType::LAND:
                        $route = ['/post/add-landing-page'];
                        break;
                    case PostType::PAGE:
                        $route = ['/post/add-page'];
                        break;
                    default:
                        $route = ['/post/add'];
                }
                return Yii::$app->urlManager->createAbsoluteUrl($route);
            case self::AFTER_SAVE_DUPLICATE:
                return $this->createDuplicateUrl($form);
            case self::AFTER_SAVE_CLOSE:

                switch ($form->post->type) {
                    case PostType::LAND:
                        $route = ['/post/landing-pages'];
                        break;
                    case PostType::PAGE:
                        $route = ['/post/pages'];
                        break;
                    default:
                        $route = ['/post/index'];
                }

                return Yii::$app->urlManager->createAbsoluteUrl($route);
            default:

                switch ($form->post->type) {
                    case PostType::SHOP:
                        return Yii::$app->urlManager->createAbsoluteUrl(['/shop/edit', 'id' => $form->post->shop_id]) . '#t2-content';
                    default:
                        return Yii::$app->urlManager->createAbsoluteUrl(['/post/edit', 'id' => $form->post->id]);
                }
        }
    }

    /**
     * @param PostForm $model
     * @return string
     * @throws yii\base\InvalidConfigException
     */
    protected function createDuplicateUrl(PostForm $model)
    {
        $a = $model->attributes;
        unset($a['key']);
        switch ($model->post->type) {
            case PostType::LAND:
                $route = ['/post/add-landing-page'];
                break;
            case PostType::PAGE:
                $route = ['/post/add-page'];
                break;
            default:
                $route = ['/post/add'];
        }
        return Yii::$app->urlManager->createAbsoluteUrl(ArrayHelper::merge($route, [$model->formName() => $a]));
    }

	/**
	 * @param int $id
	 * @return Post
	 * @throws NotFoundHttpException
	 */
	protected function findPost($id)
	{
		if (!$post = Post::findOne($id)) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $post;
	}
}
