<?php

namespace phycom\backend\controllers;

use yii;

/**
 * Class TemplateController
 * @package phycom\backend\controllers
 */
class TemplateController extends BaseController
{
	public function actionIndex()
	{
		$this->checkPermission('search_message_templates');
		return $this->render('index');
	}

	public function actionView($id)
	{
		$this->checkPermission('view_message_template');
		return $this->render('view', ['model' => Yii::$app->modelFactory->getMessageTemplate()::getTemplate($id)]);
	}

	public function actionRender($id)
	{
		$this->checkPermission('view_message_template');
		$template = Yii::$app->modelFactory->getMessageTemplate()::getTemplate($id);
		$params = $template->generateDummyParameters();

		switch ($id) {

            case 'new_order':
                $order = Yii::$app->modelFactory->getOrder()::findOne(66);
                $params['order'] = $order;
                $params['postageLabel'] = $order->shipment->postageLabelData['url'] ?? null;
                $params['link'] = Yii::$app->urlManagerBackend->createAbsoluteUrl(['/order/edit', 'id' => $order->id]);
        }


		return $template->render($params);
	}
}