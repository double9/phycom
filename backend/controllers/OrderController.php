<?php

namespace phycom\backend\controllers;

use phycom\backend\models\OrderForm;
use phycom\backend\models\SearchOrder;

use phycom\common\models\statistics\SearchOrderStatistics;
use phycom\common\models\Order;

use yii\web\NotFoundHttpException;
use Yii;


/**
 * Class OrderController
 * @package phycom\backend\controllers
 */
class OrderController extends BaseController
{
    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws NotFoundHttpException
     */
    public function beforeAction($action)
    {
        $this->checkEnabled(Yii::$app->commerce);
        return parent::beforeAction($action);
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
	public function actionIndex()
	{
		$this->checkPermission('search_orders');

		$model = new SearchOrder();
		$dataProvider = $model->search(Yii::$app->request->get());

		return $this->render('index', ['model' => $model, 'dataProvider' => $dataProvider]);
	}

    /**
     * @return string
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionRecent()
    {
        $this->checkPermission('search_orders');
        $this->ajaxOnly();

        $model = new SearchOrder();
        $dataProvider = $model->recent(Yii::$app->request->get());

        return $this->renderPartial('partials/recent-orders', ['dataProvider' => $dataProvider]);
    }

    /**
     * @return string
     * @throws \yii\web\ForbiddenHttpException
     */
	public function actionAdd()
	{
		$this->checkPermission('create_order');
		$model = new OrderForm();

		return $this->render('add', ['model' => $model]);
	}

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
	public function actionEdit($id)
	{
		$this->checkPermission('update_order');
		$order = $this->findOrder($id);
		$model = new OrderForm($order);

		return $this->render('edit', ['model' => $model, 'order' => $order]);
	}

    /**
     * @return array|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
	public function actionStatistics()
    {
        $this->checkPermission('search_orders');
        $this->ajaxOnly();

        $model = new SearchOrderStatistics();
        $model->load(Yii::$app->request->get(), '');

        if ($model->validate()) {
            return $this->asJson($model->search()->allModels);
        }
        return $this->asJson(['error' => $model->firstErrors]);
    }

	/**
	 * @param $id
	 * @return Order
	 * @throws NotFoundHttpException
	 */
	protected function findOrder($id)
	{
		$order = Yii::$app->modelFactory->getOrder()::findOne($id);
		if (!$order) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $order;
	}

}
