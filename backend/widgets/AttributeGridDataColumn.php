<?php

namespace phycom\backend\widgets;


use phycom\common\models\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Json;
use yii\web\View;

/**
 * Class AttributeGridDataColumn
 * @package phycom\backend\widgets
 *
 * @property MultiFormGrid $grid
 */
class AttributeGridDataColumn extends \yii\grid\DataColumn
{
	const TYPE_TEXT = 'text';
	const TYPE_NUM = 'number';

	public $type = self::TYPE_TEXT;

	public $inputAppend;
	public $inputPrepend;
	public $inputField;

	public function init()
	{
		parent::init();
		$this->contentOptions = function ($model, $key, $index) {
			return ['data-attribute' => $this->attribute];
		};
	}

	/**
	 * Returns the data cell value.
	 * @param mixed $model the data model
	 * @param mixed $key the key associated with the data model
	 * @param int $index the zero-based index of the data model among the models array returned by [[GridView::dataProvider]].
	 * @return string the data cell value
	 */
	public function getDataCellValue($model, $key, $index)
	{
		if ($this->value !== null) {
			if (is_string($this->value)) {
				return $this->renderField($this->value, $model, $key, $index);
			} else {
				return call_user_func($this->value, $model, $key, $index, $this);
			}
		} elseif ($this->attribute !== null) {
			return $this->renderField($this->attribute, $model, $key, $index);
		}
		return null;
	}

	/**
	 * @inheritdoc
	 */
	protected function renderDataCellContent($model, $key, $index)
	{
		if ($this->content === null) {
			$value = $this->getDataCellValue($model, $key, $index);
			return $value === null ? $this->grid->formatter->format(null, $this->format) : $value;
		} else {
			return parent::renderDataCellContent($model, $key, $index);
		}
	}

	protected function renderField($attribute, $model, $key, $index)
	{
		/**
		 * @var ActiveRecord $model
		 */
		$value = ArrayHelper::getValue($model, $attribute);

		if ($this->inputField === false) {
			return $this->grid->formatter->format($value, $this->format);
		}

		$form = $this->grid->form;

//		$field = $form->field($this->grid->formModel, $this->grid->formModelAttribute . '[' . $key . '][' . $attribute . ']');

		$field = $form->field($model, $attribute);
		$modelName = (new \ReflectionClass($model))->getShortName();
		$formModelName = (new \ReflectionClass($this->grid->formModel))->getShortName();
		$field->inputOptions['id'] = Inflector::camel2id($this->grid->formModelAttribute) . '-' . $key . '-' . $attribute;
		$field->inputOptions['data-attribute-id'] = strtolower($modelName) . '-' . strtolower($attribute);
		$field->inputOptions['name'] = $formModelName . '[' . $this->grid->formModelAttribute . '][' . $key . '][' . $attribute . ']';
		$field->inputOptions['value'] = $value ? $this->grid->formatter->format($value, $this->format) : '';

		if (is_callable($this->inputAppend) && $key !== $this->grid::TEMPLATE_KEY) {
			$appendedValue = call_user_func($this->inputAppend, $model);
			$field->template = '<div class="input-group">{input}<div class="input-group-addon">'.$appendedValue.'</div></div>';
		} else {
			$field->template = '{input}';
		}
		Html::addCssClass($field->options, 'no-margin');
		$field->label(false);

		if (is_callable($this->inputField)) {
			$field = call_user_func($this->inputField, $field, $model, $key);
		} else {
			$field->textInput();
		}
		return $field;
	}
}