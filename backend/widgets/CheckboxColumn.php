<?php

namespace phycom\backend\widgets;

/**
 * Class CheckboxColumn
 *
 * @package phycom\backend\widgets
 */
class CheckboxColumn extends \yii\grid\CheckboxColumn
{
    public $visibleOnCondition;

    /**
     * @param $model
     * @param $key
     * @param $index
     * @return string
     */
    public function renderDataCellContent($model, $key, $index)
    {
        $isVisible = true;
        if ($this->visibleOnCondition instanceof \Closure) {
            $visibleOnCondition = $this->visibleOnCondition;
            $isVisible = (bool) $visibleOnCondition($model);
        }
        if ($isVisible) {
            return parent::renderDataCellContent($model, $key, $index);
        }
        return '';
    }
}
