<?php

namespace phycom\backend\widgets;

use phycom\backend\assets\MultiFormGridAsset;
use phycom\common\models\ActiveRecord;

use yii\grid\Column;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii;

/**
 * Class MultiFormGrid
 * @package phycom\backend\widgets
 *
 * @property ActiveForm $form
 * @property yii\data\ActiveDataProvider $dataProvider
 */
class MultiFormGrid extends AjaxGrid
{
    const EVENT_ON_FIELD = 'onRenderField';

	const TEMPLATE_KEY = '__tpl__';

	public $formModel;

	public $formModelAttribute = 'models';

	public $formOptions = [];

    public $templateModel;

	public $layout = "{errors}\n{items}\n{navigation}";

	public $actions = [];

    public $newRow;

	public $dataColumnClass = MultiFormGridDataColumn::class;

	/**
	 * @var ActiveForm
	 */
	protected $form;
    /**
     * @var []
     */
	protected $fieldKeys = [];

	/**
     * @return ActiveForm
     */
	public function getForm()
	{
		return $this->form;
	}

    /**
     * @param ActiveForm $value
     */
	public function setForm(ActiveForm $value)
    {
        $this->form = $value;
    }

	public function init()
	{
		$this->columns[] = [
			'label' => false,
			'content' => function ($model) {
				return Html::a('<span class="far fa-trash-alt"></span>', '#', ['class' => 'btn btn-flat btn-danger remove-row']);
			},
			'options' => ['width' => 50]
		];
		parent::init();
		Html::addCssClass($this->tableOptions, 'multi-form-table');
		if (!$this->newRow) {
            $this->actions[] = ['label' => '<span class="fas fa-plus"></span>&nbsp;&nbsp;' . Yii::t('backend/main', 'Add new'), 'options' => ['class' => 'add-row']];
        }
        $this->formOptions = ArrayHelper::merge($this->formOptions, ['id' => $this->id . '-form']);

		$this->on(self::EVENT_ON_FIELD, function ($e) {
            /**
             * @var yii\base\Event $e
             * @var yii\widgets\ActiveField $field
             */
            $field = $e->sender['field'];
            if (isset($field->selectors['input'])) {
                $this->fieldKeys[$field->selectors['input']] = $e->sender['key'];
            }
        });
	}

    /**
     * @param string $attribute
     * @return MultiFormGridDataColumn|null
     */
	protected function getColumnByAttribute($attribute)
    {
        /**
         * @var MultiFormGridDataColumn $column
         */
        foreach ($this->columns as $column) {
            if ($column->attribute === $attribute) {
                return $column;
            }
        }
        return null;
    }


	public function run()
	{
        $this->form = ActiveForm::begin($this->formOptions);

		$id = $this->form->id;
		$view = $this->getView();
		MultiFormGridAsset::register($view);
		parent::run();
		ActiveForm::end();

		if ($this->form->enableClientScript) {

		    $formAttributes = [];
		    foreach ($this->form->attributes as $attribute) {
                $inputId = $attribute['input'];
//                if (false !== strpos($inputId, self::TEMPLATE_KEY)) {
//                    continue;
//                }
                $name = isset($this->fieldKeys[$inputId])
                    ? $this->getColumnByAttribute($attribute['name'])->getFieldName($attribute['name'], $this->fieldKeys[$inputId])
                    : $attribute['name'];

                $formAttributes[] = ArrayHelper::merge($attribute, [
                    'id'               => substr($inputId, 1),
                    'name'             => $name,
                    'validateOnChange' => true,
                    'validateOnBlur'   => true
                ]);
            }

		    $formAttributesTemplate = [];
		    for ($i = count($formAttributes) - 1; $i >= 0; $i--) {
                $a = $formAttributes[$i];
                $name = $a['name'];

                if (!isset($formAttributesTemplate[$name])) {
                    $formAttributesTemplate[$name] = [
                        'name'             => $a['name'],
                        'error'            => $a['error'],
                        'validateOnChange' => $a['validateOnChange'] ?? false,
                        'validateOnBlur'   => $a['validateOnBlur'] ?? false,
                        'validate'         => $a['validate']
                    ];
                }
            }

            $jsOptions = Json::encode([
                'formAttributes'         => $formAttributes,
                'formAttributesTemplate' => $formAttributesTemplate,
                'templateKey'            => self::TEMPLATE_KEY
            ]);

			$view->registerJs("multiFormGrid.init('#$id', $jsOptions);");
		}
	}


	public function renderSection($name)
	{
		switch ($name) {
			case '{actions}':
				return $this->renderActions();
            case '{new}':
                return $this->renderNew();
			default:
				return parent::renderSection($name);
		}
	}

	/**
	 * Renders the table body.
	 * @return string the rendering result.
	 */
	public function renderTableBody()
	{
		$models = array_values($this->dataProvider->getModels());
		$keys = $this->dataProvider->getKeys();
		$rows = [];
		foreach ($models as $index => $model) {
			$key = $keys[$index];
			if ($this->beforeRow !== null) {
				$row = call_user_func($this->beforeRow, $model, $key, $index, $this);
				if (!empty($row)) {
					$rows[] = $row;
				}
			}

			$rows[] = $this->renderTableRow($model, $key, $index);

			if ($this->afterRow !== null) {
				$row = call_user_func($this->afterRow, $model, $key, $index, $this);
				if (!empty($row)) {
					$rows[] = $row;
				}
			}
		}

		$rows[] = $this->renderNewModelTemplate(count($models));

		if (empty($rows)) {
			$colspan = count($this->columns);

			return "<tbody>\n<tr><td colspan=\"$colspan\">" . $this->renderEmpty() . "</td></tr>\n</tbody>";
		} else {
			return "<tbody>\n" . implode("\n", $rows) . "\n</tbody>";
		}
	}

	protected function renderNewModelTemplate($index)
	{
	    if ($this->dataProvider instanceof yii\data\ActiveDataProvider) {
            $modelClass = $this->dataProvider->query->modelClass;
        } else if ($this->dataProvider instanceof yii\data\ArrayDataProvider) {
		    $modelClass = $this->dataProvider->modelClass;
        } else {
	        throw new yii\base\InvalidConfigException('Invalid data provider ' . get_class($this->dataProvider));
        }

		if ($this->templateModel) {
		    if (!$this->templateModel instanceof $modelClass) {
		        throw new yii\base\InvalidArgumentException('Param templateModel must be instance of ' . $modelClass);
            }
            $model = $this->templateModel;
        } else {
            $model = new $modelClass;
        }
        /**
         * @var ActiveRecord $model
         */
		$model->loadDefaultValues();

		return $this->renderTableRow($model, static::TEMPLATE_KEY, $index);
	}

	protected function renderActions()
	{
		$html = Html::beginTag('div', ['class' => 'actions clearfix']);
		foreach ($this->actions as $action) {
			if (!isset($action['url'])) {
				$action['url'] = '#';
			}
			if (!isset($action['options'])) {
				$action['options'] = [];
			}
			Html::addCssClass($action['options'], 'btn btn-flat btn-default');
			$html .= Html::a($action['label'], $action['url'], $action['options']);
		}
		$html .= Html::endTag('div');
		return $html;
	}

    protected function renderNew()
    {
        if (is_callable($this->newRow)) {
            return call_user_func($this->newRow);
        }
        return '';
    }

	protected function renderNavigation()
	{
		$html =  Html::beginTag('div', ['class' => 'table-nav']) . Html::beginTag('div', ['class' => 'row']);

		$pager = $this->renderPager();

		if (empty($pager)) {
            $html .= Html::tag('div', $this->newRow ? $this->renderNew() : $this->renderActions(), ['class' => 'col-md-12']);
        } else {
            $html .= Html::tag('div', $this->newRow ? $this->renderNew() : $this->renderActions(), ['class' => 'col-md-6']);
            $html .= Html::tag('div', $pager, ['class' => 'col-md-6']);
        }

		$html .= Html::endTag('div') . Html::endTag('div');
		return $html;
	}

	public function renderTableRowAjax($model, $key, $index)
    {
        $ajaxPlaceholder = '{ajaxPlaceholder}';
        $cells = [];
        $numCols = count($this->columns);
        /* @var $column Column */
        foreach ($this->columns as $i => $column) {
            $cellContent = $column->renderDataCell($model, $key, $index);
            if ($i === ($numCols - 1)) {
                $cellContent .= $ajaxPlaceholder;
            }
            $cells[] = $cellContent;
        }
        if ($this->rowOptions instanceof \Closure) {
            $options = call_user_func($this->rowOptions, $model, $key, $index, $this);
        } else {
            $options = $this->rowOptions;
        }
        $options['data-key'] = is_array($key) ? json_encode($key) : (string) $key;

        $html = Html::tag('tr', implode('', $cells), $options);

        ob_start();
        ob_implicit_flush(false);

        $this->view->beginPage();
        $this->view->head();
        $this->view->beginBody();
        $this->view->endBody();
        $this->view->endPage(true);

        $ajaxContent = ob_get_clean();

        return str_replace($ajaxPlaceholder, $ajaxContent, $html);
    }
}
