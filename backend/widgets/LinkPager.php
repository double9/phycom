<?php

namespace phycom\backend\widgets;

use yii;

/**
 * Class LinkPager
 * @package phycom\backend\widgets
 */
class LinkPager extends \yii\widgets\LinkPager
{
	public static function small($pagination = false)
	{
		return static::widget([
			'pagination' => $pagination,
			'nextPageLabel' => Yii::t('backend/main', 'Next'),
			'prevPageLabel' => Yii::t('backend/main', 'Previous'),
			'options' => [
				'class' => 'no-margin pagination pagination-sm pull-right'
			]
		]);
	}

    public static function medium($pagination = false)
    {
        return static::widget([
            'pagination' => $pagination,
            'nextPageLabel' => Yii::t('backend/main', 'Next'),
            'prevPageLabel' => Yii::t('backend/main', 'Previous'),
            'options' => [
                'class' => 'no-margin pagination pagination-md pull-right'
            ]
        ]);
    }
}
