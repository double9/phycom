<?php

namespace phycom\backend\widgets;

use phycom\backend\assets\DeliveryAsset;
use phycom\backend\assets\GridAsset;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii;

/**
 * Class DataGrid
 * @package phycom\backend\widgets
 */
class DataGrid extends AjaxGrid
{
	public $layout = "{header}\n{bulkEdit}\n{errors}\n{items}\n{navigation}";

	public $tableOptions = ['class' => 'table table-responsive table-bordered table-striped no-margin table-hover dataTable'];

	public $containerOptions = ['class' => 'ajax-grid data-grid'];

	public $ajaxEnabled = false;

	public $actions = [];

	public function init()
	{
		parent::init();
		$this->pager = [
			'class' => LinkPager::class,
			'nextPageLabel' => Yii::t('backend/main', 'Next'),
			'prevPageLabel' => Yii::t('backend/main', 'Previous'),
			'options' => [
				'class' => 'no-margin pagination pagination-md pull-right'
			]
		];
	}

	public function run()
	{
		$id = $this->options['id'];
		$view = $this->getView();
		GridAsset::register($view);
		$view->registerJs("grid.init('#$id');");
		parent::run();
	}

	public function renderSection($name)
	{
		switch ($name) {
			case '{header}':
				return $this->renderHeader();
			case '{actions}':
				return $this->renderActions();
			default:
				return parent::renderSection($name);
		}
	}

	protected function renderHeader()
	{
		$html =  Html::beginTag('div', ['class' => 'table-header']) . Html::beginTag('div', ['class' => 'row']);
		$html .= Html::tag('div', $this->renderSummary(), ['class' => 'col-md-3']);
		$html .= Html::tag('div', '<div style="float: right; margin-left: 20px;">' . $this->renderPager() . '</div><div style="float: right;">' . $this->renderActions() . '</div>', ['class' => 'col-md-9']);
		$html .= Html::endTag('div') . Html::endTag('div');
		return $html;
	}

	protected function renderActions()
	{
		$html = Html::beginTag('div', ['class' => 'actions clearfix']);
		foreach ($this->actions as $action) {
		    if (isset($action['visible']) && !$action['visible']) {
		        continue;
            }
			if (!isset($action['options'])) {
				$action['options'] = [];
			}
			$html .= Html::a($action['label'], $action['url'], ArrayHelper::merge([
				'class' => 'btn btn-flat btn-default',
			], $action['options']));
		}
		$html .= Html::endTag('div');
		return $html;
	}
}
