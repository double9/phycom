<?php
/**
 * Created by PhpStorm.
 * User: st.kevich
 * Date: 25.03.18
 * Time: 18:05
 */
namespace phycom\backend\widgets\ckeditor5;

use yii\web\AssetBundle;

class InlineAsset extends AssetBundle
{
    public $sourcePath = '@vendor/npm-asset/ckeditor--ckeditor5-build-inline/build';

    public $css = [];

    public $js = [
        'ckeditor.js',
    ];

    public $depends = [];
}
