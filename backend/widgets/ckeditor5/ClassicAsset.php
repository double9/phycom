<?php

namespace phycom\backend\widgets\ckeditor5;

use yii\web\AssetBundle;
use Yii;

class ClassicAsset extends AssetBundle
{
    public $sourcePath = '@phycom/backend/widgets/ckeditor5/assets';

    public $css = [];

    public $js = [
        'ckeditor.js'
    ];

    public $depends = [];

    public function init()
    {
        parent::init();
        $translation = 'translations/' . substr(Yii::$app->language, 0, 2) . '.js';
        if (file_exists(Yii::getAlias($this->sourcePath . '/' . $translation))) {
            $this->js[] = $translation;
        }
    }
}
