<?php
/**
 * Created by PhpStorm.
 * User: st.kevich
 * Date: 25.03.18
 * Time: 18:05
 */
namespace phycom\backend\widgets\ckeditor5;

use yii\helpers\Json;
use yii\web\JsExpression;
use yii\widgets\InputWidget;
use Yii;

abstract class CKEditor5 extends InputWidget
{
    /**
     * @var string
     */
    public $editorType = 'Classic';

    /**
     * @var array
     */
    public $clientOptions = [];

    /**
     * @var array Toolbar options array
     */
    public $toolbar = [];

    /**
     * @var string Url to image upload
     */
    public $uploadUrl = '';

    /**
     * @var array
     */
    public $options = [];

    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->registerAssets($this->getView());
        $this->registerEditorJS();
        $this->printEditorTag();
    }

    /**
     * Registration JS
     */
    protected function registerEditorJS()
    {
        $options = $this->clientOptions;
        $view = $this->getView();

        if (!empty($this->toolbar)) {
            $options['toolbar'] = $this->toolbar;
        }
        if (!empty($this->uploadUrl)) {
//            $options['ckfinder'] = [
//                'uploadUrl' => $this->uploadUrl,
//                'openerMethod' => 'popup' //'modal'
//            ];
            $options['simpleUpload'] = [
                'uploadUrl' => $this->uploadUrl,
                'headers' => [
                    Yii::$app->request::CSRF_HEADER => Yii::$app->request->getCsrfToken()
                ]
            ];
        }
        $options = Json::encode($options);

        $js = new JsExpression(
            $this->editorType . "Editor.create( document.querySelector( '#{$this->options['id']}' ), {$options} )
                .then( editor => {
                    editor.model.document.on( 'change:data', () => {
                        editor.updateSourceElement();
                    });
                })
                .catch( error => {console.error( error );} );"
        );

        $view->registerJs($js);
    }

    /**
     * @param \yii\web\View $view
     */
    protected function registerAssets($view)
    {}

    /**
     * View tag for editor
     */
    protected function printEditorTag()
    {}

}
