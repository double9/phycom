<?php

namespace phycom\backend\widgets;

use phycom\backend\assets\FormAsset;
use phycom\common\helpers\JsExpression;
use phycom\common\helpers\Json;

use yii\base\Widget;
use yii\helpers\Html;
use Yii;

/**
 * Class FormCollection
 *
 * @package phycom\backend\widgets
 */
class FormCollection extends Widget
{
    /**
     * @var bool
     */
    public $tabs = false;
    /**
     * @var string
     */
    public $submitUrl;
    /**
     * @var array the HTML attributes (name-value pairs) for the forms container tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];
    /**
     * @var string[] javascript selectors of form classes to be added
     */
    public $forms = [];
    /**
     * @var string[] javascript selectors of attachment forms to be added
     */
    public $attachmentForms = [];

    /**
     * Initializes the widget.
     * This renders the container open tag.
     */
    public function init()
    {
        parent::init();
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
        if (!isset($this->submitUrl)) {
            $this->submitUrl = Yii::$app->request->getUrl();
        }
        $this->options['data-submit-url'] = $this->submitUrl;
        $this->registerClientScript();
        ob_start();
        ob_implicit_flush(false);
    }

    /**
     * Runs the widget.
     * This registers the necessary JavaScript code and renders the container open and close tags.
     */
    public function run()
    {
        $content = ob_get_clean();
        $html = Html::beginTag('div', $this->options);
        $html .= $content;
        $html .= Html::endTag('div');
        return $html;
    }


    /**
     * This registers the necessary JavaScript code.
     */
    public function registerClientScript()
    {
        $id = $this->options['id'];
        $options = Json::htmlEncode($this->getClientOptions());

        $forms = $this->parseFormData($this->forms);
        $attachmentForms = $this->parseFormData($this->attachmentForms);

        $view = $this->getView();
        FormAsset::register($view);

        $view->registerJs(<<<JS
            jQuery(function ($) {
    
                let formCollection = (new window.FormCollection('#$id', $options));
                let formSelectors = $forms;
                let attachmentForms = $attachmentForms;
        
                formSelectors.forEach(selector => formCollection.add(selector));
                attachmentForms.forEach(selector => formCollection.addAttachmentForm(selector));
            });
JS
            , \yii\web\View::POS_READY);
    }

    /**
     * @param array $data
     * @return string
     */
    protected function parseFormData(array $data)
    {
        $forms = [];
        foreach ($data as $key => $value) {
            if (is_string($key)) {
                $forms[] = ['selector' => $key, 'label' => $value];
            } else {
                $forms[] = $value;
            }
        }
        return Json::htmlEncode($forms);
    }

    /**
     * @return array
     */
    protected function getClientOptions()
    {
        $clientOptions = [];

        if ($this->tabs) {
            $clientOptions['onValidationError'] = new JsExpression('
                function (form, messages) {
                    this.$el.find(".nav-pills > li").each(function (i, el) {
                        let id = $(el).find("a").attr("href");
                        let invalidFields = $(id).find("input[aria-invalid=\"true\"], select[aria-invalid=\"true\"], textarea[aria-invalid=\"true\"]");
                        
                        if (invalidFields.length) {
                            $(el).addClass("error");
                        } else {
                            $(el).removeClass("error");
                        }               
                    });
                }'
            );
        }

        return $clientOptions;
    }
}
