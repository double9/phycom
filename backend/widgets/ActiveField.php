<?php

namespace phycom\backend\widgets;


use phycom\backend\helpers\Html;
use phycom\backend\widgets\ckeditor5\EditorClassic;

use phycom\common\helpers\f;
use phycom\common\helpers\Url;
use phycom\common\helpers\Filter;
use phycom\common\helpers\PhoneHelper;
use phycom\common\models\Country;

use dosamigos\datetimepicker\DateTimePicker as DateTimePickerA;
//use brussens\datetimepicker\Widget as DateTimePickerB;
use dosamigos\typeahead\TypeAhead;
use kartik\file\FileInput;
use kartik\select2\Select2;
use borales\extensions\phoneInput\PhoneInput;

use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii;

/**
 * Class ActiveField
 * @package phycom\backend\widgets
 *
 * @property ActiveForm $form
 */
class ActiveField extends \yii\bootstrap\ActiveField
{

    public function inputAppend($content, array $options = [])
    {
        Html::addCssClass($options, 'input-group');
        $template = '{beginLabel}{labelTitle}{endLabel}' . Html::beginTag('div', $options) . '{input}<span class="input-group-addon">' . $content . '</span>' . Html::endTag('div') . '{error}{hint}';
        $this->template = $template;
        return $this;
    }

    /**
     * @param array $options
     * @return ActiveField
     */
    public function numberInput(array $options = [])
    {
        $options['type'] = 'number';
        return $this->textInput($options);
    }


    public function datePicker($options = [])
    {
        $options = array_merge($this->inputOptions, $options);
        $this->adjustLabelFor($options);
        $this->parts['{input}'] = Filter::datePicker($this->model, $this->attribute, $options);
        return $this;
    }

    public function boolean($prompt = false, array $options = [], array $items = [])
    {
    	if ($prompt) {
    		$options = ArrayHelper::merge([
    			'prompt' => is_string($prompt) ? $prompt :  Yii::t('backend/main', 'Select')
		    ], $options);
	    }
	    if (empty($items)) {
    	    $items = [
                1 => Yii::t('backend/main', 'Yes'),
                0 => Yii::t('backend/main', 'No'),
            ];
        }
        if (!isset($options['value'])) {
            $options['value'] = isset($this->model->{$this->attribute})
                ? (int) $this->model->{$this->attribute}
                : 0;
        }
    	return $this->dropdownList($items, $options);
    }

    public function fileInput($options = [])
    {
    	return $this->widget(FileInput::class, $options);
    }

    public function multiFileInput($options = [])
    {
    	$defaults = [
		    'options' => ['multiple' => true],
		    'pluginOptions' => [
			    'theme' => 'fa',
			    'browseOnZoneClick' => true,
			    'uploadAsync' => true,
			    'autoReplace' => false,
			    'overwriteInitial' => false,
			    'maxFileCount' => $model->maxFiles,
			    'allowedFileExtensions' => $model->allowedExtensions,
			    'uploadUrl' => Url::toRoute(['product/upload-attachment', 'id' => $model->product->id]),
			    'showClose'  => false,
			    'showRemove'  => false,
			    'showCaption' =>  false,
			    'showBrowse' => false,
			    'browseClass' => 'btn btn-sm btn-primary btn-flat',
			    'browseIcon' => '<span class="far fa-image"></span> ',
			    'browseLabel' => Yii::t('backend/main','Browse'),
			    'uploadClass' => 'btn btn-sm btn-default btn-flat file-upload-btn',
			    'uploadLabel' => Yii::t('backend/main','Save'),
			    //	'defaultPreviewContent' => '<img src="'.$user->avatar.'" alt="'.$user->fullName.' Avatar">',
			    'initialPreviewShowDelete' => false,

//			'initialPreview' => [
//				"http://lorempixel.com/1920/1080/nature/1",
//				"http://lorempixel.com/1920/1080/nature/2",
//				"http://lorempixel.com/1920/1080/nature/3"
//			],
//            'initialPreviewConfig' => [
//                ['caption' => "nature-1.jpg", 'size' => 329892, 'width' => "120px",  'key' => 1],
//                ['caption' => "nature-2.jpg", 'size' => 872378, 'width' => "120px",  'key' => 2],
//                ['caption' => "nature-3.jpg", 'size' => 632762, 'width' => "120px",  'key' => 3]
//            ],

			    'previewSettings' => ['image' => ['width' => "auto", 'height' => "auto", 'style' => 'max-height: 200px;'], 'key' => 1],
			    'layoutTemplates' => [
				    'main2' => '{preview}',
				    'actions' => '{drag}',
				    'progress' => '',
			    ],
			    'fileActionSettings' => [
				    'showDrag' => true,
				    'dragIcon' => '<span class="fas fa-bars"></span> ',
				    'dragClass' => 'drag',
				    'dragTitle' => Yii::t('backend/main','Drag'),
//				'dragSettings' => []
			    ]
		    ]
	    ];
    	$defaults['pluginOptions'] = ArrayHelper::merge($defaults['pluginOptions'], $options);
    	return $this->widget(FileInput::class, $defaults);
    }

	/**
	 * @param string|bool $phoneCodeAttribute
	 * @param array $options
	 * @return $this
	 */
    public function phoneInput($phoneCodeAttribute = 'phoneCode', $options = [])
    {
    	$phoneCode = false;
    	if (isset($options['phoneCode'])) {
			$phoneCode = $options['phoneCode'];
			unset($options['phoneCode']);
	    }
    	$options = ArrayHelper::merge([
		    'jsOptions' => [
			    'preferredCountries' => ['ee', 'ru', 'fi', 'uk', 'de', 'fr'],
			    'separateDialCode' => false,
			    'autoPlaceholder' => 'off'
		    ],
		    'options' => [
		    	'class' => 'form-control'
		    ]
	    ], $options);

    	$field = $this->widget(PhoneInput::class, $options);

    	if ($phoneCodeAttribute && is_string($phoneCodeAttribute)) {
		    $field->template = "{label}\n{country_code_input}\n{input}\n{hint}\n{error}";
		    $field->parts['{country_code_input}'] = yii\helpers\Html::activeHiddenInput($this->model, $phoneCodeAttribute);
			$phoneId = yii\helpers\Html::getInputId($this->model, $this->attribute);
		    $phoneCodeId = yii\helpers\Html::getInputId($this->model, $phoneCodeAttribute);
		    $this->form->options['onsubmit'] = '$("#'.$phoneCodeId.'").val($("#'.$phoneId.'").intlTelInput("getSelectedCountryData").dialCode);';

		    $countryCode = strtolower(Country::findOneByPhoneCode($this->model->$phoneCodeAttribute)->code);
		    $this->form->getView()->registerJs("$('#$phoneId').intlTelInput('setCountry', '".$countryCode."');");
	    } else {
		    $countryCode = strtolower(Country::findOneByPhoneCode($phoneCode)->code);
		    $phoneId = yii\helpers\Html::getInputId($this->model, $this->attribute);
		    $this->form->getView()->registerJs("$('#$phoneId').intlTelInput('setCountry', '".$countryCode."');");
	    }

    	return $field;
    }


    /**
     * @param string|bool $phoneNumberAttribute
     * @param array $options
     * @return $this
     */
    public function phoneInput2($phoneNumberAttribute = 'phoneNumber', $options = [])
    {
        if (!$phoneNumberAttribute || !is_string($phoneNumberAttribute)) {
            throw new yii\base\InvalidArgumentException('Invalid phone number attribute');
        }
        $countries = Yii::$app->country->countries;
        $options = ArrayHelper::merge([
            'jsOptions' => [
                'separateDialCode' => false,
                'nationalMode' => true,
                'allowExtensions' => true,
                'formatOnDisplay' => true,
                'autoHideDialCode' => true,
                'preferredCountries' => Yii::$app->country->preferredCountries,
                'onlyCountries' => $countries,
                'allowDropdown' => empty($countries) || count($countries) > 1,
                'initialCountry' => 'auto',
            ],
            'options' => [
                'class' => 'form-control'
            ]
        ], $options);

        $field = $this->widget(PhoneInput::class, $options);
        $phoneId = yii\helpers\Html::getInputId($this->model, $this->attribute);
        $phoneNumberId = yii\helpers\Html::getInputId($this->model, $phoneNumberAttribute);

        $field->template = "{label}\n{phone_number_input}\n{input}\n{hint}\n{error}";
        $field->parts['{phone_number_input}'] = yii\helpers\Html::activeHiddenInput($this->model, $phoneNumberAttribute);

        $this->form->options['oncountrychange'] = '$("#'.$phoneNumberId.'").val($("#'.$phoneId.'").intlTelInput("getNumber"));';
        $this->form->options['onsubmit'] = '$("#'.$phoneNumberId.'").val($("#'.$phoneId.'").intlTelInput("getNumber"));';
        $this->form->options['onfocusout'] = '$("#'.$phoneNumberId.'").val($("#'.$phoneId.'").intlTelInput("getNumber"));';

        $phoneCode = $this->model->$phoneNumberAttribute ? PhoneHelper::getPhoneCode($this->model->$phoneNumberAttribute) : null;
        $countryCode = $phoneCode ? strtolower(Country::findOneByPhoneCode($phoneCode)->code) : Yii::$app->country->defaultCountry;
        $this->form->getView()->registerJs("$('#$phoneId').intlTelInput('setCountry', '".$countryCode."');");

        return $field;
    }


	/**
	 * @param $engine
	 * @param array $options
	 * @return $this
	 */
    public function typeAhead($engine, array $options = [])
    {
    	return $this->widget(TypeAhead::class,
		    [
			    'options' => ['class' => 'form-control'],
			    'engines' => [ $engine ],
			    'clientOptions' => [
				    'highlight' => true,
				    'minLength' => 3
			    ],
			    'clientEvents' => [
				    'typeahead:selected' => 'function () { console.log(\'event "selected" occured.\'); }'
			    ],
			    'dataSets' => [
				    [
					    'name' => 'categories',
					    'displayKey' => 'value',
					    'source' => $engine->getAdapterScript()
				    ]
			    ]
		    ]);
    }

    /**
     * Renders the CKEditor widget
     *
     * @param array $options
     * @return $this
     * @throws \Exception
     */
    public function editor(array $options = [])
    {
        Html::addCssClass($this->options, 'editor');

        $embed = ['mediaEmbed', '|'];
        $imageMod = [];
        if (isset($options['uploadUrl'])) {
            array_unshift($embed, 'imageUpload');
            $imageMod = ['imageTextAlternative', 'imageStyle:full', 'imageStyle:side', '|'];
        }

        $defaultToolbar = array_merge(
            ['heading', '|'],
            ['bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote', 'indent', 'outdent', '|'],
            $embed,
            $imageMod,
            ['insertTable', 'tableColumn', 'tableRow', 'mergeTableCells', '|'],
            ['removeFormat', '|'],
            ['undo', 'redo', '|']
        );


        return $this->widget(EditorClassic::class, ArrayHelper::merge([
            'options' => ['rows' => 6],
//            'toolbar' => ['heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote'],
//            'uploadUrl' => '/someUpload.php',
            'clientOptions' => [
                'language' => [
                    'ui' => substr(Yii::$app->language, 0, 2)
                ],
                'toolbar' => $defaultToolbar,
                'heading' => [
                    'options' => [
                        ['model' => 'paragraph', 'title' => 'Paragraph', 'class' => 'ck-heading_paragraph'],
                        ['model' => 'heading1', 'view' => 'h2', 'title' => 'Heading 1', 'class' => 'ck-heading_heading1'],
                        ['model' => 'heading2', 'view' => 'h3', 'title' => 'Heading 2', 'class' => 'ck-heading_heading2'],
                        ['model' => 'heading3', 'view' => 'h4', 'title' => 'Heading 3', 'class' => 'ck-heading_heading3'],
                        ['model' => 'pre', 'view' => 'pre', 'title' => 'Preformatted', 'class' => 'ck-heading_heading_pre'],
                    ]
                ],
                'image' => [
                    'upload' => [
                        'types' => [ 'png', 'jpeg', 'jpg', 'gif' ]
                    ]
                ]
//                'toolbar' => [ 'heading'],
//                'toolbar' => ["undo", "redo", "bold", "italic", "blockQuote", "ckfinder", "imageTextAlternative", "imageUpload", "heading", "imageStyle:full", "imageStyle:side", "indent", "outdent", "link", "numberedList", "bulletedList", "mediaEmbed", "insertTable", "tableColumn", "tableRow", "mergeTableCells"],
//                'format_tags' => 'p;h1;h2;h3;h4;h5;h6;pre;address;div',
//                'toolbar' => [
//                    ['name' => 'styles', 'items' => ['Styles','Format']],
//                    ['name' => 'clipboard', 'items' => ['Cut', 'Copy', 'Paste', 'PasteFromText', 'PasteFromWord', 'Undo', 'Redo']],
//                    ['name' => 'links', 'items' => ['Link', 'Unlink', 'Anchor']],
//                    ['name' => 'insert', 'items' => ['Table', 'HorizontalRule', 'SpecialChar']],
//                    ['name' => 'basicstyles', 'items' => ['Bold','Italic','Strike','-','RemoveFormat']],
//                    ['name' => 'paragraph', 'items' => ['NumberedList','BulletedList', '-', 'Outdent','Indent','-','Blockquote']],
//                    ['name' => 'document', 'items' => ['Source']]
//                ]
            ]
        ], $options));

        $height = 345;

        return $this->widget(Editor::class, [
            'options' => ArrayHelper::merge(['rows' => 6, 'style' => 'height: '.$height.'px;'], $options),
            'pajaxId' => $pajaxId,
            'preset' => 'custom',
	        'clientOptions' => [
	        	'height' => $height - 71,
                'format_tags' => 'p;h1;h2;h3;h4;h5;h6;pre;address;div',
		        'toolbar' => [
		            [],
		        	['name' => 'styles', 'items' => ['Styles','Format']],
                    ['name' => 'clipboard', 'items' => ['Cut', 'Copy', 'Paste', 'PasteFromText', 'PasteFromWord', 'Undo', 'Redo']],
			        ['name' => 'links', 'items' => ['Link', 'Unlink', 'Anchor']],
			        ['name' => 'insert', 'items' => ['Table', 'HorizontalRule', 'SpecialChar']],
			        ['name' => 'basicstyles', 'items' => ['Bold','Italic','Strike','-','RemoveFormat']],
			        ['name' => 'paragraph', 'items' => ['NumberedList','BulletedList', '-', 'Outdent','Indent','-','Blockquote']],
                    ['name' => 'document', 'items' => ['Source']]
		        ]
            ]
        ]);
    }


	/**
	 * Renders the CKEditor widget
	 * @param array $options
	 * @param string $pajaxId
	 * @return $this
	 */
	public function smallEditor(array $options = [], $pajaxId = null)
	{
		$height = 180;

		return $this->widget(Editor::class, [
			'options' => ArrayHelper::merge(['rows' => 6, 'style' => 'height: '.$height.'px;'], $options),
			'pajaxId' => $pajaxId,
			'preset' => 'custom',
			'clientOptions' => [
				'height' => $height - 71,
				'format_tags' => 'p;h1;h2;h3;h4;h5;h6;pre;address;div',
				'toolbar' => [
					['name' => 'styles', 'items' => ['Styles','Format']],
					['name' => 'clipboard', 'items' => ['Cut', 'Copy', 'Paste', 'PasteFromText', 'PasteFromWord', 'Undo', 'Redo']],
					['name' => 'links', 'items' => ['Link', 'Unlink', 'Anchor']],
					['name' => 'insert', 'items' => ['Table', 'HorizontalRule', 'SpecialChar']],
					['name' => 'basicstyles', 'items' => ['Bold','Italic','Strike','-','RemoveFormat']],
					['name' => 'paragraph', 'items' => ['NumberedList','BulletedList', '-', 'Outdent','Indent','-','Blockquote']],
                    ['name' => 'document', 'items' => ['Source']]
				]
			]
		]);
	}

    /**
     * @param array $data
     * @param array $config
     * @param array $pluginConfig
     * @return $this
     * @throws \Exception
     */
	public function multiSelect(array $data = [], array $config = [], array $pluginConfig = [])
	{
	    $value = $this->model->{$this->attribute};
        $value = is_array($value) ? array_keys($value) : [];

        return $this->multiSelectWidget($value, $data, $config, $pluginConfig);
	}

    /**
     * @param array $data
     * @param array $config
     * @param array $pluginConfig
     * @return $this
     * @throws \Exception
     */
    public function tags(array $data = [], array $config = [], array $pluginConfig = [])
    {
        $value = $this->model->{$this->attribute};
        $value = is_array($value) ? array_values($value) : [];

        $selectedTags = [];
        foreach ($value as $tag) {
            $selectedTags[$tag] = Inflector::titleize($tag);
        }
        $data = ArrayHelper::merge($data, $selectedTags); // merge in also selected tags
        $pluginConfig = ArrayHelper::merge(['tags' => true], $pluginConfig);

        return $this->multiSelectWidget($value, $data, $config, $pluginConfig);
    }

    /**
     * @param array $selectedOptions
     * @param array $allOptions
     * @param array $config
     * @param array $pluginConfig
     * @return ActiveField
     * @throws \Exception
     */
    protected function multiSelectWidget(array $selectedOptions, array $allOptions = [], array $config = [], array $pluginConfig = [])
    {
        return $this->widget(Select2::class, [
            'data'          => $allOptions,
            'theme'         => Select2::THEME_BOOTSTRAP,
            'options'       => ArrayHelper::merge(['value' => $selectedOptions], $config),
            'pluginOptions' => ArrayHelper::merge(['multiple' => true], $pluginConfig)
        ]);
    }



    public function tableField()
    {
    	$this->options = ['class' => 'form-group no-margin'];
    	$field = $this->textInput();
    	$field->label(null);
    	$field->template = '{input}';

    	return $field;
    }

	/**
	 * Setup the field for use in a tabular form
	 *
	 * @param $collectionAttribute
	 * @param $key
	 * @return $this
	 */
    public function forTable($collectionAttribute, $key)
    {
		$this->options['id'] = Inflector::camel2id($collectionAttribute) . '-' . $key . '-' . Inflector::camel2id($this->attribute);
		$this->options['name'] = $collectionAttribute . '[' . $key . '][' . $this->attribute . ']';
		$this->label(false);
		$this->template = '{input}';

		return $this;
    }

    public function hidden($value, array $options = [])
    {
    	$this->template = '{input}';
    	yii\helpers\Html::addCssClass($this->options, 'no-margin');
    	$this->label(false);
	    $this->hiddenInput($options);
	    $this->inputOptions['value'] = $value;
	    return $this;
    }

    public function addressField($modalId, array $options = [])
    {
        $value = $this->model->{$this->attribute};
        $btnLabel = $value ? Yii::t('backend/main', 'Change address') : Yii::t('backend/main', 'Add new address');

        $this->inputOptions['data-label-url'] = Url::toRoute('address/label');

        $field = $this->hiddenInput();
        $field->template = "{label}\n{value}{btn}\n{input}\n{error}\n{hint}";
        $field->parts['{value}'] = '<span class="address">' . ($value ? f::address($value) : '') . '</span>';
        $field->parts['{btn}'] = Html::a('<span class="fas fa-map-marker-alt"></span>&nbsp;&nbsp;' . $btnLabel, '#', [
            'class' => 'pull-right btn btn-sm btn-default btn-flat',
            'data-target' => '#' . $modalId,
            'data-toggle' => 'modal'
        ]);
        $field->labelOptions = ['style' => 'display: block;'];
        $field->options = ArrayHelper::merge($field->options, ['class' => 'address-field'], $options);

        return $field;
    }
}
