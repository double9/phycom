<?php

namespace phycom\backend\widgets;


use phycom\backend\assets\FormSubmitBtnAsset;

use phycom\common\helpers\Json;

use yii\base\Widget;

/**
 * Class FormSubmitBtn
 *
 * @package phycom\backend\widgets
 */
class FormSubmitBtn extends Widget
{
    /**
     * @var FormCollection
     */
    public $formCollection;

    public $options = ['class' => 'btn-group-lg pull-right'];

    public $formContainer;

    public $tabs = false;

    /**
     * Initializes the widget.
     * This renders the container open tag.
     */
    public function init()
    {
        parent::init();
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
        ob_start();
        ob_implicit_flush(false);
    }

    public function run()
    {
        $content = ob_get_clean();
        $html = $this->renderSubmitButton();
        $html .= $content;
        $this->registerClientScript();
        return $html;
    }

    /**
     * This registers the necessary JavaScript code.
     */
    public function registerClientScript()
    {
        $id = $this->options['id'];
        $view = $this->getView();
        FormSubmitBtnAsset::register($view);
        $params = Json::encode([
            'formCollection' => $this->formCollection ? '#' . $this->formCollection->id : null,
        ]);
        $view->registerJs("jQuery(function($){FormSubmitBtn.init('#$id', $params)});");
    }

    /**
     * @return string
     */
    protected function renderSubmitButton()
    {
        return $this->view->render('/partials/save-btn', ['containerOptions' => $this->options]);
    }
}
