<?php

namespace phycom\backend\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\web\View;

class Clock extends Widget
{
	public $tag = 'div';

	public function run()
	{
		$view = $this->getView();
		$date = new \DateTime();
		$today = getdate();

		$year = $today['year'];
		$mon = $today['mon'];
		$mday = $today['mday'];
		$hours = $today['hours'];
		$minutes = $today['minutes'];
		$seconds = $today['seconds'];

		$id = $this->getId();


		echo Html::tag($this->tag, $date->format('H:i:s'), ['class' => 'clock', 'id' => $id]);

		//depends on jQuery, has to be loaded on ready event
		$view->registerJs(<<<JS

	(function(){
		var curDT = new Date(Date.UTC($year, $mon, $mday, $hours, $minutes, $seconds));
		var clock = document.getElementById("$id");
	    setInterval(function () {
	        curDT.setSeconds(curDT.getSeconds() + 1);
	
	        var hours = curDT.getUTCHours();
	        var min = curDT.getUTCMinutes();
	        var sec = curDT.getUTCSeconds();
	
	        hours = ('0' + hours).slice(-2);
	        min = ('0' + min).slice(-2);
	        sec = ('0' + sec).slice(-2);
	
	        clock.innerHTML = (hours + ':' + min + ':' + sec);
	    }, 1000);
    })();
JS
			, View::POS_LOAD);
	}
}