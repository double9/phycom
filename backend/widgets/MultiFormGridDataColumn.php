<?php

namespace phycom\backend\widgets;


use phycom\common\models\ActiveRecord;

use Closure;

use yii\base\Event;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Inflector;

/**
 * Class MultiFormGridDataColumn
 * @package phycom\backend\widgets
 *
 * @property MultiFormGrid $grid
 */
class MultiFormGridDataColumn extends \yii\grid\DataColumn
{
    public $fieldOptions = [];

	public $inputAppend;

	public $inputPrepend;

	public $inputField;

    public function init()
    {
        parent::init();

        if (!$this->contentOptions) {
            $this->contentOptions = function ($model, $key, $index) {
                return ['data-attribute' => $this->attribute];
            };
        }
    }

	/**
	 * Returns the data cell value.
	 * @param mixed $model the data model
	 * @param mixed $key the key associated with the data model
	 * @param int $index the zero-based index of the data model among the models array returned by [[GridView::dataProvider]].
	 * @return string the data cell value
	 */
	public function getDataCellValue($model, $key, $index)
	{
		if ($this->value !== null) {
			if (is_string($this->value)) {
				return $this->renderField($this->value, $model, $key, $index);
			} else {
				return call_user_func($this->value, $model, $key, $index, $this);
			}
		} elseif ($this->attribute !== null) {
			return $this->renderField($this->attribute, $model, $key, $index);
		}
		return null;
	}

	/**
	 * @inheritdoc
	 */
	protected function renderDataCellContent($model, $key, $index)
	{
		if ($this->content === null) {
			$value = $this->getDataCellValue($model, $key, $index);
			return $value === null ? $this->grid->formatter->format(null, $this->format) : $value;
		} else {
			return parent::renderDataCellContent($model, $key, $index);
		}
	}

    /**
     * @param string $attribute
     * @param int $key
     * @return string
     * @throws \ReflectionException
     */
	public function getFieldName($attribute, $key)
    {
        $formModelName = (new \ReflectionClass($this->grid->formModel))->getShortName();
        return $formModelName . '[' . $this->grid->formModelAttribute . '][' . $key . '][' . $attribute . ']';
    }

	protected function renderField($attribute, $model, $key, $index)
	{
		/**
		 * @var ActiveRecord $model
		 */
		if ($model->hasAttribute($attribute)) {
            $value = ArrayHelper::getValue($model, $attribute);
        } else {
		    return '';
        }

        if (false === $this->inputField) {
            return $this->grid->formatter->format($value, $this->format);
        }

		$form = $this->grid->form;

        if ($this->fieldOptions instanceof Closure) {
            $options = call_user_func($this->fieldOptions, $model, $key, $index, $this);
        } else {
            $options = $this->fieldOptions;
        }
		$field = $form->field($model, $attribute);

		$field->inputOptions['id'] = Inflector::camel2id($this->grid->formModelAttribute) . '-' . $key . '-' . $attribute;
		$field->inputOptions['data-attribute-id'] = Inflector::camel2id($this->grid->formModelAttribute) . '-' . $key . '-' . $attribute;
		$field->inputOptions['name'] = $this->getFieldName($attribute, $key);
		$field->inputOptions['value'] = null !== $value ? $this->grid->formatter->format($value, $this->format) : '';

		if ($this->inputAppend) {
			$appendedValue = is_callable($this->inputAppend) ? call_user_func($this->inputAppend, $model) : $this->inputAppend;
			$field->template = '<div class="input-group">{input}<div class="input-group-addon">'.$appendedValue.'</div></div>';
		} else {
			$field->template = '{input}';
		}


		Html::addCssClass($field->options, 'no-margin');
		$field->label(false);

        if (is_callable($this->inputField)) {
            /**
             * @var \yii\widgets\ActiveField $field
             */
            $field = call_user_func($this->inputField, $field, $model, $key);
            foreach ($options as $key => $value) {
                $field->$key = $value;
            }

        } else {
            $field->textInput();
        }

        $this->grid->trigger($this->grid::EVENT_ON_FIELD, new Event(['sender' => ['field' => $field, 'key' => $key]]));

//        if ($key === $this->grid::TEMPLATE_KEY) {
//            $field->enableClientValidation = false;
//            $field->addAriaAttributes = false;
//        }

		return $field;
	}
}
