<?php

namespace phycom\backend\widgets;

use phycom\backend\assets\BtnActionAsset;

use rmrevin\yii\fontawesome\FAS;
use rmrevin\yii\fontawesome\FAR;

use yii\base\Widget;
use yii\helpers\Html;
use yii;

/**
 * Class DeleteAction
 * @package phycom\backend\widgets
 */
class DeleteAction extends BtnAction
{
    const POS_RIGHT = 'dropdown-menu-right';
    const POS_LEFT = 'dropdown-menu-left';

    public $btnOptions = ['class' => 'btn btn-flat btn-default btn-lg'];

    public $containerOptions = ['class' => 'btn-group btn-group-lg delete'];

    public $attributes = ['status' => 'deleted'];

    public $position = self::POS_RIGHT;

    public function init()
    {
        parent::init();
        if (!$this->label) {
            $this->label = FAR::i(FAR::_TRASH_ALT, ['style' => 'margin-right: 10px;']) . Yii::t('backend/main', 'Delete');
        }
    }

    protected function renderSubmitButton()
    {
        $btnOptions = $this->btnOptions;
        $btnOptions['data-toggle'] = 'dropdown';
        $btnOptions['aria-haspopup'] = 'true';
        $btnOptions['aria-expanded'] = 'false';

        Html::addCssClass($btnOptions, ['dropdown-toggle', 'delete-confirm']);

        $html = Html::beginTag('div', $this->containerOptions);
            $html .= Html::button($this->label, $btnOptions);
            $html .= Html::beginTag('ul', ['class' => 'dropdown-menu no-padding ' . $this->position]);

                $html .= Html::beginTag('li', ['class' => 'confirm']);
                    $html .= Html::submitButton(FAS::i(FAS::_EXCLAMATION_CIRCLE, ['style' => 'margin-right: 10px;']) . Yii::t('backend/main', 'Confirm Delete'), ['class' => 'dropdown-menu-button']);
                $html .= Html::endTag('li');

                $html .= Html::tag(
                    'li',
                    Html::a(FAS::i(FAS::_TIMES_CIRCLE, ['style' => 'margin-right: 10px;']) . Yii::t('backend/main', 'Cancel'), 'javascript:;', ['onclick' => 'return false;']),
                    ['class' => 'cancel']
                );

            $html .= Html::endTag('ul');
        $html .= Html::endTag('div');

        return $html;
    }
}