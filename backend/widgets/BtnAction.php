<?php

namespace phycom\backend\widgets;

use phycom\backend\assets\BtnActionAsset;

use yii\base\Widget;
use yii\helpers\Html;

/**
 * Class BtnAction
 * @package phycom\backend\widgets
 */
class BtnAction extends Widget
{
    public $method = 'post';

    public $options = ['style' => 'display: inline-block;'];

    public $btnOptions = ['class' => 'btn btn-flat btn-primary btn-lg'];

    public $model;

    public $attributes = [];

    public $label;

    public function run()
    {
        $options = $this->options;
        $id = $options['id'] = $options['id'] ?? $this->getId();

        echo Html::beginForm('', $this->method, $options);

        foreach ($this->attributes as $attribute => $value) {
            echo Html::activeHiddenInput($this->model, $attribute, ['value' => $value]);
        }

        echo $this->renderSubmitButton();
        echo Html::endForm();

        BtnActionAsset::register($this->view);
        $this->view->registerJs("jQuery(function($){BtnAction.init('#$id')});");
    }

    protected function renderSubmitButton()
    {
        return Html::submitButton($this->label, $this->btnOptions);
    }
}