<?php

namespace phycom\backend\widgets;

use phycom\common\interfaces\ModuleSettingsInterface;
use phycom\common\models\ModuleSettingsForm;

use yii\helpers\Inflector;
use yii\bootstrap\Widget;
use yii;

/**
 * Class ModuleSettings
 * @package phycom\backend\widgets
 *
 * @property-write ModuleSettingsInterface $module
 * @property-write ModuleSettingsForm $model
 */
class ModuleSettings extends Widget
{
    public $action;

    protected $module;
    /**
     * @var ModuleSettingsForm
     */
    protected $model;

    public function setModule(ModuleSettingsInterface $module)
    {
        $this->module = $module;
        $this->model = $module->getSettings();
    }

    public function setModel(ModuleSettingsForm $model)
    {
        $this->model = $model;
        $this->module = $model->module;
    }

    public function run()
    {
        parent::run();
        $form = ActiveForm::begin([
            'id' => $this->module->id . '-settings'
        ]);
        foreach ($this->model->attributes as $attribute => $value) {
            $field = $form->field($this->model, $attribute);
            echo $this->renderField($field);
        }
        ActiveForm::end();
    }


    protected function renderField(ActiveField $field)
    {
        $format = $this->model->getAttributeType($field->attribute);
        switch ($format) {
            case ModuleSettingsForm::TYPE_INTEGER:
                return $field->textInput(['type' => 'number', 'step' => 1]);
            case ModuleSettingsForm::TYPE_DECIMAL:
                return $field->textInput(['type' => 'number', 'step' => 0.01]);
            case ModuleSettingsForm::TYPE_BOOLEAN:
                return $field->boolean();
            case ModuleSettingsForm::TYPE_OPTION:
                foreach ($this->model->getAttributeRules($field->attribute) as $rule) {
                    if ($rule[0] === 'in') {
                        $items = [];
                        foreach ($rule['range'] as $item) {
                            $items[$item] = Inflector::titleize(strtolower($item));
                        }
                        return $field->dropDownList($items);
                    }
                }
                throw new yii\base\InvalidValueException('Required option "in" validator is missing');

            case ModuleSettingsForm::TYPE_DATETIME:
                return $field->datePicker();
            case ModuleSettingsForm::TYPE_TEXT:
            default:
                return $field->textInput();
        }
    }
}