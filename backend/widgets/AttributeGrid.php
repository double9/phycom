<?php

namespace phycom\backend\widgets;

use phycom\backend\assets\AttributeGridAsset;
use phycom\common\models\ActiveRecord;
use phycom\common\models\attributes\ProductVariantStatus;
use phycom\common\models\product\ProductVariant;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii;

/**
 * Class AttributeGrid
 * @package phycom\backend\widgets
 *
 * @property ActiveForm $form
 * @property yii\data\ActiveDataProvider $dataProvider
 */
class AttributeGrid extends SimpleGrid
{
	const TEMPLATE_KEY = '__tpl__';

	public $formModel;

	public $modelKey;

	public $formModelAttribute;

	public $formOptions = [];

	public $tableOptions = ['class' => 'table table-responsive table-striped table-hover no-margin'];

	public $layout = "{errors}\n{items}\n{new}";

	public $actions = [];

	public $newRow;

	public $dataColumnClass = AttributeGridDataColumn::class;

	/**
	 * @var ActiveForm
	 */
	public $form;

    /**
     * @return array
     */
	public function getDefaultColumns()
    {
        return [
            [
                'attribute'  => 'label',
                'format'     => 'text',
                'inputField' => false,
            ],
            [
                'attribute'  => 'status',
                'format'     => 'text',
                'inputField' => function ($field, $model, $key) {
                    /**
                     * @var \phycom\backend\widgets\ActiveField $field
                     */
                    return $field->dropDownList(ProductVariantStatus::displayValues(), ['class' => 'input-sm form-control']);
                },
                'options'    => ['width' => 100]
            ],
            [
                'attribute'  => 'created_at',
                'format'     => 'datetime',
                'inputField' => false,
                'options'    => ['width' => 200]
            ]
        ];
    }

	public function init()
	{
	    if (empty($this->columns)) {
	        $this->columns = $this->getDefaultColumns();
        }
		$this->columns[] = [
			'label' => false,
			'content' => function ($model, $key) {

				$html = Html::a('<span class="far fa-trash-alt"></span>', '#', ['class' => 'btn btn-flat btn-danger remove-row']);
				$formModelName = (new \ReflectionClass($this->formModel))->getShortName();
				$name = $formModelName . '[' . $this->formModelAttribute . '][' . $key . '][' . $this->modelKey . ']';
				$html .= Html::hiddenInput($name, $model->name, ['data-attribute' => $this->modelKey]);
				return $html;
			},
			'options' => ['width' => 50]
		];

		parent::init();
		Html::addCssClass($this->tableOptions, 'attribute-table');
		$this->actions[] = ['label' => '<span class="fas fa-plus"></span>&nbsp;&nbsp;' . Yii::t('backend/main', 'Add new'), 'options' => ['class' => 'add-row']];
		$this->formOptions = ArrayHelper::merge($this->formOptions, ['id' => $this->id . '-form']);
	}

	public function run()
	{
		$this->form = ActiveForm::begin($this->formOptions);

		$id = $this->form->id;
		$view = $this->getView();
		AttributeGridAsset::register($view);
		parent::run();

		$this->form::end();

		if ($this->form->enableClientScript) {
			$formClientOptions = Json::encode($this->form->attributes);
			$view->registerJs("attributeGrid.init('#$id', $formClientOptions);");
		}
	}

	public function renderSection($name)
	{
		switch ($name) {
			case '{actions}':
				return $this->renderActions();
			case '{new}':
				return $this->renderNew();
			default:
				return parent::renderSection($name);
		}
	}

	/**
	 * Renders the table body.
	 * @return string the rendering result.
	 */
	public function renderTableBody()
	{
		$models = array_values($this->dataProvider->getModels());
		$keys = $this->dataProvider->getKeys();
		$rows = [];
		foreach ($models as $index => $model) {
			$key = $keys[$index];
			if ($this->beforeRow !== null) {
				$row = call_user_func($this->beforeRow, $model, $key, $index, $this);
				if (!empty($row)) {
					$rows[] = $row;
				}
			}

			$rows[] = $this->renderTableRow($model, $key, $index);

			if ($this->afterRow !== null) {
				$row = call_user_func($this->afterRow, $model, $key, $index, $this);
				if (!empty($row)) {
					$rows[] = $row;
				}
			}
		}

		$rows[] = $this->renderNewModelTemplate(count($models));

		if (empty($rows)) {
			$colspan = count($this->columns);

			return "<tbody>\n<tr><td colspan=\"$colspan\">" . $this->renderEmpty() . "</td></tr>\n</tbody>";
		} else {
			return "<tbody>\n" . implode("\n", $rows) . "\n</tbody>";
		}
	}

	protected function renderNewModelTemplate($index)
	{
		$modelClass = $this->dataProvider->query->modelClass;
		/**
		 * @var ActiveRecord $model
		 */
		$model = new $modelClass;
		$model->loadDefaultValues();

		return $this->renderTableRow($model, static::TEMPLATE_KEY, $index);
	}

	protected function renderNew()
	{
		return call_user_func($this->newRow);
	}

	protected function renderActions()
	{
		$html = Html::beginTag('div', ['class' => 'actions clearfix']);
		foreach ($this->actions as $action) {
			if (!isset($action['url'])) {
				$action['url'] = '#';
			}
			if (!isset($action['options'])) {
				$action['options'] = [];
			}
			Html::addCssClass($action['options'], 'btn btn-flat btn-default');
			$html .= Html::a($action['label'], $action['url'], $action['options']);
		}
		$html .= Html::endTag('div');
		return $html;
	}
}
