<?php

namespace phycom\backend\widgets;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\Widget;
use yii;

/**
 * Class Modal
 * @package phycom\backend\widgets
 */
class Modal extends Widget
{
	public $content;
	public $title;
	public $bodyOptions = ['class' => 'panel-body'];
	public $noMargin = false;
	public $actions = false;

	protected $templateParts = [];
	protected $contentDisplayed = false;

    public static function begin($config = [])
    {
        $buttonLabel = null;
        $buttonOptions = [];
        if (isset($config['button'], $config['button']['label'])) {
            $buttonLabel = $config['button']['label'];
            unset($config['button']['label']);
            $buttonOptions = $config['button'];
            unset($config['button']);
        }
        /**
         * @var static $modal
         */
        $modal = parent::begin($config);

        if ($buttonLabel) {
            echo Html::button($buttonLabel, ArrayHelper::merge([
                'data-toggle' => 'modal',
                'data-target' => '#' . $modal->getId()
            ], $buttonOptions));
        }

        echo $modal->templateParts[0];
        $modal->contentDisplayed = true;
        return $modal;
    }

	public function init()
	{
		parent::init();
		if (!isset($this->options['id'])) {
			$this->options['id'] = $this->getId();
		}
		$this->options['title'] = $this->title ?: '&nbsp;';
		$this->options['content'] = '___';

		$defaultBodyOptions = $this->noMargin ? ['class' => 'panel-body no-margin'] : [];
		$this->options['bodyOptions'] = yii\helpers\ArrayHelper::merge($defaultBodyOptions, $this->bodyOptions);
        $this->options['footer'] = $this->renderFooter();

		$this->templateParts = explode('___', $this->getView()->renderFile( $this->viewPath . '/partials/modal.php', $this->options));
	}

	public function run()
	{
		if (!$this->contentDisplayed) {
			echo $this->templateParts[0] . $this->content;
			$this->contentDisplayed = true;
		}
		echo $this->templateParts[1];
	}

    /**
     * @return string
     */
	public function getViewPath()
	{
		return Yii::$app->viewPath;
	}

    /**
     * @return string
     */
	protected function renderFooter()
    {
        $footer = '';
        if ($this->actions && is_array($this->actions) && !empty($this->actions)) {
            foreach ($this->actions as $key => $item) {
                $url = $item['url'] ?? false;
                $options = [
                    'class' => $item['class'] ?? 'btn btn-default btn-flat'
                ];
                Html::addCssClass($options, 'pull-right btn btn-lg btn-flat');

                if ($key === 0) {
                    $options['style'] = 'margin-left: 20px;';
                }
                $footer .= ($url ? Html::a($item['title'], $url, $options) : Html::button($item['title'], $options));
            }
        }
        return $footer;
    }
}
