<?php

namespace phycom\backend\widgets;

use yii\helpers\Html;
use yii;

/**
 * Class Box
 * @package phycom\backend\widgets
 */
class Box extends yii\base\Widget
{
	public $options = ['class' => 'box box-danger'];
	public $headerOptions = ['class' => 'box-header with-border'];
	public $bodyOptions = ['class' => 'box-body no-padding'];
	public $title = '';
	public $content = '';
	public $showHeader = true;

	public static function begin($config = [])
	{
		/**
		 * @var static $widget
		 */
		$widget = parent::begin($config);
		echo $widget->renderBoxStart();
		return $widget;
	}

	public static function end()
	{
		/**
		 * @var static $widget
		 */
		$widget = parent::end();
		echo $widget->renderBoxEnd();
		return $widget;
	}

	public function run()
	{
		echo $this->content;
	}

	public function __toString()
	{
		return '';
	}

	protected function renderBoxStart()
	{
		$html = Html::beginTag('div', $this->options);
		if ($this->showHeader) {
			$html .= Html::beginTag('div', $this->headerOptions);
			$html .= Html::tag('h3', $this->title, ['class' => 'box-title']);
			$html .= Html::endTag('div');
		}
		$html .= Html::beginTag('div', $this->bodyOptions);
		return $html;
	}

	protected function renderBoxEnd()
	{
		return Html::endTag('div') . Html::endTag('div');
	}
}