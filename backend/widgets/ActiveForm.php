<?php

namespace phycom\backend\widgets;

use phycom\backend\models\ModelCollectionForm;
use phycom\common\helpers\Filter;
use phycom\backend\widgets\ActiveField;

use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class ActiveForm
 * @package phycom\backend\widgets
 */
class ActiveForm extends \yii\bootstrap\ActiveForm
{
	public $fieldClass = ActiveField::class;
	/**
	 * Renders a date-range picker
	 *
	 * @param object $model - Search model
	 * @param string $from - model attribute name for date range starting
	 * @param string $to - model attribute name for date range ending
	 * @param array $options
	 * @return string
	 */
	public function dateRangePicker($model, $from, $to, array $options = [])
	{
		return Filter::dateRangePicker($model, $from, $to, $options);
	}

	/**
	 * @inheritdoc
	 * @return ActiveField the created ActiveField object
	 */
	public function field($model, $attribute, $options = [])
	{
		return parent::field($model, $attribute, $options);
	}

    /**
     * Creates an active field for a MultiModel form setup
     *
     * @param ModelCollectionForm $formModel
     * @param Model $model
     * @param $attribute
     * @param array $options
     * @return \yii\bootstrap\ActiveField
     * @throws \ReflectionException
     */
	public function mField(ModelCollectionForm $formModel, Model $model, $attribute, $options = [])
    {
        $key = $this->findCollectionKey($formModel->models, $model);
        $formModelName = (new \ReflectionClass($formModel))->getShortName();

        $inputId = strtolower($formModelName) . '-' . $key . '-' . str_replace('_', '-', $attribute);

        $defaultOptions = [
            'template' => '{input}',
            'inputOptions' => [
                'id' =>  $inputId,
                'name' => $formModelName . '[models][' . $key . '][' . $attribute . ']',
            ],
            'selectors' => [
                'container' => '.field-' . $inputId,
                'input'     => '#' . $inputId,
            ],
            'enableClientValidation' => false
        ];

        return parent::field($model, $attribute, ArrayHelper::merge($defaultOptions, $options))->label(false);
    }

    /**
     * @param array $collection
     * @param $model
     * @return null|string
     */
    private function findCollectionKey(array $collection, $model)
    {
        foreach ($collection as $key => $value) {
            if ($value === $model) {
                return $key;
            }
        }
        return null;
    }
}
