<?php

namespace phycom\backend\widgets;

use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class SimpleGrid
 * @package phycom\backend\widgets
 */
class SimpleGrid extends GridView
{
	public $layout = "{errors}\n{items}";

	public $containerOptions = ['class' => 'ajax-grid'];

	public $tableOptions = ['class' => 'table table-responsive table-striped no-margin'];

	public $disableSorting = true;

	public function init()
	{
		parent::init();
		$this->pager = [false];
	}

	public function run()
	{
		echo $this->renderContainerStart();
		parent::run();
		echo $this->renderContainerEnd();
	}

	protected function renderContainerStart()
	{
		$options = $this->containerOptions;
		return Html::beginTag('div', $options);
	}

	protected function renderContainerEnd()
	{
		return Html::endTag('div');
	}

	protected function initColumns()
	{
		parent::initColumns();
		if ($this->disableSorting) {
			/**
			 * @var DataColumn $column
			 */
			foreach ($this->columns as $column) {
				$column->enableSorting = false;
			}
		}
	}

}