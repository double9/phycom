<?php

namespace phycom\backend\widgets;

use phycom\backend\assets\TabsAsset;

use yii\helpers\Html;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

/**
 * Class Tabs2
 * @package phycom\backend\widgets
 */
class Tabs2 extends Widget
{
    public $options = ['class' => 'nav-pills-custom'];

    public $navContainerOptions = ['class' => 'nav-pills-container col-md-2'];

    public $navOptions = ['class' => 'nav nav-pills nav-stacked'];

    public $contentOptions = ['class' => 'tab-content col-md-10 col-lg-8 col-xl-6'];

    public $contentBeforeNav;

    public $items = [];

    public $defaultViewParams = [];

    public $activeTab;

    public function run()
    {
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->id;
        }
        echo Html::beginTag('div', $this->options);
        echo Html::tag('div', $this->renderNav(), $this->navContainerOptions);
        echo Html::tag('div', $this->renderContent(), $this->contentOptions);
        echo Html::endTag('div');

        $view = $this->getView();
        TabsAsset::register($view);
        $id = $this->id;
        $active = $this->activeTab;
        $view->registerJs("jQuery('#$id').stickyTabs();", $view::POS_END);

//        if ($active) {
//            $view->registerJs(<<<JS
//                (function ($) {
//                    if ($('#t2-$active').length && !window.location.hash) {
//                        $('a[href=\'#t2-$active\']').trigger('click');
//                    }
//                })(jQuery);
//JS
//            );
//        }
    }

    protected function renderContent()
    {
        $html = '';
        foreach ($this->items as $key => $item) {

            if (isset($item['visible']) && false === $item['visible']) {
                continue;
            }

            $params = ArrayHelper::merge(
                $this->defaultViewParams,
                $item['params'] ?? [],
                [
                    'tab' => $key,
                    'tabId' => '#t2-' . $key
                ]
            );

            $content = $item['content'] ?? $this->getView()->render($item['view'], $params);
            $options = ['id' => 't2-' . $key, 'class' => 'tab-pane'];

            if ($formCollection = $item['formCollection'] ?? false) {
                $options['data-form-collection'] = 1;
            }
            if ($formSubmitAjax = $item['formSubmitAjax'] ?? false) {
                $options['data-form-ajax'] = 1;
            }
            if ($formAction = $item['formAction'] ?? false) {
                $options['data-action'] = $formAction;
            }
            if ($key === $this->activeTab) {
                Html::addCssClass($options, 'active');
            }
            $html .= Html::tag('div', $content, $options);
        }
        return $html;
    }

    protected function renderNav()
    {
        $html = '';
        foreach ($this->items as $key => $item) {

            if (isset($item['visible']) && false === $item['visible']) {
                continue;
            }

            $options = ['class' => ''];
            if ($key === $this->activeTab) {
                Html::addCssClass($options, 'active');
            }
            $html .= Html::tag('li', '<a href="#t2-' . $key . '" data-toggle="pill">' . $item['label'] . '</a>', $options);
        }
        return ($this->contentBeforeNav ?: '') . Html::tag('ul', $html, $this->navOptions);
    }
}
