<?php

namespace phycom\backend\widgets;

use yii\helpers\Html;
use yii\base\Widget;
use yii\widgets\Pjax;

/**
 * Class Tabs
 * @package phycom\backend\widgets
 */
class Tabs extends Widget
{
	const TYPE_AJAX = 'ajax';
	const TYPE_PAJAX = 'pajax';

	const RENDER_MODE_ACTIVE = 'render-active';
	const RENDER_MODE_ALL = 'render-all';

	public $options = ['class' => 'nav-tabs-custom'];

	public $navOptions = ['class' => 'nav nav-tabs clearfix', 'role' => 'tablist'];
	public $navItemOptions = ['role' => 'presentation'];
	public $navItemLinkOptions = ['aria-expanded' => 'true', 'role' => 'tab'];

	public $tabContentOptions = ['class' => 'tab-content'];
	public $tabItemOptions = ['role' => 'tabpanel', 'class' => 'tab-pane'];

	public $renderMode = self::RENDER_MODE_ACTIVE;

	public $ajaxType = self::TYPE_PAJAX;
	public $ajax = false;
	public $pajaxOptions = [];

	public $items = [];
	public $active;


	public static function begin($config = [])
	{
		$config['renderMode'] = self::RENDER_MODE_ACTIVE;
		foreach($config['items'] as $key => $tab) {
			$config['items'][$key]['content'] = '';
		}
		return parent::begin($config);
	}

	/**
	 * Initializes the widget.
	 * This renders the form open tag.
	 */
	public function init()
	{
		if (!isset($this->options['id'])) {
			$this->options['id'] = $this->getId();
		}
		ob_start();
		ob_implicit_flush(false);
	}


	public function run()
	{
		$content = ob_get_clean();

		if ($this->ajax && $this->ajaxType === self::TYPE_PAJAX) {
			Pjax::begin($this->pajaxOptions);
		}

		echo $this->renderTabsStart();
		echo $this->renderNav();
		echo $this->renderContentStart();

		if ($this->renderMode === self::RENDER_MODE_ACTIVE) {
			echo $content;
		} else {
			foreach ($this->items as $tab) {
				echo $this->renderTabContent($tab);
			}
		}

		echo $this->renderContentEnd();
		echo $this->renderTabsEnd();

		if ($this->ajax && $this->ajaxType === self::TYPE_PAJAX) {
			Pjax::end();
		}
	}


	public function __toString()
	{
		return '';
	}

	protected function renderNav()
	{
		$html = Html::beginTag('ul', $this->navOptions);
		foreach ($this->items as $tab) {
			$html .= $this->renderNavLink($tab);
		}
		$html .= Html::endTag('ul');
		return $html;
	}

	protected function renderNavLink($tab)
	{
		$navItemOptions = $this->navItemOptions;
		$navItemOptions['data-id'] = $tab['id'];
		if (isset($tab['key'])) {
			$navItemOptions['data-key'] = $tab['key'];
		}
		if ($tab['id'] === $this->active) {
			Html::addCssClass($navItemOptions, 'active');
		}

		$url = $tab['url'] ?? $tab['id'];

		$html = Html::beginTag('li', $navItemOptions);
		$html .= Html::a($tab['label'], $url, $this->navItemLinkOptions);
		$html .= Html::endTag('li');
		return $html;
	}

	protected function renderContentStart()
	{
		return Html::beginTag('div', $this->tabContentOptions);
	}

	protected function renderContentEnd()
	{
		return Html::endTag('div');
	}

	protected function renderTabContent($tab)
	{
		if (!$tab['content']) {
			return '';
		}
		$tabOptions = $this->tabItemOptions;
		if (isset($tab['key'])) {
			$tabOptions['data-key'] = $tab['key'];
		}
		if ($tabOptions['id'] === $this->active) {
			Html::addCssClass($tabOptions, 'active');
		}

		if ($this->renderMode === self::RENDER_MODE_ALL || ($this->renderMode === self::RENDER_MODE_ACTIVE && $tabOptions['id'] === $this->active)) {
			$content = $this->getContent($tab);
		} else {
			$content = '';
		}

		return Html::tag('div', $content, $tabOptions);
	}

	protected function getContent($tab)
	{
		return $this->view->render($tab['content']);
	}

	protected function renderTabsStart()
	{
		$html = Html::beginTag('div', ['class' => $this->options]);
		return $html;
	}

	protected function renderTabsEnd()
	{
		return Html::endTag('div');
	}
}

