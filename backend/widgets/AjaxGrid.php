<?php

namespace phycom\backend\widgets;

use phycom\backend\assets\AjaxGridAsset;

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii;

/**
 * Class AjaxGrid
 * @package phycom\backend\widgets
 */
class AjaxGrid extends GridView
{
	const TYPE_AJAX = 'ajax';
	const TYPE_PAJAX = 'pajax';
	/**
	 * @var string
	 */
	public $route;

	public $layout = "{bulkEdit}\n{errors}\n{items}\n{navigation}";

	public $containerOptions = ['class' => 'ajax-grid'];

	public $tableOptions = ['class' => 'table table-responsive table-striped no-margin table-hover'];
	/**
	 * Selects the implementation 'ajax' or 'pajax'
	 * @var string
	 */
	public $type = 'ajax';

	public $ajaxEnabled = true;
    /**
     * @var string
     */
	public $bulkEditForm;
    /**
     * @var \Closure
     */
    public $bulkEditCondition;
    /**
     * @var bool|array - if set automatically appends the checkbox column when bulk edit form is defined
     */
    public $bulkEditAddCheckboxColumn = [
        'class'          => CheckboxColumn::class,
        'options'        => ['width' => '30'],
        'contentOptions' => ['class' => 'row-checkbox'],
    ];


	protected $isAjaxCall = false;


	public function init()
	{
		parent::init();
		$this->pager = [
			'class' => LinkPager::class,
			'nextPageLabel' => Yii::t('backend/main', 'Next'),
			'prevPageLabel' => Yii::t('backend/main', 'Previous'),
			'options' => [
				'class' => 'no-margin pagination pagination-sm pull-right'
			]
		];
		if ($this->route && $this->dataProvider->getPagination()) {
			$this->dataProvider->getPagination()->route = $this->route;
		}
		$this->checkAjaxCall();
	}

	protected function checkAjaxCall()
	{
		$view = $this->getView();
		if ($view->context) {
            $route = $view->context->action->controller->id . '/' . $view->context->action->id;
            $this->isAjaxCall = $route === $this->route;
        }
	}

	public function beforeRun()
	{
		if (!parent::beforeRun()) {
			return false;
		}
		if ($this->type === self::TYPE_PAJAX) {
			Pjax::begin();
		}
		if (!$this->isAjaxCall) {
			echo $this->renderContainerStart();
		}
		return true;
	}

	public function afterRun($result)
	{
		if (!$this->isAjaxCall) {
			echo $this->renderContainerEnd();
		}
		if ($this->type === self::TYPE_PAJAX) {
			Pjax::end();
		}
		return parent::afterRun($result);
	}

	public function run()
	{
		if ($this->ajaxEnabled) {
			$id = $this->options['id'];
			$view = $this->getView();
			AjaxGridAsset::register($view);
			$view->registerJs("ajaxGrid.init('#$id');");
		}
		parent::run();
	}

	public function renderSection($name)
	{
		switch ($name) {
		    case '{bulkEdit}';
		        return $this->renderBulkEditForm();
			case '{navigation}':
				return $this->renderNavigation();
			default:
				return parent::renderSection($name);
		}
	}

	protected function renderContainerStart()
	{
		$options = $this->containerOptions;
		$options['data-url'] = $this->route ? Url::toRoute($this->route) : Url::current();

		return Html::beginTag('div', $options);
	}

	protected function renderContainerEnd()
	{
		return Html::endTag('div');
	}

	protected function renderNavigation()
	{
		$html =  Html::beginTag('div', ['class' => 'table-nav']) . Html::beginTag('div', ['class' => 'row']);
		$html .= Html::tag('div', $this->renderSummary(), ['class' => 'col-md-4']);
		$html .= Html::tag('div', $this->renderPager(), ['class' => 'col-md-8']);
		$html .= Html::endTag('div') . Html::endTag('div');
		return $html;
	}

    protected function initColumns()
    {
        if ($this->bulkEditForm && $checkboxColumn = $this->bulkEditAddCheckboxColumn) {
            if ($this->bulkEditCondition instanceof \Closure) {
                $checkboxColumn['visibleOnCondition'] = $this->bulkEditCondition;
            }
            array_unshift($this->columns, $checkboxColumn);
        }
        parent::initColumns();
    }

	protected function renderBulkEditForm()
    {
        if ($this->bulkEditForm) {
            return Html::tag('div', $this->view->render($this->bulkEditForm, ['grid' => $this]), ['id' => 'bulk-edit-form', 'class' => 'bulk-edit-box', 'style' => 'display: none;']);
        }
        return '';
    }
}
