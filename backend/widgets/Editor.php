<?php

namespace phycom\backend\widgets;

use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * Class Editor
 * @package phycom\backend\widgets
 */
class Editor extends CKEditor
{
	public $pajaxId;

	public function init()
	{
		parent::init();
		Html::addCssClass($this->options, 'form-control');
	}

	public function run()
	{
		parent::run();
		$pajaxId = $this->pajaxId;
		if ($pajaxId) {
			$view = $this->getView();
			$id = $this->options['id'];

			$options = $this->clientOptions !== false && !empty($this->clientOptions)
				? Json::encode($this->clientOptions)
				: '{}';

//			$view->registerJs("jQuery('#$pajaxId').on('pjax:end', function() { CKEDITOR.replaceAll(function(textarea,config) {config = '$options'; return true;}); });");
//			$view->registerJs("jQuery('#$pajaxId').on('pjax:end', function() { CKEDITOR.replace('$id', $options); dosamigos.ckEditorWidget.registerOnChangeHandler('$id'); });");
//
			$js[] = "jQuery('#$pajaxId').on('pjax:end', function() {";
//
//			$js[] = "if (typeof  CKEDITOR.instances['$id'] != 'undefined') CKEDITOR.instances['$id'].destroy(true);";

			$js[] = "if(jQuery('#$id').length) {";
			$js[] = "CKEDITOR.replace('$id', $options);";
			$js[] = "dosamigos.ckEditorWidget.registerOnChangeHandler('$id');";
			if (isset($this->clientOptions['filebrowserUploadUrl']) || isset($this->clientOptions['filebrowserImageUploadUrl'])) {
				$js[] = "dosamigos.ckEditorWidget.registerCsrfImageUploadHandler();";
			}
			$js[] = "}";
			$js[] = "});";
			$view->registerJs(implode("\n", $js));
		}
	}
}