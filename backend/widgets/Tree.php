<?php

namespace phycom\backend\widgets;

use phycom\backend\assets\TreeAsset;

use \dkhlystov\widgets\TreeGrid;

use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * Class Tree
 * @package phycom\backend\widgets
 *
 * @property ActiveDataProvider $dataProvider
 */
class Tree extends TreeGrid
{
	public $tableOptions = ['class' => 'table'];

	public $showHeader = false;

	public $showRoots = true;

	public $lazyLoad = false;

	public $rootParentId = null;

	public $pajaxId;

	public function init()
	{
		$this->dataProvider->pagination = false;
		$this->dataProvider->getModels(); // hotfix for the sorting issue
		parent::init();
	}

	public function initColumns()
	{
		if (!empty($this->columns))	{
			Html::addCssClass($this->columns[0]['contentOptions'], 'treegrid-col-1');
		}
		parent::initColumns();
	}

	public function run()
	{
		$view = $this->getView();
		TreeAsset::register($view);
		parent::run();

		$pajaxId = $this->pajaxId;
		if ($pajaxId) {
			$id = $this->options['id'];
			$options = Json::htmlEncode($this->getClientOptions());
			$view->registerJs(" jQuery('#$pajaxId').on('pjax:end', function() { if(jQuery('#$id').length) jQuery('#$id > table').treegrid($options); });");
		}
	}
}