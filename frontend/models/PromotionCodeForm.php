<?php

namespace phycom\frontend\models;

use phycom\common\models\traits\ModelTrait;

use phycom\common\models\attributes\DiscountRuleStatus;
use phycom\common\models\attributes\DiscountRuleType;
use phycom\common\models\DiscountRule;
use phycom\common\models\product\ProductCategory;

use yii\helpers\ArrayHelper;
use yii;


/**
 * Class PromotionCodeForm
 * @package phycom\frontend\models
 */
class PromotionCodeForm extends yii\base\Model
{
    use ModelTrait;

    public $code;

    protected $model;

    public function rules()
    {
        return [
            ['code', 'trim'],
            ['code', 'string', 'max' => 255],
            ['code', 'required', 'enableClientValidation' => false],
            ['code', 'exist', 'targetClass' => DiscountRule::class, 'targetAttribute' => 'code']
        ];
    }

    public function attributeLabels()
    {
        return [
            'code' => Yii::t('public/discount-rule', 'Discount code')
        ];
    }


    public function attributeHints()
    {
        return [
            'code' => Yii::t('public/discount-rule', 'Enter your coupon code if you have one')
        ];
    }


    public function getModel()
    {
        if (!$this->model && $this->code) {
            $this->model = DiscountRule::findOne(['code' => $this->code]);
        }
        return $this->model;
    }


    public function afterValidate()
    {
        $model = $this->getModel();
        if ($model && !$model->isValid) {
            $this->addError('code', Yii::t('public/discount-rule', 'Invalid code'));
            return false;
        }
        parent::afterValidate();
    }

    /**
     * @return false|DiscountRule
     */
    public function apply()
    {
        if ($this->validate()) {
            Yii::$app->cart->add($this->getModel());
            return $this->model;
        }
        return false;
    }
}