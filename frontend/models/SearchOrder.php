<?php

namespace phycom\frontend\models;

use phycom\common\components\ActiveQuery;
use phycom\common\interfaces\SearchModelInterface;
use phycom\common\models\attributes\OrderStatus;
use phycom\common\models\behaviors\CurrencyBehavior;
use phycom\common\models\Order;
use phycom\common\models\OrderItem;
use phycom\common\models\traits\SearchQueryFilter;
use phycom\common\models\User;

use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii;

/**
 * Class SearchOrder
 * @package phycom\frontend\models
 */
class SearchOrder extends Order implements SearchModelInterface
{
    use SearchQueryFilter;

    public $total;

    public $createdFrom;
    public $createdTo;
    public $updatedFrom;
    public $updatedTo;
    public $paidFrom;
    public $paidTo;

    public $hiddenStatus = [OrderStatus::DELETED, OrderStatus::EXPIRED];


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['currency'] = [
            'class' => CurrencyBehavior::class,
            'attributes' => ['total']
        ];
        return $behaviors;
    }

    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['id', 'user_id', 'shop_id'], 'integer'],
            [['comment'], 'string'],
            [['paid_at', 'created_at', 'updated_at'], 'safe'],
            [['number', 'promotion_code'], 'string', 'max' => 255],
            [['createdFrom','createdTo','updatedFrom','updatedTo','paidFrom','paidTo','total','status'],'safe']
        ];
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'total' => Yii::t('public/main', 'Total')
        ]);
    }

    public function search(array $params = [])
    {
        $query = $this->createSearchQuery();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 20
            ],
        ]);

        $this->sort($dataProvider->sort);

        if (!($this->load($params) && $this->validate())) {
            if (!empty($this->hiddenStatus)) {
                $query->andWhere(['not', ['o.status' => $this->hiddenStatus]]);
            }
            return $dataProvider;
        }

        if (!empty($this->hiddenStatus)) {
            if (!$this->status || !$this->status->in($this->hiddenStatus)) {
                $query->andWhere(['not', ['o.status' => $this->hiddenStatus]]);
            }
        }

        $query->andFilterWhere([
            'o.id' => $this->id,
            'o.number' => $this->number,
            'o.status' => (string)$this->status,
        ]);

        if ($this->total) {
            $query->andHaving('sum(oi.total) = :total', ['total' => $this->total]);
        }

        $query->filterDateRange('o.created_at', $this->createdFrom, $this->createdTo);
        $query->filterDateRange('o.updated_at', $this->updatedFrom, $this->updatedTo);
        $query->filterDateRange('o.paid_at', $this->paidFrom, $this->paidTo);

        return $dataProvider;
    }


    protected function sort(Sort $sort)
    {
        $sort->attributes['total'] = [
            'asc' => [new Expression('sum(oi.total) ASC NULLS FIRST')],
            'desc' => [new Expression('sum(oi.total) DESC NULLS LAST')]
        ];
    }

    /**
     * @return ActiveQuery
     * @throws \yii\base\Exception
     */
    protected function createSearchQuery()
    {
        $query = static::find();
        $query->select([
            'sum(oi.total) AS total',
            'o.*'
        ]);
        $query->from(['o' => Order::tableName()]);
        $query->innerJoin(['u' => User::tableName()], 'o.user_id = u.id');
        $query->leftJoin(['oi' => OrderItem::tableName()], 'oi.order_id = o.id');
        $query->andWhere(['o.user_id' => $this->user_id]);
        $query->groupBy([
            'o.id',
            'u.first_name',
            'u.last_name',
        ]);
        return $query;
    }
}