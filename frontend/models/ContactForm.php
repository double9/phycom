<?php

namespace phycom\frontend\models;


use phycom\common\models\attributes\MessageStatus;
use phycom\common\models\attributes\MessageType;
use phycom\common\models\Email;
use phycom\common\models\Message;

use yii\base\Model;
use yii;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;

    public function init()
    {
        parent::init();
        $this->subject = Yii::$app->name . ': Contact request sent from ' . Yii::$app->urlManagerFrontend->getHostInfo();
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
//            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param Email|string $email the target email address
     * @param string $name
     *
     * @return bool - whether the email was sent
     * @throws yii\base\Exception
     */
    public function sendEmail($email, $name = null)
    {
        if ($this->validate()) {

            $msg = new Message();
            $msg->type = MessageType::EMAIL;
            $msg->status = MessageStatus::NEW;
            $msg->from = $this->email;
            $msg->from_name = $this->name;
            $msg->to = (string) $email;
            $msg->to_name = $name;
            $msg->subject = $this->subject;
            $msg->content = $this->body;

            if (!$msg->save()) {
                Yii::error('Error saving message: ' . json_encode($msg->errors), __METHOD__);
                return false;
            }
            return $msg->queue();
        }
        return false;
    }
}
