<?php
namespace phycom\frontend\models;


use phycom\common\models\traits\ModelTrait;

use yii\helpers\Url;
use yii\base\Model;
use yii;

/**
 * Terms form
 */
class TermsForm extends Model
{
	use ModelTrait;

	public $agreeTerms;

	protected $termsUrl;
	protected $policyUrl;

	public function init()
    {
        parent::init();
        $this->termsUrl = $this->findUrl('terms');
        $this->policyUrl = $this->findUrl('privacy-policy');
    }

    /**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['agreeTerms'], 'required', 'requiredValue' => 1, 'message' => Yii::t('public/main', 'You have to accept the terms.')]
		];
	}

	public function attributeLabels()
	{
		return [
			'agreeTerms' => Yii::t('public/main', 'I agree with {site} <a href="{terms_url}" target="_blank">Terms and conditions</a>', [
				'terms_url' => $this->termsUrl,
                'site' => Yii::$app->name
			])
		];
	}

	public function getTermsText()
	{
		return Yii::t('public/main', 'By proceeding you agree to {site} {terms}', [
			'site' => Yii::$app->name,
			'terms' => Yii::t('public/main', '<a href="{terms_url}" target="_blank">Terms and conditions</a> and <a href="{policy_url}" target="_blank">Privacy policy</a>', [
				'terms_url' => $this->termsUrl,
				'policy_url' => $this->policyUrl
			])
		]);
	}

    /**
     * It will first finds if a page exist with the given keyword, if not a given fallback route is used.
     * If fallback route is not defined it will resolve to /site/$key
     *
     * @param string $key
     * @param string|null $fallbackRoute
     * @return mixed|string
     */
	protected function findUrl($key, $fallbackRoute = null)
    {
        if ($page = Yii::$app->pages->getPage($key)) {
            return $page->url;
        }
        return Url::toRoute($fallbackRoute ?: ['/site/' . $key]);
    }
}