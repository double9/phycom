<?php

namespace phycom\frontend\models;

use phycom\common\models\User;
use phycom\common\models\AddressForm;

/**
 * Class UserAddressForm
 * @package phycom\backend\models
 *
 * @property User $model
 */
class UserAddressForm extends AddressForm
{
	protected $user;

	public function __construct(User $user, array $config = [])
	{
		$this->user = $user;
		parent::__construct($config);
	}

	public function getModel()
	{
		return $this->user;
	}
}