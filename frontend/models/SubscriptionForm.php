<?php

namespace phycom\frontend\models;


use phycom\frontend\models\behaviors\HoneypotBehavior;

use phycom\common\models\attributes\SubscriptionStatus;
use phycom\common\models\Subscription;
use phycom\common\models\traits\ModelTrait;

use yii\base\Model;
use yii;

/**
 * Class SubscriptionForm
 * @package phycom\frontend\models
 */
class SubscriptionForm extends Model
{
    use ModelTrait;

    public $email;

    public $name;

    public function behaviors()
    {
        return [
            'honeypot' => [
                'class'     => HoneypotBehavior::class,
                'attribute' => 'name'
            ]
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => Yii::t('public/main', 'Email'),
        ];
    }

    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['name', 'safe']
        ];
    }

    public function subscribe()
    {
        if ($this->validate()) {

            $subscription = new Subscription();
            $subscription->email = $this->email;
            $subscription->status = SubscriptionStatus::ACTIVE;

            if (!Yii::$app->user->isGuest) {
                $subscription->first_name = Yii::$app->user->identity->first_name;
                $subscription->last_name = Yii::$app->user->identity->last_name;
            }

            if ($subscription->save()) {
                return $subscription;
            }
            $this->setErrors($subscription->errors);
        }
        return false;
    }
}
