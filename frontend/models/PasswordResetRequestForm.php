<?php
namespace phycom\frontend\models;

use phycom\common\models\attributes\ContactAttributeStatus;
use phycom\common\models\attributes\MessageStatus;
use phycom\common\models\attributes\MessageType;
use phycom\common\models\attributes\UserTokenType;
use phycom\common\models\Email;
use phycom\common\models\Message;
use phycom\common\models\UserToken;
use phycom\frontend\models\behaviors\HoneypotBehavior;
use yii\base\Model;
use phycom\common\models\User;
use yii;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;

    public $name;

    public function behaviors()
    {
        return [
            'honeypot' => [
                'class'     => HoneypotBehavior::class,
                'attribute' => 'name'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => Email::class,
                'filter' => ['not', ['or',
	                'user_id IS NULL',
	                ['status' => ContactAttributeStatus::DELETED]
                ]],
                'message' => Yii::t('public/main','There is no user with this email address.')
            ],
            ['name', 'safe']
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        if (!$user = Yii::$app->modelFactory->getUser()::findByEmail($this->email)) {
        	return false;
        }
	    $token = $user->getToken(UserTokenType::PASSWORD_RESET);
        if (!$token || !$token->isValid) {
	        $token = $user->generatePasswordResetToken();
        }
        return $this->sendMessage($token);
    }

	protected function sendMessage(UserToken $token)
	{
		$tpl = Yii::$app->modelFactory->getMessageTemplate()::getTemplate('change_password');
		$msg = new Message();
		$msg->type = MessageType::EMAIL;
		$msg->status = MessageStatus::NEW;
		$msg->user_from = Yii::$app->systemUser->id;
		$msg->user_to = $token->user_id;
		$msg->priority = Message::PRIORITY_HIGH;

		$params = [
            'appname' => Yii::$app->name,
            'firstname' => $token->user->first_name,
            'link' => Yii::$app->urlManagerFrontend->createAbsoluteUrl(['/site/reset-password', 't' => $token->token])
        ];
		$msg->subject = $tpl->renderTitle($params);
		$msg->content = $tpl->render($params);

		if (!$msg->save()) {
			Yii::error('Error sending password reset email: ' . json_encode($msg->errors), __CLASS__);
			return false;
		}
		return $msg->queue();
	}
}
