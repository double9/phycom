<?php

namespace phycom\frontend\models;

use yii;

class LoginForm extends \phycom\common\models\LoginForm
{
	public function attributeLabels()
	{
		$labels = parent::attributeLabels();
		$labels['username'] = Yii::t('public/main', 'Email');
		return $labels;
	}
}