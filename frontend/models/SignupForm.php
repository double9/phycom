<?php

namespace phycom\frontend\models;

use phycom\common\models\traits\ModelTrait;
use phycom\common\models\attributes\ContactAttributeStatus;
use phycom\common\models\attributes\UserStatus;
use phycom\common\models\attributes\UserType;
use phycom\common\models\Email;
use phycom\common\models\Phone;
use phycom\common\models\User;
use phycom\common\validators\PhoneInputValidator;
use phycom\common\helpers\PhoneHelper;

use phycom\frontend\models\behaviors\HoneypotBehavior;
use yii\base\Model;
use yii\db\ActiveQueryInterface;
use yii;


/**
 * Signup form
 */
class SignupForm extends Model
{
	use ModelTrait;

    public $email;
    public $firstName;
    public $lastName;
    public $phoneNumber;
    public $phone;
    public $companyName;
    public $password;

    public $name;

    public function behaviors()
    {
        return [
            'honeypot' => [
                'class'     => HoneypotBehavior::class,
                'attribute' => 'name'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstName','lastName', 'companyName'], 'trim'],
	        [['firstName','lastName'], 'required'],
            [['firstName', 'lastName'], 'string', 'min' => 2, 'max' => 100],
            [['companyName'], 'string', 'min' => 2, 'max' => 200],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'min' => 6, 'max' => 254],
	        ['email', 'unique',
		        'targetClass' => Email::class,
		        'filter' => function ($query) {
			        /**
			         * @var yii\db\Query $query
			         */
			        return $query
				        ->andWhere('user_id IS NOT NULL')
				        ->andWhere(['not', ['status' => ContactAttributeStatus::DELETED]]);
	            },
		        'message' => 'This email address has already been taken.'
	        ],
            ['phoneNumber', 'required', 'when' => function(){
                    return strlen($this->phone);
                }
            ],
	        ['phone', 'trim'],
	        ['phone', PhoneInputValidator::class],
	        [
		        'phone',
		        'unique',
		        'targetAttribute' => 'phone_nr',
		        'targetClass' => Phone::class,
		        'message' => Yii::t('public/error', 'There is already an account registered with the entered phone number.'),
		        'filter' => function (ActiveQueryInterface $query) {
			        $query->where(['phone_nr' => \phycom\common\helpers\PhoneHelper::getNationalPhoneNumber($this->phoneNumber)]);
                    $query->andWhere(['not', ['user_id' => Yii::$app->user->id]]);
			        $query->andWhere(['not', ['status' => ContactAttributeStatus::DELETED]]);
		        },
	        ],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['name', 'safe']
        ];
    }


    public function attributeLabels()
    {
        return [
            'email'       => Yii::t('public/user', 'Email'),
            'firstName'   => Yii::t('public/user', 'First name'),
            'lastName'    => Yii::t('public/user', 'Last name'),
            'phone'       => Yii::t('public/user', 'Phone'),
            'companyName' => Yii::t('public/user', 'Company name'),
            'password'    => Yii::t('public/user', 'Password')
        ];
    }

	/**
     * Signs user up.
     *
     * @return User|false the saved model or false if saving fails
     * @throws \Exception
     */
    public function signup()
    {
        if (!$this->validate()) {
            return false;
        }
        $transaction = Yii::$app->db->beginTransaction();
        try {
	        $user = Yii::$app->modelFactory->getUser();
	        $user->first_name = $this->firstName;
	        $user->last_name = $this->lastName;
	        $user->type = UserType::CLIENT;
	        $user->status = UserStatus::PENDING_ACTIVATION;
	        $user->setPassword($this->password);
	        $user->generateAuthKey();

	        if ($this->companyName) {
	        	$user->company_name = $this->companyName;
	        }

			if (!$user->save()) {
				return $this->rollback($transaction, $user->errors);
			}

	        $email = new Email();
	        $email->user_id = $user->id;
	        $email->status = ContactAttributeStatus::UNVERIFIED;
	        $email->email = $this->email;

	        if (!$email->save()) {
		        return $this->rollback($transaction, $email->errors);
	        }

	        if ($this->phone) {
		        $phone = new Phone();
		        $phone->user_id = $user->id;
		        $phone->status = ContactAttributeStatus::UNVERIFIED;
		        $phone->phone_nr = PhoneHelper::getNationalPhoneNumber($this->phoneNumber);
		        $phone->country_code = PhoneHelper::getPhoneCode($this->phoneNumber);

		        if (!$phone->save()) {
			        return $this->rollback($transaction, $phone->errors);
		        }
	        }

	        $transaction->commit();
			return $user;

        } catch (\Exception $e) {
			$transaction->rollBack();
			throw $e;
        }
    }
}
