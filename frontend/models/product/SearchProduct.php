<?php

namespace phycom\frontend\models\product;


use phycom\common\components\ActiveQuery;
use phycom\common\helpers\Diacritics;
use phycom\common\helpers\Url;
use phycom\common\interfaces\SearchModelInterface;
use phycom\common\models\AttachmentUrl;
use phycom\common\models\attributes\CategoryStatus;
use phycom\common\models\attributes\ProductStatus;
use phycom\common\models\attributes\ReviewStatus;
use phycom\common\models\behaviors\CurrencyBehavior;
use phycom\common\models\product\ProductCategory;
use phycom\common\models\product\ProductReview;
use phycom\common\models\product\ProductStatistics;
use phycom\common\models\product\ProductTag;
use phycom\common\models\Review;
use phycom\common\models\traits\SearchQueryFilter;
use phycom\common\models\File;
use phycom\common\models\Language;
use phycom\common\models\product\Product;
use phycom\common\models\product\ProductAttachment;
use phycom\common\models\product\ProductCategoryProductRelation;
use phycom\common\models\product\ProductPrice;
use phycom\common\models\translation\ProductCategoryTranslation;
use phycom\common\models\translation\ProductTranslation;

use yii\data\Sort;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\db\Query;
use yii\db\Expression;
use yii;


/**
 * Class SearchProduct
 * @package phycom\frontend\models\product
 *
 * @property string $label
 * @property string $thumbUrl
 */
class SearchProduct extends Product implements SearchModelInterface
{
	use SearchQueryFilter;

	public $pagination;
	/**
	 * @var Language
	 */
	public $language;

    public $urlKey;
	public $title;
	public $description;

	public $fileName;
	public $fileBucket;

	public $unitPrice;
	public $unitPriceAttributes;

	public $score;
	public $reviewCount;

	public $updatedFrom;
	public $updatedTo;

	public $createdFrom;
	public $createdTo;

	public $categoryId;

	public $numTransactions;
	public $qtyOrdered;

	public $keyword;

    private $tags = [];
    private $tagsValidation = false;

	public function init()
	{
		parent::init();
		$this->language = Yii::$app->lang->current;
	}

	public function rules()
	{
		return [
			[['id', 'vendor_id', 'created_by', 'image_id', 'price_id', 'categoryId'], 'integer'],
			[['include_vat'], 'boolean'],
			[['title'], 'string'],
			[['created_at','updated_at','createdFrom','createdTo','updatedFrom','updatedTo','status'], 'safe'],
			[['sku', 'urlKey', 'keyword'], 'string', 'max' => 255],
            [['reviewCount'], 'integer'],
            [['tags'], 'each', 'rule' => ['string']],
			[['unitPrice', 'score'], 'number']
		];
	}

	public function setTags($value)
    {
        // this is for "each" validator as it internally sets tags value to string and then validates the value
        if ($this->tagsValidation) {
            $this->tags = $value;
            return;
        }

        if (empty($value)) {
            $this->tags = [];
            return;
        }
        if (is_array($value)) {
            $this->tags = $value;
            return;
        }
        if (is_string($value)) {
            $jsonData = json_decode($value, true);
            // JSON is valid and we have array of tags
            if (json_last_error() === JSON_ERROR_NONE && is_array($jsonData)) {
                $this->tags = $jsonData;
            } else {
                $this->tags = [$value];
            }
        }
    }

    public function getTags()
    {
        return $this->tags;
    }

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'currency' => [
				'class' => CurrencyBehavior::class,
				'attributes' => ['unitPrice']
			]
		]);
	}

	public function getLabel()
	{
		return $this->title;
	}

    /**
     * @return string
     */
    public function getUrl()
    {
        return Url::toFeRoute(['/product/view', 'key' => $this->urlKey]);
    }

    /**
     * @param string|null $metaKey
     * @param string|null $metaValue
     * @return AttachmentUrl|object
     * @throws yii\base\InvalidConfigException
     */
    public function getImageUrl($metaKey = null, $metaValue = null)
    {
        return Yii::createObject([
            'class'    => AttachmentUrl::class,
            'bucket'   => $this->fileBucket,
            'filename' => $this->fileName
        ]);
    }

	public function getThumbUrl()
	{
		return $this->getImageUrl('thumb');
	}

	public function getPrice($units = null)
    {
        if (null === $units && $this->unitPriceAttributes) {
            $params = Json::decode($this->unitPriceAttributes);
            $productPrice = Yii::$app->modelFactory->getProductPrice($params);
            $productPrice->afterFind();
            return Yii::$app->modelFactory->getProductPriceHelper($this, $units, ['prices' => [$productPrice]]);
        }
        return parent::getPrice($units);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param null $status
     * @return ProductDataProvider|yii\data\ActiveDataProvider
     * @throws yii\base\Exception
     */
	public function search(array $params = [], $status = null)
	{
		$query = $this->createSearchQuery();

		$config = [
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ];

		$pagination = $this->pagination();

		if (null !== $pagination) {
		    $config['pagination'] = $pagination;
        }

		$dataProvider = new ProductDataProvider($config);

		$this->sort($dataProvider->sort);
        $this->load($params);

		if (!$this->validate()) {
			return $dataProvider;
		}

        if ($this->keyword) {
            $this->filterByKeyword($query);
        }

		$query->andFilterWhere([
			'p.id' => $this->id,
			'p.sku' => $this->sku,
			'price.price' => $this->unitPrice,
		]);

        if (!empty($this->status)) {
            $query->andWhere(['p.status' => (string)$this->status]);
        } else if (!empty($status)) {
            $query->andWhere(['p.status' => $status]);
        }

		$query->filterMultiAttribute(['t1.title','t2.title'], $this->title);
		$query->filterDateRange('p.created_at', $this->createdFrom, $this->createdTo);
		$query->filterDateRange('p.updated_at', $this->updatedFrom, $this->updatedTo);

		return $dataProvider;
	}


	protected function sort(Sort $sort)
	{
		$sort->attributes['title'] = [
			'asc' => [new Expression('COALESCE(t1.title, t2.title) ASC')],
			'desc' => [new Expression('COALESCE(t1.title, t2.title) DESC')],
		];
		$sort->attributes['unitPrice'] = [
			'asc' => ['price.price' => SORT_ASC],
			'desc' => ['price.price' => SORT_DESC],
		];
        $sort->attributes['score'] = [
            'asc' => [new Expression('avg(r.score) ASC NULLS LAST')],
            'desc' => [new Expression('avg(r.score) DESC NULLS LAST')],
        ];
        $sort->attributes['created_at'] = [
            'asc' => ['p.created_at' => SORT_ASC],
            'desc' => ['p.created_at' => SORT_DESC]
        ];
        $sort->attributes['tag_timestamp'] = [
            'asc' => ['tags_filter.created_at' => SORT_ASC],
            'desc' => ['tags_filter.created_at' => SORT_DESC]
        ];
        $sort->attributes['numTransactions'] = [
            'asc' => ['ps.num_transactions' => SORT_ASC],
            'desc' => ['ps.num_transactions' => SORT_DESC]
        ];
        $sort->attributes['qtyOrdered'] = [
            'asc' => ['ps.total_quantity_ordered' => SORT_ASC],
            'desc' => ['ps.total_quantity_ordered' => SORT_DESC]
        ];
	}


	protected function pagination()
    {
        return $this->pagination;
    }

    /**
     * @return ActiveQuery
     * @throws yii\base\Exception
     */
	protected function createSearchQuery()
	{
		$query = static::find();
		$query->select([
			'p.*',
			'CASE WHEN (t1.title IS NOT NULL) THEN t1.title ELSE t2.title END AS title',
			'CASE WHEN (t1.title IS NOT NULL) THEN t1.description ELSE t2.description END AS description',
            'CASE WHEN (t1.url_key IS NOT NULL) THEN t1.url_key ELSE t2.url_key END AS "urlKey"',
			'price.price AS "unitPrice"',
            new Expression('row_to_json(price.*) AS "unitPriceAttributes"'),
			'file.filename AS "fileName"',
			'file.bucket AS "fileBucket"',
            'avg(r.score) AS score',
            'count(r.id) AS "reviewCount"',
            'CASE WHEN (ps.id IS NOT NULL) THEN ps.num_transactions ELSE 0 END AS "numTransactions"',
            'CASE WHEN (ps.id IS NOT NULL) THEN ps.total_quantity_ordered ELSE 0 END AS "qtyOrdered"',
            'all_tags.data AS tags',
		]);
		$query->from(['p' => Product::tableName()]);
		$query->where(['not', ['p.status' => [ProductStatus::DELETED, ProductStatus::DRAFT, ProductStatus::HIDDEN]]]);

		$query->leftJoin(['t1' => ProductTranslation::tableName()], 't1.product_id = p.id AND t1.language = :lang');
		$query->leftJoin(['t2' => ProductTranslation::tableName()], [
            'and',
            't2.product_id = p.id',
            't2.language = :fallback_lang',
            't2.language != :lang',
            't2.title IS NOT NULL',
            't2.url_key IS NOT NULL'
        ]);

		$query->addParams([
			'lang' => $this->language->code,
			'fallback_lang' => Yii::$app->lang->source->code
		]);

		if ($this->categoryId) {
		    $this->filterByCategory($query);
		}

        if (!empty($this->tags) && $this->validate('tags')) {
            $this->filterByTags($query);
        }

        $query->leftJoin(
            ['all_tags' => 'LATERAL(' .
                (new Query())
                ->select(new Expression('jsonb_agg(pt.value) as data'))
                ->from(['pt' => ProductTag::tableName()])
                ->where('pt.product_id = p.id')
                ->createCommand()->rawSql . ')'],
            'true'
        );

		$query->leftJoin(
			['price' => 'LATERAL(' .
				(new Query())
				->select('pp.*')
				->from(['pp' => ProductPrice::tableName()])
				->where('pp.product_id = p.id')
				->orderBy(['pp.num_units' => SORT_ASC])
				->limit(1)
				->createCommand()
				->sql . ')'],
			'true'
		);

		$query->leftJoin(
			['file' => 'LATERAL(' .
				(new Query())
					->select('f.*')
					->from(['pa' => ProductAttachment::tableName()])
					->innerJoin(['f' => File::tableName()], 'f.id = pa.file_id')
					->where('pa.product_id = p.id')
					->orderBy(['pa.order' => SORT_ASC])
					->limit(1)
					->createCommand()
					->sql . ')'],
			'true'
		);

		$query->leftJoin(['ps' => ProductStatistics::tableName()], 'ps.product_id = p.id');
		$query->leftJoin(['pr' => ProductReview::tableName()], 'pr.product_id = p.id');
		$query->leftJoin(['r' => Review::tableName()], [
		    'and',
		    'pr.review_id = r.id',
            ['r.status' => ReviewStatus::APPROVED]
        ]);
		$query->addGroupBy([
		    'p.id',
            't1.title',
            't1.description',
            't1.url_key',
            't2.title',
            't2.description',
            't2.url_key',
            'ps.id',
            'price.price',
            'price.*',
            'file.filename',
            'file.bucket',
            'all_tags.data'
        ]);

		return $query;
	}

    /**
     * @param ActiveQuery $query
     */
	protected function filterByKeyword(ActiveQuery $query)
    {
        $keyword = trim(Diacritics::remove($this->keyword));

        $categoriesQuery = (new Query())
            ->select('distinct(ppc.product_id)')
            ->from(['pct' => ProductCategoryTranslation::tableName()])
            ->where('(UNACCENT(pct.title) ILIKE :search_value)')
            ->innerJoin(['pc' => ProductCategory::tableName()], [
                'and',
                'pc.id = pct.product_category_id',
                ['not', ['pc.status' => [CategoryStatus::DELETED]]]
            ])
            ->innerJoin(['ppc' => ProductCategoryProductRelation::tableName()], 'ppc.category_id = pc.id');

        $query->andWhere([
            'OR',
            '(UNACCENT(t1.title) ILIKE :search_value)',
            '(UNACCENT(t1.description) ILIKE :search_value)',
            'p.sku = :search_value',
            ['p.id' => $categoriesQuery]
        ], ['search_value' => '%' . $keyword . '%']);
    }

    /**
     * @param ActiveQuery $query
     */
	protected function filterByCategory(ActiveQuery $query)
    {
        $subQuery = new Expression(
            'WITH RECURSIVE categories AS (' .
            'SELECT c1.* FROM product_category c1 WHERE c1.id = :category_id ' .
            'UNION ALL ' .
            'SELECT c2.* FROM product_category c2 INNER JOIN categories t0 ON t0.id = c2.parent_id ' .
            ')' .
            'SELECT c.id FROM categories c WHERE c.status = :category_status ORDER BY c.parent_id NULLS FIRST, c.order ASC'
        );

        $query->innerJoin(
            ['category' => 'LATERAL(' .
                (new Query())
                    ->select('pc.*')
                    ->from(['pc' => ProductCategoryProductRelation::tableName()])
                    ->where('pc.product_id = p.id')
                    ->andWhere('pc.category_id in (' . $subQuery . ')')
                    ->limit(1)
                    ->createCommand()
                    ->sql . ')'],
            'true',
            [
                'category_id' => $this->categoryId,
                'category_status' => CategoryStatus::VISIBLE
            ]
        );
    }

    /**
     * @param ActiveQuery $query
     */
    protected function filterByTags(ActiveQuery $query)
    {
        $q = (new Query())
            ->select('ptf.*')
            ->from(['ptf' => ProductTag::tableName()])
            ->where('ptf.product_id = p.id');

            $params = [];
            // construct params to bind with the query
            foreach ($this->tags as $key => $tag ) {
                $params['tag' . $key] = $tag;
            }
            $q->andWhere('ptf.value in (:' . implode(', :',  array_keys($params)) . ')')->limit(1); // generate placeholder for every tag value

        $query->innerJoin(['tags_filter' => 'LATERAL(' . $q->createCommand()->rawSql . ')'], 'true', $params);
        $query->addGroupBy('tags_filter.created_at');
    }

    /**
     * Added support for tags validation with "each" validator
     *
     * @param null $attributeNames
     * @param bool $clearErrors
     * @return bool
     */
    public function validate($attributeNames = null, $clearErrors = true)
    {
        if (null === $attributeNames || $attributeNames === 'tags' || (is_array($attributeNames) && in_array('tags', $attributeNames))) {
            $this->tagsValidation = true;
        }
        $result = parent::validate($attributeNames, $clearErrors);
        $this->tagsValidation = false;
        return $result;
    }
}
