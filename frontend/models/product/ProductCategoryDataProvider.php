<?php

namespace phycom\frontend\models\product;

use yii\data\ActiveDataProvider;

/**
 * Class ProductCategoryDataProvider
 * @package phycom\frontend\models\product
 *
 * @property SearchProductCategory[] $models
 * @method SearchProductCategory[] getModels()
 */
class ProductCategoryDataProvider extends ActiveDataProvider
{

}