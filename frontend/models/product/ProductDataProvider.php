<?php

namespace phycom\frontend\models\product;

use yii\data\ActiveDataProvider;

/**
 * Class ProductDataProvider
 * @package phycom\frontend\models\product
 *
 * @property SearchProduct[] $models
 * @method SearchProduct[] getModels()
 */
class ProductDataProvider extends ActiveDataProvider
{

}