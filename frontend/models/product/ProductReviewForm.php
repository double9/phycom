<?php

namespace phycom\frontend\models\product;


use phycom\common\models\attributes\ReviewStatus;
use phycom\common\models\product\Product;
use phycom\common\models\product\ProductReview;
use phycom\common\models\Review;
use phycom\common\models\traits\ModelTrait;

use yii\helpers\HtmlPurifier;
use yii\base\Model;
use yii;

/**
 * Class ProductReviewForm
 * @package phycom\frontend\models\product
 */
class ProductReviewForm extends Model
{
    use ModelTrait;

    public $score;
    public $title;
    public $description;

    /**
     * @var Product
     */
    protected $product;

    public function __construct(Product $product, array $config = [])
    {
        $this->product = $product;
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            ['score', 'integer', 'min' => 1, 'max' => 5],
            ['title', 'string', 'max' => 255],
            ['description', 'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'score'       => Yii::t('public/review', 'Score'),
            'title'       => Yii::t('public/review', 'Title'),
            'description' => Yii::t('public/review', 'Description'),
        ];
    }

    public function beforeValidate()
    {
        if (Yii::$app->user->isGuest) {
            $this->addError('error', Yii::t('common/product', 'You must be logged in to add review'));
            return false;
        }
        return parent::beforeValidate();
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function create()
    {
        if ($this->validate()) {

            if (Yii::$app->user->isGuest) {
                Yii::error('Guest user attempt to create a product ' . $this->product->id . ' review', __METHOD__);
                return false;
            }

            $transaction = Yii::$app->db->beginTransaction();
            try {
                $review = new Review();
                $review->created_by = Yii::$app->user->id;
                $review->score = $this->score;
                $review->title = strip_tags($this->title);
                $review->description = HtmlPurifier::process($this->description);
                $review->verified_purchase = $this->checkVerifiedPurchase();
                $review->status = ReviewStatus::PENDING;

                if (!$review->save()) {
                    return $this->rollback($transaction, $review->errors);
                }

                $productReview = new ProductReview();
                $productReview->product_id = $this->product->id;
                $productReview->review_id = $review->id;

                if (!$productReview->save()) {
                    return $this->rollback($transaction, $productReview->errors);
                }
                $productReview->populateRelation('review', $review);

                Yii::$app->user->activity('Created a review ' . $review->id . ' for product ' . $this->product->id);
                $transaction->commit();
                return $productReview;

            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public function checkVerifiedPurchase()
    {
        return false;
    }
}