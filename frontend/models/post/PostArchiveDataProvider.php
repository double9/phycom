<?php

namespace phycom\frontend\models\post;

use yii\data\ActiveDataProvider;

/**
 * Class ArchiveDataProvider
 * @package phycom\frontend\models\post
 *
 * @property SearchPostArchive[] $models
 * @method SearchPostArchive[] getModels()
 */
class PostArchiveDataProvider extends ActiveDataProvider
{

}