<?php

namespace phycom\frontend\models\post;

use yii\data\ActiveDataProvider;

/**
 * Class PostCategoryDataProvider
 * @package phycom\frontend\models\post
 *
 * @property SearchPostCategory[] $models
 * @method SearchPostCategory[] getModels()
 */
class PostCategoryDataProvider extends ActiveDataProvider
{

}