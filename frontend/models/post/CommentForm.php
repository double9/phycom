<?php

namespace phycom\frontend\models\post;

use phycom\common\models\attributes\CommentStatus;
use phycom\common\models\Comment;
use phycom\common\models\Post;
use phycom\common\models\PostComment;
use phycom\common\models\traits\ModelTrait;

use phycom\common\models\User;
use yii\base\Model;
use yii;

/**
 * Class CommentForm
 * @package phycom\frontend\models\post
 *
 * @property-write User $author
 */
class CommentForm extends Model
{
    use ModelTrait;

    public $name;
    public $email;
    public $comment;
    public $parent;

    protected $post;
    protected $author;

    public function __construct(Post $post, array $config = [])
    {
        $this->post = $post;
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['parent'], 'integer'],
            [['name','email','comment'], 'trim'],
            [['comment'], 'required'],
            [['name','email'], 'required', 'when' => function ($model) {
                    return Yii::$app->user->isGuest;
                }
            ],
            [['name', 'email'], 'string', 'max' => 255],
            [['comment'], 'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('public/main', 'Name'),
            'email' => Yii::t('public/main', 'Email'),
            'comment' => Yii::t('public/main', 'Comment'),
        ];
    }

    public function getPost()
    {
        return $this->post;
    }

    public function setAuthor(User $user)
    {
        $this->author = $user;
    }

    public function save()
    {
        if ($this->validate()) {
            $transaction = Yii::$app->db->beginTransaction();
            try {

                $comment = new Comment();
                $comment->author_name = $this->name;
                $comment->author_email = $this->email;
                $comment->content = $this->comment;
                $comment->author_ip = Yii::$app->request->getUserIP();
                $comment->author_agent = Yii::$app->request->getUserAgent();
                $comment->status = CommentStatus::PENDING;

                if ($this->author) {
                    $comment->created_by = $this->author->id;
                    if (!$this->name) {
                        $comment->author_name = $this->author->fullName;
                    }
                    if (!$this->email) {
                        $comment->author_email = (string) $this->author->email;
                    }
                }

                if ($this->parent) {
                    $comment->parent_id = (int) $this->parent;
                }

                if (!$comment->save()) {
                    return $this->rollback($transaction, $comment->errors);
                }

                $postComment = new PostComment();
                $postComment->comment_id = $comment->id;
                $postComment->post_id = $this->post->id;

                if (!$postComment->save()) {
                    return $this->rollback($transaction, $postComment->errors);
                }

                $transaction->commit();
                return $comment;

            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
        return false;
    }
}