<?php

namespace phycom\frontend\models\post;


use phycom\common\components\ActiveQuery;
use phycom\common\interfaces\SearchModelInterface;
use phycom\common\models\Language;
use phycom\common\models\PostCategory;
use phycom\common\models\traits\SearchQueryFilter;
use phycom\common\models\attributes\PostStatus;
use phycom\common\models\attributes\CategoryStatus;
use phycom\common\models\traits\TreeBuilder;
use phycom\common\models\translation\PostCategoryTranslation;

use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\Expression;
use yii;

/**
 * Class SearchPostCategory
 * @package phycom\frontend\models\post
 *
 * @property string $label
 * @property SearchPost[] $activePosts
 */
class SearchPostCategory extends PostCategory implements SearchModelInterface
{
	use SearchQueryFilter;
	use TreeBuilder;

	/**
	 * @var Language
	 */
	public $language;

	public $title;
	public $urlKey;

	public $description;

	public $updatedFrom;
	public $updatedTo;

	public $createdFrom;
	public $createdTo;

	public $topLevel = false;
	public $path;

    public function init()
	{
		parent::init();
		$this->language = Yii::$app->lang->current;
	}

	public function rules()
	{
		return [
			[['id', 'shop_id', 'created_by', 'parent_id'], 'integer'],
			[['title'], 'string'],
            [['topLevel', 'featured'], 'boolean', 'trueValue' => true, 'falseValue' => false, 'strict' => true],
			[['created_at','updated_at','createdFrom','createdTo','updatedFrom','updatedTo', 'status'], 'safe'],
            [['urlKey'], 'string', 'max' => 255],
		];
	}

	public function getLabel()
	{
		return $this->title;
	}

	public function getRoute()
    {
        return ['post/index', 'category' => $this->urlKey, 'url-language' => Yii::$app->language];
    }

	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
		$dataProvider = new PostCategoryDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['order' => SORT_ASC]],
			'pagination' => false,
		]);

		$this->sort($dataProvider->sort);

		if ($this->load($params) && !$this->validate()) {
		    return $dataProvider;
        }
	
		$query->andFilterWhere([
			'c.id' => $this->id,
			'c.status' => (string)$this->status,
            'c.featured' => $this->featured
		]);

		$query->filterMultiAttribute(['t1.title','t2.title'], $this->title);
		$query->filterDateRange('c.created_at', $this->createdFrom, $this->createdTo);
		$query->filterDateRange('c.updated_at', $this->updatedFrom, $this->updatedTo);

		return $dataProvider;
	}


	public function searchMenuItems($setParentUrl = true)
    {
        $dataProvider = $this->search([]);
        $items = $this->buildTree($dataProvider->getModels(), null, $setParentUrl);
        return $items;
    }


	public function getActivePosts()
	{
		$searchModel = Yii::$app->modelFactory->getSearchPost();
		$searchModel->categoryId = $this->id;
		$dataProvider = $searchModel->search(['status' => PostStatus::PUBLISHED]);
		return $dataProvider->getModels();
	}

	protected function sort(Sort $sort)
	{
		$sort->attributes['title'] = [
			'asc' => [new Expression('COALESCE(t1.title, t2.title) ASC')],
			'desc' => [new Expression('COALESCE(t1.title, t2.title) DESC')],
		];
	}

	/**
	 * @return ActiveQuery
     * @throws yii\base\Exception
	 */
	protected function createSearchQuery()
	{
		$query = static::find();
        $query->select([
            'c.*',
            'CASE WHEN (t1.title IS NOT NULL) THEN t1.title ELSE t2.title END AS "title"',
            'CASE WHEN (t1.description IS NOT NULL) THEN t1.description ELSE t2.description END AS "description"',
            'CASE WHEN (t1.url_key IS NOT NULL) THEN t1.url_key ELSE t2.url_key END AS "urlKey"',
//            'CASE WHEN (t1.url_key IS NOT NULL) THEN t1.language ELSE t2.language END AS "urlLanguage"',
        ]);
		$query->from(['c' => PostCategory::tableName()]);
		$query->where(['c.status' => CategoryStatus::VISIBLE]);

		$query->leftJoin(['t1' => PostCategoryTranslation::tableName()], 't1.post_category_id = c.id AND t1.language = :lang');
		$query->leftJoin(['t2' => PostCategoryTranslation::tableName()], [
		    'or',
            // fallback to default language
            [
                'and',
                't2.post_category_id = c.id',
                't2.language = :fallback_lang',
                't2.language != :lang',
                't2.title IS NOT NULL',
                't2.url_key IS NOT NULL'
            ],
            // or if there is no record with default language then any other language with content
            [
                'and',
                't2.post_category_id = c.id',
                't2.language != :lang',
                't2.title IS NOT NULL',
                't2.url_key IS NOT NULL'
            ]
        ]);
		$query->addParams([
			'lang' => $this->language->code,
			'fallback_lang' => Yii::$app->lang->source->code
		]);

		if ($this->topLevel) {
		    $query->andWhere('c.parent_id IS NULL');
        }

		return $query;
	}
}