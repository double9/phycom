<?php

namespace phycom\frontend\models\post;

use phycom\common\helpers\Diacritics;
use phycom\common\helpers\Url;
use phycom\common\models\AttachmentUrl;
use phycom\common\models\attributes\CategoryStatus;
use phycom\common\models\attributes\CommentStatus;
use phycom\common\models\attributes\PostStatus;
use phycom\common\components\ActiveQuery;
use phycom\common\models\attributes\PostType;
use phycom\common\models\Comment;
use phycom\common\models\PostCategory;
use phycom\common\models\PostCategoryPostRelation;
use phycom\common\models\PostComment;
use phycom\common\models\product\ProductComment;
use phycom\common\models\traits\SearchQueryFilter;
use phycom\common\interfaces\SearchModelInterface;
use phycom\common\models\File;
use phycom\common\models\Language;
use phycom\common\models\Post;
use phycom\common\models\PostAttachment;
use phycom\common\models\translation\PostCategoryTranslation;
use phycom\common\models\translation\PostTranslation;

use yii\data\Sort;
use yii\db\Query;
use yii\db\Expression;
use yii;

/**
 * Class SearchPost
 * @package phycom\frontend\models\post
 *
 * @property-read string $url
 */
class SearchPost extends Post implements SearchModelInterface
{
	use SearchQueryFilter;

	/**
	 * @var Language
	 */
	public $language;

	public $urlKey;

	public $title;
	public $content;

	public $keyword;

    public $categoryId;

    public $commentCount;

	public $fileName;
	public $fileBucket;

    public $publishedFrom;
    public $publishedTo;

	public $updatedFrom;
	public $updatedTo;

	public $createdFrom;
	public $createdTo;


	public static function findUrlByKey($key)
    {
        $model = static::findByKey($key);
        return $model ? $model->url : null;
    }

    /**
     * @param string $key
     * @return static|null
     * @throws yii\base\Exception
     */
    public static function findByKey($key)
    {
        $searchModel = new static;
        $searchModel->identifier = $key;
        $provider = $searchModel->search();
        return $provider->models[0] ?? null;
    }


	public function init()
    {
        $this->language = Yii::$app->lang->current;
    }


    public function rules()
	{
		return [
			[['id', 'vendor_id', 'created_by', 'categoryId', 'commentCount'], 'integer'],
			[['title'], 'string'],
			[['created_at','updated_at','publishedFrom','publishedTo','createdFrom','createdTo','updatedFrom','updatedTo', 'status'], 'safe'],
			[['identifier', 'urlKey', 'keyword'], 'string', 'max' => 255],
		];
	}


    /**
     * @return string
     */
	public function getUrl()
    {
        if ($this->type->value === PostType::PAGE) {
            return Url::toFeRoute(['/'. $this->urlKey]);
        }
        return Url::toFeRoute(['/' . $this->type . '/view', 'key' => $this->urlKey]);
    }

    /**
     * @param string|null $metaKey
     * @param string|null $metaValue
     * @return AttachmentUrl|object
     * @throws yii\base\InvalidConfigException
     */
	public function getImageUrl($metaKey = null, $metaValue = null)
	{
        return Yii::createObject([
            'class'    => AttachmentUrl::class,
            'bucket'   => $this->fileBucket,
            'filename' => $this->fileName,
        ]);
	}


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return PostDataProvider
     * @throws yii\base\Exception
     */
	public function search(array $params = [])
	{
		$query = $this->createSearchQuery();
		$dataProvider = new PostDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['published_at' => SORT_DESC]],
			'pagination' => $this->pagination(),
		]);

		$this->sort($dataProvider->sort);

		if (!$this->validate()) {
			return $dataProvider;
		}

        if ($this->keyword) {
            $this->filterByKeyword($query);
        }

        $query->andFilterWhere([
			'p.id' => $this->id,
			'p.identifier' => $this->identifier,
			'p.status' => (string) $this->status,
		]);

		$query->filterMultiAttribute(['t1.title','t2.title'], $this->title);
		$query->filterDateRange('p.created_at', $this->createdFrom, $this->createdTo);
		$query->filterDateRange('p.updated_at', $this->updatedFrom, $this->updatedTo);
        $query->filterDateRange('p.published_at', $this->publishedFrom, $this->publishedTo);

		return $dataProvider;
	}

    protected function pagination()
    {
        return ['pageSize' => 20];
    }

	protected function sort(Sort $sort)
	{
		$sort->attributes['title'] = [
			'asc' => [new Expression('COALESCE(t1.title, t2.title) ASC')],
			'desc' => [new Expression('COALESCE(t1.title, t2.title) DESC')],
		];
		$sort->attributes['published_at'] = [
            'asc' => [new Expression('p.published_at ASC NULLS LAST')],
            'desc' => [new Expression('p.published_at DESC NULLS LAST')],
        ];
	}

    /**
     * @return ActiveQuery
     * @throws yii\base\Exception
     */
	protected function createSearchQuery()
	{
		$query = static::find();
		$query->select([
			'p.*',
			'CASE WHEN (t1.title IS NOT NULL) THEN t1.title ELSE t2.title END AS title',
            'CASE WHEN (t1.content IS NOT NULL) THEN t1.content ELSE t2.content END AS content',
            'CASE WHEN (t1.url_key IS NOT NULL) THEN t1.url_key ELSE t2.url_key END AS "urlKey"',
			'file.filename AS "fileName"',
			'file.bucket AS "fileBucket"',
            'count(com.id) AS "commentCount"'
		]);
		$query->from(['p' => Post::tableName()]);
		$query->where(['not', ['p.status' => PostStatus::DELETED]]);

		$query->leftJoin(['t1' => PostTranslation::tableName()], 't1.post_id = p.id AND t1.language = :lang');
		$query->leftJoin(['t2' => PostTranslation::tableName()], 't2.post_id = p.id AND t2.language = :fallback_lang AND t2.language != :lang');
		$query->addParams([
			'lang' => $this->language->code,
			'fallback_lang' => Yii::$app->lang->source->code
		]);

        if ($this->categoryId) {
            $this->filterByCategory($query);
        }

		$query->leftJoin(
			['file' => 'LATERAL(' .
				(new Query())
					->select('f.*')
					->from(['pa' => PostAttachment::tableName()])
					->innerJoin(['f' => File::tableName()], 'f.id = pa.file_id')
					->where('pa.post_id = p.id')
					->orderBy(['pa.order' => SORT_ASC])
					->limit(1)
					->createCommand()
					->sql . ')'],
			'true'
		);

        $query->leftJoin(['p_com' => PostComment::tableName()], 'p_com.post_id = p.id');
        $query->leftJoin(['com' => Comment::tableName()], [
            'and',
            'p_com.comment_id = com.id',
            ['com.status' => [CommentStatus::APPROVED, CommentStatus::PENDING]]
        ]);
        $query->groupBy([
            'p.id',
            't1.title',
            't1.content',
            't1.url_key',
            't2.title',
            't2.content',
            't2.url_key',
            'file.filename',
            'file.bucket'
        ]);


		if ($this->type) {
		    $query->andWhere(['p.type' => $this->type]);
        }
        if ($this->identifier) {
            $query->andWhere(['p.identifier' => $this->identifier]);
            $query->limit(1);
        }

		return $query;
	}


    /**
     * @param ActiveQuery $query
     */
    protected function filterByKeyword(ActiveQuery $query)
    {
        $keyword = trim(Diacritics::remove($this->keyword));

        $categoriesQuery = (new Query())
            ->select('distinct(ppc.post_id)')
            ->from(['pct' => PostCategoryTranslation::tableName()])
            ->where('(UNACCENT(pct.title) ILIKE :search_value)')
            ->innerJoin(['pc' => PostCategory::tableName()], [
                'and',
                'pc.id = pct.post_category_id',
                ['not', ['pc.status' => [CategoryStatus::DELETED]]]
            ])
            ->innerJoin(['ppc' => PostCategoryPostRelation::tableName()], 'ppc.category_id = pc.id');

        $query->andWhere([
            'OR',
            '(UNACCENT(t1.title) ILIKE :search_value)',
            '(UNACCENT(t1.content) ILIKE :search_value)',
            ['p.id' => $categoriesQuery]
        ], ['search_value' => '%' . $keyword . '%']);
    }


    /**
     * @param ActiveQuery $query
     */
    protected function filterByCategory(ActiveQuery $query)
    {
        $subQuery = new Expression(
            'WITH RECURSIVE categories AS (' .
            'SELECT c1.* FROM post_category c1 WHERE c1.id = :category_id ' .
            'UNION ALL ' .
            'SELECT c2.* FROM post_category c2 INNER JOIN categories t0 ON t0.id = c2.parent_id ' .
            ')' .
            'SELECT c.id FROM categories c WHERE c.status = :category_status ORDER BY c.parent_id NULLS FIRST, c.order ASC'
        );

        $query->innerJoin(
            ['category' => 'LATERAL(' .
                (new Query())
                    ->select('pc.*')
                    ->from(['pc' => PostCategoryPostRelation::tableName()])
                    ->where('pc.post_id = p.id')
                    ->andWhere('pc.category_id in (' . $subQuery . ')')
                    ->limit(1)
                    ->createCommand()
                    ->sql . ')'],
            'true',
            [
                'category_id' => $this->categoryId,
                'category_status' => CategoryStatus::VISIBLE
            ]
        );
    }
}
