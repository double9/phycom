<?php

namespace phycom\frontend\models\post;

use yii\data\ActiveDataProvider;

/**
 * Class PostDataProvider
 * @package phycom\frontend\models\post
 *
 * @property SearchPost[] $models
 * @method SearchPost[] getModels()
 */
class PostDataProvider extends ActiveDataProvider
{

}