<?php

namespace phycom\frontend\models\post;

use phycom\common\models\Post;
use phycom\common\models\PostAttachment;

use yii\data\ActiveDataProvider;

/**
 * Class ArchiveDataProvider
 * @package phycom\frontend\models\post
 *
 * @property PostAttachment[] $models
 * @method PostAttachment[] getModels()
 */
class PostAttachmentDataProvider extends ActiveDataProvider
{
    public static function create(Post $post)
    {
        return new static([
            'query'      => $post->getAttachments(),
            'sort'       => ['defaultOrder' => ['order' => SORT_ASC]],
            'pagination' => false
        ]);
    }
}