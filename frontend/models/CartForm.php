<?php

namespace phycom\frontend\models;

use phycom\common\models\File;
use phycom\common\models\OrderItem;
use phycom\frontend\models\product\ProductSelection;

use phycom\common\components\FileStorage;

use yii;

/**
 * Class CartForm
 * @package phycom\frontend\models
 */
class CartForm extends ProductSelection
{
    /**
     * @return bool|\phycom\common\models\OrderItem
     * @throws yii\base\Exception
     */
    public function createOrderItem()
    {
        if (!$this->validate()) {
            return false;
        }

        $orderItem = Yii::$app->modelFactory->getOrderItem();
        $orderItem->code = $this->product->sku;
        $orderItem->product_id = $this->product->id;
        $orderItem->quantity = $this->quantity;

        $prices = $this->product->getPrice($this->units)->getTotalPrice($this->getSelectedOptionKeys());
        $orderItem->price = $prices[0];

        foreach ($this->getOptions() as $variantName => $value) {
            if (is_null($value)) {
                continue;
            }
            if (!$this->addOptionToOrderItem($orderItem, $variantName, $value)) {
                return false;
            }
        }
        return $orderItem;
    }

    /**
     * @param OrderItem $orderItem
     * @param string $variantName
     * @param mixed $value
     * @return bool
     * @throws yii\base\Exception
     * @throws yii\base\InvalidConfigException
     */
    protected function addOptionToOrderItem(OrderItem $orderItem, string $variantName, $value)
    {
        if ($value instanceof File) {
            $filename = time() . '_' . Yii::$app->security->generateRandomString(32) . '.' . pathinfo($value->filename, PATHINFO_EXTENSION);
            $bucket = Yii::$app->fileStorage->getBucket(FileStorage::BUCKET_TEMP);
            $content = Yii::$app->fileStorage->getBucket($value->bucket)->getFileContent($value->filename);
            if (!$bucket->saveFileContent($filename, $content)) {
                $this->addError($variantName, Yii::t('public/main', 'Error creating temp file'));
                return false;
            }
            $orderItem->setOption($variantName, $filename);
        } elseif ($value instanceof yii\web\UploadedFile) {
            $filename = time() . '_' . Yii::$app->security->generateRandomString(32) . '.' . $value->extension;
            $bucket = Yii::$app->fileStorage->getBucket(FileStorage::BUCKET_TEMP);
            if (!$bucket->copyFileIn($value->tempName, $filename)) {
                $this->addError($variantName, Yii::t('public/main', 'Error saving uploaded file'));
                return false;
            }
            $orderItem->setOption($variantName, $filename);
        } else {
            $orderItem->setOption($variantName, $value);
        }
        return true;
    }
}
