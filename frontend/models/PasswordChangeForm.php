<?php
namespace phycom\frontend\models;


use phycom\common\models\User;

use yii\base\Model;
use yii;

/**
 * Password update form for a regular password update
 *
 * Class PasswordUpdateForm
 * @package phycom\frontend\models
 */
class PasswordChangeForm extends Model
{
    public $password;
    public $newPassword;
    public $repeatPassword;

    /**
     * @var \phycom\common\models\User
     */
    private $user;

    /**
     * @param User $user
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidArgumentException if token is empty or not valid
     */
    public function __construct(User $user, $config = [])
    {
        $this->user = $user;
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password', 'newPassword', 'repeatPassword'], 'required'],
            [['password', 'newPassword', 'repeatPassword'], 'string', 'min' => 6],
            ['repeatPassword', 'compare', 'compareAttribute' => 'newPassword', 'message' => Yii::t('public/main', "Passwords don't match")],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password'       => Yii::t('public/main', 'Password'),
            'newPassword'    => Yii::t('public/main', 'New Password'),
            'repeatPassword' => Yii::t('public/main', 'Repeat Password'),
        ];
    }

    public function afterValidate()
    {
        if (!$this->user->validatePassword($this->password)) {
            $this->addError('password', Yii::t('public/main', 'Wrong password'));
        }
        parent::afterValidate();
    }

    /**
     * Updates password.
     *
     * @return bool if password was reset.
     */
    public function update()
    {
        if ($this->validate()) {
            $user = $this->user;
            $user->setPassword($this->newPassword);
            return $user->save(false);
        }
        return false;
    }
}
