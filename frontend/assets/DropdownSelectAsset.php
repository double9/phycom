<?php

namespace phycom\frontend\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Dropdown se asset bundle.
 */
class DropdownSelectAsset extends AssetBundle
{
    public $sourcePath = '@phycom/frontend/assets/dropdown-select';
    public $css = [];
    public $js = [
        'dropdown.js',
    ];
    public $publishOptions = ['except' => ['*.less', '*.scss']];
    public $depends = [
        JqueryAsset::class
    ];
}