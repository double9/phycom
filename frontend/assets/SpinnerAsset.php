<?php

namespace phycom\frontend\assets;

use yii\web\JqueryAsset;
use yii\web\AssetBundle;

/**
 * Class SpinnerAsset
 *
 * @package phycom\frontend\assets
 */
class SpinnerAsset extends AssetBundle
{
	public $sourcePath = '@phycom/frontend/assets/spinner';
	public $css = [
        'spinner.css',
	];
	public $js = [
        'spinner.js'
	];
	public $publishOptions = [
        'except' => [
        	'*.less',
            '*.scss'
        ]
	];
	public $depends = [
	    JqueryAsset::class
    ];
}
