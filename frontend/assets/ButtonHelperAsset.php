<?php

namespace phycom\frontend\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class ButtonHelperAsset
 * @package phycom\frontend\assets
 */
class ButtonHelperAsset extends AssetBundle
{
	public $sourcePath = '@phycom/frontend/assets/button-helper';
	public $js = [
	    'button.js'
	];
	public $depends = [
        JqueryAsset::class
	];
}