var promotion = (function ($) {

    var instance = null;

    var Promotion = function(form) {
        this.$form = $(form);
        if (!this.$form.length) {
            throw 'Form ' + form + ' was not found';
        }
        this.form = this.$form[0];
        this.$input = this.$form.find('input[type="text"]').first();
        this.init();
    };

    Promotion.prototype.init = function () {
        var self = this;
        this.$form.on('click', 'button', function(e) {
            e.preventDefault();
            self.applyPromotion();
        });
        // disable enter key submit
        this.$form.on('keypress', ':input', function (e) {
            if (e.keyCode === 13) {
                e.preventDefault();
                self.applyPromotion();
                return false;
            };
        });

        this.$form.on()
    };

    Promotion.prototype.submitRequest = function () {
        var self = this,
            formData = this.$form.serialize();

        return $.ajax({
            url: this.$form.attr('action'),
            type: 'POST',
            data: formData
        }).then(function (data, textStatus, jqXHR) {
            if (typeof data.error !== 'undefined') {
                // self.showError(data.error);
                return new $.Deferred().reject(jqXHR, data.error).promise();
            }
            return data;
        }).fail(function (xhr, statusText) {
            // console.log(xhr);
            // var err = xhr.status + ' ' + xhr.statusText + '<br /><span><pre>' + JSON.stringify(xhr.responseJSON, null, 2) + '</pre></span>';
            self.showError(statusText);
            // return xhr;
        });
    };

    Promotion.prototype.showError = function (err) {
        console.log(err);
        ActiveFormHelper.addError(this.$form, 'promotioncodeform-code', err);
        ButtonHelper.reset($('button', this.form));
    };

    Promotion.prototype.applyPromotion = function () {

        ButtonHelper.loading($('button', this.form));

        ActiveFormHelper.validate(this.$form)
            .then($.proxy(this.submitRequest, this))
            .done($.proxy(this.updateCart, this));
    };

    Promotion.prototype.updateCart = function (data) {
        cart.addOrUpdateJs(data);
        ButtonHelper.reset($('button', this.form));
        this.hide();
    };

    Promotion.prototype.show = function () {
        this.$form.length && this.$form.show(200);
    };

    Promotion.prototype.hide = function () {
        this.$form.length && this.$form.hide();
    };

    return {
        init: function (el) {
            return instance = new Promotion(el);
        },
        hide: function () {
            instance && instance.hide();
        },
        show: function () {
            instance && instance.show();
        }
    };

})(jQuery);