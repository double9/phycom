var cart = (function ($) {

    var instance = null;

    const TYPE_PRODUCT = 'product';
    const TYPE_DELIVERY = 'delivery';
    const TYPE_DISCOUNT = 'discount';
    const TYPE_OTHER = 'other';

    var Cart = function(el, currencyCode, callback) {
        this.currencyCode = currencyCode;
        this.items = [];
        this.el = el;
        this.$el = $(el);
        if (!this.$el.length) {
            throw 'Element ' + el + ' was not found';
        }
        this.updateUrl = this.$el.data('update-url');
        this.init(callback);
    };

    Cart.prototype.init = function (callback) {
        this.$container = $('> tbody', this.$el[0]);
        var items = [];
        var self = this;
        this.$container.children().each(function(index, el) {
            items.push($(el).data());
        });
        this.items = items;
        this.$el.on('click', 'button.remove-row', function (e) {
            e.preventDefault();
            var $el = $(this),
                btn = $el.button('loading'),
                id = $el.closest('tr').data('id');

            self.removeById(id, function () {
                btn.button('reset');
            });
        });

        this.$el.on('focusout', 'input.qty', function (e) {
            var qty = parseInt($(this).val());
            if (!isNaN(qty)) {
                self.changeQuantity($(this).closest('tr').data('id'), qty);
            }
        });

        callback();
    };


    Cart.prototype.destroy = function () {
        this.$el.off('click', 'button.remove-row');
        this.$el.off('focusout','input.qty');
    };

    Cart.prototype.getTotalProductCount = function () {
        var count = 0;
        for (var i=0; i<this.items.length; i++) {
            if (this.items[i].type === TYPE_PRODUCT) {
                count++;
            }
        }
        return count;
    };

    Cart.prototype.getTotal = function () {
        var total = 0;
        for (var i=0; i<this.items.length; i++) {
            total += (this.items[i].price * this.items[i].quantity);
        }
        return total;
    };

    Cart.prototype.addOrUpdateJs = function (data, labels, removeBtn) {
        var html = this.renderLine(data, labels, removeBtn),
            item = this.findItemById(data.id);

        if (item) {
            item.type = data.type;
            item.code = data.code;
            item.price = data.price;
            item.quantity = data.quantity || 1;
            this.$container.find('tr[data-id="'+data.id+'"]').replaceWith(html);
        } else {
            this.items.push(data);
            this.$container.append(html);
        }
        this.updateTotal();
    };

    Cart.prototype.removeByCode = function (code) {
        var self = this,
            promises = [];
        for (var i=0; i<this.items.length; i++) {
            if (this.items[i].code === code) {
                this.items.splice(i, 1);
            }
        }
        this.$container.find('tr[data-code="'+code+'"]').each(function (index, el) {
            var $item = $(el);
            promises.push(self.removeItemFromSession($item.data('id')));
            $item.remove();
            self.update();
        });
        return $.when.apply(null, promises);
    };

    Cart.prototype.removeByType = function (type) {
        var self = this,
            promises = [];
        for (var i=0; i<this.items.length; i++) {
            if (this.items[i].type === type) {
                this.items.splice(i, 1);
            }
        }
        this.$container.find('tr[data-type="'+type+'"]').each(function (index, el) {
            var $item = $(el);
            promises.push(self.removeItemFromSession($item.data('id')));
            $item.remove();
            self.update();
        });
        return $.when.apply(null, promises);
    };

    Cart.prototype.removeById = function (id) {
        var self = this,
            type = null,
            key = this.findItemKeyById(id);

        if (key > -1) {
            type = this.items[key].type;
            this.items.splice(key, 1);
        }

        this.$container.find('tr[data-id="'+id+'"]').remove();
        this.update();
        return this.removeItemFromSession(id).then(function () {
            if (type === TYPE_DISCOUNT) {
                self.showPromotionForm();
            }
        });
    };

    Cart.prototype.showPromotionForm = function () {
        if (typeof promotion != 'undefined') {
            promotion.show();
        }
    };

    Cart.prototype.removeItemFromSession = function(id) {
        var self = this;
        return $.post(this.$el.data('remove-url') + '?id=' + id)
            .then($.proxy(this.updateDynamicLines, this))
            .then(function () {
                if (!self.getTotalProductCount()) {
                    window.location.reload();
                }
            });
    };

    Cart.prototype.changeQuantity = function (id, qty, callback) {
        callback = callback || function(){};
        for (var i=0; i<this.items.length; i++) {
            if (this.items[i].id === id) {

                $.post(this.updateUrl + '?id=' + id + '&qty=' + qty)
                    .then($.proxy(this.updateDynamicLines, this))
                    .then(callback);

                this.items[i].quantity = qty;

                var totalPrice = this.items[i].price * this.items[i].quantity;
                this.$container.find('tr[data-id="'+id+'"] td.price').html(CurrencySymbol.createPriceLabel(totalPrice, this.currencyCode));
                this.update();
                break;
            }
        }
    };

    Cart.prototype.renderLine = function (d, p, removeBtn) {
        p = p || {};
        removeBtn = removeBtn || false;

        return '<tr data-id="' + d.id + '" data-type="' + d.type + '" data-code="' + d.code + '" data-price="' + d.price + '" data-quantity="' + (d.quantity || 1) + '">' +
            '<td class="hidden-xs">'+ d.code + '</td>' +
            '<td></td>' +
            '<td class="hidden-xs" colspan="2"><strong>' + (p.label || d.label || '')  + '</strong></td>' +
            '<td class="visible-xs"><strong>' + (p.label || d.label || '')  + '</strong></td>' +
            '<td>' + (d.type === TYPE_PRODUCT ? (d.quantity || 1) : '') + '</td>' +
            '<td class="price">' + (p.price || CurrencySymbol.createPriceLabel(d.price, this.currencyCode)) + '</td>' +
            '<td class="clearfix">' + (removeBtn ? this.renderRemoveBtn() : '') + '</td>' +
            '</tr>';
    };

    Cart.prototype.renderRemoveBtn = function () {
        return '<button class="remove-row btn btn-danger btn-xs btn-flat pull-right"><i class="mdi mdi-remove mdi-lg"></i></button>';
    };


    Cart.prototype.findItemKeyById = function (id) {
        for (var i=0; i<this.items.length; i++) {
            if (this.items[i].id === id) {
                return i;
            }
        }
        return -1;
    };

    Cart.prototype.findItemById = function (id) {
        var key = this.findItemKeyById(id);
        return key > -1 ? this.items[key] : null;
    };

    Cart.prototype.updateDynamicLines = function () {
        var promises = [];
        for (var i=0; i<this.items.length; i++) {
            if (this.items[i].type === TYPE_DISCOUNT) {
                promises.push(this.updateLine(this.items[i].id));
            }
        }
        return $.when.apply(null, promises);
    };

    Cart.prototype.updateLine = function (id) {
        var self = this,
            i = this.findItemKeyById(id);

        if (i > -1) {
            this.$container.find('tr[data-id="'+id+'"]').addClass('loading');
            return $.ajax({
                url: this.updateUrl,
                type: 'GET',
                data: {id:id}
            }).done(function (res) {
                if (res.error) {
                    return new $.Deferred().reject(res.error).promise();
                }
                self.$container.find('tr[data-id="'+id+'"]').removeClass('loading');
                self.addOrUpdateJs(res, null, res.type !== TYPE_DELIVERY);
                self.items[i].price = res.price;
                self.update();

                return res;
            }).fail(function (xhr) {
                console.log(xhr);
                var err = xhr.status + ' ' + xhr.statusText + '<br /><span><pre>' + JSON.stringify(xhr.responseJSON, null, 2) + '</pre></span>';
                return xhr;
            });
        }
    };

    Cart.prototype.update = function () {
        // clear the cart if no products left
        if (!this.getTotalProductCount()) {
            this.items = [];
            this.$container.find('tr').each(function (index, el) {
                $(el).remove();
            });
        }
        this.updateTotal();
        this.updateTotalCount();
    };

    Cart.prototype.updateTotalCount = function () {
        var $el = $('#cart-menu .item-count');
        var totalCount = this.getTotalProductCount();
        $el.text(totalCount);
        if (totalCount === 0) {
            $el.fadeOut(200, function() {
                $('#cart-menu').addClass('empty');
            });
        }
        return this;
    };

    Cart.prototype.updateTotal = function () {
        this.$el.find('.grand-total').html(CurrencySymbol.createPriceLabel(this.getTotal(), this.currencyCode));
        return this;
    };

    /**
     * Public accessor methods
     */
    return {
        getPrototype: function () {
            return Cart.prototype;
        },
        init: function (el, currencyCode, callback) {
            currencyCode = currencyCode || 'EUR';
            callback = callback || function(){};
            this.destroy();
            return instance = new Cart(el, currencyCode, callback);
        },
        destroy: function() {
            if (instance && instance instanceof Cart) {
                instance.destroy();
            }
        },
        addOrUpdateJs: function (data, labels) {
            labels = labels || {};
            if (instance && instance instanceof Cart) {
                var removeBtn = data.type !== TYPE_DELIVERY;

                data.price = Number(data.price);
                data.quantity = Number(data.quantity || 1);

                instance.addOrUpdateJs(data, labels, removeBtn);
            }
        },
        removeByCode: function (code, callback) {
            callback = callback || function(){};
            instance.removeByCode(code).then(callback);
        },
        removeByType: function (code, callback) {
            callback = callback || function(){};
            instance.removeByType(code).then(callback);
        },
        removeById: function (id, callback) {
            callback = callback || function(){};
            instance.removeById(id).then(callback);
        },
        /**
         * @returns {int}
         */
        productCount: function() {
            if (instance && instance instanceof Cart) {
                return instance.getTotalProductCount();
            }
            return 0;
        }
    };
})(jQuery);
