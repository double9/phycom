var order = (function ($) {

    var instance = null;

    var Order = function(el) {
        this.el = el;
        this.$el = $(el);
        if (!this.$el.length) {
            throw 'Element ' + el + ' was not found';
        }
        this.cart = window.cart.init(this.el + ' .cart-table');
        this.shippingBox = window.shippingBox || null;
        this.loadingMessage = this.$el.data('loading-msg');
        this.submitUrl = this.$el.data('submit-url');
        this.$msgContainer = $('.system-messages', this.el);
        this.$submitBtn = $('.submit-order', this.el);
        this.$form = $('#order-form');
        this.$termsForm = $('#terms-form');

        this.init();
    };

    Order.prototype.init = function () {
        var self = this;
        this.$submitBtn.on('click', function (e) {
            e.preventDefault();
            self.submitOrder().done(function (data) {
                console.log(data);
                if (typeof data.url !== 'undefined')
                    window.location = data.url;
                else
                    self.hideLoader();
            });
        });
    };

    Order.prototype.destroy = function () {
        instance = null;
    };

    /**
     * @param $form
     * @returns {*}
     */
    Order.prototype.validateForm = function ($form) {
        var self = this,
            deferred = $.Deferred();

        // validate form but not submit
        $form.data('yiiActiveForm').submitting = true;
        $form.yiiActiveForm('validate');
        $form.one('submit', function (e) {
            e.preventDefault();
            return false;
        });

        $form.one('afterValidate', function (e, messages, errorAttributes) {
            for (var attribute in messages) {
                if (messages.hasOwnProperty(attribute) && messages[attribute].length) {
                    self.showError(messages[attribute][0]);
                    return deferred.reject(messages[attribute][0]);
                }
            }
            return deferred.resolve();
        });
        return deferred.promise();
    };

    Order.prototype.submitRequest = function () {
        var self = this,
            formData = this.$form.add(this.shippingBox.getForm()).serialize();

        this.showLoader();
        return $.ajax({
            url: this.submitUrl,
            type: 'POST',
            data: formData
        }).done(function (data) {

            var msg = data.msg || false;
            if (msg) {
                self.showMessage(data.msg, 'success');
            }
            if (data.error) {
                self.showError(data.error);
                return new $.Deferred().reject(data.error).promise();
            }
            return data;
        }).fail(function (xhr) {
            console.log(xhr);
            var err = xhr.status + ' ' + xhr.statusText + '<br /><span><pre>' + JSON.stringify(xhr.responseJSON, null, 2) + '</pre></span>';
            self.showError(err);
            return xhr;
            //return new $.Deferred().reject(xhr).promise();
        });
    };

    Order.prototype.submitOrder = function () {
        this.clearErrors();
        var promises = [
            this.validateForm(this.shippingBox.getForm()),
            this.validateForm(this.$form),
        ];
        if (this.$termsForm.length) {
            promises.push(this.validateForm(this.$termsForm));
        }
        return $.when.apply(null, promises).then($.proxy(this.submitRequest, this));
    };

    Order.prototype.showMessage = function(msg, type) {
        if (msg.length) {
            type = type || 'info';
            var alert = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                '<span class="msg">' + msg + '</span></div>';
            this.$msgContainer.append(alert);
        }
    };

    Order.prototype.showError = function(errorMsg) {
        var alert = '<div class="alert alert-danger alert-dismissible" role="alert">' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
            '<strong>' + (typeof i18n !== 'undefined' ? i18n.error : 'Error') + ': </strong><span class="error-msg">' + errorMsg + '</span></div>';

        var $err = this.$msgContainer.find('.alert.alert-danger');
        if ($err.length) {
            $err.append('<span class="error-msg">' + errorMsg + '</span>');
            if (!$err.hasClass('gr')) {
                $err.addClass('gr');
            }
        } else {
            this.$msgContainer.append(alert);
        }
        this.hideLoader();
    };

    Order.prototype.clearErrors = function() {
        this.$msgContainer.html('');
    };


    Order.prototype.showLoader = function () {
        if (typeof window.spinner === 'undefined') {
            console.log('Spinner not found');
        } else {
            spinner.show(this.loadingMessage);
        }
        if (this.$submitBtn.length) {
            ButtonHelper.loading(this.$submitBtn);
        }
    };

    Order.prototype.hideLoader = function () {
        if (typeof window.spinner === 'undefined') {
            console.log('Spinner not found');
        } else {
            spinner.hide();
        }
        if (this.$submitBtn.length) {
            ButtonHelper.reset(this.$submitBtn);
        }
    };

    /**
     * Public accessor methods
     */
    return {
        init: function (el) {
            this.destroy();
            return instance = new Order(el);
        },
        destroy: function () {
            if (instance && instance instanceof Order) {
                instance.destroy();
            }
        },
        submit: function () {
            return instance.submitOrder();
        }
    };

})(jQuery);