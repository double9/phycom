<?php

namespace phycom\frontend\assets;

use yii\web\AssetBundle;

/**
 * Jquery Cookie asset bundle.
 */
class JavascriptCookieAsset extends AssetBundle
{
    public $sourcePath = null;
    public $js = [
        'https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js',
    ];
}

