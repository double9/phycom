<?php

namespace phycom\frontend\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class ProductPriceAsset
 * @package phycom\frontend\assets
 */
class ProductPriceAsset extends AssetBundle
{
    public $sourcePath = '@phycom/frontend/assets/product-price';
    public $js = [
        'main.js',
    ];
    public $depends = [
        JqueryAsset::class,
        CurrencySymbolAsset::class
    ];
}
