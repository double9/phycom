<?php

namespace phycom\frontend\assets;

use yii\web\AssetBundle;

/**
 * Select 2 asset bundle.
 */
class Select2WidgetAsset extends AssetBundle
{
    public $sourcePath = '@phycom/frontend/assets/select2';
    public $css = [
        'select2.css'
    ];
    public $js = [];
    public $depends = [
        Select2Asset::class
    ];
}