<?php

namespace phycom\frontend\assets;

use yii\bootstrap\BootstrapAsset;

/**
 * Class Select2BootstrapAsset
 * @package phycom\frontend\assets
 */
class Select2BootstrapAsset extends \yii\web\AssetBundle
{
    // The files are not web directory accessible, therefore we need
    // to specify the sourcePath property. Notice the @conquer alias used.
    public $sourcePath = '@phycom/frontend/assets/select2';

    public $css = [
        'select2-bootstrap.min.css',
    ];

    public $depends = [
        BootstrapAsset::class,
        Select2Asset::class
    ];
}