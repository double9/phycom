
var ButtonHelper = (function ($) {

    const DEFAULT_LOADING_TXT = '<i class="fas fa-spinner fa-spin"></i>'; //'loading...';

    return {
        loading: function ($btn) {

            var loadingText = $btn.data('loading-text') || DEFAULT_LOADING_TXT;

            if ($btn.html() !== loadingText) {;
                $btn.data('original-text', $btn.html());
                $btn.css('min-width', $btn.outerWidth() + 'px');
                $btn.html(loadingText);
                $btn.prop('disabled', true);
            }
        },
        reset: function ($btn) {

            var loadingText = $btn.data('loading-text') || DEFAULT_LOADING_TXT,
                originalText = $btn.data('original-text') || '';

            $btn.css('min-width', '');

            if ($btn.html() === loadingText) {
                $btn.html(originalText);
                $btn.prop('disabled', false);
            }
        }
    };
})(jQuery);
