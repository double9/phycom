<?php

namespace phycom\frontend\assets;

use yii\web\AssetBundle;

/**
 * Currency symbol asset bundle.
 */
class CurrencySymbolAsset extends AssetBundle
{
	public $sourcePath = '@phycom/frontend/assets/currency-symbol';
	public $css = [];
	public $js = [
	    'main.js'
	];
	public $publishOptions = ['except' => ['*.less']];

}
