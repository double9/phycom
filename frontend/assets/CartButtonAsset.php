<?php

namespace phycom\frontend\assets;

use phycom\common\assets\ActiveFormHelperAsset;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class CartButtonAsset
 * @package phycom\frontend\assets
 */
class CartButtonAsset extends AssetBundle
{
    public $sourcePath = '@phycom/frontend/assets/cart-button';
    public $js = [
        'price-calculator.js',
        'button.js'
    ];
    public $depends = [
        JqueryAsset::class,
        ButtonHelperAsset::class,
        CurrencySymbolAsset::class,
        ActiveFormHelperAsset::class
    ];
}
