
var CartButton = (function($) {
    'use strict';

    var CartButtonWidget = function (config) {

        this.$cartBadge = $('#cart-widget');
        this.$el = $(config.form);
        if (!this.$el.length) {
            throw new Error('Element ' + config.form + ' not found');
        }
        this.el = this.$el[0];

        this.$prompt = false;
        if (config.prompt || false) {
            this.$prompt = $(config.prompt);
            if (!this.$prompt.length) {
                throw new Error('Element ' + config.prompt + ' not found');
            }
        }
        this.beforeSubmit = config.beforeSubmit || false;
        this.unitPricing = config.unitPricing || false;

        if (config.priceTarget) {
            this.$priceTarget = $(config.priceTarget);
            if (!this.$priceTarget.length) {
                throw new Error('Product price target ' + config.priceTarget + ' not found');
            }
            /**
             * @type {ProductPrice}
             */
            this.productPrice = this.$priceTarget.data('productPrice');
        } else {
            this.productPrice = null;
        }


        this.init();
    };

    CartButtonWidget.prototype.init = function () {
        var self = this;
        this.$el.on('click', '.cart-btn:not(disabled)', function (e) {
            e.preventDefault();
            var $btn = $(this);

            ButtonHelper.loading($btn);

            if (self.beforeSubmit) {
                self.beforeSubmit($btn)
                    .then(function($btn) {
                        return self.submit($btn);
                    })
                    .fail(function(err) {
                        ButtonHelper.reset($btn);
                        console.log(err);
                    });
            } else {
                self.submit($btn);
            }
            return false;
        });

        if (this.productPrice) {
            this.initPricing();
        }
    };

    CartButtonWidget.prototype.initPricing = function () {
        var self = this;
        if (!this.unitPricing) {

            let inputs = this.$el.find('input, select').toArray();
            this.prevPriceCalculator = this.productPrice.prevPrice ? new PriceCalculator(this.productPrice.prevPrice, inputs) : null;
            this.priceCalculator = new PriceCalculator(this.productPrice.price, inputs);

            this.$el.on('change', 'input, select', async function (e) {
                let price = await self.priceCalculator.update();
                if (self.prevPriceCalculator) {
                    let prevPrice = await self.prevPriceCalculator.update();
                    self.productPrice.update(price, prevPrice);
                } else {
                    self.productPrice.update(price);
                }
            });

        } else {

            this.$el.on('change', 'input, select', function (e) {
                self.productPrice.requestPrice(new FormData(self.el));
            });
        }
    };

    CartButtonWidget.prototype.setPrice = function (price, itemClassName) {
        let $priceContainer = $(this.priceTarget);
        if ($priceContainer.length) {
            let $item = $priceContainer.find(itemClassName);
            $item.attr('data-value', price);
            $item.text(CurrencySymbol.createPriceLabel(price, this.currencyCode));
        }
    };


    CartButtonWidget.prototype.submit = function ($btn) {
        var self = this;
        return $.post({
            url: this.$el.attr('action'),
            data: new FormData(this.el),
            // dataType: 'json',
            contentType: false,
            processData: false,
            success: function () {
                self.onSuccess($btn);
            }
        });
    };

    CartButtonWidget.prototype.onSuccess = function ($btn) {
        var cContainer = this.$cartBadge.find('.item-count'),
            count = parseInt(cContainer.text()) || 0;

        count++;
        cContainer.text(count);
        if (cContainer.is(':hidden')) {
            cContainer.fadeIn(200);
            this.$cartBadge.removeClass('empty');
        }
        this.showCheckoutPrompt();
        ButtonHelper.reset($btn);
    };

    CartButtonWidget.prototype.destroy = function () {
        this.$el.off('click');
        this.$el.off('change');
    };

    CartButtonWidget.prototype.showCheckoutPrompt = function () {
        if (this.$prompt) {
            $('.modal.show').not(this.$prompt).each(function (index, el) {
                $(el).modal('hide');
            });
            this.$prompt.modal({show: true});
        }
    };

    return {
        init: function (config) {
            return new CartButtonWidget(config);
        }
    };

})(jQuery);
