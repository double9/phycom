
var PriceCalculator = (function($) {
    'use strict';

    const QUANTITY_FIELD_NAME = 'CartForm[quantity]';

    let PriceCalculatorWidget = function (unitPrice, fields, afterUpdate) {
        this.unitPrice = unitPrice;
        this.quantityField = null;
        this.fields = [];

        fields = fields || [];
        fields.forEach((field) => {
            this.addField(field);
        });

        this.afterUpdate = afterUpdate || function(){};
    };

    PriceCalculatorWidget.prototype.addField = function (field) {
        let $field = $(field);
        if (!$field.length) {
            throw new Error('Field ' + JSON.stringify(field) + ' was not found');
        }

        let name = $field.attr('name');

        if (name === QUANTITY_FIELD_NAME) {
            this.quantityField = $field;
        } else {
            this.fields.push($field);
        }
    };

    PriceCalculatorWidget.prototype.update = async function () {

        let price = this.unitPrice;
        this.fields.forEach(($field) => {
            let element = $field[0].nodeName.toLowerCase();
            if (element === 'select') {
                let $selected = $field.find(':selected');
                if ($selected.length) {
                    let priceValue = $selected.attr('data-price');
                    if (priceValue && !isNaN(priceValue)) {
                        price += Number(priceValue);
                    }
                }
            } else {
                let type = $field.attr('type');
                if (type !== 'hidden' && (type !== 'radio' || $field.is(':checked'))) {
                    let priceValue = $field.attr('data-price');
                    if (priceValue && !isNaN(priceValue)) {
                        price += Number(priceValue);
                    }
                }
            }
        });

        if (this.quantityField) {
            price = Number(this.quantityField.val()) * price;
        }

        if (this.afterUpdate) {
            this.afterUpdate(price);
        }
        return price;
    };

    return PriceCalculatorWidget;

})(jQuery);
