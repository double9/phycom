<?php

namespace phycom\frontend\assets;


/**
 * Class Select2MaximizeAsset
 * @package phycom\frontend\assets
 */
class Select2MaximizeAsset extends \yii\web\AssetBundle
{
    // The files are not web directory accessible, therefore we need
    // to specify the sourcePath property. Notice the @conquer alias used.
    public $sourcePath = '@phycom/frontend/assets/select2';

    public $js=[
        'maximize-select2-height.min.js',
    ];

    public $depends= [
        Select2Asset::class
    ];
}