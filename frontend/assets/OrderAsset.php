<?php

namespace phycom\frontend\assets;

use phycom\common\assets\ActiveFormHelperAsset;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Order and cart page asset bundle.
 */
class OrderAsset extends AssetBundle
{
	public $sourcePath = '@phycom/frontend/assets/order';
	public $css = [];
	public $js = [
        'cart.js',
		'order.js',
        'promotion.js'
	];
	public $publishOptions = ['except' => ['*.less']];
	public $depends = [
        JqueryAsset::class,
        ButtonHelperAsset::class,
        ActiveFormHelperAsset::class,
        CurrencySymbolAsset::class
	];
}
