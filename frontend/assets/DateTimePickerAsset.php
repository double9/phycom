<?php

namespace phycom\frontend\assets;

use digitv\bootstrap\assets\BootstrapPluginAsset;

use yii\web\AssetBundle;

/**
 * DateTimePicker asset bundle.
 */
class DateTimePickerAsset extends AssetBundle
{
    public $sourcePath = '@vendor/npm-asset/pc-bootstrap4-datetimepicker/build';
    public $css = [
        'css/bootstrap-datetimepicker.min.css',
    ];
    public $js = [
        'js/bootstrap-datetimepicker.min.js'
    ];
    public $depends = [
        BootstrapPluginAsset::class,
        MomentAsset::class
    ];
}