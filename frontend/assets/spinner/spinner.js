var spinner = (function ($) {

    var createSpinner = function (text, el) {
        text = text || false;
        el = el || 'body';

        var html = (el === 'body')
            ? '<div class="spinner page-loading"><div class="spinner-center fixed">'
            : '<div class="spinner block-loading"><div class="spinner-center">';

        html += '<svg class="circular-spinner" width="42px" height="42px" viewBox="0 0 40 40" xmlns="http://www.w3.org/2000/svg">' +
                    '<circle class="path" fill="none" stroke-width="4" stroke-linecap="round" cx="20" cy="20" r="16"></circle>' +
                '</svg>';

                if (text) {
                    html += '<span class="loading-text">' + text + '</span>';
                }

        html += '</div></div>';

        $(el).addClass('loading');

        if (el === 'body') {
            if ($('body .wrap').length) {
                $('body .wrap').append(html);
            } else if ($('#main-content').length) {
                $('#main-content').append(html);
            } else {
                $('body').append(html);
            }
            $('.page-loading').show();
        } else {
            $(el).append(html);
            $('.block-loading').show();
        }
    };

    var hideSpinner = function (el) {
        el = el || 'body';

        if (el === 'body') {
            $(el).removeClass('loading').find('.page-loading').remove();
        } else {
            $(el).removeClass('loading').find('.block-loading').remove();
        }
    };


    /**
     * Public accessor methods
     */
    return {
        show: function (text, el) {
            createSpinner(text, el);
        },
        hide: function (el) {
            hideSpinner(el);
        }
    };

})(jQuery);
