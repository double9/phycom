var DropdownSelect = (function($) {
    'use strict';

    var DropdownSelectInput = function (input, btn) {

        this.$input = $(input);
        if (!this.$input.length) {
            throw 'Dropdown select input ' + input + ' not found';
        }
        this.input = this.$input[0];


        this.$btn = $(btn);
        if (!this.$btn.length) {
            throw 'Dropdown select button ' + btn + ' not found';
        }
        this.btn = this.$btn[0];

        this.init();
    };


    DropdownSelectInput.prototype.init = function () {

    };


    return {
        init: function (input, btn) {
            return new DropdownSelectInput(input, btn);
        }
    };

})(jQuery);