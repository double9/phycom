<?php

namespace phycom\frontend\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\web\View;

/**
 * Tree Menu asset bundle.
 */
class TreeMenuAsset extends AssetBundle
{
    public $sourcePath = '@phycom/frontend/assets/tree-menu';
    public $css = [];
    public $js = [
        'main.js'
    ];
    public $publishOptions = [
        'except' => ['*.less', '*.scss']
    ];
    public $depends = [
        JqueryAsset::class
    ];
}