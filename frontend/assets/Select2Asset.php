<?php

namespace phycom\frontend\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Select 2 asset bundle.
 */
class Select2Asset extends AssetBundle
{
    public $languageCode;
    public $sourcePath = '@npm/select2';
    public $css = [
        'dist/css/select2.min.css'
    ];
    public $js = [
        'dist/js/select2.full.min.js',
    ];
    public $depends = [
        JqueryAsset::class
    ];

    public function init()
    {
        parent::init();
        if ($this->languageCode) {
            $this->js[] = 'dist/js/i18n/' . $this->languageCode . '.js';
        }
    }
}
