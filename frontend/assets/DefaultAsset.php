<?php

namespace phycom\frontend\assets;

use yii\web\AssetBundle;

/**
 * Default frontend application asset bundle.
 */
class DefaultAsset extends AssetBundle
{
	public $sourcePath = '@phycom/frontend/assets/default';
	public $css = [
		'public.css',
		'default.css'
	];
	public $js = [
	];
	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset',
	];
}
