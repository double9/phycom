<?php

namespace phycom\frontend\assets;

use yii\web\AssetBundle;

/**
 * Select 2 asset bundle.
 */
class Select2Bootstrap4Asset extends AssetBundle
{
    public $sourcePath = '@npm/select2-bootstrap4-theme';
    public $css = [
        'dist/select2-bootstrap4.min.css'
    ];

    public $depends = [
        Select2Asset::class
    ];
}