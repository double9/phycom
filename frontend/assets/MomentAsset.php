<?php

namespace phycom\frontend\assets;

use yii\web\AssetBundle;

/**
 * moment.js asset bundle.
 */
class MomentAsset extends AssetBundle
{
    public $sourcePath = '@bower/moment/min';
    public $js = [
        'moment-with-locales.min.js'
    ];
}