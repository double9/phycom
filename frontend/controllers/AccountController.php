<?php

namespace phycom\frontend\controllers;


use phycom\common\helpers\FlashMsg;
use phycom\frontend\models\PasswordChangeForm;

use yii\filters\AccessControl;
use yii;

/**
 * Class AccountController
 * @package phycom\frontend\controllers
 */
class AccountController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['change-password'],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ]
        ];
    }

    public function actionChangePassword()
    {
        $model = new PasswordChangeForm(Yii::$app->user->identity);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->update()) {
                FlashMsg::success(Yii::t('public/main', 'Your password was successfully changed.'));
            } else {
                FlashMsg::error($model->errors);
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }
}