<?php

namespace phycom\frontend\controllers;

use phycom\common\models\traits\WebControllerTrait;
use phycom\common\models\Country;

use yii\web\Controller;
use yii;

/**
 * This is the base controller class that every frontend controller should extend.
 *
 * Class BaseController
 * @package phycom\frontend\controllers
 */
class BaseController extends Controller
{
	use WebControllerTrait;

    public function beforeAction($action)
    {
//    	if (Yii::$app->user->identity && Yii::$app->language !== Yii::$app->user->identity->settings->language) {
//    		Yii::$app->language = Yii::$app->user->identity->settings->language;
//	    }
//
//        if ($lang = Yii::$app->request->getQueryParam('lang')) {
//            if ($language = Yii::$app->lang->get($lang)) {
//                $cookie = new yii\web\Cookie();
//                $cookie->name = 'language';
//                $cookie->value = $language->code;
//                $cookie->expire = time() + 31556926;
//                $cookie->httpOnly = true;
//                Yii::$app->response->cookies->add($cookie);
//                Yii::$app->language = $language->code;
//            }
//        } else if ($lang = Yii::$app->request->cookies->get('language')) {
//            if ($language = Yii::$app->lang->get($lang->value)) {
//                Yii::$app->language = $language->code;
//            }
//        }

	    if (!Yii::$app->session->get('country')) {
            $country = Country::findOne(['code' => Yii::$app->geoip->ip()->isoCode]);
            if ($country && in_array($country, Yii::$app->country->countries)) {
                Yii::$app->country->setCountry($country->code);
                Yii::$app->session->set('country', $country->code);
            }
        }
	    return parent::beforeAction($action);
    }

//	public function afterAction($action, $result)
//	{
//		$result = parent::afterAction($action, $result);
//		$routesNotRecorded = ['site/status', 'file/download'];
//		$routesRecorded = ['user/upload-avatar'];
//
//		if (!Yii::$app->user->isGuest && (!Yii::$app->request->isAjax || in_array($action->controller->route, $routesRecorded)) && !in_array($action->controller->route, $routesNotRecorded)) {
//			UserActivity::logEffectiveUserUrl();
//		}
//		return $result;
//	}


}
