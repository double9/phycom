<?php

namespace phycom\frontend\controllers;

use phycom\common\helpers\Currency;
use phycom\frontend\models\product\ProductSelection;

use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use Yii;

/**
 * Class ProductController
 *
 * @package phycom\frontend\controllers
 */
class ProductController extends BaseController
{
    /**
     * @var string|ProductSelection
     */
    protected $productSelectionForm = ProductSelection::class;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?','@']
                    ]
                ],
            ]
        ];
    }

    /**
     * @param integer $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionGetPrice($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (!$product = Yii::$app->modelFactory->getProduct()::findOne($id)) {
            throw new NotFoundHttpException('The requested product ' . $id . ' does not exist.');
        }
        $ProductSelectionForm = $this->productSelectionForm;
        $model = new $ProductSelectionForm($product);
        $model->load(Yii::$app->request->post(), 'CartForm');

        return array_map(
            function($price) {
                return Currency::toDecimal($price);
            },
            $model->getPrice()
        );
    }
}
