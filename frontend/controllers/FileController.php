<?php

namespace phycom\frontend\controllers;


class FileController extends BaseController
{
	public function actions()
	{
		return [
			'download' => [
				'class' => 'yii2tech\filestorage\DownloadAction',
			],
		];
	}
}