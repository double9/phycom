<?php
namespace phycom\frontend\controllers;

use phycom\common\helpers\Url;
use phycom\frontend\models\LoginForm;
use phycom\frontend\models\PasswordResetRequestForm;
use phycom\frontend\models\PasswordResetForm;
use phycom\frontend\models\SignupForm;
use phycom\frontend\models\ContactForm;

use phycom\common\helpers\FlashMsg;

use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii;


/**
 * Site controller
 */
class SiteController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => yii\web\ErrorAction::class,
            ],
            'captcha' => [
                'class' => yii\captcha\CaptchaAction::class,
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
        	return $this->render('login', ['model' => $model]);
        }
    }

    /**
     * Logs out the current user.
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * @return string|yii\web\Response
     * @throws \Exception
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
	            FlashMsg::success(Yii::t('public/main', 'Please check your email for further instructions.'));
                return $this->redirect(Url::toRoute(['site/login']));
            } else {
            	FlashMsg::error(Yii::t('public/main', 'Sorry, we are unable to reset password for the provided email address.'));
            }
        }

        return $this->render('password-reset-request', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $t
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($t)
    {
        try {
            $model = new PasswordResetForm($t);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
        	FlashMsg::success(Yii::t('public/main', 'Your password was successfully changed. You can login with the new password.'));
            return $this->redirect(['/site/login']);
        }
        return $this->render('password-reset', [
            'model' => $model,
        ]);
    }

    /**
     * Displays contact page.
     * @return string|yii\web\Response
     * @throws yii\base\Exception
     */
	public function actionContact()
	{
		$model = new ContactForm();
		if ($model->load(Yii::$app->request->post())) {
		    if ($model->sendEmail(Yii::$app->vendor->email, Yii::$app->vendor->name)) {
				FlashMsg::success(Yii::t('public/main', 'Thank you for contacting us. We will respond to you as soon as possible.'));
			} else {
		        FlashMsg::error($model->errors);
				FlashMsg::error(Yii::t('public/main', 'There was an error sending your message.'));
			}
			return $this->redirect(Yii::$app->request->referrer);
		} else {
			return $this->render('contact', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Displays about page
	 * @return mixed
	 */
	public function actionAbout()
	{
        return $this->pageOrView('about');
	}

    /**
     * Displays privacy policy page
     * @return mixed
     */
    public function actionPrivacyPolicy()
    {
        return $this->pageOrView('privacy-policy');
    }

    /**
     * Displays terms and conditions page
     * @return mixed
     */
    public function actionTerms()
    {
        return $this->pageOrView('terms');
    }


    protected function pageOrView($name)
    {
        if ($page = Yii::$app->pages->getPage($name)) {
            return Yii::$app->response->redirect($page->url);
        }
        return $this->render($name);
    }
}
