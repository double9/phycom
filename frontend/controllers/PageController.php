<?php

namespace phycom\frontend\controllers;

use phycom\frontend\models\post\SearchPost;

use phycom\common\models\attributes\PostStatus;
use phycom\common\models\attributes\PostType;
use phycom\common\models\translation\PostTranslation;

use yii\web\NotFoundHttpException;
use yii;
/**
 * Class PageController
 * @package phycom\frontend\controllers
 */
class PageController extends BaseController
{
    /**
     * @param $key
     * @return string
     * @throws NotFoundHttpException
     * @throws yii\base\Exception
     */
    public function actionView($key)
    {
        $model = $this->findPageByUrlKey($key);
        return $this->render('view', ['page' => $model]);
    }

    /**
     * @param $urlKey
     * @return array|null|yii\db\ActiveRecord
     * @throws NotFoundHttpException
     * @throws yii\base\Exception
     */
    protected function findPageByUrlKey($urlKey)
    {
        $query = SearchPost::find()
            ->alias('p')
            ->select('p.*')
            ->innerJoin(['t' => PostTranslation::tableName()], [
                'and',
                't.post_id = p.id',
                ['t.url_key' => $urlKey]
            ])
            ->where(['p.status' => PostStatus::PUBLISHED, 'p.type' => PostType::PAGE]);

        if ($post = $query->one()) {
            return $post;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}