<?php

namespace phycom\frontend\controllers;

use phycom\common\models\Order;
use phycom\frontend\models\PromotionCodeForm;

use phycom\common\modules\delivery\models\ShippingForm;
use phycom\common\interfaces\CartItemInterface;
use phycom\common\helpers\Currency;
use phycom\common\helpers\FlashMsg;

use phycom\frontend\widgets\ActiveForm;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\helpers\Url;
use yii;

/**
 * Class CartController
 * @package phycom\frontend\controllers
 */
class CartController extends BaseController
{

    public $defaultAction = 'checkout';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'checkout',
                            'make-payment',
                            'apply-discount',
                            'add-product',
                            'add-product-validate',
                            'update',
                            'remove',
                            'clear',
                            'submit-order'
                        ],
                        'allow' => true,
                        'roles' => ['?','@']
                    ]
                ],
            ]
        ];
    }

    /**
     * Renders cart contents or redirects to previous page if the cart is empty
     * @return string|Response
     * @throws yii\base\InvalidConfigException
     */
    public function actionCheckout()
    {
        if (Yii::$app->cart->getCount(CartItemInterface::class) > 0) {
            if (Yii::$app->user->isGuest) {
                Yii::$app->user->setReturnUrl(Yii::$app->request->getUrl());
            }
            return $this->render('checkout-one');
        }
        if (Yii::$app->cart->getCount() > 0) {
            Yii::$app->cart->clear();
        }
        FlashMsg::warning(Yii::t('public/main', 'Shopping cart is empty'));
        return $this->redirect(Yii::$app->homeUrl);
    }

    /**
     * Renders the payment page on 2-step checkout
     * @return string|Response
     */
    public function actionMakePayment()
    {
        if ($orderId = Yii::$app->session->get('order')) {
            $order = Yii::$app->modelFactory->getOrder()::findOne($orderId);
            return $this->render('payment', ['order' => $order]);
        }
        FlashMsg::warning(Yii::t('public/main', 'Order not found or has expired'));
        return $this->redirect(Yii::$app->homeUrl);
    }

    /**
     * @return array
     * @throws yii\web\NotFoundHttpException
     */
    public function actionApplyDiscount()
    {
        $this->ajaxOnly(Response::FORMAT_JSON);

        $form = new PromotionCodeForm();
        $form->load(Yii::$app->request->post());

        if (!$discount = $form->apply()) {
            return ['error' => $form->lastError];
        }

        return [
            'id' => $discount->getUniqueId(),
            'code' => $discount->getCode(),
            'label' => $discount->getLabel(),
            'price' => Currency::toDecimal($discount->getPrice()),
            'type' => 'discount'
        ];
    }

    /**
     * @param int $id - product id
     * @param null $quantity
     * @return array
     * @throws NotFoundHttpException
     * @throws yii\base\UnknownPropertyException
     */
    public function actionAddProductValidate($id, $quantity = null)
    {
        $this->ajaxOnly(Response::FORMAT_JSON);

        if (!$product = Yii::$app->modelFactory->getProduct()::findOne($id)) {
            throw new NotFoundHttpException('The requested product ' . $id . ' does not exist.');
        }
        $model = Yii::$app->modelFactory->getCartForm($product);
        if ($quantity) {
            $model->quantity = $quantity;
        }
        $model->load(Yii::$app->request->post());
        /**
         * @var ActiveForm|string $ActiveForm
         */
        $ActiveForm = Yii::$app->modelFactory->getClassName('ActiveForm');
        return $ActiveForm::validate($model);
    }

    /**
     * @param int $id - product id
     * @param int $quantity
     * @return array
     * @throws NotFoundHttpException
     * @throws yii\base\Exception
     */
    public function actionAddProduct($id, $quantity = null)
    {
        $this->ajaxOnly(Response::FORMAT_JSON);

        if (!$product = Yii::$app->modelFactory->getProduct()::findOne($id)) {
            throw new NotFoundHttpException('The requested product ' . $id . ' does not exist.');
        }
        $model = Yii::$app->modelFactory->getCartForm($product);
        if ($quantity) {
            $model->quantity = $quantity;
        }
        $model->load(Yii::$app->request->post());

        if (!$item = $model->createOrderItem()) {
            return $model->errors;
        }

        Yii::$app->cart->add($item);

        // remove delivery items here as the parcel size might have been changed when product is added
        foreach (Yii::$app->cart->getDeliveryItems() as $item) {
            Yii::$app->cart->remove($item->getUniqueId());
        }
        return [];
    }

    /**
     * Updates cart item
     *
     * @param $id - cart item id
     * @return array
     *
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionUpdate($id)
    {
        $this->ajaxOnly(Response::FORMAT_JSON);

        $id = is_numeric($id) ? (int) $id : $id;

        if ($item = Yii::$app->cart->getItem($id)) {
            if ($qty = Yii::$app->request->get('qty')) {
                $item->setQuantity($qty);
                Yii::$app->cart->save();
            } else {
                $qty = method_exists($item, 'getQuantity') ? $item->getQuantity() : 1;
            }

            $itemType = null;
            foreach (Yii::$app->cart->itemTypes as $type => $alias) {
                if ($item instanceof $type) {
                    $itemType = $alias;
                }
            }

            return [
                'id' => $item->getUniqueId(),
                'code' => $item->getCode(),
                'price' => Currency::toDecimal($item->getPrice()),
                'type' => $itemType,
                'quantity' => $qty,
                'label' => $item->getLabel()
            ];
        }
        return ['error' => Yii::t('public/main', 'Item not found')];
    }


    /**
     * Removes an item from cart session
     *
     * @param $id - cart item id
     * @return array
     * @throws yii\web\NotFoundHttpException
     */
    public function actionRemove($id)
    {
        $this->ajaxOnly(Response::FORMAT_JSON);

        $id = is_numeric($id) ? (int) $id : $id;

        if (Yii::$app->cart->hasItem($id)) {
            Yii::$app->cart->remove($id);
        }
        if (Yii::$app->cart->getCount(CartItemInterface::class) === 0) {
            Yii::$app->cart->clear();
        }
        return [];
    }

    /**
     * Clears the cart and redirect to home
     * @return Response
     */
    public function actionClear()
    {
        Yii::$app->cart->clear();
        return $this->redirect(Yii::$app->homeUrl);
    }

    /**
     * Creates an order from the cart contents
     *
     * @return array
     *
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionSubmitOrder()
    {
        $this->ajaxOnly(Response::FORMAT_JSON);

        if (empty(Yii::$app->cart->getItems())) {
            $msg = Yii::t('public/main', 'Shopping cart is empty or session has expired');
            FlashMsg::warning($msg);
            return ['error' => $msg, 'url' => Url::toRoute('/site/index')];
        }

        $model = new ShippingForm();
        $model->load(Yii::$app->request->post());
        if (!$model->validate()) {
            return ['error' => $model->lastError];
        }
        foreach (Yii::$app->cart->getDeliveryItems() as $item) {
            Yii::$app->cart->remove($item->getUniqueId());
        }
        $shipment = $model->createShipment(Yii::$app->cart->getProductItems());

        if (!$shipment->getSelectedRate()) {
            return ['error' => Yii::t('public/main', 'Shipping rate is not selected')];
        }

        if ($shipment->hasErrorMessages()) {
            return ['error' => $shipment->messages[0]];
        } else {
            Yii::$app->cart->add($shipment->getShipmentLine());
        }

        $order = $this->getOrder();
        $orderForm = Yii::$app->modelFactory->getOrderForm(Yii::$app->user->identity, $order);
        $orderForm->load(Yii::$app->request->post());
        $redirect = Url::toRoute(['/cart/make-payment']);

        try {
            if ($order && $orderForm->update($order->id, Yii::$app->cart->getItems())) {
                return $this->ajaxSuccess([
                    'msg' => Yii::t('public/main', 'Order {order_id} updated', ['order_id' => $order->id]),
                    'url' => $redirect
                ]);
            }
            if ($order = $orderForm->create(Yii::$app->cart->getItems())) {
                $this->setOrder($order);
                return $this->ajaxSuccess([
                    'msg' => Yii::t('public/main', 'Order {order_id} created', ['order_id' => $order->id]),
                    'url' => $redirect
                ]);
            }
        } catch (yii\base\InvalidArgumentException $e) {
            if ($e->getCode() === 1) {
                Yii::$app->cart->clear();
                $msg = Yii::t('public/main', 'Shopping cart is empty');
                return $this->ajaxError($msg);
            }
            throw $e;
        }
        return ['error' => $orderForm->lastError];
    }

    protected function setOrder(Order $order)
    {
        Yii::$app->session->set('order', $order->id);
    }

    protected function getOrder()
    {
        return Yii::$app->session->get('order') ? Yii::$app->modelFactory->getOrder()::findOne(Yii::$app->session->get('order')) : null;
    }
}
