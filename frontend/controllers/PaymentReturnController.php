<?php

namespace phycom\frontend\controllers;


use phycom\common\helpers\FlashMsg;

use yii;

/**
 * Class PaymentReturnController
 * @package phycom\frontend\controllers
 */
class PaymentReturnController extends BaseController
{
    public $enableCsrfValidation = false;
    /**
     * @return yii\web\Response
     */
    public function actionIndex()
    {
        $status = Yii::$app->request->get('status');

        switch ($status) {

            case 'success':

                FlashMsg::success(Yii::t('public/payment', 'Payment was successfully completed. An order confirmation message will be sent to your email shortly.'));
                if (Yii::$app->session->get('order')) {
                    Yii::$app->session->remove('order');
                }
                Yii::$app->cart->clear();
                return $this->redirect(['/site/index']);

            case 'failed':
            case 'cancel':

                FlashMsg::warning(Yii::t('public/payment', 'Payment was cancelled'));
                return $this->redirect(['/cart/checkout']);

            case 'error':

                FlashMsg::error(Yii::t('public/payment', 'Error processing payment'));
                return $this->redirect(['/cart/checkout']);

            default:
                throw new yii\base\InvalidArgumentException('Invalid payment return status');
        }
    }
}