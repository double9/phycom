<?php

namespace phycom\frontend\components;


use phycom\common\models\Order;
use phycom\common\models\product\Product;
use phycom\common\models\User;

/**
 * Class ModelFactory
 * @package phycom\frontend\components
 *
 * @method \phycom\frontend\models\OrderForm getOrderForm(User $user = null, Order $order = null, array $params = [])
 * @method \phycom\frontend\widgets\ActiveForm getActiveForm(array $params = [])
 * @method \phycom\frontend\widgets\Modal getModal(array $params = [])
 * @method \phycom\frontend\assets\ButtonHelperAsset getButtonHelperAsset(array $params = [])
 * @method \phycom\frontend\models\product\SearchProduct getSearchProduct(array $params = [])
 * @method \phycom\frontend\models\post\SearchPost getSearchPost(array $params = [])
 * @method \phycom\frontend\models\CartForm getCartForm(Product $product, array $params = [])
 */
class ModelFactory extends \phycom\common\components\ModelFactory
{
    protected array $definitions = [
        'getOrderForm'         => 'frontend\models\OrderForm',
        'getActiveForm'        => 'frontend\widgets\ActiveForm',
        'getModal'             => 'frontend\widgets\Modal',
        'getButtonHelperAsset' => 'frontend\assets\ButtonHelperAsset',
        'getSearchProduct'     => 'frontend\models\product\SearchProduct',
        'getSearchPost'        => 'frontend\models\post\SearchPost',
        'getCartForm'          => 'frontend\models\CartForm'
    ];
}
