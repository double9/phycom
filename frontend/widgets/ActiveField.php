<?php

namespace phycom\frontend\widgets;

use phycom\common\helpers\JsExpression;
use phycom\common\helpers\PhoneHelper;
use phycom\common\models\Country;

use borales\extensions\phoneInput\PhoneInput;
use dosamigos\ckeditor\CKEditor as Editor;
use kartik\select2\Select2 as KartikSelect2;
use kartik\file\FileInput;
use yii2mod\rating\StarRating;

use yii\helpers\ArrayHelper;
use yii;

/**
 * Class ActiveField
 * @package phycom\frontend\widgets
 *
 * @property ActiveForm $form
 */
class ActiveField extends \yii\bootstrap\ActiveField
{
    /**
     * @param array $options
     * @return ActiveField
     */
    public function numberDialInput(array $options = []) : ActiveField
    {
        return $this->input('number', $options);
    }

    /**
     * @param array $options
     * @return ActiveField
     * @throws \Exception
     */
    public function datePicker(array $options = []) : ActiveField
    {
        $options = ArrayHelper::merge([
            'options' => ['type' => 'date'],
            'clientOptions' => [
                'locale' => substr(Yii::$app->language, 0, 2),
                'format' => 'YYYY-MM-DD',
                'widgetPositioning' => [
                    'horizontal' => 'auto',
                    'vertical' => 'bottom'
                ]
            ]
        ], $options);
        return $this->widget(DateTimePicker::class, $options);
    }

    /**
     * @param array $options
     * @return ActiveField
     * @throws \Exception
     */
    public function timePicker(array $options = []) : ActiveField
    {
        return $this->widget(DateTimePicker::class, ArrayHelper::merge([
            'options' => ['type' => 'time'],
            'clientOptions' => [
                'locale' => substr(Yii::$app->language, 0, 2),
                'format' => 'HH:mm',
                'widgetPositioning' => [
                    'horizontal' => 'auto',
                    'vertical' => 'bottom'
                ]
            ]
        ], $options));
    }

    /**
     * @param bool $prompt
     * @param array $options
     * @return ActiveField
     */
    public function boolean($prompt = false, array $options = []) : ActiveField
    {
    	if ($prompt) {
    		$options = ArrayHelper::merge([
    			'prompt' => is_string($prompt) ? $prompt :  Yii::t('public/main', 'Select')
		    ], $options);
	    }
    	if (!isset($options['value'])) {
            $options['value'] = (int) $this->model->{$this->attribute};
        }
    	return $this->dropdownList([
    		'1' => Yii::t('public/main', 'Yes'),
	        '0' => Yii::t('public/main', 'No'),
	    ], $options);
    }

    /**
     * @param array $options
     * @return ActiveField
     * @throws \Exception
     */
    public function fileInput($options = []) : ActiveField
    {
    	return $this->widget(FileInput::class, $options);
    }

    /**
     * @param string|bool $phoneCodeAttribute
     * @param array $options
     * @return $this
     * @throws \Exception
     */
    public function phoneInput($phoneCodeAttribute = 'phoneCode', $options = []) : ActiveField
    {
        $countries = Yii::$app->country->countries;
    	$options = ArrayHelper::merge([
		    'jsOptions' => [
			    'separateDialCode' => false,
			    'autoPlaceholder' => 'off',
                'nationalMode' => true,
                'allowExtensions' => true,
                'formatOnDisplay' => true,
                'preferredCountries' => Yii::$app->country->preferredCountries,
                'onlyCountries' => $countries,
                'allowDropdown' => empty($countries) || count($countries) > 1,
		    ],
		    'options' => [
		    	'class' => 'form-control'
		    ]
	    ], $options);

    	$field = $this->widget(PhoneInput::class, $options);
        $phoneId = yii\helpers\Html::getInputId($this->model, $this->attribute);

    	if ($phoneCodeAttribute && is_string($phoneCodeAttribute)) {

    	    $field->template = "{label}\n{country_code_input}\n{input}\n{hint}\n{error}";
		    $field->parts['{country_code_input}'] = yii\helpers\Html::activeHiddenInput($this->model, $phoneCodeAttribute);
		    $phoneCodeId = yii\helpers\Html::getInputId($this->model, $phoneCodeAttribute);

		    $this->form->options['oncountrychange'] = '$("#'.$phoneCodeId.'").val($("#'.$phoneId.'").intlTelInput("getSelectedCountryData").dialCode);';
		    $this->form->options['onsubmit'] = '$("#'.$phoneCodeId.'").val($("#'.$phoneId.'").intlTelInput("getSelectedCountryData").dialCode);';
		    $this->form->options['onfocusout'] = '$("#'.$phoneCodeId.'").val($("#'.$phoneId.'").intlTelInput("getSelectedCountryData").dialCode);';

		    $phoneCode = $this->model->$phoneCodeAttribute ?? null;
		    $countryCode = $phoneCode ? strtolower(Country::findOneByPhoneCode($phoneCode)->code) : Yii::$app->country->defaultCountry;
		    $this->form->getView()->registerJs("$('#$phoneId').intlTelInput('setCountry', '".$countryCode."');");
	    }
    	return $field;
    }

    /**
     * @param string|bool $phoneNumberAttribute
     * @param array $options
     * @return $this
     * @throws \Exception
     */
    public function phoneInput2($phoneNumberAttribute = 'phoneNumber', $options = []) : ActiveField
    {
        if (!$phoneNumberAttribute || !is_string($phoneNumberAttribute)) {
            throw new yii\base\InvalidArgumentException('Invalid phone number attribute');
        }
        $countries = Yii::$app->country->countries;
        $options = ArrayHelper::merge([
            'jsOptions' => [
                'separateDialCode' => false,
                'nationalMode' => true,
                'allowExtensions' => true,
                'formatOnDisplay' => true,
                'autoHideDialCode' => true,
                'preferredCountries' => Yii::$app->country->preferredCountries,
                'onlyCountries' => $countries,
                'allowDropdown' => empty($countries) || count($countries) > 1,
                'initialCountry' => 'auto',
                'geoIpLookup' => new JsExpression(<<<JS
                    function (callback) {
                        $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                            var countryCode = (resp && resp.country) ? resp.country : "";
                            callback(countryCode);
                        });
                    }
JS
)
            ],
            'options' => [
                'class' => 'form-control'
            ]
        ], $options);

        $field = $this->widget(PhoneInput::class, $options);
        $phoneId = yii\helpers\Html::getInputId($this->model, $this->attribute);
        $phoneNumberId = yii\helpers\Html::getInputId($this->model, $phoneNumberAttribute);

        $field->template = "{label}\n{phone_number_input}\n{input}\n{hint}\n{error}";
        $field->parts['{phone_number_input}'] = yii\helpers\Html::activeHiddenInput($this->model, $phoneNumberAttribute);

        $this->form->options['oncountrychange'] = '$("#'.$phoneNumberId.'").val($("#'.$phoneId.'").intlTelInput("getNumber"));';
        $this->form->options['onsubmit'] = '$("#'.$phoneNumberId.'").val($("#'.$phoneId.'").intlTelInput("getNumber"));';
        $this->form->options['onfocusout'] = '$("#'.$phoneNumberId.'").val($("#'.$phoneId.'").intlTelInput("getNumber"));';

        $phoneCode = $this->model->$phoneNumberAttribute ? PhoneHelper::getPhoneCode($this->model->$phoneNumberAttribute) : null;
        $countryCode = $phoneCode ? strtolower(Country::findOneByPhoneCode($phoneCode)->code) : Yii::$app->country->defaultCountry;
        $this->form->getView()->registerJs("$('#$phoneId').intlTelInput('setCountry', '".$countryCode."');");

        return $field;
    }

    /**
     * Renders the CKEditor widget
     *
     * @param array $options
     * @param string $pajaxId
     * @return $this
     * @throws \Exception
     */
    public function editor(array $options = [], $pajaxId = null) : ActiveField
    {
    	$height = 345;

        return $this->widget(Editor::class, [
            'options' => ArrayHelper::merge(['rows' => 6, 'style' => 'height: '.$height.'px;'], $options),
            'pajaxId' => $pajaxId,
            'preset' => 'custom',
	        'clientOptions' => [
	        	'height' => $height - 71,
		        'toolbar' => [
		        	['name' => 'styles', 'items' => ['Styles','Format']],
                    ['name' => 'clipboard', 'items' => ['Cut', 'Copy', 'Paste', 'PasteFromText', 'PasteFromWord', 'Undo', 'Redo']],
			        ['name' => 'links', 'items' => ['Link', 'Unlink', 'Anchor']],
			        ['name' => 'insert', 'items' => ['Table', 'HorizontalRule', 'SpecialChar']],
			        ['name' => 'basicstyles', 'items' => ['Bold','Italic','Strike','-','RemoveFormat']],
			        ['name' => 'paragraph', 'items' => ['NumberedList','BulletedList', '-', 'Outdent','Indent','-','Blockquote']]
		        ]
            ]
        ]);
    }

    /**
     * Renders the CKEditor widget
     *
     * @param array $options
     * @param string $pajaxId
     * @return $this
     * @throws \Exception
     */
	public function smallEditor(array $options = [], $pajaxId = null) : ActiveField
	{
		$height = 180;

		return $this->widget(Editor::class, [
			'options' => ArrayHelper::merge(['rows' => 6, 'style' => 'height: '.$height.'px;'], $options),
			'pajaxId' => $pajaxId,
			'preset' => 'custom',
			'clientOptions' => [
				'height' => $height - 71,
				'toolbar' => [
					['name' => 'styles', 'items' => ['Styles','Format']],
					['name' => 'clipboard', 'items' => ['Cut', 'Copy', 'Paste', 'PasteFromText', 'PasteFromWord', 'Undo', 'Redo']],
					['name' => 'links', 'items' => ['Link', 'Unlink', 'Anchor']],
					['name' => 'insert', 'items' => ['Table', 'HorizontalRule', 'SpecialChar']],
					['name' => 'basicstyles', 'items' => ['Bold','Italic','Strike','-','RemoveFormat']],
					['name' => 'paragraph', 'items' => ['NumberedList','BulletedList', '-', 'Outdent','Indent','-','Blockquote']]
				]
			]
		]);
	}

    /**
     * @param array $options
     * @param array $clientOptions
     * @return ActiveField
     * @throws \Exception
     */
	public function starRating(array $options = [], array $clientOptions = []) : ActiveField
    {
        return $this->widget(StarRating::class, [
                'options' => ArrayHelper::merge([
                    // Your additional tag options
                ], $options),
                'clientOptions' => ArrayHelper::merge([
                    // Your client options
                ], $clientOptions)
        ]);
    }

    /**
     * @param array $data
     * @param array $options
     * @return $this
     * @throws \Exception
     */
	public function multiSelect(array $data = [], array $options = []) : ActiveField
	{
		return $this->widget(KartikSelect2::class, [
			'data' => $data,
			'theme' => KartikSelect2::THEME_BOOTSTRAP,
			'options' => ArrayHelper::merge([
				'multiple' => true,
				'value' => array_keys($this->model->{$this->attribute})
			], $options)
		]);
	}

    /**
     * @param $value
     * @param array $options
     * @return ActiveField
     */
    public function hidden($value, array $options = []) : ActiveField
    {
    	$this->template = '{input}';
    	yii\helpers\Html::addCssClass($this->options, 'no-margin');
    	$this->label(false);
	    $this->hiddenInput($options);
	    $this->inputOptions['value'] = $value;
	    return $this;
    }

    /**
     * @param string $url
     * @param array $options
     * @return ActiveField
     * @throws \Exception
     */
    public function autocomplete($url, array $options = []) : ActiveField
    {
        return $this->widget(Select2::class, ArrayHelper::merge([
            'ajax'     => $url,
            'settings' => [
                'minimumInputLength' => 3
            ]
        ], $options));
    }
}
