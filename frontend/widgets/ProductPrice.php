<?php

namespace phycom\frontend\widgets;


use phycom\frontend\assets\ProductPriceAsset;

use phycom\common\helpers\Currency;
use phycom\common\helpers\f;
use phycom\common\models\product\Product;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\base\Widget;
use yii;

/**
 * Class ProductPrice
 * @package phycom\frontend\widgets
 */
class ProductPrice extends Widget
{
    public string $tag = 'div';
    /**
     * @var array
     */
    public array $options = [];
    /**
     * @var Product
     */
    public Product $product;
    /**
     * @var number
     */
    public $units = null;
    /**
     * @var array
     */
    public array $priceOptions = ['class' => 'product-price-item'];
    /**
     * @var array
     */
    public array $prevPriceOptions = ['class' => 'product-prev-price-item'];
    /**
     * @var string
     */
    public string $currencyCode;
    /**
     * @var string
     */
    public string $url;


    public function init()
    {
        parent::init();
        if (!isset($this->currencyCode)) {
            $this->currencyCode = Yii::$app->formatter->currencyCode;
        }
        if (!isset($this->url)) {
            $this->url = Yii::$app->urlManagerFrontend->createUrl(['product/get-price', 'id' => $this->product->id]);
        }
    }

    /**
     * @return string|void
     */
    public function run()
    {
        $this->options['id'] = $this->options['id'] ?? $this->getId();
        Html::addCssClass($this->options, 'product-price');

        if (!empty($this->product->prices)) {
            echo $this->renderPrice();
            $this->registerJs();
        }
    }

    /**
     * @return string
     */
    protected function renderPrice()
    {
        $options = $this->options;
        $options['data-currency-code'] = $this->currencyCode;
        $options['data-url'] = $this->url;

        $prices = $this->product->getPrice($this->units)->getPrices();

        if (count($prices) === 1) {
            $priceOptions = [$this->priceOptions];
        } else {
            $priceOptions = [$this->priceOptions, $this->prevPriceOptions];
            $options['data-discount'] = 1;
        }
        $priceData = [];
        foreach ($prices as $key => $price) {
            $priceData[] = [$price, $priceOptions[$key]];
        }


        $html = Html::beginTag($this->tag, $options);
        foreach (array_reverse($priceData) as $priceItem) {
            $html .= $this->renderPriceItem($priceItem[0], $priceItem[1]);
        }
        $html .= Html::endTag($this->tag);
        return $html;
    }

    /**
     * @param number $price
     * @param array $options
     * @return string
     */
    protected function renderPriceItem($price, array $options = [])
    {
        $priceTag = 'span';
        if (isset($options['tag'])) {
            $priceTag = $options['tag'];
            unset($options['tag']);
        }
        $options['data-value'] = Currency::toDecimal($price);
        return Html::tag($priceTag, f::currency($price), $options);
    }

    protected function registerJs()
    {
        $id = $this->options['id'];
        $view = $this->getView();
        ProductPriceAsset::register($view);

        $price = $this->product->getPrice($this->units)->getPrice();
        $prevPrice = $this->product->getPrice($this->units)->getPrevPrice();

        $jsConfig = Json::encode([
            'target'       => '#' . $id,
            'price'        => Currency::toDecimal($price),
            'prevPrice'    => $prevPrice ? Currency::toDecimal($prevPrice) : null,
            'currencyCode' => $this->currencyCode,
            'url'          => $this->url
        ]);

        $view->registerJs("jQuery(function(){ProductPrice.init($jsConfig)});");
    }
}
