<?php

namespace phycom\frontend\widgets\bootstrap4;

use phycom\common\helpers\Json;
use phycom\frontend\assets\DateTimePickerAsset;

use digitv\bootstrap\InputWidget;


use yii\helpers\Html;

/**
 * Class DateTimePicker
 * @package phycom\frontend\widgets\bootstrap4
 */
class DateTimePicker extends InputWidget
{
    /**
     * @var string the size of the input ('lg', 'md', 'sm', 'xs')
     */
    public $size;
    /**
     * @var array HTML attributes to render on the container
     */
    public $containerOptions = [];
    /**
     * @var string the addon markup if you wish to display the input as a component. If you don't wish to render as a
     * component then set it to null or false.
     */
    public $addon = '<i class="far fa-calendar-alt"></i>';
    /**
     * @var string the template to render the input.
     */
    public $template = '{input}{addon}';
    /**
     * @var bool whether to render the input as an inline calendar
     */
    public $inline = false;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->options['id'] = $this->getId();
        parent::init();

        // set the initial value if it has not been set
        if (!isset($this->clientOptions['date']) && ($value = $this->model->{$this->attribute})) {
            $this->clientOptions['date'] = $value;
        }

        if ($this->inline) {
//            $this->options['type'] = 'hidden';
            $this->options['readonly'] = 'readonly';
            $this->clientOptions['ignoreReadonly'] = true;
            $this->clientOptions['inline'] = true;
            $this->clientOptions['focusOnShow'] = false;
            $this->clientOptions['debug'] = true;
            Html::addCssClass($this->containerOptions, 'datepicker-inline');
        } else {
            Html::addCssClass($this->containerOptions, 'input-group date datepicker-popover');
        }
        if ($this->size) {
            Html::addCssClass($this->options, 'input-' . $this->size);
            Html::addCssClass($this->containerOptions, 'input-group-' . $this->size);
        }
        Html::addCssClass($this->options, 'form-control');
    }

    public function run()
    {
        $input = $this->hasModel()
            ? Html::activeTextInput($this->model, $this->attribute, $this->options)
            : Html::textInput($this->name, $this->value, $this->options);

        if ($this->addon && !$this->inline) {
            $addon = Html::tag('div', Html::tag('span', $this->addon, ['class' => 'input-group-text input-group-addon']), ['class' => 'input-group-append']);
            $input = strtr($this->template, ['{input}' => $input, '{addon}' => $addon]);
        }
        if ($this->inline) {
            $input = strtr($this->template, ['{input}' => $input, '{addon}' => '']);
        }
        echo Html::tag('div', $input, $this->containerOptions);

        $this->registerClientScript();
    }



    /**
     * Registers required script for the plugin to work as DatePicker
     */
    public function registerClientScript()
    {
        $js = [];
        $view = $this->getView();

        DateTimePickerAsset::register($view);

        $id = $this->options['id'];
        $selector = ";jQuery('#$id')";

//        if ($this->addon || $this->inline) {
//            $selector .= ".parent()";
//        }

        $options = !empty($this->clientOptions) ? Json::encode($this->clientOptions) : '';

        if ($this->inline) {
            $dateFormat = $this->clientOptions['format'] ?? 'YYYY-MM-DD';
            $this->clientEvents['dp.change'] = "function (e){jQuery('#$id').attr('value', e.date.format('$dateFormat'));}";
        }

        $js[] = "$selector.datetimepicker($options);";

        if (!empty($this->clientEvents)) {
            foreach ($this->clientEvents as $event => $handler) {
                $js[] = "$selector.on('$event', $handler);";
            }
        }

//        $js[] = "$selector.data('DateTimePicker').show();";

        $view->registerJs(implode("\n", $js));
    }
}