<?php

namespace phycom\frontend\widgets\bootstrap4;

use phycom\common\models\Language;
use digitv\bootstrap\widgets\Dropdown;
use yii;

/**
 * Class LanguageSwitcher
 * @package phycom\frontend\widgets\bootstrap4
 */
class LanguageSwitcher extends Dropdown {

    public $langLabels;

    private $isError;

    public function init()
    {
        $route = Yii::$app->controller->route;
        $params = Yii::$app->request->get();
        $this->isError = $route === Yii::$app->errorHandler->errorAction;

        array_unshift($params, '/' . $route);

        foreach (Yii::$app->urlManager->languages as $language) {
            $isWildcard = substr($language, -2) === '-*';
            if ($isWildcard) {
                $language = substr($language, 0, 2);
            }
            $params['language'] = $language;
            $params['url-language'] = $language;
            $this->items[] = [
                'label' => $this->label($language),
                'url'   => $params,
            ];
        }
        parent::init();
    }

    public function run()
    {
        // Only show this widget if we're not on the error page
        if ($this->isError) {
            return '';
        } else {
            return parent::run();
        }
    }

    public function label($code)
    {
        $language = Language::findOne(['code' => substr($code, 0, 2)]);
        return $language ? $language->native : null;
    }
}