<?php

namespace phycom\frontend\widgets\bootstrap4;

use yii\helpers\Html;

/**
 * Class Modal
 * @package phycom\frontend\widgets\bootstrap4
 */
class Modal extends \digitv\bootstrap\widgets\Modal
{
    public $title;
    public $content;

    public function run()
    {
        if (!empty($this->content)) {
            echo $this->content;
        }
        parent::run();
    }

    protected function renderHeader()
    {
        if ($this->title) {
            $this->header = $this->title;
        }
        $button = $this->renderCloseButton();
        if ($this->header !== null) {
            if ($button !== null) {
                $this->header = $this->header . "\n" . $button;
            }
            Html::addCssClass($this->headerOptions, ['widget' => 'modal-header']);
            return Html::tag('div', "\n" . $this->header . "\n", $this->headerOptions);
        } else {
            if ($button !== null) {
                return Html::tag('div', $button, ['class' => 'pos-absolute-right z-index-50 pd-t-15 pd-r-20 tx-normal']);
            }
            return null;
        }
    }
}