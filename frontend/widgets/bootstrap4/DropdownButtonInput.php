<?php

namespace phycom\frontend\widgets\bootstrap4;


use phycom\frontend\assets\DropdownSelectAsset;

use digitv\bootstrap\widgets\Button;
use digitv\bootstrap\InputWidget;
use digitv\bootstrap\Widget;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class DropdownSelect
 * @package phycom\frontend\widgets\bootstrap4
 */
class DropdownButtonInput extends InputWidget
{

    /**
     * @var string the button label
     */
    public $label = '-';
    /**
     * @var array the HTML attributes of the button group container.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $containerOptions = [];
    /**
     * @var array the HTML attributes of the button.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $buttonOptions = [];
    /**
     * @var array the configuration array for [[Dropdown]].
     */
    public $dropdown = [];
    /**
     * @var boolean whether to display a group of split-styled button group.
     */
    public $split = false;
    /**
     * @var boolean whether to render dropup.
     */
    public $dropUp = false;
    /**
     * @var string the tag to use to render the button
     */
    public $tagName = 'button';
    /**
     * @var boolean whether the label should be HTML-encoded.
     */
    public $encodeLabel = true;
    /**
     * @var string name of a class to use for rendering dropdowns withing this widget. Defaults to [[Dropdown]].
     * @since 2.0.7
     */
    public $dropdownClass = Dropdown::class;


    /**
     * Renders the widget.
     */
    public function run()
    {
        $inputId = $this->id;
        $btnId = $this->buttonOptions['id'] = $this->buttonOptions['id'] ?? $inputId . '-btn';


        unset($this->containerOptions['id']);
        Html::addCssClass($this->containerOptions, ['widget' => 'btn-group']);
        if($this->dropUp) {
            Html::addCssClass($this->containerOptions, 'dropup');
        }
        $options = $this->containerOptions;
        $tag = ArrayHelper::remove($options, 'tag', 'div');

        $this->registerPlugin('dropdown');
        $res = implode("\n", [
            Html::beginTag($tag, $options),
            $this->renderButton(),
            $this->renderDropdown(),
            $this->renderInputHtml('hidden'),
            Html::endTag($tag)
        ]);

        $view = $this->view;

        DropdownSelectAsset::register($view);
        $view->registerJs("jQuery(function(){DropdownSelect.init('#$inputId', '#$btnId')});");
        return $res;
    }

    /**
     * Generates the button dropdown.
     * @return string the rendering result.
     * @throws \Exception
     */
    protected function renderButton()
    {
        Html::addCssClass($this->buttonOptions, ['widget' => 'btn']);
        $label = $this->label;

        if ($this->value) {
            foreach ($this->dropdown['items'] as $item) {
                if ($item['value'] == $this->value) {
                    $label = $item['label'];
                    break;
                }
            }
        }

        if ($this->encodeLabel) {
            $label = Html::encode($label);
        }
        if ($this->split) {
            $options = $this->buttonOptions;
            $this->buttonOptions['data-toggle'] = 'dropdown';
            Html::addCssClass($this->buttonOptions, ['toggle' => 'dropdown-toggle']);
            $splitButton = Button::widget([
                'label' => '',
                'encodeLabel' => false,
                'options' => $this->buttonOptions,
                'view' => $this->getView(),
            ]);
        } else {
            $options = $this->buttonOptions;
            if (!isset($options['href']) && $this->tagName === 'a') {
                $options['href'] = '#';
            }
            Html::addCssClass($options, ['toggle' => 'dropdown-toggle']);
            $options['data-toggle'] = 'dropdown';
            $splitButton = '';
        }

        return Button::widget([
                'tagName' => $this->tagName,
                'label' => $label,
                'options' => $options,
                'encodeLabel' => false,
                'view' => $this->getView(),
            ]) . "\n" . $splitButton;
    }

    /**
     * Generates the dropdown menu.
     * @return string the rendering result.
     * @throws \Exception
     */
    protected function renderDropdown()
    {
        $config = $this->dropdown;
        $config['clientOptions'] = false;
        $config['view'] = $this->getView();
        /** @var Widget $dropdownClass */
        $dropdownClass = $this->dropdownClass;
        return $dropdownClass::widget($config);
    }
}