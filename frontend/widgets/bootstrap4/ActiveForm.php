<?php

namespace phycom\frontend\widgets\bootstrap4;

use phycom\common\assets\ActiveFormHelperAsset;
use phycom\frontend\models\behaviors\HoneypotBehavior;
use yii\base\Model;

/**
 * Class ActiveForm
 * @package phycom\frontend\widgets\bootstrap4
 */
class ActiveForm extends \digitv\bootstrap\ActiveForm
{
    public $fieldClass = 'phycom\frontend\widgets\bootstrap4\ActiveField';

    public function run()
    {
        $html = parent::run();
        ActiveFormHelperAsset::register($this->getView());
        return $html;
    }

    /**
     * @inheritdoc
     * @return ActiveField the created ActiveField object
     */
    public function field($model, $attribute, $options = [])
    {
        return parent::field($model, $attribute, $options);
    }

    /**
     * @param Model $model
     * @return ActiveField|string
     */
    public function honeyPotField($model)
    {
        if ($honeyPotBehavior = $model->getBehavior('honeypot')) {
            /**
             * @var HoneypotBehavior $honeyPotBehavior
             */
            return $this->field($model, $honeyPotBehavior->getHoneyPotAttribute())->honeyPot();
        }
        return '';
    }
}
