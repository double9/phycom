<?php

namespace phycom\frontend\widgets;

use phycom\common\helpers\Currency;
use phycom\common\helpers\f;
use phycom\common\helpers\JsExpression;
use phycom\common\models\attributes\ProductStatus;
use phycom\frontend\assets\CartButtonAsset;

use phycom\common\models\product\Product;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\base\Widget;
use yii;

/**
 * Class CartButton
 * @package phycom\frontend\widgets
 */
class CartButton extends Widget
{
    const PRICE_APPEND = 'append';
    const PRICE_PREPEND = 'prepend';

    /**
     * @var array
     */
    public $options = [];
    /**
     * @var array
     */
    public $buttonOptions = ['class' => 'btn btn-success'];
    /**
     * @var Product
     */
    public $product;
    /**
     * @var string
     */
    public $label;
    /**
     * @var string
     */
    public $loadingText;
    /**
     * @var string
     */
    public $route = '/cart/add-product';
    /**
     * @var bool|string
     */
    public $price = false;
    /**
     * @var array
     */
    public $priceOptions = [];
    /**
     * @var yii\widgets\ActiveForm
     */
    public $form;
    /**
     * @var string
     */
    public $promptModalId = '#checkout-prompt-modal';

    /**
     * @return string|void
     * @throws \Exception
     */
    public function run()
    {
        $this->options['id'] = $this->options['id'] ?? $this->getId();

        $options = $this->options;
        $options['data-cart'] = $this->itemsInCart();

        Html::addCssClass($options, 'cart-btn-form');

        if ($this->product->status->in([ProductStatus::INACTIVE])) {
            echo Html::tag('p', Yii::t('public/main', 'This item is not available at the moment'), ['class' => 'product-inactive-info']);
            return;
        }

        if (!$this->form) {
            echo Html::beginForm($this->getUrl(), 'post', $options);
            echo Html::hiddenInput('product', $this->product->id);
        }

        echo $this->renderCartButton();

        if (!$this->form) {
            echo Html::endForm();
        }

        $this->registerJs();
    }

    /**
     * @return string
     * @throws \Exception
     */
    protected function renderCartButton()
    {
        $html = Html::beginTag('div', ['class' => 'cart-btn-container']);

        $label = $this->label ?: $this->getDefaultLabel();

        $btnOptions = $this->buttonOptions;
        $btnOptions['data-loading-text'] = $this->loadingText ?: $this->getDefaultLoadingText();
        $btnOptions['data-unit-pricing'] = $this->hasUnitPricing() ? 1 : 0;
        Html::addCssClass($btnOptions, 'cart-btn');

        $priceOptions = $this->priceOptions;

        if ($this->price && $this->price === self::PRICE_PREPEND) {
            Html::addCssClass($priceOptions, self::PRICE_PREPEND);
            $html .= $this->renderPrice($priceOptions);
        }

        $html .= Html::button($label, $btnOptions);

        if ($this->price && $this->price === self::PRICE_APPEND) {
            Html::addCssClass($priceOptions, self::PRICE_APPEND);
            $html .= $this->renderPrice($priceOptions);
        }

        $html .= Html::endTag('div');
        return $html;
    }

    /**
     * @param array $options
     * @return string
     * @throws \Exception
     */
    protected function renderPrice(array $options = [])
    {
        return ProductPrice::widget([
            'product' => $this->product,
            'options' => $this->priceOptions
        ]);
    }


    protected function registerJs()
    {
        $id = $this->options['id'];
        $view = $this->getView();
        CartButtonAsset::register($view);

        $jsConfig = [
            'form'         => '#' . $id,
            'prompt'       => $this->promptModalId,
            'unitPricing'  => $this->hasUnitPricing(),
        ];

        if ($this->form) {
            $jsConfig['beforeSubmit'] = new JsExpression("
                function (btn) {
                    let form = btn.closest('form');
                    return ActiveFormHelper.validate(form);
                }
            ");
        }

        if ($this->price) {
            $jsConfig['priceTarget'] = !in_array($this->price, [self::PRICE_PREPEND, self::PRICE_APPEND])
                ? $this->price
                : '#' . $id . ' .product-price';
        }

        $jsConfig = Json::encode($jsConfig);
        $view->registerJs("jQuery(function(){CartButton.init($jsConfig)});");
    }

    /**
     * @return bool
     */
    protected function hasUnitPricing()
    {
        return true; //return count($this->product->prices) > 1;
    }

    /**
     * @return string
     */
    protected function getDefaultLabel()
    {
        return Yii::t('public/main', 'Add to cart');
    }

    /**
     * @return string
     */
    protected function getDefaultLoadingText()
    {
        return '<i class="fas fa-circle-notch fa-spin"></i><span style="margin-left: 5px;">' . Yii::t('public/main', 'loading') . '...</span>';
    }

    /**
     * @return array
     */
    protected function getUrl()
    {
        return [$this->route, 'id' => $this->product->id];
    }

    /**
     * Finds the count of how many of same items are in cart
     * @return int|false
     */
    protected function itemsInCart()
    {
        foreach (Yii::$app->cart->getProductItems() as $item) {
            if ($this->product->sku === $item->getUniqueId()) {
                return min(1, $item->getQuantity());
            }
        }
        return false;
    }
}
