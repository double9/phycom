<?php

namespace phycom\frontend\widgets;

use phycom\common\helpers\Filter;
use phycom\common\assets\ActiveFormHelperAsset;

/**
 * Class ActiveForm
 * @package phycom\frontend\widgets
 */
class ActiveForm extends \yii\bootstrap\ActiveForm
{
	public $fieldClass = 'phycom\frontend\widgets\ActiveField';

	public function run()
    {
        parent::run();
        ActiveFormHelperAsset::register($this->getView());
    }

    /**
	 * Renders a date-range picker
	 *
	 * @param object $model - Search model
	 * @param string $from - model attribute name for date range starting
	 * @param string $to - model attribute name for date range ending
	 * @param array $options
	 * @return string
	 */
	public function dateRangePicker($model, $from, $to, array $options = [])
	{
		return Filter::dateRangePicker($model, $from, $to, $options);
	}

	/**
	 * @inheritdoc
	 * @return ActiveField the created ActiveField object
	 */
	public function field($model, $attribute, $options = [])
	{
		return parent::field($model, $attribute, $options);
	}
}