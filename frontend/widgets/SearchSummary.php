<?php

namespace phycom\frontend\widgets;


use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii;

/**
 * Class SearchSummary
 * @package phycom\frontend\widgets
 */
class SearchSummary extends Widget
{
    public $options = [];
    /**
     * @var yii\data\BaseDataProvider
     */
    public $dataProvider;


    public function run()
    {
        $count = $this->dataProvider->getCount();

        if ($count <= 0) {
            return '';
        }

        $summaryOptions = $this->options;
        $tag = ArrayHelper::remove($summaryOptions, 'tag', 'div');

        if (($pagination = $this->dataProvider->getPagination()) !== false) {
            $totalCount = $this->dataProvider->getTotalCount();
            $begin = $pagination->getPage() * $pagination->pageSize + 1;
            $end = $begin + $count - 1;
            if ($begin > $end) {
                $begin = $end;
            }
            $page = $pagination->getPage() + 1;
            $pageCount = $pagination->pageCount;

            return Html::tag($tag, Yii::t('yii', 'Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}.', [
                'begin' => $begin,
                'end' => $end,
                'count' => $count,
                'totalCount' => $totalCount,
                'page' => $page,
                'pageCount' => $pageCount,
            ]), $summaryOptions);

        } else {
            $begin = $page = $pageCount = 1;
            $end = $totalCount = $count;

            return Html::tag($tag, Yii::t('yii', 'Total <b>{count, number}</b> {count, plural, one{item} other{items}}.', [
                'begin' => $begin,
                'end' => $end,
                'count' => $count,
                'totalCount' => $totalCount,
                'page' => $page,
                'pageCount' => $pageCount,
            ]), $summaryOptions);
        }
    }
}