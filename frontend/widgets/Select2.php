<?php

namespace phycom\frontend\widgets;


use phycom\common\helpers\JsExpression;
use phycom\frontend\assets\Select2Asset;
use phycom\frontend\assets\Select2Bootstrap4Asset;
use phycom\frontend\assets\Select2BootstrapAsset;
use phycom\frontend\assets\Select2MaximizeAsset;
use phycom\frontend\assets\Select2WidgetAsset;

use phycom\common\helpers\Json;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\AssetBundle;
use yii\widgets\InputWidget;

/**
 * Class Select2
 * @package phycom\frontend\widgets
 */
class Select2 extends InputWidget
{
    const THEME_BOOTSTRAP_V3 = 'bootstrap';
    const THEME_BOOTSTRAP_V4 = 'bootstrap4';

    /**
     * @var string
     */
    public $theme = self::THEME_BOOTSTRAP_V3;
    /**
     * Language code
     * @var string
     */
    public $language;
    /**
     * Array data
     * @example [['id'=>1, 'text'=>'enhancement'], ['id'=>2, 'text'=>'bug']]
     * @var array
     */
    public $data = [];
    /**
     * You can use Select2Action to provide AJAX data
     * @see \yii\helpers\BaseUrl::to()
     * @var array|string
     */
    public $ajax;
    /**
     * @see \yii\helpers\BaseArrayHelper::map()
     * @var array
     */
    public $items = [];
    /**
     * A placeholder value can be defined and will be displayed until a selection is made
     * @var string
     */
    public $placeholder;
    /**
     * Multiple select boxes
     * @var boolean
     */
    public $multiple = false;

    public $readonly = false;
    /**
     * Tagging support
     * @var boolean
     */
    public $tags;
    /**
     * @link https://select2.github.io/options.html
     * @var array
     */
    public $settings = [];

    /**
     * If value is integer, then it passed as "cushion" parameter
     * @link https://github.com/panorama-ed/maximize-select2-height
     * @var mixed
     */
    public $maximize = false;

    /**
     * @var string[] the JavaScript event handlers.
     */
    public $events = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if ($this->readonly) {
            $this->options['disabled'] = true;
            $this->settings['readonly'] = true;
        }
        if ($this->tags) {
            $this->options['data-tags'] = 'true';
            $this->options['multiple'] = true;
        }
        if ($this->language) {
            $this->options['data-language'] = $this->language;
        }
        if (!is_null($this->ajax)) {
            $this->options['data-ajax--url'] = Url::to($this->ajax);
            $this->options['data-ajax--cache'] = 'true';
        }
        if ($this->placeholder) {
            $this->options['data-placeholder'] = $this->placeholder;
        }
        if ($this->multiple) {
            $this->options['data-multiple'] = 'true';
            $this->options['multiple'] = true;
        }
        if (!empty($this->data)) {
            $this->options['data-data'] = Json::encode($this->data);
        }
        if (!isset($this->options['class'])) {
            $this->options['class'] = 'form-control';
        }
        if ($this->theme) {
            $this->options['data-theme'] = $this->theme;
        }
        if ($this->multiple || !empty($this->settings['multiple'])) {
            if ($this->hasModel()) {
                $name = isset($this->options['name']) ? $this->options['name'] : Html::getInputName($this->model, $this->attribute);
            } else {
                $name = $this->name;
            }
            if (substr($name, -2) != '[]') {
                $this->options['name'] = $this->name = $name . '[]';
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        if ($this->hasModel()) {
            echo Html::activeDropDownList($this->model, $this->attribute, $this->items, $this->options);
        } else {
            echo Html::dropDownList($this->name, $this->value, $this->items, $this->options);
        }
        echo Html::tag('div', '', ['id' => $this->options['id'] . '-dropdown-container', 'class' => 'dropdown-container']);
        $this->registerAssets();
    }

    /**
     * Registers Assets
     */
    public function registerAssets()
    {
        $view = $this->getView();
        /* @var $bundle AssetBundle */
        $bundle = Select2Asset::register($view);
        if ($this->language !== false) {
            $langs[0] = $this->language ? $this->language : \Yii::$app->language;
            if (($pos = strpos($langs[0], '-')) > 0) {
                // If "en-us" is not found, try to use "en".
                $langs[1] = substr($langs[0], 0, $pos);
            }
            foreach ($langs as $lang) {
                $langFile = "/js/i18n/{$lang}.js";
                if (file_exists($bundle->sourcePath . $langFile)) {
                    $view->registerJsFile($bundle->baseUrl . $langFile, ['depends' => Select2Asset::class]);
                    break;
                }
            }
        }

        switch ($this->theme) {
            case self::THEME_BOOTSTRAP_V3:
                Select2BootstrapAsset::register($view);
                break;
            case self::THEME_BOOTSTRAP_V4:
                Select2Bootstrap4Asset::register($view);
                break;
            default:
        }

        Select2WidgetAsset::register($view);

        // fixes the issue where the dropdown search field does not get focus inside bootstrap modal
        if (!isset($this->settings['dropdownParent'])) {
            $id = $this->options['id'];
            $this->settings['dropdownParent'] = "js:$('#$id-dropdown-container')";
        }

        $settings = Json::encode($this->settings);
        $js = "jQuery('#{$this->options['id']}').select2($settings)";
        if ($this->maximize) {
            Select2MaximizeAsset::register($view);
            if (is_integer($this->maximize)) {
                $this->maximize = "{cushion: $this->maximize}";
            } elseif (is_array($this->maximize)) {
                $this->maximize = Json::encode($this->maximize);
            } else {
                $this->maximize = '{}';
            }
            $js .= ".maximizeSelect2Height($this->maximize)";
        }
        foreach ($this->events as $event => $handler) {
            $js .= '.on("'.$event.'", ' . new JsExpression($handler) . ')';
        }
        $view->registerJs("$js;");
//        $view->registerJs("$.fn.modal.Constructor.prototype.enforceFocus = $.noop;");
    }
}