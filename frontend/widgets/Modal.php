<?php

namespace phycom\frontend\widgets;

use yii\bootstrap\Widget;
use yii\helpers\Html;
use yii;

/**
 * Class Modal
 * @package phycom\frontend\widgets
 */
class Modal extends Widget
{
    const SEPARATOR = '___';

    public $title;
	public $content;
	public $options = ['class' => ''];

	public $bodyOptions = ['class' => 'panel-body'];
	public $bodyClass = 'panel-body';
	public $noMargin = false;

	protected $templateParts = [];
	protected $contentDisplayed = false;

	public function init()
	{
		parent::init();
		if (!isset($this->options['id'])) {
			$this->options['id'] = $this->getId();
		}
		$this->options['title'] = $this->title ?: '&nbsp;';
		$this->options['content'] = static::SEPARATOR;

		$bodyOptions = $this->bodyOptions;
        Html::addCssClass($bodyOptions, $this->bodyClass);
		if ($this->noMargin) {
		    Html::addCssClass($bodyOptions, 'no-margin');
        }

		$this->options['bodyOptions'] = $bodyOptions;
		$this->templateParts = explode(static::SEPARATOR, $this->getView()->renderFile( $this->viewPath . '/partials/modal.php', $this->options));
	}

	public function run()
	{
		if (!$this->contentDisplayed) {
			echo $this->templateParts[0] . $this->content;
			$this->contentDisplayed = true;
		}
		echo $this->templateParts[1];
	}

	public static function begin($config = [])
	{
		/**
		 * @var static $modal
		 */
		$modal = parent::begin($config);
		echo $modal->templateParts[0];
		$modal->contentDisplayed = true;
		return $modal;
	}


	public function getViewPath()
	{
		return Yii::$app->viewPath;
	}
}