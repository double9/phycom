<?php

namespace phycom\frontend\tests\unit\models;

use phycom\common\fixtures\User as UserFixture;
use phycom\frontend\models\PasswordResetForm;

class ResetPasswordFormTest extends \Codeception\Test\Unit
{
    /**
     * @var \phycom\frontend\tests\UnitTester
     */
    protected $tester;


    public function _before()
    {
        $this->tester->haveFixtures([
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
        ]);
    }

    public function testResetWrongToken()
    {
        $this->tester->expectException('yii\base\InvalidArgumentException', function() {
            new PasswordResetForm('');
        });

        $this->tester->expectException('yii\base\InvalidArgumentException', function() {
            new PasswordResetForm('notexistingtoken_1391882543');
        });
    }

    public function testResetCorrectToken()
    {
        $user = $this->tester->grabFixture('user', 0);
        $form = new PasswordResetForm($user['password_reset_token']);
        expect_that($form->resetPassword());
    }

}
