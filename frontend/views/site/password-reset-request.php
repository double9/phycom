<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model phycom\frontend\models\PasswordResetRequestForm */

use rmrevin\yii\fontawesome\FAR;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('public/main','Request password reset');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-request-password-reset">
    <h2><?= Html::encode($this->title) ?></h2>
    <p><?= Yii::t('public/main','Please fill out your email. A link to reset password will be sent there.') ?></p>
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
                <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
                <div class="form-group">
                    <?= Html::submitButton(
	                        FAR::i(FAR::_ENVELOPE, ['class' => 'pull-left', 'style' => 'position: relative; top: 4px;']) . '&nbsp;&nbsp;' .
                            Yii::t('public/main','Send'),
                            [
                                'class' => 'btn btn-flat btn-lg btn-primary'
                            ]
                    ) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
