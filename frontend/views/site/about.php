<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = Yii::t('public/main','About');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <code><?= __FILE__ ?></code>
</div>
