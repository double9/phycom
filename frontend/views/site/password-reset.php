<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model phycom\frontend\models\PasswordResetForm */

use rmrevin\yii\fontawesome\FAS;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('public/main','Reset password');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-reset-password">
    <h1><?= Html::encode($this->title) ?></h1>
    <p><?= Yii::t('public/main','Please choose your new password:'); ?></p>
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
                <?= $form->field($model, 'password')->passwordInput(['autofocus' => true, 'placeholder' => $model->getAttributeLabel('password')])->label(false) ?>
                <div class="form-group">
                    <?= Html::submitButton(
	                        FAS::i(FAS::_SIGN_IN_ALT, ['class' => 'pull-left', 'style' => 'position: relative; top: 4px;']) . '&nbsp;&nbsp;' .
                            Yii::t('public/main','Save'),
                            [
                                'class' => 'btn btn-flat btn-primary'
                            ]
                    ) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
