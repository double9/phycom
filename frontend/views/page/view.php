<?php

/**
 * @var $this yii\web\View
 * @var $model \phycom\frontend\models\post\SearchPost
 */

$this->params['breadcrumbs'][] = $this->title;
?>
<div id="page">
    <h2><?= $this->title; ?></h2>
    <?= $model->content; ?>
</div>