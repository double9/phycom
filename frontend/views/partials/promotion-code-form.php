<?php

/**
 * @var \yii\web\View $this
 */

use phycom\frontend\models\PromotionCodeForm;
use phycom\frontend\widgets\ActiveForm;

use phycom\common\helpers\Url;
use phycom\common\helpers\c;

use yii\helpers\Html;


$model = new PromotionCodeForm();
$options = [];

if (count(Yii::$app->cart->getDiscountItems())) {
    $options['style'] = 'display: none;';
}

$this->registerJs("promotion.init('#promotion-code-form');")
?>

<?php if (c::param('promotionCodesEnabled', false)): ?>
    <?php
        $form = ActiveForm::begin([
            'id' => 'promotion-code-form',
            'action' => Url::toRoute(['/cart/apply-discount']),
            'options' => $options
        ]);
    ?>

    <div class="clearfix">
        <?= Html::button(Yii::t('public/discount-rule', 'Apply'), ['class' => 'btn btn-flat btn-primary pull-right']); ?>
        <?= $form->field($model, 'code', [
                'template' => "{input}\n{error}\n{hint}",
                'options' => [
                    'class' => 'pull-right',
                    'style' => 'margin-right: 10px;'
                ]
            ])
            ->textInput([
                'placeholder' => $model->getAttributeLabel('code'),
                'style' => 'min-width: 200px;'
            ])
            ->label(false);
        ?>
    </div>
    <hr>
    <?php ActiveForm::end(); ?>

<?php endif; ?>