<?php

namespace phycom\frontend\helpers;


use phycom\common\models\product\Product;
use phycom\common\models\product\ProductCategory;
use phycom\common\models\product\ProductTag;
use phycom\frontend\models\product\ProductDataProvider;
use phycom\frontend\models\product\SearchProduct;

use yii\helpers\ArrayHelper;
use yii\db\Query;
use yii;

/**
 * Class ProductHelper
 * @package phycom\frontend\helpers
 */
class ProductHelper extends \phycom\common\helpers\ProductHelper
{
    /**
     * @param ProductCategory|string $key - product category model or url key
     * @return SearchProduct|null
     * @throws yii\base\Exception
     */
    public static function bestDealByCategory($key)
    {
        $dataProvider = static::bestDealsByCategory($key);
        $dataProvider->query->limit(1);
        $result = $dataProvider->getModels();
        return !empty($result) ? $result[0] : null;
    }

    /**
     * @param ProductCategory|string $key - product category model or url key
     * @return ProductDataProvider|yii\data\ActiveDataProvider
     * @throws yii\base\Exception
     */
    public static function bestDealsByCategory($key)
    {
        if (is_string($key)) {
            $category = ProductCategory::findByUrlKey($key);
        } else if ($key instanceof ProductCategory) {
            $category = $key;
        } else {
            throw new yii\base\InvalidArgumentException('Invalid key given. Expected string or ' . ProductCategory::class);
        }

        $dataProvider = Yii::$app->modelFactory->getSearchProduct([
            'categoryId' => $category->id,
            'tags' => [ProductTag::TAG_BARGAIN]
        ])->search();

        $dataProvider->sort->defaultOrder = ['tag_timestamp' => SORT_DESC];

        return $dataProvider;
    }

    /**
     * @param int|null $limit
     * @return ProductDataProvider|yii\data\ActiveDataProvider
     * @throws yii\base\Exception
     */
    public static function bestDeals($limit = null)
    {
        $dataProvider = Yii::$app->modelFactory->getSearchProduct([
            'tags' => [ProductTag::TAG_BARGAIN]
        ])->search();

        $dataProvider->sort->defaultOrder = ['tag_timestamp' => SORT_DESC];

        if ($limit) {
            $dataProvider->pagination->pageSize = $limit;
        }
        return $dataProvider;
    }

    /**
     * @param int|null $limit
     * @return ProductDataProvider|yii\data\ActiveDataProvider
     * @throws yii\base\Exception
     */
    public static function featured($limit = null)
    {
        $dataProvider = Yii::$app->modelFactory->getSearchProduct([
            'tags' => [ProductTag::TAG_FEATURED]
        ])->search();

        $dataProvider->sort->defaultOrder = ['tag_timestamp' => SORT_DESC];

        if ($limit) {
            $dataProvider->pagination->pageSize = $limit;
        }
        return $dataProvider;
    }

    /**
     * @param int|null $count
     * @return ProductDataProvider|yii\data\ActiveDataProvider
     * @throws yii\base\Exception
     */
    public static function topRated($count = null)
    {
        $dataProvider = Yii::$app->modelFactory->getSearchProduct()->search();
        $dataProvider->sort->defaultOrder = ['score' => SORT_DESC];

        if ($count) {
            $dataProvider->pagination = false;
            $dataProvider->query->limit($count);
        }
        return $dataProvider;
    }

    /**
     * @param Product $product
     * @param int $limit
     * @return ProductDataProvider|yii\data\ActiveDataProvider
     * @throws yii\base\Exception
     */
    public static function related(Product $product, $limit = 10)
    {
        $searchModel = Yii::$app->modelFactory->getSearchProduct();

        if ($category = $product->getCategories()->one()) {
            /**
             * @var ProductCategory $category
             */
            $searchModel->categoryId = $category->id;
        } else if ($product->tags) {
            $searchModel->tags = ArrayHelper::getColumn($product->tags, 'value');
        }
        $dataProvider = $searchModel->search();
        $dataProvider->query->andWhere(['not', ['p.id' => $product->id]]);

        if ($limit) {
            $dataProvider->pagination->pageSize = $limit;
        }
        return $dataProvider;
    }

    /**
     * @param Product $product
     * @param int $limit
     * @return ProductDataProvider|yii\data\ActiveDataProvider
     * @throws yii\base\Exception
     */
    public static function othersHaveAlsoBought(Product $product, $limit = 10)
    {
        $searchModel = Yii::$app->modelFactory->getSearchProduct();
        $dataProvider = $searchModel->search();

        $query = (new Query())
            ->select('k.product_id')
            ->from([
                'k' => (new Query())
                    ->select(['distinct(oi1.order_id)', 'oi2.product_id'])
                    ->from(['oi1' => Yii::$app->modelFactory->getOrderItem()::tableName()])
                    ->innerJoin(['o1' => Yii::$app->modelFactory->getOrder()::tableName()], 'o1.id = oi1.order_id')
                    ->innerJoin(['oi2' => Yii::$app->modelFactory->getOrderItem()::tableName()], [
                        'and',
                        'oi2.order_id = o1.id',
                        'oi2.product_id IS NOT NULL',
                        ['not', ['oi2.product_id' => $product->id]]
                    ])
                    ->where(['oi1.product_id' => $product->id])
            ])
            ->groupBy(['k.product_id'])
            ->orderBy(['COUNT(*)' => SORT_DESC, 'k.product_id' => SORT_ASC]);

//        if ($limit) {
//            $query->limit($limit);
//        }
        $dataProvider->query->andWhere(['p.id' => $query]);

        if ($limit) {
            $dataProvider->pagination->pageSize = $limit;
        }
        return $dataProvider;
    }
}
