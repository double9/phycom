<?php

namespace phycom\frontend\helpers;


use phycom\frontend\models\product\SearchProductCategory;

use yii\base\BaseObject;


/**
 * Class ProductCategoryHelper
 * @package phycom\frontend\helpers
 */
class ProductCategoryHelper extends BaseObject
{
    /**
     * @param null $limit
     * @return \phycom\frontend\models\product\ProductCategoryDataProvider|\yii\data\ActiveDataProvider
     */
    public static function featured($limit = null)
    {
        $searchModel = new SearchProductCategory();
        $searchModel->featured = true;

        $dataProvider = $searchModel->search();
        if ($limit && $dataProvider->pagination) {
            $dataProvider->pagination->setPageSize($limit);
        }
        return $dataProvider;
    }
}