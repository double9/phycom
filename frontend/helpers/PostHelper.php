<?php

namespace phycom\frontend\helpers;

use phycom\common\models\attributes\PostStatus;
use phycom\common\models\attributes\PostType;
use phycom\common\models\Post;
use phycom\frontend\models\post\PostDataProvider;
use phycom\frontend\models\post\SearchPost;

use phycom\common\models\PostCategory;
use phycom\common\models\PostTag;

use yii\base\BaseObject;
use yii;

/**
 * Class PostHelper
 * @package phycom\frontend\helpers
 */
class PostHelper extends BaseObject
{
    /**
     * @return Post|null|yii\db\ActiveRecord
     */
    public static function landingPage()
    {
        $post = Post::find()
            ->where(['type' => PostType::LAND])
            ->andWhere(['status' => PostStatus::PUBLISHED])
            ->orderBy(['published_at' => SORT_DESC])
            ->one();

        return $post;
    }

    /**
     * @return Post|null|yii\db\ActiveRecord
     */
    public static function latestPost()
    {
        $post = Post::find()
            ->where(['type' => PostType::POST])
            ->andWhere(['status' => PostStatus::PUBLISHED])
            ->orderBy(['published_at' => SORT_DESC])
            ->one();

        return $post;
    }

    /**
     * @param PostCategory|string $key - post category model or url key
     * @param int $limit
     * @return PostDataProvider
     *
     * @throws yii\base\Exception
     */
    public static function searchByCategory($key, $limit = 5)
    {
        if (is_string($key)) {
            $category = PostCategory::findByUrlKey($key);
        } else if ($key instanceof PostCategory) {
            $category = $key;
        } else {
            throw new yii\base\InvalidArgumentException('Invalid key given. Expected string or ' . PostCategory::class);
        }

        $dataProvider = Yii::$app->modelFactory->getSearchPost([
            'type' => PostType::POST,
            'categoryId' => $category ? $category->id : -1
        ])->search();

        if ($limit) {
            $dataProvider->pagination->pageSize = $limit;
        }
        return $dataProvider;
    }

    /**
     * @param int $limit
     * @return PostDataProvider
     * @throws yii\base\Exception
     */
    public static function latest($limit = 3)
    {
        $searchModel = Yii::$app->modelFactory->getSearchPost(['type' => PostType::POST]);
        $dataProvider = $searchModel->search();
        $dataProvider->sort->defaultOrder = ['published_at' => SORT_DESC];

        if ($limit) {
            $dataProvider->pagination->pageSize = $limit;
        }
        return $dataProvider;
    }
}