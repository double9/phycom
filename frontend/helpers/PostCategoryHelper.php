<?php

namespace phycom\frontend\helpers;

use phycom\frontend\models\post\PostCategoryDataProvider;
use phycom\frontend\models\post\PostArchiveDataProvider;
use phycom\frontend\models\post\SearchPostCategory;
use phycom\frontend\models\post\SearchPostArchive;

use phycom\common\models\attributes\CategoryStatus;

use yii\base\BaseObject;
use yii;

/**
 * Class PostCategoryHelper
 * @package phycom\frontend\helpers
 */
class PostCategoryHelper extends BaseObject
{
    /**
     * @param int|null $limit
     * @return PostCategoryDataProvider
     */
    public static function categories($limit = null)
    {
        $searchModel = new SearchPostCategory();
        $searchModel->language = Yii::$app->lang->current;
        $searchModel->topLevel = true;
        $searchModel->status = CategoryStatus::VISIBLE;
        $dataProvider = $searchModel->search();

        if ($limit) {
            $dataProvider->pagination->pageSize = 5;
        }
        return $dataProvider;
    }

    /**
     * @param int|null $limit
     * @return PostArchiveDataProvider
     */
    public static function archives($limit = null)
    {
        $searchArchive = new SearchPostArchive();
        $dataProvider = $searchArchive->search();

        if ($limit) {
            $dataProvider->pagination->pageSize = 5;
        }
        return $dataProvider;
    }
}