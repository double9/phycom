<?php
return [
    'adminEmail' => 'admin@example.com',
    'wishlist' => false,
    'cookiePolicyNotification' => true
];
