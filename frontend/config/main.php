<?php
$params = array_merge(
    require(PHYCOM_PATH . '/common/config/params.php'),
    require(__DIR__ . '/params.php'),
    require(ROOT_PATH . '/common/config/params.php'),
    require(ROOT_PATH . '/frontend/config/params.php')
);

return [
    'id'        => 'app-frontend',
    'basePath'  => ROOT_PATH . '/frontend',
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'modelFactory' => [
            'class' => phycom\frontend\components\ModelFactory::class
        ],
        'request' => [
            'csrfParam' => '_csrf-phycom-frontend',
        ],
        'user' => [
            'class'           => phycom\common\components\User::class,
            'identityClass'   => phycom\common\models\User::class,
            'enableAutoLogin' => true,
            'identityCookie'  => ['name' => '_identity-phycom-frontend'],
        ],
        'session' => [
            'name'         => 'phycom-frontend',            // this is the name of the session cookie used for login on the frontend
            'cookieParams' => ['lifetime' => 7 * 86400],    // one week lifetime
            'timeout'      => 7 * 86400,
        ],
        'geoip' => [
            'class' => \lysenkobv\GeoIP\GeoIP::class
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => yii\log\FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => require(__DIR__ . '/url-manager.php'),
	    'cart' => [
		    'class' => phycom\common\components\Cart::class,
            'on eventAfterClear' => function ($e) {
                if (Yii::$app->session->get('order')) {
                    Yii::$app->session->remove('order');
                }
            },
		    'storageClass' => [
			    'class' => \phycom\common\models\cart\storage\SessionStorage::class,
//			    'deleteIfEmpty' => true
		    ]
	    ],
        'deviceDetect' => [
            'class' => \alexandernst\devicedetect\DeviceDetect::class
        ],
    ],
    'params' => $params,
];
